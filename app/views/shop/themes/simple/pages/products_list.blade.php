@extends('shop/themes/'.Support::theme_path().'templates/products')

@section('products_list') 
<section class="product-list-options clearfix">
			
    <section class="medium-6 small-12 columns product-list-flex">

		<span class="product-number">Ukupno: {{ $count_products }}</span>
        @if(Options::product_number()==1)
		<section class="per-page JSselect-field">
		
			<span class="per-page-active">
				@if(Session::has('limit'))
				{{Session::get('limit')}}
				@else
				20
				@endif
			</span>
			
			<ul>
			
				<li><a href="{{ Options::base_url() }}limit/20">20</a></li>
				<li><a href="{{ Options::base_url() }}limit/30">30</a></li>
				<li><a href="{{ Options::base_url() }}limit/50">50</a></li>
			
			</ul>
		
		</section>
		@endif


		@if(Options::compare()==1)
	<div id="JScompareArticles" class="show-compered" data-reveal-id="compare-article" >Upoređeni artikli</div>
	@endif

	 	
	</section>

	<section class="medium-6 small-12 columns product-list-flex product-list-flex-end">
	 
        <div class="currencyANDsort clearfix">
        @if(Options::product_currency()==1)
            <section class="currency JSselect-field">

                <span class="currency-active">{{Articles::get_valuta()}}</span>

                <ul>

                    <li><a href="{{ Options::base_url() }}valuta/1">Din</a></li>
                    <li><a href="{{ Options::base_url() }}valuta/2">Eur</a></li>

                </ul>

            </section>
            @endif
            @if(Options::product_sort()==1)
            <section class="sort-products JSselect-field">

                <span class="sort-active">{{Articles::get_sort()}}</span>

                <ul>

                    <li><a href="{{ Options::base_url() }}sortiranje/price_asc">Cena - Rastuće</a></li>
                    <li><a href="{{ Options::base_url() }}sortiranje/price_desc">Cena - Opadajuće</a></li>
                    <li><a href="{{ Options::base_url() }}sortiranje/news">Najnovije</a></li>
                    <li><a href="{{ Options::base_url() }}sortiranje/name">Prema nazivu</a></li>

                </ul>

            </section>
            @endif
        </div>    
         @if(Options::product_view()==1)
		@if(Session::has('list'))
		<section class="view-buttons">
		
			<a href="{{ Options::base_url() }}prikaz/grid"><span class="grid-view-button "></span></a>
			<a href="{{ Options::base_url() }}prikaz/list"><span class="list-view-button active"></span></a>
		
		</section>

		@else
		<section class="view-buttons">
		
			<a href="{{ Options::base_url() }}prikaz/grid"><span class="grid-view-button active"></span></a>
			<a href="{{ Options::base_url() }}prikaz/list"><span class="list-view-button"></span></a>
		
		</section>

		@endif
        @endif

	
	</section>

	 
</section>
<section class="pagination-container">{{ Paginator::make($articles, $count_products, $limit)->links() }}</section>
<section class="compare-section">
	<div id="compare-article" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
		<div id="JScompareTable" class="table-scroll"></div>
		<a class="close-reveal-modal close" aria-label="Close"><i class="fa fa-close"></i></a>
	</div>
</section>

<!-- PRODUCTS -->
@if(Session::has('list') or Options::product_view()==3)
<!-- LIST PRODUCTS -->
<section class="JSproduct-list list-view row">
	@foreach($articles as $row)
		@include('shop/themes/'.Support::theme_path().'partials/products/product_on_list') 
	@endforeach
</section>

@else
<!-- Grid proucts -->
<section class="JSproduct-list row">
	@foreach($articles as $row)
		@include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
	@endforeach
</section>		
@endif

@if($count_products == 0)
<p class="no-articles"> Trenutno nema artikla za date kategorije</p>
@endif

<section class="pagination-container">{{ Paginator::make($articles, $count_products, $limit)->links() }}</section>
			
@endsection	
