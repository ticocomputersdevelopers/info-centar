@extends('shop/themes/'.Support::theme_path().'templates/main')


@section('baners_sliders')
			<!-- MAIN SLIDER -->
			<section id="JSmain-slider">
				@foreach(DB::table('baneri')->where('tip_prikaza',2)->orderBy('redni_broj','asc')->get() as $row)
				<div>
					<a href="{{ $row->link }}">
						<img src="{{ Options::domain() }}{{ $row->img }}" alt="{{$row->naziv}}" />
					</a>
				</div> 
				
				@endforeach
			</section>
@endsection



@section('page')
  @include('shop/themes/'.Support::theme_path().'partials/products/action_type')

		<h2>Najpopularniji proizvodi</h2>
		<section class="JSMostPopularProducts JSproduct-slider">
			@foreach(Articles::mostPopularArticles() as $row)
				@include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
			@endforeach
		</section>

		@if(count(Articles::bestSeller()))
		<h2>Najprodavaniji proizvodi</h2>
		<section class="JSBestSellerProducts JSproduct-slider">
			@foreach(Articles::bestSeller(4) as $row)
				@include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
			@endforeach
		</section>
		@endif

		<h2>Najnoviji proizvodi</h2>
		<section class="JSMostPopularProducts JSproduct-slider">
			@foreach(Articles::latestAdded() as $row)
				@include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
			@endforeach
		</section>
@endsection