
<footer>
	<div class="container">
		<div class="row">
			<nav class="medium-4 columns">
			    <ul class="footer-links">
			    @foreach(All::footer_pages() as $row)
                        @if($row->tip_artikla_id == -1)
                        <li> 
                            <a href="{{ Options::base_url().$row->naziv_stranice }}">
                                {{ ($row->title) }}
                            </a>
                         </li>
                        @else
                            @if($row->tip_artikla_id==0)
                            <li> 
                                <a href="{{ Options::base_url().Url_mod::convert_url('akcija') }}">
                                    {{ ($row->title) }}
                                </a>
                            </li>
                            @else
                            <li> 
                                <a href="{{ Options::base_url().Url_mod::convert_url('tip').'/'.Url_mod::url_convert(Support::tip_naziv($row->tip_artikla_id)) }}">
                                    {{ ($row->title) }}
                                </a>
                            </li>
                            @endif
                        @endif
                    @endforeach 
				</ul>
				&nbsp;
			</nav>
			
			<div class="medium-4 columns newsletter-checkin">
				@if(Options::newsletter()==1)
					<h4>Prijava za newsletter</h4>
					<div class="newsletter row">
						<input type="text"  placeholder="E-mail adresa" id="newsletter" />
						<button onclick="newsletter()">Prijavi se</button>
					</div>
				@endif
				<div class="social-icons">
					 {{Options::social_icon()}}
				</div>
			</div>
 
			<!-- <div class="medium-3 medium-offset-1 columns">
                <div class="cards small-12 medium-12 columns">
                    <div class="card">
                        <a href="http://www.maestrocard.com/gateway/index.html" target="_blank">
							<img src="{{ Options::domain() }}images/cards/brand3.png" alt="MaestroCard">
						</a>
                    </div>
                    <div class="card">
                       <a href="http://www.mastercardbusiness.com" target="_blank">
							<img src="{{ Options::domain() }}images/cards/footerMasterCard.png" alt="MasterCard">
						</a>
                    </div>
                    <div class="card">
                      <a href="https://rs.visa.com/" target="_blank">
						<img src="{{ Options::domain() }}images/cards/footerVisa.png" alt="Visa">
					  </a>
                    </div>
                </div>
			</div> -->
			
			<div class="medium-12 columns footnote">
				<p class="info"> Cene su informativnog karaktera. Prodavac ne odgovara za tačnost cena iskazanih na sajtu, zadržava pravo izmena cena. Ponudu za ostale artikle, informacije o stanju lagera i aktuelnim cenama možete dobiti na upit. Plaćanje se isključivo vrši virmanski - bezgotovinski.
				</p>
				Izrada internet prodavnice - <a href="https://www.selltico.com">Selltico</a> &copy; {{ date('Y') }}. Sva prava zadržana.
			</div>
		</div> <!-- end .row -->
	</div> <!-- end .container -->
</footer>


<!-- FOOTNOTE -->



