<div class="JSproduct">
	<div class="product-content clearfix">
		@if(All::provera_akcija($row->roba_id))
		<div class="product-sale">- {{ Product::getSale($row->roba_id) }} din.</div>
		@endif
		<a href="{{Options::base_url()}}artikal/{{Url_mod::url_convert(Product::seo_title($row->roba_id))}}" class="product-image-wrapper">
			<img class="product-image" src="{{ Options::domain() }}{{ Product::web_slika($row->roba_id) }}" alt="{{ Product::seo_title($row->roba_id) }}" />
		</a>
		<a class="product-title" href="{{Options::base_url()}}artikal/{{Url_mod::url_convert(Product::seo_title($row->roba_id))}}">{{ Product::short_title($row->roba_id) }}</a>
		<section class="action-holder clearfix">
		 <div class="pricer"> 
			<span class="product-price">{{ Cart::cena(Product::get_price($row->roba_id)) }}</span>
			@if(All::provera_akcija($row->roba_id))
			<span class="old-price">{{ Cart::cena(Product::old_price($row->roba_id)) }}</span>
			@endif
		</div>
		@if(Product::getStatusArticle($row->roba_id) == 1)
			@if(Cart::check_avaliable($row->roba_id) > 0)
			<div class="btn-container">
				@if(Options::compare()==1 AND $strana != All::get_page_start() AND $strana != 'artikal')
				<button class="JScompare {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}">
					<i class="fa fa-exchange" aria-hidden="true"></i> Uporedi
				</button>
				@endif
				<button data-roba_id="{{$row->roba_id}}" class="dodavnje JSadd-to-cart"> <i class="fa fa-cart-plus"></i> U korpu</button>
			</div>
			@else 
                <div class="btn-container">
                    @if(Options::compare()==1 AND $strana != All::get_page_start() AND $strana != 'artikal')
                    <button class="JScompare {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}">
                    	<i class="fa fa-exchange" aria-hidden="true"></i> Uporedi
                    </button>
                    @endif
                    <button class="dodavnje not-available">Nije dostupno</button>	
                </div>
			@endif
		@else
		<div class="btn-container">
			@if(Options::compare()==1 AND $strana != All::get_page_start() AND $strana != 'artikal')
				<button class="JScompare {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}">
					<i class="fa fa-exchange" aria-hidden="true"></i> Uporedi
				</button>
			@endif
			<button class="dodavanje not-available">{{ Product::find_flag_cene(Product::getStatusArticle($row->roba_id),'naziv') }}</button>
		</div>
		@endif

		 </section>
		@if(Session::has('b2c_admin'.Options::server()) AND Support::check_admin(array(200)))
			<a class="article-edit-btn article-edit-btn-grid" href="{{ Options::base_url() }}admin/product/{{ $row->roba_id }}">IZMENI ARTIKAL</a>
		@endif
	</div>
</div>
