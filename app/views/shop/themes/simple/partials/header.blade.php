<header class="row">
    <!-- LOGO AREA -->
    <section class="logo-area medium-3 columns text-center small-only-text-left">
        <h1 class="for-seo"> 
            <a class="logo" href="/" title="{{Options::company_name()}}">
                <img src="{{ Options::domain() }}{{Options::company_logo()}}" alt="{{Options::company_name()}}" />
            </a>
        </h1>
    </section>
    
    <!-- SEARCH AREA -->
    <section class="JSsearchContent medium-6 columns">
        <div class="JSsearch">
            <input class="search-field" autocomplete="off" data-timer="" type="text" id="JSsearch" placeholder="Pretraga" />
            <button onclick="search()" class="JSsearch-button"> <i class="fa fa-search"></i> </button>
        </div>
    </section>
    
    <!-- CART AREA -->
    @include('shop/themes/'.Support::theme_path().'partials/cart_top')

     <div class="JSmenu-icon"><i class="fa fa-bars" aria-hidden="true"></i></div>
     
</header>