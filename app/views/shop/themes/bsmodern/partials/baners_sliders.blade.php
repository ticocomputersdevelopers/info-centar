    <!-- MAIN SLIDER -->  
    <div id="JSmain-slider" class="">
        <?php foreach(DB::table('baneri')->where('tip_prikaza',2)->where('aktivan',1)->orderBy('redni_broj','asc')->limit(10)->get() as $row){ ?>
         @if((Support::trajanje_banera($row->baneri_id)) == 1 && (Support::future_banners($row->baneri_id)) == 1)
        <?php $slajd_data = Support::slajd_data($row->baneri_id); ?>
        <div class="relative">
            <a href="<?php echo $row->link; ?>">
                <img class="img-responsive" src="{{ Options::domain() }}<?php echo $row->img; ?>" alt="{{$row->naziv}}" />
            </a>
               <div class="sliderText"> 
                @if($slajd_data->nadnaslov != '')
                <div>
                    <p class="short-desc JSInlineShort" data-target='{"action":"slide_pretitle","id":"{{$row->baneri_id}}"}'>
                        {{ $slajd_data->nadnaslov }}
                    </p>
                </div>
                @endif

                @if($slajd_data->naslov != '')
                <div>
                    <h2 class="main-desc JSInlineShort" data-target='{"action":"slide_title","id":"{{$row->baneri_id}}"}'>
                        {{ $slajd_data->naslov }}
                    </h2>
                </div>
                @endif

                @if($slajd_data->sadrzaj != '')
                <div>
                    <div class="short-desc JSInlineFull" data-target='{"action":"slide_content","id":"{{$row->baneri_id}}"}'>
                        {{ $slajd_data->sadrzaj }}
                    </div>
                </div>
                @endif

                @if($slajd_data->naslov_dugme != '')
                <div>
                    <a href="<?php echo $row->link; ?>" class="slider-link JSInlineShort" data-target='{"action":"slide_button","id":"{{$row->baneri_id}}"}'>
                        {{ $slajd_data->naslov_dugme }}
                    </a>
                </div>
                @endif
            </div>
        </div>
        @endif
        <?php } ?>
    </div>
    <!-- BANNERS -->
    <div class="banners-right main-banners">
        <?php 
        foreach(DB::table('baneri')->where('tip_prikaza', 1)->where('aktivan',1)->orderBy('redni_broj','asc')->get() as $row){ ?>
        @if((Support::trajanje_banera($row->baneri_id)) == 1 && (Support::future_banners($row->baneri_id)) == 1)
            <div class="no-padding">
                <a href="<?php echo $row->link; ?>">
                    <img class="img-responsive" src="{{ Options::domain() }}<?php echo $row->img; ?>" alt="{{$row->naziv}}" />
                </a>
            </div>
        @endif    
        <?php } ?>
    </div>  