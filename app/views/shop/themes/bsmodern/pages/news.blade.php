@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
<div class="news-section">
	@foreach(All::getNews() as $row)
		<article class="news">
			<div class="row">
				<div class="col-md-4 col-sm-4 col-xs-12">
					<img class="img-responsive center-block" src="{{ Options::domain() }}{{ $row->slika }}" alt="{{ $row->naslov }}">
				</div>
				<div class="col-md-8 col-sm-8 col-xs-12 news-text">
					<a href="{{ Options::base_url() }}{{ Url_mod::convert_url('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}">
						<h2><span class="heading-background heading-background-news">{{ $row->naslov }}</span></h2>
					</a>
					 {{ All::shortNewDesc($row->tekst) }} 
					<div class="text-right">
						<a class="news__more" href="{{ Options::base_url() }}{{ Url_mod::convert_url('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}">{{ Language::trans('Pročitaj članak') }}</a>
					</div>
				</div>
			</div> <!--  end of .row -->
		</article>
	@endforeach
	<div>
		{{ All::getNews()->links() }}
	</div> 
</div>
@endsection