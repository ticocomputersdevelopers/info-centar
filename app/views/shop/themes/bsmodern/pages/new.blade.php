@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
<div class="news-section">
	<article class="single-news">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<img class="img-responsive center-block" src="{{ Options::domain() }}{{ $slika }}" alt="{{ $naslov }}">
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12 news-text">
				<h2 class="news-title">
					<span class="heading-background heading-background-news">{{ $naslov }}</span>
				</h2>
				 {{ $sadrzaj }} 
			</div>
		</div> <!--  end of .row -->
	</article> 
</div>
@endsection