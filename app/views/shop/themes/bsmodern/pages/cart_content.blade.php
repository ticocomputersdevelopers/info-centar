@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')

<div class="cart-page">
	@if(Session::has('b2c_korpa') and Cart::broj_cart() >= 1 )
 
	    <div class="h2-container">
			<h2><span class="heading-background">{{ Language::trans('Vaša Korpa') }}</span></h2>
		</div>
		@if(Session::has('failure-message'))
			<h2>{{ Session::get('failure-message') }}</h2>
		@endif
		<ul class="cart-labels row hidden-sm hidden-xs">
			<li class="col-md-3 col-sm-3 col-sm-offset-2 col-md-offset-2">{{ Language::trans('Proizvod') }}</li>			 
			<li class="col-md-2 col-sm-2">{{ Language::trans('Cena') }}</li>
			<li class="col-md-2 col-sm-2">{{ Language::trans('Količina') }}</li>
			<li class="col-md-2 col-sm-2">{{ Language::trans('Ukupno') }}</li>
			<li class="col-md-1 col-sm-1">{{ Language::trans('Ukloni') }}</li>		 
		</ul>
		@foreach(DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_id',Session::get('b2c_korpa'))->orderBy('web_b2c_korpa_stavka_id','asc')->get() as $row)
		<ul class="JScart-item row">	 
			<li class="JScart-image col-md-2 col-sm-2 col-xs-12">
				<img src="{{ Options::domain() }}{{Product::web_slika($row->roba_id) }}" alt="{{ Product::short_title($row->roba_id) }}" class="cart-image img-responsive" />
			</li>
			<li class="cart-name col-md-3 col-sm-3 col-xs-12">
				<span> 
					<a class="" href="{{ Options::base_url() }}{{Url_mod::convert_url('artikal')}}/{{ Url_mod::url_convert(Product::seo_title($row->roba_id)) }}">
					{{ Product::short_title($row->roba_id) }}
					</a>
					{{Product::getOsobineStr($row->roba_id,$row->osobina_vrednost_ids)}}  
				</span>
			</li>

			<li class="cart-price col-md-2 col-sm-2 col-xs-12 hidden-xs"><span>{{ Cart::cena($row->jm_cena) }}</span></li>
			<li class="col-md-2 col-sm-2 col-xs-12">
				<div class="cart-add-amount clearfix">
					<a data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}" class="JScart-less" href="javascript:void(0)"><</a>
					<input type="text" class="JScart-amount" value="{{ round($row->kolicina) }}" onkeypress="validate(event)" data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}">
					<a data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}" class="JScart-more" href="javascript:void(0)">></a>
				</div>
			</li>
			<li class="cart-total-price col-md-2 col-sm-2 col-xs-12"><span>{{ Cart::cena($row->jm_cena*$row->kolicina) }}</span></li>
			<li class="cart-remove col-md-1 col-sm-1 col-xs-12">
				<a href="javascript:void(0)" data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}" class="close JSDeleteStavka">X</a>
			</li>			 
		 </ul>
		@endforeach
 

	<!-- BELOW CART -->
	<div class="below-cart row">
		<div  class="cart-remove-all-wrapper col-md-2 col-sm-5 col-xs-12">
			<button class="cart-remove-all" id="JSDeleteCart">{{ Language::trans('Isprazni korpu') }}</button>
		</div>
    </div>

    <div class="row clearfix"> 
		<div class="cart-summary-wrapper col-md-4 col-sm-6 col-xs-12 pull-right">
			@if(Options::checkTezina() == 1 AND Options::checkTroskoskovi_isporuke() == 0 AND Cart::troskovi_isporuke()>0)
			<div class="cart-summary row">
				<span class="summary-label col-md-6 col-sm-6 col-xs-6">{{ Language::trans('Cena artikala') }}: </span>
				<span class="summary-amount col-md-6 col-sm-6 col-xs-6">{{Cart::cena(Cart::cart_ukupno())}}</span>
			</div>
			<div class="cart-summary row">
				<span class="summary-label col-md-6 col-sm-6 col-xs-7">{{ Language::trans('Troškovi isporuke') }}: </span>
				<span class="summary-amount col-md-6 col-sm-6 col-xs-5">{{Cart::cena(Cart::troskovi_isporuke())}}</span>
			</div>
			<div class="cart-summary row">
				<span class="summary-label col-md-6 col-sm-6 col-xs-6">{{ Language::trans('Ukupno') }}: </span>
				<span class="summary-amount col-md-6 col-sm-6 col-xs-6">{{Cart::cena(Cart::cart_ukupno()+Cart::troskovi_isporuke())}}</span>
			</div>
			@elseif(Options::checkTroskoskovi_isporuke() == 1 AND Options::checkTezina() == 0 AND Cart::cart_ukupno() < Cart::cena_do() AND Cart::cena_do() > 0)
			<div class="cart-summary row">
				<span class="summary-label col-md-6 col-sm-6 col-xs-6">{{ Language::trans('Cena artikala') }}: </span>
				<span class="summary-amount col-md-6 col-sm-6 col-xs-6">{{Cart::cena(Cart::cart_ukupno())}}</span>
			</div>
			<div class="cart-summary row">
				<span class="summary-label col-md-6 col-sm-6 col-xs-7">{{ Language::trans('Troškovi isporuke') }}: </span>
				<span class="summary-amount col-md-6 col-sm-6 col-xs-5">{{Cart::cena(Cart::cena_dostave())}}</span>
			</div>
			<div class="cart-summary row">
				<span class="summary-label col-md-6 col-sm-6 col-xs-6">{{ Language::trans('Ukupno') }}: </span>
				<span class="summary-amount col-md-6 col-sm-6 col-xs-6">{{Cart::cena(Cart::cart_ukupno()+Cart::cena_dostave())}}</span>
			</div>
			@else
			<div class="cart-summary row">
				<span class="summary-label col-md-6 col-sm-6 col-xs-6">{{ Language::trans('Ukupno') }}: </span>
				<span class="summary-amount col-md-6 col-sm-6 col-xs-6">{{Cart::cena(Cart::cart_ukupno())}}</span>
			</div>
			@endif
		</div>
	 </div>

	<!-- CART ACTION BUTTONS -->
	<ul class="cart-action-buttons clearfix text-right list-inline" id="cart_form_scroll">
		@if(!Session::has('b2c_kupac'))
		@if(Options::neregistrovani_korisnici()==1)
		<li><button class="no-registration-btn" id="JSRegToggle">{{ Language::trans('Kupi bez registracije') }}</button></li>
		@endif
		<li><a class="no-registration-btn inline-block" href="#" role="button" id="login-icon" data-toggle="modal" data-target="#loginModal">{{ Language::trans('Prijavi se') }}</a></li>
		@endif
	</ul>
 <!-- SUBMIT ORDER AREA -->
 
 <!-- BUY WITHOUT REGISTRATION FORM -->
		<div class="without-registration" id="JSRegToggleSec" {{ Session::has('b2c_kupac') ? "" : (count(Input::old()) == 0 ? "hidden='hidden'" : "") }}> 
		
		<form method="POST" action="{{ Options::base_url() }}order-create" id="JSOrderForm" class="without-reg-form row">
			@if(Options::neregistrovani_korisnici()==1 AND !Session::has('b2c_kupac'))
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12"> 
				@if(Input::old('flag_vrsta_kupca') == 1)
		 			<div class="JSCheckVrsta without-btn personal col-md-6 col-sm-6 col-xs-12 text-center" data-vrsta="personal">
		 				{{ Language::trans('Fizičko Lice') }} <i class="fas fa-check"></i> 
		 			</div>

					<div class="JSCheckVrsta without-btn active none-personal col-md-6 col-sm-6 col-xs-12 text-center" data-vrsta="non-personal">
						{{ Language::trans('Pravno Lice') }} <i class="fas fa-check"></i>
					</div>
		 		@else
				 	<div class="JSCheckVrsta without-btn active personal col-md-6 col-sm-6 col-xs-12 text-center" data-vrsta="personal">
				 		{{ Language::trans('Fizičko Lice') }} <i class="fas fa-check"></i>
				 	</div>
					<div class="JSCheckVrsta without-btn none-personal col-md-6 col-sm-6 col-xs-12 text-center" data-vrsta="non-personal">
						{{ Language::trans('Pravno Lice') }} <i class="fas fa-check"></i>
					</div>
			 	@endif
				</div>
			</div>
			@endif
		    <input type="hidden" name="flag_vrsta_kupca" value="{{ Input::old('flag_vrsta_kupca') == 1 ? '1' : '0' }}">

			 <div class="col-md-12 col-sm-12 col-xs-12">
	 			@if(Options::neregistrovani_korisnici()==1 AND !Session::has('b2c_kupac'))
		 	  <div class="row"> 
				<div class="col-md-6 col-sm-12 col-xs-12 form-group JSwithout-reg-none-personal {{ Input::old('flag_vrsta_kupca') == 1 ? 'active' : '' }}">
					<label for="without-reg-company"><span class="red-dot-error">*</span> {{ Language::trans('Naziv Firme') }}:</label>
					<input id="without-reg-company" class="form-control" name="naziv" type="text" tabindex="1" value="{{ htmlentities(Input::old('naziv') ? Input::old('naziv') : '') }}">
					<div class="error red-dot-error">{{ $errors->first('naziv') ? $errors->first('naziv') : "" }}</div>
				</div>

				<div class="col-md-6 col-sm-12 col-xs-12 form-group JSwithout-reg-none-personal {{ Input::old('flag_vrsta_kupca') == 1 ? 'active' : '' }}">
				 	<label for="without-reg-pib">{{ Language::trans('PIB') }}:</label>
					<input id="without-reg-pib" name="pib" type="text" tabindex="2" value="{{ htmlentities(Input::old('pib') ? Input::old('pib') : '') }}">
			        <div class="error red-dot-error">{{ $errors->first('pib') ? $errors->first('pib') : "" }}</div>
				</div>

				<div class="col-md-6 col-sm-12 col-xs-12 form-group JSwithout-reg-personal {{ Input::old('flag_vrsta_kupca') == 1 ? '' : 'active' }}">
					<label for="without-reg-name"><span class="red-dot-error">*</span> {{ Language::trans('Ime') }}</label>
					<input id="without-reg-name" class="form-control" name="ime" type="text" tabindex="1" value="{{ htmlentities(Input::old('ime') ? Input::old('ime') : '') }}">
					<div class="error red-dot-error">{{ $errors->first('ime') ? $errors->first('ime') : "" }}</div>
				</div>

				<div class="col-md-6 col-sm-12 col-xs-12 form-group JSwithout-reg-personal {{ Input::old('flag_vrsta_kupca') == 1 ? '' : 'active' }}">
					<label for="without-reg-surname"><span class="red-dot-error">*</span> {{ Language::trans('Prezime') }}</label>
					<input id="without-reg-surname" class="form-control" name="prezime" type="text" tabindex="2" value="{{ htmlentities(Input::old('prezime') ? Input::old('prezime') : '') }}">
					<div class="error red-dot-error">{{ $errors->first('prezime') ? $errors->first('prezime') : "" }}</div>
				</div>

				<div class="col-md-6 col-sm-12 col-xs-12 form-group"> 
					<label for="without-reg-phone"><span class="red-dot-error">*</span> {{ Language::trans('Telefon') }}</label>
					<input id="without-reg-phone" class="form-control" name="telefon" type="text" tabindex="3" value="{{ htmlentities(Input::old('telefon') ? Input::old('telefon') : '') }}">
					<div class="error red-dot-error">{{ $errors->first('telefon') ? $errors->first('telefon') : "" }}</div>
				</div>

				<div class="col-md-6 col-sm-12 col-xs-12 form-group"> 
					<label for="without-reg-e-mail"><span class="red-dot-error">*</span> E-mail</label>
					<input id="JSwithout-reg-email" class="form-control" name="email" type="text" tabindex="4" value="{{ htmlentities(Input::old('email') ? Input::old('email') : '') }}">
					<div class="error red-dot-error">{{ $errors->first('email') ? $errors->first('email') : "" }}</div>
			    </div>
	 		 </div>
		 
	 		 <div class="row">
				<div class="col-md-6 col-sm-12 col-xs-12 form-group"> 
					<label for="without-reg-address"><span class="red-dot-error">*</span> {{ Language::trans('Adresa za dostavu') }}</label>
					<input id="without-reg-address" class="form-control" name="adresa" type="text" tabindex="5" value="{{ htmlentities(Input::old('adresa') ? Input::old('adresa') : '') }}">
					<div class="error red-dot-error">{{ $errors->first('adresa') ? $errors->first('adresa') : "" }}</div>
				</div>
				<div class="col-md-6 col-sm-12 col-xs-12 form-group"> 
				   <label for="without-reg-city"><span class="red-dot-error">*</span> {{ Language::trans('Mesto') }}/{{ Language::trans('Grad') }}</label>				 
					<input type="text" class="form-control" name="mesto" tabindex="6" value="{{ htmlentities(Input::old('mesto') ? Input::old('mesto') : '') }}">
				    <div class="error red-dot-error">{{ $errors->first('mesto') ? $errors->first('mesto') : "" }}</div>
				</div>
	 		  </div>
				@endif

				@if(Options::neregistrovani_korisnici()==1 OR Session::has('b2c_kupac'))
			 <div class="row"> 
	 			<div class="col-md-6 col-sm-12 col-xs-12 form-group">
					<label>{{ Language::trans('Način isporuke') }}:</label>
					<select name="web_nacin_isporuke_id" class="form-control" tabindex="7">
						{{Order::nacin_isporuke(Input::old('web_nacin_isporuke_id'))}}
					</select>
				</div>

				<div class="col-md-6 col-sm-12 col-xs-12 form-group">
					<label>{{ Language::trans('Način plaćanja') }}:</label>
					<select name="web_nacin_placanja_id" class="form-control" tabindex="8">
						{{Order::nacin_placanja(Input::old('web_nacin_placanja_id'))}}
					</select>
				</div>
	 		  </div>

				<div class="row"> 
					<div class="col-md-6 col-sm-6 col-xs-12 form-group"> 
						<label>{{ Language::trans('Napomena') }}:</label>
						<textarea class="form-control" rows="5" tabindex="9" name="napomena">{{ htmlentities(Input::old('napomena') ? Input::old('napomena') : '') }}</textarea>
					 </div>
					 <div class="col-md-6 col-sm-6 col-xs-12 form-group"> 
					 	<label>&nbsp;</label>
					 	<div class="capcha text-center" id="JSCaptcha" {{(Input::old('web_nacin_placanja_id') == 3 ? '' : 'hidden')}}> 
							{{ Captcha::img(5, 160, 50) }}<br>
							<span>{{ Language::trans('Unesite kod sa slike') }}</span>
							<input type="text" name="captcha-string" class="form-control" tabindex="10" autocomplete="off">
							<div class="error red-dot-error">{{ $errors->first('captcha') ? $errors->first('captcha') : "" }}</div>
						</div>
					 </div>
				 </div>
				 <div class="row">
				 	<div class="col-md-12 col-sm-12 col-xs-12">  
						<div class="required-fields"><span>*</span> {{ Language::trans('Obavezna polja') }}</div>
					</div>
				 </div>
	 			@endif
			 </div>
		 	@if(Options::neregistrovani_korisnici()==1 OR Session::has('b2c_kupac'))
			<div class="text-center col-md-12 col-sm-12 col-xs-12"><br>
				<button id="JSOrderSubmit" class="forward">{{ Language::trans('Završi kupovinu') }}</button>
			</div>
			@endif
		</form>	
	</div>
 
	@else
	<div class="cart">
		<div class="h2-container">
			<h2><span class="heading-background">{{ Language::trans('Vaša Korpa') }}</span></h2>
		</div>
		<span class="no-articles">{{ Language::trans('Trenutno nemate artikle u korpi') }}.</span>
	</div>
	@endif
</div>

@endsection