@extends('shop/themes/'.Support::theme_path().'templates/article')

@section('article_details')

 <div id="fb-root"></div> 
            <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12';
            fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
            
            @if(Session::has('success_add_to_cart'))
            $(document).ready(function(){    
                $('.JSinfo-popup').fadeIn().delay(1000).fadeOut();
                $('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>Artikal je dodat u korpu.</p>");
            });
            @endif
            </script>
    <!-- MIDDLE AREA -->
        <div id="middle-area" class="container-fluid no-padding">
            <!-- MAIN CONTENT -->
            <div id="main-content" class="bw">
                <!-- PRODUCT -->
                <ul class="breadcrumb"> 
                    {{ Product::product_bredacrumps(DB::table('roba')->where('roba_id',$roba_id)->pluck('grupa_pr_id')) }}
                </ul>
                <div id="product-preview" itemscope itemtype="http://schema.org/Product" class="clearfix">
 
                    <div class="row">
                        <!-- PRODUCT PREVIEW IMAGE -->
                        <div class="JSproduct-preview-image col-lg-6 col-md-6 col-sm-12 col-xs-12">
                              <div id="gallery_01" class="col-lg-3 col-md-3 col-sm-3 col-xs-12 text-center"> 
                                {{ Product::get_list_images($roba_id) }}  &nbsp;
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-6 col-xs-12 disableZoomer relative"> 
                            <a class="fancybox" href="{{ Options::domain() }}{{ Product::web_slika_big($roba_id) }}">
                                <span id="zoom_03" class="JSzoom_03" style="display: block;" data-zoom-image="{{ Options::domain() }}{{ Product::web_slika_big($roba_id) }}" >
                                    <img itemprop="image" class="JSzoom_03 img-responsive" id="art-img" src="{{ Options::domain() }}{{ Product::web_slika_big($roba_id) }}" alt="{{ Product::seo_title($roba_id)}}" />
                                </span>
                            </a>
                            </div>
                           
                         </div>

                        <div class="product-preview-info col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <h1 class="article-heading" itemprop="name">{{ Language::trans(Product::seo_title($roba_id)) }}</h1>
                  <!-- BRENDOVI -->
                        @if($proizvodjac_id != -1)
                        <div class="product-manufacturer" itemprop="brand">                             
                            @if( Product::slikabrenda($roba_id) != null )
                            <a class="article-brand-img" href="{{Options::base_url()}}{{Url_mod::convert_url('proizvodjac')}}/{{Url_mod::url_convert(Product::get_proizvodjac($roba_id)) }}">
                                <img src="{{ Options::domain() }}{{product::slikabrenda($roba_id) }}" alt="{{ product::get_proizvodjac($roba_id) }}" />
                            </a>
                            @else
                            <a href="{{Options::base_url()}}{{Url_mod::convert_url('proizvodjac')}}/{{Url_mod::url_convert(Product::get_proizvodjac($roba_id)) }}">
                                <span class="artical-brand-text">{{ product::get_proizvodjac($roba_id) }}</span>
                            </a>
                            @endif                                   
                        </div>
                        @endif

                        <!-- ARTICLE PASSWORD -->
                        @if(AdminOptions::sifra_view_web()==1)
                        <div class="article-sifra"><b>Šifra: {{Product::sifra($roba_id)}}</b></div>
                        @elseif(AdminOptions::sifra_view_web()==4)                       
                        <div class="article-sifra">Šifra: {{Product::sifra_d($roba_id)}}</div>
                        @elseif(AdminOptions::sifra_view_web()==3)                       
                        <div class="article-sifra">Šifra: {{Product::sku($roba_id)}}</div>
                        @elseif(AdminOptions::sifra_view_web()==2)                       
                        <div class="article-sifra"><b>Šifra: {{Product::sifra_is($roba_id)}}</b></div>
                        @endif
                        <div class="rate-me-artical">
                            {{ Product::getRating($roba_id) }}
                        </div>
                          <ul>
                             @if($grupa_pr_id != -1)
                             <li class="product-group">{{Language::trans('Proizvod iz grupe')}}:{{ Product::get_grupa($roba_id) }}</li>
                             @endif
                                @if($proizvodjac_id != -1)
                                <li class="product-manufacturer" itemprop="brand">{{Language::trans('Proizvođač')}}: 
                                    @if(Support::checkBrand($roba_id))
                                    <a href="{{ Options::base_url() }}{{ Url_mod::convert_url('proizvodjac')}}/{{ Url_mod::url_convert(Product::get_proizvodjac($roba_id)) }}">
                                        {{Product::get_proizvodjac($roba_id)}}
                                    </a>
                                    @else
                                    <span class="article-manufacturer-text"> {{Product::get_proizvodjac($roba_id)}} </span>
                                    @endif
                                </li>
                                @endif 

                               <!--  @if(Options::vodjenje_lagera() == 1)
                                    <li class="product-available-amount">{{Language::trans('Dostupna količina')}}: <span> {{Cart::check_avaliable($roba_id)}}</span></li>
                                @endif -->

                                @if(Options::checkTezina() == 1 AND Product::tezina_proizvoda($roba_id)>0)
                                    <li class="product-available-amount">{{Language::trans('Težina artikla')}}: <a href="#"> {{Product::tezina_proizvoda($roba_id)/1000}} kg</a></li>
                                @endif
                                
<!--                                 @if(Options::checkTags() == 1)
                                    @if(Product::tags($roba_id) != '')
                                    <li class="product-available-amount">
                                        {{Language::trans('Tagovi')}}: {{ Product::tags($roba_id) }}
                                    </li>
                                    @else
                                    <li class="product-available-amount">
                                        {{Language::trans('Tagovi')}}: {{Language::trans('Nema tagova')}}
                                    </li>
                                    @endif
                                @endif -->
                            </ul>

                          <!-- PRICE -->
                            <div class="product-preview-price">
                                @if(Product::pakovanje($roba_id))
                                    <span class="product-preview-price-new" itemprop="price"><span class="PDV_price">{{Language::trans('Pakovanje')}}: </span>
                                    <span class="mpPrice">{{ Product::ambalaza($roba_id) }}</span>
                                @endif                                 
                                @if(All::provera_akcija($roba_id))                                      
                                    @if(Product::get_mpcena($roba_id) != 0)
                                    <span class="product-preview-price-new" itemprop="price"><span class="PDV_price">{{Language::trans('Maloprodajna cena')}}: </span>
                                    <span class="mpPrice">{{ Cart::cena(Product::get_mpcena($roba_id)) }}</span>
                                    @endif

                                </span>
                                    <span class="product-preview-price-new" itemprop="price"><span class="PDV_price">{{Language::trans('Akcijska cena:')}}</span>{{ Cart::cena(Product::get_price($roba_id)) }}</span>
                                    @if(Product::getPopust_akc($roba_id)>0)
                                    <span class="PDV_price">{{Language::trans('Popust')}}: <i class="discount-price">{{ Cart::cena(Product::getPopust_akc($roba_id)) }}</i></span>
                                    @endif
                                @else
                                @if(Product::get_mpcena($roba_id) != 0)
                                    <span class="product-preview-price-new" itemprop="price"><span class="PDV_price">{{Language::trans('Maloprodajna cena')}}: </span>
                                    <span class="mpPrice">{{ Cart::cena(Product::get_mpcena($roba_id)) }}</span>
                                @endif
                                </span>
                                    <span class="product-preview-price-new" itemprop="price"><span class="PDV_price">{{Language::trans('WebCena:')}} </span>{{ Cart::cena(Product::get_price($roba_id)) }}</span>
                                    @if(Product::getPopust($roba_id)>0)
                                    @if(AdminOptions::web_options(132)==1)
                                        <span class="PDV_price">{{Language::trans('Popust')}}: <i class="discount-price">{{ Cart::cena(Product::getPopust($roba_id)) }}</i></span>
                                    @endif
                                    @endif
                                @endif
                             </div>
                          <!-- Facebook button -->
                                <div class="facebook-btn-share">
                                      <!-- facebook share button -->
                                      <div class="soc-network inline-block"> 
                                         <div class="fb-like" data-href="{{Options::base_url()}}{{Url_mod::convert_url('artikal')}}/{{Url_mod::url_convert(Product::seo_title($roba_id))}}" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
                                      </div>
                    <!-- Twitter button -->
                                     <div class="soc-network"> 
                                        <a href="https://twitter.com/share" class="twitter-share-button">Tweet</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                                     </div>  
                                </div>
                    <!-- FEATURES -->
                         
                            
                            <div class="add-to-cart-area clearfix">     
                                 @if(Product::getStatusArticle($roba_id) == 1)
                                    @if(Cart::check_avaliable($roba_id) > 0)
                                    <form method="POST" action="{{ Options::base_url() }}product-cart-add" id="JSAddCartForm" class="article-form">  @if(Product::check_osobine($roba_id))
                                        <div class="osobine">
                                        @foreach(Product::osobine_nazivi($roba_id) as $osobina_naziv_id)
                                            <div class="osobina">
                                                <div class="osobina_naziv">{{ Product::find_osobina_naziv($osobina_naziv_id,'naziv') }}</div>
                                                <div class="karakteristike_osobine">
                                                    @foreach(Product::osobine_vrednosti($roba_id,$osobina_naziv_id)->get() as $row2)
                                                    <label class="osobina_karakteristika" style="background-color: {{ Product::find_osobina_vrednost($row2->osobina_vrednost_id, 'boja_css') }}">
                                                        <input type="radio" name="osobine{{ $osobina_naziv_id }}" value="{{ $row2->osobina_vrednost_id }}" {{ Product::check_osobina_vrednost($roba_id,$osobina_naziv_id,$row2->osobina_vrednost_id,Input::old('osobine'.$osobina_naziv_id)) }}>
                                                        <span title="{{ Product::find_osobina_vrednost($row2->osobina_vrednost_id, 'vrednost') }}">
                                                            {{ Product::osobina_vrednost_checked($osobina_naziv_id, $row2->osobina_vrednost_id) }}
                                                        </span>
                                                    </label>
                                                    @endforeach
                                                </div>
                                            </div>
                                        @endforeach
                                        </div>
                                        @endif

                                       @if(Cart::kupac_id() > 0)
                                             <div class="like-it"><a data-roba_id="{{$roba_id}}" title="Dodaj na listu želja" class="JSadd-to-wish far fa-heart wish-list"></a></div> 
                                        @else
                                             <div class="like-it JSnot_logged"><a data-roba_id="{{$roba_id}}" title="Dodavanje artikla na listu želja moguće je samo registrovanim korisnicima" class="far fa-heart wish-list"></a></div> 
                                        @endif
                                        <input type="hidden" name="roba_id" value="{{ $roba_id }}">
                                        <div class="input_kolicina">Količina</div>
                                        <input type="text" name="kolicina" class="cart-amount" value="{{ Input::old('kolicina') ? Input::old('kolicina') : '1' }}">
                                        <button id="JSAddCartSubmit" class="JSadd-to-cart add-to-cart-artikal">U korpu</button>
                                        <div>{{ $errors->first('kolicina') ? $errors->first('kolicina') : '' }}</div>  
                                    </form>
                                    @else
                                        <button class="dodavanje not-available">Nije dostupno</button>                                  
                                    @endif
                                @else
                                <button class="dodavanje not-available" data-roba-id="{{$roba_id}}">
                                    {{ Product::find_flag_cene(Product::getStatusArticle($roba_id),'naziv') }}
                                </button>
                                @endif
                            </div>
                            <!-- ADMIN BUTTON-->
                            @if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array(200)))
                                <div class="admin-article"> 
                                    <a class="article-level-edit-btn JSFAProductModalCall" data-roba_id="{{$roba_id}}" href="javascript:void(0)">{{ Language::trans('IZMENI ARTIKAL') }}</a> 
                                    <span class="supplier"> {{ Product::get_dobavljac($roba_id) }}</span> 
                                    <span class="supplier">{{ Language::trans('NCP') }}: {{ Product::get_ncena($roba_id) }}</span>
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="row product-tags"> 
                        <div class="col-md-12 sm-12 xs-12"> 
                            @if(Options::checkTags() == 1)
                                @if(Product::tags($roba_id) != '')
                                <span>Tagovi:</span>
                                <div>
                                    {{ Product::tags($roba_id) }} 
                                </div>
                                @endif
                            @endif    
                        </div>
                    </div>        
                    <!-- PRODUCT PREVIEW TABS-->
                    <div class="product-preview-tabs row">
                      <div class="col-md-12 sm-12 xs-12"> 
                        <ul class="nav nav-tabs tab-titles">
                            <li class="active"><a data-toggle="tab" href="#description-tab">{{Language::trans('Opis')}}</a></li>
                            <li><a data-toggle="tab" href="#technical-docs">{{Language::trans('Sadržaji')}}</a></li>
                            <li><a data-toggle="tab" href="#the-comments">{{Language::trans('Komentari')}}</a></li>
                        </ul>
                            <div class="tab-content"> 
            <!-- DESCRIPTION -->
                            <div id="description-tab" class="tab-pane fade in active">
                                <p itemprop="description"> {{ Product::get_opis($roba_id) }} </p>
                                {{ Product::get_karakteristike($roba_id) }}
                            </div>
            <!-- TECHNICAL DOCS -->
                            <div id="technical-docs" class="tab-pane fade">
                                @if(Options::web_options(120))
                                    @if(count($fajlovi) > 0)
                                    <div>
                                      <!--   <h2 class="h2-margin">Dodatni fajlovi</h2> -->
                                        @foreach($fajlovi as $row)
                                        <div class="files-list-item">
                                            <a class="files-link" href="{{ $row->putanja != null ? Options::domain().$row->putanja : $row->putanja_etaz }}" target="_blank">
                                                <img src="{{ Options::domain() }}images/file-icon.png" alt="{{ $row->naziv }}">
                                                <div class="files-list-item">
                                                    <div class="files-name">{{ Language::trans($row->naziv) }}</div> <!-- {{ Product::getExtension($row->vrsta_fajla_id) }} --> 
                                                </div>
                                            </a>
                                        </div>
                                        @endforeach
                                    </div>
                                    @endif
                                 @endif
                            </div>
                <!-- COMMENTS -->
                            <div id="the-comments" class="tab-pane fade">
                                <div class="row"> 
                                <?php $query_komentary=DB::table('web_b2c_komentari')->where(array('roba_id'=>$roba_id,'komentar_odobren'=>1));
                                if($query_komentary->count() > 0){?>
                                <div class="col-md-6 col-sm-12 col-xs-12"> 
                                <ul class="comments">
                                    <?php foreach($query_komentary->get() as $row)
                                    { ?>
                                    <li class="comment">
                                        <ul class="comment-content">
                                            <li class="comment-name">{{$row->ime_osobe}}</li>
                                            <li class="comment-date">{{$row->datum}}</li>
                                            <li class="comment-rating">{{Product::getRatingStars($row->ocena)}}</li>
                                            <li class="comment-text">{{ $row->pitanje }}</li>
                                        </ul>
                                        <!-- REPLIES -->
                                        @if($row->odgovoreno == 1)
                                        <ul class="replies">
                                            <li class="comment">
                                                <ul class="comment-content">
                                                    <li class="comment-name">{{ Options::company_name() }}</li>
                                                    <li class="comment-text">{{ $row->odgovor }}</li>
                                                </ul>
                                             </li>
                                         </ul>
                                         @endif
                                      </li>
                                            <?php }?>
                                      </ul>
                                      </div>
                                        <?php }?>
                                       <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"> 
                                            <div class="form-group">
                                                <label for="JScomment_name">{{Language::trans('Vaše ime')}}</label>
                                                <input id="JScomment_name" class="form-control" onchange="check_fileds('JScomment_name')" type="text"  />
                                            </div>

                                            <div class="form-group clearfix">
                                                <label for="JScomment_message">{{Language::trans('Komentar')}}</label>
                                                <textarea class="comment-message form-control" rows="5" id="JScomment_message" onchange="check_fileds('JScomment_message')"></textarea>
                                                <input type="hidden" value="{{ $roba_id }}" id="JSroba_id" />
                                                <span class="review">
                                                    <span>{{Language::trans('Ocena')}}:</span>
                                                    <i id="JSstar1" class="fa fa-star-o review-star" aria-hidden="true"></i>
                                                    <i id="JSstar2" class="fa fa-star-o review-star" aria-hidden="true"></i>
                                                    <i id="JSstar3" class="fa fa-star-o review-star" aria-hidden="true"></i>
                                                    <i id="JSstar4" class="fa fa-star-o review-star" aria-hidden="true"></i>
                                                    <i id="JSstar5" class="fa fa-star-o review-star" aria-hidden="true"></i>
                                                    <input id="JSreview-number" value="0" type="hidden"/>
                                                </span>
                                                 <button class="submit pull-right JScomment_add">{{Language::trans('Pošalji')}}</button>
                                            </div>
                                        </div>
                                    </div>
                                 </div>
                             </div>
                <!-- BRENDOVI SLAJDER -->
                        <!--  <div class="row">
                             <div class="col-md-12">
                                <div class="dragg JSBrandSlider"> 
                                       <?php //foreach(DB::table('proizvodjac')->where('brend_prikazi',1)->get() as $row){ ?>
                                    <div class="col-md-12 col-sm-6 end sub_cats_item_brend">
                                        <a class="brand-link" href="{{Options::base_url()}}{{ Url_mod::convert_url('proizvodjac') }}/<?php //echo $row->naziv; ?> ">
                                             <img src="{{ Options::domain() }}<?php //echo $row->slika; ?>" />
                                         </a>
                                    </div>
                                    <?php //} ?>
                                </div>
                             </div>
                          </div> -->
                   
                    @if(Options::web_options(118))
                            <div class="vezani_artikli row">
                                <div class="h2-container">
                                    <br>
                                    <h2 class="JSLinked"><span class="heading-background">{{Language::trans('Vezani artikli')}}</span></h2>
                                </div>
                                 
                            @foreach($vezani_artikli as $row)
                                @if(Product::checkView($row->roba_id))
                                <div class="JSproduct col-md-3 col-sm-4 col-xs-12 no-padding">
                                    <div class="shop-product-card relative"> 
                                        <!-- PRODUCT IMAGE -->
                                        <div class="product-image-wrapper relative">

                                            <a href="{{Options::base_url()}}{{Url_mod::convert_url('artikal')}}/{{Url_mod::url_convert(Product::seo_title($row->vezani_roba_id))}}">
                                            <img class="product-image img-responsive" src="{{ Options::domain() }}{{ Product::web_slika($row->vezani_roba_id) }}" alt="{{ Product::seo_title($row->vezani_roba_id) }}" />
                                            </a>
                                            <a href="{{Options::base_url()}}{{Url_mod::convert_url('artikal')}}/{{Url_mod::url_convert(Product::seo_title($row->vezani_roba_id))}}" class="article-details hidden-xs">
                                            <span class="glyphicon glyphicon-zoom-in"></span> {{Language::trans('Detaljnije')}}</a>
                                        </div>

                                        <div class="product-meta">
                                            <div class="row text-right"> 
                                                <span class="review">
                                                    {{ Product::getRating($row->roba_id) }}
                                                </span>
                                            </div>
                                    <!-- PRODUCT PRICE -->
                                            <div class="price-holder row">
                                                <span class="product-price"> {{ Cart::cena(Product::get_price($row->vezani_roba_id)) }}</span>
                                            </div>   
                                            <a class="product-name text-left" href="{{Options::base_url()}}{{Url_mod::convert_url('artikal')}}/{{Url_mod::url_convert(Product::seo_title($row->vezani_roba_id))}}"><h2> {{ Product::short_title($row->vezani_roba_id) }}</h2></a>

                                            @if($row->flag_cena == 1)
                                                <div class="add-to-cart-container">
                                                    <!-- WISH LIST  --> 
                                                    @if(Cart::kupac_id() > 0)
                                                          <div class="like-it"><button data-roba_id="{{$row->roba_id}}" title="Dodaj na listu želja" class="JSadd-to-wish far fa-heart wish-list"></button></div> 
                                                    @else
                                                          <div class="like-it JSnot_logged"><button data-roba_id="{{$row->roba_id}}" title="Dodavanje artikla na listu želja moguće je samo registrovanim korisnicima" class="far fa-heart wish-list"></button>
                                                         </div> 
                                                    @endif      
                                                    @if(Product::getStatusArticle($row->roba_id) == 1)
                                                        @if(Cart::check_avaliable($row->vezani_roba_id) > 0)
                                                            <button class="JSadd-to-cart-vezani buy-btn" data-roba_id="{{ $row->vezani_roba_id }}">
                                                            {{Language::trans('U korpu')}}</button>
                                                            <input type="text" class="JSkolicina linked-articles-input like-it" value="1" onkeypress="validate(event)">
                                                        @else  
                                                            <button class="dodavnje not-available buy-btn">{{Language::trans('Nije dostupno')}}</button>
                                                        @endif
                                                        @else
                                                            <button class="dodavnje not-available buy-btn">
                                                                {{ Product::find_flag_cene(Product::getStatusArticle($row->roba_id),'naziv') }}
                                                            </button>
                                                      @endif
                                                </div>
                                            @endif
                                        </div>
                               <!-- ADMIN BUTTON -->
                                         @if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array(200)))
                                            <a class="article-edit-btn JSFAProductModalCall" data-roba_id="{{$row->vezani_roba_id}}" href="#">{{Language::trans('IZMENI ARTIKAL')}}</a>
                                         @endif
                                    </div>
                                </div>
                                @endif
                            @endforeach
                     
                        </div> 
                        @endif
                            <!-- RELATED PRODUCTS -->
                            <div class="h2-container row">
                                <br>
                                <h2 class="JSRelated"><span class="heading-background">{{Language::trans('Srodni proizvodi')}}</span></h2>
                            </div>
                            <div class="JSrelated-products JSproduct-slider row">
                            @foreach(Product::get_related($roba_id) as $row)
                                @include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
                            @endforeach
                            </div>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
@endsection