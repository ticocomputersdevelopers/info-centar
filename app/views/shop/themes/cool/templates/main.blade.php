<!DOCTYPE html>
<html>

	<head>
		@include('shop/themes/'.Support::theme_path().'partials/head')
		
		<script type="application/ld+json">
			{  
				"@context" : "http://schema.org",
				"@type" : "WebSite",
				"name" : "<?php echo Options::company_name(); ?>",
				"alternateName" : "<?php echo $title." | ".Options::company_name(); ?>",
				"url" : "<?php echo Options::domain(); ?>"
			}
		</script>
	</head>
<body 
    @if($strana == All::get_page_start()) id="start-page"  @endif 
    @if(Support::getBGimg() != null) 
    
    @endif
>

	<div class="row">
        <!-- PREHEADER -->
        @include('shop/themes/'.Support::theme_path().'partials/menu_top')
       
        <!-- HEADER -->
        @include('shop/themes/'.Support::theme_path().'partials/header')
        
        @if(Options::category_view()==0)
        @include('shop/themes/'.Support::theme_path().'partials/categories/categories_horizontal')
        @endif
		<!-- MIDDLE AREA -->
        <div class="main-wrapper bw clearfix">
            <section id="middle-area" class="row">
                <div class="row">
                    <div class="row">
                    @yield('baners_sliders')
                    </div>
                </div>

                <!-- MAIN CONTENT -->
                <section id="main-content" class="medium-12 columns">
                    @yield('page')
                </section>
            </section>
        </div>
		
		<!-- FOOTER -->
		@include('shop/themes/'.Support::theme_path().'partials/footer')
			
	</div>

 	<a class="JSscroll-top" href="javascript:void(0)" ><i class="fa fa-chevron-up"></i></a>
    <!-- LOGIN POPUP -->
    @include('shop/themes/'.Support::theme_path().'partials/popups')

    <!-- BASE REFACTORING -->
    <input type="hidden" id="base_url" value="{{Options::base_url()}}" />
    <input type="hidden" id="vodjenje_lagera" value="{{Options::vodjenje_lagera()}}" />
    <input type="hidden" id="lat" value="{{ All::lat_long()[0] }}" />
    <input type="hidden" id="long" value="{{ All::lat_long()[1] }}" />
    
    <!-- js includes -->
	<script src="{{Options::domain()}}js/jquery-1.11.2.min.js" type="text/javascript"></script>
	@if(Session::has('b2c_admin'.Options::server()))
	<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
	@endif
	@if($strana == All::get_page_start())
	<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}action_type.js" type="text/javascript"></script>
	<script src="{{Options::domain()}}js/slick.min.js" type="text/javascript"></script>
	@endif
    @if($strana=='sve-kategorije')
        <script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}all-categories.js" type="text/javascript" ></script>
    @endif
	<script src="{{Options::domain()}}js/foundation.min.js" type="text/javascript"></script>
	<script src="{{Options::domain()}}js/jquery.accordion.js" type="text/javascript"></script>
	<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}main_function.js" type="text/javascript"></script>
    <script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}cart.js" type="text/javascript"></script>
	<script src="{{Options::domain()}}js/jquery.fancybox.pack.js" type="text/javascript" ></script>
	<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}main.js" type="text/javascript"></script>
    
    @if($strana==Seo::get_korpa())
    <script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}order.js" type="text/javascript"></script>
    @endif
    <script src="{{Options::domain()}}js/shop/login_registration.js" type="text/javascript" ></script>

	@if($strana=='konfigurator' AND Options::web_options(121))
	<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}configurator.js" type="text/javascript"></script>
	@endif

    @if($strana==Seo::get_kontakt())
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCf05l1NhASLAtI_hy9k4Z2EowvME7JyoM&callback=initMap"></script>
    <script src="{{Options::domain()}}js/shop/map_initialization.js" type="text/javascript"></script>
    @endif
</body>
</html>
