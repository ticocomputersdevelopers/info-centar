<nav class="manufacturer-categories"> 
    <h3>Proizvođač - {{$title}}</h3>
 
    <!-- CATEGORIES LEVEL 1 -->

    <ul>
        @foreach(Support::manufacturer_categories($proizvodjac_id) as $key => $value)
            <li>
                <a href="{{ Options::base_url()}}proizvodjac/{{ Url_mod::url_convert($proizvodjac) }}/{{ Url_mod::url_convert($key) }}">
                    <span>{{ $key }}</span>
                    <span>{{ $value }}</span>
                </a>
            </li>
        @endforeach 
    </ul>
</nav>
