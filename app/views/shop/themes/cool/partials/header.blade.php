<header class="row boja">
    <div class="main-wrapper clearfix align-flex-center">
        <!-- LOGO AREA -->
        <section class="logo-area medium-3 small-12 columns">
            <h1 class="for-seo"> 
                <a class="logo" href="/" title="{{Options::company_name()}}">
                    <img src="{{ Options::domain() }}{{Options::company_logo()}}" alt="{{Options::company_name()}}" />
                </a>     
            </h1>     
        </section>

        <!-- SEARCH AREA -->
        <section class="JSsearchContent small-12 medium-6 columns left">
            <div class="JSsearch">
                <input class="search-field" autocomplete="off" data-timer="" type="text" id="JSsearch" placeholder="Pretraga" />
                <button onclick="search()" class="JSsearch-button"><i class="fa fa-search"></i></button>
            </div>
        </section>  

        <!-- CART AREA -->
        @include('shop/themes/'.Support::theme_path().'partials/cart_top')
    </div>

    <div class="menu-background row">
        <div class="main-wrapper clearfix">

            <!-- MAIN MENU -->
            <!-- SIDEBAR LEFT -->
            <aside id="sidebar-left" class="medium-3 large-3 columns">
                @if(Options::category_view()==1)
                @include('shop/themes/'.Support::theme_path().'partials/categories/category')
                @endif                   
            </aside>

            <nav class="large-9 medium-9 columns JSmain-nav">

                <ul id="main-menu" class="clearfix">

                    @foreach(All::header_pages('flag_page') as $row)
                        @if($row->tip_artikla_id == -1)
                        <li> 
                            <a href="{{ Options::base_url().$row->naziv_stranice }}">
                                {{ ($row->title) }}
                            </a>
                         </li>
                        @else
                            @if($row->tip_artikla_id==0)
                            <li> 
                                <a href="{{ Options::base_url().Url_mod::convert_url('akcija') }}">
                                    {{ ($row->title) }}
                                </a>
                            </li>
                            @else
                            <li> 
                                <a href="{{ Options::base_url().Url_mod::convert_url('tip').'/'.Url_mod::url_convert(Support::tip_naziv($row->tip_artikla_id)) }}">
                                    {{ ($row->title) }}
                                </a>
                            </li>
                            @endif
                        @endif
                    @endforeach

                    @if(Options::web_options(121)==1)
                        <?php $konfiguratori = All::getKonfiguratos(); ?>
                        @if(count($konfiguratori) > 0)
                            @if(count($konfiguratori) > 1)
                                <li class="dropdown">
                                    <a href="#" class="dropbtn">Konfiguratori</a>
                                    <ul class="dropdown-content">
                                        @foreach($konfiguratori as $row)
                                        <li><a href="{{ Options::base_url() }}konfigurator/{{ $row->konfigurator_id }}">{{ $row->naziv }}</a></li>
                                        @endforeach
                                    </ul>
                                </li>
                            @else
                                <li><a href="{{ Options::base_url() }}konfigurator/{{ $konfiguratori[0]->konfigurator_id }}">Konfigurator</a></li>
                            @endif
                        @endif
                    @endif
                </ul>
           </nav>
        </div>
        <div class="JSmenu-icon"><i class="fa fa-bars" aria-hidden="true"></i></div>
    </div>
</header>