<section class="small-12 medium-3 columns text-center">	
	<div class="header-cart">
		<a class="header-cart-icon" href="{{ Options::base_url() }}{{ Seo::get_korpa() }}">Vaša korpa <span class="JSbroj_cart">{{ Cart::broj_cart() }}</span>
        <input type="hidden" id="h_br_c" value="{{ Cart::broj_cart() }}" /></a>
		<ul class="JSheader-cart-content">
			@include('shop/themes/'.Support::theme_path().'partials/mini_cart_list')
		</ul>
	</div>
</section>