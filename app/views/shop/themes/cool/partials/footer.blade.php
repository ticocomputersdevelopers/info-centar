
<footer>
	<div class="main-wrapper clearfix footer-background">
		<div class="row">
			<nav class="medium-4 small-12 columns">
			    <ul class="footer-links">
			    	@foreach(All::footer_pages() as $row)
                        @if($row->tip_artikla_id == -1)
                        <li> 
                            <a href="{{ Options::base_url().$row->naziv_stranice }}">
                                {{ ($row->title) }}
                            </a>
                         </li>
                        @else
                            @if($row->tip_artikla_id==0)
                            <li> 
                                <a href="{{ Options::base_url().Url_mod::convert_url('akcija') }}">
                                    {{ ($row->title) }}
                                </a>
                            </li>
                            @else
                            <li> 
                                <a href="{{ Options::base_url().Url_mod::convert_url('tip').'/'.Url_mod::url_convert(Support::tip_naziv($row->tip_artikla_id)) }}">
                                    {{ ($row->title) }}
                                </a>
                            </li>
                            @endif
                        @endif
                    @endforeach &nbsp;
				</ul>
			</nav>
			
			<div class="medium-4 small-12 columns newsletter-checkin">
				@if(Options::newsletter()==1)
					<h4>Prijava za newsletter</h4>
					<div class="newsletter row">
						<input type="text"  placeholder="E-mail adresa" id="newsletter" />
						<button onclick="newsletter()">Prijavi se</button>
					</div>
				@endif
				<div class="social-icons text-center">
					 {{Options::social_icon()}} 
				</div>
			</div>

		<!-- 	<div class="medium-4 small-12 columns banke-centar">
				<div class="row">
					<a class="medium-12 columns text-right small-only-text-center" href="http://www.visa.ca/verified/infopane/index.html" target="_blank">
					<img src="{{ Options::domain() }}images/cards/visa.jpg" alt="visa"></a>
				</div>

				<div class="row">
					<a class="medium-12 columns text-right small-only-text-center" href="http://www.mastercardbusiness.com/mcbiz/index.jsp?template=/orphans&content=securecodepopup" target="_blank">
					<img src="{{ Options::domain() }}images/cards/mastercard.gif" alt="mastercard"></a>
				</div>

				<!- <div class="row">
					<a class="medium-12 columns text-right small-only-text-center" href="https://www.trustmark.com/" target="_blank">
					<img src="{{ Options::domain() }}images/cards/etrustmark.png" alt="trustmark"></a>
				</div> 
           </div> -->
			      <!-- POLICY -->
         

			<div class="medium-12 small-12 columns footnote text-center">
				<p class="info"> Cene su informativnog karaktera. Prodavac ne odgovara za tačnost cena iskazanih na sajtu, zadržava pravo izmena cena. Ponudu za ostale artikle, informacije o stanju lagera i aktuelnim cenama možete dobiti na upit. Plaćanje se isključivo vrši virmanski - bezgotovinski.
				</p>
				Izrada internet prodavnice - <a href="https://www.selltico.com">Selltico</a> &copy; {{ date('Y') }}. Sva prava zadržana.
			</div>
		</div> <!-- end .row -->
	</div> <!-- end .container -->
</footer>


<!-- FOOTNOTE -->



