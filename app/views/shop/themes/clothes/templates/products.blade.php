<!DOCTYPE html>
<html lang="sr">
	<head>
		@include('shop/themes/'.Support::theme_path().'partials/head')
	</head>
	<body id="product-page">
	 
	<!-- Header -->
	@include('shop/themes/'.Support::theme_path().'partials/header')
				
		
	<div class="products-page bw">

		<div class="container">

			<div class="row">
				<div class="col-md-12">
					@if($strana=='artikli')
					    <h2 class="title">
				            {{ $grupa_pr_id==0 ? Language::trans('Svi artikli') : Groups::getGrupa($grupa_pr_id) }}
				        </h2>
					@elseif($strana=='proizvodjac')
						<h2 class="title">
				            {{ Language::trans($proizvodjac) }}
				        </h2>
					@elseif($strana=='tip')
						<h2 class="title">
				            {{ Language::trans($tip) }}
				        </h2>
					@elseif($strana=='akcija')
						<h2 class="title">
				            {{ Language::trans('Artikli na akciji') }}
				        </h2>
				    @endif 
				</div>
			</div>


			<div class="row products-page-content">

				<aside id="sidebar-left" class="col-md-3 col-sm-12 col-xs-12">
					<h3 class="category-title">
						Kategorije
					</h3>
					@include('shop/themes/'.Support::theme_path().'partials/filters')
				</aside>
 
	            @yield('products_list')

			</div> <!-- End of row -->
		</div> <!-- End of container -->

    </div> <!-- End of products-page -->
		
	<!-- Footer -->
	@include('shop/themes/'.Support::theme_path().'partials/footer')
 
 	<a class="JSscroll-top" href="javascript:void(0)"><i class="fa fa-chevron-up"></i></a>

	<!-- LOGIN POPUP -->
	@include('shop/themes/'.Support::theme_path().'partials/popups')

	<!-- BASE REFACTORING -->
	<input type="hidden" id="base_url" value="{{Options::base_url()}}" />
	<input type="hidden" id="lang_id" value="{{Language::lang_id()}}" />
	<input type="hidden" id="vodjenje_lagera" value="{{Options::vodjenje_lagera()}}" />

	<!-- js includes -->
	@if(Session::has('b2c_admin'.Options::server()) OR (Options::enable_filters()==1 AND $filter_prikazi))
	<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
	@endif
	@if(Session::has('b2c_admin'.Options::server()))
	<script type="text/javascript" src="{{Options::domain()}}js/tinymce2/tinymce.min.js"></script>
	<script src="{{ Options::domain()}}js/jquery-ui.min.js"></script>
	<script src="{{Options::domain()}}js/shop/front_admin/main.js"></script>
	<script src="{{Options::domain()}}js/shop/front_admin/products.js"></script>
	<script src="{{Options::domain()}}js/shop/front_admin/product.js"></script>
	@endif	

	<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}main_function.js"></script>
	<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}cart.js"></script>
	<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}main.js"></script>

  	@if(Options::header_type()==1)
		<!-- fixed navgation bar -->
 		<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}fixed_header.js"></script>
	@endif


	<script type="text/javascript">
		
		// filters slide down
		$(".JSfilters-slide-toggle").click(function(){
		    $(this).next(".JSfilters-slide-toggle-content").slideToggle('fast');
		});

		if($('.filter-box').length > 0 ) {
			$('.selected-filters-wrapper').css('margin-bottom', '12px');
		}
		
		if($('.selected-filters li').length > 0 ) {
			$('.filters-button-div').css('display','block');
			$('.selected-filters-wrapper').css('margin-bottom', '35px');
			
		}
	
	</script>
</body>
</html>