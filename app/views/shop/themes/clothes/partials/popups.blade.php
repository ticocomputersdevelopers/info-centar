
<div class="popup info-confirm-popup JSinfo-popup">
    <div class="popup-wrapper">	
	    <div class="JSpopup-inner">
		 
	    </div>	
	</div>
</div>

@foreach(Support::popup_banners() as $popup_banner) 
	@if($popup_banner->tip_prikaza == 9)
		<div class="JSfirst-popup first-popup">
			<div class="first-popup-inner relative">  
				 <a href="{{ $popup_banner->link }}" class="relative inline-block" target="_blank">  
				 	<img class="popup-img" src="{{AdminOptions::base_url()}}{{$popup_banner->img}}">	
				 </a> 
				 <span class='JSclose-me-please'>&times;</span>
		 	</div>
		</div>
	@endif
@endforeach