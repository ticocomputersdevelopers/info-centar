

<div class="header-cart">
	<a href="{{ Options::base_url() }}{{ Seo::get_korpa() }}">
		<span class="desc">Korpa</span>

		<span class="cart-icon"><i class="fas fa-shopping-basket"></i></span>
		<span class="JSbroj_cart"> {{ Cart::broj_cart() }} </span> 
		<input type="hidden" id="h_br_c" value="{{ Cart::broj_cart() }}" />	
	</a>
  
	<div class="JSheader-cart-content">
		@include('shop/themes/'.Support::theme_path().'partials/mini_cart_list') 
	</div>
</div>	

