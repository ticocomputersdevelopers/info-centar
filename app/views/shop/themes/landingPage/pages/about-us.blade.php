@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')

	<div class="about-us-page">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<h2 class="title">
						{{ Language::trans('O nama') }}
					</h2>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="about-short-desc">
						<p>
							Bane was a member of the League of Shadows. And then he was excommunicated. And any man who is too extreme for Ra's Al Ghul, is not to be trifled with.

							The first time I stole so that I wouldn't starve, yes. I lost many assumptions about the simple nature of right and wrong. And when I traveled I learned the fear before a crime and the thrill of success. But I never became one of them.
						</p>
					</div>
				</div>
			</div>

			
			<div class="row">
				<div class="col-md-4 col-sm-4 col-xs-4 member-div">
					<div class="img-frame" style="background-image: url('https://internationalscents.com/wp-content/uploads/2017/02/Office-man-737x737.jpg');">
					</div>
					<span class="team-name">Marko Marković</span>
					<span class="team-desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</span>
				</div>

				<div class="col-md-4 col-sm-4 col-xs-4 member-div">
					<div class="img-frame" style="background-image: url('https://d2gg9evh47fn9z.cloudfront.net/800px_COLOURBOX4569087.jpg');">

					</div>
					<span class="team-name">Marko Marković</span>
					<span class="team-desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</span>
				</div>

				<div class="col-md-4 col-sm-4 col-xs-4 member-div">
					<div class="img-frame" style="background-image: url('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRpdtedGKlDxm8e9fcbMCGAFOncYTzD6sD61OixNA1G0Exxc0LU');">
				
					</div>
					<span class="team-name">Marko Marković</span>
					<span class="team-desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</span>
				</div>
			</div>
		</div>


		<div class="about-list-div">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="about-page-list">
							<ul>
								<li>But it's not who you are underneath.</li>
								<li>Never underestimate Gotham City.</li>
								<li>It means you're hatred, and it also means losing someon.</li>
								<li>You have nothing, nothing to threaten me with.</li>
								<li>Nothing to do with all your strength.</li>
								<li>It's what you do that defines you.</li>
							</ul>
						</div>
					</div>

					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="about-page-list">
							<ul>
								<li>But it's not who you are underneath.</li>
								<li>Never underestimate Gotham City.</li>
								<li>It means you're hatred, and it also means losing someon.</li>
								<li>You have nothing, nothing to threaten me with.</li>
								<li>Nothing to do with all your strength.</li>
								<li>It's what you do that defines you.</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>


		
		<div class="about-text-div">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<div class="about-text">
							<p>
								Tragamo za novim članovima. Prijavi se da budes deo našeg tima.
								There is a prison in a more ancient part of the world. A pit where men are thrown to suffer and die. But sometimes a man rises from the darkness. Sometimes, the pit sends something back.

								To manipulate the fears in others, you must first master your own. Are you ready to begin?

								Theatricality and deception, powerful agents to the uninitiated. But we are initiated aren't we, Bruce? Members of the League of Shadows. And you betrayed us.

								To manipulate the fears in others, you must first master your own. Are you ready to begin?

								Theatricality and deception, powerful agents to the uninitiated. But we are initiated aren't we, Bruce? Members of the League of Shadows. And you betrayed us.
							</p>
						</div>
					</div>

					<div class="col-md-6">
						<div class="img-div">
							<img class="img-responsive" src="https://cdn.colorlib.com/wp/wp-content/uploads/sites/2/seocrawler-minimal-marketing-agency-wordpress-theme.jpg">
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>

@endsection