@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('baners_sliders')
    @include('shop/themes/'.Support::theme_path().'partials/baners_sliders')
@endsection



@section('page')

	<div id="home-content">

		<div class="kzm-section">
			<div class="container">
				<div class="row">
					<div class="col-md-4 col-sm-4 col-xs-4">
						<h3 class="heading">
							3.000
						</h3>
						<span class="short-desc">
							Mladih u Jagodini (2017)
						</span>
					</div>

					<div class="col-md-4 col-sm-4 col-xs-4">
						<h3 class="heading">
							70%
						</h3>
						<span class="short-desc">
							Mladih nezoposlenih
						</span>
					</div>

					<div class="col-md-4 col-sm-4 col-xs-4">
						<h3 class="heading">
							100%
						</h3>
						<span class="short-desc">
							Motivacije za promenu
						</span>
					</div>	
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="main-desc">
							{{AdminSupport::tekst_pocetna()}}
						</div>
					</div>
				</div>
			</div>
		</div>
	
		<!-- <div class="features-section">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h2 class="action-heading">
							{{ Language::trans('Our Services') }}
						</h2>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="short-desc">
							<p>My name is Merely Ducard but I speak for Ra's al Ghul... a man greatly feared by the criminal underworld. A mon who can offer you a path. Someone like you is only here by choice.You have been exploring the criminal fraternity but whatever your original intentions you have to become truly lost.</p>
						</div>
					</div>
				</div>
		
				<div class="features-content">
					<div class="row">
						<div class="col-md-4">
							<i class="icon far fa-gem"></i>
							<span class="heading">Digital Design</span>
							<div class="desc">You have inspired good. But you spat in the faces of Gotham's criminals. Didn't you think there might be casualties? </div>
						</div>
		
						<div class="col-md-4">
							<i class="icon fas fa-solar-panel"></i>
							<span class="heading">Unlimited Color</span>
							<div class="desc">You have inspired good. But you spat in the faces of Gotham's criminals. Didn't you think there might be casualties? </div>
						</div>
		
						<div class="col-md-4">
							<i class="icon fas fa-landmark"></i>
							<span class="heading">Strategy Solutions</span>
							<div class="desc">You have inspired good. But you spat in the faces of Gotham's criminals. Didn't you think there might be casualties? </div>
						</div>
		
					</div>
				</div>
		
				<div class="features-content">
					<div class="row">
						<div class="col-md-4">
							<i class="icon fab fa-react"></i>
							<span class="heading">Awesome Support</span>
							<div class="desc">You have inspired good. But you spat in the faces of Gotham's criminals. Didn't you think there might be casualties? </div>
						</div>
		
						<div class="col-md-4">
							<i class="icon fas fa-cogs"></i>
							<span class="heading">Truly Multipurpose</span>
							<div class="desc">You have inspired good. But you spat in the faces of Gotham's criminals. Didn't you think there might be casualties? </div>
						</div>
		
						<div class="col-md-4">
							<i class="icon fab fa-telegram-plane"></i>
							<span class="heading">Easy to customize</span>
							<div class="desc">You have inspired good. But you spat in the faces of Gotham's criminals. Didn't you think there might be casualties? </div>
						</div>
		
					</div>
				</div>
			</div>
		</div>
		 -->
		

		@include('shop/themes/'.Support::theme_path().'partials/banners')

		<!-- <div class="about-us-section">
			<div class="container">
				<div class="row">
					<div class="col-md-6 desc-div">
						<span class="heading">We are digital creative agency</span>
						<div class="desc">
							We have purged your fear. You are ready to Iead these men. You are ready to become a member of the League of Shadows. But first, you must demonstrate your commitment to justice.
						</div>
						<ul class="listing">
							<li><i class="icon fas fa-circle"></i>I seek the means to fight injustice.</li>
							<li><i class="icon fas fa-circle"></i>I've sewn you up, I've set your bones.</li>
							<li><i class="icon fas fa-circle"></i>I've buried enough members in the Wayne family.</li>
							<li><i class="icon fas fa-circle"></i>My anger outweights my guilt.</li>
						</ul>
		
					</div>
		
					<div class="col-md-6">
						<div class="img-div">
							<img class="img-responsive" src="https://s3.envato.com/files/238742699/seoengine-preview/01_preview_joomla.__large_preview.png">
						</div>
					</div>
				</div>
			</div>
		</div>		      
		
		<div class="team-section">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h2 class="action-heading">
							{{ Language::trans('Our Team') }}
						</h2>
					</div>
				</div>
		
				<div class="row">
					<div class="col-md-12">
						<div class="short-desc">
							<p>My name is Merely Ducard but I speak for Ra's al Ghul... a man greatly feared by the criminal underworld. A mon who can offer you a path. Someone like you is only here by choice.You have been exploring the criminal fraternity but whatever your original intentions you have to become truly lost.</p>
						</div>
					</div>
				</div>
		
		
				<div class="team-content">
					<div class="row">
						<div class="col-md-4">
							<div class="team-img-div">
								<img class="img-responsive" src="http://ehonami.blob.core.windows.net/media/2013/08/is-a-mans-office-bad-for-your-health_300.jpg">
							</div>
							<span class="heading">John Doe</span>
							<div class="desc">You have inspired good. But you spat in the faces of Gotham's criminals. Didn't you think there might be casualties? </div>
						</div>
		
						<div class="col-md-4">
							<div class="team-img-div">
								<img class="img-responsive" src="https://image.ibb.co/npaje9/ofice_woman.jpg">
							</div>
							<span class="heading">John Doe</span>
							<div class="desc">You have inspired good. But you spat in the faces of Gotham's criminals. Didn't you think there might be casualties? </div>
						</div>
		
						<div class="col-md-4">
							<div class="team-img-div">
								<img class="img-responsive" src="http://ehonami.blob.core.windows.net/media/2013/08/is-a-mans-office-bad-for-your-health_300.jpg">
							</div>
							<span class="heading">John Doe</span>
							<div class="desc">You have inspired good. But you spat in the faces of Gotham's criminals. Didn't you think there might be casualties? </div>
						</div>
		
					</div>
				</div>
			</div>
		</div>
		
		
		<div class="about-us2-section">
			<div class="container">
				<div class="row">
					<div class="col-md-6 desc-div">
						<span class="heading">We are digital creative agency</span>
						<div class="desc">
							We have purged your fear. You are ready to Iead these men. You are ready to become a member of the League of Shadows. But first, you must demonstrate your commitment to justice.
						</div>
						<ul class="listing">
							<li><i class="icon fas fa-circle"></i>I seek the means to fight injustice.</li>
							<li><i class="icon fas fa-circle"></i>I've sewn you up, I've set your bones.</li>
							<li><i class="icon fas fa-circle"></i>I've buried enough members in the Wayne family.</li>
							<li><i class="icon fas fa-circle"></i>My anger outweights my guilt.</li>
						</ul>
		
					</div>
		
					<div class="col-md-6">
						<div class="img-div">
							<img class="img-responsive" src="https://cdn.colorlib.com/wp/wp-content/uploads/sites/2/seocrawler-minimal-marketing-agency-wordpress-theme.jpg">
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="separe-div">
			<div class="container">
				<div class="col-md-12">
					<h3 class="heading">
						Sabil is agency, for Business & development since 1995 based in Paris.
					</h3>
					
					<p class="desc">
						At Sabil we care about every detail, our goal to make our clients satisfied with their projects.
					</p>
				</div>
			</div>
		</div>
		
		<div class="about-us3-section">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h2 class="action-heading">
							{{ Language::trans('Some text heading') }}
						</h2>
					</div>
				</div>
		
				<div class="row">
					<div class="col-md-12">
						<div class="short-desc">
							<p>Let me get this straight. You think that your client, one of the wealthiest, most powerful men in the world is secretly a vigilante who spends his nights beating criminals to a pulp with his bare hands and your plan is to blackmail this person?Good luck.</p>
						</div>
					</div>
				</div>
				
				<div class="about-us3-content">
					<div class="row">
						<div class="col-md-6">
							<div class="heading">
								<i class="icon fas fa-globe"></i>	
								<h3>Some heading text</h3>
							</div>
							<p class="desc">We have purged your fear. You are ready to Iead these men. You are ready to become a member of the League of Shadows. But first, you must demonstrate your commitment to justice.</p>
						</div>
		
						<div class="col-md-6">
							<div class="heading">
								<i class="icon fas fa-stream"></i>
								<h3>Some heading text</h3>	
							</div>
					
							<p class="desc">What if, before she died, she wrote a letter saying she chose Harvey Dent over you? And what if, to spare you pain, I burnt that letter? But first, you must demonstrate your commitment to justice.</p>
						</div>
					</div>
		
					<div class="row">
						<div class="col-md-6">
							<div class="heading">
								<i class="icon fas fa-shield-alt"></i>
								<h3>Some heading text</h3>
							</div>
							<p class="desc">What if, before she died, she wrote a letter saying she chose Harvey Dent over you? And what if, to spare you pain, I burnt that letter? But first, you must demonstrate your commitment to justice.</p>
						</div>
		
						<div class="col-md-6">
							<div class="heading">
								<i class="icon far fa-lightbulb"></i>
								<h3>Some heading text</h3>
							</div>
							<p class="desc">We have purged your fear. You are ready to Iead these men. You are ready to become a member of the League of Shadows. But first, you must demonstrate your commitment to justice.</p>
						</div>
					</div>
				</div>
			</div>
		</div> -->


		@include('shop/themes/'.Support::theme_path().'partials/newsletter')

		@if(Options::web_options(101) == 1)
			@include('shop/themes/'.Support::theme_path().'partials/section-news')
		@endif

	</div>

	<script type="text/javascript">


		/* Block slick slide */
		$(document).ready(function () { 
			$('.multiple-items-news').slick({
			  	infinite: true,
			  	slidesToShow: 3,
			  	slidesToScroll: 2,
			  	autoplay: true,
					autoplaySpeed: 3000,
					speed: 2000,
			responsive: [
				{
				breakpoint: 1100,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
				  }
				},
				{
				  breakpoint: 800,
				  settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
					autoplaySpeed: 1500,
						speed: 1500
				  }
				},
				{
				breakpoint: 580,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					autoplaySpeed: 1200,
						speed: 800
				  }
				}
			]
			});

		});

		if($('#admin-menu').length > 0) {
			$( document ).ready(function() {
			  $('.JSScrolDown').on('click', function(e) {
			    e.preventDefault();

			    $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top - $('header').height() - $('#admin-menu').height() - 60}, 500, 'linear');  	
			    
			  });
			});
		} else {
			$( document ).ready(function() {
			  $('.JSScrolDown').on('click', function(e) {
			    e.preventDefault();

			    $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top - $('header').outerHeight()}, 500, 'linear');  	
			    
			  });
			});
		}

	</script>


@endsection