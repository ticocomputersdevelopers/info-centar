  

@if(!empty(All::getShortListNews()))

	<div class="section-news">
		<div class="container">

			<div class="row">
				<div class="col-md-12">
					<h2 class="action-heading">
						<a href="{{ Options::base_url() }}{{ Url_mod::convert_url('blog') }}">
						{{ Language::trans('Blog') }}
						</a>
					</h2>
				</div>
			</div>

			<div class="multiple-items-news">
				@foreach(All::getShortListNews() as $row)
					<div class="single-news">
						<a class="image-div" href="{{ Options::base_url() }}{{ Url_mod::convert_url('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}" style="background-image: url('{{ Options::domain() }}{{ $row->slika }}');">

							<!-- @if(isset($row->slika))
								<img src="{{ Options::domain() }}{{ $row->slika }}" alt="{{ $row->naslov }}" />
							@else
							
							<img src="{{ Options::domain() }}images/no-image.jpg" alt="{{ $row->naslov }}" class="img-responsive" />
							
							@endif -->
						</a>
				
						<h3 class="news-title">
							<a href="{{ Options::base_url() }}{{ Url_mod::convert_url('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}">
								{{ $row->naslov }}
							</a>
						</h3>
						
						<span class="create-news-date">
							{{ Support::date_convert($row->datum) }}
						</span>

						<!-- {{ All::shortNewDesc($row->tekst) }} Opis vesti  -->
					
						<!-- <a class="" href="{{ Options::base_url() }}{{ Url_mod::convert_url('blog') }}/{{ $row->web_vest_b2c_id }}">{{ Language::trans('Pročitaj članak') }}</a>  Procitaj vise - vesti -->
							
					</div>
				@endforeach
			</div>
		
		</div>
	</div>

@endif