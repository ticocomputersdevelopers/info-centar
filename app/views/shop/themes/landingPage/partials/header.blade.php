

<header id="fixed_header"> 
 
    <div class="container">
        <div class="row">
          <div class="col-md-3 col-sm-3 col-xs-6">
            <div>
               <a class="logo" href="/" title="{{Options::company_name()}}">
                    <img src="{{ Options::domain() }}{{Options::company_logo()}}" alt="{{Options::company_name()}}" /> 
                </a>
            </div>
          </div>

          <div class="col-md-9 col-sm-9 col-xs-6 text-right">
            <ul class="site-navigation">

          
              <span class="exit">
                  <i class="fas fa-times"></i>
              </span>
 
              @foreach(All::menu_top_pages() as $row)
                  @if($row->grupa_pr_id != -1 and $row->grupa_pr_id != 0)
                  <li>
                      <a href="{{ Options::base_url()}}{{ Url_mod::url_convert(Groups::getGrupa($row->grupa_pr_id)) }}/0/0/0-0">{{ Language::trans($row->title) }}</a>
                  </li>
                  @elseif($row->tip_artikla_id == -1)
                      <li>
                          <a href="{{ Options::base_url().Url_mod::url_convert($row->naziv_stranice) }}">{{ Language::trans($row->title) }}</a>
                      </li>
                  @else
                    @if($row->tip_artikla_id==0)
                        <li>
                            <a href="{{ Options::base_url().Url_mod::convert_url('akcija') }}">{{ Language::trans($row->title) }}</a>
                        </li>
                    @else
                        <li>
                            <a href="{{ Options::base_url().Url_mod::convert_url('tip').'/'.Url_mod::url_convert(Support::tip_naziv($row->tip_artikla_id)) }}">
                            {{ Language::trans($row->title) }}</a> 
                        </li>
                    @endif
                  @endif
              @endforeach

 
            </ul>
            

            <span id="navigationToggle">
              <i class="fas fa-bars"></i>
            </span>
          </div>
        </div>

        <div class="body-overlay">
          
        </div>
    </div>
   
 </header>
 




