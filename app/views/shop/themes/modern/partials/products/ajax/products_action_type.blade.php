@foreach($articles as $row)
<div class="JSproduct">
	<div class="product-content product-tip clearfix">
		@if(All::provera_akcija($row->roba_id))
 			<div class="product-sale shake">- {{ Product::getSale($row->roba_id) }} din</div>
 		@endif
		<a href="{{Options::base_url()}}artikal/{{Url_mod::url_convert(Product::seo_title($row->roba_id))}}" class="product-image-wrapper">
			<img class="product-image" src="{{ Options::domain() }}{{ Product::web_slika($row->roba_id) }}" alt="{{ Product::seo_title($row->roba_id) }}" />
		</a>
		<a class="product-title" href="{{Options::base_url()}}artikal/{{Url_mod::url_convert(Product::seo_title($row->roba_id))}}">
			{{ Product::short_title($row->roba_id) }}
		</a>

		<section class="action-holder clearfix">
			<div class="price-holder">
	            <span class="product-price">{{ Cart::cena(Product::get_price($row->roba_id)) }}</span>
	            @if(All::provera_akcija($row->roba_id))
	            <span class="old-price">{{ Cart::cena(Product::old_price($row->roba_id)) }}</span>
	            @endif
	        </div>  
        </section> 
 	
 	@if(Cart::kupac_id() > 0)
        <section class="product-akcija-div-centered"> 
		@if(Product::getStatusArticle($row->roba_id) == 1)
			@if(Cart::check_avaliable($row->roba_id) > 0)
			    <!-- <a class="product-title-details" href="{{Options::base_url()}}artikal/{{Url_mod::url_convert(Product::seo_title($row->roba_id))}}">Detaljnije</a> -->
				<button data-roba_id="{{$row->roba_id}}" class="dodavnje JSadd-to-cart">Dodaj u korpu</button>
				<button data-roba_id="{{$row->roba_id}}" title="Lista zelja" class="dodavnje JSadd-to-wish wish-btn"><i class="fa fa-heart-o"></i></button>
			@else 
				<button class="dodavnje not-available">Nije dostupno</button>	
                <button data-roba_id="{{$row->roba_id}}" title="Lista zelja" class="dodavnje JSadd-to-wish wish-btn"><i class="fa fa-heart-o"></i></button>
			@endif
		@else
			<button class="dodavanje not-available">{{ Product::find_flag_cene(Product::getStatusArticle($row->roba_id),'naziv') }}</button>
		@endif
 		</section>
 	@else	
        <section class="product-akcija-div-centered"> 
		@if(Product::getStatusArticle($row->roba_id) == 1)
			@if(Cart::check_avaliable($row->roba_id) > 0)
			    <!-- <a class="product-title-details" href="{{Options::base_url()}}{{Url_mod::url_convert(Product::seo_title($row->roba_id))}}">Detaljnije</a> -->
				<button data-roba_id="{{$row->roba_id}}" class="dodavnje JSadd-to-cart">Dodaj u korpu</button>
				<button data-roba_id="{{$row->roba_id}}" title="Doadavanje artikla na listu želja moguće je samo registrovanim korisnicima" class="dodavnje JSadd-to-wish wish-btn"><i class="fa fa-heart-o" disabled></i></button>
			@else 
				<button class="dodavnje not-available">Nije dostupno</button>	
                <button data-roba_id="{{$row->roba_id}}" title="Doadavanje artikla na listu želja moguće je samo registrovanim korisnicima" class="dodavnje JSadd-to-wish wish-btn" disabled><i class="fa fa-heart-o"></i></button>
			@endif
		@else
			<button class="dodavanje not-available">{{ Product::find_flag_cene(Product::getStatusArticle($row->roba_id),'naziv') }}</button>
		@endif
 		</section>
	   @if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array(200)))
			<a class="article-edit-btn" target="_blank" href="{{ Options::base_url() }}admin/product/{{ $row->roba_id }}">IZMENI ARTIKAL</a>
	   @endif
	@endif
	</div>
</div>
@endforeach