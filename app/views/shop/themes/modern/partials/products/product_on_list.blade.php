<section class="JSproduct list-product-on-480"> 
	<div class="product-content clearfix">
    <div class="row flex-on-list"> 
	<section class="image-holder no-padding-at-640 medium-3 small-4 columns">
		<a href="{{Options::base_url()}}artikal/{{Url_mod::url_convert(Product::seo_title($row->roba_id))}}" class="product-image-wrapper product-image-wrapper-list-view clearfix">
		    <img class="product-image" src="{{ Options::domain() }}{{ Product::web_slika($row->roba_id) }}" alt="{{ Product::seo_title($row->roba_id) }}" />
		</a>
	</section>

	<section class="description-holder medium-9 small-8 columns">
		<a class="product-title" href="{{Options::base_url()}}artikal/{{Url_mod::url_convert(Product::seo_title($row->roba_id))}}">
			{{ Product::short_title($row->roba_id) }}
		</a>

		 <!-- SIFRA ARTIKLA -->
        <!--  <div class="article-password hide-on-mobile">Šifra artikla: {{ Product::get_sifra($row->roba_id) }}</div> -->
	</section>
	
<!-- OPIS KARAKTERISTIKE -->
	<!-- <section class="medium-5 columns text-align-left">
		{{ Product::get_karakteristike_short_grupe($row->roba_id) }}
	</section>  -->

  <section class="price-holder price-holder-list-480 text-right medium-6 right small-8 columns">
		<span class="product-price product-price-list-480">{{ Cart::cena(Product::get_price($row->roba_id)) }}</span> 
		<br>
		@if(All::provera_akcija($row->roba_id))
		<span class="old-price product-price-list-480">{{ Cart::cena(Product::old_price($row->roba_id)) }}</span>
		@endif
  </section>

	<section class="action-holder medium-4 small-12 columns right clearfix">
		 
		@if(Product::getStatusArticle($row->roba_id) == 1)
			@if(Cart::check_avaliable($row->roba_id) > 0)
			<div class="btn-container btn-container-on-list">

				@if(Cart::kupac_id() > 0)
					<!-- <a class="product-title-details" href="{{Options::base_url()}}{{Url_mod::url_convert(Product::seo_title($row->roba_id))}}">Detaljnije</a> -->
					<button data-roba_id="{{$row->roba_id}}" title="Dodaj u korpu" class="dodavnje JSadd-to-cart">Dodaj u korpu</button>
					<button data-roba_id="{{$row->roba_id}}" title="Lista zelja" class="dodavnje JSadd-to-wish wish-btn"><i class="fa fa-heart-o"></i></button>
					@else
					<button data-roba_id="{{$row->roba_id}}" title="Dodaj u korpu" class="dodavnje JSadd-to-cart">Dodaj u korpu</button>
					<button data-roba_id="{{$row->roba_id}}" title="Dodavanje artikla na listu želja moguće je samo registrovanim korisnicima" class="dodavnje JSadd-to-wish wish-btn" disabled><i class="fa fa-heart-o"></i></button>
					@endif
				@if(Options::compare()==1)
				<button class="JScompare {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}" title="Uporedi">
					<i class="fa fa-exchange" aria-hidden="true" title="Uporedi"></i>
				</button>
				@endif
			</div>
			@else 
			<div class="btn-container btn-container-on-list">
	            	@if(Cart::kupac_id() > 0)
	                <button class="dodavnje not-available" title="Nije dostupno">Nije dostupno</button>
	                <button data-roba_id="{{$row->roba_id}}" title="Lista zelja" class="dodavnje JSadd-to-wish wish-btn"><i class="fa fa-heart-o"></i></button>
	                @if(Options::compare()==1 )
	                <button class="JScompare {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}" title="Uporedi">
	                	<i class="fa fa-exchange" aria-hidden="true" title="Uporedi"></i>
	                </button>
	                @endif	
	                @else
	                <button class="dodavnje not-available" title="Nije dostupno">Nije dostupno</button>
	                <button data-roba_id="{{$row->roba_id}}" title="Doadavanje artikla na listu želja moguće je samo registrovanim korisnicima" class="dodavnje JSadd-to-wish wish-btn" disabled><i class="fa fa-heart-o"></i></button>
	                @if(Options::compare()==1 )
	                <button class="JScompare {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}" title="Uporedi">
	                	<i class="fa fa-exchange" aria-hidden="true" title="Uporedi"></i>
	                </button>
	                @endif
	                @endif
			 </div> <!-- end of btn-container -->
			@endif

		 @else
		<div class="btn-container ">
			<button class="dodavanje not-available" title="Nije dostupno">{{ Product::find_flag_cene(Product::getStatusArticle($row->roba_id),'naziv') }}</button>
			@if(Options::compare()==1)
			<button class="JScompare {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}" title="Uporedi">
				<i class="fa fa-exchange" aria-hidden="true" title="Uporedi"></i>
			</button>
			@endif
		</div>		
		@endif
		</section>
	 	@if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array(200)))
			<a class="article-edit-btn" href="{{ Options::base_url() }}admin/product/{{ $row->roba_id }}">IZMENI ARTIKAL</a>
		@endif
   </div>
  </div>
</section>
