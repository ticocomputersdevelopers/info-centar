 <footer>
	<div class="row footer-background">
	  <div class="main-wrapper clearfix">
		<div class="medium-3 columns"> 
			<!-- SOCIAL ICONS -->
			 <div class="row">
				<div class="footer-soc-icons"> 
				  	<div class="social-icons hide-for-large-up">
						{{Options::social_icon()}}
					</div>
				</div>
			</div>
			<div class="row">
				<nav class="medium-12 columns">
					<ul class="footer-links">
						@foreach(All::footer_pages() as $row)
                        @if($row->tip_artikla_id == -1)
                        <li> 
                            <a href="{{ Options::base_url().$row->naziv_stranice }}">
                                {{ ($row->title) }}
                            </a>
                         </li>
                        @else
                            @if($row->tip_artikla_id==0)
                            <li> 
                                <a href="{{ Options::base_url().Url_mod::convert_url('akcija') }}">
                                    {{ ($row->title) }}
                                </a>
                            </li>
                            @else
                            <li> 
                                <a href="{{ Options::base_url().Url_mod::convert_url('tip').'/'.Url_mod::url_convert(Support::tip_naziv($row->tip_artikla_id)) }}">
                                    {{ ($row->title) }}
                                </a>
                            </li>
                            @endif
                        @endif
                    @endforeach
					</ul>
				</nav>
			</div>&nbsp;
		</div>  

	<!-- STRANICE --> 
		<div class="medium-9 columns"> 
			 <div class="medium-6 medium-offset-1 columns text-center newsletter-checkin">
				@if(Options::newsletter()==1)
					<h4>Prijava za newsletter</h4>
					<div class="newsletter row">
						<input type="text" placeholder="E-mail" id="newsletter" />
						<button onclick="newsletter()">Prijavi se</button>
					</div>
				@endif
		  	</div>
	 
			<div class="medium-3 right columns">
				<h5 class="footer-box-pages-title">Kontakt</h5>
				<ul class="footer-box-pages">
					<li class="footer-shop-description"> 
						 {{ Options::company_fax() }}<br>
						 {{ Options::company_phone() }}<br>						 
						<a class="mailto" href="mailto:{{ Options::company_email() }}">{{ Options::company_email() }}</a></li>
					</li>
			    </ul>
			</div>
		</div> 
	  </div>
		 
	<!-- BANKE -->
	 <div class="main-wrapper clearfix">
		<div class="row footer-banks">
			<ul class="medium-7 columns footer-banks-ul">
				<li class="medium-1 columns banks-footer">
					<a href="http://www.maestrocard.com/gateway/index.html" target="_blank">
						<img src="{{ Options::domain() }}images/cards/brand3.png" alt="MaestroCard">
					</a>
				</li>

				<!-- <li class="medium-1 columns banks-footer">
					<a href="http://www.mastercardbusiness.com" target="_blank">
						<img src="{{ Options::domain() }}images/cards/footerMasterCard.png" alt="MasterCard">
					</a>
				</li> -->

				<!-- <li class="medium-1 columns banks-footer">
					<a href="https://rs.visa.com/" target="_blank">
						<img src="{{ Options::domain() }}images/cards/footerVisa.png" alt="Visa">
					</a>
				</li> -->
		
	  			<li class="medium-2 columns end banks-footer">
					<a href="https://www.raiffeisenbank.rs/" target="_blank">
						<img src="{{ Options::domain() }}images/cards/footerRaiffeisen_Bank.png" alt="RaiffeisenBank">
					</a>
				</li>
			</ul>
		</div>
	<!-- INFO -->
		<div class="row"> 
			<div class="medium-12 columns footnote">
				<p class="info">Cene su informativnog karaktera. Prodavac ne odgovara za tačnost cena iskazanih na sajtu, zadržava pravo izmena cena. Ponudu za ostale artikle, informacije o stanju lagera i aktuelnim cenama možete dobiti na upit. Plaćanje se isključivo vrši virmanski - bezgotovinski.
				</p>
				Izrada internet prodavnice - <a href="https://www.selltico.com">Selltico</a> &copy; {{ date('Y') }}. Sva prava zadržana.
			</div>
		</div>
	 </div>
   </div>
</footer>
    
	
	 
	
 