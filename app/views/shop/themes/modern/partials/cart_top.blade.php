<section class="medium-2 small-3 columns text-right small-only-text-center">
	<div class="header-cart">
		<a class="header-cart-icon" href="{{ Options::base_url() }}{{ Seo::get_korpa() }}">
			<span class="JSbroj_cart"> {{ Cart::broj_cart() }} </span> 
			<input type="hidden" id="h_br_c" value="{{ Cart::broj_cart() }}" />
		</a>

		<ul class="JSheader-cart-content">
			@include('shop/themes/'.Support::theme_path().'partials/mini_cart_list') 
		</ul>
	</div>
</section> 