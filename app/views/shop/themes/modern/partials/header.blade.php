<header class="row">
    <div class="main-wrapper clearfix div-in-header">
        <!-- LOGO AREA -->
        <section class="logo-area medium-3 columns">
           <h1 class="for-seo"> 
              <a class="logo" href="/" title="{{Options::company_name()}}">
                  <img src="{{ Options::domain() }}{{Options::company_logo()}}" alt="{{Options::company_name()}}" />
              </a>
           </h1>
         </section>
         
  <!-- SVE KATEGORIJE -->
        <section class="medium-9 columns navbar-flex">
          <div class="all-category-button medium-2 small-6 columns">  
             {{ Groups::firstGropusSelect('2') }} 
          </div> 

          <!-- SEARCH AREA -->
          <div class="JSsearchContent2 medium-7 columns">
            <input class="search-field" autocomplete="off" data-timer="" type="text" id="JSsearch2" placeholder="Pretraga" />
            <button onclick="search2()" class="JSsearch-button2"> <i class="fa fa-search"></i> </button>
          </div>

          <!-- CART AREA -->
          @include('shop/themes/'.Support::theme_path().'partials/cart_top')
        </section>
         <div class="JSmenu-icon"><i class="fa fa-bars" aria-hidden="true"></i></div>
    </div>
   
    <div class="menu-background row" id="JSfixedNavbar">
        <div class="main-wrapper clearfix"> 
            <!-- MAIN MENU -->
            <!-- SIDEBAR LEFT -->
            <aside id="sidebar-left" class="medium-6 large-3 columns">
                @if(Options::category_view()==1)
                @include('shop/themes/'.Support::theme_path().'partials/categories/category')
                @endif               
            </aside>
          
            <nav class="large-9 columns JSmain-nav" id="navbar">
                <ul id="main-menu" class="clearfix">

                  @foreach(All::header_pages('flag_page') as $row)
                        @if($row->tip_artikla_id == -1)
                        <li> 
                            <a href="{{ Options::base_url().$row->naziv_stranice }}">
                                {{ ($row->title) }}
                            </a>
                         </li>
                        @else
                            @if($row->tip_artikla_id==0)
                            <li> 
                                <a href="{{ Options::base_url().Url_mod::convert_url('akcija') }}">
                                    {{ ($row->title) }}
                                </a>
                            </li>
                            @else
                            <li> 
                                <a href="{{ Options::base_url().Url_mod::convert_url('tip').'/'.Url_mod::url_convert(Support::tip_naziv($row->tip_artikla_id)) }}">
                                    {{ ($row->title) }}
                                </a>
                            </li>
                            @endif
                        @endif
                    @endforeach
                  @if(Options::web_options(121)==1)
                      <?php $konfiguratori = All::getKonfiguratos(); ?>
                      @if(count($konfiguratori) > 0)
                          @if(count($konfiguratori) > 1)
                              <li class="dropdown">
                                  <a href="#" class="dropbtn">Konfiguratori</a>
                                  <ul class="dropdown-content">
                                      @foreach($konfiguratori as $row)
                                      <li><a href="{{ Options::base_url() }}konfigurator/{{ $row->konfigurator_id }}">{{ $row->naziv }}</a></li>
                                      @endforeach
                                  </ul>
                              </li>
                          @else
                              <li><a href="{{ Options::base_url() }}konfigurator/{{ $konfiguratori[0]->konfigurator_id }}">Konfigurator</a></li>
                          @endif
                      @endif
                  @endif
              </ul>                        
           </nav>
         </div>
        
    </div>
</header>

 <!-- Fixed NAVBAR -->
<div id="JSreplace" class="row">
   <div class="main-wrapper clearfix">
    <h1 class="for-seo"> 
      <a href="{{ Options::base_url()}}" title="{{Options::company_name()}}"> 
        <div class="medium-3 columns fixedNavbar-logo">
            <img src="{{ Options::domain() }}{{Options::company_logo()}}" alt="{{Options::company_name()}}" />
        </div>
      </a>
    </h1>
    <div class="medium-9 columns navbar-flex">
      <div class="all-category-button medium-2 columns">
        {{ Groups::firstGropusSelect() }}
      </div>
       <!-- SEARCH AREA -->
       <div class="JSsearchContent medium-5 columns">
            <input class="search-field" autocomplete="off" data-timer="" type="text" id="JSsearch" placeholder="Pretraga" />
            <button onclick="search()" class="JSsearch-button"> <i class="fa fa-search"></i> </button>
       </div>

      <div class="medium-3 columns small-9 text-center padding-zero"> 
        @if(Session::has('b2c_kupac'))
        <div class="logout-wrapper clearfix">
          <a id="logged-user" href="{{Options::base_url()}}korisnik/{{Url_mod::url_convert(WebKupac::get_user_name())}}">
            <span>{{ WebKupac::get_user_name() }}</span>
          </a>
          <a id="logged-user" href="{{Options::base_url()}}korisnik/{{Url_mod::url_convert(WebKupac::get_company_name())}}">
            <span>{{ WebKupac::get_company_name() }}</span>
          </a>
          <a id="logout-button" href="{{Options::base_url()}}logout"><span>Odjavi se</span></a>
        </div>
        @else 

          <i class="fa fa-sign-in"></i>
          <a href="#" id="login-icon" data-reveal-id="myModal-login"><span>Uloguj se</span></a><i class="pipes">/</i>
          <a id="registration-icon" href="{{Options::base_url()}}registracija"> 
            <i class="fa fa-user-plus"></i>
            <span>Registracija</span>
          </a>
        @endif
      </div>

    <section class="medium-2 small-3 columns padding-zero">
      <div class="header-cart">
        <a class="header-cart-icon" href="{{ Options::base_url() }}{{ Seo::get_korpa() }}">
          <span class="JSbroj_cart"> {{ Cart::broj_cart() }} </span> 
          <input type="hidden" id="h_br_c" value="{{ Cart::broj_cart() }}" />
        </a>

        <ul class="JSheader-cart-content">
          @include('shop/themes/'.Support::theme_path().'partials/mini_cart_list') 
        </ul>
      </div>
    </section>
   </div>  
 </div>
</div>