@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
<div class="row reg-page-padding">
  <div class="medium-5 columns fiz-lice-container">
	 <h3 class="registracija-fizickog-lica">Registracija za pravna lica</h3>
    <div> 
		 <span class="fiz-lice-page-span"><i class="fa fa-angle-double-right"></i>&nbsp; Brza kupovina.</span><br>
		 <span class="fiz-lice-page-span"><i class="fa fa-angle-double-right"></i>&nbsp; Mogućnost skupljanja poena i ostvarivanja povremenih popusta. </span><br>
		 <span class="fiz-lice-page-span"><i class="fa fa-angle-double-right"></i>&nbsp; Ostvarivanje partnerskih popusta i mogućnost dobijanja dilerskih cena.</span>
	</div>
</div>	
<div class="medium-5 columns"> 
<form action="{{ Options::base_url()}}registracija-post" method="post" class="registration-form" autocomplete="off">
	<input type="hidden" name="flag_vrsta_kupca" value="1">
	<div class="field-group login-input-row">
		<div class="medium-4 columns text-right small-only-text-left"> 
			<label for="naziv">Naziv firme</label>
		</div>
		<div class="medium-8 columns">
			<input class="login-form__input" name="naziv" type="text" value="{{ htmlentities(Input::old('naziv') ? Input::old('naziv') : '') }}" >
			<div class="error">{{ $errors->first('naziv') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('naziv') : "" }}
			</div>
		</div> 
	</div>

	<div class="field-group login-input-row">
		<div class="medium-4 columns text-right small-only-text-left"> 
			<label for="pib">PIB</label>
		</div>
		<div class="medium-8 columns">
			<input class="login-form__input" name="pib" type="text" value="{{ Input::old('pib') ? Input::old('pib') : '' }}" >
		<div class="error">{{ $errors->first('pib') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('pib') : "" }}</div>
		</div>
	</div>	

	<div class="field-group row login-input-row">
		<div class="medium-4 columns text-right small-only-text-left"> 
			<label for="email">E-mail</label>
		</div>
		<div class="medium-8 columns">
			<input class="login-form__input" autocomplete="false" name="email" type="text" value="{{ Input::old('email') ? Input::old('email') : '' }}" >
			<div class="error">{{ $errors->first('email') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('email') : "" }}
			</div>
		</div>
	</div>	

	<div class="field-group row login-input-row">
		<div class="medium-4 columns text-right small-only-text-left"> 
			<label for="lozinka">Lozinka</label>
		</div>
		<div class="medium-8 columns">
			<input class="login-form__input" name="lozinka" type="password" value="{{ htmlentities(Input::old('lozinka') ? Input::old('lozinka') : '') }}">
			<div class="error">{{ $errors->first('lozinka') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('lozinka') : "" }}</div>
		</div>
	</div>

	<div class="field-group row login-input-row">
		<div class="medium-4 columns text-right small-only-text-left"> 
			<label for="telefon">Telefon</label>
		</div>
		<div class="medium-8 columns">
			<input class="login-form__input" name="telefon" type="text" value="{{ Input::old('telefon') ? Input::old('telefon') : '' }}" >
			<div class="error">{{ $errors->first('telefon') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('telefon') : "" }}</div>
		</div>
	</div>

	<div class="field-group row login-input-row">
		<div class="medium-4 columns text-right small-only-text-left"> 
			<label for="adresa">Adresa</label>
		</div>
		<div class="medium-8 columns">
			<input class="login-form__input" name="adresa" type="text" value="{{ htmlentities(Input::old('adresa') ? Input::old('adresa') : '') }}" >
			<div class="error">{{ $errors->first('adresa') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('adresa') : "" }}</div>
		</div>
	</div>

	<div class="field-group row login-input-row">
		<div class="medium-4 columns text-right small-only-text-left"> 
			<label for="mesto_id">Mesto/Grad</label>
		</div>
		<div class="medium-8 columns">
			<input class="login-form__input" name="mesto" type="text" value="{{ htmlentities(Input::old('mesto') ? Input::old('mesto') : '') }}" >
			<div class="error">{{ $errors->first('mesto') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('mesto') : "" }}</div>
		</div>
	</div>
	<div class="row">
		<div class="medium-12 columns text-right small-only-text-left">   
			<button type="submit" class="login-form-button admin-login">Registruj se</button>
		</div>
	</div>
 </form>

	@if(Session::get('message'))
		<div>Potvrda registracije je poslata na vašu e-mail adresu!</div>
	@endif
</div>
</div>

@endsection