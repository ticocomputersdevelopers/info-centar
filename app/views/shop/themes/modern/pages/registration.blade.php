@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
<div class="row login-page-padding">
	<div class="medium-12 columns">
		<div class="medium-6 columns"> 
			<div class="medium-6 columns"> 
				<img src="{{ Options::domain() }}images/legal-person.jpg" alt="login_and_registration_image">
			</div>

			<div class="medium-6 columns"> 
				<h3 class="legal-person-h3">Fizička lica</h3>
				<p class="legal-person-paragraph">Registracija za fizička lica. Registrovani korisnici imaju različite mogućnosti i pogodnosti.</p>
				<a class="legal-person-link-button" href="{{ Options::base_url()}}fizicko_lice">Registruj se</a>
			</div>
		</div>

		<div class="medium-6 columns">
			<div class="medium-6 columns"> 
				<img src="{{ Options::domain() }}images/tuxedo.jpg" alt="login_and_registration_image">
			</div>

	 		<div class="medium-6 columns"> 
	 			<h3 class="legal-person-h3">Pravna lica</h3>
	 			<p class="legal-person-paragraph">Registracija za kompanije i pravna lica. Ukoliko ste registrovani kao pravno lice maksimalno vam je olakšano naručivanje proizvoda.</p>
	 		 	<a class="legal-person-link-button" href="{{ Options::base_url()}}pravno_lice">Registruj se</a>
	 		</div>
		 </div>
	</div> 
</div>
@endsection 