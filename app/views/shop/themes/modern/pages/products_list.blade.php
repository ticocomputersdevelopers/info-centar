@extends('shop/themes/'.Support::theme_path().'templates/products')

@section('products_list')

<section class="JSproduct-list-options clearfix row">
			
    <section class="medium-6 small-12 columns">
 
<!-- PER PAGE -->
		<span class="product-number"><i class="vidiKao">UKUPNO:</i> {{ $count_products }}</span>
        @if(Options::product_number()==1)
		<section class="per-page JSselect-field">	
			<span class="per-page-active">
				@if(Session::has('limit'))
				{{Session::get('limit')}}
				@else
				20
				@endif
			</span>
			
			<ul>			
				<li><a href="{{ Options::base_url() }}limit/20">20</a></li>
				<li><a href="{{ Options::base_url() }}limit/30">30</a></li>
				<li><a href="{{ Options::base_url() }}limit/50">50</a></li>			
			</ul>			 
		</section>
		@endif

 		@if(Options::compare()==1)
			<div id="JScompareArticles" class="show-compered" data-reveal-id="compare-article"><span>Upoređeni artikli</span></div>
		@endif
	
	</section>

	<section class="medium-6 small-12 columns flexed-div">
        <div class="currencyANDsort clearfix">
        @if(Options::product_currency()==1)
            <section class="currency JSselect-field">
                <span class="currency-active">{{Articles::get_valuta()}}</span>
                <ul>
                    <li><a href="{{ Options::base_url() }}valuta/1">Din</a></li>
                    <li><a href="{{ Options::base_url() }}valuta/2">Eur</a></li>
                </ul>
            </section>
            @endif
            @if(Options::product_sort()==1)
            <section class="sort-products JSselect-field"> 
                <span class="sort-active">{{Articles::get_sort()}}</span>
                <ul>
                    <li><a href="{{ Options::base_url() }}sortiranje/price_asc">Cena: min</a></li>
                    <li><a href="{{ Options::base_url() }}sortiranje/price_desc">Cena: max</a></li>
                    <li><a href="{{ Options::base_url() }}sortiranje/news">Najnovije</a></li>
                    <li><a href="{{ Options::base_url() }}sortiranje/name">Prema nazivu</a></li>
                </ul>
            </section>
            @endif
	          
	        <!-- GRID LIST VIEW -->
    	@if(Options::product_view()==1)
		@if(Session::has('list'))
		<section class="view-buttons"> 
			<a href="{{ Options::base_url() }}prikaz/list"><span class="list-view-button"><i class="fa fa-bars grid-list-icons"></i></span></a>
			<a href="{{ Options::base_url() }}prikaz/grid"><span class="grid-view-button active"><i class="fa fa-bars list-view-icon grid-list-icons"></i></span></a>  		 
		 </section>

		@else
		<section class="view-buttons">  
			<a href="{{ Options::base_url() }}prikaz/list"><span class="list-view-button active"><i class="fa fa-bars grid-list-icons"></i></span></a>
			<a href="{{ Options::base_url() }}prikaz/grid"><span class="grid-view-button"><i class="fa fa-bars list-view-icon grid-list-icons"></i></span></a>
		 </section>

		@endif
        @endif  
         </div>       
	</section>
</section>

<section class="row pagination-products-list">
	{{ Paginator::make($articles, $count_products, $limit)->links() }}
</section>

<section class="compare-section">
	<div id="compare-article" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
		<div id="JScompareTable" class="table-scroll"></div>
		<a class="close-reveal-modal close" aria-label="Close"><i class="fa fa-close"></i></a>
	</div>
</section>

<!-- PRODUCTS -->
@if(Session::has('list') or Options::product_view()==3)
<!-- LIST PRODUCTS -->
<section class="JSproduct-list list-view row">
	@foreach($articles as $row)
		@include('shop/themes/'.Support::theme_path().'partials/products/product_on_list') 
	@endforeach
</section>

@else
<!-- Grid proucts -->
<section class="JSproduct-list row">
	@foreach($articles as $row)
		@include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
	@endforeach
</section>		
@endif

@if($count_products == 0)
<br>
<span class="no-articles"> Trenutno nema artikla za date kategorije</span>
@endif

<section class="row pagination-products-list"> 
	{{ Paginator::make($articles, $count_products, $limit)->links() }}
</section>

@endsection