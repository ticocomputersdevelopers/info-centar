@foreach($articles as $row)
<div class="JSproduct col-md-4 col-sm-4 col-xs-12">
	<div class="shop-product-card relative">
		<!-- SALE PRICE -->
		@if(All::provera_akcija($row->roba_id))
		<div class="label label-red sale-label"> 
			 - {{ Product::getSale($row->roba_id) }} din
		</div>
		@endif
		<div class="product-image-wrapper relative">
			<!-- PRODUCT IMAGE -->
			<a href="{{Options::base_url()}}{{Url_mod::convert_url('artikal')}}/{{Url_mod::url_convert(Product::seo_title($row->roba_id))}}">
				<img class="product-image img-responsive" src="{{ Options::domain() }}{{ Product::web_slika($row->roba_id) }}" alt="{{ Product::seo_title($row->roba_id) }}" />
			</a>
			<!-- ADD TO CART BUTTON -->
			<div class="add-to-cart-container"> 		
				@if(Product::getStatusArticle($row->roba_id) == 1)
					@if(Cart::check_avaliable($row->roba_id) > 0)
					<div data-roba_id="{{$row->roba_id}}" class="dodavnje JSadd-to-cart buy-btn">
					    {{ Language::trans('Dodaj u korpu') }}
					</div>
					@else 
						<div class="dodavnje not-available buy-btn">{{ Language::trans('Nije dostupno') }}</div>	
					@endif  <!-- NOT AVAILABLE -->
				@else
					<div class="dodavanje not-available buy-btn">{{ Product::find_flag_cene(Product::getStatusArticle($row->roba_id),'naziv') }}</div>
				@endif
			 
				@if(Cart::kupac_id() > 0) 
					<button data-roba_id="{{$row->roba_id}}" class="JSadd-to-wish wish-list">
						<i title="Dodaj na listu želja" class="fa fa-heart-o"></i>
					</button>
				@else
					<button data-roba_id="{{$row->roba_id}}" class="wish-list JSnot_logged">
					 	<i title="Dodavanje artikla na listu želja moguće je samo registrovanim korisnicima" class="fa fa-heart-o"></i>
					</button>
				@endif	 
			 	<button class="quick-view JSQuickViewButton" data-roba_id="{{$row->roba_id}}">
					<i title="Brzi pregled" class="glyphicon glyphicon-fullscreen"></i>
				</button> 
			</div>
		</div>
		<div class="product-meta text-center">
			<span class="review">
				{{ Product::getRating($row->roba_id) }}
			</span>
			<!-- PRODUCT TITLE -->
			<a class="product-name" href="{{Options::base_url()}}{{Url_mod::convert_url('artikal')}}/{{Url_mod::url_convert(Product::seo_title($row->roba_id))}}">
				<h2> {{ Product::short_title($row->roba_id) }} </h2>
			</a>
			<!-- PRODUCT PRICE -->
			<div class="price-holder row">
				<span class="product-price">{{ Cart::cena(Product::get_price($row->roba_id)) }}</span>
				@if(All::provera_akcija($row->roba_id))
					<span class="product-old-price">{{ Cart::cena(Product::old_price($row->roba_id)) }}</span>
				@endif	   
			</div> 
		</div> 
		<!-- ADMIN BUTTON -->
		@if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array(200)))
			<a class="article-edit-btn JSFAProductModalCall" data-roba_id="{{$row->roba_id}}" href="javascript:void(0)">{{ Language::trans('IZMENI ARTIKAL') }}</a>
		@endif
	</div>
</div>
@endforeach