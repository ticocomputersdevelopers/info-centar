@if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array(200)))
    <div id="admin-menu"> 
        <div class="container text-right">
            <a href="#!" data-toggle="modal" data-target="#FAProductsModal"><i class="fas fa-clipboard-list"></i> {{ Language::trans('Artikli') }}</a>
            |
            <a href="#!" id="JSShortAdminSave"><i class="fas fa-save"></i> {{ Language::trans('Sačuvaj izmene') }}</a>
            |
            <span class="ms admin-links"><a target="_blank" href="{{ Options::base_url() }}admin"><i class="fas fa-cogs"></i>  {{ Language::trans('Admin Panel') }}</a></span>|
            <span class="ms admin-links"><a href="{{ Options::domain() }}admin-logout">{{ Language::trans('Odjavi se') }}</a></span>
        </div>
    </div>
    @include('shop/front_admin/modals/products')
    @include('shop/front_admin/modals/product')
@endif
  
<div id="preheader" class="">
   <!--  <div class="social-icons">  
        {{Options::social_icon()}} 
    </div> -->
 
    <!-- LOGIN & REGISTRATION ICONS --> 
    <div class="container">
 
        @if(Options::user_registration()==1)
        <div class="navbar preheader-icons row clearfix">
            <div class="pull-right col-md-1 col-sm-1 col-xs-2 no-padding text-center"> 
                 @if(Options::checkB2B())
                    <a id="b2b-login-icon" href="{{Options::domain()}}b2b/login" class="confirm inline-block">B2B</a> 
                 @endif 
            </div>  
          <!--   @if(Options::stranice_count() > 0)
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#top-navigation">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>                        
                </button>
            @endif
            <div class="col-md-11 col-sm-11 col-xs-12 no-padding"> 
                <div class="collapse navbar-collapse" id="top-navigation">      
                    <ul class="nav navbar-nav top-menu-links">
                        @foreach(All::menu_top_pages() as $row)
                            @if($row->tip_artikla_id == -1)
                                <li><a href="{{ Options::base_url().Url_mod::url_convert($row->naziv_stranice) }}">{{ Language::trans($row->title) }}</a></li>
                            @else
                            @if($row->tip_artikla_id==0)
                                <li><a href="{{ Options::base_url().Url_mod::convert_url('akcija') }}">{{ Language::trans($row->title) }}</a></li>
                            @else
                                <li><a href="{{ Options::base_url().Url_mod::convert_url('tip').'/'.Url_mod::url_convert(Support::tip_naziv($row->tip_artikla_id)) }}">
                                    {{ Language::trans($row->title) }}</a> 
                                </li>
                            @endif
                            @endif
                        @endforeach
                    </ul>
                 </div>
             </div> -->
           </div>
         @endif    
      </div> 
   </div>
 

 