<header class="container-fluid">   
    <div class="row div-in-header" id="JSfixed_header"> 
        <div class="col-md-2 col-sm-3 col-xs-12">
            <h1 class="for-seo"> 
                <a class="logo inline-block" href="/" title="{{Options::company_name()}}">
                    <img src="{{ Options::domain() }}{{Options::company_logo()}}" alt="{{Options::company_name()}}" class="img-responsive"/>
                </a>
            </h1>
        </div>
        <div id="responsive-nav" class="col-md-6 col-sm-6 col-xs-12 resp-nav-class no-padding">
        
            @if(Options::category_view()==1)
            @include('shop/themes/'.Support::theme_path().'partials/categories/category')
            @endif               
        
            <ul id="main-menu" class="list-inline pages-in-header inline-block relative">
                <?php $first=DB::table('web_b2c_seo')->where(array('parrent_id'=>0,'header_menu'=>1))->orderBy('rb_strane','asc')->get(); ?>

                @foreach($first as $row)
                <li class="header-dropdown relative">
                   @if(All::broj_cerki($row->web_b2c_seo_id) > 0)  
                    <a href="{{ Options::base_url().Url_mod::url_convert($row->naziv_stranice) }}" class="hdr-links">{{$row->title}}</a>
                        <?php $second=DB::table('web_b2c_seo')->where(array('parrent_id'=>$row->web_b2c_seo_id,'header_menu'=>1))->orderBy('rb_strane','asc')->get(); ?>

                        <ul class="header-dropdown-content">
                          @foreach($second as $row2)
                            <li> 
                              <a href="{{ Options::base_url().Url_mod::url_convert($row2->naziv_stranice) }}">{{$row2->title}}</a>
                            </li>
                          @endforeach
                        </ul>
                     @else   
                         <a href="{{ Options::base_url().Url_mod::url_convert($row->naziv_stranice) }}" class="hdr-links">{{$row->title}}</a> 
                   @endif                    
                  </li>                     
                @endforeach

                @if(Options::web_options(121)==1)
                    <?php $konfiguratori = All::getKonfiguratos(); ?>
                    @if(count($konfiguratori) > 0)
                    @if(count($konfiguratori) > 1)
                    <li class="konf-dropdown">
                        <a href="#!" class="konf-dropbtn inline-block">
                            <span class="glyphicon glyphicon-menu-right chevy-chase"></span> Konfiguratori
                        </a>
                        <ul class="konf-dropdown-content">
                            @foreach($konfiguratori as $row)
                            <li>
                                <a href="{{ Options::base_url() }}konfigurator/{{ $row->konfigurator_id }}">{{ $row->naziv }}</a>
                            </li>
                            @endforeach
                        </ul>
                    </li>
                    @else
                    <li>
                        <a href="{{ Options::base_url() }}konfigurator/{{ $konfiguratori[0]->konfigurator_id }}">
                            <span class="glyphicon glyphicon-menu-right chevy-chase"></span>Konfigurator
                        </a>
                    </li>
                    @endif
                    @endif
                @endif
            </ul>     
        </div>
        <div class="col-md-4 col-sm-9 col-xs-12 no-padding text-center"> 
            <div class="JSsearchContent2 header-search relative inline-block">
                <!-- SEARCH SELECT HIDDEN -->
                <span class="hide">{{ Groups::firstGropusSelect('2') }} </span>
                <input class="search-field" autocomplete="off" data-timer="" type="text" id="JSsearch2" placeholder="Pretraga" />
                <button onclick="search2()" class="JSsearch-button2"> <i class="fa fa-search"></i> </button>   
            </div>
            <div class="header-icons inline-block"> 
            @if(Session::has('b2c_kupac'))
            <span class="JSbroj_wish glyphicon glyphicon-heart" title="Omiljeni artikli"><span> {{ Cart::broj_wish() }} </span></span> 
            <a href="{{Options::base_url()}}{{Url_mod::convert_url('korisnik')}}/{{Url_mod::url_convert(WebKupac::get_user_name())}}" id="logged-user" class="inline-block">
                <span>{{ WebKupac::get_user_name() }}</span>
            </a>
            <a id="logged-user inline-block" href="{{Options::base_url()}}{{Url_mod::convert_url('korisnik')}}/{{Url_mod::url_convert(WebKupac::get_company_name())}}">
                <span>{{ WebKupac::get_company_name() }}</span>
            </a>
            <a id="logout-button" class="inline-block" href="{{Options::base_url()}}logout">
                <span class="glyphicon glyphicon-log-out" title="Odjavi se"></span>
            </a>
            @else 
            <div class="login-dropdown dropdown inline-block"> 
                <button class="login-btn" type="button" data-toggle="dropdown">
                    <span class="fa fa-user-o"></span>  
                </button>
                <ul class="dropdown-menu">
                    <li>
                        <a href="#" id="login-icon" data-toggle="modal" data-target="#loginModal"><span>
                             {{ Language::trans('Prijava') }}</span>
                        </a>
                    </li>
                    <li>
                        <a id="registration-icon" href="{{Options::base_url()}}{{ Url_mod::convert_url('registracija') }}"><span>
                            {{ Language::trans('Registracija') }}</span>
                        </a>
                    </li>
                </ul>
            </div> 
            @endif
            <!-- CART AREA -->   
            @include('shop/themes/'.Support::theme_path().'partials/cart_top') 

             <div class="resp-nav-btn"><span class="glyphicon glyphicon-menu-hamburger"></span></div>     
           </div>   
        </div>      
    </div>
</header>

<!-- MODAL ZA LOGIN -->
<div class="modal fade" id="loginModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><br>
                    <span class="welcome-login">{{ Language::trans('Za pristup Vašem nalogu unesite Vaš E-mail i lozinku') }}.</span>
                </h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="JSemail_login">E-mail</label>
                    <input class="form-control" placeholder="E-mail adresa" id="JSemail_login" type="text" value="" autocomplete="off">
                </div>
                <div class="form-group">
                    <label for="JSpassword_login">{{ Language::trans('Lozinka') }}</label>
                    <input class="form-control" placeholder="{{ Language::trans('Lozinka') }}" autocomplete="off" id="JSpassword_login" type="password" value="">
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" onclick="user_forgot_password()" class="modal-btns-login admin-login pull-left">{{ Language::trans('Zaboravljena lozinka') }}</button>
                <button type="submit" id="login" onclick="user_login()" class="modal-btns-login admin-login">{{ Language::trans('Prijavi se') }}</button>
                
                <a class="modal-btns-login inline-block" href="{{Options::base_url()}}registracija">Registruj se</a>
                <div class="field-group error-login JShidden-msg" id="JSForgotSuccess"><br>
                    {{ Language::trans('Novu lozinku za logovanje dobili ste na navedenoj e-mail adresi.') }}
                </div> 
            </div>
        </div>   
    </div>
</div>

  <!--========= QUICK VIEW MODAL ======== -->
  <div class="modal fade" id="JSQuickView" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
            <button type="button" id="JSQuickViewCloseButton" class="close close-me-btn">
              <span>&times;</span>
            </button>
          </div>
          <div class="modal-body" id="JSQuickViewContent">
            <img alt="loader-image" class="gif-loader" src="{{Options::base_url()}}images/quick_view_loader.gif">
          </div>
      </div>    
    </div>
  </div>
