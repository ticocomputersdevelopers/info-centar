    <!-- MAIN SLIDER -->  
    <div id="JSmain-slider" class="bw">
        <?php foreach(DB::table('baneri')->where('tip_prikaza',2)->orderBy('redni_broj','asc')->limit(10)->get() as $row){ ?>
        <?php $slajd_data = Support::slajd_data($row->baneri_id); ?>
        <div class="relative">
            <a href="<?php echo $row->link; ?>">
                <img class="img-responsive" src="{{ Options::domain() }}<?php echo $row->img; ?>" alt="{{$row->naziv}}" />
            </a> 
            <div class="sliderText"> 
                @if($slajd_data->nadnaslov != '')
                <div>
                    <p class="short-desc JSInlineShort" data-target='{"action":"slide_pretitle","id":"{{$row->baneri_id}}"}'>
                        {{ $slajd_data->nadnaslov }}
                    </p>
                </div>
                @endif

                @if($slajd_data->naslov != '')
                <div>
                    <h2 class="main-desc JSInlineShort" data-target='{"action":"slide_title","id":"{{$row->baneri_id}}"}'>
                        {{ $slajd_data->naslov }}
                    </h2>
                </div>
                @endif

                @if($slajd_data->sadrzaj != '')
                <div>
                    <div class="short-desc JSInlineFull" data-target='{"action":"slide_content","id":"{{$row->baneri_id}}"}'>
                        {{ $slajd_data->sadrzaj }}
                    </div>
                </div>
                @endif

                @if($slajd_data->naslov_dugme != '')
                <div>
                    <a href="<?php echo $row->link; ?>" class="slider-link JSInlineShort" data-target='{"action":"slide_button","id":"{{$row->baneri_id}}"}'>
                        {{ $slajd_data->naslov_dugme }}
                    </a>
                </div>
                @endif
            </div>
        </div>
        <?php } ?>
    </div>
    <!-- BANNERS -->
    <div class="container"> 
        <div class="banners-right text-center bw">
            <?php foreach(DB::table('baneri')->where('tip_prikaza', 1)->orderBy('redni_broj','asc')->get() as $row){ ?>
            <?php $slajd_data = Support::slajd_data($row->baneri_id); ?>
                <div class="inline-block relative">
                    <a class="center-block" href="<?php echo $row->link; ?>">
                        <img class="img-responsive" src="{{ Options::domain() }}<?php echo $row->img; ?>" alt="{{$row->naziv}}" />
                    </a>
                    <div class="sliderText"> 
                        @if($slajd_data->nadnaslov != '')
                        <div>
                            <p class="short-desc JSInlineShort" data-target='{"action":"slide_pretitle","id":"{{$row->baneri_id}}"}'>
                                {{ $slajd_data->nadnaslov }}
                            </p>
                        </div>
                        @endif

                        @if($slajd_data->naslov != '')
                        <div>
                            <h2 class="main-desc JSInlineShort" data-target='{"action":"slide_title","id":"{{$row->baneri_id}}"}'>
                                {{ $slajd_data->naslov }}
                            </h2>
                        </div>
                        @endif

                        @if($slajd_data->sadrzaj != '')
                        <div>
                            <div class="short-desc JSInlineFull" data-target='{"action":"slide_content","id":"{{$row->baneri_id}}"}'>
                                {{ $slajd_data->sadrzaj }}
                            </div>
                        </div>
                        @endif

                        @if($slajd_data->naslov_dugme != '')
                        <div>
                            <a href="<?php echo $row->link; ?>" class="slider-link JSInlineShort" data-target='{"action":"slide_button","id":"{{$row->baneri_id}}"}'>
                                {{ $slajd_data->naslov_dugme }}
                            </a>
                        </div>
                        @endif
                    </div>
                </div>
            <?php } ?>
        </div>  
    </div>