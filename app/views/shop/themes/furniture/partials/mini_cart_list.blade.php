@if(Cart::broj_cart()>0)
<ul> 
	@foreach(DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_id',Cart::korpa_id())->orderBy('web_b2c_korpa_stavka_id','asc')->get() as $row)
	<li class="text-left">
		<div> 
		    <span class="header-cart-image-wrapper inline-block">
			    <img src="{{ Options::domain() }}{{Product::web_slika($row->roba_id)}}" alt="{{Product::short_title($row->roba_id)}}"/>
			</span>
			<a class="inline-block" href="{{Options::base_url()}}{{Url_mod::convert_url('artikal')}}/{{Url_mod::url_convert(Product::seo_title($row->roba_id))}}">	{{Product::short_title($row->roba_id)}}
			</a>
			<a href="javascript:void(0)" data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}" title="{{ Language::trans('Ukloni artikal') }}" class="close JSDeleteStavka">X</a>
		</div>
		<div> 
			<span class="header-cart-amount">{{round($row->kolicina)}}</span>
			<span class="header-cart-price">{{Cart::cena($row->jm_cena)}}</span>
		</div>
	</li> 
 
	@endforeach	
 </ul>
<span class="header-cart-summary">{{ Language::trans('Ukupno') }}: <span>{{ Cart::cena(Cart::cart_ukupno()) }}</span></span> 
<div class="text-right"> 
	<a class="inline-block" href="{{ Options::base_url() }}{{ Seo::get_korpa() }}">Završi kupovinu</a>
</div>
@endif
 