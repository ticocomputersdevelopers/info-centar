<div class="header-cart-container relative inline-block"> 
	<a class="header-cart relative inline-block" href="{{ Options::base_url() }}{{ Seo::get_korpa() }}">
		<span class="glyphicon glyphicon-shopping-cart cart-me"></span>		
		<span class="JSbroj_cart badge"> {{ Cart::broj_cart() }} </span> 		
		<input type="hidden" id="h_br_c" value="{{ Cart::broj_cart() }}" />	
	</a>

	<div class="JSheader-cart-content">
		@include('shop/themes/'.Support::theme_path().'partials/mini_cart_list') 
	</div>
</div>  