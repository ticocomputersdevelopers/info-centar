<footer>
	<div class="container">
		<?php $newslatter_description = All::newslatter_description(); ?>
		@if(Options::newsletter()==1)
			<div class="row">
				<div class="col-md-5 col-sm-5 col-xs-12">
					<h5 class="newsletter-title JSInlineShort" data-target='{"action":"newslatter_label"}'>{{ $newslatter_description->naslov }}</h5> 
					<h6 class="JSInlineFull" data-target='{"action":"newslatter_content"}'>{{ $newslatter_description->sadrzaj }}</h6>
				</div>
				<div class="col-md-7 col-sm-7 col-xs-12">
					@if(Options::newsletter()==1)
						<div class="newsletter">			 
					    	<input type="text" placeholder="E-mail adresa" id="newsletter" />
							<button onclick="newsletter()">{{ Language::trans('Prijavi se') }}</button> 
						</div>
					@endif
				</div>
			</div>
		@endif
 
		<div class="row footer-sections">
			<br>
			<br>
			@foreach(All::footer_sections() as $footer_section)
				@if($footer_section->naziv == 'slika')
				<div class="col-md-3 col-sm-3 col-xs-12">				
					<div class="footer-desc-div">
						@if(!is_null($footer_section->slika))
						<a class="logo" href="{{ $footer_section->link }}">
							 <img  class="img-responsive inline-block" src="{{ Options::domain() }}{{ $footer_section->slika }}" alt="{{Options::company_name()}}" />
						</a>
						@else
						<a class="logo" href="/" title="{{Options::company_name()}}">
							 <img  class="img-responsive inline-block" src="{{ Options::domain() }}{{Options::company_logo()}}" alt="{{Options::company_name()}}" />
						</a>
						@endif 
						<p class="JSInlineFull" data-target='{"action":"footer_section_content","id":"{{$footer_section->futer_sekcija_id}}"}'>
							{{ $footer_section->sadrzaj }} 
						</p>
					</div>
				</div>
				@elseif($footer_section->naziv == 'text')
				<div class="col-md-3 col-sm-3 col-xs-12"> 
					<div class="footer-desc-div">
						<h5 class="footer-box-pages-title JSInlineShort" data-target='{"action":"footer_section_label","id":"{{$footer_section->futer_sekcija_id}}"}'>
							{{ $footer_section->naslov }}
						</h5> 
						<div class="JSInlineFull" data-target='{"action":"footer_section_content","id":"{{$footer_section->futer_sekcija_id}}"}'>
							{{ $footer_section->sadrzaj }} 
						</div>
					</div>
				</div> 
				@elseif($footer_section->naziv == 'linkovi')
				<div class="col-md-3 col-sm-3 col-xs-12"> 
					<h5 class="footer-box-pages-title JSInlineShort" data-target='{"action":"footer_section_label","id":"{{$footer_section->futer_sekcija_id}}"}'>
						{{ $footer_section->naslov }}
					</h5> 
					<ul class="footer-links">
						@foreach(All::footer_section_pages($footer_section->futer_sekcija_id) as $page)
							<li>
								<a href="{{ Options::base_url().Url_mod::url_convert($page->naziv_stranice) }}">{{ Language::trans($page->title) }}</a>
							</li>
						@endforeach
					</ul>  
				</div> 
				@elseif($footer_section->naziv == 'kontakt')
				<div class="col-md-3 col-sm-3 col-xs-12"> 
					<h5 class="footer-box-pages-title JSInlineShort" data-target='{"action":"footer_section_label","id":"{{$footer_section->futer_sekcija_id}}"}'>
						{{ $footer_section->naslov }}
					</h5>
					<ul>
					@if(Options::company_adress() OR Options::company_city())
						<li>
							{{ Options::company_adress() }}, <br>
						 	{{ Options::company_city() }}
						</li>
					@endif
					@if(Options::company_fax())
						<li>
							<a class="mailto" href="tel:{{ Options::company_fax() }}"> {{ Options::company_fax() }} </a>
						</li>
					@endif
					@if(Options::company_phone())
						<li>
							<a class="mailto" href="tel:{{ Options::company_phone() }}"> {{ Options::company_phone() }} </a>
						</li>
					@endif
					@if(Options::company_email())
						<li>
							<a class="mailto" href="mailto:{{ Options::company_email() }}"> {{ Options::company_email() }} </a>
						</li>
					@endif
					</ul>
				</div>		
				@endif
				 
				@if($footer_section->naziv == 'drustvene_mreze') 
				<div class="col-md-3 col-sm-3 col-xs-12"> 
			    	<h5 class="footer-box-pages-title JSInlineShort" data-target='{"action":"footer_section_label","id":"{{$footer_section->futer_sekcija_id}}"}'>
				    	{{ $footer_section->naslov }}
				    </h5>
					<div class="social-icons"> {{Options::social_icon()}} </div>   
				</div> 
				@endif
			@endforeach
		</div> 
		<br>
		<br>
  
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 text-center footer-links">
				<p>{{ Options::company_name() }} &copy; {{ date('Y') }}. Sva prava zadržana. - <a href="https://www.selltico.com/">Izrada internet prodavnice</a> - 
					<a href="https://www.selltico.com/"> Selltico. </a>
				</p>
			</div>
		</div>
		@if(Support::banca_intesa())
			<div class="row foot-note">
				<div class="col-md-6 col-sm-6 col-xs-12">
					<ul class="list-inline sm-text-center">
						<li>
							<a href="https://www.bancaintesa.rs" target="_blank">
								<img src="{{ Options::domain() }}images/cards/banca-intesa.png" alt="bank-logo">
							</a>
						</li> 
						<li>
							<a href="https://rs.visa.com/pay-with-visa/security-and-assistance/protected-everywhere.html" target="_blank">
								<img src="{{ Options::domain() }}images/cards/verified-by-visa.jpg" alt="bank-logo">
							</a>
						</li>
						<li>
							<a href="http://www.mastercard.com/rs/consumer/credit-cards.html" target="_blank">
								<img src="{{ Options::domain() }}images/cards/master-card-secure.gif" alt="bank-logo">
							</a>
						</li>
					</ul>
				</div>	
	 
				<div class="col-md-6 col-sm-6 col-xs-12">
					<ul class="list-inline sm-text-center text-right">
						<li><img src="{{ Options::domain() }}images/cards/visa-card.png" alt="bank-logo"></li> 
						<li><img src="{{ Options::domain() }}images/cards/american-express.png" alt="bank-logo"></li>
						<li><img src="{{ Options::domain() }}images/cards/master-card.png" alt="bank-logo"></li>
						<li><img src="{{ Options::domain() }}images/cards/maestro-card.png" alt="bank-logo"></li>
					</ul>
				</div>
			</div>
		@endif 
</footer>
