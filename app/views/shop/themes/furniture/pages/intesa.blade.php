@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')

	<div class="col-md-12 col-sm-12 col-xs-12 text-center">
		<br><br>
		<h3>Nakon potvrde vrši se preusmeravanje na sajt Banke Inteze, gde se na zaštićenoj stranici realizuje proces plaćanja.</h3></br>

		<h4>Podaci o korisniku:</h4>
		<ul>
			@if($web_kupac->flag_vrsta_kupca == 1)
				<li>Firma: {{ $web_kupac->naziv }}</li>
			@else
				<li>Ime: {{ $web_kupac->ime.' '.$web_kupac->prezime }}</li>
			@endif
			<li>Adresa: {{ $web_kupac->adresa }}, {{ $web_kupac->mesto }}</li>
		</ul>
		</br>
		<h4>Podaci o trgovcu:</h4>
		<ul>
			<li>{{ $preduzece->naziv }}</li>
			<li>Kontakt osoba: {{ $preduzece->kontakt_osoba }}</li>
			@if($preduzece->matbr_registra != '')
			<li>MBR: {{ $preduzece->matbr_registra }}</li>
			@endif
			@if($preduzece->pib != '')
			<li>VAT Reg No: {{ $preduzece->pib }}</li>
			@endif
			<li>{{ $preduzece->adresa }}, {{ $preduzece->mesto }}</li>
			<li>Tel: {{ $preduzece->telefon }}</li>
			<li><a href="mailto:{{ $preduzece->email }}">{{ $preduzece->email }}</a></li>
		</ul>
		</br>

		<input type="checkbox" id="JSConditions" onclick="clickCheckbox()"> <a href="{{ Options::base_url() }}intesa-uslovi" target="_blank">Uslovi korišćenja</a>

		<form method="post" action="https://bib.eway2pay.com/fim/est3Dgate">

		 		<button type="submit" id="JSIntesaSubmit" disabled>Završi plaćanje karticom</button>

 				<input type="hidden" name="clientid" value="{{ $clientId }}">
				<input type="hidden" name="oid" value="{{ $oid }}">
				<input type="hidden" name="amount" value="{{ $amount }}">
				<input type="hidden" name="okurl" value="{{ $okUrl }}">
				<input type="hidden" name="failUrl" value="{{ $failUrl }}">
				<input type="hidden" name="TranType" value="{{ $transactionType }}">
				<input type="hidden" name="currency" value="{{ $currency }}">
				<input type="hidden" name="rnd" value="{{ $rnd }}">
				<input type="hidden" name="hash" value="{{ $hash }}">
				<input type="hidden" name="storetype" value="3D_PAY_HOSTING">
				<input type="hidden" name="hashAlgorithm" value="ver2">
				<input type="hidden" name="encoding" value="utf-8" />
				<input type="hidden" name="lang" value="en">			
				
		</form>
		<script type="text/javascript">
			function clickCheckbox(){
				if(document.getElementById("JSConditions").checked){
					document.getElementById("JSIntesaSubmit").removeAttribute('disabled');
				}else{
					document.getElementById("JSIntesaSubmit").setAttribute('disabled','disabled');
				}
			}
		</script>
	</div>
@endsection