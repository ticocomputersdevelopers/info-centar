@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page') 
<div class="news-section">
	<div class="row single-news">
		<div class="col-md-4 col-sm-4 col-xs-12">
			<img class="img-responsive center-block" src="{{ Options::domain() }}{{ $slika }}" alt="{{ $naslov }}">
		</div>
		<div class="col-md-8 col-sm-8 col-xs-12 news-text">
			<h2 class="news-title"><span>{{ $naslov }}</span></h2>
			 {{ $sadrzaj }} 
		</div>
	</div>  
</div>
@endsection