@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
<div class="h2-container row"> 
	<h2><span class="heading-background">{{ Language::trans('Kontaktirajte nas') }}</span></h2> 
</div>
  
<div class="row"> 	
	<div class="contact-info col-md-4 col-sm-12 col-xs-12">
	 	<h3 class="contact-heading">{{ Language::trans('Kontakt informacije') }}</h3><br>
	 	<ul>
	 		@if(Options::company_name() != '')
				<li class="col-md-4 col-sm-5 col-xs-5">{{ Language::trans('Firma') }}:</li>
				<li class="col-md-8  col-sm-7 col-xs-7"> {{ Options::company_name() }} &nbsp;</li>
			@endif
			@if(Options::company_adress() != '')
			    <li class="col-md-4 col-sm-5 col-xs-5">{{ Language::trans('Adresa') }}:</li>
			    <li class="col-md-8 col-sm-7 col-xs-7"> {{ Options::company_adress() }} &nbsp;</li>
			@endif
			@if(Options::company_city() != '')
				<li class="col-md-4 col-sm-5 col-xs-5">{{ Language::trans('Grad') }}:</li>
				<li class="col-md-8 col-sm-7 col-xs-7"> {{ Options::company_city() }} &nbsp;</li>
			@endif
			@if(Options::company_phone() != '')
				<li class="col-md-4 col-sm-5 col-xs-5">{{ Language::trans('Telefon') }}:</li>
				<li class="col-md-8 col-sm-7 col-xs-7"> {{ Options::company_phone() }} &nbsp;</li>
			@endif
			@if(Options::company_fax() != '')
				<li class="col-md-4 col-sm-5 col-xs-5">{{ Language::trans('Fax') }}:</li>
				<li class="col-md-8 col-sm-7 col-xs-7"> {{ Options::company_fax() }} &nbsp;</li>
			@endif
			@if(Options::company_pib() != '')
				<li class="col-md-4 col-sm-5 col-xs-5">{{ Language::trans('PIB') }}:</li>
				<li class="col-md-8 col-sm-7 col-xs-7"> {{ Options::company_pib() }} &nbsp;</li>
			@endif
			@if(Options::company_email() != '')
				<li class="col-md-4 col-sm-5 col-xs-5">E-mail:</li>
				<li class="col-md-8 col-sm-7 col-xs-7">
					<a class="mailto" href="mailto:{{ Options::company_email() }}">{{ Options::company_email() }}</a>
				</li>
			@endif
		</ul>
	</div> 

	<div class="contact-form col-md-8 col-sm-12 col-xs-12"> 
		<h3 class="contact-heading">{{ Language::trans('Pošaljite poruku') }}</h3><br>
		<div class="row"> 
			<div class="form-group col-md-6 col-sm-6 col-xs-12">
				<label id="label_name">{{ Language::trans('Vaše ime') }} *</label>
				<input class="contact-name form-control" id="JSkontakt-name" type="text" onchange="check_fileds('JSkontakt-name')" >
			</div>
			<div class="form-group col-md-6 col-sm-6 col-xs-12">
				<label id="label_email">{{ Language::trans('Vaša e-mail adresa') }} *</label>
				<input class="contact-email form-control" id="JSkontakt-email" onchange="check_fileds('JSkontakt-email')" type="text" >
			</div>		
		 
			<div class="form-group col-md-12 col-sm-12 col-xs-12">	
				<label id="label_message">{{ Language::trans('Vaša poruka') }} </label>
				<textarea class="contact-message form-control" rows="5" id="message"></textarea>
			  	<div class="text-right"> 
					<button class="submit" onclick="meil_send()">{{ Language::trans('Pošalji') }}</button>
				</div>
			</div>
		</div>
	</div>
</div>
	@if(Options::company_map() != '' && Options::company_map() != ';')
	<div id="map_canvas" class="map"></div>
	@endif

@endsection     