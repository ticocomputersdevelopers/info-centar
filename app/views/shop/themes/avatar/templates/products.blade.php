<!DOCTYPE html>
<html lang="sr">
	<head>
		@include('shop/themes/'.Support::theme_path().'partials/head')
	</head>
	<body id="product-page"
    @if($strana == All::get_page_start()) id="start-page"  @endif 
    @if(Support::getBGimg() != null) 
        style="background-image: url({{Options::base_url()}}{{Support::getBGimg()}}); background-size: cover; background-repeat: no-repeat; background-attachment: fixed; background-position: center;" 
    @endif
	>
	 
		<!-- Header -->
		@include('shop/themes/'.Support::theme_path().'partials/header')
			
			
		@if(Options::category_view()==0 AND $strana != 'proizvodjac' AND $strana != 'akcija' AND $strana != 'tip')

			@include('shop/themes/'.Support::theme_path().'partials/categories/categories_horizontal')
		@endif		

		
	 
	<div id="middle-area"> 
		<div id="main-content">
			<div class="container">
				<div class="products-page bw">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						@if($strana!='akcija' and $strana!='tip')
							<div class="page-breadcrumb">
							    <ul class="breadcrumb">
									@if($strana!='pretraga' and $strana!='filteri' and $strana!='proizvodjac')
										{{ Url_mod::breadcrumbs2()}}
									@elseif($strana == 'proizvodjac')

									<li>
										<a href="{{ Options::base_url() }}{{ Url_mod::convert_url('brendovi') }}">Brendovi</a>  
										@if($grupa)
										<a href="{{ Options::base_url() }}{{ Url_mod::convert_url('proizvodjac') }}/{{ Url_mod::url_convert($proizvodjac) }}">{{ All::get_manofacture_name($proizvodjac_id) }}</a>
									    {{ Groups::get_grupa_title($grupa) }}
									</li>

										@else

										{{ All::get_manofacture_name($proizvodjac_id) }} 

										@endif
									@else

										<li>
											<a href="{{ Options::base_url() }}">
												{{ All::get_title_page_start() }}
											</a>
										</li>

										<li>
											<a href="">
											{{$title}}
											</a>
										</li>
									@endif
							    </ul>
						    </div>

					    @endif 
					</div>
				</div>


			<!-- @include('shop/themes/'.Support::theme_path().'partials/breadcrumb') -->


				<div class="row products-page-content">

					<aside id="sidebar-left" class="col-md-3 col-sm-12 col-xs-12">
						<div class="JSShowFilters">
							Prikaži filtere
						</div>
						@include('shop/themes/'.Support::theme_path().'partials/filters')
					</aside>
 		 	

				   	@if($strana!='pretraga' and $strana!='filteri' and $strana!='proizvodjac' and $strana != 'tagovi')
						@if(isset($sub_cats) and !empty($sub_cats))
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="group-parent-div">
									@foreach($sub_cats as $row)
									
										<div class="sub_category_item">
											<a href="{{ Options::base_url()}}{{ $url }}/{{ Url_mod::url_convert($row->grupa) }}/0/0/0-0">
											@if(isset($row->putanja_slika))
											<div>
												<img src="{{ Options::domain() }}{{ $row->putanja_slika }}" alt="{{ $row->grupa }}" class="img-responsive" />
											</div>
											
											<div>
												<p>
													{{ $row->grupa }}
												</p>
											</div>

											@else
											<div>
												<img src="{{ Options::domain() }}images/No-image-available.jpg" alt="{{ $row->grupa }}" class="img-responsive" />
											</div>

											<div>
												<p>
													{{ $row->grupa }}
												</p>
											</div>
											@endif
											</a>
										</div>
									
									@endforeach
								</div>
							</div>
						</div>
						@endif
					@endif
	 
		            @yield('products_list')

				</div> <!-- End of row -->
			</div> <!-- End of container -->
		</div> <!-- End of main-content -->
	</div> <!-- End of midle-area -->
</div> <!-- End of products-page -->
		
	<!-- Footer -->
	@include('shop/themes/'.Support::theme_path().'partials/footer')
 
 	<a class="JSscroll-top" href="javascript:void(0)"><i class="fa fa-chevron-up"></i></a>

	<!-- LOGIN POPUP -->
	@include('shop/themes/'.Support::theme_path().'partials/popups')

	<!-- BASE REFACTORING -->
	<input type="hidden" id="base_url" value="{{Options::base_url()}}" />
	<input type="hidden" id="vodjenje_lagera" value="{{Options::vodjenje_lagera()}}" />
	
	<!-- js includes -->
	@if(Session::has('b2c_admin'.Options::server()) OR (Options::enable_filters()==1 AND $filter_prikazi))
	<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
	@endif
	@if(Session::has('b2c_admin'.Options::server()))
	<script type="text/javascript" src="{{Options::domain()}}js/tinymce2/tinymce.min.js"></script>
	<script src="{{ Options::domain()}}js/jquery-ui.min.js"></script>
	<script src="{{Options::domain()}}js/shop/front_admin/main.js"></script>
	<script src="{{Options::domain()}}js/shop/front_admin/products.js"></script>
	<script src="{{Options::domain()}}js/shop/front_admin/product.js"></script>
	@endif	
	
	<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}main_function.js"></script>
	<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}cart.js"></script>
	<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}main.js"></script>
  	@if(Options::header_type()==1)
		<!-- fixed navgation bar -->
 		<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}fixed_header.js"></script>
	@endif
</body>
</html>