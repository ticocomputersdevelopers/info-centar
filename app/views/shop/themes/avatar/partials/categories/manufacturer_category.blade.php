

<nav class="manufacturer-categories"> 
    <h3>Proizvođač - {{$title}}</h3>
    <ul class="row">
        @foreach(Support::manufacturer_categories($proizvodjac_id) as $key => $value)
            <li>
                <a  class="" href="{{ Options::base_url()}}{{ Url_mod::convert_url('proizvodjac') }}/{{ Url_mod::url_convert($proizvodjac) }}/{{ Url_mod::url_convert($key) }}">
                    <span class="">{{ Language::trans($key) }}</span>
                    <span class="">{{ $value }}</span>
                </a>
            </li>
        @endforeach  
    </ul>
</nav>
