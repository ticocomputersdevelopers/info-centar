

<nav class="manufacturer-categories"> 
    <h3>{{$title}}</h3>
    <ul class="row">
        @foreach(Support::akcija_categories() as $key => $value)
            <li>
                <a  class="" href="{{ Options::base_url()}}{{ Url_mod::convert_url('akcija') }}/{{ Url_mod::url_convert($key) }}">
                    <span class="">{{ Language::trans($key) }}</span>
                    <span class="">{{ $value }}</span>
                </a>
            </li>
        @endforeach  
    </ul>
</nav> 
 