


<div class="JSproduct {{ (isset($sub_cats) && count($sub_cats)>0) ? 'col-md-3 col-sm-3' : 'col-md-4 col-sm-4'}} col-xs-12"> 
	<div class="shop-product-card"> 

	<div class="multiple-function-div">
		@if(Cart::kupac_id() > 0)
			<div>
			  	<button data-roba_id="{{$row->roba_id}}" data-toggle="tooltip" title="Dodaj na listu želja" class="JSadd-to-wish wish-list">
				  	<i class="far fa-heart"></i>
				</button>
			</div> 
		@else
			<div>
			  	<button data-roba_id="{{$row->roba_id}}" title="Dodavanje artikla na listu želja moguće je samo registrovanim korisnicima" class="wish-list JSnot-wish">
			  		<i class="far fa-heart"></i>
				</button>
			</div>  
		@endif	

		@if(Options::compare()==1 AND isset($filter_prikazi) AND $filter_prikazi == 1)
		<div>
			<span class="JScompare {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}">
				<i title="{{ Language::trans('Uporedi') }}" class="fas fa-exchange-alt"></i>
			</span>
		</div>
		@endif

		<div>
			<span class="JSQuickViewButton" data-roba_id="{{$row->roba_id}}">
				<i class="fas fa-search"></i>
			</span>
		</div>
	</div>

		<!-- SALE PRICE -->
		@if(All::provera_akcija($row->roba_id))
			<div class="sale-label">
				{{ Language::trans('Akcija') }}
				<!-- <span class="for-sale-price">- {{ Product::getSale($row->roba_id) }} din</span> -->
			</div>
		@endif
		
		<div class="product-image-wrapper">
 		<!-- PRODUCT IMAGE -->
			<a href="{{Options::base_url()}}{{Url_mod::convert_url('artikal')}}/{{Url_mod::url_convert(Product::seo_title($row->roba_id))}}">

				<img class="product-image img-responsive" 
				src="{{ Options::domain() }}{{ Product::web_slika($row->roba_id) }}" 
				alt="{{ Product::seo_title($row->roba_id) }}" />

				<img alt="{{ Product::seo_title($row->roba_id) }}" class="second-product-image" src="{{ Options::domain() }}{{ Product::web_slika_second($row->roba_id) }}" >

			</a>

		 </div>
   
	<div class="product-meta"> 

		<!-- PRODUCT TITLE -->
		<div>
			<a class="product-name" href="{{Options::base_url()}}{{Url_mod::convert_url('artikal')}}/{{Url_mod::url_convert(Product::seo_title($row->roba_id))}}">
				<h2> {{ Product::short_title($row->roba_id) }} </h2>
			</a>
		</div>

		
  
		<div class="price-holder">
		    <span class="product-price"> {{ Cart::cena(Product::get_price($row->roba_id)) }} </span>
            @if(All::provera_akcija($row->roba_id))
            <span class="product-old-price">{{ Cart::cena(Product::old_price($row->roba_id)) }}</span>
            @endif     
        </div>	 

    	<div class="text-center"> 
             <span class="review"> 
				{{ Product::getRating($row->roba_id) }}
            </span>
        </div>

		<div class="add-to-cart-container"> 						
		 	@if(Product::getStatusArticle($row->roba_id) == 1)	
				@if(Cart::check_avaliable($row->roba_id) > 0)	 
					<div data-roba_id="{{$row->roba_id}}" title="{{ Language::trans('Dodaj u korpu') }}" class="dodavnje JSadd-to-cart buy-btn">
						{{ Language::trans('Dodaj u korpu') }}			 
			        </div>	
							    
				@else 

			 	<!-- NOT AVAILABLE -->
		        <div class="dodavnje not-available buy-btn" title="Nije dostupno">
			        {{Language::trans('Nije dostupno')}}
			    </div>	      
		                  

				@endif

				@else  
					<!-- NOT AVAILABLE -->	 
					<div class="dodavanje not-available buy-btn">
						{{ Product::find_flag_cene(Product::getStatusArticle($row->roba_id),'naziv') }} 
					</div>					 		 	 	
			@endif 	
		</div>             

	 </div>


	 @if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array(200)))
		<a class="article-edit-btn JSFAProductModalCall" data-roba_id="{{$row->roba_id}}" href="javascript:void(0)">
			{{ Language::trans('IZMENI ARTIKAL') }}
		</a> 
	 @endif  <!-- Admin buttZZon -->
	</div>
</div>
 
               
  
  