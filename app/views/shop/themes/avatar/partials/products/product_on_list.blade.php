


    <div class="shop-product-card-list row"> 

	<div class="col-md-3 col-sm-3 col-xs-12 product-image-wrapper">
		<a href="{{Options::base_url()}}{{Url_mod::convert_url('artikal')}}/{{Url_mod::url_convert(Product::seo_title($row->roba_id))}}" class="">

		    <img class="product-image img-responsive" src="{{ Options::domain() }}{{ Product::web_slika($row->roba_id) }}" alt="{{ Product::seo_title($row->roba_id) }}" />

		    <img class="second-product-image" src="{{ Options::domain() }}{{ Product::web_slika_second($row->roba_id) }}" >
		</a>
	</div> 

	<div class="col-md-4 col-sm-4 col-xs-12"> 
		<a class="product-name" href="{{Options::base_url()}}{{Url_mod::convert_url('artikal')}}/{{Url_mod::url_convert(Product::seo_title($row->roba_id))}}">
			{{ Product::short_title($row->roba_id) }}
		</a>
	</div> 
<!-- <div> 
	<section class="medium-5 columns text-align-left">
		{{ Product::get_karakteristike_short($row->roba_id) }}
	</section> 
</div> -->

	<div class="col-md-5 col-sm-5 col-xs-12">
		<div class="prices-div">

			<div class="multiple-function-div">
				@if(Cart::kupac_id() > 0)
					<div>
					  	<button data-roba_id="{{$row->roba_id}}" title="Dodaj na listu želja" class="JSadd-to-wish wish-list">
						  	<i class="far fa-heart"></i>
						</button>
					</div>

				@else
				
					<div>
					  	<button data-roba_id="{{$row->roba_id}}" title="Dodavanje artikla na listu želja moguće je samo registrovanim korisnicima" class="wish-list JSnot-wish">
					  		<i class="far fa-heart"></i>
						</button>
					</div>  
				@endif
				
				@if(Options::compare()==1 AND isset($filter_prikazi) AND $filter_prikazi == 1)
				<div>
					<span class="JScompare {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}">
						<i class="fas fa-exchange-alt" title="{{ Language::trans('Uporedi') }}"></i>
					</span>
				</div>
				@endif

				<div>
					<span class="JSQuickViewButton" data-roba_id="{{$row->roba_id}}">
						<i class="fas fa-search"></i>
					</span>
				</div>
			</div>
		
			<div class="product-price">
				{{ Cart::cena(Product::get_price($row->roba_id)) }}

				@if(All::provera_akcija($row->roba_id))
					<span class="product-old-price">
						{{ Cart::cena(Product::old_price($row->roba_id)) }}
					</span>
				@endif
			</div>

			
			<div class="star-div"> 
				{{ Product::getRating($row->roba_id) }}
			</div>
		

			 <div class="add-to-cart-container"> 

	 		<!-- 	Wish list	
	 					 	@if(Cart::kupac_id() > 0)
	 					 		<div class="like-it">
	 					 			<button data-roba_id="{{$row->roba_id}}" title="Dodaj na listu želja" class="JSadd-to-wish glyphicon glyphicon-heart wish-list"></button>
	 					 		</div> 
	 		
	 					 	@else
	 		
	 						 	<div class="like-it">
	 						 		<button data-roba_id="{{$row->roba_id}}" title="Dodavanje artikla na listu želja moguće je samo registrovanim korisnicima" class="glyphicon glyphicon-heart wish-list"></button>
	 						 	</div>
	 					 	@endif 
	 		 -->
	        <!--  <div class="">Šifra artikla: {{ Product::get_sifra($row->roba_id) }}</div> -->
   				<!-- Add to cart button -->
				@if(AdminSupport::getStatusArticle($row->roba_id) == 1)

				@if(Cart::check_avaliable($row->roba_id) > 0)
			  		<div data-roba_id="{{$row->roba_id}}" title="{{ Language::trans('Dodaj u korpu') }}" class="dodavnje JSadd-to-cart buy-btn">
				 		{{ Language::trans('Dodaj u korpu') }}			 
				    </div>	
				 @else 		

	       		    <div class="dodavnje not-available buy-btn" title="{{ Language::trans('Nije dostupno') }}"> 
		       		    {{ Language::trans('Nije dostupno') }}
		       		</div>

			  @endif

				 @else	 
					<div class="dodavanje not-available buy-btn" title="{{ Language::trans('Nije dostupno') }}">
						{{ AdminSupport::find_flag_cene(AdminSupport::getStatusArticle($row->roba_id),'naziv') }}
					</div>
						
				@endif
				</div>
			</div>
		</div>
 
	<!-- Opis karakteristike -->
	<!-- <div class="col-md-5  ">
		{{ Product::get_karakteristike_short_grupe($row->roba_id) }}
	</div>  -->
 
  		@if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array(200)))
				<a class="article-edit-btn JSFAProductModalCall" data-roba_id="{{$row->roba_id}}" href="javascript:void(0)">
				{{ Language::trans('IZMENI ARTIKAL') }}
			</a>
		@endif
   </div>

    