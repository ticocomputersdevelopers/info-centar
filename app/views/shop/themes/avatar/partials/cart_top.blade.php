
<div class="space-between cart-wish-div"> 
	<div class="wish-div">

		<a class="resp-icon JSSearchIcon" href="#!">
			<i class="fas fa-search"></i>
		</a>


	    @if(Session::has('b2c_kupac'))       
		    @if(trim(WebKupac::get_user_name())) 
		        <a href="{{Options::base_url()}}{{Url_mod::convert_url('lista-zelja')}}/{{Url_mod::url_convert(WebKupac::get_user_name())}}">
		            <i class="fas fa-heart"></i>
		            <span>
		            	{{ Language::trans('Lista želja') }}
		            </span>
		            <span class="JSbroj_wish">{{ Cart::broj_wish() }}</span>
		        </a>
		    @endif 

		    @if(trim(WebKupac::get_company_name()))
		        <a href="{{Options::base_url()}}{{Url_mod::convert_url('lista-zelja')}}/{{Url_mod::url_convert(WebKupac::get_company_name())}}">
		        	<i class="fas fa-heart"></i>
		        	<span>
		        		{{ Language::trans('Lista želja') }}
		        	</span>
		        	<span class="JSbroj_wish">
			        	{{ Cart::broj_wish() }}
			        </span>
		        </a> 
		    @endif
	    @endif

	    @if(Options::compare()==1 AND isset($filter_prikazi) AND $filter_prikazi == 1)
		<a role="button" id="JScompareArticles"  data-toggle="modal" data-target="#compared-articles" href="#!">
			<i class="fas fa-exchange-alt"></i>
			<span>
				Upoređeni artikli
			</span>
		</a>
		@endif
		
	</div>

	<div class="header-cart-div">
		<a class="header-cart" href="{{ Options::base_url() }}{{ Seo::get_korpa() }}">
			<span class="cart-icon"><i class="fas fa-shopping-basket"></i></span>
			<span class="JSbroj_cart badge"> {{ Cart::broj_cart() }} </span> 
			<input type="hidden" id="h_br_c" value="{{ Cart::broj_cart() }}" />	
		</a>

		<div class="cart-value-div">
			<span id="JScart_ukupno" class="cart-value">{{ Cart::cena(Cart::cart_ukupno()) }}</span>
		</div>

		<div class="JSheader-cart-content">
			@include('shop/themes/'.Support::theme_path().'partials/mini_cart_list') 
		</div>
	</div>
	
</div> 
