    



<!-- MAIN SLIDER -->  

<div class="container">
    
        <div class="col-md-12 main-slider bw">
            <div id="JSmain-slider" >
                @foreach(DB::table('baneri')->where('tip_prikaza',2)->orderBy('redni_broj','asc')->limit(10)->get() as $row)
                <?php $slajd_data = Support::slajd_data($row->baneri_id); ?>
                    <div>
                        @if($slajd_data->naslov_dugme == '')
                        <a class="AllSliderLink" href="<?php echo $row->link; ?>" >
                        @endif
                                <div class="JSmain-slider" style="background-image: url('{{ Options::domain() }}<?php echo $row->img; ?>'); ">
                                
                                
                                    <div class="slider-desc-div">
                                    @if($slajd_data->nadnaslov != '')
                                        <div>
                                            <p class="short-desc JSInlineShort" data-target='{"action":"slide_pretitle","id":"{{$row->baneri_id}}"}'>
                                                {{ $slajd_data->nadnaslov }}
                                            </p>
                                        </div>
                                    @endif

                                    @if($slajd_data->naslov != '')
                                        <div>
                                            <h2 class="main-desc JSInlineShort" data-target='{"action":"slide_title","id":"{{$row->baneri_id}}"}'>
                                                {{ $slajd_data->naslov }}
                                            </h2>
                                        </div>
                                    @endif

                                    @if($slajd_data->sadrzaj != '')
                                        <div>
                                            <div class="short-desc JSInlineFull" data-target='{"action":"slide_content","id":"{{$row->baneri_id}}"}'>
                                                {{ $slajd_data->sadrzaj }}
                                            </div>
                                        </div>
                                    @endif

                                    @if($slajd_data->naslov_dugme != '')
                                        <div>
                                            <a href="<?php echo $row->link; ?>" class="slider-link JSInlineShort" data-target='{"action":"slide_button","id":"{{$row->baneri_id}}"}'>
                                                {{ $slajd_data->naslov_dugme }}
                                            </a>
                                        </div>
                                     @endif
                                    </div>
                               
                                </div>
                        @if($slajd_data->naslov_dugme == '')
                        </a>
                        @endif
                    </div>
                @endforeach
            </div>

            <?php $first_banner = DB::table('baneri')->where('tip_prikaza', 1)->orderBy('redni_broj','asc')->first(); ?>
            @if(!is_null($first_banner))
            <?php $slajd_data = Support::slajd_data($first_banner->baneri_id); ?>

                @if($slajd_data->naslov_dugme == '')
                <a class="JSBannerLink" href="<?php echo $first_banner->link; ?>" >
                @endif
                    <div class="banners-bg custom-baner" style="background-image: url('{{ Options::domain() }}<?php echo $first_banner->img; ?>'); ">

                        <div class="baners-desc-div">
                            <div class="banner-desc">
                                @if($slajd_data->nadnaslov != '')
                                <div>
                                    <p class="short-desc JSInlineShort" data-target='{"action":"slide_pretitle","id":"{{$first_banner->baneri_id}}"}'>
                                        {{ $slajd_data->nadnaslov }}
                                    </p>
                                </div>
                                @endif

                                @if($slajd_data->naslov != '')
                                <div>
                                    <h2 class="main-desc JSInlineShort" data-target='{"action":"slide_title","id":"{{$first_banner->baneri_id}}"}'>
                                        {{ $slajd_data->naslov }}
                                    </h2>
                                </div>
                                @endif

                                @if($slajd_data->sadrzaj != '')
                                <div>
                                    <div class="short-desc JSInlineFull" data-target='{"action":"slide_content","id":"{{$first_banner->baneri_id}}"}'>
                                        {{ $slajd_data->sadrzaj }}
                                    </div>
                                </div>
                                @endif

                                @if($slajd_data->naslov_dugme != '')
                                <div>
                                    <a href="<?php echo $first_banner->link; ?>" class="baner-link JSInlineShort" data-target='{"action":"slide_button","id":"{{$first_banner->baneri_id}}"}'>
                                        {{ $slajd_data->naslov_dugme }}
                                    </a>
                                </div>
                                @endif
                            </div>
                        </div>
                   
                    </div>
                @if($slajd_data->naslov_dugme == '')
                </a>
                @endif
            
            @endif

        </div>
    
</div>

    <!-- BANNERS -->
    <div class="baners-div">
        <div class="container">
            <div class="row bw">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <aside class="banners-right">
                        @foreach(DB::table('baneri')->where('tip_prikaza', 1)->orderBy('redni_broj','asc')->get() as $key => $row)
                    
                            @if($key!=0)
                            <?php $slajd_data = Support::slajd_data($row->baneri_id); ?>
                            <figure>
                                @if($slajd_data->naslov_dugme == '')
                                <a class="JSBannerLink" href="<?php echo $row->link; ?>" >
                                @endif
                                    <div class="banners-bg" style="background-image: url('{{ Options::domain() }}<?php echo $row->img; ?>'); ">

                                        <div class="baners-desc-div">
                                            <div class="banner-desc">
                                                @if($slajd_data->nadnaslov != '')
                                                <div>
                                                    <p class="short-desc JSInlineShort" data-target='{"action":"slide_pretitle","id":"{{$row->baneri_id}}"}'>
                                                        {{ $slajd_data->nadnaslov }}
                                                    </p>
                                                </div>
                                                @endif

                                                @if($slajd_data->naslov != '')
                                                <div>
                                                    <h2 class="main-desc JSInlineShort" data-target='{"action":"slide_title","id":"{{$row->baneri_id}}"}'>
                                                        {{ $slajd_data->naslov }}
                                                    </h2>
                                                </div>
                                                @endif

                                                @if($slajd_data->sadrzaj != '')
                                                <div>
                                                    <div class="short-desc JSInlineFull" data-target='{"action":"slide_content","id":"{{$row->baneri_id}}"}'>
                                                        {{ $slajd_data->sadrzaj }}
                                                    </div>
                                                </div>
                                                @endif

                                                @if($slajd_data->naslov_dugme != '')
                                                <div>
                                                    <a href="<?php echo $row->link; ?>" class="baner-link JSInlineShort" data-target='{"action":"slide_button","id":"{{$row->baneri_id}}"}'>
                                                        {{ $slajd_data->naslov_dugme }}
                                                    </a>
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                   
                                    </div>
                                @if($slajd_data->naslov_dugme == '')
                                </a>
                                @endif
                            </figure>

                            @endif
                        @endforeach
                    </aside>  
                </div>
            </div>
        </div>
    </div> 




