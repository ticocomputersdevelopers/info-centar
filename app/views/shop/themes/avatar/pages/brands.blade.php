@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
 
	<div class="container">
		<div class="brands-pag bw">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="page-breadcrumb">
					<ul>
						<li>
							<a href="/pocetna">
								Početna /
							</a>
						</li>
						<li>
							<a href="#!">
								Brendovi
							</a>
						</li>
					</ul>
				</div>

				<h2 class="page-heading">
					{{ Language::trans('Brendovi') }}
				</h2>
			</div>
		</div>

		<div class="row brands-content">
		
			@foreach($brendovi as $brend)
			    <span class="col-lg-2 col-md-2 col-sm-6 col-xs-6">
			        <a class="brend-item" href="{{ Options::base_url() }}{{ Url_mod::convert_url('proizvodjac')}}/{{ Url_mod::url_convert($brend->naziv) }}">
			            @if($brend->slika != null OR $brend->slika != '')
			            <img class="img-responsive" src="{{ Options::domain() }}{{ $brend->slika }}" alt="{{ Language::trans($brend->naziv) }}" />
			            @else
			            {{ Language::trans($brend->naziv) }}
			            @endif
			        </a>
			    </span>	
			@endforeach
			
		</div>
	</div>
</div>
@endsection