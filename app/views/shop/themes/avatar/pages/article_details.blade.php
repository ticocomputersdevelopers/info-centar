@extends('shop/themes/'.Support::theme_path().'templates/article')

@section('article_details')

    <div id="fb-root"></div>

    <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12';
    fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    
    @if(Session::has('success_add_to_cart'))
    $(document).ready(function(){    
        alertSuccess("Artikal je dodat u korpu.");
    });
    @endif
    </script>
 

<div id="middle-area">
    <div id="main-content"> 
        <div class="container"> 
             <div class="article-details-page bw">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="page-breadcrumb">
                            <ul class="breadcrumb"> 
                                {{ Product::product_bredacrumps(DB::table('roba')->where('roba_id',$roba_id)->pluck('grupa_pr_id')) }}
                                <li>{{Language::trans(Product::seo_title($roba_id))}}</li>
                            </ul>
                        </div>

                        <h2 class="page-heading">
                            {{ Product::get_grupa_title($roba_id) }}
                        </h2>
                    </div>
                </div>

           <!-- <div class="row">
               <div class="col-lg-12">
                   <ul class="breadcrumb-div"> 
                       {{ Product::product_bredacrumps(DB::table('roba')->where('roba_id',$roba_id)->pluck('grupa_pr_id')) }}
                   </ul>
               </div>
           </div> -->


                <div class="row article-details-div" itemscope itemtype="http://schema.org/Product">
                    
                        <!-- Product preview image -->
                        <div class="JSproduct-preview-image col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <div id="gallery_01" class=""> 
                                {{ Product::get_list_images($roba_id) }}
                            </div>

                            <div class="big-galery-img disableZoomer"> 
                                <a class="fancybox" href="{{ Options::domain() }}{{ Product::web_slika_big($roba_id) }}">
                                    <span id="zoom_03" class="JSzoom_03" style="display: block;" data-zoom-image="{{ Options::domain() }}{{ Product::web_slika_big($roba_id) }}" >
                                        <img itemprop="image" class="JSzoom_03 img-responsive" src="{{ Options::domain() }}{{ Product::web_slika_big($roba_id) }}" alt="{{ Product::seo_title($roba_id)}}" />
                                    </span>
                                </a>
                            </div>
                        </div>

                        <div class="product-preview-info col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <h2 class="article-heading" itemprop="name">
                                {{ Language::trans(Product::seo_title($roba_id)) }}
                            </h2>

                            <div class="rate-me-artical">
                                {{ Product::getRating($roba_id) }}
                            </div>



                            <!-- Brendovi -->
                            @if($proizvodjac_id != -1)
                                <div class="product-manufacturer" itemprop="brand">
                                    <span>
                                        {{Language::trans('Proizvođač')}}:
                                    </span>
            
                                    @if( Product::slikabrenda($roba_id) != null )
                                        <a class="article-brand-img" href="{{Options::base_url()}}{{Url_mod::convert_url('proizvodjac')}}/{{Url_mod::url_convert(Product::get_proizvodjac($roba_id)) }}">
                                            <img src="{{ Options::domain() }}{{product::slikabrenda($roba_id) }}" alt="{{ product::get_proizvodjac($roba_id) }}" />
                                        </a>
                                    @else

                                    <a href="{{Options::base_url()}}{{Url_mod::convert_url('proizvodjac')}}/{{Url_mod::url_convert(Product::get_proizvodjac($roba_id)) }}">
                                        <span class="artical-brand-text">{{ product::get_proizvodjac($roba_id) }}</span>
                                    </a>

                                    @endif                                   
                                </div>
                            @endif

                            <!-- Article password -->
                                @if(AdminOptions::sifra_view_web()==1)
                                <div class="article-sifra"><b>Šifra: {{Product::sifra($roba_id)}}</b></div>
                                @elseif(AdminOptions::sifra_view_web()==4)                       
                                <div class="article-sifra">Šifra: {{Product::sifra_d($roba_id)}}</div>
                                @elseif(AdminOptions::sifra_view_web()==3)                       
                                <div class="article-sifra">Šifra: {{Product::sku($roba_id)}}</div>
                                @elseif(AdminOptions::sifra_view_web()==2)                       
                                <div class="article-sifra"><b>Šifra: {{Product::sifra_is($roba_id)}}</b></div>
                                @endif                        


                               <!--  @if($proizvodjac_id != -1)
                                   <div class="product-manufacturer" itemprop="brand">{{Language::trans('Proizvođač')}}: 
                                       @if(Support::checkBrand($roba_id))
                                       <a href="{{ Options::base_url() }}{{ Url_mod::convert_url('proizvodjac')}}/{{ Url_mod::url_convert(Product::get_proizvodjac($roba_id)) }}">
                                           {{Product::get_proizvodjac($roba_id)}}
                                       </a>
                                       @else
                                           <span class="article-manufacturer-text"> 
                                               {{Product::get_proizvodjac($roba_id)}} 
                                           </span>
                                       @endif
                                   </div>
                               @endif  -->

                              <!--   @if(Options::vodjenje_lagera() == 1)
                                    <div class="product-available-amount">
                                        
                                            {{Language::trans('Dostupna količina')}}:
                                      
                                        <span>
                                            {{Cart::check_avaliable($roba_id)}}
                                        </span> 
                                    </div>
                                @endif -->

                                @if(Options::checkTezina() == 1 AND Product::tezina_proizvoda($roba_id)>0)
                                    <div class="product-available-amount">{{Language::trans('Težina artikla')}}: 
                                        <a href="#"> 
                                            {{Product::tezina_proizvoda($roba_id)/1000}} kg
                                        </a>
                                    </div>
                                @endif
                                
                                    <!-- @if(Options::checkTags() == 1)
                                        @if(Product::tags($roba_id) != '')
                                        <li class="product-available-amount">
                                            {{Language::trans('Tagovi')}}: {{ Product::tags($roba_id) }}
                                        </li>
                                        @else
                                        <li class="product-available-amount">
                                            {{Language::trans('Tagovi')}}: {{Language::trans('Nema tagova')}}
                                        </li>
                                        @endif
                                    @endif -->
                          

                            <!-- Price -->
                            <div class="product-preview-price">
                                @if(Product::pakovanje($roba_id))
                                    <div class="product-preview-price-new" itemprop="price">
                                        <span class="PDV_price">{{Language::trans('Pakovanje')}}: </span>
                                        <span class="mpPrice">{{ Product::ambalaza($roba_id) }}</span>
                                    </div>
                                @endif                                    
                                @if(All::provera_akcija($roba_id))                                      
                                    <div class="product-preview-price-new" itemprop="price">
                                        <span class="PDV_price">
                                            {{Language::trans('MPC')}}: 
                                        </span>
                                    
                                        <span class="mpPrice">
                                            {{ Cart::cena(Product::get_mpcena($roba_id)) }}
                                        </span>
                                    </div>

                                    <div class="product-preview-price-new" itemprop="price">
                                        <span class="PDV_price">
                                            {{Language::trans('Akcijska cena:')}}
                                        </span>

                                        <span class="mpPrice">
                                            {{ Cart::cena(Product::get_price($roba_id)) }}
                                        </span>
                                    </div>

                                    @if(Product::getPopust_akc($roba_id)>0)
                                        <div class="product-preview-price-new" itemprop="price">
                                            <span class="PDV_price">{{Language::trans('Popust')}}: 
                                                
                                            </span>

                                            <span class="discount-price">
                                                {{ Cart::cena(Product::getPopust_akc($roba_id)) }}
                                            </span>
                                        </div>
                                    @endif

                                @else
                                    <div class="product-preview-price-new" itemprop="price">
                                        <span class="PDV_price">
                                            {{Language::trans('MPC')}}: 
                                        </span>

                                        <span class="mpPrice">
                                            {{ Cart::cena(Product::get_mpcena($roba_id)) }}
                                        </span>
                                    </div>

                                    <div class="product-preview-price-new" itemprop="price">
                                        <span class="PDV_price">
                                            {{Language::trans('Cena:')}} 
                                        </span>
                                        
                                        <span class="mpPrice">
                                            {{ Cart::cena(Product::get_price($roba_id)) }}
                                        </span>
                                    </div>

                                    @if(Product::getPopust($roba_id)>0)
                                    @if(AdminOptions::web_options(132)==1)
                                        <div class="product-preview-price-new">
                                            <span class="PDV_price">
                                                {{Language::trans('Popust')}}: 
                                            </span>

                                            <span class="discount-price">
                                                {{ Cart::cena(Product::getPopust($roba_id)) }}
                                            </span>
                                        </div>
                                    @endif
                                    @endif
                                @endif
                             </div> <!-- End of price -->

                            <div class="add-to-cart-area">     
                                 @if(AdminSupport::getStatusArticle($roba_id) == 1)
                                    @if(Cart::check_avaliable($roba_id) > 0)
                                    <form method="POST" action="{{ Options::base_url() }}product-cart-add" id="JSAddCartForm" class="article-form">                                      
                                        <input type="hidden" name="roba_id" value="{{ $roba_id }}">

                                        <div class="properties-div">
                                            @if(Product::check_osobine($roba_id))
                                                <div class="osobine">
                                                    @foreach(Product::osobine_nazivi($roba_id) as $osobina_naziv_id)
                                                        <div class="osobina">
                                                            <div class="osobina_naziv">{{ Product::find_osobina_naziv($osobina_naziv_id,'naziv') }}</div>
                                                            <div class="karakteristike_osobine">
                                                                @foreach(Product::osobine_vrednosti($roba_id,$osobina_naziv_id)->get() as $row2)
                                                                <label class="osobina_karakteristika" style="background-color: {{ Product::find_osobina_vrednost($row2->osobina_vrednost_id, 'boja_css') }}">
                                                                    <input type="radio" name="osobine{{ $osobina_naziv_id }}" value="{{ $row2->osobina_vrednost_id }}" {{ Product::check_osobina_vrednost($roba_id,$osobina_naziv_id,$row2->osobina_vrednost_id,Input::old('osobine'.$osobina_naziv_id)) }}>
                                                                    <span title="{{ Product::find_osobina_vrednost($row2->osobina_vrednost_id, 'vrednost') }}">
                                                                        {{ Product::osobina_vrednost_checked($osobina_naziv_id, $row2->osobina_vrednost_id) }}
                                                                    </span>
                                                                </label>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            @endif
                                        </div>
                            
                                        <div class="button-container">
                                            <input type="number" name="kolicina" class="cart-amount" value="{{ Input::old('kolicina') ? Input::old('kolicina') : '1' }}">

                                            <button id="JSAddCartSubmit" class="JSadd-to-cart atractive-comment-btn">
                                                Dodaj u korpu
                                            </button>

                                            @if(Cart::kupac_id() > 0)
                                                <div class="like-it">
                                                    <a href="#!" data-roba_id="{{$roba_id}}" title="Dodaj na listu želja" class="JSadd-to-wish wish-list">
                                                        <i class="far fa-heart"></i>
                                                    </a>
                                                </div> 
                                            @else

                                                <div class="like-it">
                                                    <a href="#!" data-roba_id="{{$roba_id}}" title="Dodavanje artikla na listu želja moguće je samo registrovanim korisnicima" class="JSnot-wish">
                                                        <i class="far fa-heart"></i>
                                                    </a>
                                                </div> 
                                            @endif
                                        </div>

                                        <div class="quantity-error">
                                            {{ $errors->first('kolicina') ? $errors->first('kolicina') : '' }}
                                        </div>  
                                    
                                    @else
                                        <div class="button-container">
                                            <button class="dodavanje not-available comment-btn">Nije dostupno</button>

                                            @if(Cart::kupac_id() > 0)
                                                <div class="like-it">
                                                    <a href="#!" data-roba_id="{{$roba_id}}" title="Dodaj na listu želja" class="JSadd-to-wish wish-list">
                                                        <i class="far fa-heart"></i>
                                                    <an>
                                                </div> 
                                            @else

                                                <div class="like-it">
                                                    <a href="#!" data-roba_id="{{$roba_id}}" title="Dodavanje artikla na listu želja moguće je samo registrovanim korisnicima" class="JSnot-wish">
                                                        <i class="far fa-heart"></i>
                                                    </a>
                                                </div> 
                                            @endif           
                                        </div>

                                        </form>

                                    @endif
                                    
                                @else
                               
                                <div class="button-container">
                                            <button class="dodavanje not-available  comment-btn" data-roba-id="{{$roba_id}}">
                                                {{ AdminSupport::find_flag_cene(AdminSupport::getStatusArticle($roba_id),'naziv') }}
                                            </button>

                                            @if(Cart::kupac_id() > 0)
                                                <div class="like-it">
                                                    <a href="#!" data-roba_id="{{$roba_id}}" title="Dodaj na listu želja" class="JSadd-to-wish wish-list">
                                                        <i class="far fa-heart"></i>
                                                    </a>
                                                </div> 
                                            @else

                                                <div class="like-it">
                                                    <a href="#!" data-roba_id="{{$roba_id}}" title="Dodavanje artikla na listu želja moguće je samo registrovanim korisnicima" class="JSnot-wish">
                                                        <i class="far fa-heart"></i>
                                                    </a>
                                                </div> 
                                            @endif           
                                        </div>
                                @endif
                            </div>

                              
                            <!-- Share button -->
                            <div class="share-div">
                                  <span>Podelite:</span>
                                  <div class="soc-network"> 
                                     <div class="fb-like" data-href="{{Options::base_url()}}{{Url_mod::convert_url('artikal')}}/{{Url_mod::url_convert(Product::seo_title($roba_id))}}" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
                                  </div>
                           
                                 <div class="soc-network"> 
                                    <a href="https://twitter.com/share" class="twitter-share-button">Tweet</a> 
                                    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                                 </div>  
                            </div> <!-- End of Share button -->

                            
                            @if($grupa_pr_id != -1)
                                <div class="product-group">
                                    <span>Kategorija:</span>
                                    {{ Product::get_grupa($roba_id) }}
                                </div>
                            @endif
                         
                       

                            <!-- Admin button -->  
                                @if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array(200)))
                            <div class="admin-article"> 
                                    <a class="article-level-edit-btn JSFAProductModalCall" data-roba_id="{{$roba_id}}" href="javascript:void(0)">
                                        {{ Language::trans('IZMENI ARTIKAL') }}
                                    </a> 

                                    <span class="supplier"> 
                                        {{ Product::get_dobavljac($roba_id) }}
                                    </span> 

                                    <span class="supplier">
                                        {{ Language::trans('NCP') }}: {{ Product::get_ncena($roba_id) }}
                                    </span>
                            </div><!-- End of Admin button -->
                                @endif


                          
                        </div> <!-- End of product preview-info -->



        <div class="col-md-12 col-sm-12 col-xs-12 product-tags"> 
            @if(Options::checkTags() == 1)
                @if(Product::tags($roba_id) != '')
                <div class="tag-content">
                    <div class="tags-title">Tagovi:</div>
                    
                    {{ Product::tags($roba_id) }} 
                   
                </div>
                @endif
            @endif    
        </div>


                        
                @include('shop/themes/'.Support::theme_path().'partials/product-preview-tab')

                @include('shop/themes/'.Support::theme_path().'partials/bound-article')

                @include('shop/themes/'.Support::theme_path().'partials/related-article')
                    
                </div> <!-- End of row -->



            </div> <!-- End of container -->
        </div> <!-- End of article-details -->
    </div> <!-- End of main-content -->
</div> <!-- End of middle-area -->
@endsection 