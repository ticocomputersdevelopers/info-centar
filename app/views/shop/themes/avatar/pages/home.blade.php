@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('baners_sliders')
    @include('shop/themes/'.Support::theme_path().'partials/baners_sliders')
@endsection

@section('page')

    <script type="text/javascript">
        function alertSuccess(message) {
            swal(message, "", "success");
                
            setTimeout(function() { 
                swal.close();  
            }, 1800);
        }
        function alertAmount(message) {
            swal(message , {
              buttons: false,
            });
                
            setTimeout(function() {
                swal.close();  
            }, 1800);
        }    
    </script>
    
    <script type="text/javascript">
        @if(Session::has('login_success'))
        alertSuccess("{{ Session::get('login_success') }}");
        @endif
        
        @if(Session::has('registration_success'))
        alertSuccess("{{ Session::get('registration_success') }}");
        @endif

        @if(Session::has('loggout_succes'))
        alertAmount("{{ Session::get('loggout_succes') }}");
        @endif

        @if(Session::has('confirm_registration_message'))
        alertAmount("{{ Session::get('confirm_registration_message') }}");
        @endif
    </script>



<div class="container">
    <div class="bw"> 
        @include('shop/themes/'.Support::theme_path().'partials/products/action_type')
        
        @if(Options::web_options(136)==0)

            @if(Options::web_options(208)==1)
  
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <h2 class="section-heading JSInlineShort" data-target='{"action":"front_admin_label","id":"1"}'>{{ Language::trans(Support::front_admin_label(1)) }}</h2>
                </div>
            </div>
            <!-- Article on front page -->
            <div class="JSMostPopularProducts JSproduct-slider">
                 
                @foreach(Articles::mostPopularArticles() as $row)
                    @include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
                @endforeach
            </div>
  
            @if(count(Articles::bestSeller()))
     
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <h2 class="section-heading JSInlineShort" data-target='{"action":"front_admin_label","id":"2"}'>{{ Language::trans(Support::front_admin_label(2)) }}</h2>
                </div>
            </div>
                  
            <div class="JSBestSellerProducts JSproduct-slider"> 
                @foreach(Articles::bestSeller(4) as $row)
                    @include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
                @endforeach
            </div>
      
            @endif
 
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <h2 class="section-heading JSInlineShort" data-target='{"action":"front_admin_label","id":"3"}'>{{ Language::trans(Support::front_admin_label(3)) }}</h2>
                </div>
            </div>

            <div class="JSMostPopularProducts JSproduct-slider">
                
                @foreach(Articles::latestAdded() as $row)
                    @include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
                @endforeach
            </div>
      
            @endif
        @else
 
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <h2 class="section-heading JSInlineShort" data-target='{"action":"home_all_articles"}'>
                    {{ Language::trans(Support::title_all_articles()) }}
                </h2> 
            </div>
        </div>

        <div class="JSproduct-slider all-articles"> 
            @foreach($articles as $row)
                @include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
            @endforeach
        </div>
   

           <!--  <div class="col-md-12 col-sm-12 col-xs-12 text-center">
               {{ Paginator::make($articles, $count_products, $limit)->links() }}
           </div> -->
        @endif   
        <br> 
      </div>
</div>

        
@endsection


@section('home_section')
    @if(Options::web_options(99) == 1 AND count(All::getShortListNews()))
        @include('shop/themes/'.Support::theme_path().'partials/section-news')
    @endif

    @if(count(All::getManufacturersShortList()) > 0)
        @include('shop/themes/'.Support::theme_path().'partials/manufacturers-listing')
    @endif

    @include('shop/themes/'.Support::theme_path().'partials/newsletter')
@endsection