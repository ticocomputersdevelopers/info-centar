@extends('shop/themes/'.Support::theme_path().'templates/products')

@section('products_list')

<div class="{{ (isset($sub_cats) && count($sub_cats)>0) ? 'col-md-12' : 'col-md-9'}} col-sm-12 col-xs-12">
	<div class="JSproduct-list-options">				
	   

			<div class="list-option-item number-of-prdocut">
				{{ Language::trans('Ukupno') }}:
				{{ $count_products }} 
			</div>
		
		 <div class="text-center">
		 	<div style="display: inline-block;">
			    @if(Options::product_number()==1)
					<div class="dropdown list-option-item">	
						 <button class="currency-btn dropdown-toggle" type="button" data-toggle="dropdown">	
						 	@if(Session::has('limit'))
							{{Session::get('limit')}}
							@else
							20
							@endif
							<span class="caret"></span>
						</button>

						<ul class="dropdown-menu currency-list">			
							<li>
								<a href="{{ Options::base_url() }}{{ Url_mod::convert_url('limit') }}/20">
									20
								</a>
							</li>
							<li>
								<a href="{{ Options::base_url() }}{{ Url_mod::convert_url('limit') }}/30">
									30
								</a>
							</li>
							<li>
								<a href="{{ Options::base_url() }}{{ Url_mod::convert_url('limit') }}/50">
									50
								</a>
							</li>			
						</ul>			 
					</div>
				@endif
			
			
		        @if(Options::product_currency()==1)
		            <div class="dropdown list-option-item">
		            	 <button class="currency-btn dropdown-toggle" type="button" data-toggle="dropdown">
		            	 	 {{Articles::get_valuta()}} 
		            	 	 <span class="caret"></span>
		            	 </button>
		                 
		                <ul class="dropdown-menu currency-list">
		                    <li>
		                    	<a href="{{ Options::base_url() }}{{ Url_mod::convert_url('valuta') }}/1">
			                    	{{ Language::trans('Din') }}
			                    </a>
			                </li>
		                    <li>
		                    	<a href="{{ Options::base_url() }}{{ Url_mod::convert_url('valuta') }}/2">
			                    	{{ Language::trans('Eur') }}
			                    </a>
			                </li>
		                </ul>
		            </div>

		        @endif
			</div>

			<div style="display: inline-block;">
	            @if(Options::product_sort()==1)
		            <div class="dropdown list-option-item"> 
		            	 <button class="currency-btn dropdown-toggle" type="button" data-toggle="dropdown">
			                 {{Articles::get_sort()}}
			                 <span class="caret"></span>
		            	</button>

		                <ul class="dropdown-menu currency-list">    
		                	@if(Options::web_options(207) == 0)
		                    <li><a href="{{ Options::base_url() }}{{ Url_mod::convert_url('sortiranje') }}/price_asc">{{ Language::trans('Cena: min') }}</a></li>
		                    <li><a href="{{ Options::base_url() }}{{ Url_mod::convert_url('sortiranje') }}/price_desc">{{ Language::trans('Cena: max') }}</a></li>
		                    @else
		                    <li><a href="{{ Options::base_url() }}{{ Url_mod::convert_url('sortiranje') }}/price_desc">{{ Language::trans('Cena: max') }}</a></li>
		                    <li><a href="{{ Options::base_url() }}{{ Url_mod::convert_url('sortiranje') }}/price_asc">{{ Language::trans('Cena: min') }}</a></li>
		                    @endif
		                    <li>
			                    	<a href="{{ Options::base_url() }}{{ Url_mod::convert_url('sortiranje') }}/news">{{ Language::trans('Najnovije') }}
			                    	</a>
			                    </li>
		                    <li>
		                    	<a href="{{ Options::base_url() }}{{ Url_mod::convert_url('sortiranje') }}/name">
			                    	{{ Language::trans('Prema nazivu') }}
			                    </a>
			                </li>
		                </ul>
		            </div>
	            @endif
		          
		       
		    	@if(Options::product_view()==1)
					@if(Session::has('list'))
						<div class="view-buttons"> 
							<a href="{{ Options::base_url() }}{{ Url_mod::convert_url('prikaz') }}/list">
								<span class="list-view-button active">
									<i class="fas fa-list-ul"></i>
								</span>
							</a>

							<a href="{{ Options::base_url() }}{{ Url_mod::convert_url('prikaz') }}/grid">
								<span class="grid-view-button">
									<i class="fas fa-th"></i>
								</span>
							</a>  		 
						</div>

					@else

					<div class="view-buttons">  
						<a href="{{ Options::base_url() }}{{ Url_mod::convert_url('prikaz') }}/list">
							<span class="grid-view-button">
								<i class="fas fa-list-ul"></i>
							</span>
						</a>

						<a href="{{ Options::base_url() }}{{ Url_mod::convert_url('prikaz') }}/grid">
							<span class="list-view-button active">
								<i class="fas fa-th"></i>
							</span>
						</a>
					 </div>
					@endif
		        @endif
	        </div>  

		</div>
	</div>

</div>

	<div class="{{ (isset($sub_cats) && count($sub_cats)>0) ? 'col-md-12' : 'col-md-9'}} col-sm-12 col-xs-12 pagination-div top">
		{{ Paginator::make($articles, $count_products, $limit)->links() }}
	</div>


	<!-- Products -->
	@if(Session::has('list') or Options::product_view()==3)

		<!-- List products -->
		<div class="JSproduct-list list-view {{ (isset($sub_cats) && count($sub_cats)>0) ? 'col-lg-12 col-md-12' : 'col-lg-9 col-md-9'}} col-sm-12 col-xs-12">
			@foreach($articles as $row)
				@include('shop/themes/'.Support::theme_path().'partials/products/product_on_list') 
			@endforeach
		</div>

		@else

		<!-- Grid proucts -->
		
		<div class="JSproduct-list {{ (isset($sub_cats) && count($sub_cats)>0) ? 'col-lg-12 col-md-12' : 'col-lg-9 col-md-9'}} col-sm-12 col-xs-12 no-padding">
			@foreach($articles as $row)
				@include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
			@endforeach
		</div>	
		
	@endif

	@if($count_products == 0)
		<div class="col-md-12 col-sm-12 col-xs-12"> 
			<div class="login-title no-articles-page bw"> 
				{{ Language::trans('Trenutno nema artikla za date kategorije') }}
			</div>
		</div>
	@endif

	<div class="JSproduct-list {{ (isset($sub_cats) && count($sub_cats)>0) ? 'col-lg-12 col-md-12' : 'col-lg-9 col-md-9 col-lg-push-2 col-md-push-2'}} col-sm-12 col-xs-12 pagination-div bottom"> 
		{{ Paginator::make($articles, $count_products, $limit)->links() }}
	</div>

@endsection