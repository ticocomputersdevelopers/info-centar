@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
@if(Session::has('message'))
<script type="text/javascript">
    $(document).ready(function(){
		swal("{{ Language::trans('Poslali smo Vam potvrdu o registraciji na e-mail koji ste uneli. Molimo Vas da potvrdite Vašu e-mail adresu klikom na link iz e-mail poruke') }}.", {
		  buttons: {
		    yes: {
		      text: "Zatvori",
		      value: true,
		    }
		  },
		}).then(function(value){ });
    });
</script>
@endif
<div class="registration-page">
	<div class="container">
		<div class="row bw">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="page-breadcrumb">
					<ul>
						<li>
							<a href="pocetna">
								{{ Language::trans('Početna / ') }}
							</a>
						</li>
						<li>
							<a href="#!">
								{{ Language::trans('Registracija') }}
							</a>
						</li>
					</ul>
				</div>

				<h2 class="page-heading">
					{{ Language::trans('Registrujte se') }}
				</h2>
			</div>
		</div>

		<div class="row bw">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div>
					<p class="login-title">
						Registrujte svoj nalog kao 
						@if(Input::old('flag_vrsta_kupca') == 1)
						<span class="private-user">fizicko lice</span>, kao 
						<span class="active-user company-user">pravno lice</span>
						@else
						<span class="active-user private-user">fizicko lice</span>, kao 
						<span class="company-user">pravno lice</span>
						@endif
					</p>
				</div>
			</div>
		</div>

		<div class="row bw">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div>
					<form action="{{ Options::base_url()}}registracija-post" method="post" class="registration-form" autocomplete="off">
						<div class="frm-control">
							<input type="hidden" name="flag_vrsta_kupca" value="0">
						</div>
						<div class="frm-control-div">		
							<div class="frm-control">
								<label for="ime">
									Ime
									<span class="label-star">*</span>				
								</label>
								<input id="ime" name="ime" type="text" value="{{ htmlentities(Input::old('ime') ? Input::old('ime') : '') }}" >
								<div class="error red-dot-error">
								    {{ $errors->first('ime') ? $errors->first('ime') : "" }}
							 	</div>
							</div>

							<div class="frm-control">
								<label for="prezime">
									Prezime
									<span class="label-star">*</span>
								</label>
								<input id="prezime" name="prezime" type="text" value="{{ Input::old('prezime') ? Input::old('prezime') : '' }}" >
								<div class="error red-dot-error">
									{{ $errors->first('prezime') ? $errors->first('prezime') : "" }}
								</div>
							</div>
						</div>
						<div class="frm-control-div">	
							<div class="frm-control">
								<label for="naziv">
									Naziv firme
									<span class="label-star">*</span>
								</label>
								<input id="naziv" name="naziv" type="text" value="{{ htmlentities(Input::old('naziv') ? Input::old('naziv') : '') }}" >
								<div class="error red-dot-error">
									{{ $errors->first('naziv') ? $errors->first('naziv') : "" }}
								</div>
							</div>
							<div class="frm-control">
								<label for="pib">
									PIB
									<span class="label-star">*</span>
								</label>
								<input id="pib" name="pib" type="text" value="{{ Input::old('pib') ? Input::old('pib') : '' }}" >
								<div class="error red-dot-error">
									{{ $errors->first('pib') ? $errors->first('pib') : "" }}
								</div>
							</div>
						</div>
						<div class="frm-control-div">				
							<div class="frm-control">
								<label for="email">
									E-mail
									<span class="label-star">*</span>
								</label>
								<input id="email" autocomplete="off" name="email" type="text" value="{{ Input::old('email') ? Input::old('email') : '' }}" >
								<div class="error red-dot-error">
									{{ $errors->first('email') ? $errors->first('email') : "" }}
								</div>
							</div>						
							<div class="frm-control">
								<label for="lozinka">
									Lozinka
									<span class="label-star">*</span>
								</label>
								<input id="lozinka" name="lozinka" type="password" value="{{ htmlentities(Input::old('lozinka') ? Input::old('lozinka') : '') }}">
								<div class="error red-dot-error">
									{{ $errors->first('lozinka') ? $errors->first('lozinka') : "" }}
								</div>
							</div>
						</div>
						<div class="frm-control-div">	
							<div class="frm-control">
								<label for="telefon">
									Telefon
									<span class="label-star">*</span>
								</label>
								<input id="telefon" name="telefon" type="text" value="{{ Input::old('telefon') ? Input::old('telefon') : '' }}" >
								<div class="error red-dot-error">
									{{ $errors->first('telefon') ? $errors->first('telefon') : "" }}
								</div>
							</div>

							<div class="frm-control">
								<label for="adresa">
									Adresa
									<span class="label-star">*</span>
								</label>
								<input id="adresa" name="adresa" type="text" value="{{ htmlentities(Input::old('adresa') ? Input::old('adresa') : '') }}" >
								<div class="error red-dot-error">
									{{ $errors->first('adresa') ? $errors->first('adresa') : "" }}
								</div>
							</div>	
							<div class="frm-control">
								<label for="mesto">
									Mesto
									<span class="label-star">*</span>
								</label>
								<input id="mesto" name="mesto" type="text" value="{{ htmlentities(Input::old('mesto') ? Input::old('mesto') : '') }}" >
								<div class="error red-dot-error">
									{{ $errors->first('mesto') ? $errors->first('mesto') : "" }}
								</div>
							</div>
						</div>
						<div> 
							<button type="submit" class="atractive-comment-btn">
								{{ Language::trans('Registruj se') }}
							</button>
						</div>
					</form>

					<div class="fusnote">
						<span class="label-star">*</span>
						Obavezna polja
					</div>
				</div>
			</div><br>
		</div> 
	</div>
</div>
@endsection 