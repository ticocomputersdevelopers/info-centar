@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')

<div class="container">
	<div class="cart-page bw"> 
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="page-breadcrumb">
					<ul>
						<li>
							<a href="pocetna">
								Početna /
							</a>
						</li>
						<li>
							<a href="#!">
								Korpa
							</a>
						</li>
					</ul>
				</div>

				<h2 class="page-heading">
					{{ Language::trans('Vaša Korpa') }}
				</h2>
			</div>
		</div>

	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			@if(Session::has('b2c_korpa') and Cart::broj_cart() >= 1 )
			<div class="cart">

				@if(Session::has('intesa_failure'))
					<h2>{{ Language::trans('Žao nam je, transakcija nije uspela. Pokušajte ponovo.') }}</h2>
				@endif
				<ul class="cart-labels clearfix">
					<li class="col-md-3 col-sm-3 col-sm-offset-2 col-md-offset-2">{{ Language::trans('Proizvod') }}</li>			 
					<li class="col-md-2 col-sm-2">{{ Language::trans('Cena') }}</li>
					<li class="col-md-2 col-sm-2">{{ Language::trans('Količina') }}</li>
					<li class="col-md-2 col-sm-2">{{ Language::trans('Ukupno') }}</li>
					<li class="col-md-1 col-sm-1">{{ Language::trans('Ukloni') }}</li>		 
				</ul>
				@foreach(DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_id',Session::get('b2c_korpa'))->orderBy('web_b2c_korpa_stavka_id','asc')->get() as $row)
				<ul class="JScart-item clearfix">	 
					<li class="JScart-image col-md-2 col-sm-2 col-xs-12">
						<img src="{{ Options::domain() }}{{Product::web_slika($row->roba_id) }}" alt="{{ Product::short_title($row->roba_id) }}" class="cart-image img-responsive" />
					</li>
					<li class="cart-name col-md-3 col-sm-3 col-xs-12">
						<a class="" href="{{ Options::base_url() }}{{Url_mod::convert_url('artikal')}}/{{ Url_mod::url_convert(Product::seo_title($row->roba_id)) }}">
						{{ Product::short_title($row->roba_id) }}
							<div>
								{{Product::getOsobineStr($row->roba_id,$row->osobina_vrednost_ids)}}  
							</div>
						</a>
						
					</li>

					<li class="cart-price col-md-2 col-sm-2 col-xs-12">{{ Cart::cena($row->jm_cena) }}</li>
					<li class="col-md-2 col-sm-2 col-xs-12 flex-me">
						<div class="cart-add-amount clearfix">
							<a data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}" class="JScart-less" href="javascript:void(0)"><</a>
							<input type="text" class="JScart-amount" value="{{ round($row->kolicina) }}" onkeypress="validate(event)" data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}">
							<a data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}" class="JScart-more" href="javascript:void(0)">></a>
						</div>
					</li>
					<li class="cart-total-price col-md-2 col-sm-2 col-xs-12">{{ Cart::cena($row->jm_cena*$row->kolicina) }}</li>
					<li class="cart-remove col-md-1 col-sm-1 col-xs-12">
						<a href="javascript:void(0)" data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}" class="JSDeleteStavka">
							<i class="fas fa-times"></i>
						</a>
					</li>			 
				 </ul>
				@endforeach

			</div>
		</div>
	</div>

	<!-- BELOW CART -->
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="cart-btn-div"> 
				<button class="comment-btn" id="JSDeleteCart">{{ Language::trans('Isprazni korpu') }}</button>
			</div>	
   		</div>
    </div>

    <div class="row"> 
    	<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="amount-section">
				<h2 class="amount-heading">{{ Language::trans('Iznos narudžbine') }}</h2>
				<div class="amount-div">
					@if(Options::checkTezina() == 1 AND Cart::troskovi_isporuke()>0)
						<div>
							<span class="description">{{ Language::trans('Cena artikala') }} : </span>
							<span class="value">{{Cart::cena(Cart::cart_ukupno())}}</span>
						</div>

						<div>
							<span class="description">{{ Language::trans('Troškovi isporuke') }} : </span>
							<span class="value">{{Cart::cena(Cart::troskovi_isporuke())}}</span>
						</div>

						<div>
							<span class="description">{{ Language::trans('Ukupno') }} : </span>
							<span class="value">{{Cart::cena(Cart::cart_ukupno()+Cart::troskovi_isporuke())}}</span>
						</div>
						@elseif(Options::checkTroskoskovi_isporuke() == 1 AND Options::checkTezina() == 0 AND Cart::cart_ukupno() < Cart::cena_do() AND Cart::cena_do() > 0)
						<div>
							<span class="description">{{ Language::trans('Cena artikala') }}: </span>
							<span class="value">{{Cart::cena(Cart::cart_ukupno())}}</span>
						</div>
						<div>
							<span class="description">{{ Language::trans('Troškovi isporuke') }}: </span>
							<span class="value">{{Cart::cena(Cart::cena_dostave())}}</span>
						</div>
						<div>
							<span class="description">{{ Language::trans('Ukupno') }}: </span>
							<span class="value">{{Cart::cena(Cart::cart_ukupno()+Cart::cena_dostave())}}</span>
						</div>
					@else

						<div>
							<span class="description">{{ Language::trans('Ukupno') }}: </span>
							<span class="value">{{Cart::cena(Cart::cart_ukupno())}}</span>
						</div>
					@endif
				</div>
			</div>
		</div>
	</div>

		<!-- Cart action buttons -->
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12" id="cart_form_scroll">

				@if(Options::user_registration()==1)
					@if(!Session::has('b2c_kupac'))
						<div class="without-registration-btn">
							<div>
								<button class="comment-btn" id="JSRegToggle">{{ Language::trans('Kupi bez registracije') }}</button>
							</div>

							<div>
								<a class="comment-btn" href="#" role="button" id="login-icon" data-toggle="modal" data-target="#loginModal">{{ Language::trans('Prijavi se') }}</a>
							</div>
						</div>
					@endif


			 	<!-- Buy without registration form -->
				<div class="without-registration" id="JSRegToggleSec" {{ Session::has('b2c_kupac') ? "" : (count(Input::old()) == 0 ? "hidden='hidden'" : "") }}> 

						@if(Options::neregistrovani_korisnici()==1)

						<form method="POST" action="{{ Options::base_url() }}order-create" id="JSOrderForm" class="without-reg-form">

								@if(!Session::has('b2c_kupac'))

								<div class="chose-user-cart">

								<div class="frm-control-div">
									
									@if(Input::old('flag_vrsta_kupca') == 1)
									<div class="frm-control">
										<div class="JSCheckVrsta type-of-buyer personal" data-vrsta="personal">
											{{ Language::trans('Fizičko Lice') }} <i class="fa fa-check"></i> 
										</div>
									</div>
									<div class="frm-control">
										<div class="JSCheckVrsta type-of-buyer active" data-vrsta="non-personal">
											{{ Language::trans('Pravno Lice') }} <i class="fa fa-check"></i>
										</div>
									</div>
									@else
								

									<div class="frm-control">
										<div class="JSCheckVrsta type-of-buyer active " data-vrsta="personal">
											{{ Language::trans('Fizičko Lice') }} <i class="fa fa-check"></i>
										</div>
									</div>
									
									<div class="frm-control">
										<div class="JSCheckVrsta type-of-buyer none-personal " data-vrsta="non-personal">
											{{ Language::trans('Pravno Lice') }} <i class="fa fa-check"></i>
										</div>
									</div>
									@endif
									
								</div>

								<input type="hidden" name="flag_vrsta_kupca" value="{{ Input::old('flag_vrsta_kupca') == 1 ? '1' : '0' }}">
				
									<div class="frm-control-div"> 
										<div class="frm-control JSwithout-reg-none-personal {{ Input::old('flag_vrsta_kupca') == 1 ? 'active' : '' }}">
											<label for="without-reg-company">
												<span class="label-star">*</span> 
												{{ Language::trans('Naziv Firme') }}:
												<div class="error red-dot-error">
													{{ $errors->first('naziv') ? $errors->first('naziv') : "" }}
												</div>
											</label>
											<input id="without-reg-company" name="naziv" type="text" tabindex="1" value="{{ htmlentities(Input::old('naziv') ? Input::old('naziv') : '') }}" placeholder="Naziv firme">
										</div>

										<div class="frm-control JSwithout-reg-none-personal {{ Input::old('flag_vrsta_kupca') == 1 ? 'active' : '' }}">
											<label for="without-reg-pib">
												{{ Language::trans('PIB') }}:
												<div class="error red-dot-error">
													{{ $errors->first('pib') ? $errors->first('pib') : "" }}
												</div>
											</label>
											<input id="without-reg-pib" name="pib" type="text" tabindex="2" value="{{ htmlentities(Input::old('pib') ? Input::old('pib') : '') }}" placeholder="PIB">	
										</div>
									</div><!-- End of frm-control div -->

									<div class="frm-control-div"> 
										<div class="frm-control JSwithout-reg-personal {{ Input::old('flag_vrsta_kupca') == 1 ? '' : 'active' }}">
											<label for="without-reg-name">
												<span class="label-star">*</span> 
												{{ Language::trans('Ime') }}
												<div class="error red-dot-error">
													{{ $errors->first('ime') ? $errors->first('ime') : "" }}
												</div>
											</label>
											<input id="without-reg-name" name="ime" type="text" tabindex="1" value="{{ htmlentities(Input::old('ime') ? Input::old('ime') : '') }}" placeholder="Ime">
										</div>

										<div class="frm-control JSwithout-reg-personal {{ Input::old('flag_vrsta_kupca') == 1 ? '' : 'active' }}">
											<label for="without-reg-surname">
												<span class="label-star">*</span> 
												{{ Language::trans('Prezime') }}
												<div class="error red-dot-error">
													{{ $errors->first('prezime') ? $errors->first('prezime') : "" }}
												</div>
											</label>
											<input id="without-reg-surname" name="prezime" type="text" tabindex="2" value="{{ htmlentities(Input::old('prezime') ? Input::old('prezime') : '') }}" placeholder="Prezime">
										</div>
									</div><!-- End of frm-control div -->

									<div class="frm-control-div">
										<div class="frm-control"> 
											<label for="without-reg-phone">
												<span class="label-star">*</span> 
												{{ Language::trans('Telefon') }}
												<div class="error red-dot-error">
													{{ $errors->first('telefon') ? $errors->first('telefon') : "" }}
												</div>
											</label>
											<input id="without-reg-phone" name="telefon" type="text" tabindex="3" value="{{ htmlentities(Input::old('telefon') ? Input::old('telefon') : '') }}" placeholder="Telefon">
										</div>

										<div class="frm-control"> 
											<label for="without-reg-e-mail">
												<span class="label-star">*</span> E-mail
												<div class="error red-dot-error">
													{{ $errors->first('email') ? $errors->first('email') : "" }}
												</div>
											</label>
											<input id="JSwithout-reg-email" name="email" type="text" tabindex="4" value="{{ htmlentities(Input::old('email') ? Input::old('email') : '') }}" placeholder="E-mail">
										</div>
									</div><!-- End of frm-control div -->

									<div class="frm-control-div">
										<div class="frm-control"> 
											<label for="without-reg-address">
												<span class="label-star">*</span> 
												{{ Language::trans('Adresa za dostavu') }}
												<div class="error red-dot-error">
													{{ $errors->first('adresa') ? $errors->first('adresa') : "" }}
												</div>
											</label>
											<input id="without-reg-address" name="adresa" type="text" tabindex="5" value="{{ htmlentities(Input::old('adresa') ? Input::old('adresa') : '') }}" placeholder="Adresa za dostavu">
										</div>

										<div class="frm-control"> 
											<label for="without-reg-city">
												<span class="label-star">*</span> 
												{{ Language::trans('Mesto') }}/{{ Language::trans('Grad') }}
												<div class="error red-dot-error">
													{{ $errors->first('mesto') ? $errors->first('mesto') : "" }}
												</div>
											</label>		
											<input type="text" name="mesto" tabindex="6" placeholder="Mesto" value="{{ htmlentities(Input::old('mesto') ? Input::old('mesto') : '') }}">
										</div>

										<!-- <div class="col-md-3"> 
										<label>Postanski broj:</label>
										<input type="text" name="postanski_broj">
										</div> -->
									</div><!-- End of row -->

								</div>

								@endif


								<div class="frm-control-div"> 
									<div class="frm-control">
										<label>{{ Language::trans('Način isporuke') }}:</label>
										<select name="web_nacin_isporuke_id" tabindex="7">
											{{Order::nacin_isporuke(Input::old('web_nacin_isporuke_id'))}}
										</select>
									</div>

									<div class="frm-control">
										<label>{{ Language::trans('Način plaćanja') }}:</label>
										<select name="web_nacin_placanja_id" tabindex="8">
											{{Order::nacin_placanja(Input::old('web_nacin_placanja_id'))}}
										</select>
									</div>
								</div>

								<div class="frm-control-div" style="align-items: flex-start;-webkit-align-items: flex-start;"> 
									<div class="frm-control"> 
										<label>{{ Language::trans('Napomena') }}:</label>
										<textarea rows="5" tabindex="9" name="napomena">{{ htmlentities(Input::old('napomena') ? Input::old('napomena') : '') }}</textarea>
									</div>

									<div class="frm-control" id="JSCaptcha" {{(Input::old('web_nacin_placanja_id') == 3 ? '' : 'hidden')}}> 
										<div class="capcha">
											{{ Captcha::img(5, 160, 50) }}<br>
											<span>{{ Language::trans('Unesite kod sa slike') }}</span>
											<input type="text" name="captcha-string" class="form-control" tabindex="10" autocomplete="off">
											<div class="error red-dot-error">{{ $errors->first('captcha') ? $errors->first('captcha') : "" }}</div>
										 </div>
									</div>
								</div>

								<div class="frm-control-div">
									<div class="frm-control"> 
										<span class="label-star">*</span> 
										{{ Language::trans('Obavezna polja') }}
									</div>
								</div>


							<div class="text-center">
								<button id="JSOrderSubmit" class="atractive-comment-btn">{{ Language::trans('Završi kupovinu') }}</button>
							</div>

						</form>	

					@endif

				</div>
				 
				@endif

				@else
					<div>
						<p class="login-title">
							{{ Language::trans('Trenutno nemate artikle u korpi') }}
						</p>
					</div>
				@endif

			</div> <!-- End of without registration div -->

		</div> <!-- End of row -->

	</div> <!-- End of container -->
</div> <!-- End of page -->
 
@endsection