

@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')

 
	<div class="container">
		<div class="news-page bw">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="page-breadcrumb">
					<ul>
						<li>
							<a href="pocetna">
								Početna /
							</a>
						</li>
						<li>
							<a href="#!">
								Blog
							</a>
						</li>
					</ul>
				</div>

				<h2 class="page-heading">
					{{ Language::trans('Blog') }}
				</h2>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 blog-flex">
				@foreach(All::getNews() as $row)
					<div class="single-news-on-page">
						<a class="image-div" href="{{ Options::base_url() }}{{ Url_mod::convert_url('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}" style="background-image: url('{{ Options::domain() }}{{ $row->slika }}');">
							<!-- <img src="{{ Options::domain() }}{{ $row->slika }}" alt="{{ $row->naslov }}" /> -->
						</a>

						<div class="single-news-content">
							<!-- <div class="news-category space-between">
								<a href="#!">Business</a>
								<div>
									<a href="#!" class="like">
										<i class="far fa-heart"></i>
										<span>10</span>
									</a>
							
									<a href="#!" class="comment">
									<i class="far fa-comments"></i>
									<span>1</span>
									</a>
								</div>
							</div> -->
							
							<a class="title" href="{{ Options::base_url() }}{{ Url_mod::convert_url('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}">
								{{ $row->naslov }}
							</a>

							<span class="create-news-date">{{ Support::date_convert($row->datum) }}</span>
							
							<div class="short-description">{{ All::shortNewDesc($row->tekst) }}</div>

							<div class="more-news-div">
								<a href="{{ Options::base_url() }}{{ Url_mod::convert_url('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}">{{ Language::trans('Pročitaj više') }}
								<i class="fas fa-chevron-right"></i>
								</a>
							</div>
						</div>
					</div>
				@endforeach
			</div>
		</div>
	</div>
</div>
@endsection