@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
 
<div class="container">
	<div class="wish-page bw">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="page-breadcrumb">
					<ul>
						<li>
							<a href="pocetna">
								{{ Language::trans('Početna / ') }}
							</a>
						</li>
						<li>
							<a href="#!">
								{{ Language::trans('Lista želja') }}
							</a>
						</li>
					</ul>
				</div>

				<h2 class="page-heading">
					{{ Language::trans('Lista želja') }}
				</h2>
			</div>
		</div>

		@if(count(All::getWishIds($web_kupac->web_kupac_id)) > 0)
		
		<div class="row">
		@foreach(All::getWishIds($web_kupac->web_kupac_id) as $row)
			<div class="JSproduct col-md-3 col-sm-4 col-xs-12"> 		
				<div class="shop-product-card">  

					<div class="multiple-function-div">
						@if(Cart::kupac_id() > 0)
			  				<div>
			  					<span data-roba_id="{{$row->roba_id}}" title="Dodaj na listu želja" class="JSadd-to-wish wish-list">	
			  						<i class="far fa-heart"></i>
			  					</span>
			  				</div> 
			  			@else
			  				<div>
			  					<span data-roba_id="{{$row->roba_id}}" title="Dodavanje artikla na listu želja moguće je samo registrovanim korisnicima" class="wish-list">	
			  						<i class="far fa-heart"></i>
			  					</span>
			  				</div>
			  			@endif

						<div>
							<span type="button" class="JSQuickViewButton" data-roba_id="{{$row->roba_id}}">
								<i class="fas fa-search"></i>
							</span>
						</div>
					</div>

					<!-- SALE PRICE -->
					@if(All::provera_akcija($row->roba_id))
						<div class="sale-label">
							{{ Language::trans('Akcija') }}
							<!-- <span class="for-sale-price">- {{ Product::getSale($row->roba_id) }} din</span> -->
						</div>
					@endif

					<div class="product-image-wrapper">
						<a href="{{Options::base_url()}}{{Url_mod::convert_url('artikal')}}/{{Url_mod::url_convert(Product::seo_title($row->roba_id))}}">

							<img class="product-image img-responsive" src="{{ Options::domain() }}{{ Product::web_slika($row->roba_id) }}" alt="{{ Product::seo_title($row->roba_id) }}" />

							<img class="second-product-image" src="{{ Options::domain() }}{{ Product::web_slika_second($row->roba_id) }}" >
						</a>
					</div>
			   
					<div class="product-meta"> 

						<div>
							<a class="product-name text-left" href="{{Options::base_url()}}{{Url_mod::convert_url('artikal')}}/{{Url_mod::url_convert(Product::seo_title($row->roba_id))}}">
								<h2> 
									{{ Product::short_title($row->roba_id) }} 
								</h2>
							</a>
						</div>

						<div class="price-holder">
							<div> 
					            <span class="product-price"> 
					            	{{ Cart::cena(Product::get_price($row->roba_id)) }} 
					            </span>

					            @if(All::provera_akcija($row->roba_id))
						            <span class="product-old-price">
						            	{{ Cart::cena(Product::old_price($row->roba_id)) }}
						            </span>
					            @endif
				            </div>
				        </div>	 

		            	<div class="text-center"> 
				             <span class="review"> 
								{{ Product::getRating($row->roba_id) }}
				            </span>
				        </div>

						<div class="add-to-cart-container">  
					     	@if(AdminSupport::getStatusArticle($row->roba_id) == 1)	
								@if(Cart::check_avaliable($row->roba_id) > 0)	 
									<div data-roba_id="{{$row->roba_id}}" title="{{ Language::trans('Dodaj u korpu') }}" class="dodavnje JSadd-to-cart buy-btn">
										{{ Language::trans('Dodaj u korpu') }}			 
							        </div>

							        <button data-roba_id="{{$row->roba_id}}" title="Ukloni" class="dodavnje JSukloni">
							        	{{Language::trans('Ukloni')}} 
							        </button> 			    
								@else  	
					                <div class="dodavnje not-available buy-btn" title="{{ Language::trans('Nije dostupno') }}"> 
					                	{{ Language::trans('Nije dostupno') }}
					                </div>

							        <button data-roba_id="{{$row->roba_id}}" title="Ukloni" class="dodavnje JSukloni">
							        	{{Language::trans('Ukloni')}} 
							        </button>         	                            
								@endif

								@else  
									<div class="dodavanje not-available buy-btn">
										{{ AdminSupport::find_flag_cene(AdminSupport::getStatusArticle($row->roba_id),'naziv') }} 	
							 	 	</div>
			 	 
							 	 	<button data-roba_id="{{$row->roba_id}}" title="Ukloni" class="dodavnje JSukloni">
							 	 		{{Language::trans('Ukloni')}} 
							 	 	</button>		 	
							  @endif 	
						  </div>             
					</div>	 
				</div>
			</div>
		 	@endforeach
		</div>
		 

			@else
				<div class="row">
					<div class="col-md-12">
						<div class="login-title">
							{{ Language::trans('Lista želja je prazna') }}
						</div>
					</div>
				</div>
			@endif
	</div>
</div>
@endsection