@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')

<div class="new-content">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-lg-12">
				{{ $content}}
			</div>
		</div>
	</div>
</div>

@endsection