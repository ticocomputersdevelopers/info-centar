@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
@if(Session::has('message'))
<script type="text/javascript">
    $(document).ready(function(){
		swal("{{ Language::trans('Poslali smo Vam potvrdu o registraciji na e-mail koji ste uneli. Molimo Vas da potvrdite Vašu e-mail adresu klikom na link iz e-mail poruke') }}.", {
		  buttons: {
		    yes: {
		      text: "Zatvori",
		      value: true,
		    }
		  },
		}).then(function(value){ });
    });
</script>
@endif
<div class="registration-page">
	<div class="container">

		<div class="row">
			<div class="col-md-12">
				<div>
					<p class="title">
						{{ Language::trans('Registrujte se') }} <br>
						
						<span class="login-title">
							kao
							@if(Input::old('flag_vrsta_kupca') == 1)
							<span class="private-user">fizičko lice</span>, kao 
							<span class="active-user company-user">pravno lice</span>
							@else
							<span class="active-user private-user">fizičko lice</span>, kao 
							<span class="company-user">pravno lice</span>
							@endif
						</span>
					</p>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div>
					<form action="{{ Options::base_url()}}registracija-post" method="post" class="registration-form" autocomplete="off">
						<div class="frm-control">
							<input type="hidden" name="flag_vrsta_kupca" value="0">
						</div>
						<div class="frm-control-div">		
							<div class="frm-control">
								<label for="ime">
									Ime
									<div class="error red-dot-error">
										{{ $errors->first('ime') ? $errors->first('ime') : "" }}
									</div>
								</label>
								<input name="ime" type="text" value="{{ htmlentities(Input::old('ime') ? Input::old('ime') : '') }}" >
							</div>

							<div class="frm-control">
								<label for="prezime">
									Prezime
									<div class="error red-dot-error">
										{{ $errors->first('prezime') ? $errors->first('prezime') : "" }}
									</div>
								</label>
								<input name="prezime" type="text" value="{{ Input::old('prezime') ? Input::old('prezime') : '' }}" >
							</div>
						</div>
						<div class="frm-control-div">	
							<div class="frm-control">
								<label for="naziv">
									Naziv firme
									<div class="error red-dot-error">
										{{ $errors->first('naziv') ? $errors->first('naziv') : "" }}
									</div>
								</label>
								<input name="naziv" type="text" value="{{ htmlentities(Input::old('naziv') ? Input::old('naziv') : '') }}" >
							</div>
							<div class="frm-control">
								<label for="pib">
									PIB
									<div class="error red-dot-error">
										{{ $errors->first('pib') ? $errors->first('pib') : "" }}
									</div>
								</label>
								<input name="pib" type="text" value="{{ Input::old('pib') ? Input::old('pib') : '' }}" >
							</div>
						</div>
						<div class="frm-control-div">				
							<div class="frm-control">
								<label for="email">
									E-mail
									<div class="error red-dot-error">
										{{ $errors->first('email') ? $errors->first('email') : "" }}
									</div>
								</label>
								<input autocomplete="off" name="email" type="text" value="{{ Input::old('email') ? Input::old('email') : '' }}" >
							</div>						
							<div class="frm-control">
								<label for="lozinka">
									Lozinka
									<div class="error red-dot-error">
										{{ $errors->first('lozinka') ? $errors->first('lozinka') : "" }}
									</div>
								</label>
								<input name="lozinka" type="password" value="{{ htmlentities(Input::old('lozinka') ? Input::old('lozinka') : '') }}">
							</div>
						</div>
						<div class="frm-control-div">	
							<div class="frm-control">
								<label for="telefon">
									Telefon
									<div class="error red-dot-error">
										{{ $errors->first('telefon') ? $errors->first('telefon') : "" }}
									</div>
								</label>
								<input name="telefon" type="text" value="{{ Input::old('telefon') ? Input::old('telefon') : '' }}" >
							</div>

							<div class="frm-control">
								<label for="adresa">
									Adresa
									<div class="error red-dot-error">
										{{ $errors->first('adresa') ? $errors->first('adresa') : "" }}
									</div>
								</label>
								<input name="adresa" type="text" value="{{ htmlentities(Input::old('adresa') ? Input::old('adresa') : '') }}" >
							</div>	
							<div class="frm-control">
								<label for="mesto">
									Mesto
									<div class="error red-dot-error">
										{{ $errors->first('mesto') ? $errors->first('mesto') : "" }}
									</div>
								</label>
								<input name="mesto" type="text" value="{{ htmlentities(Input::old('mesto') ? Input::old('mesto') : '') }}" >
							</div>
						</div>
						<div class="text-right"> 
							<button type="submit" class="orange-btn">
								{{ Language::trans('Registruj se') }}
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection 