@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('baners_sliders')
    @include('shop/themes/'.Support::theme_path().'partials/baners_sliders')
@endsection

@section('page')

    
    @if(Options::web_options(136)==0)
         
		@include('shop/themes/'.Support::theme_path().'partials/products/action_type')
	      

		@include('shop/themes/'.Support::theme_path().'partials/newsletter')

		@if(Options::web_options(99) == 1)
			@include('shop/themes/'.Support::theme_path().'partials/section-news')
		@endif

	@else

		<script type="text/javascript">
			$('.JSScrolDown').attr('href', '#JSAllarticle');
		</script>

		<div class="container all-articles-contnet" id="JSAllarticle">
	        <div class="row">
	        	<div class="col-md-12 text-center">
	        		<h2 class="action-heading JSInlineShort" data-target='{"action":"home_all_articles"}'>
	            		{{ Language::trans(Support::title_all_articles()) }}
	            	</h2> 
	            </div>
	        </div>

	        <div class="JSproduct-slider all-articles"> 
	            @foreach($articles as $row)
	                @include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
	            @endforeach
	        </div>

	        <div class="col-md-12 col-sm-12 col-xs-12 text-center JSArticlePagination" style="padding-bottom: 40px;">
	           {{ Paginator::make($articles, $count_products, $limit)->links() }}
	       	</div> 
		</div>
	
	@endif 

	<script type="text/javascript">

		if($($('.JSArticlePagination .pagination').find('li')).length < 1) {
			$('.JSArticlePagination').css('padding-bottom', '0px');
		}

		function alertSuccess(message) {
			swal(message, "", "success");
				
			setTimeout(function() { 
				swal.close();  
			}, 1800);
		}
		function alertAmount(message) {
			swal(message , {
			  buttons: false,
			});
				
			setTimeout(function() {
				swal.close();  
			}, 1800);
		}	

		/* Block slick slide */
		$(document).ready(function () { 
			$('.multiple-items-news').slick({
			  	infinite: true,
			  	slidesToShow: 3,
			  	slidesToScroll: 2,
			  	autoplay: true,
					autoplaySpeed: 3000,
					speed: 2000,
			responsive: [
				{
				breakpoint: 1100,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
				  }
				},
				{
				  breakpoint: 800,
				  settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
					autoplaySpeed: 1500,
						speed: 1500
				  }
				},
				{
				breakpoint: 580,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					autoplaySpeed: 1200,
						speed: 800
				  }
				}
			]
			});

		});
</script>


	


	@if(Options::web_options(114) == 1) 
	<script type="text/javascript">
		if($('#admin-menu').length > 0) {
			$( document ).ready(function() {
			  $('.JSScrolDown').on('click', function(e) {
			    e.preventDefault();

			    $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top - $('header').height() - $('#admin-menu').height() - 36}, 500, 'linear');  	
			    
			  });
			});
		} else {
			$( document ).ready(function() {
			  $('.JSScrolDown').on('click', function(e) {
			    e.preventDefault();

			    $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top - $('header').height() - 20}, 500, 'linear');  	
			    
			  });
			});
		}
	</script>
	@else

	<script type="text/javascript">
			$( document ).ready(function() {
			  $('.JSScrolDown').on('click', function(e) {
			    e.preventDefault();

			    $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top}, 500, 'linear');  	
			    
			  });
			});
	</script>
	@endif

	
	<script type="text/javascript">
		$( document ).ready(function() {
			@if(Session::has('login_success'))
			alertSuccess("{{ Session::get('login_success') }}");
			@endif

			@if(Session::has('registration_success'))
			alertSuccess("{{ Session::get('registration_success') }}");
			@endif

			@if(Session::has('loggout_succes'))
			alertAmount("{{ Session::get('loggout_succes') }}");
			@endif
			
    		@if(Session::has('confirm_registration_message'))
            alertAmount("{{ Session::get('confirm_registration_message') }}");
    		@endif
    	});
	</script>
    
@endsection