
<div class="action-type-section" id="JSshift_right"> 
    <div class="container">
            @if(All::broj_akcije() > 0 AND Options::web_options(98) == 1)
            <div class="action-type-div">
                <div class="row">
                    <div class="col-lg-12">
                        @if(Session::has('b2c_admin'.Options::server()))
                        <h2 class="action-heading JSInlineShort" data-target='{"action":"sale","id":"34"}'>
                           {{Language::trans(Support::title_akcija())}}
                        </h2>
                        @else
                        <h2 class="action-heading">
                            <a class="" href="{{ Options::base_url() }}akcija">{{Language::trans(Support::title_akcija())}}</a>
                        </h2>
                        @endif
                    </div>
                </div>         
                <div class="row">
            
                    <div class="sale-list JSproduct-list row slider JSslide-articles">
                        <div class="loader-wrapper wide js-tip-loader min-height js-loader-akcija">
                            <div class="loader"></div>
                        </div>

                        <button class="previous btn-prev action-btn slider-prev" id="JSakcija-previous" data-id="akcija" data-offset="1">
                            
                        </button>

                        <button class="next btn-next action-btn slider-next" id="JSakcija-next" data-id="akcija" data-offset="1">
                           
                        </button>

                        <div class="ajax-content row" id="JSakcija-content" data-tipid="akcija">
                            <!-- Product ajax content -->
                        </div>
         
                    </div>
                   
                </div>
            @endif
        
            <?php $tipovi = DB::table('tip_artikla')->where('tip_artikla_id','<>',-1)->where('active',1)->orderBy('rbr','asc')->get(); ?>
        </div>
    </div>
    @include('shop/themes/'.Support::theme_path().'partials/banners')

    <div class="container">
        @if(count($tipovi) > 0) 
            @foreach($tipovi as $tip)
                @if(All::provera_tipa($tip->tip_artikla_id))
                
                    <div class="custom-action-div">

                        <div class="row">
                            <div class="col-lg-12">
                            @if(Session::has('b2c_admin'.Options::server()))
                            <h2 class="action-heading JSInlineShort" data-target='{"action":"type","id":"{{$tip->tip_artikla_id}}"}'>
                                {{ Language::trans($tip->naziv) }}
                            </h2>
                            @else
                            <h2 class="action-heading">
                                 <a class="" href="{{ Options::base_url().Url_mod::convert_url('tip').'/'.Url_mod::url_convert(Support::tip_naziv($tip->tip_artikla_id)) }}">{{ Language::trans($tip->naziv) }}</a>
                            </h2>
                            @endif
                            </div>
                        </div>
                    
                   
                        <div class="tip-list JSproduct-list row slider JSslide-articles">
                            <div class="loader-wrapper wide js-tip-loader min-height js-loader-{{ $tip->tip_artikla_id }}">
                                <div class="loader"></div>
                            </div>

                            <button class="previous btn-prev action-btn slider-prev" id="JS{{ $tip->tip_artikla_id.'-' }}previous" data-id="{{ $tip->tip_artikla_id }}" data-offset="1">
                            
                            </button>

                            <button class="next btn-next action-btn slider-next" id="JS{{ $tip->tip_artikla_id.'-' }}next" data-id="{{ $tip->tip_artikla_id }}" data-offset="1">
                               
                            </button>

                            <div class="ajax-content" id="JS{{ $tip->tip_artikla_id.'-' }}content" data-tipid="{{ $tip->tip_artikla_id }}">
                                <!-- Product ajax content -->
                            </div>
                        </div>
         
                    </div>
                @endif
            @endforeach
        @endif
    </div>
</div>