

<nav class="manufacturer-categories">
      <ul class="row">
        <li class="col-md-12 col-sm-12 col-xs-12">
            <a  class="" href="{{ Options::base_url()}}{{Url_mod::url_convert('svi-artikli')}}">
                <span class="">{{ Language::trans('Svi artikli') }}</span>
            </a>
        </li>
        @foreach(Support::tip_categories($tip_id) as $key => $value)
            <li class="col-md-12 col-sm-12 col-xs-12">
                <a  class="" href="{{ Options::base_url()}}{{ Url_mod::convert_url('tip') }}/{{ Url_mod::url_convert($tip) }}/{{ Url_mod::url_convert($key) }}">
                    <span class="">{{ Language::trans($key) }}</span>
                </a>
            </li>
        @endforeach 
    </ul>
</nav> 
 
 