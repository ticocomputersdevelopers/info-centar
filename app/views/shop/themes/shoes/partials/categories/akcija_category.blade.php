

<nav class="manufacturer-categories">
    <ul class="row">
        <li>
            <a  class="" href="{{ Options::base_url()}}{{Url_mod::url_convert('svi-artikli')}}">
                <span class="">{{ Language::trans('Svi artikli') }}</span>
            </a>
        </li>
        @foreach(Support::akcija_categories() as $key => $value)
            <li>
                <a  class="" href="{{ Options::base_url()}}{{ Url_mod::convert_url('akcija') }}/{{ Url_mod::url_convert($key) }}">
                    <span class="">{{ Language::trans($key) }}</span>
                </a>
            </li>
        @endforeach  
    </ul>
</nav> 
 