

@if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array(200)))
    <div id="admin-menu"> 
        <div class="container text-right">
            <div class="col-md-12"><i class="fas fa-clipboard-list"></i>
                <span>
                    <a href="#!" data-toggle="modal" data-target="#FAProductsModal">
                        {{ Language::trans('Artikli') }}
                    </a>
                </span> 
                |  <i class="fas fa-save"></i>
                <span>
                    <a href="#!" id="JSShortAdminSave"> 
                        {{ Language::trans('Sačuvaj izmene') }}
                    </a>
                </span> 
                |
                <span class="ms admin-links"><i class="fas fa-cogs"></i>
                    <a target="_blank" href="{{ Options::base_url() }}admin">
                        {{ Language::trans('Admin Panel') }}
                    </a>
                </span>  
                |
                <span class="ms admin-links">
                    <a href="{{ Options::domain() }}admin-logout">
                        {{ Language::trans('Odjavi se') }}
                    </a>
                </span>
            </div>
        </div>
    </div> 
    @include('shop/front_admin/modals/products')
    @include('shop/front_admin/modals/product')
@endif