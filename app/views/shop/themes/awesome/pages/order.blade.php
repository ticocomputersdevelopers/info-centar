@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
<section class="shopping-information">

     <h2>Završena kupovina</h2>

    <table>
        <tbody>
            <th colspan="2">Informacije o narudžbini:</th>
            <tr>
                <td>Broj porudžbine:</td>
                <td>{{Order::broj_dokumenta($web_b2c_narudzbina_id)}}</td>
            </tr>
            <tr>
                <td>Datum porudžbine:</td>
                <td>{{Order::datum_porudzbine($web_b2c_narudzbina_id)}}</td>
            </tr>
            <tr>
                <td>Način isporuke:</td>
                <td>{{Order::n_i($web_b2c_narudzbina_id)}}</td>
            </tr>
            <tr>
                <td>Način plaćanja:</td>
                <td>{{Order::n_p($web_b2c_narudzbina_id)}}</td>
            </tr>
            <tr>
                <td>Napomena:</td>
                <td>{{Order::napomena_nar($web_b2c_narudzbina_id)}}</td>
            </tr>
        
        </tbody>

        <tbody>
            <th colspan="2">Informacije o kupcu:</th>
                <tr>
                @if($kupac->flag_vrsta_kupca == 0)
                    <td>Ime i Prezime:</td>
                    <td>{{ $kupac->ime.' '.$kupac->prezime }}</td>
                @else
                    <td>Firma i PIB:</td>
                    <td>{{ $kupac->naziv.' '.$kupac->pib }}</td>
                @endif
                </tr>
                <tr>
                <td>Adresa:</td>
                <td>{{ $kupac->adresa }}</td>
                </tr>
            <tr>
            <td>Mesto:</td>
            <td>{{ Order::mesto_narudzbina($kupac->mesto) }}</td>
            </tr>
            <tr>
            <td>Telefon:</td>
            <td>{{ $kupac->telefon }}</td>
            </tr>
            <tr>
            <td>Email:</td>
            <td>{{ $kupac->email }}</td>
            </tr>
        </tbody>

        <tbody>
            <th colspan="2">Informacije o prodavcu:</th>
            <tr>
                <td>Naziv prodavca:</td>
                <td>{{Options::company_name()}}</td>
            </tr>
            <tr>
                <td>Adresa:</td>
                <td>{{Options::company_adress()}}</td>
            </tr>
            <tr>
                <td>Telefon:</td>
                <td>{{Options::company_phone()}}</td>
            </tr>
            <tr>
                <td>Fax:</td>
                <td>{{Options::company_fax()}}</td>
            </tr>
            <tr>
                <td>PIB:</td>
                <td>{{Options::company_pib()}}</td>
            </tr>
            <tr>
                <td>Šifra delatnosti:</td>
                <td>{{Options::company_delatnost_sifra()}}</td>
            </tr>
            <tr>
                <td>Žiro račun:</td>
                <td>{{Options::company_ziro()}}</td>
            </tr>
            <tr>
                <td>E-mail:</td>
                <td>{{Options::company_email()}}</td>
            </tr>
        </tbody>
    </table>

    <table>
        <tbody>
        <th colspan="5">Informacije o narucenim proizvodima:</th>
        <tr>
            <td class="cell-product-name">Naziv proizvoda:</td>
            <td class="cell">Cena :</td>
            <td class="cell">Količina</td>
            <td class="cell">Ukupna cena:</td>
        </tr>
        @foreach(DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->get() as $row)
        <tr>
            <td class="cell-product-name">{{ Product::short_title($row->roba_id) }} {{ Product::getOsobineStr($row->roba_id,$row->osobina_vrednost_ids) }}</td>
            <td class="cell">{{ Cart::cena($row->jm_cena) }}</td>
            <td class="cell">{{ (int)$row->kolicina }}</td>
            <td class="cell">{{ Cart::cena(($row->kolicina*$row->jm_cena)) }}</td>
        </tr>
        @endforeach
        </tbody>
    </table>
    <table>
        <tbody>
            @if(Options::checkTezina() == 1 AND Order::troskovi_isporuke($web_b2c_narudzbina_id)>0)
            <tr>
                <td class="summary">Cena artikala: {{Cart::cena(Order::narudzbina_ukupno($web_b2c_narudzbina_id))}}</td>
            </tr>
            <tr>
                <td class="summary">Troškovi isporuke: {{Cart::cena(Order::troskovi_isporuke($web_b2c_narudzbina_id))}}</td>
            </tr>
            <tr>
                <td class="summary">Ukupno: {{Cart::cena(Order::narudzbina_ukupno($web_b2c_narudzbina_id)+Order::troskovi_isporuke($web_b2c_narudzbina_id))}}</td>
            </tr>
            @else
            <tr>
                <td class="summary">Ukupno: {{Cart::cena(Order::narudzbina_ukupno($web_b2c_narudzbina_id))}}</td>
            </tr>
            @endif
        </tbody>
    </table>

</section>
@endsection