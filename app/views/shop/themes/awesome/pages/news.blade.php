@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
<section class="news-section">
	@foreach(All::getNews() as $row)
		<article class="news">
			<div class="row">
				<div class="columns medium-12 large-4">
					<img src="{{ Options::domain() }}{{ $row->slika }}" alt="{{ $row->naslov }}">
				</div>
				<div class="columns medium-12 large-8">
					<a href="{{ Options::base_url() }}{{ Url_mod::convert_url('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}"><h2 class="news-title-list">{{ $row->naslov }}</h2></a>
					<p>{{ All::shortNewDesc($row->tekst) }}</p>
					<div class="btn-container right">
						<a class="btn btn-primary news__more" href="{{ Options::base_url() }}{{ Url_mod::convert_url('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}">Pročitaj članak</a>
					</div>
				</div>
			</div> <!--  end of .row -->
		</article>
	@endforeach
	<div>
		{{ All::getNews()->links() }}
	</div>
</section>
@endsection