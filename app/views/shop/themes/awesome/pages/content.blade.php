@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')

<section class="new-content clearfix about-us-page">
	{{ $content }}
</section>

@endsection