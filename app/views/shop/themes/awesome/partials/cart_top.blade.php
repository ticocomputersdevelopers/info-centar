
<div class="header-cart medium-2 columns small-3 text-center"> 
	<a class="header-cart-icon" href="{{ Options::base_url() }}{{ Seo::get_korpa() }}">
		<span class="JSbroj_cart">{{ Cart::broj_cart() }}</span>
		<input type="hidden" id="h_br_c" value="{{ Cart::broj_cart() }}" />
	</a>

	<ul class="JSheader-cart-content">
		@include('shop/themes/'.Support::theme_path().'partials/mini_cart_list')
	</ul>
</div>

