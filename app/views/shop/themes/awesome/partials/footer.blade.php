
<footer>
	<div class="main-wrapper clearfix ">
		<div class="row">
			<nav class="medium-3 columns flex-horizontal-center">
			    <ul class="footer-links">
			    	@foreach(All::footer_pages() as $row)
                        @if($row->tip_artikla_id == -1)
                        <li> 
                            <a href="{{ Options::base_url().$row->naziv_stranice }}">
                                {{ ($row->title) }}
                            </a>
                         </li>
                        @else
                            @if($row->tip_artikla_id==0)
                            <li> 
                                <a href="{{ Options::base_url().Url_mod::convert_url('akcija') }}">
                                    {{ ($row->title) }}
                                </a>
                            </li>
                            @else
                            <li> 
                                <a href="{{ Options::base_url().Url_mod::convert_url('tip').'/'.Url_mod::url_convert(Support::tip_naziv($row->tip_artikla_id)) }}">
                                    {{ ($row->title) }}
                                </a>
                            </li>
                            @endif
                        @endif
                    @endforeach &nbsp;
				</ul>
			</nav>

		 	<div class="medium-6 columns newsletter-checkin">

				<div class="medium-12 columns">
					<h4 class="follow_us text-center">Pratite nas </h4>  
					<div class="social-icons text-center">
						 {{Options::social_icon()}}
					</div>
				</div>

				<div class="medium-10 small-12 columns">
					@if(Options::newsletter()==1)
						<h4 class='registar_for_newsletter text-center'>Prijava za newsletter</h4>
						<div class="newsletter">
							<input type="text"  placeholder="E-mail adresa" id="newsletter" />
							<button onclick="newsletter()">Prijavi se</button>					
						</div>
				   @endif
				</div>

			 </div>


			<div class="medium-3 columns">
				<h4>Tehnička podrška</h4>
				<div class="work-time-width">
					<div>
						<i class="fa fa-clock-o icons_technical_support" aria-hidden="true"></i>
						<span>Radnim danima 9-19h
						<br>Subotom 9-15h</span>
					</div>

					<div>
						<i class="fa fa-phone icons_technical_support" aria-hidden="true"></i>
					    <span> 0800-500-000</span>
					</div>
				</div>					
			</div>
		</div>

<!-- 	<div class="row">
		<div class="cards medium-12 columns">
			<div class="card small-12 medium-2 columns">
				<a href="http://www.visa.ca/verified/infopane/index.html" target="_blank">
					<img src="{{ Options::domain() }}images/cards/visa.jpg" alt="banks">
				</a>
			</div>

			<div class="card small-12 medium-2 columns">
				<a href="http://www.mastercardbusiness.com/mcbiz/index.jsp?template=/orphans&content=securecodepopup" target="_blank">
					<img src="{{ Options::domain() }}images/cards/mastercard.gif" alt="mastercard">
				</a>
			</div>
			<div class="card small-12 medium-2 columns">
				<a href="http://www.bancaintesabeograd.com" target="_blank">
					<img src="{{ Options::domain() }}images/cards/banca-inteza.jpg" alt="intesa"></a>
			</div>
			<!- <div class="card small-12 medium-2 end columns">
				<a href="https://www.unicreditbank.rs/rs/pi.html" target="_blank">
					<img src="{{ Options::domain() }}images/cards/etrustmark.png" alt="etrustmark"></a>
			</div> 
		</div>
	</div> -->

	<div class="row">			
		<div class="small-12 columns footnote">
			<p class="info"> Cene su informativnog karaktera. Prodavac ne odgovara za tačnost cena iskazanih na sajtu, zadržava pravo izmena cena. Ponudu za ostale artikle, informacije o stanju lagera i aktuelnim cenama možete dobiti na upit. Plaćanje se isključivo vrši virmanski - bezgotovinski.
			</p>
			Izrada internet prodavnice - <a href="https://www.selltico.com">Selltico</a> &copy; {{ date('Y') }}. Sva prava zadržana.
	    </div>
	</div>				
</div> <!-- end .row -->
</footer>


<!-- FOOTNOTE -->



