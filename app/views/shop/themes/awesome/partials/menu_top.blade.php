
@if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array(200)))
 <section class="row text-right" id="admin-menu">
    <div class="main-wrapper clearfix"> 
        <span class="">Prijavljeni ste kao administrator</span>
        <span class="ms admin-links"><a target="_blank" href="{{ Options::base_url() }}admin">Admin Panel</a></span>
        <span class="ms admin-links"><a href="{{ Options::domain() }}admin-logout">Odjavi se</a></span>
    </div>
</section>
@endif

<section id="JSpreheader" class="row">
    <div class="social-icons" target='_blank'>
        {{Options::social_icon()}}
    </div>

    <!-- LOGIN & REGISTRATION ICONS --> 
    <section class="main-wrapper clearfix">    
        @if(Options::user_registration()==1)
            <section class="row preheader-icons">

                <div class="menu-close medium-12 columns">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </div>

                <div class="medium-9 small-12 columns top-menu-links small-only-text-center">
                    @foreach(All::menu_top_pages() as $row)
                        @if($row->grupa_pr_id != -1 and $row->grupa_pr_id != 0)
                        <li>
                          <a href="{{ Options::base_url()}}{{ Url_mod::url_convert(Groups::getGrupa($row->grupa_pr_id)) }}/0/0/0-0">{{ Language::trans($row->title) }}</a>
                        </li>
                        @elseif($row->tip_artikla_id == -1)
                            <a href="{{ Options::base_url().$row->naziv_stranice }}">
                                {{ ($row->title) }}
                            </a>
                         @else
                            @if($row->tip_artikla_id==0)
                                 <a href="{{ Options::base_url().Url_mod::convert_url('akcija') }}">
                                    {{ ($row->title) }}
                                </a>
                             @else
                                 <a href="{{ Options::base_url().Url_mod::convert_url('tip').'/'.Url_mod::url_convert(Support::tip_naziv($row->tip_artikla_id)) }}">
                                    {{ ($row->title) }}
                                </a>
                             @endif
                        @endif
                    @endforeach
                </div>

                <div class="medium-3 small-12 columns text-right small-only-text-center">
                    @if(Options::checkB2B())
                        <a id="b2b-login-icon" href="{{Options::base_url()}}b2b/login" class="btn confirm global-bradius">B2B Portal</a> 
                    @endif
                    &nbsp;
                </div>                  
               
            
               @endif
           </section>
    </section>  
</section>