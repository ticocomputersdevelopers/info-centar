
<header class="row">
<div class="header-gradient">
<div class="background-color-blue ">
   
    <div class="main-wrapper ">
        <div class="gray-div">

        <!-- CALL CENTAR-->
        <div class="small-12 medium-6 columns call_centar clearfix i_have_login">
            <span>CALL CENTAR</span>
            <i class="fa fa-phone" aria-hidden="true"></i>
            <span>0800-555-000</span>     
        </div>
 
        <!-- LOGIN AND REGISTAR-->
            
            <div class="small-12 medium-6 columns login-flex-center">
              @if(Session::has('b2c_kupac'))
                <div class="logout-wrapper clearfix">
                    <span class="logged-user">Ulogovan korisnik:</span>
                    <a id="logged-user" href="{{Options::base_url()}}korisnik/{{Url_mod::url_convert(WebKupac::get_user_name())}}">
                        <span>{{ WebKupac::get_user_name() }}</span>
                    </a>
                    <a id="logged-user" href="{{Options::base_url()}}korisnik/{{Url_mod::url_convert(WebKupac::get_company_name())}}">
                        <span>{{ WebKupac::get_company_name() }}</span>
                    </a>
                    <a id="logout-button" href="{{Options::base_url()}}logout"><span>Odjavi se</span></a>
                </div>
               @else 
                <div class="position">
                    <a class="" id="registration-icon" href="{{Options::base_url()}}prijava"><i class="fa fa-user-plus" aria-hidden="true"></i>
                        <span>Registracija</span>
                    </a>
                </div>

               <div class="position display-popup"> 
                    <a class="" id="login-icon" href="{{Options::base_url()}}prijava">
                        <i class="fa fa-sign-in" aria-hidden="true"></i><span>Uloguj se </span>
                    </a>
                </div>
                @endif

             <!-- CART AREA -->
             @include('shop/themes/'.Support::theme_path().'/partials/cart_top')
          </div>          
        </div>
      </div> 
   </div>

    <div class="main-wrapper clearfix">
        <div class="row flex-items-center">
        <!-- LOGO AREA -->
            <div class="logo-area medium-2 small-12 columns text-center small-only-text-left"> 
                <h1 class="for-seo"> 
                    <a class="logo" href="{{ Options::base_url()}}" title="{{Options::company_name()}}">
                        <img src="{{ Options::domain() }}{{Options::company_logo()}}" alt="{{Options::company_name()}}" />
                    </a>
                </h1>
            </div>

            <!-- SEARCH AREA -->
            <div class="JSsearchContent large-4 medium-4 small-12 columns">
                <div class="JSsearch">
                    <input class="search-field search-field-bc" autocomplete="off" data-timer="" type="text" id="JSsearch" placeholder="Pretraga" />
                    <button onclick="search()" class="JSsearch-button"> <i class="fa fa-search"></i></button>
                </div>
            </div>  

            <!-- POLICY -->
            <div class="medium-5 large-5 small-6 columns all_policy">
                <div class="block_policy">
                    <i class='image_policy money'></i>
                    <p class="text_policy">povracaj novca za 30 dana </p>                 
                </div>

                <div class="block_policy">
                    <div class='image_policy delivery'></div>
                    <p class="text_policy">besplatna dostava preko 1000 RSD </p>        
                </div>

                <div class="block_policy">
                    <div class='image_policy prices'></div>               
                    <p class="text_policy">zagarantovano najnize cene </p>               
                </div>
            </div>
             <div class="JSmenu-icon"><i class="fa fa-bars" aria-hidden="true"></i></div>
        </div>
    </div>
</div>

    <div class="menu-background row">
        <div class="main-wrapper clearfix">

            <!-- MAIN MENU -->
            <!-- SIDEBAR LEFT -->
            <aside id="sidebar-left" class="medium-12 large-3 columns">
                @if(Options::category_view()==1)
                @include('shop/themes/'.Support::theme_path().'/partials/categories/category')
                @endif                  
            </aside>

            <nav class="large-9 columns JSmain-nav">
                <ul id="main-menu" class="clearfix">
                    @foreach(All::header_pages('flag_page') as $row)
                        @if($row->tip_artikla_id == -1)
                        <li> 
                            <a href="{{ Options::base_url().$row->naziv_stranice }}">
                                {{ ($row->title) }}
                            </a>
                         </li>
                        @else
                            @if($row->tip_artikla_id==0)
                            <li> 
                                <a href="{{ Options::base_url().Url_mod::convert_url('akcija') }}">
                                    {{ ($row->title) }}
                                </a>
                            </li>
                            @else
                            <li> 
                                <a href="{{ Options::base_url().Url_mod::convert_url('tip').'/'.Url_mod::url_convert(Support::tip_naziv($row->tip_artikla_id)) }}">
                                    {{ ($row->title) }}
                                </a>
                            </li>
                            @endif
                        @endif
                    @endforeach
                    
                    @if(Options::web_options(121)==1)
                        <?php $konfiguratori = All::getKonfiguratos(); ?>
                        @if(count($konfiguratori) > 0)
                            @if(count($konfiguratori) > 1)
                                <li class="dropdown">
                                    <a href="#" class="dropbtn">Konfiguratori</a>
                                    <ul class="dropdown-content">
                                        @foreach($konfiguratori as $row)
                                        <li><a href="{{ Options::base_url() }}konfigurator/{{ $row->konfigurator_id }}">{{ $row->naziv }}</a></li>
                                        @endforeach
                                    </ul>
                                </li>
                            @else
                                <li><a href="{{ Options::base_url() }}konfigurator/{{ $konfiguratori[0]->konfigurator_id }}">Konfigurator</a></li>
                            @endif
                        @endif
                    @endif
                </ul>
           </nav>

         </div>
        
    </div>
    
</header>