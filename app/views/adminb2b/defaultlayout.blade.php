<!DOCTYPE html>
<html>
<head>
    <title>{{$title}}</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="msapplication-tap-highlight" content="no"/> 
    <link rel="icon" type="image/png" href="{{ AdminOptions::base_url()}}favicon.ico">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <link href="{{ AdminB2BOptions::base_url()}}css/foundation.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ AdminB2BOptions::base_url()}}css/normalize.css" rel="stylesheet" type="text/css" />
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="{{ AdminOptions::base_url()}}css/alertify.core.css">
    <link rel="stylesheet" href="{{ AdminOptions::base_url()}}css/alertify.default.css">
    <script src="{{ AdminOptions::base_url()}}js/alertify.js" type="text/javascript"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css" rel="stylesheet" />
    <link href="{{ AdminB2BOptions::base_url()}}css/admin.css" rel="stylesheet" type="text/css" />
</head>
<body class="{{ Session::get('adminHeaderWidth') }}">
    <div class="flex-wrapper">
        @include('adminb2b.partials.header')
        @yield('content')    
    </div>
    <script src="{{ AdminB2BOptions::base_url()}}js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script src="{{ AdminB2BOptions::base_url()}}js/jquery-ui.min.js"></script>
    <script src="{{ AdminB2BOptions::base_url()}}js/foundation.min.js" type="text/javascript"></script>
    <script src="{{ AdminB2BOptions::base_url()}}js/alertify.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>
    <script type="text/javascript"> var base_url = "{{AdminB2BOptions::base_url()}}"; </script>
    <script src="{{ AdminB2BOptions::base_url()}}js/adminb2b/admin_b2b.js" type="text/javascript"></script>
    @if($strana == 'b2b_narudzbine')   
    <script src="{{ AdminB2BOptions::base_url()}}js/adminb2b/admin_b2b_narudzbine.js" type="text/javascript"></script>
    @endif
    @if($strana == 'b2b_artikli')   
    <script src="{{ AdminB2BOptions::base_url()}}js/adminb2b/admin_b2b_articles.js" type="text/javascript"></script>
    @endif
    @if($strana == 'proizvodjaci' || $strana == 'partneri')
    <script src="{{ AdminB2BOptions::base_url()}}js/adminb2b/admin_b2b_proizvodjaci_partneri.js" type="text/javascript"></script>
    @endif
    @if($strana == 'rabat_kombinacije')   
    <script src="{{ AdminB2BOptions::base_url()}}js/adminb2b/admin_b2b_rabat_kombinacije.js" type="text/javascript"></script>
    @endif
    @if($strana == 'b2b_podesavanja')   
    <script src="{{ AdminB2BOptions::base_url()}}js/adminb2b/admin_b2b_settings.js" type="text/javascript"></script>
    @endif

    @if(in_array( $strana,array('b2b_stranice','b2b_baneri_slajderi','vest')))
    <script type="text/javascript" src="{{ AdminOptions::base_url()}}js/tinymce2/tinymce.min.js"></script>
    <script type="text/javascript">
    tinymce.init({
        selector: ".special-textareas",
        height : "800px",
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste",
            "autoresize"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
    });
    </script>
    @endif
            
    <!-- 
    <script src="{{ AdminOptions::base_url()}}/js/admin/admin.js" type="text/javascript"></script>
    <script src="{{ AdminOptions::base_url()}}/js/admin/admin_funkcije.js" type="text/javascript"></script> -->
    <script> var all_ids = {{ isset($all_ids) ? $all_ids : 'null' }}; </script>
</body>
</html>