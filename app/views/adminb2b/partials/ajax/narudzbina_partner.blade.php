
<div class="field-group columns medium-12 large-6">
    <label for="">Adresa</label>
    <input name="adresa" type="text" value="{{ $kupac->adresa }}" disabled>
</div>
<div class="field-group columns medium-12 large-6">
    <label for="">Mesto</label> 
    <input name="mesto" type="text" value="{{ $kupac->mesto }}" disabled>
</div>
<div class="field-group columns medium-12 large-6">
    <label for="">Telefon</label>
    <input name="telefon" type="text" value="{{ $kupac->telefon }}" disabled>
</div>
