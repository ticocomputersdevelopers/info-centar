@extends('adminb2b.defaultlayout')
@section('content')
 
<section class="" id="main-content">
	
	@include('adminb2b.partials.partner_rabat_tabs')

	<div class="row art-row">
		<section class="medium-3 columns">
			<div class="flat-box">
				<h3 class='title-med'>Unos novih rabata</h3><br>
				<form class="rabat-combination-form" action="{{AdminOptions::base_url()}}admin/b2b/rabat_kombinacije" method="POST">
					<div class="select-control">
						<label></label>
						<select class="select2" name="partner">
						<option value="-1">Izaberite partnera</option>
						@foreach($partneri as $row)
						<option value="{{ $row->partner_id }}">{{ $row->naziv }}</option>
						@endforeach
						</select>
					</div>
					<div class="select-control">
						<label></label>
						<select class="select2" name="grupa">
						{{ AdminB2BProizvodjac::getGroups(0) }}
						</select>
					</div>
					<div class="select-control">
						<label></label>
						<select class="select2" name="proizvodjac">
						<option value="-1">Izaberite proizvođača</option>
						@foreach($proizvodjaci as $row)
						<option value="{{ $row->proizvodjac_id }}">{{ $row->naziv }}</option>
						@endforeach
						</select>
					</div>
					<div class="select-control"> 
						<label></label>
						<input type="number" name="rabat" required class="" placeholder="Rabat"> 
					</div>
					<div class="text-center">
						<input type="submit" class="btn-primary btn" value="Unesi">
					</div>
				</form>
			</div>
		</section>
		<section class="large-8 medium-8 columns">
			<div id="comb-table" class="flat-box"> 
				<div class="columns medium-5 no-padd"> 
					<div class="btn-container"> 
						<input type="text" id="search-fast" v-model="search" placeholder="Pretraga.."> 
					</div><br>
				</div> 
				<table class="articles-page-tab">
					<thead>
						<tr class="st order-list-titles row">
							<th class="table-head">Partner</th>
							<th class="table-head">Grupa</th>
							<th class="table-head">Proizvođač</th>
							<th class="table-head">Rabat</th>
							<th class="table-head"></th>
							</tr>
					</thead>
					<tbody class="proizvodjaci-b2b">
						@foreach($kombinacije as $kombinacija)
						<tr class="order-list-item row">
							<td>&nbsp; {{ $kombinacija->partner_naziv }}</td>
							<td>{{ $kombinacija->grupa }}</td>
							<td>{{ $kombinacija->proizvodjac_naziv }}</td>
							<td><input type="number" class="JSKombinacijaRabat" value="{{ $kombinacija->rabat }}"></td>
							<td>
							<button class="JSUpdatePartnerRabatGrupa btn save-it-btn btn-small " data-partner_rabat_grupa_id="{{ $kombinacija->id }}">Sačuvaj</button>
							<button class="JSDeletePartnerRabatGrupa btn btn-small btn-danger" data-partner_rabat_grupa_id="{{ $kombinacija->id }}">Izbriši</button>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
				{{ $kombinacije->links() }}
			</div>
		</section>
	</div>
</section>
@endsection

<!--   	methods: {
        save: function(id, rabat) {

        	$.post(
				base_url+'admin/b2b/ajax/rabat_edit', {
					action: 'rabat_kombinacija_edit',
					id: id,
					rabat: rabat
				}, function (response){
					if(response == 1) {
						alertify.success('Uspešno ste sačuvali rabat.');
					} else {
						alertify.error('Greška prilikom upisivanja u bazu.');
					}
				}
			);

            console.log("Saved ", id, rabat);
        },
        deleteRow: function(row, id) {

        	$.post(
				base_url+'admin/b2b/rabat_kombinacije/'+id+'/delete', {}, 
				function (response){
					if(response == 1) {
						alertify.success('Uspešno izbrisano');
						window.location.reload(true);

					} else {
						alertify.error('Greška');
					}
				}
			);

            //console.log("Deleted", id);
        }
    } -->


