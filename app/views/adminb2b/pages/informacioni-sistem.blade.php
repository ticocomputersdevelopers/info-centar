@extends('adminb2b.defaultlayout')
@section('content')
	<div id="main-content" class="kupci-page">
		@if(Session::has('message'))
			<script>
				alertify.success('{{ Session::get('message') }}');
			</script>
		@endif
		@if(Session::has('error_message'))
			<script>
				alertify.error('{{ Session::get('error_message') }}');
			</script>
		@endif
		@if(AdminB2BOptions::info_sys('excel'))
		<div class="row">
			<div class="medium-12 large-6 large-centered columns">
			<h1>UPLOAD fajla iz informacionog sistema</h1>
				<div class="flat-box">
				
					<form action="{{AdminOptions::base_url()}}admin/b2b/is/file-upload" method="POST" enctype="multipart/form-data">
						<div class="row">
							<div class="field-group column medium-12 large-6">
								<label for="">Fajl:</label>
								<select type="text" name="table">
									<option value="grupa_pr">Kategorije</option>
									<option value="proizvodjac">Proizvođači</option>
									<option value="lager">Lager</option>
									<option value="tarifna_grupa">Poreske stope</option>
									<option value="jedinica_mere">Jedinice mere</option>
									<option value="roba">Artikli</option>
									<option value="partner">Kupci</option>
								</select>
							</div>
							<div class="field-group column medium-12 large-3" hidden>
								<label for="">Vrsta fajla:</label>
								<select type="text" name="kind">
									<option value="xlsx">XLSX</option>
									<option value="xml">XML</option>
									<option value="csv">CSV</option>
								</select>
							</div>
							<div class="field-group column medium-12 large-3">
								<label for="">Izaberite:</label>
								<input type="file" name="import_file">
							</div>
						</div>

						<div class="btn-container center no-margin">
							<button type="submit" id="submit" class="btn btn-primary">Dodaj fajl</button>
							<a href="{{AdminOptions::base_url()}}admin/file-upload" class="btn btn-secondary">Otkaži</a>
						</div>
					</form>

					<h2 class="text-center">Učitavanje podataka iz informacionog sistema</h2>
					<form action="{{AdminOptions::base_url()}}admin/b2b/is/load" method="POST">
						<div class="row">
							<div class="field-group column medium-12 large-4" hidden>
								<label for="">Tip učitavanja:</label>
								<select type="text" name="kind">
									<!-- <option value="">Izaberi tip učitavanja</option> -->
									<option value="xlsx">XLSX</option>
									<option value="xml">XML</option>
									<option value="csv">CSV</option>
									<option value="sql">SQL</option>
								</select>
							</div>
						</div>

						<div class="btn-container center no-margin">
							<button type="submit" id="submit" class="btn btn-primary">Učitaj</button>
						</div>
					</form>

					@if(Session::has('error_messages'))
						<p class="error">DOŠLO JE DO GREŠKE PRILIKOM UČITAVANJA PODATAKA!</p>
						@foreach(Session::get('error_messages') as $err_mess)
						<p class="error">{{$err_mess}}</p>
						@endforeach
					@endif
				</div>

			</div>
		</div>
		@endif
		@if(AdminB2BOptions::info_sys('xml'))
		<div class="row">
			<div class="medium-12 large-6 large-centered columns">
				<h1>UPLOAD fajla iz informacionog sistema</h1>
				<div class="flat-box">
					<form action="{{AdminOptions::base_url()}}admin/b2b/is/file-upload" method="POST" enctype="multipart/form-data">
						<div class="row">
							<div class="field-group column medium-12 large-6">
								<label for="">Fajl:</label>
								<select type="text" name="table">
									<option value="roba">Artikli</option>
									<option value="grupa_pr">Kategorije</option>
									<option value="proizvodjac">Proizvođači</option>
									<option value="lager">Lager</option>
									<option value="tarifna_grupa">Poreske stope</option>
									<option value="jedinica_mere">Jedinice mere</option>
									<option value="partner">Kupci</option>
								</select>
							</div>
							<div class="field-group column medium-12 large-3">
								<label for="">Izaberite:</label>
								<input type="file" name="import_file">
							</div>
							<input type="hidden" name="kind" value="xml">
						</div>

						<div class="btn-container center no-margin">
							<button type="submit" id="submit" class="btn btn-primary">Dodaj fajl</button>
						</div>
					</form>
				</div>
			<h1>Učitavanje podataka iz XML fajla</h1>
				<div class="flat-box">
					<form method="POST" action="{{ AdminOptions::base_url() }}admin/b2b/xml/post">
						<div class="btn-container center no-margin">
						<button type="submit" class="btn btn-primary" onClick="this.form.submit(); this.disabled=true; this.value='Učitavanje…';">Učitaj</button>
						</div>
					</form>
				</div>

			</div>
		</div>
		@endif
		@if(AdminB2BOptions::info_sys('wings'))
		<div class="row">
			<div class="medium-12 large-6 large-centered columns">
			<h1>Učitavanje podataka iz WINGS-ovog informacionog sistema</h1>
				<div class="flat-box">
					<form method="POST" action="{{ AdminOptions::base_url() }}admin/b2b/wings/post">
						<div class="btn-container center no-margin">
						<button type="submit" class="btn btn-primary" onClick="this.form.submit(); this.disabled=true; this.value='Učitavanje…';">Učitaj</button>
						</div>
					</form>
				</div>

			</div>
		</div>
		@endif

		@if(AdminB2BOptions::info_sys('calculus'))
		<div class="row">
			<div class="medium-12 large-6 large-centered columns">
			<h1>Učitavanje podataka iz CALCULUS-ovog informacionog sistema</h1>
				<div class="flat-box">
					<form method="POST" action="{{ AdminOptions::base_url() }}admin/b2b/calculus/post">
						<div class="btn-container center no-margin">
						<button type="submit" class="btn btn-primary" onClick="this.form.submit(); this.disabled=true; this.value='Učitavanje…';">Učitaj</button>
						</div>
					</form>
				</div>

			</div>
		</div>
		@endif

		@if(AdminB2BOptions::info_sys('infograf'))
		<div class="row">
			<div class="medium-12 large-6 large-centered columns">
			<h1>Učitavanje podataka iz informacionog sistema</h1>
				<div class="flat-box">
					<form method="POST" action="{{ AdminOptions::base_url() }}admin/b2b/infograf/post">
						<div class="btn-container center no-margin">
						<button type="submit" class="btn btn-primary" onClick="this.form.submit(); this.disabled=true; this.value='Učitavanje…';">Učitaj</button>
						</div>
					</form>
				</div>

			</div>
		</div>
		@endif

		@if(AdminB2BOptions::info_sys('logik'))
		<div class="row">
			<div class="medium-12 large-6 large-centered columns">
			<h1>Učitavanje podataka iz LOGIK-ovog informacionog sistema</h1>
				<div class="flat-box">
					<form method="POST" action="{{ AdminOptions::base_url() }}admin/b2b/logik/post">
						<div class="btn-container center no-margin">
						<button type="submit" class="btn btn-primary" onClick="this.form.submit(); this.disabled=true; this.value='Učitavanje…';">Učitaj</button>
						</div>
					</form>
				</div>

			</div>
		</div>
		@endif

	</div>
@endsection