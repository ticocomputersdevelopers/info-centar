<div class="row small-gutter art-row">
	<h2 class="title-med">Proizvođači</h2><br>
	<div class="btn-container box-sett"> 
		<input type="text" id="myInput" placeholder="Pretraga..">
	</div>
	<table id="myTable">
	  <tr class="header">
	    <th></th>
	    <th></th>
	  </tr>
	  @foreach($proizvodjaci as $row)
	  <tr class="proizvodjaci-b2b">
	    <td>&nbsp; {{ $row->naziv }}</td>
	    <td><span><input type="number" class="JSrabat" data-id="{{ $row->proizvodjac_id }}" value="{{ (isset($row->rabat)) ? $row->rabat : '' }}"></span></td>
	  </tr>
	  @endforeach
	</table> 
</div>