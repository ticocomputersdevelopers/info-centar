@extends('adminb2b.defaultlayout')
@section('content')
<section id="main-content" class="pages-page row">
	<section class="medium-3 columns">
		<div class="flat-box">
			<h3 class="text-center h3-margin">Strane:</h3>
			<ul class="page-list lista" id="JSPagesSortableB2b">
				<li class="new-page new-elem" id="0">
					<a href="{{AdminB2BOptions::base_url()}}admin/b2b/b2b_stranice/0">Nova strana</a>
					<div class="icon"><i class="fa fa-plus" aria-hidden="true"></i></div>
				</li>
				@foreach($query_list_pages as $row)
				<li class="ui-state-default @if($row->web_b2b_seo_id == $web_b2b_seo_id) active @endif" id="{{$row->web_b2b_seo_id}}" {{$row->disable == 1 ? 'style="background-color: #ddd"' : ''}}>
				<a href="{{AdminB2BOptions::base_url()}}admin/b2b/b2b_stranice/{{$row->web_b2b_seo_id}}">{{$row->title}}</a>
				</li>
				@endforeach
			</ul>
		</div>
	</section>
	<section class="page-edit medium-9 columns">
		<div class="flat-box">
			<h3 class="text-center h3-margin">Sadržaj:</h3>
			<form name="form_pages" action="{{AdminB2BOptions::base_url()}}admin/b2b/pages" method="post">
				<input type="hidden" name="jezik_id" value="{{ $jezik_id }}">
				<section class="page-edit-top">
					<div class="row">
						<div class="medium-4 columns page-edit-name">
							<label>Naziv strane:</label>
							<input type="text" id="page_name" name="page_name" value="{{Input::old('page_name') ? Input::old('page_name') : $naziv}}" {{$disable==1?'disabled="disabled"':''}} onchange="check_fileds('page_name')">
						</div>
						@if($disable==0)
						<div class="medium-3 columns include-in-menu">
							<label>Listaj artikle grupe:</label>  
							<select name="grupa_pr_id" class="medium-12 columns seo-button-wrapper">
								<option value="-1"></option>
								@foreach(DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>0,'web_b2b_prikazi'=>1))->orderBy('redni_broj','asc')->get() as $grupa)
								@if(Input::old('grupa_pr_id') == $grupa->grupa_pr_id)
								<option value="{{ $grupa->grupa_pr_id }}" selected>{{ $grupa->grupa }}</option>
								@else
								@if($grupa_pr_id == $grupa->grupa_pr_id)
								<option value="{{ $grupa->grupa_pr_id }}" selected>{{ $grupa->grupa }}</option>
								@else
								<option value="{{ $grupa->grupa_pr_id }}">{{ $grupa->grupa }}</option>
								@endif
								@endif
								@endforeach
							</select>
						</div>
<!-- 						<div class="medium-3 columns include-in-menu">
							<label>Listaj artikle akcije ili tipova:</label>  
							<select name="tip_artikla_id" class="medium-12 columns seo-button-wrapper">
							<option value="-1"></option>
							@if(!is_null(Input::old('tip_artikla_id')) AND Input::old('tip_artikla_id') == 0)
							<option value="0" selected>Akcija</option>
							@else
							@if($tip_artikla_id == 0)
							<option value="0" selected>Akcija</option>
							@else
							<option value="0">Akcija</option>
							@endif
							@endif
							@foreach(AdminSupport::getTipovi(1) as $tip)
							@if(Input::old('tip_artikla_id') == $tip->tip_artikla_id)
							<option value="{{ $tip->tip_artikla_id }}" selected>{{ $tip->naziv }}</option>
							@else
							@if($tip_artikla_id == $tip->tip_artikla_id)
							<option value="{{ $tip->tip_artikla_id }}" selected>{{ $tip->naziv }}</option>
							@else
							<option value="{{ $tip->tip_artikla_id }}">{{ $tip->naziv }}</option>
							@endif
							@endif
							@endforeach
							</select>
						</div> -->
						@endif
						<div class="medium-4 columns include-in-menu">
							<br>
							<label class="checkbox-label medium-6 columns">
							<input type="checkbox" id="b2b_header_menu" name="b2b_header_menu" {{ Input::old('b2b_header_menu') ? 'checked="checked"' : ($b2b_header ? 'checked="checked"' : '') }}> 
							<span class="label-text">Header meniju</span>
							</label>
							<label class="checkbox-label medium-6 columns">
							<input type="checkbox" id="b2b_footer_menu" name="b2b_footer_menu" {{ Input::old('b2b_footer_menu') ? 'checked="checked"' : ($b2b_footer ? 'checked="checked"' : '') }}>
							<span class="label-text">Footer meniju</span>
							</label>  
						</div>
					</div>
				 
					<br>
					<div class="row">
						@if(count($jezici) > 1)
						<div class="languages">
							<ul>	
							@foreach($jezici as $jezik)
							<li><a class="{{ $jezik_id == $jezik->jezik_id ? 'active' : '' }} btn-small btn btn-secondary" href="{{AdminB2BOptions::base_url()}}admin/b2b/b2b_stranice/{{ $web_b2b_seo_id }}/{{ $jezik->jezik_id }}">{{ $jezik->naziv }}</a></li>
							@endforeach
							</ul>
						</div>
						@endif
					</div>
					<!-- SEO SETTINGS -->
					<!-- 						<section class="seo-settings row">
					<div class="medium-6 columns field-group {{ $errors->first('seo_title') ? 'error' : '' }}">
					<label>Title:</label>
					<input id="seo-title" type="text" value="{{Input::old('seo_title') ? Input::old('seo_title') : $seo_title}}"  name="seo_title" onkeydown=" return character_limit(event,60)">
					</div>
					<div class="medium-6 columns field-group {{ $errors->first('keywords') ? 'error' : '' }}">
					<label>Keywords:</label>
					<input id="seo-keywords" type="text" value="{{Input::old('keywords') ? Input::old('keywords') : $keywords}}" name="keywords" onkeydown=" return character_limit(event,159)">
					</div>

					<div class="medium-6 columns field-group {{ $errors->first('description') ? 'error' : '' }}">
					<label>Description:</label>
					<input id="seo-description" type="text" value="{{Input::old('description') ? Input::old('description') : $desription}}" name="description" onkeydown=" return character_limit(event)">
					</div>
					</section> -->
				</section>
				@if($disable==0)
				<!-- PAGE EDIT TEXT EDITOR -->
				<div class="row"> 
					<textarea class="special-textareas" name="content" id="content"  style="width:100%">
					{{$content}}
					</textarea>
				</div>
				@endif
				<input type="hidden" name="web_b2b_seo_id" value="{{$web_b2b_seo_id}}" />
				<input type="hidden" name="status" value="{{$status}}" />
				<input type="hidden" name="flag_page" id="flag_page" value="{{$flag_page}}" />
				<input type="hidden" name="flag_b2b_show" id="flag_b2b_show" value="{{$flag_b2b}}" />
				<div class="btn-container center">
					<a href="#" class="page-edit-save btn btn-primary save-it-btn">Sačuvaj</a>
				</div>
			</form>
			@if($disable==0)
			@foreach($stranice as $row)
			<a href="{{ AdminB2BSupport::page_link_b2b($row->naziv_stranice) }}" target="_blank" class="btn btn-primary" value="{{$web_b2b_seo_id}}">Vidi stranicu</a>
			@endforeach
			<form class="pages-form" method="post" action="{{AdminB2BOptions::base_url()}}admin/b2b/delete_page">
				<input type="hidden" name="web_b2b_seo_id" value="{{$web_b2b_seo_id}}" />				
				<input type="submit" value="Obriši stranicu" class="btn btn-danger">
			</form>
			<div class="image-upload-area clearfix">	
				<form method="post" enctype="multipart/form-data" action="{{AdminB2BOptions::base_url()}}admin/b2b/image_upload" >
					<div class="row"> 
						<div class="column"> 
							<label>Dodajte sliku na strani:</label>
							<div class="bg-image-file"> 
								<input type="file" id="img" name="img" class="file-input">
								<button class="file-upload btn btn-secondary btn-small" type="submit">Učitaj</button>
							</div>
						</div>
					</div>
				</form>
				<div class="images_upload has-tooltip" title='Prevucite sliku na željenu poziciju'>
				{{AdminB2BSupport::upload_directory()}}
				</div>
			</div>
			@endif
		</div> 
	</section>
</section>
@endsection
