@extends('adminb2b.defaultlayout')
@section('content')
<section id="main-content" class="banners">
	@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
	@endif
	<section class="medium-12 large-3 columns">
		<div class="flat-box">
			<h3 class="text-center h3-margin">Baneri</h3>
			<ul class="banner-list JSListaBanerB2B" id="banner-sortable" data-table="2">
				<li id="0" class="new-banner new-elem">
					<a href="{{AdminB2BOptions::base_url()}}admin/b2b/baneri-slajdovi/0/1">Novi baner</a>
					<div class="icon"><i class="fa fa-plus" aria-hidden="true"></i></div>
				</li>
				@foreach($banners as $banner)
				<li id="{{$banner->baneri_id}}" class="{{ ( (B2bOptions::trajanje_banera($banner->baneri_id) == 'NULL' ) && ( B2bOptions::trajanje_banera($banner->baneri_id) <= 1))  ? 'deadline' : '' }} ui-state-default @if($banner->baneri_id == $id) active @endif" id="{{$banner->baneri_id}}">
					<a href="{{AdminB2BOptions::base_url()}}admin/b2b/baneri-slajdovi/{{$banner->baneri_id}}/1">{{$banner->naziv}}</a>
					<a class="icon tooltipz JSbtn-delete" aria-label="Obriši" data-link="{{AdminB2BOptions::base_url()}}admin/b2b/baneri-delete/{{$banner->baneri_id}}"><i class="fa fa-times" aria-hidden="true"></i></a>
				</li>
				@endforeach
			</ul>	
			<h3 class="text-center h3-margin">Slajderi</h3>
			<ul class="banner-list JSListaSliderB2B" id="slide-sortable" data-table="2">
				<li id="0" class="new-slide new-elem">
					<a href="{{AdminB2BOptions::base_url()}}admin/b2b/baneri-slajdovi/0/2">Novi slajd
					</a>
					<div class="icon"><i class="fa fa-plus" aria-hidden="true"></i></div>
				</li>
				@foreach($sliders as $slide)
				<li id="{{$slide->baneri_id}}" class="{{ ( (B2bOptions::trajanje_banera($slide->baneri_id) == 'NULL' ) && ( B2bOptions::trajanje_banera($slide->baneri_id) <= 1))  ? 'deadline' : '' }} ui-state-default @if($slide->baneri_id == $id) active @endif" id="{{$slide->baneri_id}}">
				<a href="{{AdminB2BOptions::base_url()}}admin/b2b/baneri-slajdovi/{{$slide->baneri_id}}/2">{{$slide->naziv}}</a>
				<a class="icon tooltipz JSbtn-delete" aria-label="Obriši" data-link="{{AdminB2BOptions::base_url()}}admin/b2b/baneri-delete/{{$slide->baneri_id}}"><i class="fa fa-times" aria-hidden="true"></i></a></li>
				@endforeach
			</ul>
			<h3 class="text-center h3-margin">Pop-Up baner</h3>
			<ul class="banner-list JSListaBaner" id="banner-sortable" data-table="2">
				<li id="0" class="new-banner new-elem">
					<a href="{{AdminB2BOptions::base_url()}}admin/b2b/baneri-slajdovi/0/9">Novi pop-up baner</a>
					<div class="icon"><i class="fa fa-plus" aria-hidden="true"></i></div>
				</li>
				@foreach($popup as $popbanner)
				<li id="{{$popbanner->baneri_id}}" class="{{ ( (B2bOptions::trajanje_banera($popbanner->baneri_id) == 'NULL' ) && ( B2bOptions::trajanje_banera($popbanner->baneri_id) <= 1))  ? 'deadline' : '' }} ui-state-default @if($popbanner->baneri_id == $id) active @endif" id="{{$popbanner->baneri_id}}">
					<a href="{{AdminB2BOptions::base_url()}}admin/b2b/baneri-slajdovi/{{$popbanner->baneri_id}}/9">{{$popbanner->naziv}}</a>
					<a class="icon tooltipz JSbtn-delete" aria-label="Obriši" data-link="{{AdminB2BOptions::base_url()}}admin/b2b/baneri-delete/{{$popbanner->baneri_id}}"><i class="fa fa-times" aria-hidden="true"></i></a>
				</li>
				@endforeach
			</ul>
		</div>
				<!-- BACKGROUND IMAGE -->
		<div class="flat-box">
			<h3 class="text-center h3-margin">Pozadinska slika</h3>
			<ul class="">
				<form action="{{AdminB2BOptions::base_url()}}admin/b2b/bg-img-edit" method="POST" enctype="multipart/form-data">
					<input type="file" name="bgImg" value="Izaberi sliku">
					
					@if(B2bOptions::getBGimg() == null)
					<img src="{{AdminB2BOptions::base_url()}}/images/no-image.jpg" alt="" class="bg-img">
					@else
					<img src="{{AdminB2BOptions::base_url()}}{{ B2bOptions::getBGimg() }}" alt="" class="bg-img">
					@endif
					<h3 class="text-center h3-margin">Preporučene dimenzije slike: 1920x1080</h3>
					<br>
					<!-- <label for="aktivna">
						<input type="checkbox" name="aktivna" value=""> Aktivna slika
					</label> -->

					<label class="text-center">Link leve strane</label>
					<input id="link" value="{{B2bOptions::getlink()}}" type="text" name="link">
					<label class="text-center">Link desne strane</label>
					<input id="link2" value="{{B2bOptions::getlink2()}}" type="text" name="link2">
					<input type="hidden" value="3" name="flag"><br>
					<div class="text-center btn-container">
						<button type="submit" class="btn btn-primary save-it-btn">Sačuvaj</button>
						<a class="btn btn-danger" href="{{AdminB2BOptions::base_url()}}admin/b2b/bg-img-delete">Obriši</a>
					</div>
				</form>
			</ul>
		</div> 
	</section> 
	<section class="page-edit medium-12 large-9 columns">
		<div class="flat-box"> 
			<form action="{{AdminB2BOptions::base_url()}}admin/b2b/banner-edit" name="banner_upload" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="logo-right">
				@if($type == 1)
				<h3 class="text-center h3-margin">Izmeni baner</h3>
				@elseif($type == 2)
				<h3 class="text-center h3-margin">Izmeni slajder</h3>
				@elseif($type == 9)
				<h3 class="text-center h3-margin">Izmeni popup baner</h3>
				@endif
				<section class="banner-edit-top row">
					<div class="medium-5 columns">
						@if($type == 1)
						<label>Naziv banera</label>
						@elseif($type == 2)
						<label>Naziv slajdera</label>
						@elseif($type == 9)
						<label>Naziv popup banera</label>
						@endif
						<div class="relative"> 
							<input id="naziv" value="{{ Input::old('naziv') ? Input::old('naziv') : trim($item->naziv) }}" type="text" name="naziv" />
							<span class="banner-uploads has-tooltip"><input type="file" name="img"><span>Dodaj sliku</span></span>
					 	</div>
				  		<div class="error">{{ $errors->first('naziv') ? $errors->first('naziv') : '' }}</div>
						<div class="error">{{ $errors->first('img') ? ($errors->first('img') == 'Niste popunili polje.' ? 'Niste izabrali sliku.' : $errors->first('img')) : '' }}</div>
					</div>
					<div class="medium-6 columns">
						<label>{{$type==1 ? 'Link banera' : 'Link slajdera'}}</label>
						<input id="link" value="{{ Input::old('link') ? Input::old('link') : trim($item->link) }}" type="text" name="link">
						<div class="error">{{ $errors->first('link') ? $errors->first('link') : '' }}</div>
					</div>
				</section>

				<div class="row">
					<div class="medium-2 columns"> 
						<label>Aktivan</label>
					  	<select name="aktivan">
							<option value="1">DA</option>
							<option value="0" {{ (Input::old('aktivan') == '0') ? 'selected' : (($item->aktivan == 0) ? 'selected' : '') }}>NE</option>
						</select>
					</div>
					@if($type == 1)
					<div class="column medium-3 akcija-column">
						<label>Baner postavljen:</label>
						<input class="akcija-input" id="datum_od_b2b" name="datum_od" autocomplete="off" type="text" value="{{ Input::old('datum_od') ? Input::old('datum_od') : $datum_od }}">
						<span id="datum_od_delete"><i class="fa fa-close tooltipz-left" aria-label="Ukloni"></i></span>
					</div>
					@elseif($type == 2)
					<div class="column medium-3 akcija-column">
						<label>Slajder postavljen:</label>
						<input class="akcija-input" id="datum_od_b2b" name="datum_od" autocomplete="off" type="text" value="{{ Input::old('datum_od') ? Input::old('datum_od') : $datum_od }}">
						<span id="datum_od_delete"><i class="fa fa-close tooltipz-left" aria-label="Ukloni"></i></span>
					</div>
					@elseif($type == 9)
					<div class="column medium-3 akcija-column">
						<label>Pop-Up baner postavljen:</label>
						<input class="akcija-input" id="datum_od_b2b" name="datum_od" autocomplete="off" type="text" value="{{ Input::old('datum_od') ? Input::old('datum_od') : $datum_od }}">
						<span id="datum_od_delete"><i class="fa fa-close tooltipz-left" aria-label="Ukloni"></i></span>
					</div>
					@endif
					<div class="column medium-3 akcija-column">
						<label>Traje Do:</label>
						<input class="akcija-input" id="datum_do_b2b" name="datum_do" autocomplete="off" type="text" value="{{ Input::old('datum_do') ? Input::old('datum_do') : $datum_do }}">
						<span id="datum_do_delete"><i class="fa fa-close tooltipz-left" aria-label="Ukloni"></i></span>
					</div>
				</div>				
			 
				<section class="banner-preview-area">
					<img class="banner-preview" src="{{AdminB2BOptions::base_url()}}{{$item->img}}" alt="{{$item->naziv}}" />
				</section>		
				<div class="banner-display-pages">
					<input type="hidden" name="baneri_id" value="{{$item->baneri_id}}" />
					<input type="hidden" name="tip_prikaza" value="{{$type}}" />
					<div class="btn-container center">
						<button class="btn btn-primary save-it-btn">Sačuvaj</button>
					</div>
				</div>
			</form>
		</div>
	</section>
</section>
@endsection

