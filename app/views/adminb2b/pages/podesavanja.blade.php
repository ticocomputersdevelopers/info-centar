@extends('adminb2b.defaultlayout')
@section('content')
<div id="main-content" class="orders-page row">
	<div class="columns medium-4 flat-box b2b-options">
		<h2 class="title-med">Opcije</h2>
		<div class="row"> 
			@foreach(DB::table('options')->whereIn('options_id',array(3018,3028,3032,3034))->orderBy('options_id','asc')->get() as $row)
				<div class="column medium-12">  
					<label>{{ $row->naziv }}</label>
						<input type="checkbox" class="JSOptionId" data-id="{{ $row->options_id }}" {{ $row->int_data == 1 ? 'checked="checked"' : '' }}>	
				</div>
			@endforeach

		</div>

		<h2 class="title-med">Magacini</h2> 	
		<div class="row"> 
			@foreach(DB::table('orgj')->where('orgj_id','<>',-1)->orderBy('orgj_id','asc')->get() as $row)
				<div class="column medium-12">
					<label>{{ $row->naziv }}</label>
  					<input class="JSOrgjEnable" data-id="{{ $row->orgj_id }}" type="checkbox" {{ $row->b2b == 1 ? 'checked' : '' }} {{ DB::table('imenik_magacin')->where(array('imenik_magacin_id'=>20,'orgj_id'=>$row->orgj_id))->count() == 1 ? 'disabled' : '' }}>
					<input name="orgj_primary" class="JSOrgjPrimary" data-id="{{ $row->orgj_id }}" type="radio" {{ DB::table('imenik_magacin')->where(array('imenik_magacin_id'=>20,'orgj_id'=>$row->orgj_id))->count() == 1 ? 'checked' : '' }} {{ $row->b2b == 1 ? '' : 'disabled' }}>
				</div>
			@endforeach
		</div>

		<h2 class="title-med">Dodaj novi magacin</h2> 	
		<div class="row">
			 <div class="column medium-12">
				<input placeholder="Novi magacin" type="text" id="JSNewMagtacinVal">
			</div>
		</div>
		<div class="row">
			<div class="btn-container text-center">
				<button class="JSNewMagtacinSave btn btn-secondary save-it-btn">Sačuvaj</button>
			</div>
		</div>
	</div>
</div>
@endsection