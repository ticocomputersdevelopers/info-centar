<div class="text-center filters-remove-div">
    <!-- <span class="">Izabrani filteri:</span> -->
    <a class="reset-filters-button" href="{{ Request::url()}}">Poništi filtere</a>
</div>
 
<form class="filter-form" method="get" action="">
    <ul>
        <li>
            <a href="javascript:void(0)" class="filter-links">Proizvođač 
                <i class="fa fa-angle-down"></i>
            </a>
            <div class="select-fields-content">
                @foreach(B2b::getManufacturers($grupa_pr_id,$filters) as $row)
                    @if(isset($filters['proizvodjac']))
                        @if(in_array($row->proizvodjac_id,$filters['proizvodjac']))
                            <label>
                                <input type="checkbox" data-name="proizvodjac" data-type="remove" data-old-value="{{implode('-',$filters['proizvodjac'])}}"  class="filter-item" value="{{ $row->proizvodjac_id }}" checked> {{$row->naziv}} ( {{$row->products}} )
                            </label>
                        @else
                            @if( $filters['proizvodjac'][0]!=null )
                                <label>
                                    <input type="checkbox"  data-name="proizvodjac" data-type="add" data-old-value="{{implode('-',$filters['proizvodjac'])}}" class="filter-item" value="{{ implode('-',$filters['proizvodjac']+[''=>$row->proizvodjac_id]) }}"> {{$row->naziv}} ( {{$row->products}} )
                                </label>
                            @else
                                <label>
                                    <input type="checkbox"  data-name="proizvodjac" data-type="add" data-old-value="{{implode('-',$filters['proizvodjac'])}}"  class="filter-item" value="{{ $row->proizvodjac_id }}"> {{$row->naziv}} ( {{$row->products}} )
                                </label>
                            @endif
                        @endif
                        @else
                            <label>
                                <input type="checkbox"  data-name="proizvodjac" data-type="add" data-old-value=""  class="filter-item" value="{{ $row->proizvodjac_id }}"> {{$row->naziv}} ( {{$row->products}} )
                            </label>
                     @endif
                 @endforeach
            </div>
        </li>
    @foreach($filtersItems as $filter)
        <li>
            <a href="javascript:void(0)" class="filter-links">{{$filter['grupa_naziv']}}
                <i class="fa fa-angle-down"></i>
            </a>
            <div class="select-fields-content">
                @foreach($filter['grupa_pr_vrednost'] as $row)
                    @if(isset($filters[$filter['grupa_naziv_var']]))
                        @if(in_array($row['vrednost_id'],$filters[$filter['grupa_naziv_var']]))
                        <label>
                            <input type="checkbox" data-name="{{$filter['grupa_naziv_var']}}" data-type="remove" data-old-value="{{implode('-',$filters[$filter['grupa_naziv_var']])}}"  class="filter-item" value="{{ $row['vrednost_id'] }}" checked> {{$row['vrednost_naziv']}} ( {{$row['broj_proizvoda']}} )
                        </label>
                        @else
                        @if( $filters[$filter['grupa_naziv_var']][0]!=null )
                        <label>
                            <input type="checkbox"  data-name="{{$filter['grupa_naziv_var']}}" data-type="add" data-old-value="{{implode('-',$filters[$filter['grupa_naziv_var']])}}"  class="filter-item" value="{{ implode('-',$filters[$filter['grupa_naziv_var']]+[''=>$row['vrednost_id']]) }}"> {{$row['vrednost_naziv']}} ( {{$row['broj_proizvoda']}} )
                        </label>
                        @else
                        <label>
                            <input type="checkbox"  data-name="{{$filter['grupa_naziv_var']}}" data-type="add" data-old-value="{{implode('-',$filters[$filter['grupa_naziv_var']])}}"  class="filter-item" value="{{ $row['vrednost_id'] }}"> {{$row['vrednost_naziv']}} ( {{$row['broj_proizvoda']}} )
                        </label>
                        @endif
                    @endif
                    @else
                    <label>
                        <input type="checkbox"  data-name="{{$filter['grupa_naziv_var']}}" data-type="add" data-old-value=""  class="filter-item" value="{{ $row['vrednost_id'] }}"> {{$row['vrednost_naziv']}} ( {{$row['broj_proizvoda']}} )
                    </label>
                    @endif
                @endforeach
            </div>
        </li>
    @endforeach
    </ul>
</form>
 
 