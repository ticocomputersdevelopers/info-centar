<nav id="JScategories" class="category-nav">

	<?php $query_category_first=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>0,'web_b2b_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
    <h3 class="JScategories-titile">Kategorije <i class="fa fa-bars hidden-sm hidden-xs aria-hidden"></i><span class="glyphicon glyphicon-remove JSclose-nav"></span></h3> 
	 
	<!-- CATEGORIES LEVEL 1 -->

    <ul class="JSlevel-1" id="the_categories">
    @if(B2bOptions::category_type()==1) 
		@foreach ($query_category_first as $row1)
			@if(B2bCommon::broj_cerki($row1->grupa_pr_id) >0)

			<li class="level-1-list">
				<a href="{{ B2bOptions::base_url()}}b2b/artikli/{{ B2bUrl::url_convert($row1->grupa) }}" class="">
					@if(B2bCommon::check_image_grupa($row1->putanja_slika))
					<span class="level-1-img-container hidden-sm hidden-xs"> 
						<img class="level-1-img" src="{{ B2bOptions::base_url() }}{{$row1->putanja_slika}}" alt="{{ $row1->grupa }}" />
				 	</span>
					@endif
					{{$row1->grupa}}
				</a>
				 
				<?php $query_category_second=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row1->grupa_pr_id,'web_b2b_prikazi'=>1))->orderBy('redni_broj','asc') ?>

				<ul class="JSlevel-2 row">
			 		@foreach ($query_category_second->get() as $row2)
					<li class="col-md-6 col-sm-6 col-xs-12">  
						@if(B2bCommon::check_image_grupa($row2->putanja_slika))
							<a href="{{ B2bOptions::base_url()}}b2b/artikli/{{ B2bUrl::url_convert($row1->grupa) }}/{{ B2bUrl::url_convert($row2->grupa) }}" class="level-2-container-for-image hidden-sm hidden-xs col-md-2 col-sm-2 col-xs-12">
								<img class="level-2-img" src="{{ B2bOptions::base_url() }}{{$row2->putanja_slika}}" alt="{{ $row2->grupa }}" />
							</a>
						@endif
					 
						<a href="{{ B2bOptions::base_url()}}b2b/artikli/{{ B2bUrl::url_convert($row1->grupa) }}/{{ B2bUrl::url_convert($row2->grupa) }}" 
						class="col-md-10 col-sm-10 col-xs-12">
							{{$row2->grupa}}
						</a>
					 	@if(B2bCommon::broj_cerki($row2->grupa_pr_id) >0)
						<span class=""> 
							<i class="" aria-hidden="true"></i>
						</span>
						@endif  	
					 

						@if(B2bCommon::broj_cerki($row2->grupa_pr_id) >0)
						<?php $query_category_third=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row2->grupa_pr_id,'web_b2b_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
						<ul class="JSlevel-3 row">
							@foreach($query_category_third as $row3)
							
							<li class="col-md-12 col-sm-12 col-xs-12">						 
								<a href="{{ B2bOptions::base_url()}}b2b/artikli/{{ B2bUrl::url_convert($row1->grupa) }}/{{ B2bUrl::url_convert($row2->grupa) }}/{{ B2bUrl::url_convert($row3->grupa) }}" class="col-md-12 col-sm-12 col-xs-12">
									{{$row3->grupa}}
								</a>

								@if(B2bCommon::broj_cerki($row3->grupa_pr_id) >0)
								<span class="JSCategoryLinkExpend">
									<i class="fa fa-chevron-down" aria-hidden="true"></i>
								</span>
								@endif  
 
								@if(B2bCommon::broj_cerki($row3->grupa_pr_id) >0)
								<?php $query_category_forth=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row3->grupa_pr_id,'web_b2b_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
											
								<ul class="JSlevel-4 row">
									@foreach($query_category_forth as $row4)
									<li class="col-md-12 col-sm-12 col-xs-12">
										<a href="{{ B2bOptions::base_url()}}b2b/artikli/{{ B2bUrl::url_convert($row1->grupa) }}/{{ B2bUrl::url_convert($row2->grupa) }}/{{ B2bUrl::url_convert($row3->grupa) }}/{{ B2bUrl::url_convert($row4->grupa) }}" class="">{{$row4->grupa}}</a>
									</li>
									@endforeach
								</ul>
								@endif		
							</li>					 	
							@endforeach
						</ul>
						@endif
					</li>
					@endforeach
				</ul>
			</li>

			@else

			<li class="level-1-list">		 
				<a href="{{ B2bOptions::base_url()}}b2b/artikli/{{ B2bUrl::url_convert($row1->grupa) }}" class="">		 
					@if(B2bCommon::check_image_grupa($row1->putanja_slika))
					<span class="level-1-img-container hidden-sm hidden-xs"> 
						<img class="level-1-img" src="{{ B2bOptions::base_url() }}{{$row1->putanja_slika}}" alt="{{ $row1->grupa }}" />
					</span>
					@endif
					{{$row1->grupa}}  				
				</a>			 
			</li>
			@endif
		@endforeach
	@else
		@foreach ($query_category_first as $row1)
			
			    
		@if(B2bCommon::broj_cerki($row1->grupa_pr_id) >0)
		  	<a href="{{ B2bOptions::base_url()}}b2b/artikli/{{ B2bUrl::url_convert($row1->grupa) }}">
				{{$row1->grupa}}
			</a>
		<?php $query_category_second=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row1->grupa_pr_id,'web_b2b_prikazi'=>1))->orderBy('redni_broj','asc') ?>

		 

		@foreach ($query_category_second->get() as $row2)
			<li>
				<a href="{{ B2bOptions::base_url()}}b2b/artikli/{{ B2bUrl::url_convert($row1->grupa) }}/{{ B2bUrl::url_convert($row2->grupa) }}">
					{{$row2->grupa}}
				</a>
			@if(B2bCommon::broj_cerki($row2->grupa_pr_id) >0)
			<?php $query_category_third=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row2->grupa_pr_id,'web_b2b_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
				<ul class="">
				@foreach($query_category_third as $row3)
				  <a href="{{ B2bOptions::base_url()}}b2b/artikli/{{ B2bUrl::url_convert($row1->grupa) }}/{{ B2bUrl::url_convert($row2->grupa) }}/{{ B2bUrl::url_convert($row3->grupa) }}">{{$row3->grupa}}
				  </a>
				@if(B2bCommon::broj_cerki($row3->grupa_pr_id) >0)
				<?php $query_category_forth=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row3->grupa_pr_id,'web_b2b_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>

				<ul class="">
					@foreach($query_category_forth as $row4)
					<a href="{{ B2bOptions::base_url()}}b2b/artikli/{{ B2bUrl::url_convert($row1->grupa) }}/{{ B2bUrl::url_convert($row2->grupa) }}/{{ B2bUrl::url_convert($row3->grupa) }}/{{ B2bUrl::url_convert($row4->grupa) }}">{{$row4->grupa}}</a>
					@endforeach
				</ul>
				@endif	
				@endforeach
				</ul>
			@endif
			</li>
		@endforeach

		@else
			<li><a href="{{ B2bOptions::base_url()}}b2b/artikli/{{ B2bUrl::url_convert($row1->grupa) }}">{{$row1->grupa}}</a>
		@endif

		</li>
		@endforeach				
	@endif
 
</ul>
 
</nav>
