<div class="product-list table-responsive">
    <table class="table table-condensed table-bordered table-striped"> 
        <thead> 
            <tr> 
                <th>Naziv</th>
                <th>Slika</th>
                <th>Cena</th>
                <th>Rabat</th>
                <th>Cena sa rabatom</th>
                <th>PDV Stopa</th>
                <th>Cena sa PDV-om</th>
                <th>Količina</th>
                <th>Korpa</th>
            </tr>
        </thead>
        <tbody> 
            @foreach($query_products as $row)
            <tr> 
                <td class="relative">
                    <a class="product-title" href="{{B2bOptions::base_url()}}b2b/artikal/{{B2bUrl::url_convert(B2bArticle::seo_title($row->roba_id))}}">{{ B2bArticle::short_title($row->roba_id) }}</a>
                    <div class="addon-title">{{ B2bArticle::addon_title($row->roba_id) }}</div>
                    <a class="article-edit-btn" target="_blank" href="{{ Options::base_url() }}admin/product/{{ $row->roba_id }}">IZMENI ARTIKAL</a>
                </td>
                <td>
                <a href="{{B2bOptions::base_url()}}b2b/artikal/{{B2bUrl::url_convert(B2bArticle::seo_title($row->roba_id))}}" class="product-image-wrapper">
                    <img class="img-responsive" src="{{B2bOptions::base_url()}}{{ B2bArticle::web_slika($row->roba_id) }}" alt="{{ B2bArticle::seo_title($row->roba_id) }}" />
                </a>
                </td>
                <?php
                $rabatCene = B2bArticle::b2bRabatCene($row->roba_id);
                $quantity = B2bArticle::quantityB2b($row->roba_id) - B2bBasket::getB2bQuantityItem($row->roba_id);
                ?>
                <td><span data-toggle="tooltip" title="Cena"> {{B2bBasket::cena($rabatCene->osnovna_cena)}} </span></td>
                <td><span data-toggle="tooltip" title="Rabat"> {{number_format($rabatCene->ukupan_rabat,2)}} % </span></td>
                <td><span data-toggle="tooltip" title="Cena sa rabatom"> {{B2bBasket::cena($rabatCene->cena_sa_rabatom)}} </span></td>
                <td><span data-toggle="tooltip" title="PDV stopa"> {{number_format($rabatCene->porez,2)}} % </span></td>
                <td><span data-toggle="tooltip" title="Cena sa PDV-om"> {{B2bBasket::cena($rabatCene->ukupna_cena,2)}} </span></td>
                <td><span data-toggle="tooltip" title="Količina"> {{$quantity <= 10 ? $quantity :'> 10'}} </span></td>
                <td>
                    @if(B2bArticle::getStatusArticle($row->roba_id) == 1)
                        @if($quantity>0)
                            <input class="add-amount" id="quantity-{{$row->roba_id}}" type="text" value="1">
                            <button class="add-to-cart-products" data-max-quantity="{{$quantity}}" data-roba-id="{{$row->roba_id}}"> 
                                <i class="fa fa-shopping-cart"></i> U korpu
                            </button>
                        @else
                            <span>Nije dostupan</span>
                            @endif
                        @else
                        <span>{{ B2bArticle::find_flag_cene(B2bArticle::getStatusArticle($row->roba_id),'naziv') }}</span>
                    @endif 
                </td> 
            </tr>
            @endforeach
        </tbody> 
    </table>
</div>