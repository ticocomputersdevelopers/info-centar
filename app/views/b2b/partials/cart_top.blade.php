<ul class="header-cart-content" id="header-cart-content">
    @if(Session::has('b2b_cart') AND isset($items))
        @include('b2b/partials/ajax/header_cart')
    @endif
</ul>