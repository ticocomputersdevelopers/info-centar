@foreach($query_products as $row)
<div class="col-md-4 col-sm-6 col-xs-12 no-padding product"> 
	<div class="product-content sm-text-center">
		<a href="{{B2bOptions::base_url()}}b2b/artikal/{{B2bUrl::url_convert(B2bArticle::seo_title($row->roba_id))}}" class="product-image-wrapper">
			<img class="img-responsive" src="{{B2bOptions::base_url()}}{{ B2bArticle::web_slika($row->roba_id) }}" alt="{{ B2bArticle::seo_title($row->roba_id) }}" />
		</a>
		<div class="product-info clearfix">
			<?php
			$rabatCene = B2bArticle::b2bRabatCene($row->roba_id);
			$quantity = B2bArticle::quantityB2b($row->roba_id) - B2bBasket::getB2bQuantityItem($row->roba_id);
			?>
			<div class="text-right quantity-text sm-text-center"><span> Količina: {{$quantity <= 10 ? $quantity :'> 10'}} </span></div>
			<div class="product-price"><span> {{B2bBasket::cena($rabatCene->ukupna_cena,2)}} </span></div>
			<a class="product-title" href="{{B2bOptions::base_url()}}b2b/artikal/{{B2bUrl::url_convert(B2bArticle::seo_title($row->roba_id))}}">
				{{ B2bArticle::short_title($row->roba_id) }}
			</a> 
			<div class="addon-title">{{ B2bArticle::addon_title($row->roba_id) }}</div>
			<div class="add-to-cart-container"> 
				@if(B2bArticle::getStatusArticle($row->roba_id) == 1)
					@if($quantity>0)
						<input class="add-amount" id="quantity-{{$row->roba_id}}" type="text" value="1">
						<button class="add-to-cart-products" data-max-quantity="{{$quantity}}" data-roba-id="{{$row->roba_id}}"> 
							<i class="fa fa-shopping-cart"></i> U korpu
						</button>
					@else
						<button class="dodavnje not-available">Nije dostupan</button>
						@endif
					@else
					<button>{{ B2bArticle::find_flag_cene(B2bArticle::getStatusArticle($row->roba_id),'naziv') }}</button>
				@endif
			</div>
		</div>
		<a class="article-edit-btn" target="_blank" href="{{ Options::base_url() }}admin/product/{{ $row->roba_id }}">IZMENI ARTIKAL</a>
	</div>
</div>
@endforeach


      
 