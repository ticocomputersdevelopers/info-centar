<div class="shopping-information container">
    <br>
    <h2><span class="heading-background">Završena kupovina</span></h2>
    <div class="table-responsive"> 
        <table class="table table-striped table-bordered table-condensed table-hover">
            <tbody>
                <tr class="info"> 
                    <th colspan="2">Informacije o narudžbini:</th>
                </tr>
                <tr>
                    <td>Broj porudžbine:</td>
                    <td>{{$order->broj_dokumenta}}</td>
                </tr>
                <tr>
                    <td>Datum porudžbine:</td>
                    <td>{{date('d.m.Y',strtotime($order->datum_dokumenta))}}</td>
                </tr>
                <tr>
                    <td>Način isporuke:</td>
                    <td>{{B2bBasket::getNameNacinIsporuke($order->web_nacin_isporuke_id)}}</td>
                </tr>
                <tr>
                    <td>Način plaćanja:</td>
                    <td>{{B2bBasket::getNameNacinPlacanja($order->web_nacin_placanja_id)}}</td>
                </tr>
                <tr>
                    <td>Napomena:</td>
                    <td>{{$order->napomena}}</td>
                </tr>
                 <tr class="info"> 
                    <th colspan="2">Informacije o kupcu:</th>
                </tr>
                <tr>
                    <td>Naziv:</td>
                    <td>{{$partner->naziv}}</td>
                </tr>
                <tr>
                    <td>PIB:</td>
                    <td>{{$partner->pib}}</td>
                </tr>
                <tr>
                    <td>Matični broj:</td>
                    <td>{{$partner->broj_maticni}}</td>
                </tr>
                <tr>
                    <td>Adresa:</td>
                    <td>{{$partner->adresa}}</td>
                </tr>
                <tr>
                    <td>Mesto:</td>
                    <td>{{ $partner->mesto }}</td> 
                </tr>
                <tr>
                    <td>Telefon:</td>
                    <td>{{$partner->telefon}}</td>
                </tr>
                <tr>
                    <td>Email:</td>
                    <td>{{$partner->mail}}</td>
                </tr>
                <tr class="info"> 
                    <th colspan="2">Informacije o prodavcu:</th>
                </tr>
                <tr>
                    <td>Naziv prodavca:</td>
                    <td>{{B2bOptions::company_name()}}</td>
                </tr>
                <tr>
                    <td>Adresa:</td>
                    <td>{{B2bOptions::company_adress()}}</td>
                </tr>
                <tr>
                    <td>Telefon:</td>
                    <td>{{B2bOptions::company_phone()}}</td>
                </tr>
                <tr>
                    <td>Fax:</td>
                    <td>{{B2bOptions::company_fax()}}</td>
                </tr>
                <tr>
                    <td>PIB:</td>
                    <td>{{B2bOptions::company_pib()}}</td>
                </tr>
                <tr>
                    <td>Šifra delatnosti:</td>
                    <td>{{B2bOptions::company_delatnost_sifra()}}</td>
                </tr>
                <tr>
                    <td>Žiro račun:</td>
                    <td>{{B2bOptions::company_ziro()}}</td>
                </tr>
                <tr>
                    <td>E-mail:</td>
                    <td>{{B2bOptions::company_email()}}</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="table-responsive"> 
        <table class="table table-bordered table-striped table-condensed">
            <tbody>
                <tr class="info"> 
                    <th colspan="7">Proizvodi:</th>
                </tr>
                <tr>
                    <th>Naziv </th>
                    <th>Cena </th>
                    <th>Rabat </th>
                    <th>Cena sa rab. </th>
                    <th>PDV </th>
                    <th>Količina </th>
                    <th>Ukupno </th>
                </tr>
                @foreach($orderItems as $row)
                <?php
                $rabatCene = B2bArticle::b2bRabatCene($row->roba_id);
                ?>
                <tr>
                    <td>{{B2bArticle::getName($row->roba_id)}} </td>
                    <td>{{number_format($rabatCene->osnovna_cena,2)}} </td>
                    <td>{{number_format(($rabatCene->ukupan_rabat),2)}} % </td>
                    <td>{{number_format($rabatCene->cena_sa_rabatom,2)}} </td>
                    <td>{{number_format($rabatCene->porez,2)}} %</td>
                    <td>{{(int)$row->kolicina }} </td>
                    <td>{{number_format($rabatCene->ukupna_cena*$row->kolicina,2)}} </td>
                </tr>
                @endforeach
                <?php
                $orderTotal = B2bBasket::orderTotal($order->web_b2b_narudzbina_id);
                ?>
                <tr class="final-price">
                    <th colspan="5"></th>
                    <th colspan="1">Osnovica:</th>
                    <th colspan="1">{{number_format($orderTotal->cena_sa_rabatom,2)}}</th>
                </tr>
                <tr class="final-price">
                    <th colspan="5"></th>
                    <th colspan="1">PDV:</th>
                    <th colspan="1">{{number_format($orderTotal->porez_cena,2)}}</th>
                </tr>
                <tr class="final-price">
                    <th colspan="5"></th>
                    <th colspan="1">Ukupno:</th>
                    <th colspan="1">{{number_format($orderTotal->ukupna_cena,2)}}</th>
                </tr>
            </tbody>
        </table>
    </div>
</div>