<div class="shopping-information container">
    <br>
    <h2><span class="heading-background">PRIJAVA ZA SERVIS</span></h2>
      <div class="table-responsive"> 
        <table class="table table-bordered table-striped table-condensed">
            <tbody>

                <tr class="info"> 
                    <th colspan="7">Informacije o uređaju:</th>
                </tr>
                <tr>
                   
                    <th>Uredjaj: {{$naziv}} </th>
                    <th>Datum kupovine: {{$datum}} </th>
                    <th>Broj fakture: {{$brfakture}} </th>
                    <th>Serijski broj: {{$serial}} </th>
                    <th>Opis kvara: {{$opis}}</th>
                </tr>         
               
            </tbody>
        </table>
    </div>
</div>