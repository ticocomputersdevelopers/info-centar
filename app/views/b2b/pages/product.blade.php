@extends('b2b.templates.main')
 
@section('content')
    <?php B2bCommon::articleViewB2B($roba_id); ?> 
<div class="main-content">
    <div class="row"> 
        <div class="col-md-12 col-sm-12 col-xs-12 no-padding"> 
            <ul class="breadcrumb">
                {{ B2bArticle::product_bredacrumps(DB::table('roba')->where('roba_id',$roba_id)->pluck('grupa_pr_id')) }}
            </ul>
        </div>
    </div>

    <div id="product-preview" itemscope itemtype="http://schema.org/Product" class="clearfix">
        <div class="row">
  <!-- PRODUCT PREVIEW IMAGE -->
        <div class="product-preview-image col-md-6 col-sm-6 col-xs-12">
            <div class="product-view">   
                <img id="JSarticle-img" itemprop="image" class="img-responsive" src="{{ B2bOptions::base_url() }}{{ B2bArticle::web_slika_big($roba_id) }}" 
                alt="{{ B2bArticle::seo_title($roba_id)}}"/>

                <div id="JSarticle-modal-container" class="article-modal-container">  
                    <div class="article-modal-container-inner"> 
                        <span class="JSarticle-modal-close">&times;</span>
                        <div class="modal-img-container"> 
                            <img id="JSarticle-modal-img" class="img-responsive article-modal-img" src="{{ B2bOptions::base_url() }}{{ B2bArticle::web_slika_big($roba_id) }}" alt="{{ B2bArticle::seo_title($roba_id)}}"/>
                        </div>
                        <div class="modal-galery-container"> {{ B2bArticle::get_list_images($roba_id) }} </div>
                    </div>
                </div>
            </div>
            <div class="JSImgLoop"> 
                {{ B2bArticle::get_list_images($roba_id) }}  
            </div>
        </div>
                         
            <div class="col-md-6 col-sm-6 col-xs-12 product-preview-info"> 
    <!-- PRODUCT PREVIEW INFO -->
                <?php
                if(B2bOptions::vodjenje_lagera()==1)
                    {$dost_kol = B2bArticle::quantityB2b($roba_id) - B2bBasket::getB2bQuantityItem($roba_id);}
                else {
                    $dost_kol=1;
                } ?>
              
                <h1 class="article-heading" itemprop="name">{{ B2bArticle::seo_title($roba_id)}}</h1>
                <ul>
                    <li>Šifra:{{ B2bArticle::sifra_is($roba_id) }}</li>
                    <li>Proizvod iz grupe:{{ B2bArticle::get_grupaB2b($roba_id) }}</li>
                    <li itemprop="brand">Proizvodjač:{{ B2bArticle::get_proizvodjac($roba_id) }}</li>
                    @if(B2bOptions::vodjenje_lagera() == 1)
                        <li>Dostupna količina:{{$dost_kol}} </li>
                    @endif
                </ul>
                <div class="product-preview-price">
                    <span itemprop="price">{{ B2bBasket::cena(B2bArticle::b2bRabatCene($roba_id)->ukupna_cena) }}</span>
                </div>
                <div class="add-to-cart-area">
                    @if(B2bArticle::getStatusArticle($roba_id) == 1)
                        @if($dost_kol>0)
                            <input class="add-amount" data-max-quantity="{{$dost_kol}}" type="text" value="1">
                            <button class="add-to-cart add-to-cart-btn" data-roba-id="{{$roba_id}}">Dodaj u korpu</button>
                            <!-- <a class="add-amount-less"  href="javascript:void(0)"><</a> -->
                            <!-- <a class="add-amount-more" data-max-quantity="{{$dost_kol}}" href="javascript:void(0)">></a> -->
                        @else
                            <button class="add-to-cart not-available">Nije dostupno</button>
                        @endif
                        @else
                        <button class="add-to-cart" >{{ B2bArticle::find_flag_cene(B2bArticle::getStatusArticle($roba_id),'naziv') }}</button>
                    @endif
                </div>
                <div class="facebook-btn-share">
                    <div class="fb-share-button" data-href="{{B2bOptions::base_url()}}b2b/artikal/{{B2bUrl::url_convert(B2bArticle::seo_title($roba_id))}}" data-layout="button"></div>
                </div>
            </div>
        </div>
        
  <!-- PRODUCT PREVIEW TABS-->
        <div class="product-preview-tabs">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#home">Opis</a></li>
                <li><a data-toggle="tab" href="#tech_docs">Sadržaji</a></li>
                <!-- <li><a data-toggle="tab" href="#menu2">Menu 2</a></li> -->
            </ul>
            <div class="tab-content">
                <div id="home" class="tab-pane fade in active">
                    <p itemprop="description">{{ B2bArticle::get_opis($roba_id) }}</p>
                    {{ B2bArticle::get_karakteristike($roba_id) }}
                </div>
                <div id="tech_docs" class="tab-pane fade">
                   @if(B2bOptions::web_options(120) && count($fajlovi) > 0)
                      <div>
                          @foreach($fajlovi as $row)
                          <div class="files-list-item">
                              <a class="files-link" href="{{ $row->putanja != null ? Options::domain().$row->putanja : $row->putanja_etaz }}" target="_blank">
                                  <img src="{{ Options::domain() }}images/file-icon.png" alt="{{ $row->naziv }}">
                                  <div class="files-list-item">
                                      <div class="files-name">{{ $row->naziv }}</div> 
                                  </div>
                              </a>
                          </div>
                          @endforeach
                      </div>
                   @endif
                </div>
         <!--        <div id="menu2" class="tab-pane fade">
                   <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
                </div> -->
            </div>
        </div>
 
   <!-- TAGS -->
        @if(B2bArticle::tags_count($roba_id)>1)
            {{ B2bArticle::get_tags($roba_id) }}
        @endif
        <!-- RELATED PRODUCTS -->
        <br>
        <h2 class="JSLinked"><span class="heading-background">Srodni proizvodi</span></h2>
        <div class="related-products product-slider">
            {{ B2bArticle::get_relatedB2b($roba_id) }}
        </div>
    </div> 
</div>
@endsection

@section('footer')
     
    <!-- <script src="{{ B2bOptions::base_url()}}js/jquery-1.11.2.min.js" type="text/javascript" ></script> -->
    <script src="{{ B2bOptions::base_url()}}js/slick.min.js" type="text/javascript" ></script>
    <!-- <script src="{{ B2bOptions::base_url()}}js/jquery.elevateZoom-3.0.8.min.js" type="text/javascript" ></script>
    <script src="{{ B2bOptions::base_url()}}js/jquery.fancybox.pack.js" type="text/javascript" ></script> -->
    <!-- <script src="{{ B2bOptions::base_url()}}js/b2b/b2b_main.js" type="text/javascript" ></script> -->

    <script>
        $(document).ready(function () {
           var id = {{$roba_id}};
           // $('.add-amount-less').click(function (){
           //    var quantity = $('.add-amount').val();
           //    if(quantity <= 1){
           //        $('.info-popup').fadeIn().delay(2000).fadeOut();

           //        $('.info-popup .popup-inner').html("<p class='p-info'>Minimalna količina je 1 kom.</p>");
           //    }
           //    else {
           //        $('.add-amount').val(quantity-1);
           //    }
           // });

           //  $('.add-amount-more').click(function (){
           //      var quantity = $('.add-amount').val();
           //      var max = $(this).data('max-quantity');
           //      if(quantity == max){
           //          $('.info-popup').fadeIn().delay(2000).fadeOut();

           //          $('.info-popup .popup-inner').html("<p class='p-info'>Maksimalna količina je "+max+" kom.</p>");
           //      }
           //      else {
           //          $('.add-amount').val(Number(quantity)+1);
           //      }
           //  });

           //  $('.add-amount').change(function() {
           //     var quantity = $(this).val();
           //     var max = $(this).data('max-quantity');
           //      if(quantity <= 1){
           //          $('.info-popup').fadeIn().delay(2000).fadeOut();

           //          $('.info-popup .popup-inner').html("<p class='p-info'>Minimalna količina je 1 kom.</p>");
           //          $(this).val(1);
           //      }
           //      else if(quantity >= max){
           //          $('.info-popup').fadeIn().delay(2000).fadeOut();

           //          $('.info-popup .popup-inner').html("<p class='p-info'>Maksimalna količina je "+max+" kom.</p>");
           //          $(this).val(max);
           //      }

           //  });
            $('.add-to-cart-btn').click(function (){
                var quantity = $('.add-amount').val();
                var max = $('.add-amount').data('max-quantity');
                if(quantity < 1){
                    $('.info-popup').fadeIn().delay(1200).fadeOut();
                    $('.info-popup .popup-inner').html("<p class='p-info'>Minimalna količina je 1 kom.</p>");
                    $(this).val(1);
                }
                else if(quantity > max){
                    $('.info-popup').fadeIn().delay(1200).fadeOut();
                    $('.info-popup .popup-inner').html("<p class='p-info'>Maksimalna količina je "+max+" kom.</p>");
                    $(this).val(max);
                }
                else {
                    var _this = $(this);
                    $.ajax({
                        type: "POST",
                        url:'{{route('b2b.cart_add')}}',
                        cache: false,
                        data:{roba_id:id, status:2, quantity:quantity},  
                        success:function(res){
                            $('.info-popup').fadeIn().delay(1200).fadeOut();
                            $('.info-popup .popup-inner').html("<p class='p-info'>Artikal je dodat u korpu.</p>");
                            $('#broj_cart').text(res.countItems);
                            $('#header-cart-content').html(res.cartContent);
                            $('.add-amount').data('max-quantity', res.cartAvailable);
                            $('.add-amount-more').data('max-quantity', res.cartAvailable);
                            location.reload();
                        }
                    });
                }
            });
            // $('.addCart').click(function(){
            //    var roba_id = $(this).data('product-id');
            //    var max = $(this).data('max-quantity');
            //     if(max <=0 ){
            //         $('.info-popup').fadeIn().delay(800).fadeOut();
            //         $('.info-popup .popup-inner').html("<p class='p-info'>Za dati arikal dodali ste maksimalnu količinu u korpu.</p>");
            //     }
            //     else {
            //         var _this = $(this);
            //         $.ajax({
            //             type: "POST",
            //             url:'{{route('b2b.cart_add')}}',
            //             cache: false,
            //             data:{roba_id:roba_id, status:2, quantity:1},
            //             success:function(res){
            //                 $('.info-popup').fadeIn().delay(800).fadeOut();
            //                 $('.info-popup .popup-inner').html("<p class='p-info'>Artikal je dodat u korpu.</p>");
            //                 location.reload();
            //             }
            //         })
            //     }
            // });
          });

    </script>
@endsection