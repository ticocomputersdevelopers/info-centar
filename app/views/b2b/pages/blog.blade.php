@extends('b2b.templates.main')
@section('content') 
<div class="main-content">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12 single-news-container">
			<img class="img-responsive" src="{{ B2bOptions::base_url() . $vest->slika }}" alt="{{ $vest->naslov }}">
			<h2><span class="heading-background"> {{ $vest->naslov }}</span></h2>
			<p>{{ $vest->sadrzaj }}</p>
		</div>
	</div>   
</div>
@endsection