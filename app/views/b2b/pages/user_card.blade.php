@extends('b2b.templates.main')
@section('content') 
<div class="main-content">
    <br>
    <h2><span class="heading-background">Vaša Kartica</span></h2>
    <div class="row user-card-row">
        <div class="col-md-3 col-sm-3 col-xs-6"> 
            <span>Filtriraj: </span>
            <label>Datum od:</label>
            <input type="text" value="{{ !is_null($date_start) ? $date_start : '' }}" id="date_start">
        </div>

        <div class="col-md-3 col-sm-3 col-xs-6">
            <label>Datum do:</label>
            <input type="text" id="date_end" value="{{ !is_null($date_end) ? $date_end : '' }}">
        </div>

        <div class="col-md-6 col-sm-6 col-xs-12">
            <button id="JSFilterDate">Potvrdi</button>
            <a href="{{B2bOptions::base_url()}}b2b/user/card" role="button"><i class="fa fa-close"></i> Poništi filtere</a>
            @if(B2bOptions::info_sys('calculus') || B2bOptions::info_sys('infograf') || B2bOptions::info_sys('logik'))
                <form method="POST" action="{{B2bOptions::base_url()}}b2b/user/card-refresh">
                    <button onClick="this.form.submit(); this.disabled=true;">Osveži karticu</button>
                </form>
            @endif
        </div>  
    </div>

    <div class="table-responsive user-card-table"> 
        <table class="table table-condensed table-striped table-bordered table-hover"> 
            <thead> 
                <tr> 
                    <th>Vrsta dokumenta</th>
                    <th>Datum dokumenta</th> 
                    <th>Datum dospeća</th>
                    <th>Duguje</th>
                    <th>Potražuje</th>
                </tr>
            </thead>
            <tbody> 
            @foreach($cart_items as $item)
                <tr> 
                    <td>{{$item->vrsta_dokumenta}}</td>
                    <td>{{date("d-m-Y",strtotime($item->datum_dokumenta))}}</td>
                    <td>{{date("d-m-Y",strtotime($item->datum_dospeca))}}</td>
                    <td>{{number_format($item->duguje,2)}}</td>
                    <td>{{number_format($item->potrazuje,2)}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="row"> 
        <ul class="user-card-total-price col-md-4 col-sm-6 col-xs-12 pull-right">
            <li>Ukupno dugovanje : {{number_format($sumDuguje,2)}} </li>
            <li>Ukupno potraživanje :  {{number_format($sumPotrazuje,2)}} </li>
            <li>Saldo :  {{number_format($saldo,2)}} </li>
        </ul>
    </div>

    <div class="row">
        {{ $cart_items->links() }}
    </div> 
</div>
@endsection

@section('footer')
<!-- <script src="{{ B2bOptions::base_url()}}js/jquery-1.11.2.min.js" type="text/javascript" ></script> -->
<!-- <script src="{{ B2bOptions::base_url()}}js/b2b/main_function.js" type="text/javascript" ></script>
<script src="{{ B2bOptions::base_url()}}js/b2b/b2b_main.js" type="text/javascript" ></script> -->
<!-- <script src="{{ B2bOptions::base_url()}}js/jquery.fancybox.pack.js" type="text/javascript" ></script> -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
$(function() {
    $( "#date_start, #date_end").datepicker({
        dateFormat: 'yy-mm-dd'
    });
});

$('#date_start, #date_end').keydown(false);

$('#JSFilterDate').click(function(){
    var datum_start = $('#date_start').val();
    var datum_end = $('#date_end').val();
    if(datum_start == ''){
        datum_start = 'null';
    }
        if(datum_end == ''){
        datum_end = 'null';               
    }
    location.href = "{{ B2bOptions::base_url() }}b2b/user/card/"+datum_start+"/"+datum_end;
});
</script>
@endsection