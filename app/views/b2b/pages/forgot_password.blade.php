@extends('b2b.templates.login')
@section('content')
 
<div class="b2bLoginContainer">
    <div class="b2b-login row b2b-flex">
        <div class="col-md-5 col-sm-5 col-xs-12 center"> 
            <a class="logo-b2b-login" href="{{ B2bOptions::base_url()}}" title="{{ B2bOptions::company_name()}}">
                <img class="b2b-login-img" src="{{ B2bOptions::base_url()}}{{ B2bOptions::company_logo()}}" alt="">
            </a>
        </div> 
        <div class="col-md-7 col-sm-7 col-xs-12 left-border"> 
            <div class="b2bForm"> 
               
                <form action="{{route('b2b.forgot_password_post')}}" method="post" >
                    <div class="row">
                        <div class="col-md-7 col-sm-7 no-padd center"> 
                            <a href="{{route('b2b.registration')}}" ><p class="center no-margin">REGISTRACIJA</p></a> 
                        </div>
                        <div class="col-md-5 col-sm-5 no-padd"> 
                            <a class="submit b2bRegisterBtn center right" href="login">PRIJAVA</a>
                        </div>
                    </div><br>   
                    <div class="field-group"> 
                        <label for="email" class="text-left">Email</label>
                        <input id="mail" name="mail" class="login-form__input form-control" value="{{ Input::old('mail') }}">
                        <div class="error">{{$errors->first('mail')}}</div>
                    </div> 
                    <div class="btn-container center">
                        <button class="submit admin-login btn">Potvrdi</button>
                    </div>
                    <!--   <a class="submit btn became-partner" href="login">Otkaži</a>
                    <a class="submit btn became-partner" href="{{route('b2b.registration')}}" >Postanite partner</a>  -->
                </form> 
                <span class="text-right">B2B ENGINE BY: 
                <a href="https://www.selltico.com/" target="_blank"> <img src="{{ Options::base_url()}}images/logo-selltico.png">     </a>
                </span> 
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    @if(Session::has('forgot_message'))
    bootbox.alert({
        message: "{{ Session::get('forgot_message') }}"
    });
    @endif
</script>
@endsection