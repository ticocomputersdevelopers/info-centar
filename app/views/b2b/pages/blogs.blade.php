@extends('b2b.templates.main')
@section('content')
<div class="main-content">
	<div class="news-section">
		@foreach($news as $row)
			<div class="news row">
				<div class="col-md-3 col-sm-3 col-xs-12">
					<a href="{{ B2bOptions::base_url() }}b2b/blog/{{ $row->web_vest_b2b_id }}">
						<img class="img-responsive" src="{{ B2bOptions::base_url() . $row->slika }}" alt="{{$row->naslov}}">
					</a>
				</div>
				<div class="col-md-9 col-sm-9 col-xs-12"> 
					<h2><span class="heading-background"> {{ $row->naslov }}</span></h2> 
					<p>{{ B2bVesti::shortNewDesc($row->tekst) }}</p>
					<div class="text-right"> 
						<a class="news-read" href="{{ B2bOptions::base_url() }}b2b/blog/{{ $row->web_vest_b2b_id }}">Pročitaj članak</a>
					</div>
				</div>
			</div>
		@endforeach
		<div>
			{{ $news->links() }}
		</div>
	</div>
</div>
@endsection