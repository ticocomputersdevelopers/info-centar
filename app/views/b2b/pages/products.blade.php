@extends('b2b.templates.main')
@section('content') 
<div class="main-content">
    <div class="row">
        <ul class="breadcrumb">
            @if($strana!='pretraga' and $strana!='filteri' and $strana!='proizvodjac')
                {{ B2bUrl::b2bBreadcrumbs()}}
            @elseif($strana == 'proizvodjac')
                <li><a href="{{ Options::base_url() }}b2b/brendovi">Brendovi</a></li>
                @if($grupa)
                <li><a href="{{ Options::base_url() }}b2b/proizvodjac/{{ B2bUrl::url_convert($proizvodjac) }}">{{ B2bCommon::get_manofacture_name($proizvodjac_id) }}</a></li>
                <li>{{ !is_null($grupa_pr_id) ? B2bCommon::grupa_title($grupa_pr_id) : '' }}</li>
                @else
                    <li>{{ B2bCommon::get_manofacture_name($proizvodjac_id) }} </li>
                @endif
            @else                   
                <li><a href="{{ B2bOptions::base_url() }}b2b">{{ All::get_title_page_start() }}</a></li>
                <li>{{$title}}</li>
            @endif
        </ul>
    </div>

    <div class="row"> 
        <div class="col-md-3 col-sm-3 col-xs-12 all-filters"> 
            @if(Route::getCurrentRoute()->getName()=='products_first' or  Route::getCurrentRoute()->getName()=='products_second' or Route::getCurrentRoute()->getName() == 'products_third' or Route::getCurrentRoute()->getName() == 'products_fourth')
                @include('b2b.partials.filters')
            @elseif($strana == 'proizvodjac')
                @include('b2b.partials.manufacturer_category')
            @endif
        </div>
        <div class="col-md-9 col-sm-9 col-xs-12 no-padding">
            @if($strana != 'proizvodjac' && isset($sub_cats) and !empty($sub_cats) and $url)
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    @foreach($sub_cats as $row)
                    <a href="{{ B2bOptions::base_url()}}b2b/artikli/{{ $url }}/{{ B2bUrl::url_convert($row->grupa) }}">
                        <div class="col-md-3 col-sm-3 col-xs-12 sub_category_item">
                            @if(isset($row->putanja_slika))
                            <img src="{{ B2bOptions::domain() }}{{ $row->putanja_slika }}" alt="{{ $row->grupa }}" class="col-md-3 col-sm-3 col-xs-3 img-responsive" />
                            <p class="col-md-8 col-sm-8 col-xs-8 text-center">
                                {{ $row->grupa }}
                            </p>
                            @else
                            <img src="{{ B2bOptions::domain() }}images/No-image-available.jpg" alt="{{ $row->grupa }}" class="col-md-3 col-sm-3 col-xs-3 img-responsive" />
                            <p class="col-md-8 col-sm-8 col-xs-8 text-center">
                                {{ $row->grupa }}
                            </p>
                            @endif
                        </div>
                    </a>
                    @endforeach
                </div>
            </div>
            @endif
            <div class="row product-list-options">  <!-- ================= PRODUCT INFO OPTIONS ===================== -->
                <div class="col-md-7 col-sm-8 col-xs-12 sm-text-center"> 
                    <span>Ukupno: <b>{{ $count_products }}</b></span>

                    @if(B2bOptions::product_number()==1)
                        <div class="dropdown">
                            <button class="btn currency-btn dropdown-toggle" type="button" data-toggle="dropdown"> 
                                @if(Session::has('limit'))
                                    {{Session::get('limit')}}
                                @else
                                    20
                                @endif
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu currency-list">
                                <li><a href="{{ B2bOptions::base_url() }}limit/20">20</a></li>
                                <li><a href="{{ B2bOptions::base_url() }}limit/30">30</a></li>
                                <li><a href="{{ B2bOptions::base_url() }}limit/50">50</a></li>
                            </ul>
                        </div>
                    @endif

                    @if(B2bOptions::product_sort()==1)
                        <div class="dropdown"> 
                            <button class="btn currency-btn dropdown-toggle" type="button" data-toggle="dropdown">
                                {{B2bCommon::get_sort()}} 
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu currency-list">
                                @if(B2bOptions::web_options(207) == 0)
                                <li><a href="{{route('b2b.ordering',['price_asc'])}}">Cena - Rastuće</a></li>
                                <li><a href="{{route('b2b.ordering',['price_desc'])}}">Cena - Opadajuće</a></li>
                                @else
                                <li><a href="{{route('b2b.ordering',['price_desc'])}}">Cena - Opadajuće</a></li>
                                <li><a href="{{route('b2b.ordering',['price_asc'])}}">Cena - Rastuće</a></li>
                                @endif
                                <li><a href="{{route('b2b.ordering',['news'])}}">Najnovije</a></li>
                                <li><a href="{{route('b2b.ordering',['name'])}}">Prema nazivu</a></li>
                            </ul>
                        </div>
                    @endif 
                   
                    @if(B2bArticle::b2bproduct_view()==1)
                        <div class="view-buttons"> 
                            <a href="{{ B2bOptions::base_url() }}b2b/prikaz/table">
                                <span class="{{(Session::has('b2b_prikaz') AND Session::get('b2b_prikaz') == 'table' OR !Session::has('b2b_prikaz')) ? 'active' : '' }}">
                                     <i class="glyphicon glyphicon-menu-hamburger"></i> 
                                </span>
                            </a>
                            <a href="{{ B2bOptions::base_url() }}b2b/prikaz/list">
                                <span class="{{(Session::has('b2b_prikaz') AND Session::get('b2b_prikaz') == 'list') ? 'active' : '' }}">
                                    <i class="glyphicon glyphicon-th-list"></i>
                                </span>
                            </a>
                            <a href="{{ B2bOptions::base_url() }}b2b/prikaz/grid">
                                <span class="{{(Session::has('b2b_prikaz') AND Session::get('b2b_prikaz') == 'grid') ? 'active' : '' }}">
                                    <i class="glyphicon glyphicon-th"></i>
                                </span>
                            </a>       
                        </div>
                    @endif  
                </div>
                
  <!-- ========= PAGINATION ==============-->
                <div class="col-md-5 col-sm-4 col-xs-12 text-right"> 
                    @if(Route::getCurrentRoute()->getName()=='b2b.search')
                        {{ $query_products->appends(['q'=>Input::get('q')])->links() }}
                    @else
                        {{ $query_products->links() }}
                    @endif
                </div>
            </div>  

            <div class="row">       <!-- ============ PRODUCT CONTENT ============== -->
                @if(Session::has('b2b_prikaz'))
                    @if(Session::get('b2b_prikaz') == 'table')
                        @include('b2b.partials/product_on_table')        
                    @elseif(Session::get('b2b_prikaz') == 'list')
                        @include('b2b.partials/product_on_list')
                    @elseif(Session::get('b2b_prikaz') == 'grid')
                        @include('b2b.partials/product_on_grid')
                    @endif   
                @else
                     @include('b2b.partials/product_on_table')       
                @endif

                @if($count_products == 0)
                    <p class="empty-page"> Trenutno nema artikla za date kategorije</p> 
                @endif 
            </div>
<!--========== PAGINATION DOWN ========= -->
            <div class="row text-right"> 
                @if(Route::getCurrentRoute()->getName()=='b2b.search')
                    {{ $query_products->appends(['q'=>Input::get('q')])->links() }}
                @else
                    {{ $query_products->links() }}   
                @endif
            </div>
        </div>
    </div> 
</div>
@endsection

@section('footer')
 
@endsection
