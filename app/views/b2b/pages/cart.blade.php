@extends('b2b.templates.main')
@section('content')
<div class="main-content">
    <br>
    <h2><span class="heading-background">Vaša Korpa</span></h2>
    @if(Session::has('b2b_cart') and B2bBasket::b2bCountItems() > 0)
        <div class="cart-labels table-responsive">
        <table class="table table-condensed table-striped table-bordered"> 
            <thead> 
                <tr> 
                    <th colspan="2">Proizvod</th>
                    <th>Cena</th>
                    <th>Rabat</th>
                    <th>Cena sa rab.</th>
                    <th>PDV %</th>
                    <th>Količina </th> 
                    <th>Ukupno</th>
                    <th>Ukloni</th> 
                </tr>
            </thead>

            <tbody> 
                @foreach($items as $row)
                <tr>
                    <td class="cart-image">
                        <img class="img-responsive" src="{{B2bOptions::base_url()}}{{ B2bArticle::web_slika($row->roba_id) }}" alt="{{ ($row->roba_id) }}">
                    </td>
                    <td>
                        <a href="{{B2bOptions::base_url()}}b2b/artikal/{{B2bUrl::url_convert(B2bArticle::seo_title($row->roba_id))}}">
                            {{B2bArticle::short_title($row->roba_id)}}
                        </a>
                    </td>

                    <?php
                    $rabatCene = B2bArticle::b2bRabatCene($row->roba_id);
                    $totalItem = $rabatCene->ukupna_cena*$row->kolicina;
                    ?>
                    <td>{{B2bBasket::cena($rabatCene->osnovna_cena)}}</td>
                    <td>{{number_format($rabatCene->ukupan_rabat,2)}}%</td>
                    <td>{{B2bBasket::cena($rabatCene->cena_sa_rabatom)}}</td>
                    <td>{{number_format($rabatCene->porez,2)}}%</td>
                    <td>
                        <a class="cart-less" data-roba-id="{{ $row->roba_id }}"  href="#!"><</a>
                        <input class="cart-amount" id="quantity-{{ $row->roba_id }}" data-roba-id="{{ $row->roba_id }}" data-old-kol="{{ (int)$row->kolicina }}" id="{{ $row->web_b2b_korpa_stavka_id }}" data-max-kol="{{ B2bArticle::quantityB2b($row->roba_id) }}" type="text" value="{{(int)$row->kolicina}}">
                        <a class="cart-more" data-roba-id="{{ $row->roba_id }}" data-max-kol="{{ B2bArticle::quantityB2b($row->roba_id) }}" href="#!">></a> 
                    </td>
                    <td> {{B2bBasket::cena($totalItem)}}</td>
                    <td class="cart-remove">
                        <a href="javascript:deleteCartItem({{ $row->web_b2b_korpa_stavka_id }})" title="Ukloni artikal" class="close">x</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <button class="cart-remove-all">Isprazni korpu</button>
        </div>
    </div>

    <div class="row">
        <div class="cart-summary col-md-4 col-sm-6 col-xs-12 pull-right">
            <?php
            $cartTotal = B2bBasket::total();
            ?>
            <div class="clearfix">Osnovica: <span class="pull-right"> {{ B2bBasket::cena($cartTotal->cena_sa_rabatom) }} </span></div> 
            <div class="clearfix">PDV: <span class="pull-right"> {{ B2bBasket::cena($cartTotal->porez_cena) }} </span></div> 
            <div class="clearfix">Ukupno: <span class="pull-right"> {{ B2bBasket::cena($cartTotal->ukupna_cena) }} </span></div> 
        </div>
    </div>
<!-- PAYMENT METHOD -->
    <form class="payment-method" action="{{ route('b2b.check_out') }}" method="POST">
        <div class="row"> 
            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                <label for="nacin_isporuke">Način isporuke:</label>
                <select id="nacin_isporuke" name="nacin_isporuke" class="form-control">
                    {{B2bBasket::nacin_isporuke()}}
                </select>
            </div>
            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                <label for="nacin_placanja">Način plaćanja:</label>
                <select id="nacin_placanja" name="nacin_placanja" class="form-control">
                {{B2bBasket::nacin_placanja()}}
                </select>
            </div>
           <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label for="napomena">Napomena:</label>
                <textarea class="form-control" name="napomena" id="napomena"></textarea>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 text-right"> 
                <button class="submit-order-button">Prosledi porudžbinu</button>
            </div>
        </div>
    </form>
    @else
    <p class="empty-page">Trenutno nemate artikle u korpi.</p>
    @endif
</div>
 
@endsection
@section('footer')
 
<!-- <script src="{{ B2bOptions::base_url()}}js/jquery-1.11.2.min.js" type="text/javascript" ></script>
<script src="{{ B2bOptions::base_url()}}js/b2b/b2b_main.js" type="text/javascript" ></script>
<script src="{{ B2bOptions::base_url()}}js/jquery.fancybox.pack.js" type="text/javascript" ></script> -->
<script>
$(document).ready(function(){
    $('.cart-less').click(function(){
        var roba_id = $(this).data('roba-id');
        var quantity = $('#quantity-'+roba_id);
        if(quantity.val()==1){
            $('.info-popup').fadeIn().delay(1200).fadeOut();
            $('.info-popup .popup-inner').html("<p class='p-info'>Minimalna količina je 1 kom.</p>");
            quantity.val(quantity.data('old-kol'));
        }
        else {
            $.ajax({
                type: "POST",
                url:'{{route('b2b.cart_add')}}',
                cache: false,
                data:{roba_id:roba_id, status:3, quantity:1},
                success:function(res){
                location.reload();
                }
            });
        }
    });

    $('.cart-more').click(function(){
        var roba_id = $(this).data('roba-id');
        var max = $(this).data('max-kol');
        var quantity = $('#quantity-'+roba_id);
        if(quantity.val()==max){
            $('.info-popup').fadeIn().delay(1200).fadeOut();
            $('.info-popup .popup-inner').html("<p class='p-info'>Maksimalna količina je "+max+" kom.</p>");
            quantity.val(quantity.data('old-kol'));
        }
        else {
            $.ajax({
                type: "POST",
                url:'{{route('b2b.cart_add')}}',
                cache: false,
                data:{roba_id:roba_id, status:2, quantity:1},
                success:function(res){
                location.reload();
                }
            });
        }
    });

    $('.cart-amount').change(function(){
        var roba_id = $(this).data('roba-id');
        var old_kol = $(this).data('old-kol');
        var max = $(this).data('max-kol');
        var quantity = $(this).val();
        if(quantity < 1){
            $('.info-popup').fadeIn().delay(1200).fadeOut();
            $('.info-popup .popup-inner').html("<p class='p-info'>Minimalna količina je 1 kom.</p>");
            $(this).val(old_kol);
        }
        else if(quantity > max){
            $('.info-popup').fadeIn().delay(1200).fadeOut();
            $('.info-popup .popup-inner').html("<p class='p-info'>Maksimalna količina je "+max+" kom.</p>");
            $(this).val(old_kol);
        }
        else {
            $.ajax({
                type: "POST",
                url:'{{route('b2b.cart_add')}}',
                cache: false,
                data:{roba_id:roba_id, status:1, quantity:quantity},
                success:function(res){
                location.reload();
                }
            });
        }
    });

    $('.cart-remove-all').click(function() {
        $.ajax({
            type: "POST",
            url:'{{route('b2b.cart_delete_all')}}',
            cache: false,
            success:function(){
                location.reload();
            }
        });
    });
});

function deleteCartItem(cart_id){
    $.ajax({
        type: "POST",
        url:'{{route('b2b.cart_delete')}}',
        cache: false,
        data:{cart_id:cart_id},
        success:function(res){
            location.reload();
        }
    });
}
</script>
@endsection