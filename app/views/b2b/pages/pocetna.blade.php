@extends('b2b.templates.main')
 
@section('content')
  <div id="main-slider" class="bw">
        <?php foreach(DB::table('baneri_b2b')->where('tip_prikaza',2)->where('aktivan',1)->orderBy('redni_broj','asc')->get() as $row){ ?>
        @if((B2bOptions::trajanje_banera($row->baneri_id)) == 1 && (B2bOptions::future_banners($row->baneri_id)) == 1)
        <div>
            <a href="<?php echo $row->link; ?>"><img class="img-responsive" alt="{{$row->naziv}}" src="{{B2bOptions::base_url()}}<?php echo $row->img; ?>" /></a>
        </div>
        @endif
        <?php } ?>
    </div>
    <div class="banners-right row bw">
        <?php foreach(DB::table('baneri_b2b')->where('tip_prikaza', 1)->where('aktivan',1)->orderBy('redni_broj','asc')->get() as $row){ ?>
            @if((B2bOptions::trajanje_banera($row->baneri_id)) == 1 && (B2bOptions::future_banners($row->baneri_id)) == 1)
            <div class="col-md-4 col-sm-4 col-xs-4 no-padding">
                <a href="<?php echo $row->link; ?>">
                    <img width='290' height='160' class="img-responsive" src="{{B2bOptions::base_url()}}<?php echo $row->img; ?>" alt="{{$row->naziv}}" />
                </a>
            </div>
            @endif
        <?php } ?>
    </div>  
 <div class="main-content">
    <div class="row"> 
        <h2><span class="heading-background">Na Akciji</span></h2>
        <div class="sale-products product-slider">
            @foreach(B2bCommon::akcija() as $row)
            <div class="product col-md-3 col-sm-4 col-xs-12 no-padding">
                <div class="product-content">
                    <a href="{{B2bOptions::base_url()}}b2b/artikal/{{B2bUrl::url_convert(B2bArticle::seo_title($row->roba_id))}}" class="product-image-wrapper">
                        <img class="product-image img-responsive" src="{{B2bOptions::base_url()}}{{ B2bArticle::web_slika($row->roba_id) }}" alt="{{ B2bArticle::seo_title($row->roba_id) }}" />
                    </a>
             
                    <div class="product-info sm-text-center">
                        <div class="product-price"><span>{{ B2bBasket::cena(B2bArticle::b2bRabatCene($row->roba_id)->ukupna_cena) }}</span></div>
                        <a class="product-title" href="{{B2bOptions::base_url()}}b2b/artikal/{{B2bUrl::url_convert(B2bArticle::seo_title($row->roba_id))}}">
                            {{ B2bArticle::short_title($row->roba_id) }}
                        </a>
                        @if(B2bArticle::getStatusArticle($row->roba_id) == 1)
                            <?php
                            $cartAvailable = B2bArticle::quantityB2b($row->roba_id) - B2bBasket::getB2bQuantityItem($row->roba_id);
                            ?>
                            @if($cartAvailable>0 )
                                <div class="add-to-cart-container"> 
                                    <button data-product-id="{{$row->roba_id}}"  data-max-quantity="{{$cartAvailable}}" class="add-to-cart addCart">
                                        <i class="fa fa-shopping-cart"></i> U korpu
                                    </button>
                                </div>
                            @else
                                <div class="add-to-cart-container"> 
                                    <button class="dodavnje not-available">Nije dostupno</button>
                                </div>
                            @endif
                        @else
                            <div class="add-to-cart-container"> 
                                <button class="dodavanje not-available">
                                    {{ B2bArticle::find_flag_cene(B2bArticle::getStatusArticle($row->roba_id),'naziv') }}
                                </button>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    @if(B2bOptions::web_options(151)==1) 
        <h2><span class="heading-background">Najpopularniji proizvodi</span></h2>
        <div class="b2bMostPopular">
            @foreach(B2bCommon::mostPopularArticlesB2B() as $row)
            <div class="product col-md-3 col-sm-4 col-xs-12 no-padding">
                <div class="product-content">
                    <a href="{{B2bOptions::base_url()}}b2b/artikal/{{B2bUrl::url_convert(B2bArticle::seo_title($row->roba_id))}}" class="product-image-wrapper">
                        <img class="product-image img-responsive" src="{{B2bOptions::base_url()}}{{ B2bArticle::web_slika($row->roba_id) }}" alt="{{ B2bArticle::seo_title($row->roba_id) }}" />
                    </a>
             
                    <div class="product-info sm-text-center">
                        <div class="product-price"><span>{{ B2bBasket::cena(B2bArticle::b2bRabatCene($row->roba_id)->ukupna_cena) }}</span></div>
                        <a class="product-title" href="{{B2bOptions::base_url()}}b2b/artikal/{{B2bUrl::url_convert(B2bArticle::seo_title($row->roba_id))}}">
                            {{ B2bArticle::short_title($row->roba_id) }}
                        </a>
                        @if(B2bArticle::getStatusArticle($row->roba_id) == 1)
                            <?php
                            $cartAvailable = B2bArticle::quantityB2b($row->roba_id) - B2bBasket::getB2bQuantityItem($row->roba_id);
                            ?>
                            @if($cartAvailable>0 )
                                <div class="add-to-cart-container"> 
                                    <button data-product-id="{{$row->roba_id}}"  data-max-quantity="{{$cartAvailable}}" class="add-to-cart addCart">
                                        <i class="fa fa-shopping-cart"></i> U korpu
                                    </button>
                                </div>
                            @else
                                <div class="add-to-cart-container"> 
                                    <button class="dodavnje not-available">Nije dostupno</button>
                                </div>
                            @endif
                        @else
                            <div class="add-to-cart-container"> 
                                <button class="dodavanje not-available">
                                    {{ B2bArticle::find_flag_cene(B2bArticle::getStatusArticle($row->roba_id),'naziv') }}
                                </button>
                            </div>
                        @endif
                    </div>
                    <a class="article-edit-btn" target="_blank" href="{{ Options::base_url() }}admin/product/{{ $row->roba_id }}">IZMENI ARTIKAL</a>
                </div> 
            </div>            
            @endforeach            
        </div> 

        <h2><span class="heading-background">Najprodavanjiji proizvodi</span></h2>
        <div class="b2bMostPopular">
            @foreach(B2bCommon::bestSellerB2B() as $row)
            <div class="product col-md-3 col-sm-4 col-xs-12 no-padding">
                <div class="product-content">
                    <a href="{{B2bOptions::base_url()}}b2b/artikal/{{B2bUrl::url_convert(B2bArticle::seo_title($row->roba_id))}}" class="product-image-wrapper">
                        <img class="product-image img-responsive" src="{{B2bOptions::base_url()}}{{ B2bArticle::web_slika($row->roba_id) }}" alt="{{ B2bArticle::seo_title($row->roba_id) }}" />
                    </a>
             
                    <div class="product-info sm-text-center">
                        <div class="product-price"><span>{{ B2bBasket::cena(B2bArticle::b2bRabatCene($row->roba_id)->ukupna_cena) }}</span></div>
                        <a class="product-title" href="{{B2bOptions::base_url()}}b2b/artikal/{{B2bUrl::url_convert(B2bArticle::seo_title($row->roba_id))}}">
                            {{ B2bArticle::short_title($row->roba_id) }}
                        </a>
                        @if(B2bArticle::getStatusArticle($row->roba_id) == 1)
                            <?php
                            $cartAvailable = B2bArticle::quantityB2b($row->roba_id) - B2bBasket::getB2bQuantityItem($row->roba_id);
                            ?>
                            @if($cartAvailable>0 )
                                <div class="add-to-cart-container"> 
                                    <button data-product-id="{{$row->roba_id}}"  data-max-quantity="{{$cartAvailable}}" class="add-to-cart addCart">
                                        <i class="fa fa-shopping-cart"></i> U korpu
                                    </button>
                                </div>
                            @else
                                <div class="add-to-cart-container"> 
                                    <button class="dodavnje not-available">Nije dostupno</button>
                                </div>
                            @endif
                        @else
                            <div class="add-to-cart-container"> 
                                <button class="dodavanje not-available">
                                    {{ B2bArticle::find_flag_cene(B2bArticle::getStatusArticle($row->roba_id),'naziv') }}
                                </button>
                            </div>
                        @endif
                    </div>
                    <a class="article-edit-btn" target="_blank" href="{{ Options::base_url() }}admin/product/{{ $row->roba_id }}">IZMENI ARTIKAL</a>
                </div> 
            </div>            
            @endforeach            
        </div> 

        <h2><span class="heading-background">Najnoviji proizvodi</span></h2>
        <div class="b2bMostPopular">
            @foreach(B2bCommon::latestAdded() as $row)
            <div class="product col-md-3 col-sm-4 col-xs-12 no-padding">
                <div class="product-content">
                    <a href="{{B2bOptions::base_url()}}b2b/artikal/{{B2bUrl::url_convert(B2bArticle::seo_title($row->roba_id))}}" class="product-image-wrapper">
                        <img class="product-image img-responsive" src="{{B2bOptions::base_url()}}{{ B2bArticle::web_slika($row->roba_id) }}" alt="{{ B2bArticle::seo_title($row->roba_id) }}" />
                    </a>
             
                    <div class="product-info sm-text-center">
                        <div class="product-price"><span>{{ B2bBasket::cena(B2bArticle::b2bRabatCene($row->roba_id)->ukupna_cena) }}</span></div>
                        <a class="product-title" href="{{B2bOptions::base_url()}}b2b/artikal/{{B2bUrl::url_convert(B2bArticle::seo_title($row->roba_id))}}">
                            {{ B2bArticle::short_title($row->roba_id) }}
                        </a>
                        @if(B2bArticle::getStatusArticle($row->roba_id) == 1)
                            <?php
                            $cartAvailable = B2bArticle::quantityB2b($row->roba_id) - B2bBasket::getB2bQuantityItem($row->roba_id);
                            ?>
                            @if($cartAvailable>0 )
                                <div class="add-to-cart-container"> 
                                    <button data-product-id="{{$row->roba_id}}"  data-max-quantity="{{$cartAvailable}}" class="add-to-cart addCart">
                                        <i class="fa fa-shopping-cart"></i> U korpu
                                    </button>
                                </div>
                            @else
                                <div class="add-to-cart-container"> 
                                    <button class="dodavnje not-available">Nije dostupno</button>
                                </div>
                            @endif
                        @else
                            <div class="add-to-cart-container"> 
                                <button class="dodavanje not-available">
                                    {{ B2bArticle::find_flag_cene(B2bArticle::getStatusArticle($row->roba_id),'naziv') }}
                                </button>
                            </div>
                        @endif
                    </div>
                    <a class="article-edit-btn" target="_blank" href="{{ Options::base_url() }}admin/product/{{ $row->roba_id }}">IZMENI ARTIKAL</a>
                </div> 
            </div>            
            @endforeach            
        </div> 
</div>
    @endif 
<?php $strana=B2bCommon::get_page_start();?>

<!-- IZDVAJAMO -->
<script type="text/javascript">
    @if(Session::has('login_message'))
    bootbox.alert({
        message: "Uspešno ste se prijavili.",
    });
    setTimeout(function(){ bootbox.hideAll(); },2500);
    @endif
</script>
@endsection
@section('footer')
 
    <!-- <script src="{{ B2bOptions::base_url()}}js/jquery-1.11.2.min.js" type="text/javascript"></script> -->
    <script src="{{ B2bOptions::base_url()}}js/slick.min.js" type="text/javascript"></script>
    <!-- <script src="{{ B2bOptions::base_url()}}js/b2b/b2b_main.js" type="text/javascript"></script>
    <script src="{{ B2bOptions::base_url()}}js/jquery.fancybox.pack.js" type="text/javascript"></script> -->

@endsection