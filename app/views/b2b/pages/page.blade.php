@extends('b2b.templates.main')
@section('content') 
<div class="main-content">
    <div class="row"> 
        <div class="col-md-12 col-sm-12 col-xs-12 custom-pages"> 
            {{ $web_content}}
        </div>
    </div> 
</div>
<input type="hidden" id="base_url" value="{{B2bOptions::base_url()}}" />
@endsection
@section('footer')
@endsection