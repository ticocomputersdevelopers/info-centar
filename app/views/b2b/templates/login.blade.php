<!DOCTYPE html>

<html lang="sr">

<head>

    <title>{{ $seo['title'] }} | {{B2bOptions::company_name() }}</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="{{ $seo['description'] }}" />
    <meta name="author" content="{{$seo['keywords']}}" />
    <meta name="msapplication-tap-highlight" content="no"/>

    <link rel="icon" type="image/png" href="{{ B2bOptions::base_url()}}favicon.ico">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <!-- <link href="{{ B2bOptions::base_url()}}css/foundation.min.css" rel="stylesheet" type="text/css" /> -->
   <!-- BOOTSTRAP -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="{{Options::domain()}}js/bootbox.min.js"></script>
    
    <link href="{{ B2bOptions::base_url()}}css/admin.css" rel="stylesheet" type="text/css" />

</head>

<body class="body-login">
  <!--   <div class="color-overlay-image"></div>
    <div class="color-overlay"></div> -->

    @yield('content')

</body>

</html>