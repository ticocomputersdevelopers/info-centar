<!DOCTYPE html>
<html lang="sr">
<head>
    <title>{{ $seo['title'] }} | {{B2bOptions::company_name() }}</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="{{ $seo['description'] }}" />
    <meta name="author" content="{{$seo['keywords']}}" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

     <!-- BOOTSTRAP -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="{{Options::domain()}}js/bootbox.min.js"></script>

    <link href="{{ B2bOptions::base_url()}}css/slick.css" rel="stylesheet" type="text/css" />
    <link href="{{ B2bOptions::base_url()}}css/b2b/style.css" rel="stylesheet" type="text/css" />
    <link rel="icon" type="image/png" href="{{ B2bOptions::base_url()}}favicon.ico">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
     
   <!--  <link href="{{ B2bOptions::base_url()}}css/fancybox.css" rel="stylesheet" type="text/css" /> -->
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
    @yield('head')
</head>

<body id="start-page"  
    @if(B2bOptions::getBGimg() != null) 
        style="background-image: url({{B2bOptions::base_url()}}{{B2bOptions::getBGimg()}}); background-size: cover; background-repeat: no-repeat; background-attachment: fixed; background-position: center;" 
    @endif
    >

<header class=""> 
  <!-- PREHEADER -->
    @include('b2b.partials.menu_top')
<div class="JSfixed-hdr">
    <div id="JSfixed_header"> 
        <div class="container"> 
     <!-- LOGO AREA -->
            <div class="row div-in-header"> 
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <a class="logo" href="{{ B2bOptions::base_url()}}b2b" title="{{B2bOptions::company_name()}}">
                        <img class="img-responsive" src="{{ B2bOptions::base_url()}}{{B2bOptions::company_logo()}}" alt="{{B2bOptions::company_name()}}" />
                    </a>
                </div>
      <!-- SEARCH AREA -->
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <form action="{{route('b2b.search')}}" method="GET" id="JSlive-search" class="search-form">
                        <input class="search-field" type="text" id="search" autocomplete="off" name="q" value="{{Input::get('q')?Input::get('q'):''}}" placeholder="Pretraga" />
                        <button class="JSsearch-button"> <i class="fa fa-search"></i> </button>
                    </form>
                </div>
      <!-- CURENCY -->
                <div class="col-md-3 col-sm-2 col-xs-6 text-center currency-change"> 
                    @if(Session::get('b2b_valuta') == 1) 
                        <a href="!#" class=""       id="valuta_eur">EUR</a> | <a id="valuta_din" class="active" href="#">RSD</a>
                    @elseif(Session::get('b2b_valuta') == 2) 
                        <a href="!#" class="active" id="valuta_eur">EUR</a> | <a id="valuta_din" class=""       href="#">RSD</a>
                    @else
                        <a href="!#" class=""       id="valuta_eur">EUR</a> | <a id="valuta_din" class="active" href="#">RSD</a>
                    @endif

                    @if(Session::has('b2b_valuta') and Session::get('b2b_valuta') != 1)
                        <span> Kurs {{ B2b::kurs() }}</span>
                    @endif
                </div>
       <!-- CART AREA -->
                <div class="col-md-2 col-sm-3 col-xs-6 text-center mini-cart-container">
                    <a class="header-cart" href="{{ B2bOptions::base_url() }}b2b/{{ B2bCommon::get_korpa() }}">
                        <i class="fa fa-shopping-cart"></i>
                        <i class="badge cart-amount-number">{{ B2bBasket::b2bCountItems() }}</i> 
                        <span id="header_total_amount_show" class="cart-amount-price">
                            {{ B2bBasket::cena(B2bBasket::total()->ukupna_cena) }}
                        </span>   
                    </a>
                   @include('b2b.partials.cart_top')
                </div>
            </div>
        </div>
    </div>
</div>
 <!-- CATEGORIES -->
    <div class="categories-menu clearfix">
        <div class="container">
            <div class="row" id="responsive-nav"> 
                <div class="col-md-3 col-sm-3 col-xs-12 no-padding"> 
                    @include('b2b.partials.categories')
                </div>
            </div>
        </div>
        <div class="resp-nav-btn pull-right"><span class="glyphicon glyphicon-menu-hamburger"></span></div>
    </div>
</header>

<div class="container"> 
     
        @yield('content')
    
</div>

  @include('b2b.partials.popups')

 <!-- FOOTER -->
  @include('b2b.partials.footer')
 
<!-- LOGIN POPUP -->
  @yield('footer')
<input type="hidden" id="base_url" value="{{ AdminB2BOptions::base_url() }}">
<script src="{{ B2bOptions::base_url()}}js/b2b/b2b_main.js" type="text/javascript"></script>
<script src="{{ B2bOptions::base_url()}}js/b2b/main_function.js" type="text/javascript"></script>
@if(Options::header_type()==1)
    <script src="{{ B2bOptions::base_url()}}js/b2b/fixed_header.js"></script>
@endif
<script>
    function deleteItemCart(cart_id){
        $.ajax({
            type: "POST",
            url:'{{route('b2b.cart_delete')}}',
            cache: false,
            data:{cart_id:cart_id},
            success:function(res){
                location.reload();
            }
        });
    }
</script>
</body>
</html>