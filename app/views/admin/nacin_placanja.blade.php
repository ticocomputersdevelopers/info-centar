<section class="osobine-page" id="main-content">

	@include('admin/partials/tabs')

	@if(Session::has('message'))
		<script>
			alertify.success('{{ Session::get('message') }}');
		</script>
	@endif
	
	<div class="row">
		<section class="small-12 medium-12 large-3 columns">
			<div class="flat-box">	
				<h3 class="title-med">Način plaćanja <i class="fa fa-credit-card"></i></h3>
				<div class="row"> 
					<div class="columns medium-12 after-select-margin"> 
						<select name="" id="JSnacinPlacanja">
							<option data-id="0">Dodaj novi</option>
							@foreach($nacini_placanja as $row)
								<option data-id="{{ $row->web_nacin_placanja_id }}" @if($row->web_nacin_placanja_id == $web_nacin_placanja_id) selected @endif>{{ $row->naziv }}</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
		</section>
		<section class="small-12 medium-12 large-5 columns">
			<div class="flat-box">
			@if($web_nacin_placanja_id > 0)		
				@foreach($nacin_placanja as $row)
					<form action="{{ AdminOptions::base_url() }}admin/nacin_placanja/{{$web_nacin_placanja_id}}" method="POST">
						<h3 class="title-med">Izmeni</h3>
						<div class="row"> 
							<div class="columns medium-12 field-group {{ $errors->first('naziv') ? ' error' : '' }}">
								<input type="text" name="naziv" value="{{ $row->naziv }}">
							</div>
							<div class="columns medium-12"> 
								<input type="checkbox" name="aktivno" @if($row->selected == 1) : checked @endif> Aktivno
							</div>
						 </div>
						<div class="btn-container text-center"> 
							<button type="submit" class="btn btn-primary save-it-btn" class="btn btn-primary">Sačuvaj</button>
						</div>
					</form>
					@if($web_nacin_placanja_id != 3)
					<form action="{{ AdminOptions::base_url() }}admin/nacin_placanja/{{$web_nacin_placanja_id}}/delete" method="POST">
						<div class="row"> 
							<div class="columns medium-12 text-center">
								<input type="submit" value="Obriši" class="btn btn-danger">
							</div>
					   </div>
					</form>
					@endif
				@endforeach
			@elseif($web_nacin_placanja_id == 0)
				<form action="{{ AdminOptions::base_url() }}admin/nacin_placanja/{{$web_nacin_placanja_id}}" method="POST">
					<h3 class="title-med">Novo</h3>
				    <div class="row"> 
						<div class="columns medium-12 field-group {{ $errors->first('naziv') ? ' error' : '' }}">
							<input type="text" name="naziv" value="" autofocus="autofocus">
						</div>
						<div class="columns medium-12">
							<input type="checkbox" name="aktivno"> Aktivno
						</div>
						 
					</div>
					<div class="row"> 
						<div class="btn-container text-center"> 
							<button type="submit" class="btn btn-primary save-it-btn">Sačuvaj</button>
						</div>
				    </div>
				</form>
			@endif
			</div>
		</section>
	</div>

</section>