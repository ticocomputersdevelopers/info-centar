<div id="main-content" class="kupci-page">
	@if(Session::has('message'))
		<script>
			alertify.success('{{ Session::get('message') }}');
		</script>
	@endif
	@if(Session::has('alert'))
		<script>
			alertify.error('{{ Session::get('alert') }}');
		</script>
	@endif
	<div class="row m-subnav">
		<div class="large-2 medium-3 small-12 columns ">
			 <a class="m-subnav__link" href="{{AdminOptions::base_url()}}admin/web_import/0/0/0/nn-nn-nn-nn-nn-nn-nn-nn-nn-nn-nn-nn/0/nn-nn" >
				<div class="m-subnav__link__icon">
					<i class="fa fa-arrow-left" aria-hidden="true"></i>
				</div>
				<div class="m-subnav__link__text">
					Nazad
				</div>
			</a>
		</div>
	</div><br>

	<div class="row">
		<div class="medium-7 columns">
			<div class=""> 
				<table>
					<thead>
						<tr>
							<th>Dobavljač</th>
							<th>Proizvođač dobavljača</th>
							<th>Naš proizvođač</th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="text-right" colspan='100%;'>
								<a class="btn btn-small btn-primary" href="{{AdminOptions::base_url()}}admin/import-podaci-proizvodjac/{{ $partner_id }}/0">Dodaj novi</a>
							</td>
						</tr>
						@foreach($parner_proizvodjaci as $parner_proizvodjac)
						<tr>
							<td>{{ AdminSupport::dobavljac($parner_proizvodjac->partner_id) }}</td>
							<td>{{ $parner_proizvodjac->proizvodjac }}</td>
							<td>{{ AdminSupport::find_proizvodjac($parner_proizvodjac->proizvodjac_id,'naziv') }}</td>
							<td>
								<a class="btn btn-small btn-danger" href="{{AdminOptions::base_url()}}admin/import-podaci-proizvodjac-delete/{{ $parner_proizvodjac->partner_id }}/{{ $parner_proizvodjac->partner_proizvodjac_id }}">Obriši</a>
							</td>
							<td>
								<a class="btn btn-small btn-primary" href="{{AdminOptions::base_url()}}admin/import-podaci-proizvodjac/{{ $parner_proizvodjac->partner_id }}/{{ $parner_proizvodjac->partner_proizvodjac_id }}">Izmeni</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>

		<div class="medium-5 columns">
			<div class="flat-box">
			<h1 class="title-med">Povezivanje proizvođača</h1>
				<form action="{{AdminOptions::base_url()}}admin/import-podaci-proizvodjac-save" method="POST">
					<input type="hidden" name="partner_id" value="{{ $parner_proizvodjac_item->partner_id }}">
					<input type="hidden" name="partner_proizvodjac_id" value="{{ $parner_proizvodjac_item->partner_proizvodjac_id }}">
					<div class="row">
						<div class="field-group column medium-6">
							<label for="">Dobavljač</label>
							<select name="partner_id" class="m-input-and-button__input import-select" id="JSDobavljacSelect">
								<option value="0">Svi dobavljači</option>
								@foreach(AdminImport::dobavljaci_web_import_tabela() as $dobavljac)
									@if($dobavljac->partner_id == $partner_id)
										<option value="{{ $dobavljac->partner_id }}" selected>{{ $dobavljac->naziv }}</option>
									@else
										<option value="{{ $dobavljac->partner_id }}">{{ $dobavljac->naziv }}</option>
									@endif
								@endforeach
							</select>
							<div class="error">{{ $errors->first('partner_id') }}</div>
						</div>
					</div>

					<div class="row">
						<div class="field-group column medium-6">
							<label for="">Proizvođač dobavljača</label>
							<div class="short-group-container">
								<select name="proizvodjac">
									@foreach($proizvodjaci_dobavljaca as $row)
									<option value="{{ $row->proizvodjac }}" {{ (Input::old('proizvodjac') ? Input::old('proizvodjac') : $parner_proizvodjac_item->proizvodjac) == $row->proizvodjac ? 'selected' : '' }}>{{ $row->proizvodjac }}</option>
									@endforeach
								</select>
							</div>
							<div class="error">{{ $errors->first('proizvodjac') }}</div>
						</div>

						<div class="field-group column medium-6">
							<label for="">Naš proizvođač</label>
							<select name="proizvodjac_id" class="m-input-and-button__input import-select">
								@foreach(AdminSupport::getProizvodjaci() as $proizvodjac)
									@if($proizvodjac->proizvodjac_id == (Input::old('proizvodjac_id') ? Input::old('proizvodjac_id') : $parner_proizvodjac_item->proizvodjac_id))
										<option value="{{ $proizvodjac->proizvodjac_id }}" selected>{{ $proizvodjac->naziv }}</option>
									@else
										<option value="{{ $proizvodjac->proizvodjac_id }}">{{ $proizvodjac->naziv }}</option>
									@endif
								@endforeach
							</select>
							<div class="error">{{ $errors->first('proizvodjac_id') }}</div>
						</div>
                        <div class="columns medium-6 field-group">
                            <label>Mapiraj sve artikle</label>
                            <input type="checkbox" name="mapped_all" {{ !is_null(Input::old('mapped_all')) ? "checked" : "" }}>
                        </div>
                        <div class="columns medium-6 field-group">
                            <label>Mapiraj artikle bez našeg proizvođača</label>
                            <input type="checkbox" name="mapped_news" {{ !is_null(Input::old('mapped_news')) ? "checked" : "" }}>
                        </div>
					</div>

					<div class="row"> 
						<div class="btn-container center">
							<button type="submit" id="submit" class="btn btn-primary save-it-btn">Sačuvaj</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>