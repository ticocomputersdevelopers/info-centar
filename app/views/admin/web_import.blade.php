<section class="collapse import-content" id="main-content">
<!--=========== LOADER =============== -->
	<div class="JSads-loader"> 
		<div class="ads-loader-inner"> 
			<div>  
				<span class="loadSpiner"></span>
				<span>Sadržaj se učitava. Molimo Vas sačekajte.</span>
			</div>
		</div>
	</div>   
	<div class="row small-gutter">
		
		<div class="columns medium-2">
			<div class="flat-box">  
				<h3 class="text-center">Import cenovnika</h3>
		 
					<select class="m-input-and-button__input import-select" id="dobavljac_select">
						<option value="0">Svi dobavljači</option>
						@foreach(AdminImport::dobavljaci_web_import_tabela() as $dobavljac)
							@if( in_array($dobavljac->partner_id ,explode("-",$criteriaImport['dobavljac'])) )
								<option value="{{ $dobavljac->partner_id }}" selected>{{ $dobavljac->naziv }}</option>
							@else
								<option value="{{ $dobavljac->partner_id }}">{{ $dobavljac->naziv }}</option>
							@endif
						@endforeach
					</select>

					<form method="POST" action="{{AdminOptions::base_url()}}admin/upload_web_import" enctype="multipart/form-data">
						 <input type="hidden" name="partner_id" value="{{ Session::has('upload_file_js') ? Session::get('upload_file_js')[3] : $partner_id }}"> 
					  	<div class="">
							<label for="">Kurs evra: </label>
							<div class="m-input-and-button">
								<input class="JSkursVal wiKurs" type="text" name="kurs"  value="{{ Input::old('kurs') ? Input::old('kurs') : $kurs }}">
						  		@if(AdminOptions::gnrl_options(3030)==1)
						  		<input type="checkbox" name="roba_insert" {{ !is_null(Input::old('roba_insert')) ? 'checked' : '' }} > <label> Umatiči nove</label>
						  		@endif
								<!-- <a href="#" id="kursButton" class="m-input-and-button__btn btn btn-icon btn-secondary"><i class="fa fa-check" aria-hidden="true"></i></a> 
								-->
								<!-- <a href="#" id="JSaddKurs" class="m-input-and-button__btn btn btn-icon btn-secondary">Preračunaj</a> -->
							</div>
						</div> 
						<div class="text-center">
							<div class="bg-image-file">
							   <!-- <label>Izaberite fajl:</label> -->
								<input type="file" name="upload_file" value="Izaberi fajl" placeholder="Izaberi fajl">
								<input type="hidden" id="has_import" value="{{ Session::get('upload_file_js')[0] }}">
								<input type="hidden" id="upload_file" value="{{ Session::get('upload_file_js')[1] }}">
								<input type="hidden" id="upload_file_extension" value="{{ Session::get('upload_file_js')[2] }}">
								<input type="hidden" id="roba_insert" value="{{ Session::get('upload_file_js')[4] }}">
							</div>
							<input class="btn-admin btn btn-primary btn-import btn-small center" type="submit" value="Importuj" name="submit">
							@if(AdminOptions::gnrl_options(3002)==1)
					  			<a class="btn btn-primary btn-import add-new-btn tooltipz" aria-label="Upload fajla" href="{{ AdminOptions::base_url()}}admin/file-upload">A</a>
					  		@endif
					  		@if(AdminOptions::gnrl_options(3030)==1)
					  		<a class="btn btn-primary btn-import add-new-btn tooltipz" aria-label="Import podaci grupa" href="{{ AdminOptions::base_url()}}admin/import_podaci_grupa/{{ explode('-',$criteriaImport['dobavljac'])[0] }}/0">G</a>
					  		<a class="btn btn-primary btn-import add-new-btn tooltipz" aria-label="Import podaci Proizvodjač" href="{{ AdminOptions::base_url()}}admin/import-podaci-proizvodjac/{{ isset($criteriaImport['dobavljac']) ? explode('-',$criteriaImport['dobavljac'])[0] : '0' }}/0">P</a>
					  		@endif
						</div>
						 
						<!-- <a href="{{AdminOptions::base_url()}}auto-import-3/{{AdminOptions::sifra()}}" class="btn-admin btn btn-primary btn-import add-new-btn center">Kompletno preračunavanje</a> -->
					</form>
 				
				<div id="message_import" class="center"></div>
			</div> <!-- end of .flat-box -->
		</div>		

		<div class="columns medium-4">
			<div class="flat-box">
				<h3 class="text-center">Rad sa artiklima</h3>
				<div class="row">
		  			<div class="columns medium-7">
						<div class="row">
							<div class="m-input-and-button">
								<select id="webImportGrupa" class="admin-select m-input-and-button__input JSexecuteInput">
									{{ AdminSupport::selectGroups(0) }}
								</select>
								<button id="grupaButton" class="btn btn-icon btn-secondary m-input-and-button__btn JSexecuteBtn btn-radius tooltipz" aria-label="Dodeli" data-execute='[{"table":"dobavljac_cenovnik", "column":"grupa_pr_id"}]'><i class="fa fa-check" aria-hidden="true"></i></button>

								<a class="btn btn-primary m-input-and-button__btn tooltipz" aria-label="Napravi novu grupu" href="{{AdminOptions::base_url()}}admin/grupe/0" target="_blank"><i class="fa fa-plus" aria-hidden="true"></i></a>
							 </div> <!-- end of .field-group no-label-margin__flex -->
						 </div>
				
						<div class="row">
							 <div class="m-input-and-button">
								<select id="webImportProizvodjac" class="m-input-and-button__input JSexecuteInput">
									<option value="-1">Izabrerite proizvođača</option>
									@foreach(AdminSupport::getProizvodjaci() as $proizvodjac)
									<option value="{{ $proizvodjac->proizvodjac_id }}">{{ $proizvodjac->naziv }}</option>
									@endforeach
								</select>

								<button id="proizvodjacButton" class="m-input-and-button__btn btn btn-icon btn-secondary JSexecuteBtn btn-radius tooltipz" aria-label="Dodeli" data-execute='[{"table":"dobavljac_cenovnik", "column":"proizvodjac_id"}]'><i class="fa fa-check" aria-hidden="true"></i></button>

								<a class="btn btn-primary m-input-and-button__btn tooltipz" aria-label="Napravi novog proizvođača" href="{{AdminOptions::base_url()}}admin/proizvodjac/0" target="_blank"><i class="fa fa-plus" aria-hidden="true"></i></a>
							</div> <!-- end of .m-input-and-button -->
						</div>

 						<div class="row">
					 		<div class="m-input-and-button">
								<select id="webImportTarifnagrupa" class="m-input-and-button__input JSexecuteInput">
									<option selected>Izaberite tarifnu grupu</option>
									@foreach (AdminSupport::getPoreskeGrupe() as $porez)
									@if($porez->default_tarifna_grupa == 1)
									<option selected value="{{ $porez->tarifna_grupa_id }}">{{ $porez->naziv }}</option>
									@else
									<option value="{{ $porez->tarifna_grupa_id }}">{{ $porez->naziv }}</option>
									@endif

									@endforeach
								</select>	
								<button id="tarifna_grupaButton" class="m-input-and-button__btn btn btn-secondary JSexecuteBtn btn-radius tooltipz" aria-label="Dodeli" data-execute='[{"table":"dobavljac_cenovnik", "column":"tarifna_grupa_id"}]'><i class="fa fa-check" aria-hidden="true"></i></button>				
							</div> <!-- end of .m-input-and-button -->
						</div>
					</div> <!-- end of .medium-6 .large-6 -->
					
					<div class="columns medium-offset-1 medium-4">

						<div class="field-group no-label-margin">
						<!-- 	<label for="">MP marža</label> -->
							<div class="m-input-and-button">
								<input class="JSexecuteInput web-marza-input" type="text" id="mp_marza" placeholder="MP marža"> 
								<button id="mp_marza_but" class="btn btn-icon btn-primary JSexecuteBtn btn-radius tooltipz" aria-label="Dodeli" data-execute='[{"table":"dobavljac_cenovnik", "column":"mp_marza"},{"table":"roba", "column":"mp_marza"}]'><i class="fa fa-check" aria-hidden="true"></i></button>
							</div>
						</div>

						<div class="field-group no-label-margin">
							<!-- <label for="">Web marža</label> -->
							<div class="m-input-and-button">
								<input class="JSexecuteInput web-marza-input" type="text" id="web_marza" placeholder="Web marža"> 
								<button id="web_marza_but" class="btn btn-icon btn-primary JSexecuteBtn btn-radius tooltipz" aria-label="Dodeli" data-execute='[{"table":"dobavljac_cenovnik", "column":"web_marza"},{"table":"roba", "column":"web_marza"}]'><i class="fa fa-check" aria-hidden="true"></i></button>
							</div>
						</div>
						<div class="field-group no-label-margin">
						<!-- 	<label for="">Količina</label> -->
							<div class="m-input-and-button">
								<input class="JSexecuteInput web-marza-input" type="text" id="kolicina_mass" placeholder="Količina"> 
								<button id="mp_marza_but" class="btn btn-icon btn-primary JSexecuteBtn btn-radius tooltipz" aria-label="Dodeli" data-execute='[{"table":"dobavljac_cenovnik", "column":"kolicina"}]'><i class="fa fa-check" aria-hidden="true"></i></button>
							</div>
						</div>
					 </div>  
				</div>

				<div class="row"> 
					 <div class="columns medium-12 large-12 text-center">
						<button class="JSunesi_nove btn-admin btn btn-primary btn-import add-new-btn center" id="unesi_nove" value="">Unesi nove</button>
					</div>
		  		</div> 
			</div>
		</div>
		
		<div class="columns medium-6">
			<div class="flat-box">
				<h3 class="text-center">Filtriranje</h3>
				<div class="row collapse filter-row">
				<div class="wrap">
					<table class="filter-table columns medium-4">
						<tr>
							<th>Da</th>
							<th>Ne</th>
						</tr>
						<tr>
							<td>
								<input class="filter-check" type="checkbox" data-check="13">
							</td>
							<td>
								<input class="filter-check" type="checkbox" data-check="14">
							</td>
							<td>Umatičeni</td>
						</tr>
						<tr>
							<td>
								<input class="filter-check" type="checkbox" data-check="9">
							</td>
							<td>
								<input class="filter-check" type="checkbox" data-check="10">
							</td>
							<td>Lager dobavljača</td>
						</tr>
						<tr>
							<td>
								<input class="filter-check" type="checkbox" data-check="19">
							</td>
							<td>
								<input class="filter-check" type="checkbox" data-check="20">
							</td>
							<td>Lager (naš)</td>
						</tr>
						<tr>
							<td>
								<input class="filter-check" type="checkbox" data-check="21">
							</td>
							<td>
								<input class="filter-check" type="checkbox" data-check="22">
							</td>
							<td>Najniža NC</td>
						</tr>
						<tr>
							<td>
								<input class="filter-check" type="checkbox" data-check="17">
							</td>
							<td>
								<input class="filter-check" type="checkbox" data-check="18">
							</td>
							<td>Nova NC</td>
						</tr>
					</table>
					<div class="columns medium-4">
					<table class="filter-table">
					<tr>
						<th>Da</th>
						<th>Ne</th>
					</tr>
					<tr>
						<td>
							<input class="filter-check" type="checkbox" data-check="15">
						</td>
						<td>
							<input class="filter-check" type="checkbox" data-check="16">
						</td>
						<td>Na webu</td>
					</tr>
					<tr>
						<td>
							<input class="filter-check" type="checkbox" data-check="11">
						</td>
						<td>
							<input class="filter-check" type="checkbox" data-check="12">
						</td>
						<td>Aktivni</td>
					</tr>
					<tr>
						<td>
							<input class="filter-check" type="checkbox" data-check="23">
						</td>
						<td>
							<input class="filter-check" type="checkbox" data-check="24">
						</td>
						<td>Povezani</td>
					</tr>
				</table>
				<div class="nc-options row">
					<div class="columns medium-8">
						<div class="columns medium-6">
							<label for="" class="tooltipz" aria-label="Nabavna cena">NC od</label>
							<input type="text" id="JSCenaOd" class="">
						</div>
						<div class="columns medium-6">
							<label for="" class="tooltipz" aria-label="Nabavna cena">NC do</label>
							<input type="text" id="JSCenaDo" class="">
						</div>
					</div>
					<div class="columns medium-4 large-4 btns-od-do">
						<a href="#" id="JSCenaSearch" class="m-input-and-button__btn btn btn-primary btn-xm btn-radius">
						<i class="fa fa-search" aria-hidden="true"></i>
						</a>
						<a href="#" class="m-input-and-button__btn btn btn-secondary tooltipz" aria-label="Poništi">
						<i class="fa fa-times" id="JSCenaClear" aria-hidden="true"></i>
						</a>
					</div>
				</div>
				</div>
				<table class="filter-table columns medium-4">
					<tr>
						<th>Da</th>
						<th>Ne</th>
					</tr>
					<tr>
						<td>
							<input class="filter-check" type="checkbox" data-check="1">
						</td>
						<td>
							<input class="filter-check" type="checkbox" data-check="2">
						</td>
						<td>Opis</td>
					</tr>
<!-- 					<tr>
						<td>
							<input class="filter-check" type="checkbox" data-check="3">
						</td>
						<td>
							<input class="filter-check" type="checkbox" data-check="4">
						</td>
						<td>Karak. (HTML)</td>
					</tr> -->
					<tr>
						<td>
							<input class="filter-check" type="checkbox" data-check="5">
						</td>
						<td>
							<input class="filter-check" type="checkbox" data-check="6">
						</td>
						<td>Karak. (dob.)</td>
					</tr>
					<tr>
						<td>
							<input class="filter-check" type="checkbox" data-check="7">
						</td>
						<td>
							<input class="filter-check" type="checkbox" data-check="8">
						</td>
						<td>Slike</td>
					</tr>
				</table>
 		 	</div>
			</div>
			</div> <!-- end of .flat-box -->
		</div>

	</div><!-- opcije end -->

	<section class="search-section">
		<div class="flat-box margin search-block">

			<div class="row">
			
				<div class="columns medium-4 no-padd">
					<section class="m-input-and-button">
						<input type="text" class="m-input-and-button__input search-field" id="search" autocomplete="off" placeholder="Pretraži...">
						<button type="submit" id="search-btn" value="Pronađi" class="m-input-and-button__btn btn btn-primary btn-radius tooltipz"><i class="fa fa-search" aria-hidden="true"></i></button> 

						<button type="submit" id="clear-btn" value="Poništi" class="m-input-and-button__btn btn btn-secondary tooltipz" aria-label="Poništi"><i class="fa fa-times" id="JSCenaClear" aria-hidden="true"></i></button> 
					</section>
				</div>
				
				<section class="columns medium-3 m-input-and-button select-box">
					<select  id="grupa_select" class="m-input-and-button">						
						{{ AdminSupport::selectGroups($criteriaImport['grupa_pr_id']) }}
					</select>
					<button class="btn btn-primary btn-block m-input-and-button__btn btn-icon JSGrProDob btn-radius tooltipz" aria-label="Dodeli"><i class="fa fa-check" aria-hidden="true"></i></button>
				</section>

				<section class="columns medium-3 select-box">
					<div class="m-input-and-button">
						<select class="admin-select js-example-basic-multiple m-input-and-button" id="proizvodjac_select" multiple="multiple">
							@foreach(AdminSupport::getProizvodjaci() as $proizvodjac)
								@if( in_array($proizvodjac->proizvodjac_id ,explode("+",$criteriaImport['proizvodjac'])) )
								<option value="{{ $proizvodjac->proizvodjac_id }}" selected>{{ $proizvodjac->naziv }}</option>
								@else
								<option value="{{ $proizvodjac->proizvodjac_id }}">{{ $proizvodjac->naziv }}</option>
								@endif
							@endforeach
						</select>
						<button class="btn btn-primary btn-block JSGrProDob m-input-and-button__btn btn-icon btn-radius tooltipz" aria-label="Dodeli"><i class="fa fa-check" aria-hidden="true"></i></button>
					</div>
				</section>
			</div>
		</div>
	</section>
	
	<ul class="custom-menu">
		<li>
			<button class="custom-menu-item" id="all" value="">Izaberi sve (F3)</button>
		</li>
		<li>
			<button class="JSunesi_nove custom-menu-item" id="">Unesi nove</button>
		</li>
		<li>
			@if(AdminOptions::gnrl_options(3006)==1)
			<button class="custom-menu-item" id="JSCeneRunModal" value="">Prihvati cene i nazive</button>
			@else
			<button class="custom-menu-item JSPrihvatiCeneNaziv" data-naziv="0" value="">Prihvati cene</button>
			@endif	
		</li>
		<li>
			@if(AdminOptions::gnrl_options(3006)==1)
			<button class="custom-menu-item" id="JSLagerRunModal" value="">Prihvati lager</button>
			@else
			<button class="custom-menu-item JSPrihvatiLager" value="">Prihvati lager</button>
			@endif
		</li>
		<li>
			<button class="custom-menu-item" id="preracunaj_cene" value="">Preračunaj cene</button>
		</li>
		<li>
			<button class="custom-menu-item JSexecute" data-execute='[{"table":"dobavljac_cenovnik", "column":"flag_prikazi_u_cenovniku", "val":"0"},{"table":"roba", "column":"flag_prikazi_u_cenovniku", "val":"0"}]'>Skini sa web-a</button>
		</li>		
		<li>
			<button class="custom-menu-item JSexecute" data-execute='[{"table":"dobavljac_cenovnik", "column":"flag_prikazi_u_cenovniku", "val":"1"},{"table":"roba", "column":"flag_prikazi_u_cenovniku", "val":"1"}]'>Stavi na web</button>
		</li>
		<li>
			<button class="custom-menu-item JSexecute" data-execute='[{"table":"dobavljac_cenovnik", "column":"flag_zakljucan", "val":"1"},{"table":"roba", "column":"flag_zakljucan", "val":"true"}]'>Zaključaj</button>
		</li>   
		<li>
			<button class="custom-menu-item JSexecute" data-execute='[{"table":"dobavljac_cenovnik", "column":"flag_zakljucan", "val":"0"},{"table":"roba", "column":"flag_zakljucan", "val":"false"}]'>Otključaj</button>
		</li>
		<li>
			<button class="custom-menu-item" id="kolicina_null">Storniraj kolicine na 0.</button>
		</li>
		<li>
			@if(AdminOptions::gnrl_options(3006)==1)
			<button class="custom-menu-item" id="JSKarakSlikeRunModal" value="">Preuzmi dodatne karakteristike i slike</button>
			@else
			<button class="custom-menu-item JSKarakSlike" value="">Preuzmi dodatne karakteristike i slike</button>
			@endif
		</li>
		<li>
			<button class="custom-menu-item" id="obrisi" value="">Obriši artikal</button>
		</li>
		<li>
			<button class="custom-menu-item" id="prepisi_cenu" value="">Prepiši web cenu</button>
		</li>
		<li>
			<button class="custom-menu-item" id="povezi_artikle" value="">Poveži artikle (F9)</button>
		</li>
		<li>
			<button class="custom-menu-item" id="razvezi_artikle" value="">Razveži artikle (F10)</button>
		</li>
		<li>
			<button class="custom-menu-item JSexecute" data-execute='[{"table":"dobavljac_cenovnik", "column":"flag_aktivan", "val":"1"},{"table":"roba", "column":"flag_aktivan", "val":"1"}]'>Prebaci u aktivne</button>
		</li>
		<li>
			<button class="custom-menu-item JSexecute" data-execute='[{"table":"dobavljac_cenovnik", "column":"flag_aktivan", "val":"0"},{"table":"roba", "column":"flag_aktivan", "val":"0"}]'>Prebaci u neaktivne</button>
		</li>
		<li>
			<button class="custom-menu-item" id="JSRefresh" value="">Osveži (F5)</button>
		</li>
		<li>
			<button class="custom-menu-item" id="JSEditTeksta" value="">Edit teksta</button>
		</li>
		
	</ul>

	<section class="webimport-table">
		<div class="flat-box">
			<div class="row">
				<div class="column medium-12 large-6 articles-number">
					<span>Broj artikala: <b>{{$count}}</b></span>
					|<span>Po strani: <b>{{$limit}}</b></span>
					|<b>Selektovano: <span id="JSselektovano">0</span></b>
				</div>
				<div class="column medium-12 large-6">
					{{ is_array($tabela) ? Paginator::make($tabela, $count, $limit)->links() : '' }}
				</div>
			</div>
 		<div class="row">
			<div class="table-scroll artiacle-table">
				<table class="dc_table">
					<thead class="fixed-head">
				    	<tr>
				    		<th id="but-col" class="table-head"></th>
				    		<th id="dob-col" class="table-head JSSort tooltipz" aria-label="Dobavljač" data-coll="dc.partner_id" data-sort="ASC">Dob</th>
				      		<th id="rid-col" class="table-head JSSort" data-coll="dc.roba_id" data-sort="ASC">Roba id</th>
				      		<!-- <th id="sku-col" class="table-head JSSort" data-coll="dc.sku" data-sort="ASC">Sku</th> -->
				      		<th id="sfr-col" class="table-head JSSort" data-coll="dc.sifra_kod_dobavljaca" data-sort="ASC">Šifra</th>

				      		<th id="akt-col" class="table-head JSSort tooltipz" aria-label="Aktivan" data-coll="dc.flag_aktivan" data-sort="ASC">Akt.</th>

				      		<th id="fpc-col" class="table-head JSSort" data-coll="dc.flag_prikazi_u_cenovniku" data-sort="ASC">Web</th>
				      		<th id="sl-col" class="table-head JSSort" data-coll="dc.flag_slika_postoji" data-sort="ASC">Slika</th>
				      		<th id="op-col" class="table-head JSSort" data-coll="dc.flag_opis_postoji" data-sort="ASC">Opis</th>
				      		<th id="naz-col" class="table-head JSSort" data-coll="dc.naziv" data-sort="ASC">Naziv</th>
				      		<th id="kol-col" class="table-head JSSort tooltipz" aria-label="Količina" data-coll="dc.kolicina" data-sort="ASC">Kol</th>
				      		<th id="wc-col" class="table-head JSSort tooltipz" aria-label="Web Cena" data-coll="dc.web_cena" data-sort="ASC">WCen</th>
				      		<th id="wm-col" class="table-head JSSort tooltipz" aria-label="Web marža" data-coll="dc.web_marza" data-sort="ASC">WM</th>
				      		<th id="nc-col" class="table-head JSSort tooltipz" aria-label="Nabavna cena" data-coll="dc.cena_nc" data-sort="ASC">NC</th>
				      		<th id="ncp-col" class="table-head tooltipz" aria-label="Nabavna cena + PDV">NC+PDV</th>
				      		<th id="mc-col" class="table-head JSSort tooltipz" aria-label="Maloprodajna cena" data-coll="dc.mpcena" data-sort="ASC">MCen</th>
				      		<th id="mpm-col" class="table-head JSSort tooltipz" aria-label="Maloprodajna marža" data-coll="dc.mp_marza" data-sort="ASC">MPM</th>
				      		<th id="pmc-col" class="table-head JSSort tooltipz" aria-label="Preporučena Maloprodajna Cena" data-coll="dc.pmp_cena" data-sort="ASC">PMC</th>
				      		<th id="pdv-col" class="table-head">PDV</th>
				      		<th id="gr-col" class="table-head JSSort" data-coll="dc.grupa" data-sort="ASC">Grupa</th>
				      		<th id="pgr-col" class="table-head JSSort" data-coll="dc.podgrupa" data-sort="ASC">Podgrupa</th>
				      		<th id="ngr-col" class="table-head JSSort" data-coll="dc.grupa_pr_id" data-sort="ASC">Naša grupa</th>
				      		<th id="pro-col" class="table-head JSSort" data-coll="dc.proizvodjac" data-sort="ASC">Proizvođač</th>
				      		<th id="npr-col" class="table-head JSSort" data-coll="dc.proizvodjac_id" data-sort="ASC">Naš proizvođač</th>
				      		<!-- <th id="mod-col" class="table-head JSSort" data-coll="dc.model" data-sort="ASC">Model</th> -->
				      		<th id="zak-col" class="table-head JSSort tooltipz-left" aria-label="Zaključan" data-coll="dc.flag_zakljucan" data-sort="ASC">Zklj</th>
				      	</tr>
				  	</thead>
				  	<tbody id="selectable" class="web-import-selectable">
				  	@if ($tabela != null)
						@foreach($tabela as $row)
						    <tr class="ui-widget-content {{ AdminImport::rowColors($row->flag_aktivan,$row->povezan) }}" data-id="{{ $row->dobavljac_cenovnik_id  }}">
						    	<td class="but-col"><button class="btn btn-secondary edit-article-btn btn-circle small JSEditRow tooltipz-right" aria-label="Izmeni" data-sifra="{{ $row->sifra_kod_dobavljaca }}" data-zakljucan="{{ $row->flag_zakljucan }}"><i class="fa fa-pencil" aria-hidden="true"></i></button></td>

						    	<td class="dob-col">
						    		<span>{{ $row->dobavljac_naziv }}</span>
						    	</td>

								<td class="check-roba_id">{{ $row->roba_id }}</td>
						     <!--    <td class="sku-col">{{ $row->sku }}</td>
 -->
								<td class="sfr-col sifra">
									<span class="JSsifraVrednost">
										{{ $row->sifra_kod_dobavljaca }}
									</span>
									<span class="JSEditSifraBtnSpan">
										<input type="text" class="JSsifraInput" value="{{ htmlentities($row->sifra_kod_dobavljaca) }}" />
										<i class="fa fa-pencil JSEditSifraBtn" aria-hidden="true"></i>
									</span>
								</td>
								
								<td class="flag_aktivan"><span>{{ $row->flag_aktivan == 1 ? '<i class="fa fa-check green" aria-hidden="true"></i>' : '<i class="fa fa-times red" aria-hidden="true"></i>' }}</span></td>

								<td class="flag_prikazi_u_cenovniku"><span>{{ $row->flag_prikazi_u_cenovniku == 1 ? '<i class="fa fa-check green" aria-hidden="true"></i>' : '<i class="fa fa-times red" aria-hidden="true"></i>' }}</span></td>

								<td class="flag_slika_postoji sl-col">
									<span>  {{ $row->flag_slika_postoji == 1 ? '<i class="fa fa-check green" aria-hidden="true"></i>' : '<i class="fa fa-times red" aria-hidden="true"></i>' }}</span>
								</td>

								<td class="flag_opis_postoji sl-col">
									<span>	{{ $row->flag_opis_postoji == 1 ? '<i class="fa fa-check green" aria-hidden="true"></i>' : '<i class="fa fa-times red" aria-hidden="true"></i>' }}
									</span>
								</td>

								<td class="table-fixed-width naziv" style="max-width: {{$naziv_width}}px;width: {{$naziv_width}}px;">
									<span class="JSnazivVrednost">
										{{ $row->naziv }}
									</span>
									<span class="JSEditNazivBtnSpan">
										<input type="text" class="JSnazivInput" value="{{ htmlentities($row->naziv) }}" />
										<i class="fa fa-pencil JSEditNazivBtn" aria-hidden="true"></i>
									</span>
								</td>
								
								<td class="kolicina">
									<span class="JSkolicinaVrednost">{{ isset($row->kolicina) ? intval($row->kolicina) : '' }}</span>
									<span class="JSEditKolicinaBtnSpan">
										<input type="number" class="JSkolicinaInput" value="{{ isset($row->kolicina) ? intval($row->kolicina) : '' }}" />
										<i class="fa fa-pencil JSEditKolicinaBtn" aria-hidden="true"></i>
									</span>
								</td>				

								<td class="cena_web_marza"><span>{{ round($row->web_cena).'.00' }}</span></td>
								<td class="web_marza"><span>{{ $row->web_marza }}</span></td>
								<td class="cena_nc"><span>{{ round($row->cena_nc).'.00' }}</span></td>
								<td class="cena_nc_pdv"><span>{{ round(($row->cena_nc*(1+$row->tarifna_grupa_porez/100))).'.00' }}</span></td>
								<td class="cena_mp_marza"><span>{{ round($row->mpcena).'.00' }}</span></td>
								<td class="mp_marza"><span>{{ round($row->mp_marza).'.00' }}</span></td>
								<td class="pmc-col"><span>{{ round($row->pmp_cena).'.00' }}</span></td>
								<td class="tarifna_grupa_id"><span>{{ $row->tarifna_grupa_porez }}</span></td>
								<td class="gr-col"><span>{{ $row->grupa }}</span></td>
								<td class="table-fixed-width-small pgr-col"><span>{{ $row->podgrupa }}</span></td>
								<td class="grupa_pr_id"><span>{{ $row->nasa_grupa }}</span></td>
								<td class="pro-col"><span>{{ $row->proizvodjac }}</span></td>
								<td class="proizvodjac_id"><span>{{ $row->nas_proizvodjac }}</span></td>
								<!-- <td class="model">{{ $row->model }}</td> -->
								<td class="flag_zakljucan"><span>{{ $row->flag_zakljucan == 1 ? 'DA' : 'NE' }}</span></td>
						    </tr>
						@endforeach
					@endif		   
				  	</tbody>
				</table>
			</div><!--   table end -->
		</div>

            <div class="row">
                <div class="column medium-12 large-6 large-offset-6">
                    {{ is_array($tabela) ? Paginator::make($tabela, $count, $limit)->links() : '' }}
                </div>
            </div>
			<div class="row">
				<div class="table-scroll second-table-import artiacle-table">
					<table>
						<thead>
					    	<tr class="table-fixed-header__tr">
					    		<th class=""></th>
					      		<th class="tooltipz" aria-label="Dobavljač">Dob</th>
					      		<th class="">Roba_id</th>
					      		<th class="">Šifra</th>
					      		<th class="tooltipz" aria-label="Aktivan">Akt.</th>
					      		<th class="">Web</th>
					      		<th height="20" class=" naziv-fix">Naziv</th>
					      		<th class="tooltipz" aria-label="Količina">Kol</th>
					      		<th class="tooltipz" aria-label="Nabavna cena">NC</th>
					      		<th class="tooltipz" aria-label="Web marža">WM</th>
					      		<th class="tooltipz" aria-label="Web cena">WCen</th>
					      		<th class="">Naša grupa</th>
					      		<th class="">Naš proizvođač</th>		      		 
					      	</tr>
					  	</thead>
					  	<tbody id="JSRobaList">
					  		@foreach($roba as $row)
						    <tr class="JSRobaRow" data-id="{{ $row->roba_id  }}">
						    	<td class="">
									<a target="_blank" class="" href="{{AdminOptions::base_url()}}admin/product/{{ $row->roba_id }}">
										<button class="btn btn-secondary btn-xm edit-article-btn btn-circle small tooltipz-right" aria-label="Izmeni">
											<i class="fa fa-pencil" aria-hidden="true"></i>
										</button>
									</a>
								</td>
						      	<td class="dobavljac">{{ AdminSupport::dobavljac($row->dobavljac_id) }}</td>
								<td>{{ $row->roba_id }}</td>
								<td>{{ $row->sifra_d }}</td>
								<td class="flag_aktivan">{{ $row->flag_aktivan == 1 ? 'DA' : 'NE' }}</td>
								<td class="flag_prikazi_u_cenovniku">{{ $row->flag_prikazi_u_cenovniku == 1 ? 'DA' : 'NE' }}</td>
								<td class="roba_naziv">{{ $row->naziv }}</td>
								<td class="kolicina">{{ isset($row->kolicina) ? intval($row->kolicina) : '' }}</td>	
								<td class="racunska_cena_nc">{{ $row->racunska_cena_nc }}</td>
								<td class="web_marza">{{ $row->web_marza }}</td>
								<td class="web_cena">{{ $row->web_cena }}</td>
								<td>{{ AdminArticles::findGrupe($row->grupa_pr_id,'grupa') }}</td>
								<td>{{ AdminSupport::find_proizvodjac($row->proizvodjac_id,'naziv') }}</td>
						    </tr>
						    @endforeach
						    @if($criteriaImport['grupa_pr_id'] || $criteriaImport['proizvodjac'] || $criteriaImport['dobavljac'] || $criteriaImport['search'])
						    <tr>
						    	<td colspan="19">
						    		<div class="btn-container center no-margin">
						    			<button id="JSMore" data-page="2" class="btn btn-small btn-secondary btn-block">VIŠE</button>
						    		</div>
						    	</td>
						    </tr>
						    @endif
					  	</tbody>
					</table>
				</div>

			</div>
		</div>
	</section> <!-- end of .webimport-table -->
</section>

<div id="wait">
	<p><img class="loader" src="{{ AdminOptions::base_url()}}images/admin/wheel.gif">Molimo vas sačekajte...</p>
</div>

<div id="JSEditFieldsModal" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	<h4 class="modal-title-h4">Izmeni polja</h4><br>
		<input type="hidden" id="JSEditDCId" class="JSdcid">
		<input type="hidden" id="JSEditPorez" class="JSporez">
		<div class="columns medium-12 large-12">
			<div class="row">
				<div class="columns medium-12 large-12">					
					<label>Naziv</label>
					<input type="text" id="JSEditNaziv">
				</div>
			</div>
			<div class="row">
				<div class="columns medium-4 large-4">
					<label>Količina</label>
					<input type="text" id="JSEditKolicina">
				</div>
				<div class="columns medium-4 large-4">
					<label>Web marža</label>
					<input type="text" id="JSEditWM">
					<span class="JSEditWMbtn btn  btn-primary btn-small-check"><i class="fa fa-check" aria-hidden="true"></i></span>
				</div>
				<div class="columns medium-4 large-4">
					<label>MP marža</label>
					<input type="text" id="JSEditMM">
					<span class="JSEditMMbtn btn  btn-primary btn-small-check"><i class="fa fa-check" aria-hidden="true"></i></span>
				</div>
			</div>
			<div class="row">
				<div class="columns medium-4 large-4">
					<label>Nabavna cena</label>
					<input type="text" id="JSEditNC">
					<span class="JSEditNCbtn btn  btn-primary btn-small-check"><i class="fa fa-check" aria-hidden="true"></i></span>
				</div>
				<div class="columns medium-4 large-4">
					<label>Web cena</label>
					<input type="text" id="JSEditWC">
					<span class="JSEditWCbtn btn  btn-primary btn-small-check"><i class="fa fa-check" aria-hidden="true"></i></span>
				</div>
				<div class="columns medium-4 large-4">
					<label>MP cena</label>
					<input type="text" id="JSEditMC">
					<span class="JSEditMCbtn btn  btn-primary btn-small-check"><i class="fa fa-check" aria-hidden="true"></i></span>
				</div>
			</div>
			<div class="row">
				<div class="columns medium-12 large-12">	
					<div class="edit-modl-div">			
						<span>Slike</span>						
						<div id="JSDobSlike"></div>
					</div>	
				</div>
			</div>

			<div class="row">
				<div class="columns medium-4 large-4">					
					<label>Opis</label>
					<textarea type="text" id="JSOpis" ></textarea>
				</div>
			</div>

			<div class="row">
				<div class="columns medium-12 large-12 text-center btn-container">
					<button id="submitBtn" class="btn btn-primary save-it-btn"> Sačuvaj</button>

					
				</div>
			</div>
		</div>
</div>

<div id="JSPrihvatiLagerModal" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	<h4 class="modal-title-h4">Prihvati lager</h4><br>
		<div class="columns medium-12 large-12">
			<div class="row">
				<div class="columns medium-8 large-8">
					<select class=" admin-select m-input-and-button__input" id="magacin_select">
						@foreach(DB::table('orgj')->where('b2c',1)->orderBy('orgj_id','asc')->get() as $row)
							@if($row->orgj_id == DB::table('imenik_magacin')->where('izabrani',1)->pluck('orgj_id'))
							<option value="{{ $row->orgj_id }}" selected>{{ $row->naziv }}</option>
							@else
							<option value="{{ $row->orgj_id }}">{{ $row->naziv }}</option>
							@endif
						@endforeach
					</select>
				</div>
				<div class="columns medium-4 large-4">					
					<button id="" class="btn  btn-primary JSPrihvatiLager"><i class="fa fa-check" aria-hidden="true"></i> Prihvati lager</button>
				</div>
			</div>
		</div>
</div>

<div id="JSPrihvatiCeneModal" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	<h4 class="modal-title-h4">Prihvati cene i naziv</h4><br>
		<div class="columns medium-12 large-12">
			<div class="row">
			<div>Primarni artikl kod povezanih artikala</div>
				<div class="columns medium-3 large-3">
					<select class=" admin-select m-input-and-button__input" id="JSPrimaryPartner">
						<option value="0">Izaberi dobavljaca</option>
						@foreach(AdminImport::dobavljaci_web_import_tabela() as $dobavljac)
						<option value="{{ $dobavljac->partner_id }}">{{ $dobavljac->naziv }}</option>
						@endforeach
					</select>
				</div>
				<div class="columns medium-2 large-2">
					<input type="checkbox" name="kolicina" id="kolicina" checked>
					Sa kolicinom
				</div>
				<div class="columns medium-2 large-2">
					<input type="checkbox" name="cena" id="cena" checked>
					Sa najnizom cenom
				</div>
				<div class="columns medium-2 large-2">					
					<button class="btn  btn-primary JSPrihvatiCeneNaziv" data-naziv="0"><i class="fa fa-check" aria-hidden="true"></i> Prihvati cene</button>
				</div>
				<div class="columns medium-2 large-2">					
					<button class="btn  btn-primary JSPrihvatiCeneNaziv" data-naziv="1"><i class="fa fa-check" aria-hidden="true"></i> Prihvati naziv</button>
				</div>
			</div>
		</div>
</div>

<div id="JSKarakSlikeModal" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	<h4 class="modal-title-h4">Preuzmi dodatne karakteristike i slike</h4><br>
	<div class="columns medium-12 large-12">
		<div class="row">
		<div>Primarni artikl kod povezanih artikala</div>
			<div class="columns medium-8 large-8">
				<select class=" admin-select m-input-and-button__input" id="JSPrPartnerId">
					<option value="0">Izaberi dobavljaca</option>
					@foreach(AdminImport::dobavljaci_web_import_tabela() as $dobavljac)
					<option value="{{ $dobavljac->partner_id }}">{{ $dobavljac->naziv }}</option>
					@endforeach
				</select>
			</div>
			<div class="columns medium-4 large-4">					
				<button class="btn  btn-primary JSKarakSlike" data-naziv="0"><i class="fa fa-check" aria-hidden="true"></i> Preuzmi</button>
			</div>
		</div>
	</div>
</div>

<div id="JSEditModal" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	<h4 class="modal-title-h4">Izmena artikla</h4><br>
	<div class="column medium-12 large-12 text-center">
		Artikal je zaključan! Da li želite da otključate artikal?
	</div>
	<div class="column medium-12 large-12 text-center">
		<button id="JSEditYes" class="btn btn-primary m-input-and-button__btn">DA</button>
		<button id="JSEditNo" class="btn btn-danger m-input-and-button__btn">NE</button>
	</div>
</div>

<div id="JStextEditModal" class="reveal-modal sm-modal txt-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	<h4 class="modal-title-h4">Edit teksta</h4>

	<div class="row">
		<div class="columns medium-5 text-right">
			<input type="radio" id="JStextEditNaziv" name="naziv-opis" value="naziv" checked />Naziv
		</div>
		<div class="columns medium-6 center">
			<input type="radio" id="JStextEditOpis" name="naziv-opis" value="opis">Opis
		</div>
	</div> 
	 
	<h3 class="title-txt">Dodavanje teksta</h3>
	 
	<div class="row"> 
		<div class="columns medium-6 center">
			<input type="radio" id="JStextEditStart" name="start-end" value="start" checked /><label>Na početku</label> 
			<input type="radio" id="JStextEditEnd" name="start-end" value="end"><label>Na kraju</label>	
		</div>
		<div class="columns medium-6">	  
			<input type="text" id="JStextEditNazivInput" class="m-input-and-button__input"  placeholder="Tekst">
			<button id="JStextInsertButton" class="btn btn-primary btn-abs m-input-and-button__btn">Dodaj</button>
		</div>
	</div> 
	<br>

	<h3 class="title-txt">Zameni tekst</h3>

	<div class="row"> 
		<div class="columns medium-12">
			<div class="row"> 
				<div class="columns medium-6 center">
					<input type="radio" id="JStextEditDeo" name="ceo-deo" value="deoTeksta" checked /><label>Deo teksta</label> 
					<input type="radio" id="JStextEditCelo" name="ceo-deo" value="celaRec"><label>Cela reč</label>
				</div>
			</div>
			<div class="row"> <br>
				<div class="columns medium-6">
					<input type="text" name="JStextEditInput1" id="JStextEditInput1" class="m-input-and-button__input" placeholder="Tekst koji se menja">
				</div>
				<div class="columns medium-6"> 
					<input type="text" name="JStextEditInput2" id="JStextEditInput2" class="m-input-and-button__input" placeholder="Novi tekst">
					<button id="JStextReplaceButton" class="btn btn-primary btn-abs m-input-and-button__btn">Zameni</button>
				</div> 
			</div>
		</div>
	</div>
	<br>
 	<label id="Results"></label>
</div>