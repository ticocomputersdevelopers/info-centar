<section id="main-content" class="b2cAnalics">
	<div class="row">
		<div class="column large-12">
			<h1 class="h1-title">Analitika</h1>
		</div>
	</div>
	<!-- analytics -->
	<div class="row">
		<div class="column medium-6 small-12 large-4 anly-box">
			<div class="flat-box">
				<ul class="anly-ul">
<!-- <li class="anly-ul__li">
						<span class="anly-ul__li__text">Porudžbine na čekanju</span>
						<span class="anly-ul__li__count">{{ AdminAnalitika::getNaCekanju() }}</span>
					</li>
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">Obrađene porudžbine</span>
						<span class="anly-ul__li__count">{{ AdminAnalitika::getObradjene() }}</span>
					</li> -->
					@if(isset($nove))
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">Nove porudžbine</span>
						<span class="anly-ul__li__count">{{ $nove }} </span>						
					</li>
					@else
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">Nove porudžbine</span>
						<span class="anly-ul__li__count">{{ AdminAnalitika::getNew() }}</span>
					</li>
					@endif
					@if(isset($prihvaceno))
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">Prihvaćene porudžbine</span>
						<span class="anly-ul__li__count">{{ $prihvaceno }} </span>	
					</li>
					@else
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">Prihvaćene porudžbine</span>
						<span class="anly-ul__li__count">{{ AdminAnalitika::getPrihvacene() }}</span>
					</li>
					@endif
					@if(isset($realizovano))
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">Realizovane porudžbine</span>
						<span class="anly-ul__li__count">{{ $realizovano }} </span>						
					</li>
					@else
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">Realizovane porudžbine</span>
						<span class="anly-ul__li__count">{{ AdminAnalitika::getRealizovane() }}</span>
					</li>
					@endif
					@if(isset($stornirano))
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">Stornirane porudžbine</span>
						<span class="anly-ul__li__count">{{ $stornirano }} </span>	
					</li>
					@else
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">Stornirane porudžbine</span>
						<span class="anly-ul__li__count">{{ AdminAnalitika::getStornirane() }}</span>
					</li>
					@endif
					@if(isset($ukupno))
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">Ukupno porudžbina</span>
						<span class="anly-ul__li__count">{{ $ukupno }} </span>	
					</li>
					@else
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">Ukupno porudžbina</span>
						<span class="anly-ul__li__count">{{ AdminAnalitika::getUkupnoPorudzbina() }}</span>
					</li>
					@endif

						<div class="graph-box">
							<canvas height=140 id="graph5"></canvas>
						</div>  
				</ul>
			</div>
		</div>
		<div class="column medium-6 small-12 large-4 anly-box">
			<div class="flat-box">
				<ul class="anly-ul">
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">Jučerašnji prihod</span>
						<span class="anly-ul__li__count">{{ number_format(AdminAnalitika::jucerasnjiPrihod(), 2, ',', ' ')  }} din</span>
					</li>
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">Proteklih mesec dana</span>
						<span class="anly-ul__li__count">{{ number_format(AdminAnalitika::mesecniPrihod(), 2, ',', ' ')  }} din</span>
					</li>
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">Ukupan prihod</span>
						<span class="anly-ul__li__count">{{ 
						number_format(AdminAnalitika::ukupanPrihod(), 2, ',', ' ')  }} din</span>
					</li>
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">Ukupna Razlika u ceni</span>
						<span class="anly-ul__li__count">{{ 
						number_format(AdminAnalitika::ukupanRUC(), 2, ',', ' ')  }} din</span>
					</li>

				</ul>
			</div>
			
			<!-- FILTER PRIHODA -->
			<div class="flat-box">
				<ul class="anly-ul">
					<li class="anly-ul__li"> 
						<div class="column medium-5">
							<label for="">Datum Od</label>
							<input id="datum_od_din" class="datum-val has-tooltip" name="datum_od_din" type="text" value="{{$od}}">
						</div>
						<div class="column medium-5">
							<label for="">Datum Do</label>
							<input id="datum_do_din" class="datum-val has-tooltip" name="datum_do_din" type="text" value="{{$do}}">
						</div>
						<div class="column medium-2 no-padd">
							<label for="">&nbsp;</label>
							<a class="btn btn-danger fr" href="/admin/analitika">Poništi</a>
						</div> 
					</li>

					@if(isset($prihod))
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">Prihod u odabranom periodu: </span>
						<span class="anly-ul__li__count">{{ number_format($prihod, 2, ',', ' ') }} din</span>
					</li>
					<li class="anly-ul__li">
						<span class="anly-ul__li__text tooltipz" aria-label="Razlika u ceni">RUC</span>
						<span class="anly-ul__li__count">{{number_format($razlika, 2, ',', ' ')  }} din</span>						
					</li>
					
					@else
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">Prihod u odabranom periodu: </span>
						<span class="anly-ul__li__count">Nema rezultata</span>
					</li>
					<li class="anly-ul__li">
						<span class="anly-ul__li__text tooltipz" aria-label="Razlika u ceni">RUC</span>
						<span class="anly-ul__li__count">Nema rezultata</span>
					</li>
					@endif
				</ul>
			</div>
		</div>

		<div class="column medium-6 small-12 large-4 anly-box">
			<div class="flat-box">
				<ul class="anly-ul">
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">Ukupno artikala u prodavnici</span>
						<span class="anly-ul__li__count">{{ AdminAnalitika::ukupnoArtikala() }}</span>
					</li>
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">Ukupno korisnika registrovano</span>
						<span class="anly-ul__li__count">{{ AdminAnalitika::ukupnoKorisnika() }}</span>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="column medium-12 large-6">
			<div class="flat-box graph-box">
				<canvas height=140 id="graph"></canvas>
			</div>  
		</div>
		<div class="column medium-12 large-6">
			<div class="flat-box graph-box">
				<canvas height=140 id="graph2"></canvas>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="column medium-12 large-6">
			<div class="flat-box graph-box">
				<canvas height=140 id="graph3"></canvas>
			</div>  
		</div>
		<div class="column medium-12 large-6">
			<div class="flat-box graph-box">
				<canvas height=140 id="graph4"></canvas>
			</div>
		</div>
	</div>

	<div class="row">
		 <div class="flat-box"> 
			<div class="column medium-12 large-6">
			  	<table class="analitics-table">
					<thead>
						<th>&nbsp;Naziv</th>
						<th>Broj prodatih</th>
					</thead>
					<tbody>
					@foreach($artikli as $row)
					@if($row->count > 0)
					<tr>
						<td class="first-td"> {{ AdminCommon::analitika_title($row->roba_id) }} </td>
						<td> {{ $row->count + 0 }} </td> 
					</tr>
					@endif
					@endforeach
					</tbody>
				</table>
			</div>
 	 		<div class="column medium-12 large-6">
			<table class="analitics-table">
				<tr>
					<th>&nbsp; Naziv</th>
					<th>Broj pregleda</th>
				</tr>
				@foreach(AdminCommon::mostPopularArticles() as $row)
				@if($row->pregledan_puta > 0)
				<tr>
					<td class="first-td"> {{ AdminCommon::analitika_title($row->roba_id) }} </td>
					<td> {{ $row->pregledan_puta }} </td>
				</tr>
				@endif
				@endforeach
			</table>
		</div>
		<div class="columns medium-12 no-padd"> 
			<div class="column medium-12">
				<table class="analitics-table">
					<thead> 
						<tr>
							<th>&nbsp;Naziv (Grupe / Artikla)</th>
							<th>Broj prodatih</th>
							<th>Ukupna cena</th>
							<th>RUC</th>
						</tr>
					</thead>
					<tbody> 
						@foreach($analitikaGrupa as $grupa)
						<tr class="JSAnalitikaGrupa" data-id="{{ $grupa->grupa_pr_id }}" data-isopen="closed">
							<td class="first-td">
								<a href="#!">{{$grupa->grupa}}</a>
							</td>
							<td>
								{{round($grupa->sum)}}
							</td>
							<td>
								{{sprintf("%.2f",$grupa->ukupno)}}
							</td>
							<td>
								{{sprintf("%.2f",$grupa->razlika)}} ( {{round($grupa->nc_cena)}} )
							</td>
						</tr>
						<?php $suma=0; ?>
						@foreach(AdminAnalitika::prikazGrupa($grupa->grupa_pr_id) as $prikaz)
							@if(isset($prikaz->naziv))
							<tr class="JSAnalitikaArtikliGrupe sub-td" data-id="{{ $grupa->grupa_pr_id }}" hidden>
								<td class="colapsable-td">&nbsp;&nbsp; - {{$prikaz->naziv}} </td>
								<td> {{round($prikaz->sum)}} </td>
								<td> {{sprintf("%.2f",$prikaz->ukupno)}} </td>
								<td>
								@if($prikaz->racunska_cena_nc > 0)
								{{sprintf("%.2f",$prikaz->razlika)}}
								<?php $suma +=$prikaz->razlika ?>
								@endif
								</td>
							</tr>
							@endif
						@endforeach

						@endforeach
					</tbody>
				</table>
			</div>
			@if(false)
			<div class="column medium-6">
				<div class="table-scroll"> 
				<table class="analitics-table analitics-table-artical">
					<thead> 
						<tr>
							<th>&nbsp;Naziv artikla</th>
							<th>Broj prodatih</th>
							<th>Ukupna cena</th>
							<th>Ruc</th>
						</tr>
					</thead>
					<?php $suma=0; ?>
						@foreach(array() as $prikaz)
						@if(isset($prikaz->naziv))
				    <tbody> 
						<tr>
						<td> {{$prikaz->naziv}} </td>
						<td> {{round($prikaz->sum)}} </td>
						<td> {{sprintf("%.2f",$prikaz->ukupno)}} </td>
						<td>
						@if($prikaz->racunska_cena_nc > 0)
						{{sprintf("%.2f",$prikaz->razlika)}}
						<?php $suma +=$prikaz->razlika ?>
						@endif
						</td>
						</tr>
						@endif
						@endforeach
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td><b> {{sprintf("%.2f",$suma)}} </b></td>
						</tr>
					</tbody>
				</table>
				</div>
			</div>
		 	@endif
		</div>
		<div class="columns medium-12 no-padd"> 
			<div class="column medium-6">
				<h3 class="title-med">Najprodavanije grupe</h3>
				<div class="graph-box"><br>
					<canvas height=140 id="graph6"></canvas>
				</div>
			</div>
			<div class="column medium-6">
				<h3 class="title-med">Razlika u ceni (RUC)</h3>
				<div class="graph-box"><br>
			   		<canvas height=140 id="graph7"></canvas>
				</div>
			</div>
		</div>
		</div>
	</div>
 

<div class="row">
	 <div class="flat-box"> 
	<h3 class="title-med">Najposećenije grupe</h3>

	<div class="column medium-6"> 
		<div class="graph-box"><br>
		<canvas height=140 id="graph8"></canvas>
		</div>
	</div>
	<div class="column medium-6">
		<table class="analitics-table">
			<thead> 
			<tr>
				<th>Naziv grupe</th>
				<th>Broj poseta</th>
			</tr>
			</thead>
			<tbody> 
				@foreach($analitikaGrupaPregled as $grupa)
				<tr>
					<td>
						@if($datum_od && $datum_do)
						{{$grupa->grupa1}}
						@else
						{{$grupa->grupa1}}
						@endif
					</td>
					<td>
						{{$grupa->sum}}
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div> 
	</div>
</div>












</section> <!-- end of #main-content -->

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.1/Chart.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.1/Chart.bundle.js"></script>

<script>

var ctx = document.getElementById("graph");
var myChart = new Chart(ctx, {
	type: 'line',
	data: {
		labels: [
				@foreach(AdminAnalitika::orderYears($od,$do) as $key => $year_name)
					@foreach(array("Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul", "Avg", "Sep", "Okt", "Nov", "Dec") as $month_name)
						"{{ $month_name.' ('.$year_name.')' }}",
					@endforeach
				@endforeach
				],
		datasets: [{
			lineTension: 0.3,
			label: ' Narudžbine',
			data: [ @foreach(AdminAnalitika::orderYears($od,$do) as $year)
						@foreach(range(1,12) as $month)
							{{ AdminAnalitika::mesecnaAnalitika($month,$year,$od,$do)}},
						@endforeach
					@endforeach
					],
			backgroundColor: [
				'rgba(118, 186, 108, 0.46)'
			],
			borderColor: [
				'rgba(118, 186, 108, 1)'
			],
			borderWidth: 1
		}]
	},
	options: {
		// responsive: false,
		scales: {
			yAxes: [{
				ticks: {
					beginAtZero:true
				}
			}]
		}
	}
});

var ctx2 = document.getElementById("graph2");
var myChart2 = new Chart(ctx2, {
	type: 'line',
	data: {
		labels: [@foreach(AdminAnalitika::orderYears($od,$do) as $key => $year_name)
					@foreach(array("Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul", "Avg", "Sep", "Okt", "Nov", "Dec") as $month_name)
						"{{ $month_name.' ('.$year_name.')' }}",
					@endforeach
				@endforeach
				],
		datasets: [{
			lineTension: 0.3,
			label: ' Prihod',
			data: [ @foreach(AdminAnalitika::orderYears($od,$do) as $year)
						@foreach(range(1,12) as $month)
							{{ AdminAnalitika::mesecnaAnalitika2($month,$year,$od,$do)}},
						@endforeach
					@endforeach
					],
			backgroundColor: [
				'rgba(110, 151, 207, 0.58)'
			],
			borderColor: [
				'rgba(110, 151, 207, 1)'
			],
			borderWidth: 1
		}]
	},
	options: {
		// responsive: false,
		scales: {
			yAxes: [{
				ticks: {
					beginAtZero:true
				}
			}]
		}
	}
});

var ctx3 = document.getElementById("graph3");
var myChart3 = new Chart(ctx3, {
	type: 'bar',
	data: {
		labels: [
			@foreach($artikli as $row)
				<?php echo '"'.AdminCommon::analitika_title($row->roba_id) .'",' ?>
			@endforeach
			],
		datasets: [{

			label: ' Najprodavaniji artikli',
			data: [ 
				@foreach($artikli as $row)
					{{ "$row->count" }},
				@endforeach
				],
			backgroundColor: [
				@foreach($artikli as $row)
					'rgba(255, 254, 15, 0.55)',
				@endforeach
				
			],
			borderColor: [
				@foreach($artikli as $row)
					'#ffab00',
				@endforeach
			],
			borderWidth: 1
		}]
	},
	options: {
		// responsive: false,
		
		scales: {
			yAxes: [{
				ticks: {
					beginAtZero:true
				}
			}],
			xAxes: [{display:false}]
		}
	}
});

var ctx4 = document.getElementById("graph4");
var myChart4 = new Chart(ctx4, {
	type: 'bar',
	data: {
		labels: [
			@foreach(AdminCommon::mostPopularArticles() as $row)
				<?php echo "'".AdminCommon::analitika_title($row->roba_id) ."'," ?>
			@endforeach
		],
		datasets: [{
			label: ' Artikli sa najvećim brojem pregleda',
			data: [ @foreach(AdminCommon::mostPopularArticles() as $row)
						{{ "$row->pregledan_puta" }},
					@endforeach
					],
			backgroundColor: [
				'rgba(240, 65, 36, 0.6)',
				'rgba(240, 65, 36, 0.5)',
				'rgba(240, 65, 36, 0.4)',
				'rgba(240, 65, 36, 0.3)'
			],
			borderColor: [
				'rgba(240, 65, 36, 1)',
				'rgba(240, 65, 36, 1)',
				'rgba(240, 65, 36, 1)',
				'rgba(240, 65, 36, 1)'
			],
			borderWidth: 1
		}]
	},
	options: {
		// responsive: false,
		scales: {
			yAxes: [{
				ticks: {
					beginAtZero:true
				}
			}],
			xAxes: [{display:false}]
		}
	}
});


var ctx5 = document.getElementById("graph5");
var myChart5 = new Chart(ctx5, {
	type: 'doughnut',
	data: {
		labels: [
        	"Nove",
        	"Prihvaćene porudžbine",
			"Realizovane porudžbine",
        	"Stornirane"

		],
		datasets: [{
			label: '',
			data: [
				{{ $ukupno1 }},
				{{ $ukupno4 }},
				{{ $ukupno3 }}, 
				{{ $ukupno2 }} 
			],
			backgroundColor: [
				"#FF6384", 
                "#59cd74",
   				"#a6a6a6",
				"#a370c2"
			],
			borderColor: [
                "#FF6384",
                "#59cd74",
				"#a6a6a6",
                "#a370c2"
			],
			borderWidth: 1
		}]
	},
	options: {
		legend: {
            display: false
        }
	}
});

var ctx6 = document.getElementById("graph6");
var myChart6 = new Chart(ctx6, {
	type: 'doughnut',
	data: {
		labels: [
			@foreach($analitikaSkraceno as $grupa)
			'{{ $grupa->grupa }}',
			@endforeach
			'Ostalo'
		],
		datasets: [{
			label: '',
			data:[
			@foreach($analitikaSkraceno as $grupa_count)
			'{{ sprintf("%.2f",$grupa_count->ukupno) }}',
			@endforeach
			'{{ sprintf("%.2f",$ostalo) }}'
			],
			backgroundColor: [
				"#59cd74",
                "#FFCE56",
   				"#FF6384",
				"#a6a6a6",
   				"#009933",
   				"#8080ff"
			],
			borderColor: [
                "#59cd74",
                "#FFCE56",
   				"#FF6384",
				"#a6a6a6",
				"#009933",
   				"#8080ff"
			],
			borderWidth: 1
		}]
	},
	options: {
		legend: {
            display: true,
            position: 'bottom'
        }
	}
});

var ctx7 = document.getElementById("graph7");
var myChart7 = new Chart(ctx7, {
	type: 'bar',
	data: {
		labels: [
			@foreach($analitikaRuc as $ruc)
			'{{ $ruc->grupa }}',
			@endforeach
			
		],
		datasets: [{
			label: '',
			data:[
				<?php $s=0 ?>
				@foreach($analitikaRuc as $grupa_count)
				@if($grupa_count->razlika>0)
				'{{ sprintf("%.2f",$grupa_count->razlika) }}',
				@endif
				@endforeach
				@if ($ostalo1>0)
				'{{ sprintf("%.2f",$ostalo1) }}'
				@endif
			],
			backgroundColor: [
				"#59cd74",
                "#FFCE56",
   				"#FF6384",
				"#a6a6a6",
   				"#009933",
   				"#8080ff"
			],
			borderColor: [
               "#59cd74",
                "#FFCE56",
   				"#FF6384",
				"#a6a6a6",
				"#009933",
   				"#8080ff"
			],
			borderWidth: 1
		}]
	},
	options: {
		legend: {
            display: false,
            position: 'bottom'
        }
	}
});





var ctx8 = document.getElementById("graph8");
var myChart8 = new Chart(ctx8, {
	type: 'doughnut',
	data: {
		labels: [
			@foreach($analitikaGrupaPregled as $grupa)
			'{{ $grupa->grupa1 }}',
			@endforeach
		],
		datasets: 
		[{
			label: '',
			data:[
			@foreach($analitikaGrupaPregled as $grupa_sum)
			'{{ $grupa_sum->sum }}',
			@endforeach
			],
			backgroundColor: [
				"#59cd74",
                "#FFCE56",
   				"#FF6384",
				"#a6a6a6",
   				"#009933"

			],
			borderColor: [
                "#59cd74",
                "#FFCE56",
   				"#FF6384",
				"#a6a6a6",
				"#009933"
			],
			borderWidth: 1
		}]
	},
	options: {
		legend: {
            display: true,
            position: 'bottom'
        }
	}
});

</script>

