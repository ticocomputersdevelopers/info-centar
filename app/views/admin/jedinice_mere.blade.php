<div id="main-content" >
@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
@endif
	@include('admin/partials/tabs')
	<div class="row">
		<div class="large-3 medium-3 small-12 columns ">
			<div class="flat-box">
				<h3 class="title-med">Izaberi jedinicu mere</h3>
				<div class="manufacturer"> 
					<select class="JSeditSupport search_select">
						<option value="{{ AdminOptions::base_url() }}admin/jedinica-mere/0">Dodaj novi</option>
						@foreach(AdminSupport::getJediniceMere() as $row)
							<option value="{{ AdminOptions::base_url() }}admin/jedinica-mere/{{ $row->jedinica_mere_id }}" @if($row->jedinica_mere_id == $jedinica_mere_id) {{ 'selected' }} @endif>({{ $row->jedinica_mere_id }}) {{ $row->naziv }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>

		<section class="small-12 medium-12 large-5 large-centered columns">
			<div class="flat-box">

				<h1 class="title-med">{{ $title }}</h1>
				<!-- <h1 id="info"></h1> -->

				<form method="POST" action="{{ AdminOptions::base_url() }}admin/jedinica-mere-edit" enctype="multipart/form-data">
					<div class="row">
						<input type="hidden" name="jedinica_mere_id" value="{{ $jedinica_mere_id }}"> 

						<div class="columns medium-12 field-group{{ $errors->first('naziv') ? ' error' : '' }}">
							<label for="naziv">Naziv jedinice mere</label>
							<input type="text" name="naziv" value="{{ htmlentities(Input::old('naziv') ? Input::old('naziv') : $naziv) }}" autofocus="autofocus"> 
						</div>
					</div>
					<div class="row">
						<div class="btn-container center">
							<button type="submit" class="btn btn-primary save-it-btn">Sačuvaj</button>
						    @if($jedinica_mere_id != 0)
								<button class="btn btn-danger JSbtn-delete" data-link="{{ AdminOptions::base_url() }}admin/jedinica-mere-delete/{{ $jedinica_mere_id }}">Obriši
								</button>
							@endif
						</div>
					</div>
				</form>
				 
				</div> <!-- end of .flat-box -->
				
		</section>
	</div> <!-- end of .row -->
 </div>