<!DOCTYPE html>
<html>
    <head>
        <title>{{$title}}</title>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="msapplication-tap-highlight" content="no"/> 

        <link rel="icon" type="image/png" href="{{ AdminOptions::base_url()}}favicon.ico">

        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
        <link href="{{ AdminOptions::base_url()}}css/foundation.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ AdminOptions::base_url()}}css/normalize.css" rel="stylesheet" type="text/css" />
        <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"> -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="{{ AdminOptions::base_url()}}css/alertify.core.css">
        <link rel="stylesheet" href="{{ AdminOptions::base_url()}}css/alertify.default.css">
        <link href="{{ AdminOptions::base_url()}}css/admin.css" rel="stylesheet" type="text/css" />
        <script src="{{ AdminOptions::base_url()}}js/alertify.js" type="text/javascript"></script>
  
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-122633081-1"></script>
        <script>
         window.dataLayer = window.dataLayer || [];
         function gtag(){dataLayer.push(arguments);}
         gtag('js', new Date());

         gtag('config', 'UA-122633081-1');
        </script>
    </head>
<body class="{{ Session::get('adminHeaderWidth') }}">

      @include('admin.partials.aop_trial')
    <div class="flex-wrapper">
       
        <!-- HEADER -->
        <!-- MAIN MENU -->
        @if(in_array(AdminB2BOptions::web_options(130),array(0,1)))
    	   @include('admin.partials.header')
        @else
            @include('adminb2b.partials.header')
        @endif
    	
	    <!-- ORDER FILTERS -->
        @if($strana=='pocetna' AND Admin_model::check_admin(array(600,800)))
            @include('admin/porudzbine')
        @elseif($strana=='stranice' AND Admin_model::check_admin(array(7000)))
            @include('admin/pages')
        @elseif($strana=='kontakt' AND Admin_model::check_admin(array(1000)))
            @include('admin/kontakt')
        @elseif($strana=='baneri-slajderi' AND Admin_model::check_admin(array(7100)))
            @include('admin/baneri-slajderi')
        @elseif($strana=='footer' AND Admin_model::check_admin(array(7000)))
            @include('admin/footer')
        @elseif($strana=='podesavanja' AND Admin_model::check_admin(array(8000)))
            @include('admin/podesavanja')
        @elseif($strana=='artikli' AND Admin_model::check_admin(array(200)))
            @include('admin/artikli')
        @elseif($strana=='product' AND Admin_model::check_admin(array(200)))
            @include('admin/product')
        @elseif($strana=='product_short' AND Admin_model::check_admin(array(200)))
            @include('admin/product_short')
        @elseif($strana=='web_import' AND Admin_model::check_admin(array(400)))
            @include('admin/web_import') 
        @elseif($strana=='import_podaci_grupa' AND Admin_model::check_admin(array(400)))
            @include('admin/import_podaci_grupa')
        @elseif($strana=='grupe' AND Admin_model::check_admin(array(233,234,235,239,244,245,246,248)))
            @include('admin/grupe')
        @elseif($strana=='slike' AND Admin_model::check_admin(array(200)))
            @include('admin/slike')
        @elseif($strana=='opis' AND Admin_model::check_admin(array(220)))
            @include('admin/opis')
        @elseif($strana=='seo' AND Admin_model::check_admin(array(220)))
            @include('admin/seo')
        @elseif($strana=='karakteristike' AND Admin_model::check_admin(array(255,256)))
            @include('admin/karakteristike')
        @elseif($strana=='vezani_artikli' AND Admin_model::check_admin(array(5219)))
            @include('admin/vezani_artikli')
        @elseif($strana=='product_akcija' AND Admin_model::check_admin(array(200)))
            @include('admin/akcija')
        @elseif($strana=='dodatni_fajlovi' AND Admin_model::check_admin(array(200)))
            @include('admin/dodatni_fajlovi')
        @elseif($strana=='generisane' AND Admin_model::check_admin(array(257)))
            @include('admin/generisane')
        @elseif($strana=='dobavljac_karakteristike' AND Admin_model::check_admin(array(258)) AND Admin_model::check_admin(array(400)))
            @include('admin/dobavljac_karak')
        @elseif($strana=='proizvodjac' AND Admin_model::check_admin(array(3200,3400)))
            @include('admin/proizvodjac')
        @elseif($strana=='mesto' AND Admin_model::check_admin(array(1800)))
            @include('admin/mesto')
        @elseif($strana=='tip_artikla' AND Admin_model::check_admin(array(2400)))
            @include('admin/tipovi')
        @elseif($strana=='jedinica_mere' AND Admin_model::check_admin(array(2400)))
            @include('admin/jedinice_mere')
        @elseif($strana=='poreske_stope' AND Admin_model::check_admin(array(2400)))
            @include('admin/poreske_stope')
        @elseif($strana=='stanje_artikla' AND Admin_model::check_admin(array(2400)))
            @include('admin/stanje_artikla')
        @elseif($strana=='status_narudzbine' AND Admin_model::check_admin(array(2400)))
            @include('admin/status_narudzbine')
        @elseif($strana=='kurirska_sluzba' AND Admin_model::check_admin(array(2400)))
            @include('admin/kurirska_sluzba')
        @elseif($strana=='konfigurator' AND Admin_model::check_admin(array(9500)))
            @include('admin/konfigurator')
        @elseif($strana=='upload_image' AND Admin_model::check_admin(array(200)))
            @include('admin/upload_image')
        @elseif($strana=='analitika' AND Admin_model::check_admin(array(9000)))
            @include('admin/analitika')
        @elseif($strana=='narudzbina' AND Admin_model::check_admin(array(600,800)))
            @include('admin/narudzbina')
        @elseif($strana=='narudzbina_search' AND Admin_model::check_admin(array(600,800)))
            @include('admin/narudzbina_search')
        @elseif($strana=='narudzbina_stavka' AND Admin_model::check_admin(array(600,800)))
            @include('admin/narudzbina_stavka')  
        @elseif($strana=='vesti' AND Admin_model::check_admin(array(6600)))
            @include('admin/vesti')
        @elseif($strana=='footer_setting' AND Admin_model::check_admin(array(6600)))
            @include('admin/footer_setting')
        @elseif($strana=='vest' AND Admin_model::check_admin(array(6600)))
            @include('admin/vest_single')
        @elseif($strana=='kupci_partneri' AND Admin_model::check_admin(array(1600,1800,2000)))
            @include('admin/kupci_partneri')
        @elseif($strana=='kupci' AND Admin_model::check_admin(array(1800)))
            @include('admin/kupci')
        @elseif($strana=='partneri' AND Admin_model::check_admin(array(1600,2000)))
            @include('admin/partneri')
        @elseif($strana=='kupac' AND Admin_model::check_admin(array(1800)))
            @include('admin/kupac')
        @elseif($strana=='partner' AND Admin_model::check_admin(array(1600,2000)))
            @include('admin/partner')
        @elseif($strana=='administratori' AND Admin_model::check_admin(array(5600,5800)))
            @include('admin/administratori')
        @elseif($strana=='grupa_modula' AND Admin_model::check_admin(array(6000)))
            @include('admin/grupa_modula')
        @elseif($strana=='crm')
            @include('admin/crm_zadaci')
        @elseif($strana=='komentari')
            @include('admin/komentari')
        @elseif($strana=='komentar')
            @include('admin/komentar')
        @elseif($strana=='osobine')
            @include('admin/osobine')
        @elseif($strana=='product_osobine')
            @include('admin/product_osobine')
        @elseif($strana=='nacin_placanja')
            @include('admin/nacin_placanja')
        @elseif($strana=='nacin_isporuke')
            @include('admin/nacin_isporuke')
        @elseif($strana=='troskovi_isporuke')
            @include('admin/troskovi_isporuke')
        @elseif($strana=='kurs')
            @include('admin/kurs')
        @elseif($strana=='file_upload')
            @include('admin/file_upload')
        @elseif($strana=='import_partner_proizvodjac')
            @include('admin/import_partner_proizvodjac')
        @endif
    </div> <!-- end of flex-wrapper -->

    <!--  articles criteria -->   
    <?php if(isset($criteria)){ ?><script> var criteria = new Array("{{ $criteria['grupa_pr_id'] ? $criteria['grupa_pr_id'] : 0 }}", "{{ isset($criteria['proizvodjac']) ? $criteria['proizvodjac'] : 0 }}", 
    "{{ isset($criteria['dobavljac']) ? $criteria['dobavljac'] : 0 }}", "{{ isset($criteria['tip']) ? $criteria['tip'] : 0 }}", "{{ isset($criteria['karakteristika']) ? $criteria['karakteristika'] : 0 }}", "{{ isset($criteria['magacin']) ? $criteria['magacin'] : 0 }}", "{{ isset($criteria['exporti']) ? $criteria['exporti'] : 0 }}", "{{ isset($criteria['filteri']) ? $criteria['filteri'] : 'nn-nn-nn-nn-nn-nn-nn-nn-nn-nn-nn-nn-nn-nn' }}", "{{ isset($criteria['flag']) ? $criteria['flag'] : 0 }}", "{{ isset($criteria['search']) ? $criteria['search'] : 0 }}", "{{ isset($criteria['nabavna']) ? $criteria['nabavna'] : 'nn-nn' }}"); </script><?php }else{ ?><script> var criteria; </script><?php } ?> 

    <!-- import criteria -->
     <?php if(isset($criteriaImport)){ ?><script> var criteriaImport = new Array("{{ $criteriaImport['grupa_pr_id'] ? $criteriaImport['grupa_pr_id'] : 0 }}", "{{ isset($criteriaImport['proizvodjac']) ? $criteriaImport['proizvodjac'] : 0 }}", 
     "{{ isset($criteriaImport['dobavljac']) ? $criteriaImport['dobavljac'] : 0 }}", "{{ isset($criteriaImport['filteri']) ? $criteriaImport['filteri'] : 'nn-nn-nn-nn-nn-nn-nn-nn-nn-nn-nn-nn' }}", "{{ isset($criteriaImport['search']) ? $criteriaImport['search'] : 0 }}", "{{ isset($criteriaImport['nabavna']) ? $criteriaImport['nabavna'] : 'nn-nn' }}");</script><?php }else{ ?><script> var criteriaImport; </script><?php } ?>
    <script> var all_ids = {{ isset($all_ids) ? $all_ids : 'null' }}; var fast_insert_option = '{{ AdminOptions:: gnrl_options(3030) }}'; </script>


    <input type="hidden" id="base_url" value="{{AdminOptions::base_url()}}" />
    <input type="hidden" id="live_base_url" value="{{AdminOptions::live_base_url()}}" />
    <script src="{{ AdminOptions::base_url()}}js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script src="{{ AdminOptions::base_url()}}js/jquery-ui.min.js"></script>
    <script src="{{ AdminOptions::base_url()}}js/foundation.min.js" type="text/javascript"></script>
    <script src="{{ AdminOptions::base_url()}}js/admin/admin.js" type="text/javascript"></script>
    <script src="{{ AdminOptions::base_url()}}js/admin/admin_funkcije.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>
   
    @if(in_array($strana, array('stranice','vest','opis','karakteristike','product_short','grupe','baneri-slajderi','footer')))
        <script type="text/javascript" src="{{ AdminOptions::base_url()}}js/tinymce2/tinymce.min.js"></script>
        <script type="text/javascript">
            tinymce.init({
                selector: ".special-textareas",
                height : "800px",
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table contextmenu paste",
                    "autoresize"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
            });
        </script>
    @endif

    @if($strana=='product' OR $strana=='product_short' OR $strana=='generisane' OR $strana=='dobavljac_karakteristike' OR $strana=='vezani_artikli' OR $strana=='upload_image' OR $strana=='dodatni_fajlovi')
        <script src="{{ AdminOptions::base_url()}}js/admin/admin_product.js" type="text/javascript"></script>
    @endif
    @if($strana=='baneri-slajderi') 
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    @elseif($strana=='footer') 
    <script src="{{ AdminOptions::base_url()}}js/admin/admin_footer.js" type="text/javascript"></script>
    @elseif($strana=='artikli')
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/jstree.min.js"></script>
        <script src="{{ AdminOptions::base_url()}}js/admin/admin_articles.js" type="text/javascript"></script>
    @elseif($strana=='grupe') 
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/jstree.min.js"></script>
        <script src="{{ AdminOptions::base_url()}}js/admin/admin_grupe.js" type="text/javascript"></script>
    @elseif($strana=='product_akcija') 
        <script src="{{ AdminOptions::base_url()}}js/admin/admin_artikal_akcija.js" type="text/javascript"></script>
    @elseif($strana=='slike') 
        <script src="{{ AdminOptions::base_url()}}js/admin/admin_slike.js" type="text/javascript"></script>
    @elseif($strana=='web_import') 
        <script src="{{ AdminOptions::base_url()}}js/admin/admin_import.js" type="text/javascript"></script>
    @elseif($strana=='proizvodjac' OR $strana=='tip_artikla' OR $strana=='jedinica_mere' OR $strana=='poreske_stope' OR $strana=='stanje_artikla' OR $strana=='status_narudzbine' OR $strana=='kurirska_sluzba' OR $strana=='kurirska_sluzba' OR $strana=='konfigurator' OR $strana=='mesto')
        <script src="{{ AdminOptions::base_url()}}js/admin/admin_support.js" type="text/javascript"></script> 
    @elseif($strana=='pocetna' OR $strana=='narudzbina') 
        <script src="{{ AdminOptions::base_url()}}js/admin/admin_narudzbine.js" type="text/javascript"></script>
    @elseif($strana=='grupa_modula' OR $strana=='administratori') 
        <script src="{{ AdminOptions::base_url()}}js/admin/admin_permission.js" type="text/javascript"></script>
    @elseif($strana=='analitika') 
        <script src="{{ AdminOptions::base_url()}}js/admin/admin_analitika.js" type="text/javascript"></script>    
    @elseif($strana=='osobine') 
        <script src="{{ AdminOptions::base_url()}}js/admin/admin_osobine.js" type="text/javascript"></script> 
    @elseif($strana=='nacin_placanja') 
        <script src="{{ AdminOptions::base_url()}}js/admin/admin_nacin_placanja.js" type="text/javascript"></script> 
    @elseif($strana=='nacin_isporuke') 
        <script src="{{ AdminOptions::base_url()}}js/admin/admin_nacin_isporuke.js" type="text/javascript"></script>     
    @elseif($strana=='kupci' OR $strana=='kupac') 
        <script src="{{ AdminOptions::base_url()}}js/admin/admin_kupci.js" type="text/javascript"></script>     
    @elseif($strana=='partneri') 
        <script src="{{ AdminOptions::base_url()}}js/admin/admin_partneri.js" type="text/javascript"></script>
    @elseif($strana=='kurs')
        <script src="{{ AdminOptions::base_url()}}js/admin/admin_kurs.js" type="text/javascript"></script>
    @elseif($strana=='narudzbina_stavka')
        <script src="{{ AdminOptions::base_url()}}js/admin/admin_narudzbina_stavka.js" type="text/javascript"></script>
    @elseif($strana=='kontakt' OR $strana=='partner')
        <script src="{{ AdminOptions::base_url()}}js/admin/admin_kontakt.js" type="text/javascript"></script>
    @elseif($strana=='import_partner_proizvodjac' OR $strana=='import_podaci_grupa')
        <script src="{{ AdminOptions::base_url()}}js/admin/admin_import_data.js" type="text/javascript"></script>
    @elseif($strana=='vesti' OR $strana=='vest')
        <script src="{{ AdminOptions::base_url()}}js/admin/admin_vesti.js" type="text/javascript"></script>
    @endif
    
    <section class="popup more-popup">
		<div class="popup-wrapper">
			<section class="popup-inner">
				<span class="popup-close">X</span>
				<h3>Detalji porudžbine</h3>
			    <section class="popup-inner2"></section>
			</section>
		</div>
	</section>

    <section class="popup info-confirm-popup info-popup">
        <div class="popup-wrapper">
    	    <section class="popup-inner">
    	    </section>
    	</div>
    </section>

</body>
</html>