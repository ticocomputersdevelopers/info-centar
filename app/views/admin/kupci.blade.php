<div id="main-content" class="kupci-page">
@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
@endif
	<div class="row"> 
		<div class="columns medium-12 no-padd">
			@if(in_array(AdminOptions::web_options(130),array(0,1)))
			@include('admin/partials/sifarnik-tabs')
			@endif
		</div>
	</div>
	<div class="row"> 
		<div class="columns medium-12"> 
			<a class="btn btn-create btn-small" href="{{AdminOptions::base_url()}}admin/kupci_partneri/kupci/0">Novi kupac</a> 
		</div>
	</div>
	<div class="row">
		<div class="columns medium-9">
			<div class="">
				<div class="row collapse">
					<div class="columns medium-6 large-5">
						<a href="{{AdminOptions::base_url()}}admin/kupci_partneri/kupci" class="JS-svi btn btn-primary btn-xm {{ ($vrsta_kupca == 'n' AND $status_registracije == 'n') ? 'filter_active' : '' }}">Svi kupci</a>
						<a href="{{AdminOptions::base_url()}}admin/kupci_partneri/kupci/0/{{$status_registracije}}/{{$search}}" class="JS-svi btn btn-primary btn-xm {{ $vrsta_kupca == '0' ? 'filter_active' : '' }}">Fizička lica</a>
						<a href="{{AdminOptions::base_url()}}admin/kupci_partneri/kupci/1/{{$status_registracije}}/{{$search}}" class="JS-svi btn btn-primary btn-xm {{ $vrsta_kupca == '1' ? 'filter_active' : '' }}">Pravna lica</a>
						<a href="{{AdminOptions::base_url()}}admin/kupci_partneri/kupci/{{$vrsta_kupca}}/1/{{$search}}" class="JS-svi btn btn-primary btn-xm {{ $status_registracije == '1' ? 'filter_active' : '' }}">Registrovani</a>
						<a href="{{AdminOptions::base_url()}}admin/kupci_partneri/kupci/{{$vrsta_kupca}}/0/{{$search}}" class="JS-svi btn btn-primary btn-xm {{ $status_registracije == '0' ? 'filter_active' : '' }}">Neregistrovani</a>
					</div>
					<div class="columns medium-6 large-7">
						<form method="POST" action="{{AdminOptions::base_url()}}admin/kupci_partneri/kupci/{{$vrsta_kupca}}/{{$status_registracije}}" class="m-input-and-button">
							<input type="text" name="search" value="{{urldecode($search)}}" placeholder="Pretraga..." class="m-input-and-button__input">
							<input class="btn btn-primary btn-small kupci-btn" type="submit" value="Pretraga" class="m-input-and-button__btn">
							<a class="btn btn-danger btn-small" href="{{AdminOptions::base_url()}}admin/kupci_partneri/kupci">Poništi</a>
						</form>
					</div>
				</div> <!-- end of .row -->
			</div> <!-- end of .flet-box -->
		</div>
		<div class="columns medium-3">
			<div class="text-right">
				<a class="JSExportXML btn btn-primary btn-small" href="#">Export</a>
				<a class="btn btn-primary btn-small" href="{{AdminOptions::base_url()}}admin/kupci_partneri/export_newslette">Newsletter</a>
			</div>
		</div>

<div id="JSchooseXml" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	<h4 class="modal-title-h4">Export</h4>
		<div class="column medium-12 large-12">
			<h3 class="text-center">Izaberite željene kolone:</h3>
				<div class="row">
					@if($vrsta_kupca == "1")
						<input type="checkbox" data-name="naziv" class="JSKolonaExport" data-status="naziv">Naziv
						<input type="checkbox" data-name="pib" class="JSKolonaExport" data-status="pib">Pib
					@elseif($vrsta_kupca == "0")
						<input type="checkbox" data-name="ime" class="JSKolonaExport" data-status="ime">Ime
						<input type="checkbox" data-name="prezime" class="JSKolonaExport" data-status="prezime">Prezime
					@else
						<input type="checkbox" data-name="naziv" class="JSKolonaExport" data-status="naziv">Naziv
						<input type="checkbox" data-name="pib" class="JSKolonaExport" data-status="pib">Pib
						<input type="checkbox" data-name="ime" class="JSKolonaExport" data-status="ime">Ime
						<input type="checkbox" data-name="prezime" class="JSKolonaExport" data-status="prezime">Prezime						
					@endif
					<input type="checkbox" data-name="adresa" class="JSKolonaExport" data-status="adresa">Adresa
					<input type="checkbox" data-name="mesto" class="JSKolonaExport" data-status="mesto">Mesto
					<input type="checkbox" data-name="email" class="JSKolonaExport" data-status="email">Email
					<input type="checkbox" data-name="telefon" class="JSKolonaExport" data-status="telefon">Telefon
				</div>
				<button data-link="{{AdminOptions::base_url()}}admin/kupci_partneri/export_kupac/xml/{{$vrsta_kupca}}/{{$status_registracije}}/null/{{urlencode($search)}}" class="btn btn-primary m-input-and-button__btn JSExportKupci" >Export XML</button>

				<button data-link="{{AdminOptions::base_url()}}admin/kupci_partneri/export_kupac/xls/{{$vrsta_kupca}}/{{$status_registracije}}/null/{{urlencode($search)}}" class="btn btn-primary m-input-and-button__btn JSExportKupci" >Export XLS</button>
		</div> 
</div>

	</div>
	<div class="row">
		<div class="column large-12">
			<div class="flat-box">
				<table>
					<tr>
						<th>Naziv</th>
						<th>Adresa</th>
						<th>Mesto</th>
						<th>Email</th>
						<th>Telefon</th>
						<td></td>
					</tr>
					@foreach($web_kupac as $row)
						<tr>
							@if($row->flag_vrsta_kupca == 0)
							<td>{{ $row->ime }} {{ $row->prezime }}</td>
							@else
							<td>{{ $row->naziv }}</td>
							@endif
							<td>{{ $row->adresa }}</td>
							<td>{{ $row->mesto }}</td>
							<td>{{ $row->email }}</td>
							<td>{{ $row->telefon }}</td>
							<td class="table-col-icons">
								<a class="edit icon" href="{{AdminOptions::base_url()}}admin/kupci_partneri/kupci/{{ $row->web_kupac_id }}"><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Izmeni</a>
								<a class="remove icon JSbtn-delete" data-link="{{AdminOptions::base_url()}}admin/kupci_partneri/kupci/{{ $row->web_kupac_id }}/0/delete"><i class="fa fa-trash" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Obriši</a>
								@if($row->flag_vrsta_kupca == 1)
								<a class="save-as-partner icon" href="{{AdminOptions::base_url()}}admin/kupci_partneri/kupci/{{ $row->web_kupac_id }}/snimi_kao_partner"><i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Snimi kao partnera</a>
								@endif
							</td>
						</tr>
					@endforeach
				</table>
				{{ $web_kupac->links() }}
			</div>
		</div>
	</div>
	<input type="hidden" id="JSCheckConfirm" data-id="{{ Session::has('confirm_kupac_id')?Session::get('confirm_kupac_id'):0 }}">

</div>