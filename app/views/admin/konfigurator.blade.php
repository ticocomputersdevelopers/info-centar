@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
@endif
<section class="" id="main-content">
	@include('admin/partials/tabs')

	<div class="row">
		<section class="small-12 medium-12 large-3 columns">
			<div class="flat-box">
				<h3 class="title-med">Izaberi Konfigurator</h3>
				<select class="JSeditSupport">
					<option value="{{ AdminOptions::base_url() }}admin/konfigurator">Dodaj novi</option>
					@foreach($konfiguratori as $row)
						<option value="{{ AdminOptions::base_url() }}admin/konfigurator/{{ $row->konfigurator_id }}"{{ $row->konfigurator_id == $konfigurator_id ? 'selected' : '' }}>{{ $row->naziv }}</option>
					@endforeach
				</select>
			</div>
		</section>
		<section class="small-12 medium-12 large-5 columns ">
			<div class="flat-box">
				<h1 class="title-med">{{ $title }}</h1>

				<form method="POST" action="{{ AdminOptions::base_url() }}admin/konfigurator-edit" enctype="multipart/form-data">
					<div class="row">
						<input type="hidden" name="konfigurator_id" value="{{ $konfigurator_id }}"> 
						<div class="columns medium-6 field-group{{ $errors->first('naziv') ? ' error' : '' }}">
							<label for="">Naziv konfiguratora</label>
							<input type="text" name="naziv" value="{{ htmlentities(Input::old('naziv') ? Input::old('naziv') : $naziv) }}" autofocus="autofocus"> 
						</div>
						<div class="columns medium-6 field-group">
							<label for="">Aktivan</label>
							<select name="dozvoljen">
								{{ AdminSupport::selectCheck($dozvoljen) }}
							</select>
						</div>
					</div>
					<div class="row">
						<div class="btn-container center">
							<button type="submit" class="btn btn-primary save-it-btn">Sačuvaj</button>
							@if($konfigurator_id != null)
								<button class="btn btn-danger JSbtn-delete" data-link="{{ AdminOptions::base_url() }}admin/konfigurator-delete/{{ $konfigurator_id }}">Obriši</button>
							@endif
						</div>
					</div>
				</form>
				 
			</div>
		</section>
		@if($konfigurator_id != null)
		<section class="small-12 medium-12 large-4 columns ">
			<div class="flat-box clearfix">
				<h1 class="title-med">Grupe</h1>
				
				<table id="JSTabelaKonf" class="karakteristike-tabela" data-id="{{ $konfigurator_id }}" role="grid">
					@foreach($konfigurator_grupe as $row)
						<tr class="inline-list">
							<td><input type="text" class="JSRbr ordered-number" value="{{ $row->rbr }}"></td>
							<td>			
								<select class="JSgrupa_pr_id">
									{{AdminSupport::selectGroups($row->grupa_pr_id)}}
								</select>
							</td>
							<td column="1">
								<button class="JSSaveKonf name-ul__li__save button-option tooltipz" aria-label="Sačuvaj" data-id="{{ $row->grupa_pr_id }}"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
							</td>
							<td column="1">
								<button class="JSDeleteKonf name-ul__li__remove button-option tooltipz" aria-label="Obriši" data-id="{{ $row->grupa_pr_id }}"><i class="fa fa-times" aria-hidden="true"></i></button>
							</td>
						</tr>
					@endforeach
						<tr class="inline-list">
							<td class="JSRbr" data-id="new" style="width:60px"></td>
							<td>			
								<select class="JSgrupa_pr_id">
									{{AdminSupport::selectGroups()}}
								</select>
							</td>
							<td column="1">
								<button class="JSSaveKonf name-ul__li__save button-option tooltipz" aria-label="Sačuvaj" data-id="0"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
							</td>
							<td column="1">
							</td>
						</tr>
				</table>
			</div>
		</section>
		@endif
	</div>
  <!-- </form> -->
</section>