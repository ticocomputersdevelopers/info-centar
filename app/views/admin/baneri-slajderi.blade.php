<section id="main-content" class="banners">
@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
@endif
	<section class="medium-12 large-3 columns">
		<div class="flat-box">
			
			<!-- BANNER LIST -->
			<h3 class="text-center h3-margin">Baneri</h3>
			<ul class="banner-list JSListaBaner" id="banner-sortable" data-table="2">
				<li id="0" class="new-banner new-elem">
					<a href="{{AdminOptions::base_url()}}admin/baneri-slajdovi/0/1">Novi baner</a>
					<div class="icon"><i class="fa fa-plus" aria-hidden="true"></i></div>
				</li>
				@foreach($banners as $banner)
				<li id="{{$banner->baneri_id}}" class="{{ ( (AdminSupport::trajanje_banera($banner->baneri_id) == 'NULL' ) && ( AdminSupport::trajanje_banera($banner->baneri_id) <= 1))  ? 'deadline' : '' }} ui-state-default @if($banner->baneri_id == $id) active @endif" id="{{$banner->baneri_id}}">
					<a href="{{AdminOptions::base_url()}}admin/baneri-slajdovi/{{$banner->baneri_id}}/1">{{$banner->naziv}}</a>
					<a class="icon tooltipz JSbtn-delete" aria-label="Obriši" data-link="{{AdminOptions::base_url()}}admin/baneri-delete/{{$banner->baneri_id}}"><i class="fa fa-times" aria-hidden="true"></i></a>
				</li>
				@endforeach
			</ul>
			
			<!-- SLIDE LIST -->		
			<h3 class="text-center h3-margin">Slajderi</h3>
			<ul class="banner-list JSListaSlider" id="slide-sortable" data-table="2">
				<li id="0" class="new-slide new-elem">
					<a href="{{AdminOptions::base_url()}}admin/baneri-slajdovi/0/2">Novi slajd
					</a>
					<div class="icon"><i class="fa fa-plus" aria-hidden="true"></i></div>
				</li>
				@foreach($sliders as $slide)
					<li id="{{$slide->baneri_id}}" class="{{ ( (AdminSupport::trajanje_banera($slide->baneri_id) == 'NULL' ) && ( AdminSupport::trajanje_banera($slide->baneri_id) <= 1))  ? 'deadline' : '' }} ui-state-default @if($slide->baneri_id == $id) active @endif" id="{{$slide->baneri_id}}">
					<a href="{{AdminOptions::base_url()}}admin/baneri-slajdovi/{{$slide->baneri_id}}/2">{{$slide->naziv}}</a>
					<a class="icon tooltipz JSbtn-delete" aria-label="Obriši" data-link="{{AdminOptions::base_url()}}admin/baneri-delete/{{$slide->baneri_id}}"><i class="fa fa-times" aria-hidden="true"></i></a></li>
				@endforeach
			</ul>

			<h3 class="text-center h3-margin">Popup baner</h3>
			<ul class="banner-list JSListaBaner" id="banner-sortable" data-table="2">
				<li id="0" class="new-banner new-elem">
					<a href="{{AdminOptions::base_url()}}admin/baneri-slajdovi/0/9">Novi popup baner</a>
					<div class="icon"><i class="fa fa-plus" aria-hidden="true"></i></div>
				</li>
				@foreach($popup as $popbanner)
				<li id="{{$popbanner->baneri_id}}" class="{{ ( (AdminSupport::trajanje_banera($popbanner->baneri_id) == 'NULL' ) && ( AdminSupport::trajanje_banera($popbanner->baneri_id) <= 1))  ? 'deadline' : '' }} ui-state-default @if($popbanner->baneri_id == $id) active @endif" id="{{$popbanner->baneri_id}}">
					<a href="{{AdminOptions::base_url()}}admin/baneri-slajdovi/{{$popbanner->baneri_id}}/9">{{$popbanner->naziv}}</a>
					<a class="icon tooltipz JSbtn-delete" aria-label="Obriši" data-link="{{AdminOptions::base_url()}}admin/baneri-delete/{{$popbanner->baneri_id}}"><i class="fa fa-times" aria-hidden="true"></i></a>
				</li>
				@endforeach
			</ul>
	
		</div> <!-- end of .flat-box -->
		
		@if(in_array(DB::table('prodavnica_stil')->where('izabrana',1)->first()->prodavnica_tema_id, array(5,6,10)))
		<!-- BACKGROUND IMAGE -->
		<div class="flat-box">
			<h3 class="text-center h3-margin">Pozadinska slika</h3>
			<ul class="">
				<form action="{{AdminOptions::base_url()}}admin/bg-img-edit" method="POST" enctype="multipart/form-data">
					<input type="file" name="bgImg" value="Izaberi sliku">
					
					@if(AdminSupport::getBGimg() == null)
					<img src="{{AdminOptions::base_url()}}/images/no-image.jpg" alt="" class="bg-img">
					@else
					<img src="{{AdminOptions::base_url()}}{{ AdminSupport::getBGimg() }}" alt="" class="bg-img">
					@endif
					<h3 class="text-center h3-margin">Preporučene dimenzije slike: 1920x1080</h3>
					<br>
					<!-- <label for="aktivna">
						<input type="checkbox" name="aktivna" value=""> Aktivna slika
					</label> -->
					<div> 
						<label class="text-center">Link leve strane</label>
						<input id="link" value="{{AdminSupport::getlink()}}" type="text" name="link">
						<label class="text-center">Link desne strane</label>
						<input id="link2" value="{{AdminSupport::getlink2()}}" type="text" name="link2">
					</div><br>
					<input type="hidden" value="3" name="flag">
					<div class="text-center btn-container">
						<button type="submit" class="btn btn-primary save-it-btn">Sačuvaj</button>
						<a class="btn btn-danger" href="{{AdminOptions::base_url()}}admin/bg-img-delete">Obriši</a>
					</div>
				</form>
			</ul>
		</div>
	 	@endif
	</section> <!-- end of .medium-3 .columns -->
	
	<!-- BANNER EDIT -->
	<section class="page-edit medium-12 large-9 columns">
		<div class="flat-box banner-edit-top"> 
		<form action="{{AdminOptions::base_url()}}admin/banner-edit" name="banner_upload" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="logo-right">
			@if($type == 1)
			<h3 class="text-center h3-margin">Izmeni baner</h3>
			@elseif($type == 2)
			<h3 class="text-center h3-margin">Izmeni slajder</h3>
			@elseif($type == 9)
			<h3 class="text-center h3-margin">Izmeni popup baner</h3>
			@endif
			<!-- BANNER EDIT TOP -->
			<section class="row">
				<div class="medium-5 columns">
					@if($type == 1)
					<label>Naziv banera</label>
					@elseif($type == 2)
					<label>Naziv slajdera</label>
					@elseif($type == 9)
					<label>Naziv popup banera</label>
					@endif
					<div class="relative"> 
						<input id="naziv" class="input-bann" value="{{ Input::old('naziv') ? Input::old('naziv') : trim($item->naziv) }}" type="text" name="naziv" />
						<span class="banner-uploads has-tooltip"><input type="file" name="img"><span>Dodaj sliku</span></span>
					</div>
					<div class="error">{{ $errors->first('naziv') ? $errors->first('naziv') : '' }}</div> 
					<div class="error">{{ $errors->first('img') ? ($errors->first('img') == 'Niste popunili polje.' ? 'Niste izabrali sliku.' : $errors->first('img')) : '' }}</div>
				</div>
				
				<div class="medium-6 columns">
					@if($type == 1)
					<label>Link banera</label>
					@elseif($type == 2)
					<label>Link slajdera</label>
					@elseif($type == 9)
					<label>Link popup banera</label>
					@endif
					<input id="link" value="{{ Input::old('link') ? Input::old('link') : trim($item->link) }}" type="text" name="link">
					<div class="error">{{ $errors->first('link') ? $errors->first('link') : '' }}</div>
				</div>
			</section>
 			<div class="row"> 
				<div class="medium-2 columns"> 
					<label>Aktivan</label>
				  	<select name="aktivan">
						<option value="1">DA</option>
						<option value="0" {{ (Input::old('aktivan') == '0') ? 'selected' : (($item->aktivan == 0) ? 'selected' : '') }}>NE</option>
					</select>
				</div>
				@if($type == 1)
				<div class="column medium-3 akcija-column">
					<label>Baner postavljen:</label>
					<input class="akcija-input" id="datum_od" name="datum_od" autocomplete="off" type="text" value="{{ Input::old('datum_od') ? Input::old('datum_od') : $datum_od }}">
					<span id="datum_od_delete"><i class="fa fa-close tooltipz-left" aria-label="Ukloni"></i></span>
				</div>
				@elseif($type == 2)
				<div class="column medium-3 akcija-column">
					<label>Slajder postavljen:</label>
					<input class="akcija-input" id="datum_od" name="datum_od" autocomplete="off" type="text" value="{{ Input::old('datum_od') ? Input::old('datum_od') : $datum_od }}">
					<span id="datum_od_delete"><i class="fa fa-close tooltipz-left" aria-label="Ukloni"></i></span>
				</div>
				@elseif($type == 9)
				<div class="column medium-3 akcija-column">
					<label>Pop-Up baner postavljen:</label>
					<input class="akcija-input" id="datum_od" name="datum_od" autocomplete="off" type="text" value="{{ Input::old('datum_od') ? Input::old('datum_od') : $datum_od }}">
					<span id="datum_od_delete"><i class="fa fa-close tooltipz-left" aria-label="Ukloni"></i></span>
				</div>
				@endif
				<div class="column medium-3 akcija-column">
					<label>Traje Do:</label>
					<input class="akcija-input" id="datum_do" name="datum_do" autocomplete="off" type="text" value="{{ Input::old('datum_do') ? Input::old('datum_do') : $datum_do }}">
					<span id="datum_do_delete"><i class="fa fa-close tooltipz-left" aria-label="Ukloni"></i></span>
				</div>
				@if(!empty($datum_do) && $type == 1) 
				<div class="column medium-3 baners-date">
					<label>&nbsp;</label>
					<span>Baner ističe za <input type="text" class="time-limit center" value="{{ AdminSupport::trajanje_banera($id) }}" disabled> dana.</span>
				</div> 
				@elseif(!empty($datum_do) && $type ==2)
				<div class="column medium-3 baners-date">
					<label>&nbsp;</label>
					<span>Slajder ističe za <input type="text" class="time-limit center" value="{{ AdminSupport::trajanje_banera($id) }}" disabled> dana.</span>
				</div>
				@elseif(!empty($datum_do) && $type ==9)
				<div class="column medium-3 baners-date">
					<label>&nbsp;</label>
					<span>PopUp baner ističe za <input type="text" class="time-limit center" value="{{ AdminSupport::trajanje_banera($id) }}" disabled> dana.</span>
				</div>
				@endif
		 	</div>
			 
			
			<!-- BANNER PREVIEW -->
			<section class="banner-preview-area">
				<img class="banner-preview" src="{{AdminOptions::base_url()}}{{$item->img}}" alt="{{$item->naziv}}" />
			</section>
			
			@if(count($jezici) > 1)
			<div class="row"> 
				<div class="languages">
					<ul>	
					@foreach($jezici as $jezik)
						<li><a class="{{ $jezik_id == $jezik->jezik_id ? 'active' : '' }} btn-small btn btn-secondary" href="{{AdminOptions::base_url()}}admin/baneri-slajdovi/{{ $id }}/{{ $type }}/{{ $jezik->jezik_id }}">{{ $jezik->naziv }}</a></li>
					@endforeach
					</ul>
				</div>
			</div>
			@endif 
			@if($type != 9)
			<section class="banner-edit-top row">
				<div class="medium-6 columns">
					<label>Naslov</label>
					<input id="naslov" value="{{ Input::old('naslov') ? Input::old('naslov') : trim($lang_data->naslov) }}" type="text" name="naslov" />
					<div class="error">{{ $errors->first('naslov') ? $errors->first('naslov') : '' }}</div>
				</div>
				
				<div class="medium-6 columns">
					<label>Nadnaslov</label>
					<input id="nadnaslov" value="{{ Input::old('nadnaslov') ? Input::old('nadnaslov') : trim($lang_data->nadnaslov) }}" type="text" name="nadnaslov">
					<div class="error">{{ $errors->first('nadnaslov') ? $errors->first('nadnaslov') : '' }}</div>
				</div>

				<div class="medium-6 columns">
					<label>Naslov dugmeta</label>
					<input id="naslov_dugme" value="{{ Input::old('naslov_dugme') ? Input::old('naslov_dugme') : trim($lang_data->naslov_dugme) }}" type="text" name="naslov_dugme">
					<div class="error">{{ $errors->first('naslov_dugme') ? $errors->first('naslov_dugme') : '' }}</div>
				</div>
				@if($type == 1)
				<input class="ordered-number" type="hidden" name="redni_broj" value="{{ !is_null(Input::old('redni_broj')) ? Input::old('redni_broj') : $redni_broj }}" >
				@endif
				<div class="medium-12 columns">
					<label>Sadržaj</label>
					<textarea class="special-textareas" name="sadrzaj">{{ Input::old('sadrzaj') ? Input::old('sadrzaj') : trim($lang_data->sadrzaj) }}</textarea>
					<div class="error">{{ $errors->first('sadrzaj') ? $errors->first('sadrzaj') : '' }}</div>
				</div>
			</section>
			@endif

			<!-- BANNER DISPLAY PAGES -->			
			<div class="banner-display-pages">
				<input type="hidden" name="baneri_id" value="{{$item->baneri_id}}" />
				<input type="hidden" name="tip_prikaza" value="{{$type}}" />
				<input type="hidden" name="jezik_id" value="{{$jezik_id}}" />
				<div class="btn-container center">
					<button class="btn btn-primary save-it-btn">Sačuvaj</button>
				</div>
			</div>
		</form>
		</div>

	 	<!-- ====================== -->
		<div class="text-center">
			<a href="#" class="video-manual" data-reveal-id="banners-manual">Uputstvo <i class="fa fa-film"></i></a>
		   	<div id="banners-manual" class="reveal-modal medium" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
				<div class="video-manual-container"> 
				<p><span class="video-manual-title">Baneri i slajder</span></p> 
				<iframe src="https://player.vimeo.com/video/271254080" width="840" height="426" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
				<a class="close-reveal-modal" aria-label="Close">&#215;</a>
			</div>
		 </div>
		</div>
		<!-- ====================== -->
	</section>
</section>

 