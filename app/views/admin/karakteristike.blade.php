<div id="main-content" class="">
@include('admin/partials/product-tabs')
	@include('admin/partials/karak-tabs')
	
	<div class="row">
		<div class="column large-6 medium-6 large-centered">
			<form method="POST" action="{{AdminOptions::base_url()}}admin/karakteristike_edit">
				<input type="hidden" id="roba_id" name="roba_id" value="{{ $roba_id ? $roba_id : 0 }}">
				  <div class="flat-box"> 
				     <div class="row">
						<div class="column medium-12"> 
					     	<textarea class="special-textareas" name="web_karakteristike">{{ $web_karakteristike }}</textarea>
							<div class="btn-container center">
								<a class="setting-button btn btn-primary" href="#" id="JSsablon">Unesi šablon</a>
								<button type="submit" class="setting-button btn btn-primary save-it-btn">Sačuvaj</button>
							</div>
						</div>
					</div>
				</div> <!-- end of .row -->
			</form>
			<div class="image-upload-area clearfix">
				<form method="post" enctype="multipart/form-data" action="{{AdminOptions::base_url()}}admin/image_upload_opis" >
					<label>Dodajte sliku u opisu:</label>

					<input type="file" id="img" name="img" class="file-input">
					<button class="file-upload btn btn-secondary" type="submit">Učitaj</button>
				</form>
				<div class="images_upload has-tooltip" title='Prevucite sliku na željenu poziciju'>
					{{AdminSupport::upload_directory_opis()}}
				</div>
			</div>	
		</div> <!-- end of .coluimn -->
	 </div> <!-- end of .row -->
 </div>