@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
@endif
<section class="comments-page" id="main-content">

		@include('admin/partials/tabs')

	<div class="row">
		<section class="small-12 medium-10 large-10 large-centered medium-centered columns">
			<div class="flat-box">
				<h3 class="title-med">Komentari</h3>
				<div class="row"> 
					<div class="column medium-12 table-scroll"> 
						<table>
							<thead>
								<tr>
									<th>Ime</th>
									<th>Komentar</th>
									<th>Artikal</th>
									<th>Status</th>
									<th>Detaljnije</th>
								</tr>
							</thead>
							<tbody>
								@foreach($comments as $row)
								<tr>
									<td>{{ $row->ime_osobe }}</td>
									<td>{{ $row->pitanje }}</td>
									<td>
										<a href="{{AdminArticles::article_link($row->roba_id)}}" target="_blank">
										{{AdminArticles::find($row->roba_id, 'naziv')}}
										</a>
									</td>
									<td>
										@if($row->komentar_odobren == 1)
										<span class="radius secondary label">Odobreno</span>
										@else
										<span class="label success radius">Novo</span>	
										@endif
									</td>
									<td><a href="{{AdminOptions::base_url()}}admin/komentari/{{$row->web_b2c_komentar_id}}">Detaljnije</a></td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</section>
			<!-- ====================== -->
			<div class="text-center">
				<a href="#" class="video-manual" data-reveal-id="comments-manual">Uputstvo <i class="fa fa-film"></i></a>
			   	<div id="comments-manual" class="reveal-modal medium" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
					<div class="video-manual-container"> 
					<p><span class="video-manual-title">Komentari</span></p>
					<iframe src="https://player.vimeo.com/video/271252889" width="840" height="426" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
					</div>
					<a class="close-reveal-modal" aria-label="Close">&#215;</a>
				</div>
			</div>
		<!-- ====================== -->
 	</div>
</section>