@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
@endif
<div id="main-content" class="images-edit">
	@include('admin/partials/product-tabs')
 

	<div class="row firma-slike-row"> 
		<div id="JSUploadImages" class="big-image medium-8 columns medium-centered no-padd"></div>
    </div>
	<div class="row slike-form-row">
	 	<form class="column medium-8 medium-centered no-padd" method="POST" action="{{AdminOptions::base_url()}}admin/slika-edit" enctype="multipart/form-data">
			<input type="hidden" name="roba_id" value="{{ $roba_id ? $roba_id : 0 }}"> 
			<div class="btn-container row">
			    <h2 class="title-big center">Slike</h2>
				<div class="columns medium-5 no-padd"> 
					<p>Dodaj nove slike</p>
					<input type="file" name="slika[]" multiple> 
				</div>
				<div class="columns medium-7"> 
					<input type="submit" class="btn btn-primary save-it-btn" value="SAČUVAJ SLIKE">
				</div>
			</div>
			<div class="error">{{ Session::has('error') ? Session::get('error') : '' }}</div>
		</form>
	</div> 

	<div class="row firma-slike-row">
		<div class="column medium-8 small-12 medium-centered no-padd">
			<div class="row imges-heading"> 
				<div class="columns medium-6"> 
					<input type="checkbox" id="JSSlikaSelected"> 
					<label>Označi sve</label>
				</div>
				<div class="columns medium-6 text-right"> 
					<label>Izaberi akciju</label>
					<select id="JSSlikaExecute">
						<option class="JSSlikaExecuteOption" value=""></option>
						<option class="JSSlikaExecuteOption" value="active">Prikaži u prodavnici</option>
						<option class="JSSlikaExecuteOption" value="unactive">Skloni iz prodavnice</option>
						<option class="JSSlikaExecuteOption" value="delete">Obriši</option>
					</select>
				</div>
			</div>
		</div>
		<div class="no-float"> 
		@foreach(AdminArticles::getSlike($roba_id) as $row)
		<div class="column medium-8 small-12 col-slika medium-centered {{ $row->flag_prikazi == 1 ? 'active' : '' }}">
			<div class="radio-button-field row"> 
			    <input type="checkbox" class="JSSlikaSelected" data-id="{{ $row->web_slika_id }}">
				 
				<div class="columns medium-4">
					<img src="{{ AdminOptions::base_url().$row->putanja }}">
				</div> 
				<div class="columns medium-5 text-right">
					<textarea class="JSSlikaAlt" data-id="{{ $row->web_slika_id }}" placeholder="Naziv slike (alt tag)">{{$row->alt}}</textarea>
					<button class="JSSlikaAltSave invisible-btn" data-id="{{ $row->web_slika_id }}" hidden="hidden">SAČUVAJ</button> 
				</div>  
			    <div class="main-image">  
					<input type="radio" class="JSAkcija" data-robaid="{{ $roba_id }}" data-id="{{ $row->web_slika_id }}" @if($row->akcija) checked @endif>
					<label class="no-margin">Glavna slika </label> 
				</div>
			</div>
		</div>
		@endforeach 
		</div>
	</div>
