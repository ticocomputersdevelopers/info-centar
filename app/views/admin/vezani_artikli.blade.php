<div id="main-content">
	@include('admin/partials/product-tabs')
	<div class="row">
		<div class="columns medium-12 large-6 large-centered medium-centered">
			<div class="articles flat-box">
				<input class="" autocomplete="off" data-timer="" type="text" id="articles" data-id="{{ $roba_id }}" placeholder="Unesite id ili ime artikla ili prozvođača ili krajnju grupu" />
				<div id="search_content"></div>
			</div>
		</div>
	</div>
	<div class="row"> 
		<div class="columns medium-12 large-6 large-centered medium-centered">
			<div class="flat-box">
				<table style="margin: 0;">
					@if(count($vezani_artikli)>0)
					<thead>
						<tr>
							<th>
								ROBA ID
							</th>
							<th>
								GRUPA 
							</th>
							<th>
								NAZIV
							</th>
							<th>
								FLAG CENE
							</th>
							<th>
							</th>
						</tr>
					</thead>
					@foreach($vezani_artikli as $row)
					<tr>
						<td>
							&nbsp;{{ $row->vezani_roba_id }}
						</td>
						<td>
							{{ AdminArticles::findGrupe(AdminArticles::find($row->vezani_roba_id,'grupa_pr_id'),'grupa') }}
						</td>
						<td>
							{{ AdminArticles::find($row->vezani_roba_id,'naziv') }}
						</td>
						<td>
							<input type="checkbox" class="JSFlagCenaVezan" data-store='{"roba_id":"{{ $roba_id }}", "vezani_roba_id":"{{ $row->vezani_roba_id }}"}' {{ $row->flag_cena == 1 ? 'checked' : '' }}>
						</td>
						<td>
							<button class="JSDeleteVezan name-ul__li__remove button-option tooltipz" aria-label="Obriši" data-store='{"roba_id":"{{ $roba_id }}", "vezani_roba_id":"{{ $row->vezani_roba_id }}"}'>&times;</button>
						</td>
					</tr>
					@endforeach
					@endif
				</table>
			</div>
		</div>
    </div>
	</div> <!-- end of .row -->
 