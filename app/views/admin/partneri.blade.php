<div id="main-content" class="kupci-page">
@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
@endif
	<div class="row"> 
		<div class="column medium-12 no-padd"> 
			@if(in_array(AdminOptions::web_options(130),array(0,1)))
			@include('admin/partials/sifarnik-tabs') <!-- end of m-tabs -->
			@endif
		</div>
	</div>

	<div class="row"> 
		<div class="column medium-12"> 
			<a class="btn btn-create btn-small" href="{{AdminOptions::base_url()}}admin/kupci_partneri/partner/0">Novi partner</a> 
		</div>
	</div>

	<div class="row">
		<div class="columns medium-6"> 
			<form method="POST" action="{{AdminOptions::base_url()}}admin/kupci_partneri/partneri" class="m-input-and-button">
				<input type="text" name="search" value="{{ urldecode($search) }}" placeholder="Pretraga..." class="m-input-and-button__input">
				<input class="btn btn-primary m-input-and-button__btn" value="Pretraga" type="submit">
				<a class="btn btn-danger btn-small" href="{{AdminOptions::base_url()}}admin/kupci_partneri/partneri">Poništi</a>
			</form> 
		</div>
		<div class="columns medium-6">
			<div class="text-right">
				<a class="btn btn-primary btn-small JSExport" href="#">Export</a>
			</div>
		</div>
	</div>
	<div id="JSchoose" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
		<a class="close-reveal-modal" aria-label="Close">&#215;</a>
		<h4 class="modal-title-h4">Export</h4>
			<div class="row text-center">
				<h3 class="text-center">Izaberite željene kolone:</h3> 
				<div class="columns medium-2"> 
					<input type="checkbox" data-name="naziv" class="JSKolonaExport" data-status="naziv">Naziv 	
				</div>
				<div class="columns medium-2"> 
					<input type="checkbox" data-name="adresa" class="JSKolonaExport" data-status="adresa">Adresa 
				</div>
				<div class="columns medium-2"> 
					<input type="checkbox" data-name="mesto" class="JSKolonaExport" data-status="mesto">Mesto 
				</div>
				<div class="columns medium-2"> 
					<input type="checkbox" data-name="email" class="JSKolonaExport" data-status="email">Email
				</div> 
				<div class="columns medium-2"> 
					<input type="checkbox" data-name="telefon" class="JSKolonaExport" data-status="telefon">Telefon
				</div> 
			</div> 
			<div class="text-center"><br>
				<button data-link="{{AdminOptions::base_url()}}admin/kupci_partneri/export_partner/xml/null/{{urlencode($search)}}" class="btn btn-primary m-input-and-button__btn JSExportPartneri" >Export XML</button>
				<button data-link="{{AdminOptions::base_url()}}admin/kupci_partneri/export_partner/xls/null/{{urlencode($search)}}" class="btn btn-primary m-input-and-button__btn JSExportPartneri" >Export XLS</button>
			</div>
	  </div>
 	<div class="row"> 
		<div class="columns medium-12 large-12">
			<table>
				<tr>
					<th>Naziv</th>
					<th>Adresa</th>
					<th>Mesto</th>
					<th>Email</th>
					<th>Telefon</th>
					<td></td>
				</tr>
				@foreach($partneri as $row)
				<tr>
					<td>{{ $row->naziv }}</td>
					<td>{{ $row->adresa }}</td>
					<td>{{ $row->mesto }}</td>
					<td>{{ $row->mail }}</td>
					<td>{{ $row->telefon }}</td>
					<td class="table-col-icons">
						<a class="edit icon" href="{{AdminOptions::base_url()}}admin/kupci_partneri/partner/{{ $row->partner_id }}"><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Izmeni</a>
						<a class="remove icon JSbtn-delete" data-link="{{AdminOptions::base_url()}}admin/kupci_partneri/partneri/{{ $row->partner_id }}/0/delete"><i class="fa fa-trash" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Obriši</a>
					</td>
				</tr>
				@endforeach

			</table>
			{{ $partneri->links() }}
		</div>
	</div>
	<input type="hidden" id="JSCheckPartnerConfirm" data-id="{{ Session::has('confirm_partner_id')?Session::get('confirm_partner_id'):0 }}">
</div>