<div id="main-content" class="article-edit">
@if(Session::has('message_success'))
	<script>
		alertify.success('{{ Session::get('message_success') }}');
	</script>
@endif
	<?php
	$old = count(Input::old()) ? 1 : 0;
	// echo '<pre>';
	// var_dump(Input::old()); die();
	?>
	@if($roba_id)
		<div class="row m-subnav">
			@if(Admin_model::check_admin(array(200)))
		    @include('admin/partials/product-tabs')
		    @endif
		</div>
	@endif
	<div class="row">
		@if(Session::has('message'))
		<div class="column medium-12 erase-div"> 
			<div class="text-red">Artikal ima istoriju!</div> 
			<a class="btn btn-danger" href="{{ AdminOptions::base_url() }}admin/product-delete/{{ $roba_id }}/1">Ipak obriši!</a>
		</div>
		@endif
		@if(Session::has('limit_message'))
		<div class="column medium-12 erase-div"> 
			<div class="text-red">Unošenje novih artikala nije dozvoljeno!</div>
		</div>
		@endif
		<form method="POST" action="{{AdminOptions::base_url()}}admin/product-edit" id="JSArticleForm">
			<input type="hidden" name="roba_id" value="{{ $roba_id ? $roba_id : 0 }}">
			<input type="hidden" name="clone_id" value="{{ $clone_id ? $clone_id : 0 }}">
			<div class="clearfix floating-field-group">
<!-- 				<div class="field-group">
					<label>Knjigovosvena šifra robe</label>
					@if($roba_id!=0)
					<div>{{ $roba_id }}</div>
					@else
					<div></div>
					@endif
				</div> -->
			<div class="columns large-8 medium-8 cut-padding">
				<div class="row article-edit-box">
					<div class="columns large-3 medium-3">
						<div class="field-group">
							<label>Vrsta</label>
							@if($old)
								@if(Input::old('flag_usluga'))
								<input type="radio" name="flag_usluga" value="0"><label for="">Roba</label>
								<input type="radio" name="flag_usluga" value="1" checked><label for="">Usluga</label>
								@else
								<input type="radio" name="flag_usluga" value="0" checked><label for="">Roba</label>
								<input type="radio" name="flag_usluga" value="1"><label for="">Usluga</label>					
								@endif
							@else
								@if($flag_usluga)
								<input type="radio" name="flag_usluga" value="0"><label for="">Roba</label>
								<input type="radio" name="flag_usluga" value="1" checked><label for="">Usluga</label>
								@else
								<input type="radio" name="flag_usluga" value="0" checked><label for="">Roba</label>
								<input type="radio" name="flag_usluga" value="1"><label for="">Usluga</label>					
								@endif
							@endif
						</div>
					</div>
					<div class="columns large-1 medium-1 no-padd">
						<div class="field-group">
							<label>SKU</label>
							<input type="text" name="sku" value="{{ htmlentities($old ? Input::old('sku') : $sku) }}">
							<div class="error">{{ $errors->first('sku') }}</div>
						</div>
					</div>

					<div class="columns large-8 medium-8 no-padd">
						<div class="columns large-9 medium-9 cut-padding">
							<div class="field-group karak-vrsta">
									<label>Vrsta karakteristika</label>
									@if($old)
										@if(Input::old('web_flag_karakteristike')==0)
										<input type="radio" name="web_flag_karakteristike" value="0" checked><label>HTML</label>
										<input type="radio" name="web_flag_karakteristike" value="1"><label>Generisane</label>
										@if(Admin_model::check_admin(array(400)))
										<input type="radio" name="web_flag_karakteristike" value="2"><label>Od dobavljača</label>
										@endif
										@elseif(Input::old('web_flag_karakteristike')==1)
										<input type="radio" name="web_flag_karakteristike" value="0"><label>HTML</label>
										<input type="radio" name="web_flag_karakteristike" value="1" checked><label>Generisane</label>
										@if(Admin_model::check_admin(array(400)))
										<input type="radio" name="web_flag_karakteristike" value="2"><label>Od dobavljača</label>
										@endif
										@else
										<input type="radio" name="web_flag_karakteristike" value="0"><label>HTML</label>
										<input type="radio" name="web_flag_karakteristike" value="1"><label>Generisane</label>
										@if(Admin_model::check_admin(array(400)))
										<input type="radio" name="web_flag_karakteristike" value="2" checked><label>Od dobavljača</label>
										@endif
										@endif
									@else
										@if($web_flag_karakteristike==0)
										<input type="radio" name="web_flag_karakteristike" value="0" checked><label>HTML</label>
										<input type="radio" name="web_flag_karakteristike" value="1"><label>Generisane</label>
										@if(Admin_model::check_admin(array(400)))
										<input type="radio" name="web_flag_karakteristike" value="2"><label>Od dobavljača</label>
										@endif
										@elseif($web_flag_karakteristike==1)
										<input type="radio" name="web_flag_karakteristike" value="0"><label>HTML</label>
										<input type="radio" name="web_flag_karakteristike" value="1" checked><label>Generisane</label>
										@if(Admin_model::check_admin(array(400)))
										<input type="radio" name="web_flag_karakteristike" value="2"><label>Od dobavljača</label>
										@endif
										@else
										<input type="radio" name="web_flag_karakteristike" value="0"><label>HTML</label>
										<input type="radio" name="web_flag_karakteristike" value="1"><label>Generisane</label>
										@if(Admin_model::check_admin(array(400)))
										<input type="radio" name="web_flag_karakteristike" value="2" checked><label>Od dobavljača</label>
										@endif
										@endif
									@endif
							</div>
						</div>
						<div class="columns large-3 medium-3">
							<div class="field-group">
								<label>Osobine</label>
								<select name="osobine">
									@if($old)
										@if(Input::old('osobine'))
										<option value="1" selected>DA</option>
										<option value="0" >NE</option>
										@else
										<option value="1" >DA</option>
										<option value="0" selected>NE</option>
										@endif
									@else
										@if($osobine)
										<option value="1" selected>DA</option>
										<option value="0" >NE</option>
										@else
										<option value="1" >DA</option>
										<option value="0" selected>NE</option>
										@endif
									@endif
								</select>
							</div>
						</div>
					</div>

					<div class="columns large-12 medium-12">
						<div class="naziv-web-big field-group">
							<label>Naziv na web-u</label>
							<input type="text" name="naziv_web" value="{{ htmlentities($old ? Input::old('naziv_web') : $naziv_web) }}">
							<div class="error">{{ $errors->first('naziv_web') }}</div>
						</div>
					</div>
				</div>

			 <div class="article-edit-box">
			    <div class="row"> 
					<div class="columns large-6 medium-6">
						<div class="field-group">
							<label>Grupa</label>
							<div class="custom-select-arrow"> 
								<input type="text" name="grupa_pr_grupa" value="{{Input::old('grupa_pr_grupa') ? Input::old('grupa_pr_grupa') : $grupa_pr_grupa }}" autocomplete="off">
							</div>
							<div class="short-group-container"> 
								{{ AdminSupport::listGroups() }}
							</div>
							<div class="error">{{ $errors->first('grupa_pr_grupa') }}</div>
						</div>
					</div>
					<div class="columns large-6 medium-6">
						<div class="field-group">
							<label>Proizvođač</label>
							<div class="custom-select-arrow"> 
								<input type="text" name="proizvodjac" value="{{Input::old('proizvodjac') ? Input::old('proizvodjac') : $proizvodjac }}" autocomplete="off">
							</div>
							<div class="short-group-container">
								<ul id="JSListaProizvodjaca" hidden="hidden">
									@foreach(AdminSupport::getProizvodjaci() as $row)
									<li class="JSListaProizvodjac" data-proizvodjac="{{ $row->naziv }}">{{ $row->naziv }}</li>
									@endforeach
								</ul>
							</div>
							<div class="error">{{ $errors->first('proizvodjac') }}</div>
						</div>
					</div>
				</div>

				<div class="row"> 
					<div class="columns large-6 medium-6">
						<div class="field-group">
							<label>Dodatne grupe</label>
							<select id="roba_grupe" name="roba_grupe[]" multiple="multiple">
								@if($old)
									{{ AdminSupport::select2Groups(Input::old('roba_grupe') ? Input::old('roba_grupe') : array()) }}
								@else
									{{ AdminSupport::select2Groups($roba_grupe) }}
								@endif
							</select>
						</div>
					</div>

					<div class="columns large-6 medium-6">
						<div class="field-group">
							<label>Poreska grupa</label>
							<div class="columns medium-4 large-4 no-padd"> 
								<select name="tarifna_grupa_id" id="tarifna_grupa_id">
									@foreach(AdminSupport::getPoreskeGrupe() as $row)
									@if($old)
										@if(Input::old('tarifna_grupa_id') == $row->tarifna_grupa_id))
										<option value="{{ $row->tarifna_grupa_id }}" selected>{{ $row->naziv }}</option>
										@else
										<option value="{{ $row->tarifna_grupa_id }}">{{ $row->naziv }}</option>
										@endif
									@else
										@if($tarifna_grupa_id === $row->tarifna_grupa_id)
										<option value="{{ $row->tarifna_grupa_id }}" selected>{{ $row->naziv }}</option>
										@else
										<option value="{{ $row->tarifna_grupa_id }}">{{ $row->naziv }}</option>
										@endif
									@endif
									@endforeach
								</select>
							</div>
							<div class="columns medium-8 large-8 no-padd"> &nbsp;
								<input type="checkbox" id="JSIWebCena"> Promeni i web cenu
							</div>
						</div>
					</div>
			   </div>

				<div class="row"> 
					<div class="columns large-6 medium-6">
						<div class="field-group">
							<label>Jedinica mere</label>
							<div class="custom-select-arrow"> 
								<input type="text" name="jedinica_mere" value="{{Input::old('jedinica_mere') ? Input::old('jedinica_mere') : $jedinica_mere }}" autocomplete="off">
							</div>
							<div class="short-group-container">
								<ul id="JSListaJedinicaMera" hidden="hidden">
									@foreach(AdminSupport::getJediniceMere() as $row)
									<li class="JSListaJedinicaMere" data-jedinica_mere="{{ $row->naziv }}">{{ $row->naziv }}</li>
									@endforeach
								</ul>
							</div>
							<div class="error">{{ $errors->first('jedinica_mere') }}</div>
						</div>
					</div>

					<div class="columns large-6 medium-6">
						<div class="field-group">
							<label>Težina (gr.)</label>
							<input type="text" name="tezinski_faktor" value="{{ htmlentities($old ? Input::old('tezinski_faktor') : $tezinski_faktor) }}">
							<div class="error">{{ $errors->first('tezinski_faktor') }}</div>
						</div>
					</div>
				</div>
				<div class="row"> 
					<div class="columns large-6 medium-6">
						<div class="field-group">
							<label>Pakovanje </label>
							<select name="flag_ambalaza">
							@if($old)
								@if(Input::old('flag_ambalaza'))
								<option value="1" selected>DA</option>
								<option value="0" >NE</option>
								@else
								<option value="1" >DA</option>
								<option value="0" selected>NE</option>
								@endif
							@else
								@if($flag_ambalaza)
								<option value="1" selected>DA</option>
								<option value="0" >NE</option>
								@else
								<option value="1" >DA</option>
								<option value="0" selected>NE</option>
								@endif
							@endif
							</select>
						</div>
					</div>
					<div class="columns large-6 medium-6">
						<div class="field-group {{ $errors->first('ambalaza') ? 'error' : '' }}">
							<label>Broj artikala u pakovanju </label>
							<input type="text" name="ambalaza" value="{{ htmlentities($old ? Input::old('ambalaza') : $ambalaza) }}">
							<div class="error">{{ $errors->first('ambalaza') }}</div>
						</div>
					</div>
				</div>

			</div>

				<div class="row article-edit-box">
					<div class="columns large-6 medium-6">
						<div class="field-group">
							<label>Model</label>
							<input type="text" name="model" value="{{ htmlentities($old ? Input::old('model') : $model) }}">
							<div class="error">{{ $errors->first('model') }}</div>
						</div>
					</div>
					<div class="columns large-6 medium-6">
						<div class="field-group">
							<label>Garancija (u mesecima)</label>
							<input type="text" name="garancija" value="{{ htmlentities($old ? Input::old('garancija') : $garancija) }}">
							<div class="error">{{ $errors->first('garancija') }}</div>
						</div>
					</div>
					<div class="columns large-6 medium-6">
						<div class="field-group">
							<label>Barkod</label>
							<input type="text" name="barkod" value="{{ $old ? Input::old('barkod') : $barkod }}">
							<div class="error">{{ $errors->first('barkod') }}</div>
						</div>
					</div>
					<div class="columns large-6 medium-6">	
						<div class="field-group">
							<label>Stanje artikla</label>
							<div class="custom-select-arrow"> 
								<input type="text" name="roba_flag_cene" value="{{Input::old('roba_flag_cene') ? Input::old('roba_flag_cene') : $roba_flag_cene }}" autocomplete="off">
							</div>
							<div class="short-group-container">
								<ul id="JSListaFlagCene" hidden="hidden">
									@foreach(AdminSupport::getFlagCene() as $row)
									<li class="JSListaFlagCena" data-roba_flag_cene="{{ $row->naziv }}">{{ $row->naziv }}</li>
									@endforeach
								</ul>
							</div>
							<div class="error">{{ $errors->first('roba_flag_cene') }}</div>
						</div>
					</div>
				</div>
				
				<div class="row article-edit-box">
					<div class="columns large-6 medium-6">	
						<div class="field-group">
							<label>Magacin</label>
							<select name="orgj_id" id="orgj_id">
								@foreach(DB::table('orgj')->where('b2c',1)->orderBy('orgj_id','asc')->get() as $row)
								@if($old)
									@if(Input::old('orgj_id') == $row->orgj_id)
									<option value="{{ $row->orgj_id }}" selected>{{ $row->naziv }} ({{ $row->orgj_id }} {{ DB::table('imenik_magacin')->where('orgj_id',$row->orgj_id)->pluck('izabrani') == 1 ? 'def.' : '' }})</option>
									@else
									<option value="{{ $row->orgj_id }}">{{ $row->naziv }} ({{ $row->orgj_id }} {{ DB::table('imenik_magacin')->where('orgj_id',$row->orgj_id)->pluck('izabrani') == 1 ? 'def.' : '' }})</option>
									@endif
								@else
									@if($orgj_id == $row->orgj_id)
									<option value="{{ $row->orgj_id }}" selected>{{ $row->naziv }} ({{ $row->orgj_id }} {{ DB::table('imenik_magacin')->where('orgj_id',$row->orgj_id)->pluck('izabrani') == 1 ? 'def.' : '' }})</option>
									@else
									<option value="{{ $row->orgj_id }}">{{ $row->naziv }} ({{ $row->orgj_id }} {{ DB::table('imenik_magacin')->where('orgj_id',$row->orgj_id)->pluck('izabrani') == 1 ? 'def.' : '' }})</option>
									@endif
								@endif
								@endforeach
							</select>
						</div>
					</div>
					<div class="columns large-6 medium-6">	
						<div class="field-group">
							<label>Količina</label>
							<input type="text" name="kolicina" value="{{ $old ? Input::old('kolicina') : $kolicina }}">
							<div class="error">{{ $errors->first('kolicina') }}</div>
						</div>
					</div>
				</div>
				<div class="row article-edit-box">
					<div class="columns large-12 medium-12">
						<div class="btn-container center">
							@if(isset($clone_id))
							<input type="checkbox" name="slike_clone" checked>Kloniraj slike
							@endif
							<button type="submit" id="JSArticleSubmit" class="btn btn-primary save-it-btn">Sačuvaj</button> 
							 
							@if($roba_id != 0)
							<a class="btn btn-danger" href="{{ AdminOptions::base_url() }}admin/product-delete/{{ $roba_id }}">Obriši</a>
								@if(Admin_model::check_admin(array(2400)))
								<a class="btn-secondary btn" href="{{AdminOptions::base_url()}}admin/product/0/{{ $roba_id }}">Kloniraj artikal</a>
								@endif
							@endif

							<a href="#" id="JSArtikalRefresh" class="btn btn-primary">Poništi izmene</a>
							@if(AdminOptions::gnrl_options(3029))
							<a href="{{AdminOptions::base_url()}}admin/product-short/{{$roba_id}}" class="btn btn-primary">Brza izmena</a>
							@endif
						</div>
					</div>
				</div>

			</div><!--  end of column 8 -->
			
			<div class="columns large-4 medium-4 cut-padding">
					@if(Admin_model::check_admin(array(400)))
					<div class="row article-edit-box">
						<div class="columns large-12 medium-12">
						
							<div class="field-group">
								<label>Šifra kod dobavljača</label>
								<input type="text" name="sifra_d" value="{{ $old ? Input::old('sifra_d') : $sifra_d }}">
								<div class="error">{{ $errors->first('sifra_d') }}</div>
							</div>
						</div>
						<div class="columns large-12 medium-12">
							<div class="field-group">
								<label>Dobavljač</label>
								<select name="dobavljac_id">
									<option value=""></option>
									@foreach(AdminSupport::getDobavljaci() as $row)
									@if($old)
										@if(Input::old('dobavljac_id') != '' && Input::old('dobavljac_id') == $row->partner_id)
										<option value="{{ $row->partner_id }}" selected>{{ $row->naziv }}</option>
										@else
										<option value="{{ $row->partner_id }}">{{ $row->naziv }}</option>
										@endif
									@else
										@if($dobavljac_id === $row->partner_id)
										<option value="{{ $row->partner_id }}" selected>{{ $row->naziv }}</option>
										@else
										<option value="{{ $row->partner_id }}">{{ $row->naziv }}</option>
										@endif
									@endif
									@endforeach
								</select>
							</div>
						</div>
					</div>
					@endif

					<div class="row article-edit-box">
						<div class="columns large-4 medium-4">
							
							<div class="field-group">
								<label>Na Web-u </label>
								<select name="flag_prikazi_u_cenovniku">
								@if($old)
									@if(Input::old('flag_prikazi_u_cenovniku'))
									<option value="1" selected>DA</option>
									<option value="0" >NE</option>
									@else
									<option value="1" >DA</option>
									<option value="0" selected>NE</option>
									@endif
								@else
									@if($flag_prikazi_u_cenovniku)
									<option value="1" selected>DA</option>
									<option value="0" >NE</option>
									@else
									<option value="1" >DA</option>
									<option value="0" selected>NE</option>
									@endif
								@endif
								</select>
							</div>
							
						</div>
						<div class="columns large-4 medium-4">
							<div class="field-group">
								<label>Aktivan</label>
								<select name="flag_aktivan">
									@if($old)
										@if(Input::old('flag_aktivan'))
										<option value="1" selected>DA</option>
										<option value="0" >NE</option>
										@else
										<option value="1" >DA</option>
										<option value="0" selected>NE</option>
										@endif
									@else
										@if($flag_aktivan)
										<option value="1" selected>DA</option>
										<option value="0" >NE</option>
										@else
										<option value="1" >DA</option>
										<option value="0" selected>NE</option>
										@endif
									@endif
								</select>
							</div>
						</div>
						<div class="columns large-4 medium-4">
						<div class="field-group">
							<label>Zaključan</label>
							<select name="flag_zakljucan">
							@if($old)
								@if(Input::old('flag_zakljucan')=="true")
								<option value="true" selected>DA</option>
								<option value="false" >NE</option>
								@else
								<option value="true" >DA</option>
								<option value="false" selected>NE</option>
								@endif
							@else
								@if($flag_zakljucan)
								<option value="true" selected>DA</option>
								<option value="false" >NE</option>
								@else
								<option value="true" >DA</option>
								<option value="false" selected>NE</option>
								@endif
							@endif
							</select>
						</div>	
						</div>
						<div class="columns large-12 medium-12">
							<div class="field-group">
								<label>Tip artikla</label>
								<div class="custom-select-arrow"> 
									<input type="text" name="tip_artikla" value="{{Input::old('tip_artikla') ? Input::old('tip_artikla') : $tip_artikla }}" autocomplete="off">
								</div>
								<div class="short-group-container"> 
									<ul id="JSListaTipaArtikla" hidden="hidden">
										<li class="JSListaTipArtikla" data-tip_artikla="">Bez tipa</li>
										@foreach(AdminSupport::getTipovi() as $row)
										<li class="JSListaTipArtikla" data-tip_artikla="{{ $row->naziv }}">{{ $row->naziv }}</li>
										@endforeach
									</ul>
								</div>
								<div class="error">{{ $errors->first('tip_artikla') }}</div>
							</div>
						</div>

					</div>
					

				 <div class="article-edit-box">
				 	<div class="row">
						<div class="columns large-5 medium-5">
							<div class="field-group columns large-6 medium-6">
								<label>Nabavna cena</label>
								<input type="text" name="racunska_cena_nc" id="racunska_cena_nc" value="{{ $old ? Input::old('racunska_cena_nc') : $racunska_cena_nc }}" {{ AdminArticles::vrsteCena('NC') ? '' : 'disabled' }}>
								<div class="error">{{ $errors->first('racunska_cena_nc') }}</div>
							</div>
						</div>
			   		</div>
				 	<div class="row">	
						<div class="columns large-5 medium-5">
							<div class="field-group">
								<label>Maloprodajna cena</label>
								<input type="text" name="mpcena" value="{{ $old ? Input::old('mpcena') : $mpcena }}" {{ AdminArticles::vrsteCena('MP') ? '' : 'disabled' }}>
								<div class="error">{{ $errors->first('mpcena') }}</div>
							</div>
						</div>
						<div class="columns large-5 medium-5">
							<div class="field-group columns">
								<label>MP marža (%)</label>
								<input type="text" name="mp_marza" id="mp_marza" value="{{ $old ? Input::old('mp_marza') : $mp_marza }}" {{ AdminArticles::vrsteCena('MP') ? '' : 'disabled' }}>
								<div class="error">{{ $errors->first('mp_marza') }}</div>
							</div>
						</div>
					  
						<div class="columns large-2 medium-2 no-padd">
							<div class="field-group columns">
								<label>Primeni</label>
								<input type="checkbox" id="mp_marza_check">
							</div>
						</div>
				    </div>

				   <div class="row"> 
						<div class="columns large-5 medium-5">
							<div class="field-group columns large-5">
								<label>Web cena</label>
								<input type="text" name="web_cena" class="JSCenaChange" value="{{ $old ? Input::old('web_cena') : $web_cena }}" {{ AdminArticles::vrsteCena('WEB') ? '' : 'disabled' }}>
								<div class="error">{{ $errors->first('web_cena') }}</div>
							</div>
						</div>
						<div class="columns large-5 medium-5">
							<div class="field-group columns large-5">
								<label>Web marža (%)</label>
								<input type="text" name="web_marza" id="web_marza" value="{{ $old ? Input::old('web_marza') : $web_marza }}" {{ AdminArticles::vrsteCena('WEB') ? '' : 'disabled' }}>
								<div class="error">{{ $errors->first('web_marza') }}</div>
							</div>
						</div>
						<div class="columns large-2 medium-2 no-padd">
							<div class="field-group columns">
								<label>Primeni</label>
								<input type="checkbox" id="web_marza_check">
							</div>
						</div>
					</div>	
				 </div>
 			</div>  	
		</form>
	</div> <!-- end of .row -->	
</div>


