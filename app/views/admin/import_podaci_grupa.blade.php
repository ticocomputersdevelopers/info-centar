<div id="main-content" class="kupci-page">
    @if(Session::has('message'))
        <script>
            alertify.success('{{ Session::get('message') }}');
        </script>
    @endif
    @if(Session::has('alert'))
        <script>
            alertify.error('{{ Session::get('alert') }}');
        </script>
    @endif
    <div class="row m-subnav">
        <div class="large-2 medium-3 small-12 columns ">
             <a class="m-subnav__link" href="{{AdminOptions::base_url()}}admin/web_import/0/0/0/nn-nn-nn-nn-nn-nn-nn-nn-nn-nn-nn-nn/0/nn-nn" >
                <div class="m-subnav__link__icon">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                </div>
                <div class="m-subnav__link__text">
                    Nazad
                </div>
            </a>
        </div>
    </div><br>

    <div class="row">
        <div class="medium-7 columns">
            <div class="table-groups-import">
                <table>
                    <thead>
                        <tr class="rower">
                            <th>Partner</th>
                            <th>Grupa dobavljača</th>
                            <th>Naša grupa</th>
                            <th class="tooltipz" aria-label="Web marža">WM</th>
                            <th class="tooltipz" aria-label="MP marža">MPM</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="text-right">
                            <td>
                                <a class="btn btn-small btn-primary" href="{{AdminOptions::base_url()}}admin/import_podaci_grupa/{{ $partner_id }}/0">Dodaj novi
                                </a>
                            </td>
                        </tr>
                        @foreach($partner_grupa as $grupa)
                        <tr class="rower">
                            <td>{{ AdminSupport::dobavljac($grupa->partner_id) }}</td>
                            <td>{{ $grupa->grupa}}</td>
                            <td>{{ AdminGroups::find($grupa->grupa_pr_id,'grupa') }}</td>
                            <td>{{ round($grupa->web_marza, 2)}}</td>
                            <td>{{ round($grupa->mp_marza, 2)}}</td>
                            <td>
                                <a class="btn btn-small btn-danger" href="{{AdminOptions::base_url()}}admin/import_podaci_delete/{{ $grupa->partner_id }}/{{ $grupa->partner_grupa_id }}">Obriši</a>
                            </td>
                            <td>
                                <a class="btn btn-small btn-primary" href="{{AdminOptions::base_url()}}admin/import_podaci_grupa/{{ $grupa->partner_id }}/{{ $grupa->partner_grupa_id }}">Izmeni</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="medium-5 columns">
            <div class="flat-box">
                <h1 class="title-med">Import podaci</h1>
                <form method="POST" action="{{AdminOptions::base_url()}}admin/import_podaci_save" >
                    <input type="hidden" name="partner_grupa_id" value="{{$partner_grupa_id}}">
                    <div class="columns medium-12 field-group">
                        <div class="columns medium-12 field-group {{ $errors->first('partner_id') ? ' error' : '' }}">
                            <label>Partneri</label>
                            <select name="partner_id" class="m-input-and-button__input import-select" id="JSPartneriSelect">
                                <option value="0">Svi partneri</option>
								@foreach(AdminImport::dobavljaci_web_import_tabela() as $dobavljac)
                                @if($dobavljac->partner_id == $partner_id)
                                <option value="{{ $dobavljac->partner_id }}" selected>{{ $dobavljac->naziv }}</option>
                                @else
                                <option value="{{ $dobavljac->partner_id }}">{{ $dobavljac->naziv }}</option>
                                @endif
                                @endforeach
                            </select>
                            {{ $errors->first('partner_id') }}
                        </div>
                        <div class="columns medium-12 field-group {{ $errors->first('grupa') ? ' error' : '' }}">
                            <label>Grupa dobavljača</label>
                            <div class="short-group-container">
                                <select name="grupa">
                                    @foreach($grupe_dobavljaca as $row)
                                    <option value="{{ AdminImport::mapped_groups($row->grupa,$row->podgrupa) }}" {{ (Input::old('grupa') ? Input::old('grupa') : $partner_grupa_item->grupa) == AdminImport::mapped_groups($row->grupa,$row->podgrupa) ? 'selected' : '' }}>{{ AdminImport::mapped_groups($row->grupa,$row->podgrupa) }}</option>
                                    @endforeach
                                </select>
                            </div>
                            {{ $errors->first('grupa') }}
                        </div>
                        <div class="columns medium-12 field-group {{ $errors->first('grupa_pr_id') ? ' error' : '' }}">
                            <label>Naša grupa</label>
                            <select name="grupa_pr_id">
                            {{ AdminSupport::selectGroups(Input::old('grupa_pr_id') ? Input::old('grupa_pr_id') : $partner_grupa_item->grupa_pr_id) }}
                            </select>
                            {{ $errors->first('grupa_pr_id') }}
                        </div>
                        <div class="columns medium-6 field-group {{ $errors->first('web_marza') ? ' error' : '' }}">
                            <label>Web marža</label>
                            <input type="text" name="web_marza" value="{{(Input::old('web_marza') ? Input::old('web_marza') : round($partner_grupa_item->web_marza,2))}}">
                            {{ $errors->first('web_marza') }}
                        </div>
                        <div class="columns medium-6 field-group {{ $errors->first('mp_marza') ? ' error' : '' }}">
                            <label>Mp marža</label>
                            <input type="text" name="mp_marza" value="{{(Input::old('mp_marza') ? Input::old('mp_marza') : round($partner_grupa_item->mp_marza,2))}}">
                            {{ $errors->first('mp_marza') }}
                        </div>
                        <div class="columns medium-6 field-group">
                            <label>Mapiraj sve artikle</label>
                            <input type="checkbox" name="mapped_all" {{ !is_null(Input::old('mapped_all')) ? "checked" : "" }}>
                        </div>
                        <div class="columns medium-6 field-group">
                            <label>Mapiraj artikle bez naše grupe</label>
                            <input type="checkbox" name="mapped_news" {{ !is_null(Input::old('mapped_news')) ? "checked" : "" }}>
                        </div>
                    </div>
                    <div class="btn-container center">
                        <button type="submit" class="btn btn-primary save-it-btn">Sačuvaj</button>
                    </div>
                </form>
            </div>
        </div>
      </div>
 </div>

