<div id="main-content" class="kupci-page">
@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
@endif	
	<div class="row m-subnav">
		<div class="large-2 medium-3 small-12 columns ">
			 <a class="m-subnav__link" href="{{AdminOptions::base_url()}}admin/kupci_partneri/kupci" >
				<div class="m-subnav__link__icon">
					<i class="fa fa-arrow-left" aria-hidden="true"></i>
				</div>
				<div class="m-subnav__link__text">
					Nazad
				</div>
			</a>
		</div>
	</div>

	<div class="row">
		<div class="medium-6 medium-centered columns">
			<div class="flat-box">
			
				<form action="{{AdminOptions::base_url()}}admin/kupci_partneri/kupci/{{ $web_kupac_id }}/save" method="post" class="new-customer-form" autocomplete="false">
					<div class="center" @if($web_kupac_id != 0) {{ 'style="display: none;"' }} @endif>
						<div class="toggles center">
							<input type="hidden" name="web_kupac_id" value="{{ $web_kupac_id }}">
							<input type="checkbox" name="flag_vrsta_kupca" id="checkboxPP" class="ios-toggle JSKupacCheckbox" 
							{{ count(Input::old())>0 ? (!is_null(Input::old('flag_vrsta_kupca')) ? 'checked' : '') : ($flag_vrsta_kupca == 1 ? 'checked' : '') }}>
							<label for="checkboxPP" class="switch-label" data-off="Fizicko lice" data-on="Pravno lice"></label>
						</div>
					
					</div>

					<div class="row JSPravnoLice">
						<div class="column medium-6 {{ $errors->first('naziv') ? ' error' : '' }}">
							<label>Naziv</label>
							<input id="naziv" name="naziv" value="{{ Input::old('naziv') ? Input::old('naziv') : $naziv }}" type="text">
							{{ $errors->first('naziv') }}
						</div>

						<div class="column medium-6 {{ $errors->first('pib') ? ' error' : '' }}">
							<label>Pib</label>
							<input id="pib" name="pib" value="{{ Input::old('pib') ? Input::old('pib') : $pib }}" type="text">
							{{ $errors->first('pib') }}
						</div>
					</div> <!-- end of .row -->

					<div class="row JSPrivatnoLice active">
						<div class="column medium-6 {{ $errors->first('ime') ? ' error' : '' }}">
							<label for="">Ime</label>
							<input id="ime" name="ime" value="{{ Input::old('ime') ? Input::old('ime') : $ime }}" type="text">
							{{ $errors->first('ime') }}
						</div>

						<div class="column medium-6 {{ $errors->first('prezime') ? ' error' : '' }}">
							<label for="">Prezime</label>
							<input id="prezime" name="prezime" value="{{ Input::old('prezime') ? Input::old('prezime') : $prezime }}" type="text">
							{{ $errors->first('prezime') }}
						</div>
					</div> <!-- end of .row -->

					<div class="row">
 
					@if(AdminNarudzbine::api_posta(3))
					<div class="column medium-6">
						<label for="without-reg-city">Opština</label>				 
						<select class="form-control" name="opstina" id="JSOpstina" tabindex="6">
						@foreach(AdminNarudzbine::opstine() as $key => $opstina)
							@if((Input::old('opstina') ? Input::old('opstina') : AdminNarudzbine::opstina_id($ulica_id)) == $opstina->narudzbina_opstina_id)
							<option value="{{ $opstina->narudzbina_opstina_id }}" selected>{{ $opstina->naziv }}</option>
							@else
							<option value="{{ $opstina->narudzbina_opstina_id }}">{{ $opstina->naziv }}</option>
							@endif
						@endforeach
						</select>
					</div>
					<div class="column medium-6">
						<label for="without-reg-city">Naselje</label>
						<select class="form-control" name="mesto_id" id="JSMesto">
						@foreach(AdminNarudzbine::mesta(Input::old('opstina') ? Input::old('opstina') : AdminNarudzbine::opstina_id($ulica_id)) as $key => $mesto_obj)
							@if((Input::old('mesto_id') ? Input::old('mesto_id') : AdminNarudzbine::mesto_id($ulica_id)) == $mesto_obj->narudzbina_mesto_id)
							<option value="{{ $mesto_obj->narudzbina_mesto_id }}" selected>{{ $mesto_obj->naziv }}</option>
							@else
							<option value="{{ $mesto_obj->narudzbina_mesto_id }}">{{ $mesto_obj->naziv }}</option>
							@endif
						@endforeach
						</select>
					</div>
					<div class="column medium-6">
						<label for="without-reg-city">Ulica</label>				 
						<select class="form-control" name="ulica_id" id="JSUlica">
						@foreach(AdminNarudzbine::ulice(Input::old('mesto_id') ? Input::old('mesto_id') : AdminNarudzbine::mesto_id($ulica_id)) as $key => $ulica)
							@if((Input::old('ulica') ? Input::old('ulica') : $ulica_id) == $ulica->narudzbina_ulica_id)
							<option value="{{ $ulica->narudzbina_ulica_id }}" selected>{{ $ulica->naziv }}</option>
							@else
							<option value="{{ $ulica->narudzbina_ulica_id }}">{{ $ulica->naziv }}</option>
							@endif
						@endforeach
						</select>
					</div>
					<div class="column medium-6 {{ $errors->first('broj') ? ' error' : '' }}">
						<label for="without-reg-number">Broj</label>
						<input id="without-reg-number" class="form-control" name="broj" type="text" value="{{ htmlentities(Input::old('broj') ? Input::old('broj') : $broj) }}">
						{{ $errors->first('broj') }}
					</div>
					@endif

					<div class="column medium-6 {{ $errors->first('adresa') ? ' error' : '' }}">
						<label for="">Adresa</label>
						<input id="adresa" name="adresa" value="{{ Input::old('adresa') ? Input::old('adresa') : $adresa }}" type="text">
						{{ $errors->first('adresa') }}
					</div>

					<div class="column medium-6 {{ $errors->first('mesto') ? ' error' : '' }}">
				    	<label for=""> Mesto/Grad</label>
						<input type="text" name="mesto" value="{{ Input::old('mesto') ? Input::old('mesto') : $mesto }}">
						{{ $errors->first('mesto') }}
					</div>
			 
					<div class="column medium-6 {{ $errors->first('telefon') ? ' error' : '' }}">
						<label for="">Telefon</label>
						<input id="telefon" name="telefon" value="{{ Input::old('telefon') ? Input::old('telefon') : $telefon }}" type="text">
						{{ $errors->first('telefon') }}
					</div>

					<div class="column medium-6 {{ $errors->first('telefon_mobilni') ? ' error' : '' }}">
						<label>Mobilni</label>
						<input id="mobilni" name="telefon_mobilni" value="{{ Input::old('telefon_mobilni') ? Input::old('telefon_mobilni') : $telefon_mobilni }}" type="text">
						{{ $errors->first('telefon_mobilni') }}
					</div>
			 
					<div class="column medium-6 {{ $errors->first('fax') ? ' error' : '' }}">
						<label >Fax</label>
						<input id="fax" name="fax" value="{{ Input::old('fax') ? Input::old('fax') : $fax }}" type="text">
						{{ $errors->first('fax') }}
					</div>

					<div class="column medium-6 {{ $errors->first('email') ? ' error' : '' }}">
						<label>Email</label>
						<input id="email" autocomplete="off" name="email" value="{{ Input::old('email') ? Input::old('email') : $email }}" type="email">
						{{ $errors->first('email') }} 
					</div>
	 
					<div class="column medium-6 {{ $errors->first('lozinka') ? ' error' : '' }}">
						<label>Lozinka</label>
						<input type="password" id="login-pass1" name="lozinka" value="{{ Input::old('lozinka') ? Input::old('lozinka') : $lozinka }}">
						<input style="display: none" id="login-pass2" type="text" value="{{ Input::old('lozinka') ? Input::old('lozinka') : $lozinka }}">
						<a href="#" id="show-pass">Prikaži lozinku</a><br>
						{{ $errors->first('lozinka') }}
					 </div>
					 <div class="column medium-6"> 
					 	<input type="checkbox" name="flag_prima_poruke" class=""
						{{ count(Input::old())>0 ? (!is_null(Input::old('flag_prima_poruke')) ? 'checked' : '') : ($flag_prima_poruke == 1 ? 'checked' : '') }}
						><label>Newsletter</label> 
						&nbsp;<input type="checkbox" name="status_registracije" class="" {{ count(Input::old())>0 ? (!is_null(Input::old('status_registracije')) ? 'checked' : '') : ($status_registracije == 1 ? 'checked' : '') }}>
						<label>Registrovan</label>  
			 		</div>
				</div> <!-- end of .row -->
			 

					<div class="row"> 
						 <div class="btn-container center">
							<button type="submit" id="submit" class="btn btn-primary save-it-btn">Sačuvaj</button>
							<a href="{{AdminOptions::base_url()}}admin/kupci_partneri/kupci/" class="btn btn-secondary">Otkaži</a>
							@if($web_kupac_id!=0)
							<a class="btn btn-danger JSbtn-delete" data-link="{{AdminOptions::base_url()}}admin/kupci_partneri/kupci/{{ $web_kupac_id }}/0/delete">Obriši</a>
							@endif
						</div>
					</div>
				</form> 
			</div>
		</div>
		<input type="hidden" id="JSCheckConfirm" data-id="{{ Session::has('confirm_kupac_id')?Session::get('confirm_kupac_id'):0 }}">
	</div>

	<div class="row">
		<table>
			<thead>
				<tr>
					<td>NARUDŽBINA</td>
					<td>STATUS</td>
					<td>STAVKE</td>
				</tr>
			</thead>
		@foreach(AdminNarudzbine::narudzbine($web_kupac_id) as $narudzbina)
			<tr>
				<td><a href="{{ AdminOptions::base_url() }}admin/narudzbina/{{ $narudzbina->web_b2c_narudzbina_id }}" target="_blank">{{ $narudzbina->broj_dokumenta }}</a></td>
				<td>{{ AdminNarudzbine::narudzbina_status_active($narudzbina->web_b2c_narudzbina_id) }}</td>
				<td>
					<table class="no-margin fix-width-table">
						<thead>
							<tr>
								<td class="td-1">ROBA_ID</td>
								<td class="td-2">NAZIV</td>
								<td class="td-3">KOLIČINA</td>
								<td class="td-4">CENA ARTIKLA</td>
							</tr>
						</thead>
						<?php $ukupna_cena = 0; ?>
					@foreach(AdminNarudzbine::narudzbina_stavke($narudzbina->web_b2c_narudzbina_id) as $stavka)
						<?php $ukupna_cena += $stavka->jm_cena*$stavka->kolicina; ?>
						<tr>
							<td>{{ $stavka->roba_id }}</td>
							<td>{{ AdminArticles::find($stavka->roba_id, 'naziv_web') }}</td>
							<td>{{ $stavka->kolicina }}</td>
							<td>{{ intval($stavka->jm_cena) }}.00 .din</td>
						</tr>
					@endforeach
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td><b> UKUPNO: {{ $ukupna_cena }}.00 .din </b></td>
						</tr>
					</table>
				</td>
			</tr>
		@endforeach
		</table>
	</div>
</div>