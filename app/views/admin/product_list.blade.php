<!--=========== LOADER =============== -->
<div class="JSads-loader"> 
	<div class="ads-loader-inner"> 
		<div>  
			<span class="loadSpiner"></span>
			<span>Sadržaj se učitava. Molimo Vas sačekajte.</span>
		</div>
	</div>
</div>   


<div class="fixed-cat-menu">
	<i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Kategorije
</div>
<div class="row small-gutter art-row">
	<div class="row bsh flex">  
			<div class="column medium-6 large-8 bw">
			 
				<h3 class="text-center">Filtriranje </h3>
				<div class="columns medium-3 large-3">	
					<table class="filter-table">
						<tr>
							<th>Da</th>
							<th>Ne</th>
							<th></th>
						</tr>
						<tr>
							<td><input class="filter-check" type="checkbox" data-check="11"></td>
							<td><input class="filter-check" type="checkbox" data-check="12"></td>
							<td>Lager (naš)</td>
						</tr>
						<tr>
							<td><input class="filter-check" type="checkbox" data-check="21"></td>
							<td><input class="filter-check" type="checkbox" data-check="22"></td>
							<td>Lager dobavljača</td>
						</tr>
						<tr>
							<td><input class="filter-check" type="checkbox" data-check="13"></td>
							<td><input class="filter-check" type="checkbox" data-check="14"></td>
							<td>Akcija</td>
						</tr>
						<tr>
							<td><input class="filter-check" type="checkbox" data-check="15"></td>
							<td><input class="filter-check" type="checkbox" data-check="16"></td>
							<td>Zaključan</td>
						</tr>
					</table>
				</div>	<!-- end of cols -->
				<div class="column medium-4 large-4">
					<table class="filter-table">
						<tr>
							<th>Da</th>
							<th>Ne</th>
							<th></th>
						</tr>
						<tr>
							<td><input class="filter-check" type="checkbox" data-check="19"></td>
							<td><input class="filter-check" type="checkbox" data-check="20"></td>
							<td>Na Web-u</td>
						</tr>
						@if(AdminOptions::checkB2B())
						<tr>
							<td><input class="filter-check" type="checkbox" data-check="23"></td>
							<td><input class="filter-check" type="checkbox" data-check="24"></td>
							<td>Na B2B-u</td> 
						</tr>
						@endif
						<tr>
							<td><input class="filter-check" type="checkbox" data-check="17"></td>
							<td><input class="filter-check" type="checkbox" data-check="18"></td>
							<td>Aktivan</td>
						</tr>
					</table>

					<div class="nc-options row">
						<div class="columns medium-6">
							<div class="columns medium-6">
								<label for="" class="tooltipz" aria-label="Nabavna cena">NC od</label>
								<input type="text" id="JSCenaOd" class="">
							</div>
							<div class="columns medium-6">
								<label for="" class="tooltipz" aria-label="Nabavna cena">NC do</label>
								<input type="text" id="JSCenaDo" class="">
							</div>
						</div>
						<div class="columns medium-6 large-6 btns-od-do">
							<a href="#" id="JSCenaSearch" class="m-input-and-button__btn btn btn-primary btn-xm btn-radius">
							<i class="fa fa-search" aria-hidden="true"></i>
							</a>
							<a href="#" class="m-input-and-button__btn btn btn-secondary btn-xm tooltipz" aria-label="Poništi">
							<i class="fa fa-times" id="JSCenaClear" aria-hidden="true"></i>
							</a>
						</div>
					</div>

				</div>
					<div class="column medium-5 large-5">	
							<table class="filter-table">
								<tr>
									<th>Da</th>
									<th>Ne</th>
									<th></th>
									<th>Da</th>
									<th>Ne</th>
									<th></th>
								</tr>

								<tr>
									<td><input class="filter-check" type="checkbox" data-check="9"></td>
									<td><input class="filter-check" type="checkbox" data-check="10"></td>
									<td>Slike</td>
									<td><input class="filter-check" type="checkbox" data-check="25"></td>
									<td><input class="filter-check" type="checkbox" data-check="26"></td>
									<td>Težina</td>
								</tr>
								<tr>
									<td><input class="filter-check" type="checkbox" data-check="1"></td>
									<td><input class="filter-check" type="checkbox" data-check="2"></td>
									<td>Opis</td>
									<td><input class="filter-check" type="checkbox" data-check="27"></td>
									<td><input class="filter-check" type="checkbox" data-check="28"></td>
									<td>Tag</td>
								</tr>
								<tr>
									<td><input class="filter-check" type="checkbox" data-check="3"></td>
									<td><input class="filter-check" type="checkbox" data-check="4"></td>
									<td>Karakteristike (HTML)</td>
									<td><input id="JSFlag" class="" type="checkbox" {{ $criteria['flag']==1?'checked':'' }}></td>
									<td></td>
									<td>Flag</td>
									<td></td>
								</tr>
								<tr>
									<td><input class="filter-check" type="checkbox" data-check="5"></td>
									<td><input class="filter-check" type="checkbox" data-check="6"></td>
									<td colspan="4">Karakteristike (generisane)</td>
								</tr>
								<tr>
									<td><input class="filter-check" type="checkbox" data-check="7"></td>
									<td><input class="filter-check" type="checkbox" data-check="8"></td>
									<td colspan="4">Karakteristike (dobavljač)</td>
								</tr>
							</table>
						</div> <!-- end of .column -->
						
			  <!-- end of .column -->		
			</div>
			<div class="column medium-6 large-4 bw action-buttons">	
				<h3 class="text-center">Opcije </h3>

				<div class="row">	 
					<div class="column medium-6">
						<button class="btn btn-secondary btn-xm1 btn-w-icons" id="all" value=""><i class="fa fa-list" aria-hidden="true"></i> Izaberi sve (F3)</button>
					</div>
					<div class="column medium-3">
						<button class="btn btn-secondary btn-xm JSexecute" data-execute='[{"table":"roba", "column":"flag_zakljucan", "val":"false"},{"table":"dobavljac_cenovnik", "column":"flag_zakljucan", "val":"0"}]'><i class="fa fa-unlock-alt" aria-hidden="true"></i> Otključaj</button>
					</div>
					
					<div class="column medium-3">
						<button class="btn btn-secondary btn-xm JSexecute" data-execute='[{"table":"roba", "column":"flag_zakljucan", "val":"true"},{"table":"dobavljac_cenovnik", "column":"flag_zakljucan", "val":"1"}]'><i class="fa fa-lock" aria-hidden="true"></i> Zaključaj</button>
					</div>
				</div> <!-- end of .row -->

				<div class="row">	
					<div class="column medium-12 large-3">
						<button class="btn btn-secondary btn-xm JSexecute" data-execute='[{"table":"roba", "column":"flag_prikazi_u_cenovniku", "val":"1"}, {"table":"dobavljac_cenovnik", "column":"flag_prikazi_u_cenovniku", "val":"1"}]'><i class="fa fa-upload" aria-hidden="true"></i> Prikaži na Web-u</button>
					</div>
					<div class="column medium-12 large-3">
						<button class="btn btn-secondary btn-xm JSexecute" data-execute='[{"table":"roba", "column":"flag_prikazi_u_cenovniku", "val":"0"}, {"table":"dobavljac_cenovnik", "column":"flag_prikazi_u_cenovniku", "val":"0"}]'><i class="fa fa-download" aria-hidden="true"></i> Skloni sa Web-a</button> 
					</div>
					<div class="column medium-12 large-3">
						<button data-reveal-id="JSakcijaModal" class="btn btn-secondary btn-xm " ><i class="fa fa-calendar-check-o" aria-hidden="true"></i> Akcija</button>
					</div>

					<div class="column medium-12 large-3">
						<button class="btn btn-secondary btn-xm JSexecute" data-execute='[{"table":"roba", "column":"akcija_flag_primeni", "val":"0"},{"table":"roba", "column":"akcija_popust", "val":"0"},{"table":"roba", "column":"akcijska_cena", "val":"0"}]'><i class="fa fa-calendar-times-o" aria-hidden="true"></i> Skloni</button>	
					</div>
 	  			</div>

				<div class="row">  	
					 @if(AdminOptions::checkB2B())
					<div class="column medium-12 large-3">
						<button class="btn btn-secondary btn-xm JSexecute" data-execute='[{"table":"roba", "column":"flag_cenovnik", "val":"1"}]'><i class="fa fa-upload" aria-hidden="true"></i> Prikaži na B2B-u</button>
					</div>
					@endif 
					@if(AdminOptions::checkB2B())
					<div class="column medium-12 large-3">
						<button class="btn btn-secondary btn-xm JSexecute" data-execute='[{"table":"roba", "column":"flag_cenovnik", "val":"0"}]'><i class="fa fa-download" aria-hidden="true"></i> Skloni sa B2B-a</button>
					</div> 
					@endif
					
					 <div class="column medium-12 large-3">
						<button class="btn btn-secondary btn-xm JSexecute" data-execute='[{"table":"roba", "column":"flag_aktivan", "val":"1"}, {"table":"dobavljac_cenovnik", "column":"flag_aktivan", "val":"1"}]'><i class="fa fa-toggle-on" aria-hidden="true"></i> Aktivno</button>
					</div>
					
					<div class="column medium-12 large-3">
						<button class="btn btn-secondary btn-xm JSexecute" data-execute='[{"table":"roba", "column":"flag_aktivan", "val":"0"}, {"table":"dobavljac_cenovnik", "column":"flag_aktivan", "val":"0"}]'><i class="fa fa-toggle-off" aria-hidden="true"></i> Neaktivno</button>
					</div>
				 </div> <!-- end of .row -->
			</div>

	 
</div> <!-- end of .row -->



<div class="flat-box">
	<div class="row search-row article-custom-columns flex">
  		<section class="order-filters first-col loco search-section-group clearfix">
  			<div class="row"> 
				<div class="column medium-8">		
					<input type="text" class="" id="search" autocomplete="off" placeholder="Pretraži...">
				</div>
				<div class="column medium-4">
					<button type="submit" id="search-btn" value="Pronađi" class="m-input-and-button__btn btn btn-primary btn-xm"><i class="fa fa-search" aria-hidden="true"></i></button>
					<button type="submit" id="clear-btn" value="Poništi" aria-label="Poništi" class="m-input-and-button__btn btn btn-secondary btn-xm tooltipz"><i class="fa fa-times" id="JSCenaClear" aria-hidden="true"></i></button>
				</div>
			</div>
		  </section>
  	<!-- filter za karakteristike -->
		@if( $criteria['grupa_pr_id'] > 0 )	
		<div class="third-col loco col-h">
			<select class="admin-select m-input-and-button__input" id="karakt_select">
				<option value="0">Izaberi karakteristiku</option>
				@foreach(AdminSupport::getKarakNaziv($grupa_pr_id) as $karak)
					@if(str_replace('-','',$criteria['karakteristika']) == $karak->grupa_pr_naziv_id )						
					<option value="{{ $karak->grupa_pr_naziv_id }}" selected>{{ $karak->naziv }}</option>
					@else
					<option value="{{ $karak->grupa_pr_naziv_id }}" >{{ $karak->naziv }}</option>
					@endif
				@endforeach
			</select>
			<button class="btn btn-primary btn-abs dobavljac_proizvodjac m-input-and-button__btn" title="Artikli koji imaju dodeljenu izabranu karakteristiku"><i class="fa fa-check" aria-hidden="true"></i>
			</button>
			<button class="btn btn-primary btn-abs2 dobavljac_proizvodjac m-input-and-button__btn" data-kind="NE" title="Artikli koji nemaju dodeljenu izabranu karakteristiku"><i class="fa fa-minus-square-o" aria-hidden="true" ></i>
			</button> 
		</div>
		@endif
		<!-- end -->

		<div class="second-col loco col-h">
			<div class="form-bottom-btn">
				<select class="" id="dobavljac_select" multiple="multiple">
					@foreach(AdminSupport::dobavljac_ime() as $dobavljac)
						@if( in_array($dobavljac->partner_id ,explode("-",$criteria['dobavljac'])) )
							<option value="{{ $dobavljac->partner_id }}" selected>{{ $dobavljac->naziv }}</option>
						@else
							<option value="{{ $dobavljac->partner_id }}">{{ $dobavljac->naziv }}</option>
						@endif
					@endforeach
				</select>
				<button class="btn btn-primary btn-abs dobavljac_proizvodjac m-input-and-button__btn"><i class="fa fa-check" aria-hidden="true"></i></button>
			</div>
		</div>
					
		<div class="fourth-col loco col-h">
			<div class="form-bottom-btn">
					<select class="admin-select m-input-and-button__input" id="proizvodjac_select" multiple="multiple">
					@foreach(AdminSupport::getProizvodjaci() as $proizvodjac)
						@if( in_array($proizvodjac->proizvodjac_id ,explode("+",$criteria['proizvodjac'])) )
						<option value="{{ $proizvodjac->proizvodjac_id }}" selected>{{ $proizvodjac->naziv }}</option>
						@else
						<option value="{{ $proizvodjac->proizvodjac_id }}">{{ $proizvodjac->naziv }}</option>
						@endif
					@endforeach
				</select>
				<button class="btn btn-primary btn-abs dobavljac_proizvodjac m-input-and-button__btn"><i class="fa fa-check" aria-hidden="true"></i></button>
			</div>
		</div>
		<div class="fifth-col loco col-h">
			<div class="form-bottom-btn">
				<select class="admin-select m-input-and-button__input" id="magacin_select">
					<option value="0">Svi magacini</option>
					@foreach(DB::table('orgj')->where('b2c',1)->orderBy('orgj_id','asc')->get() as $row)
						@if( in_array($row->orgj_id ,explode("-",$criteria['magacin'])) )
						<option value="{{ $row->orgj_id }}" selected>{{ $row->naziv }}</option>
						@else
						<option value="{{ $row->orgj_id }}">{{ $row->naziv }}</option>
						@endif
					@endforeach
				</select>
				<button class="btn btn-primary btn-abs dobavljac_proizvodjac m-input-and-button__btn"><i class="fa fa-check" aria-hidden="true"></i></button>
			</div>
		</div>
		<div class="sixth-col loco col-h">
			<div class="form-bottom-btn">
				<select class=" admin-select m-input-and-button__input" id="tip_select" multiple="multiple">
					@foreach(AdminSupport::getTipovi() as $tip)
						@if( in_array($tip->tip_artikla_id ,explode("-",$criteria['tip'])) )
						<option value="{{ $tip->tip_artikla_id }}" selected>{{ $tip->naziv }}</option>
						@else
						<option value="{{ $tip->tip_artikla_id }}">{{ $tip->naziv }}</option>
						@endif
					@endforeach
				</select>
				<button class="btn btn-primary btn-abs dobavljac_proizvodjac m-input-and-button__btn"><i class="fa fa-check" aria-hidden="true"></i></button>
			</div>
		</div>


	</div>
</div>

<!-- nesto ovako je bilo order-filters -->
<section class="articles-listing clearfix">
	<div class="row collapse">
	
		<section class="total-article-count columns medium-7">
 			<a class="dodavanje_artikla btn-create add-new-article" id="new-art" href="{{AdminOptions::base_url()}}admin/product{{AdminOptions::gnrl_options(3029)==1 ? '-short' : ''}}/0" target="_blank">
 				<i class="fa fa-plus-square plusikona" aria-hidden="true"></i>Dodaj novi artikal (F2)
 			</a>
			
			@if(isset($count_products))
				<small>Ukupno artikala: <b>{{$count_products}}</b></small> &nbsp;&nbsp; 
			@endif
			<small>Po strani: <b>{{$limit}}</b></small>
			| <small>Selektovano: </small><b><small id="JSselektovano">0</small></b>
			<small><b>| {{$title}}</b></small>
		</section> <!-- end of .total-article-count -->


		<div class="columns medium-5 text-right">
			<form method="POST" action="{{AdminOptions::base_url()}}admin/products-import" enctype="multipart/form-data">
				<div class="bg-image-file"> 
					<input type="file" name="import_file">
					 
					<button class="btn btn-primary btn-small" type="submit">Importuj</button>
					<div class="error"> 
						{{ $errors->first('import_file') ? $errors->first('import_file') : '' }}
					</div>
				</div>	 
			</form>
		</div>

		 
		
	</div> <!-- end of .row -->

	<div class="table-scroll">
		<table class="article-artiacle-table articles-page-tab JSproduct_list_table">
			<thead class=""><!--  fixed-head -->
				<tr class="st order-list-titles row">
					<th class="table-head"></th>
					@if(AdminOptions::sifra_view()==1)
					<th class="table-head" data-coll="r.roba_id" data-sort="ASC"><a href="#">ID</a> </th>
					@endif					
					@if(AdminOptions::sifra_view()==2)
					<th class="table-head" data-coll="r.sifra_is" data-sort="ASC"><a href="#">Šifra IS</a> </th>
					@endif					
					@if(AdminOptions::sifra_view()==3)
					<th class="table-head" data-coll="r.sku" data-sort="ASC"><a href="#">SKU</a> </th>
					@endif
					@if(AdminOptions::sifra_view()==4)
					<th class="table-head" data-coll="r.sifra_d" data-sort="ASC"><a href="#">Šifra Dob</a> </th>
					@endif
					<!-- <th class="table-head" data-coll="sku" data-sort="ASC"><a href="#">SKU</a> </th> -->
					<th class="table-head" data-coll="flag_prikazi_u_cenovniku" data-sort="ASC"><a href="#">WEB</a></th>
					
					<th class="table-head" data-coll="akcija_flag_primeni" data-sort="ASC"><a href="#">AKCIJA</a></th>
					<th class="table-head" data-coll="naziv" data-sort="ASC" style="width: {{$naziv_width}}px"><a href="#">NAZIV </a></th>
					<th class="table-head" data-coll="kolicina" data-sort="ASC"><a href="#" class="tooltipz" aria-label="Količina">Kol </a></th>
					<th class="table-head" data-coll="web_cena" data-sort="ASC"><a href="#" class="tooltipz" aria-label="Web cena">WebCena </a></th>
					<th class="table-head" data-coll="web_marza" data-sort="ASC"><a href="#" class="tooltipz" aria-label="Web Marža">W-M </a></th>
					<th class="table-head" data-coll="mpcena" data-sort="ASC"><a href="#" class="tooltipz" aria-label="Maloprodajna cena">MpCena </a></th>
					<th class="table-head" data-coll="mp_marza" data-sort="ASC"><a href="#" class="tooltipz" aria-label="Maloprodajna Marža">Mp-M </a></th>
					<th class="table-head" data-coll="kolicina" data-sort="ASC"><a href="#">PDV </a></th>
					<th class="table-head" data-coll="sifra_d" data-sort="ASC"><a href="#"  class="tooltipz" aria-label="Nabavna cena">NC</a> </th>
					<th class="table-head" data-coll="dobavljac_id" data-sort="ASC"><a href="#"  class="tooltipz" aria-label="Dobavljač">DOB. </a></th>
					<!-- <th class="table-head" data-coll="dobavljac_kolicina" data-sort="ASC"><a href="#">Kol D. </a></th>
					<th class="table-head" data-coll="sifra_d" data-sort="ASC"><a href="#">ŠIFRA D</a> </th> -->
					<th class="table-head" data-coll="web_opis" data-sort="ASC"><a href="#">OPIS</a> </th>
					<th class="table-head" data-coll="web_flag_karakteristike" data-sort="ASC"><a href="#"  class="tooltipz" aria-label="Flag karakteristika">FLAG KARAK.</a> </th>
					
					<th class="table-head" data-coll="br_slika" data-sort="ASC"><a href="#" class="tooltipz" aria-label="Broj slika">Br.sl.</a> </th>
					<th class="table-head" data-coll="tip_cene" data-sort="ASC"><a href="#">TIP</a></th>
					<th class="table-head" data-coll="flag_zakljucan" data-sort="ASC"><a href="#" class="tooltipz" aria-label="Zaključan">ZKLJ</a></th>
					<th class="table-head" data-coll="proizvodjac_id" data-sort="ASC"><a href="#" class="tooltipz" aria-label="Proizvođač">PRO. </a></th>
					<th class="table-head" data-coll="flag_aktivan" data-sort="ASC"><a href="#" class="tooltipz-left" aria-label="Aktivno">AKT</a></th>	
				</tr>
			</thead>
			<tbody id="selectable">
			 @foreach($articles as $row)
				<tr class="admin-list order-list-item row ui-widget-content {{$row->flag_aktivan == 1 ? '' : 'text-red'}}" data-id="{{ $row->roba_id }}">
					<td class="">
						<button class="btn btn-secondary btn-xm edit-article-btn btn-circle small JSArticleEdit tooltipz-right" aria-label="Izmeni artikal" data-zakljucan="{{ $row->flag_zakljucan == 'true'?'1':'0' }}" data-short="{{ AdminOptions::gnrl_options(3029) }}">
							<i class="fa fa-pencil" aria-hidden="true"></i>
						</button>
					</td>
					@if(AdminOptions::sifra_view()==1)
					<td class="">{{ $row->roba_id }}</td>
					@endif
					@if(AdminOptions::sifra_view()==2)
					<td class="">{{ $row->sifra_is }}</td>
					@endif
					@if(AdminOptions::sifra_view()==3)	
					<td class="">{{ $row->sku }}</td>
					@endif
					@if(AdminOptions::sifra_view()==4)
					<td class="">{{ $row->sifra_d }}</td>
					@endif
					<td class="flag_prikazi_u_cenovniku">{{ $row->flag_prikazi_u_cenovniku == 1 ? '<i class="fa fa-check green" aria-hidden="true"></i>' : '<i class="fa fa-times red" aria-hidden="true"></i>' }}</td>
					<td class="akcija_flag_primeni">
					@if(AdminArticles::provera_akcija($row->roba_id)==1)
						<span class="akcija-a">A</span>
					@else
						<span class="akcija-bez">-</span>
					@endif
					</td>
					
					<td class="table-fixed-width1 naziv1">
						<span class="JSnaziv1Vrednost">
						{{ $row->naziv }}
						</span>
						<span class="JSEditNaziv1BtnSpan">
						<input type="text" class="JSnaziv1Input" value="{{ htmlentities($row->naziv) }}" />
						<i class="fa fa-pencil JSEditNaziv1Btn" aria-hidden="true"></i>
						</span>
					</td>

					<td class="kolicina">
						<span class="JSkolicinaVrednost">
							{{ AdminArticles::kolicina($row->kolicina,$row->roba_id,$row->kolicina,$criteria['magacin']) }}
						</span>
						<span class="JSEditkolicinaBtnSpan">
							<input type="text" class="JSkolicinaInput" value="{{ AdminArticles::kolicina($row->kolicina,$row->roba_id,$row->kolicina,$criteria['magacin'])}}" />
							<i class="fa fa-pencil JSEditkolicinaBtn" aria-hidden="true"></i>
						</span>
					</td>

					<td class="wc-col WebCena">
						<span class="JSWebCenaVrednost">
							{{ $row->web_cena }}
						</span>
						<span class="JSEditWebCenaBtnSpan">
							<input type="text" class="JSWebCenaInput" value="{{ htmlentities($row->web_cena) }}" />
							<i class="fa fa-pencil JSEditWebCenaBtn" aria-hidden="true"></i>
						</span>
					</td>
					<td class="web_marza">{{ $row->web_marza }}</td>
					<td class="mpcena">{{ $row->mpcena }}</td>
					<td class="mp_marza">{{ $row->mp_marza }}</td>
					<td class="tarifna_grupa">{{ AdminSupport::find_tarifna_grupa($row->tarifna_grupa_id,'porez') }}</td>
					<td class="racunska_cena_nc">{{ $row->racunska_cena_nc }}</td>					
					<td class="">{{ $row->dobavljac }}</td>

					<td class="">{{ $row->web_opis != null && $row->web_opis != '' ? 'DA' : 'NE' }}</td>
					<td class="web_flag_karakteristike">{{ AdminSupport::nameOfCharact($row->web_flag_karakteristike) }}</td>
					
					<td class="web_slika">{{ $row->br_slika }}</td>
					<td class="tip_cene">{{ AdminSupport::tip_naziv($row->tip_cene) }}</td>
					<td class="flag_zakljucan">{{ $row->flag_zakljucan == 'true' ? 'DA' : 'NE' }}</td>
					<td class="proizvodjac_id">{{ All::get_manofacture_name($row->proizvodjac_id) }}</td>
					<td class="flag_aktivan">{{ $row->flag_aktivan == 1 ? '<i class="fa fa-check green" aria-hidden="true"></i>' : '<i class="fa fa-times red" aria-hidden="true"></i>' }}</td>	

				</tr>
				@endforeach
				
			</tbody>
		</table>
	</div>
	
	<div class="row collapse">
	{{ Paginator::make($articles, $count_products, $limit)->links() }}
	</div>
 
</section>
	<!-- ====================== -->
		<div class="text-center">
			<a href="#" class="video-manual" data-reveal-id="articles-manual">Uputstvo <i class="fa fa-film"></i></a>
		   	<div id="articles-manual" class="reveal-modal medium" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
				<div class="video-manual-container"> 
					<p><span class="video-manual-title">Artikli</span></p> 
			  		<iframe src="https://player.vimeo.com/video/271254462" width="840" height="426" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
  					<a class="close-reveal-modal" aria-label="Close">&#215;</a>
				</div>
			</div>
		</div>
 	<!-- ====================== -->
<!-- MODAL ZA DETALJI PORUDJINE -->
<div id="JSSelectImageModal" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
  <div class="content"></div>
  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>
<!-- MODAL END -->
<div id="wait">
	<p><img class="loader" src="{{ AdminOptions::base_url()}}images/admin/wheel.gif">Molimo vas sačekajte...</p>
</div>


<!-- MODALS -->



<div id="JSchooseGroupModal" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	<h4 class="modal-title-h4">Promeni grupu</h4>
	<div class="m-input-and-button small-margin-bottom column medium-12">
			<select class="JSexecuteInput m-input-and-button__input" id="change-group">
				{{AdminSupport::selectGroups()}}
			</select>
			<button class="m-input-and-button__btn btn btn-secondary btn-xm JSexecuteBtn" data-execute='[{"table":"roba", "column":"grupa_pr_id"},{"table":"dobavljac_cenovnik", "column":"grupa_pr_id"}]'><i class="fa fa-check" aria-hidden="true"></i></button>
		</div>
	</div>
</div>

<div id="JSchooseBrandModal" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	<h4 class="modal-title-h4">Promeni proizvođača</h4>
	<div class="row"> 
		<div class="m-input-and-button small-margin-bottom column medium-12">
			<select class="JSexecuteInput m-input-and-button__input" id="change-manufac">
				<option value="">Promeni proizvođača</option>
				@foreach(AdminSupport::getProizvodjaci() as $row)
					<option value="{{ $row->proizvodjac_id }}">{{ $row->naziv }}</option>
				@endforeach
			</select>
			<button class="m-input-and-button__btn btn btn-secondary btn-xm JSexecuteBtn" data-execute='[{"table":"roba", "column":"proizvodjac_id"},{"table":"dobavljac_cenovnik", "column":"proizvodjac_id"}]'><i class="fa fa-check" aria-hidden="true"></i></button>
		</div>
	</div>
</div>

<div id="JSchooseTipModal" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	<h4 class="modal-title-h4">Promeni tip</h4>
	<div class="row"> 
		<div class="m-input-and-button small-margin-bottom column medium-12">
			<select class="JSexecuteInput m-input-and-button__input">
				<option value="null">Bez tipa</option>
				@foreach(AdminSupport::getTipovi() as $row)
					<option value="{{ $row->tip_artikla_id }}">{{ $row->naziv }} ({{ $row->tip_artikla_id }})</option>
				@endforeach
			</select>
			<button class="m-input-and-button__btn btn btn-secondary btn-xm JSexecuteBtn" data-execute='[{"table":"roba", "column":"tip_cene"}]'><i class="fa fa-check" aria-hidden="true"></i></button>
		</div>
    </div>
</div>

<div id="JSchoosePriceModal" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	<h4 class="modal-title-h4">Promeni vrstu cene</h4>
	<div class="row"> 
		<div class="m-input-and-button small-margin-bottom column medium-12">
			<select class="JSexecuteInput m-input-and-button__input">
				@foreach(AdminSupport::getFlagCene() as $row)
					<option value="{{ $row->roba_flag_cene_id }}">{{ $row->naziv }} </option>
				@endforeach
			</select>
			<button class="m-input-and-button__btn btn btn-secondary btn-xm JSexecuteBtn" data-execute='[{"table":"roba", "column":"roba_flag_cene_id"}]'><i class="fa fa-check" aria-hidden="true"></i></button>
		</div>
	</div>	
</div>

<div id="JSchooseQuantityModal" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	<h4 class="modal-title-h4">Promeni količinu</h4>
	<div class="row"> 
		<div class="m-input-and-button small-margin-bottom column medium-12">
			<select class="JSMagacinKolicina m-input-and-button__input">
			@foreach(DB::table('orgj')->where('b2c',1)->orderBy('orgj_id','asc')->get() as $row)
				<option value="{{ $row->orgj_id }}">{{ $row->naziv }} </option>
			@endforeach
			</select>
			<input type="text" class="m-input-and-button__input" id="JSKolicinaAdd" placeholder="Promeni količinu">
			<button class="m-input-and-button__btn btn btn-secondary btn-xm" id="JSKolicinaBtn"><i class="fa fa-check" aria-hidden="true"></i></button>
		</div>
	</div>	
</div>

<div id="JSTezinaModal" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	<h4 class="modal-title-h4">Promeni težinu (gr.)</h4>
	<div class="row"> 
		<div class="m-input-and-button small-margin-bottom column medium-12">
			<input type="text" class="m-input-and-button__input" id="JSTezinaAdd" placeholder="Promeni težinu">
			<button class="m-input-and-button__btn btn btn-secondary btn-xm" id="JSTezinaBtn"><i class="fa fa-check" aria-hidden="true"></i></button>
		</div>
	</div>	
</div> 
<div id="JSTagModal" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	<h4 class="modal-title-h4">Dodeli tagove</h4>
	<div class="row"> 
		<div class="m-input-and-button small-margin-bottom column medium-12">
			<input type="text" class="m-input-and-button__input" id="JSTagAdd" placeholder="Dodeli tagove">
			<button class="m-input-and-button__btn btn btn-secondary btn-xm" id="JSTagBtn"><i class="fa fa-check" aria-hidden="true"></i></button>
		</div>
	</div>	
</div>

<div id="JSchooseTaxModal" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	<h4 class="modal-title-h4">Poreska stopa</h4>
	
	<div class="row bw box-art">
		<div class="column medium-12 large-5">
			<label>Poreska stopa</label>
			<select class="m-input-and-button__input" id="JSValPoreskaStopa">
				<option value=""></option>
				@foreach(AdminSupport::getPoreskeGrupe() as $row)
					<option value="{{ $row->tarifna_grupa_id }}">{{ $row->naziv }}</option>
				@endforeach
			</select>
		</div>
		<div class="column medium-12 large-7">
			<label>Promeni i web cenu</label>
			<select class="m-input-and-button__input" id="JSCenaPoreskaStopa">
				<option value="1">DA</option>
				<option value="0">NE</option>
			</select>
			<button class="p-stopa m-input-and-button__btn btn-admin btn btn-primary" id="JSPoreskaStopa">Promeni poresku stopu</button>
		</div>
	</div>
</div>
			
				

<div id="JSchoosePicturesModal" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	<h4 class="modal-title-h4">Dodavanje slike</h4>
		<div class="row upload-row">
		  	<div class="column medium-12 large-5">
				<div class="m-input-and-button options-div small-margin-bottom">
					<a href="{{ AdminOptions::base_url() }}admin/upload-image-article" class="articles-btn-l a-btn-articles btn-admin btn btn-primary btn-xm imgs-btn" target="_blank">Učitaj novu sliku</a>
					
					<button id="JSSelectImageAssign" class="articles-btn-l a-btn-articles btn-admin btn btn-primary btn-xm imgs-btn">Dodeli sliku</button>
				</div>
			</div>
			<div class="column medium-12 large-4">
				<div id="JSSelectedImage"><span class="options-title-img">Dodaj sliku</span></div>
			</div>
	  	</div>
</div>

		

<div id="JSchooseExportModal" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	<h4 class="modal-title-h4">Export artikala</h4>
		<div class="column medium-12 large-12">
			@if(count(AdminSupport::getExporti()) > 0)
			<h3 class="text-center">Export</h3>
			<div class="row">
				<div class="columns medium-7"> 
					<select class="columns medium-6" id="export_select">
						<option value="0">Svi artikli</option>
						@foreach(AdminSupport::getExporti() as $export)
							@if($export->export_id == str_replace('-','',$criteria['exporti']))
							<option value="{{ $export->export_id }}" selected data-export="{{ $export->vrednost_kolone }}">{{ $export->naziv }}</option>
							@else
							<option value="{{ $export->export_id }}" data-export="{{ $export->vrednost_kolone }}">{{ $export->naziv }}</option>
							@endif
						@endforeach
					</select>&nbsp;
					<button class="btn btn-primary dobavljac_proizvodjac m-input-and-button__btn">Da</button>
					<button class="btn btn-primary m-input-and-button__btn" id="JSExportNo">Ne</button>
					<button class="btn btn-primary m-input-and-button__btn JSExport tooltipz" aria-label="Dodeli" data-kind="export_dodeli">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</button>
					<button class="btn btn-primary m-input-and-button__btn JSExport tooltipz" aria-label="Ukloni" data-kind="export_ukloni">
						<i class="fa fa-times" aria-hidden="true"></i>
					</button>
				</div>

			 
				<div class="columns medium-5">
					<select id="JSExportKind" class="columns medium-5">
						<option value="xml">XML</option>
						<option value="xls">XLS</option>
						<option value="csv">CSV</option>
						<option value="json">JSON</option>
					</select>&nbsp;
					<button class="btn btn-primary m-input-and-button__btn" id="JSExportExecute">Exportuj</button>
				</div>
				
				 
			</div>
			@endif
			 <div class="row">
			 	<br>
				<a class="articles-btn exp-btn btn btn-primary btn-xm" href="{{ AdminOptions::base_url()}}admin/export_xls">EXPORT SVIH ARTIKALA (.xls)</a>	
			</div>
		</div> 
</div>

<div id="JSmassEditModal" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	<h4 class="modal-title-h4">Edit cena</h4>

	<div class="column medium-12 large-12">
		<div class="row massEditRow">
			<div class="medium-3 columns">
				<label for="">Od cene</label>
				<select id="JSMassEditCenaOd" class="inp-sm">
					<option value="nc">Nabavna cena</option>
					<option value="wc">Web cena</option>
					<option value="mc">Maloprodajna cena</option>
				</select>
			</div>
		
			<div class="medium-3 columns">
				<label for="">Za cenu</label>
				<select id="JSMassEditCenaZa" class="inp-sm">
					<option value="nc">Nabavna cena</option>
					<option value="wc">Web cena</option>
					<option value="mc">Maloprodajna cena</option>
				</select>
			</div>
			<div class="medium-6 columns">
				<label for="">Preracunavanje Web i MP cene</label>
				<button id="JSPreracunajWebCenuButton" class="btn btn-primary m-input-and-button__btn" title="WebCena">Preracunaj Web Cenu</button>
				<button id="JSPreracunajMPCenuButton" class="btn btn-primary m-input-and-button__btn" title="MpCena">Preracunaj MP Cenu</button>
			</div>
		</div>

		<div class="row">
			<div class="medium-6 columns">
				<label for="">Marža (%)</label>
				<input type="text" id="JSMassEditMarzaInput" class="m-input-and-button__input fl inp-sm" id="" placeholder="">
				<button id="JSMassEditMarzaButton" class="btn btn-primary m-input-and-button__btn"><i class="fa fa-plus" aria-hidden="true"></i></button>
				<input type="checkbox" id="JSMassEditMarzaCheck">Prihvati maržu
			</div>
			<div class="medium-6 columns">
				<label for="">Web Cena</label>
				<input type="text" class="m-input-and-button__input fl inp-sm" id="JSWebCenaEdit" placeholder="">
				<button class="m-input-and-button__btn btn btn-secondary btn-xm" id="JSWebCenaEditBtn"><i class="fa fa-check" aria-hidden="true"></i></button>
			</div>
		</div>

		<div class="row"> 

			<div class="medium-6 columns">
				<label for="">Rabat (%)</label>
				<input type="text" id="JSMassEditRabatInput" class="m-input-and-button__input fl inp-sm" id="" placeholder="">
				<button id="JSMassEditRabatButton" class="btn btn-primary m-input-and-button__btn"><i class="fa fa-plus" aria-hidden="true"></i></button>
			</div>

			<div class="medium-6 columns">
				<label for="">MP Cena</label>
				<input type="text" class="m-input-and-button__input fl inp-sm" id="JSMPEdit" placeholder="">
				<button class="m-input-and-button__btn btn btn-secondary btn-xm" id="JSMPEditBtn"><i class="fa fa-check" aria-hidden="true"></i></button>
			</div>
		</div>
	</div>
</div>

<div id="JScharacteristicsModal" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	<h4 class="modal-title-h4">Dodela karakteristika</h4>
		<div id="JSTableContentGenerisane" class="m-input-and-button small-margin-bottom column medium-12">	
		</div>
</div>

<div id="JSobradaKarakteristikaModal" class="reveal-modal xsm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	<h4 class="modal-title-h4">Obrada karakteristika</h4>
	<br>
	<div class="row"> 
		<div class="column medium-6 large-6">
			<select id="JSKarakteristikaVrsta">
				<option value="">Izaberite vrstu karakteristika</option>
				<option value="0">HTML karakteristike</option>
				<option value="1">Generisane karakteristike</option>
				<option value="2">Od dobavljača</option>
			</select>
		</div>

		<div class="column medium-6 large-6">
			<button id="JSKarakteristikeDodeli" class="btn btn-primary m-input-and-button__btn">Dodeli</button>
			<button id="JSKarakteristikeObrisi" class="btn btn-danger m-input-and-button__btn">Obriši</button>
		</div>
	</div>
	<hr>
	
	<div class="column medium-12 large-12">
	Prebaci HTML karakteristike u opis
	<button id="JSHtmlPrebaci" class="btn btn-primary m-input-and-button__btn">Prebaci</button>
	</div>
</div>

<div id="JSArticleEditModal" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	<h4 class="modal-title-h4">Izmena artikla</h4>	
	<div class="column medium-12 large-12 text-center">
		Artikal je zaključan! Da li želite da otključate artikal?
	</div>
	<div class="column medium-12 large-12 text-center">
		<button id="JSArticleEditYes" class="btn btn-primary m-input-and-button__btn">DA</button>
		<button id="JSArticleEditNo" class="btn btn-danger m-input-and-button__btn">NE</button>
	</div>
</div>

<div id="JStextEditModal" class="reveal-modal sm-modal txt-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	<h4 class="modal-title-h4">Edit teksta</h4>

	<div class="row">
		<div class="columns medium-5 text-right">
			<input type="radio" id="JStextEditNaziv" name="naziv-opis" value="naziv" checked />Naziv
		</div>
		<div class="columns medium-6 center">
			<input type="radio" id="JStextEditOpis" name="naziv-opis" value="opis">Opis
		</div>
	</div> 
	 
	<h3 class="title-txt">Dodavanje teksta</h3>
	 
	<div class="row"> 
		<div class="columns medium-6 center">
			<input type="radio" id="JStextEditStart" name="start-end" value="start" checked /><label>Na početku</label> 
			<input type="radio" id="JStextEditEnd" name="start-end" value="end"><label>Na kraju</label>	
		</div>
		<div class="columns medium-6">	  
			<input type="text" id="JStextEditNazivInput" class="m-input-and-button__input"  placeholder="Tekst">
			<button id="JStextInsertButton" class="btn btn-primary btn-abs m-input-and-button__btn">Dodaj</button>
		</div>
	</div> 
	<br>

	<h3 class="title-txt">Zameni tekst</h3>

	<div class="row"> 
		<div class="columns medium-12">
			<div class="row"> 
				<div class="columns medium-6 center">
					<input type="radio" id="JStextEditDeo" name="ceo-deo" value="deoTeksta" checked /><label>Deo teksta</label> 
					<input type="radio" id="JStextEditCelo" name="ceo-deo" value="celaRec"><label>Cela reč</label>  
				</div>
	 	 	</div>
	 	 	<div class="row"> <br>
			 	<div class="columns medium-6">
			 		<input type="text" name="JStextEditInput1" id="JStextEditInput1" class="m-input-and-button__input" placeholder="Tekst koji se menja">
			 	</div>
			 	<div class="columns medium-6"> 
					<input type="text" name="JStextEditInput2" id="JStextEditInput2" class="m-input-and-button__input" placeholder="Novi tekst">
					<button id="JStextReplaceButton" class="btn btn-primary btn-abs m-input-and-button__btn">Zameni</button>
				</div> 
			</div>
		</div> 
	</div>
	<br>
 	<label id="Results"></label>
</div> 

<div id="JSakcijaModal" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	<h4 class="modal-title-h4">AKCIJA</h4>  
 
 	<div class="row">
  		<div class="columns medium-4">
			<label>Unesite popust</label>
			<input type="text" style="width: 90%;" id="JSAkcijaAdd" placeholder="Akcijski popust (%)"> 
		</div>
		<div class="column medium-4 akcija-column action-modal">
			<label>Datum akcije od</label>
			<input class="akcija-input" id="datum_akcije_od" name="datum_akcije_od" autocomplete="off" type="text" required>
			<span id="datum_od_delete"><i class="fa fa-times" aria-hidden="true"></i></span>
		</div> 
		<div class="column medium-4 akcija-column action-modal">
			<label>Datum akcije do</label>
			<input class="akcija-input" id="datum_akcije_do" name="datum_akcije_do" autocomplete="off" type="text" required>
			<span id="datum_do_delete"><i class="fa fa-times" aria-hidden="true"></i></span>
		</div>	 
	</div> 
	<div class="center"> 
		<button class="m-input-and-button__btn btn btn-secondary btn-xm" id="JSAkcijaBtn">&nbsp; Unesi &nbsp;</button> 
	</div>
</div>