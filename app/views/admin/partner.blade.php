<div id="main-content" class="kupci-page">
@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
@endif
	  
		<div class="row"> 
			<div class="large-2 medium-3 small-12 columns">
				<a class="m-subnav__link JS-nazad" href="{{AdminOptions::base_url()}}admin/kupci_partneri/partneri">
					<div class="m-subnav__link__icon">
						<i class="fa fa-arrow-left" aria-hidden="true"></i>
					</div>
					<div class="m-subnav__link__text">
						Nazad
					</div>
				</a>
			</div>
		</div>
			 <div class="row">
				<form action="{{AdminOptions::base_url()}}admin/kupci_partneri/partneri/{{ $partner_id }}/save" method="post" class="new-customer-form" autocomplete="false">
				
					<input type="hidden" id="partner_id" name="partner_id" value="{{ $partner_id }}">

					<div class="row partners">
						<div class="column medium-12 large-4">
							<div class="flat-box">
							<div>
								<label>Naziv</label>
								<input class="{{ $errors->first('naziv') ? 'error' : '' }}" id="naziv" name="naziv" value="{{ htmlentities(Input::old('naziv') ? Input::old('naziv') : $naziv) }}" type="text">
								<label class="error">{{ $errors->first('naziv') }}</label>
							</div>

							<div>
								<label>Puni naziv</label>
								<input class="{{ $errors->first('naziv_puni') ? 'error' : '' }}" id="naziv_puni" name="naziv_puni" value="{{ Input::old('naziv_puni') ? Input::old('naziv_puni') : $naziv_puni }}" type="text">
							</div>

					@if(AdminNarudzbine::api_posta(3))
					<div> 
						<label>Opština</label>				 
						<select class="form-control" name="opstina" id="JSOpstina" tabindex="6">
						@foreach(AdminNarudzbine::opstine() as $key => $opstina)
							@if((Input::old('opstina') ? Input::old('opstina') : AdminNarudzbine::opstina_id($ulica_id)) == $opstina->narudzbina_opstina_id)
							<option value="{{ $opstina->narudzbina_opstina_id }}" selected>{{ $opstina->naziv }}</option>
							@else
							<option value="{{ $opstina->narudzbina_opstina_id }}">{{ $opstina->naziv }}</option>
							@endif
						@endforeach
						</select>
					</div>
					<div>
						<label>Naselje</label>
						<select class="form-control" name="mesto_id" id="JSMesto">
						@foreach(AdminNarudzbine::mesta(Input::old('opstina') ? Input::old('opstina') : AdminNarudzbine::opstina_id($ulica_id)) as $key => $mesto_obj)
							@if((Input::old('mesto_id') ? Input::old('mesto_id') : AdminNarudzbine::mesto_id($ulica_id)) == $mesto_obj->narudzbina_mesto_id)
							<option value="{{ $mesto_obj->narudzbina_mesto_id }}" selected>{{ $mesto_obj->naziv }}</option>
							@else
							<option value="{{ $mesto_obj->narudzbina_mesto_id }}">{{ $mesto_obj->naziv }}</option>
							@endif
						@endforeach
						</select>
					</div>
					<div> 
						<label>Ulica</label>				 
						<select class="form-control" name="ulica_id" id="JSUlica">
						@foreach(AdminNarudzbine::ulice(Input::old('mesto_id') ? Input::old('mesto_id') : AdminNarudzbine::mesto_id($ulica_id)) as $key => $ulica)
							@if((Input::old('ulica_id') ? Input::old('ulica_id') : $ulica_id) == $ulica->narudzbina_ulica_id)
							<option value="{{ $ulica->narudzbina_ulica_id }}" selected>{{ $ulica->naziv }}</option>
							@else
							<option value="{{ $ulica->narudzbina_ulica_id }}">{{ $ulica->naziv }}</option>
							@endif
						@endforeach
						</select>
					</div>
					<div> 
						<label>Broj</label>
						<input class="{{ $errors->first('broj') ? 'error' : '' }}" id="without-reg-number" class="form-control" name="broj" type="text" value="{{ htmlentities(Input::old('broj') ? Input::old('broj') : $broj) }}">
						<label class="error">{{ $errors->first('broj') }}</label>
					</div>
					@endif						
 							<div>
								<label>Adresa</label>
								<input class="{{ $errors->first('adresa') ? 'error' : '' }}" id="adresa" name="adresa" value="{{ htmlentities(Input::old('adresa') ? Input::old('adresa') : $adresa) }}" type="text">
								<label class="error">{{ $errors->first('adresa') }}</label>
							</div>
 
							<div>
						    	<label> Mesto/Grad</label>
								<input class="{{ $errors->first('mesto') ? 'error' : '' }}" type="text" name="mesto" value="{{ Input::old('mesto') ? Input::old('mesto') : $mesto }}">
								<label class="error">{{ $errors->first('mesto') }}</label>
							</div>
 
							<div>
								<label>Država</label>
								<select type="text" name="drzava_id">
									{{ AdminKupci::listDrzava(Input::old('drzava_id') ? Input::old('drzava_id') : $drzava_id) }}
								</select>
							</div>
							<div>
								<label>Naziv delatnosti</label>
								<input class="{{ $errors->first('delatnost_naziv') ? 'error' : '' }}" id="delatnost_naziv" name="delatnost_naziv" value="{{ Input::old('delatnost_naziv') ? Input::old('delatnost_naziv') : $delatnost_naziv }}" type="text">
							</div>

							<div>
								<label for="">Email</label>
								<input class="{{ $errors->first('mail') ? 'error' : '' }}" id="mail" autocomplete="off" name="mail" value="{{ Input::old('mail') ? Input::old('mail') : $mail }}" type="email">
								<label class="error">{{ $errors->first('mail') }}</label>					
							</div>
						
							<div>
								<label>Limit</label>
								<input class="{{ $errors->first('limit_p') ? 'error' : '' }}" id="limit_p" name="limit_p" value="{{ Input::old('limit_p') ? Input::old('limit_p') : $limit_p }}" type="text">
								<label class="error">{{ $errors->first('limit_p') }}</label>
							</div>

							<div>
								<label for="">Valuta</label>
								<select type="text" name="valuta_id">
									{{ AdminKupci::listValuta(Input::old('valuta_id') ? Input::old('valuta_id') : $valuta_id) }}
								</select>
							</div>

							<div>
								<label>Tip cene</label>
								<select type="text" name="tip_cene_id">
									{{ AdminKupci::listTipCene(Input::old('tip_cene_id') ? Input::old('tip_cene_id') : $tip_cene_id) }}
								</select>
							</div>

							<div>
								<label>Napomena</label>
								<textarea class="{{ $errors->first('napomena') ? 'error' : '' }}" id="napomena" name="napomena">{{ Input::old('napomena') ? Input::old('napomena') : $napomena }}</textarea>
								<label class="error">{{ $errors->first('napomena') }}</label>
							</div>
						</div> 
					 </div>


						<div class="column medium-12 large-4">
							<div class="flat-box">

							<div>
								<label>Kontakt Osoba</label>
								<input type="text" class="{{ $errors->first('kontakt_osoba') ? 'error' : '' }}" id="kontakt_osoba" name="kontakt_osoba" value="{{ Input::old('kontakt_osoba') ? Input::old('kontakt_osoba') : $kontakt_osoba }}">
								<label class="error">{{ $errors->first('kontakt_osoba') }}</label>
							 </div>

							<div>
								<label>Telefon</label>
								<input type="text" class="{{ $errors->first('telefon') ? 'error' : '' }}" id="telefon" name="telefon" value="{{ Input::old('telefon') ? Input::old('telefon') : $telefon }}">
								<label class="error">{{ $errors->first('telefon') }}</label>
							 </div>

							 <div>
								<label>Fax</label>
								<input class="{{ $errors->first('fax') ? 'error' : '' }}" id="fax" name="fax" value="{{ Input::old('fax') ? Input::old('fax') : $fax }}" type="text">
								<label class="error">{{ $errors->first('fax') }}</label>
							</div>

							 <div>
								<label>Šifra</label>
								<input id="sifra" class="{{ $errors->first('sifra') ? 'error' : '' }}" name="sifra" value="{{ Input::old('sifra') ? Input::old('sifra') : $sifra }}" type="text">
								<label class="error">{{ $errors->first('sifra') }}</label>
							</div>

							<div>
								<label>Pib</label>
								<input class="{{ $errors->first('pib') ? 'error' : '' }}" id="pib" name="pib" value="{{ Input::old('pib') ? Input::old('pib') : $pib }}" type="text">
								<label class="error">{{ $errors->first('pib') }}</label>
							</div>

							<div>
								<label>Račun</label>
								<input class="{{ $errors->first('racun') ? 'error' : '' }}" id="racun" autocomplete="off" name="racun" value="{{ Input::old('racun') ? Input::old('racun') : $racun }}" type="text">
								<label class="error">{{ $errors->first('racun') }}</label>
							</div>

							<div>
								<label>Šifra delatnosti</label>
								<input class="{{ $errors->first('delatnost_sifra') ? 'error' : '' }}" id="delatnost_sifra" name="delatnost_sifra" value="{{ Input::old('delatnost_sifra') ? Input::old('delatnost_sifra') : $delatnost_sifra }}" type="text">
								<label class="error">{{ $errors->first('delatnost_sifra') }}</label>
							</div>

							 <div>
								<label>Matični broj</label>
								<input class="{{ $errors->first('broj_maticni') ? 'error' : '' }}" id="broj_maticni" name="broj_maticni" value="{{ Input::old('broj_maticni') ? Input::old('broj_maticni') : $broj_maticni }}" type="text">
								<label class="error">{{ $errors->first('broj_maticni') }}</label>
							 </div>
 
							<div>
								<label>Broj registra</label>
								<input class="{{ $errors->first('broj_registra') ? 'error' : '' }}" id="broj_registra" name="broj_registra" value="{{ Input::old('broj_registra') ? Input::old('broj_registra') : $broj_registra }}" type="text">
								<label class="error">{{ $errors->first('broj_registra') }}</label>
							</div>

							<div>
								<label>Broj rešenja</label>
								<input class="{{ $errors->first('broj_resenja') ? 'error' : '' }}" id="broj_resenja" name="broj_resenja" value="{{ Input::old('broj_resenja') ? Input::old('broj_resenja') : $broj_resenja }}" type="text">
								<label class="error">{{ $errors->first('broj_resenja') }}</label>
							</div>
							 
							<div>
								<label>Rabat</label>
								<div class="row collapse">
									<div class="small-10 columns">
										<input class="{{ $errors->first('rabat') ? 'error' : '' }}" id="rabat" name="rabat" value="{{ Input::old('rabat') ? Input::old('rabat') : $rabat }}" type="text">
									</div>
									<div class="small-2 columns">
										<span class="postfix">%</span>
									</div>
								</div>
							</div>

							<div>
								<label for="">Login</label>
								<input id="login" name="login" value="{{ Input::old('login') ? Input::old('login') : $login }}" type="text">
							</div>

							<div>
								<label for="">Password</label>
 								<input type="password" class="" id="login-pass1" name="password" value="{{ htmlentities(Input::old('password') ? Input::old('password') : $password) }}">
								<input style="display: none" id="login-pass2" type="text" value="{{ htmlentities(Input::old('password') ? Input::old('password') : $password) }}">
								<a href="#" id="show-pass">Prikaži lozinku</a><br>
								<label class="error">{{ $errors->first('password') }}</label>
 							</div>
						</div> <!-- end of .column -->
					</div>

						<div class="column medium-12 large-4">
							<div class="flat-box">
							@if($partner_id != 0)
								<div class="">
									<label for="">Vrsta partnera</label>
									<div class="JSVrstePartnera m-checkbox-list">
										@foreach(DB::table('partner_vrsta')->get() as $row)
											<div class="m-checkbox-list__row">
												<input class="JS-vrsta" data-id="{{ $row->partner_vrsta_id }}" type="checkbox" {{ in_array($row->partner_vrsta_id, AdminKupci::checkVrstaPartner($partner_id)) ? 'checked' : '' }}> {{ $row->naziv }}
											</div>
										@endforeach
									</div>
								</div>
								@endif
						@if(Admin_model::check_admin())
						<div class="field-group column JSServiceSetings" {{ in_array(117, AdminKupci::checkVrstaPartner($partner_id)) ? '' : 'hidden' }}>
							<label for="">Podaci za import</label>

							<div>
								<label for="">Izaberite podrzan import</label>
								<select name="import_naziv_web">
									{{ AdminKupci::podrzaniImporti(Input::old('import_naziv_web') ? Input::old('import_naziv_web') : $import_naziv_web) }}
								</select>
							</div>
							<div>
								<label for="">Link za automatsko skidanje fajla</label>
								<input type="text" class="{{ $errors->first('auto_link') ? 'error' : '' }}" name="auto_link" value="{{ Input::old('auto_link') ? Input::old('auto_link') : $auto_link }}">
							</div>
							<div>
								<label for="">Puni automatski import</label>
								<select name="auto_import">
									{{ AdminSupport::selectCheck(Input::old('auto_import') ? Input::old('auto_import') : $auto_import) }}
								</select>
							</div>
							<div>
								<label for="">Automatsko azuriranje cena i kolicina</label>
								<select name="auto_import_short">
									{{ AdminSupport::selectCheck(Input::old('auto_import_short') ? Input::old('auto_import_short') : $auto_import_short) }}
								</select>
							</div>
							<div>
								<label for="">Preračunavanje cena (Kratki auto import)</label>
								@if($preracunavanje_cena == 1)
								<input type="checkbox" name="preracunavanje_cena" checked>
								@else
								<input type="checkbox" name="preracunavanje_cena">
								@endif
							</div>
							<div>
								<label for="">Korisnicko ime Servisa</label>
								<input type="text" class="{{ $errors->first('service_username') ? 'error' : '' }}" name="service_username" value="{{ Input::old('service_username') ? Input::old('service_username') : $service_username }}">
							</div>
							<div>
								<label for="">Lozinka za Servis</label>
								<input type="text" class="{{ $errors->first('service_password') ? 'error' : '' }}" name="service_password" value="{{ Input::old('service_password') ? Input::old('service_password') : $service_password }}">
							</div>
							<div>
								<label for="">Kurs</label>
								<input type="number" class="{{ $errors->first('zadnji_kurs') ? 'error' : '' }}" name="zadnji_kurs" value="{{ Input::old('zadnji_kurs') ? Input::old('zadnji_kurs') : $zadnji_kurs }}">
							</div>
						</div>
						@endif
						</div> <!-- end of .column -->
				 	</div>
					</div> <!-- end of .row -->

					<div class="row"> 
						<div class="column medium-12"> 
							<div class="btn-container center">
								<button type="submit" id="submit" class="btn btn-primary save-it-btn">Sačuvaj</button>
								<a href="{{AdminOptions::base_url()}}admin/kupci_partneri/partneri/" class="btn btn-secondary">Otkaži</a>
								@if($partner_id!=0)
								<a class="btn btn-danger JSbtn-delete" data-link="{{AdminOptions::base_url()}}admin/kupci_partneri/partneri/{{ $partner_id }}/0/delete">Obriši</a>
								@endif
							</div>
						</div>
					</div>
					 
				</form>
		</div>
 
	@if(in_array(AdminOptions::web_options(130),array(1,2)) && $partner_id>0)
<div class="row">
    <div class="columns medium-9">
	    <h2 class="title-med">Kartica Partnera</h2>
	    @if(AdminOptions::info_sys('calculus') || AdminOptions::info_sys('infograf') || AdminOptions::info_sys('logik'))
		    <form method="POST" action="{{AdminOptions::base_url()}}admin/partner/card-refresh/{{$partner_id}}">
		        <button onClick="this.form.submit(); this.disabled=true;" class="btn btn-primary">Osveži karticu</button>
		    </form>
		@endif
        <div class="table-responsive user-card-table"> 
            <table class="table table-condensed table-striped table-bordered table-hover"> 
                <thead> 
                    <tr> 
                        <th>Vrsta dokumenta</th>
                        <th>Datum dokumenta</th> 
                        <th>Datum dospeća</th>
                        <th>Duguje</th>
                        <th>Potražuje</th>
                    </tr>
                </thead>
                <tbody> 
                @foreach($kartica as $item)
                <tr> 
                    <td>{{$item->vrsta_dokumenta}}</td>
                    <td>{{date("d-m-Y",strtotime($item->datum_dokumenta))}}</td>
                    <td>{{date("d-m-Y",strtotime($item->datum_dospeca))}}</td>
                    <td>{{number_format($item->duguje,2)}}</td>
                    <td>{{number_format($item->potrazuje,2)}}</td>
                </tr>
                @endforeach
                </tbody>
            </table>
            {{ $kartica->links() }}
        </div>
    </div><br>
    <div class="columns medium-3 admin-partner"> 
        <div class="row">
            <span class="columns medium-7 no-padd">Ukupno dugovanje: </span>
            <span class="columns medium-5 no-padd">{{number_format($sumDuguje,2)}} </span>
        </div>
        <div class="row">
            <span class="columns medium-7 no-padd">Ukupno potraživanje: </span>
            <span class="columns medium-5 no-padd">{{number_format($sumPotrazuje,2)}} </span>
        </div>
        <div class="row">
            <span class="columns medium-7 no-padd">Saldo: </span>
            <span class="columns medium-5 no-padd">{{number_format($saldo,2)}} </span>
        </div>
    </div>
</div>
@endif

<input type="hidden" id="JSCheckPartnerConfirm" data-id="{{ Session::has('confirm_partner_id')?Session::get('confirm_partner_id'):0 }}">
</div>

 