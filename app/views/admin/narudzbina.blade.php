<section id="main-content">
@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
@endif
@if(Session::has('alert'))
	<script>
		alertify.error('{{ Session::get('alert') }}');
	</script>
@endif
	<div class="row m-subnav">
		<div class="large-2 medium-3 small-12 columns ">
			 <a class="m-subnav__link JS-nazad" href="#">
				<div class="m-subnav__link__icon">
					<i class="fa fa-arrow-left" aria-hidden="true"></i>
				</div>
				<div class="m-subnav__link__text">
					Nazad
				</div>
			</a>
		</div>
	</div>
	<div class="row"> 
	<form class="flat-box" method="POST" action="{{AdminOptions::base_url()}}admin/narudzbina">
		<input type="hidden" id="narudzbina_id" name="web_b2c_narudzbina_id" value="{{ $web_b2c_narudzbina_id }}">
		<div class="row pt">
			<div class="columns medium-6"> 
				<div class="row"> 
					<div class="customer-name medium-12 columns field-group">
						<label for="">Ime kupca</label>
						<select name="web_kupac_id" id="JSWebKupac" class="{{ $errors->first('web_kupac_id') ? 'error' : '' }}" {{($realizovano == 1 or $stornirano == 1) ? 'disabled' : ''}}>
							{{ AdminPartneri::kupciSelect(!is_null(Input::old('web_kupac_id')) ? Input::old('web_kupac_id') : $web_kupac_id) }}
						</select>
						@if($realizovano != 1 and $stornirano != 1)
		                <a href="#" id="JSKupacDetail" class="btn btn-primary btn-small tooltipz" aria-label="Izmeni"><i class="fa fa-pencil" aria-hidden="true"></i></a>
		                <a href="{{ AdminOptions::base_url() }}admin/kupci_partneri/kupci/0" class="btn btn-primary btn-small" target="_blank">Kreiraj kupca</a>
		                @endif
		            </div>
	            </div>

				 <?php $kupac = AdminKupci::kupacPodaci(!is_null(Input::old('web_kupac_id')) ? Input::old('web_kupac_id') : $web_kupac_id); ?>
				<div id="JSKupacAjaxContent" class="row">
					@include('admin/partials/ajax/narudzbina_kupac')
				</div>
				<br>
		 		<div class="row"> 
					<div class="columns medium-12 large-12 order-label" style="visibility: {{ ($errors->first() || $web_b2c_narudzbina_id == 0) ? 'hidden' : 'visible' }}">
							@if($prihvaceno == 0 and $realizovano == 0 and $stornirano == 0)			
								<a href="#" class="JS-prihvati btn btn-create">PRIHVATI</a>
								<a href="#" class="JS-storniraj btn btn-primary">STORNIRAJ</a>
								<a href="#" class="JS-delete btn btn-primary">OBRIŠI</a>
								<p class="order-status-p"> Status narudžbine: Nova</p>

							@elseif($prihvaceno != 0 and $realizovano == 0 and $stornirano == 0)
								<a href="#" class="JS-realizuj btn btn-create">REALIZUJ</a>
								<a href="#" class="JS-storniraj btn btn-primary">STORNIRAJ</a>
								<label for="">Poslato</label>
								<input id="poslato" name="posta_slanje_poslato" type="checkbox" {{ $posta_slanje_poslato == 1 ? 'checked' : '' }}>
								<p class="order-status-p">Status narudžbine: Prihvaćena</p>
							@elseif($realizovano != 0 and $stornirano == 0)
								<a href="#" class="JS-storniraj btn btn-primary">STORNIRAJ</a>								
								<p class="order-status-p">Status narudžbine: Realizovana</p>
							@elseif($stornirano != 0)
								<p class="order-status-p">Status narudžbine: Stornirana</p>
								<a href="#" class="JS-nestorniraj btn btn-primary">VRATI IZ STORNIRANIH</a>
							@endif
						</div>
				 </div>
		 	 </div>

			<div class="columns medium-6">
				<div class="row"> 
					<div class="field-group columns medium-6">
						<label for="">Broj dokumenta</label>
						<input name="broj_dokumenta" type="text" class="{{ $errors->first('broj_dokumenta') ? 'error' : '' }}" value="{{ !is_null(Input::old('broj_dokumenta')) ? Input::old('broj_dokumenta') : $broj_dokumenta }}" {{($realizovano == 1 or $stornirano == 1) ? 'disabled' : ''}}>
					</div>
 					<div class="field-group columns medium-6">
						<label for="">Datum</label>
						<input id="order-date" class="datum-val has-tooltip" name="datum_dokumenta" type="text" value="{{ Input::old('datum_dokumenta') ? Input::old('datum_dokumenta') : $datum_dokumenta }}" {{($realizovano == 1 or $stornirano == 1) ? 'disabled' : ''}}>
					</div>
				</div>

				<div class="row"> 
					<div class="field-group columns medium-6">
						<label for="">Način plaćanja</label>
						<select name="web_nacin_placanja_id" {{($realizovano == 1 or $stornirano == 1) ? 'disabled' : ''}}>
							{{ AdminPartneri::nacinPlacanja(!is_null(Input::old('web_nacin_placanja_id')) ? Input::old('web_nacin_placanja_id') : $web_nacin_placanja_id) }}
						</select>
					</div>
 					<div class="field-group columns medium-6">
						<label for="">Način isporuke</label>
						<select name="web_nacin_isporuke_id" {{($realizovano == 1 or $stornirano == 1) ? 'disabled' : ''}}>
							{{ AdminPartneri::nacinIsporuke(!is_null(Input::old('web_nacin_isporuke_id')) ? Input::old('web_nacin_isporuke_id') : $web_nacin_isporuke_id) }}
						</select>
					</div>
				</div>

				<div class="row"> 
					<div class="field-group columns medium-6">
						<label for="">Kurirska služba</label>
						<select name="posta_slanje_id" {{($realizovano == 1 or $stornirano == 1) ? 'disabled' : ''}}>
							{{ AdminPartneri::kurirskaSluzba(!is_null(Input::old('posta_slanje_id')) ? Input::old('posta_slanje_id') : $posta_slanje_id) }}
						</select>
					</div>
 					<div class="field-group columns medium-6">
						<label for="">Dodeli status</label>
						<select  name="narudzbina_status_id" {{($realizovano == 1 or $stornirano == 1) ? 'disabled' : ''}}>
							{{ AdminPartneri::statusnarudzbine(!is_null(Input::old('narudzbina_status_id')) ? Input::old('narudzbina_status_id') : $narudzbina_status_id) }}
						</select>
					</div>
				</div>

				<div class="row"> 	
					<div class="field-group columns medium-6">
						<label for="">IP adresa</label>
						<input name="ip_adresa" type="text" value="{{ Input::old('ip_adresa') ? Input::old('ip_adresa') : $ip_adresa }}" {{($realizovano == 1 or $stornirano == 1) ? 'disabled' : ''}}>
					</div>	
	 				<div class="field-group columns medium-6">
						<label for="">Broj pošiljke</label>
						<input name="posta_slanje_broj_posiljke" type="text" class="{{ $errors->first('posta_slanje_broj_posiljke') ? 'error' : '' }}" value="{{ !is_null(Input::old('posta_slanje_broj_posiljke')) ? Input::old('posta_slanje_broj_posiljke') : $posta_slanje_broj_posiljke }}" {{($realizovano == 1 or $stornirano == 1) ? 'disabled' : ''}}>
					</div>
				</div>

				<div class="row"> 	
					<div class="field-group columns medium-12">
						<label for="">Napomena</label>
						<textarea name="napomena" rows="2">{{ !is_null(Input::old('napomena')) ? Input::old('napomena') : $napomena }}</textarea>
					</div>
				</div>
			</div>
 			 
		  	<div class="columns medium-12 large-12 text-center">
		  		<br>
				<div class="btn-container"> 
					<button type="submit" class="btn btn-primary save-it-btn">Sačuvaj</button>
				 
				@if(!$errors->first() AND $web_b2c_narudzbina_id != 0)
					<a target="_blank" href="/admin/pdf/{{ $web_b2c_narudzbina_id }}" class="btn btn-primary">PDF</a>
					
					@if($realizovano == 1 and $stornirano == 0)
						<a target="_blank" href="/admin/pdf_racun/{{ $web_b2c_narudzbina_id }}" class="btn btn-primary">Račun</a>
					@elseif($realizovano == 0 and $stornirano == 0 and $prihvaceno == 1)
						<a target="_blank" href="/admin/pdf_ponuda/{{ $web_b2c_narudzbina_id }}" class="btn btn-primary">Ponuda</a>
						<a target="_blank" href="/admin/pdf_predracun/{{ $web_b2c_narudzbina_id }}" class="btn btn-primary">Predračun</a>
					@elseif($stornirano == 1)
						<a target="_blank" href="/admin/pdf_racun/{{ $web_b2c_narudzbina_id }}" class="btn btn-primary">Račun</a>
					@else
						<a target="_blank" href="/admin/pdf_ponuda/{{ $web_b2c_narudzbina_id }}" class="btn btn-primary">Ponuda</a>
						<a target="_blank" href="/admin/pdf_predracun/{{ $web_b2c_narudzbina_id }}" class="btn btn-primary">Predračun</a>
					@endif
 				@endif
				</div>
			</div>
		 </div> <!-- end of .row -->
	</form>
	</div>
	@if(!$errors->first() AND $web_b2c_narudzbina_id != 0)
	<div class="row"> 
	<div class="flat-box">
		<div class="row pt">
			
			<div class="columns medium-12 large-12">
				<div class="add-to-order">
					<label> Dodaj artikal narudžbini</label>
				</div>
				<div class="articles">
					<input class="" autocomplete="off" data-timer="" type="text" id="JSsearch_porudzbine" data-id="" placeholder="Unesite id ili ime artikla ili prozvođača ili krajnju grupu" {{($realizovano == 1 or $stornirano == 1) ? 'disabled' : ''}} />
					<div id="search_content"></div>
				</div>
			 
				<div class="items-order">
					<table class="table table_narudzbina">
						 <thead>
							<tr>
								<th class="tab-width-0">ROBA ID</th>
								<th class="tab-width-0">KOL</th>
								<th class="tab-width-3">NAZIV</th>
								<th class="hide-for-small-only tab-width-0">LAGER</th>
								<th class="" data-coll="cena">CENA</th>
								<th class="" data-coll="dobavljac">DOBAVLJAC</th>
								@if(AdminNarudzbine::api_posta(3))
								<th class="">POŠILJALAC</th>
								<th class="">KURIR. SLUŽBA</th>
								<th class="">POŠTA STATUS</th>
								<th class="">POŠTA ZAHTEV</th>
								@endif
								@if($realizovano != 1 and $stornirano != 1)
								<th class="tab-width-0"></th>
								@endif
								<th class="tab-width-0"></th>
							</tr>
						</thead>
						@foreach($narudzbina_stavke as $row)
						<tr>
							<td>
								&nbsp;{{ $row->roba_id}}
							</td>
							<td>
								<?php AdminOptions::vodjenje_lagera()==1 ? $max=AdminSupport::lager($row->roba_id)+AdminNarudzbine::kolicina($web_b2c_narudzbina_id,$row->roba_id) : $max=1000; ?>
								<input class="kolicina ammount-input" data-stavka-id="{{ $row->web_b2c_narudzbina_stavka_id }}" type="number" min="1" max="{{ $max }}" value="{{ round($row->kolicina) }}" data-old-kolicina="{{ round($row->kolicina) }}" {{($realizovano == 1 or $stornirano == 1) ? 'disabled' : ''}}>	
							</td>
							<td class="narudzbina-td-naziv">
								{{ AdminArticles::find($row->roba_id, 'naziv') }}
					            {{ AdminOsobine::getOsobineStr($row->roba_id,$row->osobina_vrednost_ids) }}
					        </td>
							<td class="JSLager hide-for-small-only">
								{{ AdminSupport::lager($row->roba_id) }}
							</td>	

						    @if($realizovano != 1 and $stornirano != 1)
						    <td class="cena">
								<span class="JSCenaVrednost">
							 		{{ number_format($row->jm_cena, 2, '.', '') }}
								</span>
								<span class="JSEditCenaBtnSpan">
									<input type="text" data-stavka-id="{{ $row->web_b2c_narudzbina_stavka_id }}" class="JSCenaInput" value="{{ number_format($row->jm_cena, 2, '.', '') }} " >
									<i class="fa fa-pencil JSEditCenaBtn" aria-hidden="true"></i>
 								</span>
							</td>
							@else
							<td>
								
							</td>
							@endif
							@if(!is_null(AdminNarudzbine::dobavljac($row->roba_id)))
							<td class="dobavljac">
								{{ AdminNarudzbine::dobavljac($row->roba_id)->naziv }}
							</td>
							@else
							<td>
								
							</td>
							@endif

							@if(AdminNarudzbine::api_posta(3))
								<td>{{ AdminPartneri::partner($row->posiljalac_id)->naziv }}</td>
								<?php $posta_slanje_naziv = AdminSupport::find_kurirska_sluzba($row->posta_slanje_id, 'naziv'); ?>
								<td>{{ !is_null($posta_slanje_naziv) ? $posta_slanje_naziv : AdminSupport::find_kurirska_sluzba($posta_slanje_id, 'naziv') }}</td>
								@if(!is_null($row->posta_status_id))
									<td class="tab-widtd-2">{{AdminSupport::posta_status($row->posta_status_id)->name}}</td>
								@else
									<td></td>
								@endif
								<td>{{ $row->posta_zahtev == 1 ? '<span style="color: red;">POSLATO</span>' : '' }}</td>
								<td>
									@if($row->posta_zahtev == 0)
										<a class="mail-btn" href="{{AdminOptions::base_url()}}admin/narudzbina-stavka/{{$row->web_b2c_narudzbina_stavka_id}}">POŠTA</a>
								    @endif
								</td>
							@endif
						 
							@if($realizovano != 1 and $stornirano != 1)
							<td class="tab-width-0 text-center">
								<button data-stavka-id="{{ $row->web_b2c_narudzbina_stavka_id }}" class="JSDeleteStavka name-ul__li__remove button-option tooltipz-left" aria-label="Obriši"><i class="fa fa-times" aria-hidden="true"></i></button>
							</td>
							@endif
						</tr>
						@endforeach
 
					</table> 

					<table class="table">
					   @if(AdminOptions::web_options(133) == 1 AND AdminOptions::web_options(150) == 0 AND $troskovi_isporuke>0)
						<tr>
							<td colspan="12" class="text-right">Cena artikala: </td>
							<td colspan="1" class="text-right" id="cena-artikla">{{ number_format(floatval($ukupna_cena),2,'.','') }}</td> 
						</tr>
						<tr>
							<td colspan="12" class="text-right">Troškovi isporuke: </td>
							<td colspan="1" class="text-right" id="troskovi-isporuke">{{ $troskovi_isporuke }}</td> 
						</tr>
						
						<tr>
							<td colspan="12" class="text-right"><b>Ukupno za uplatu: </b></td> 
							<td colspan="1" class="text-right" id="ukupna-cena"><b> {{ number_format(floatval($ukupna_cena+$troskovi_isporuke),2,'.','') }}</b></td>
						</tr>
						@elseif(AdminOptions::web_options(133) == 0 AND AdminOptions::web_options(150) == 1 AND $cena_dostave>0 AND $cena_do > $ukupna_cena AND $cena_do >0)	
						<tr>
							<td colspan="12" class="text-right">Cena artikala: </td>
							<td colspan="1" class="text-right" id="cena-artikla">{{ number_format(floatval($ukupna_cena),2,'.','') }}</td> 
						</tr>
						<tr>
							<td colspan="12" class="text-right">Troškovi isporuke: </td>
							<td colspan="1" class="text-right" id="troskovi-isporuke">{{ $cena_dostave }}</td> 
						</tr>
						
						<tr>
							<td colspan="12" class="text-right"><b>Ukupno za uplatu: </b></td> 
							<td colspan="1" class="text-right" id="ukupna-cena"><b> {{ number_format(floatval($ukupna_cena+$cena_dostave),2,'.','') }}</b></td>
						</tr>
			            @else
						<tr>
							<td colspan="12" class="text-right"><b>Ukupno za uplatu: </b></td> 
							<td colspan="1" class="text-right" id="ukupna-cena"><b> {{ number_format(floatval($ukupna_cena),2,'.','') }}</b></td>
						</tr>
			            @endif
					</table> 
				</div>
			</div>

		</div> <!-- end of .row -->
	</div>
	</div>
	@if(AdminNarudzbine::api_posta(3))
	<div class="row">
		<div class="flat-box">
			<label>Pošte slanja</label>
			<table>
			@foreach($narudzbina_poste_slanja as $posta_slanje)
			<tr>
				<td>&nbsp; {{ $posta_slanje->naziv }}</td>
				<td><a href="{{AdminOptions::base_url()}}admin/posta-posalji/{{ $web_b2c_narudzbina_id }}/{{ $posta_slanje->posta_slanje_id }}" class="mail-btn">POŠALJI</a></td>
				<td><a href="{{AdminOptions::base_url()}}admin/posta-resetuj/{{ $web_b2c_narudzbina_id }}/{{ $posta_slanje->posta_slanje_id }}" class="mail-btn">RESETUJ</a></td>
			</tr>
			@endforeach
			</table>
		</div>
	</div>
	@endif
	@endif
	
	</section>
