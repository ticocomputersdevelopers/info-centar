<?php $input_futer_sekcija_tip_id = (!is_null(Input::old('futer_sekcija_tip_id')) ? intval(Input::old('futer_sekcija_tip_id')) : $item->futer_sekcija_tip_id); ?>
<section id="main-content" class="banners">
@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
@endif
	<section class="medium-12 large-3 columns">
		<div class="flat-box">
			
			<!-- BANNER LIST -->
			<h3 class="text-center h3-margin">Sekcije</h3>
			<ul class="banner-list JSListFooterSections" data-table="2">
				<li id="0" class="new-banner new-elem">
					<a href="{{AdminOptions::base_url()}}admin/futer/0/1">Nova sekcija</a>
					<div class="icon"><i class="fa fa-plus" aria-hidden="true"></i></div>
				</li>
				@foreach($sections as $section)
				<li id="{{$section->futer_sekcija_id}}" class="ui-state-default @if($section->futer_sekcija_id == $id) active @endif" id="{{$section->futer_sekcija_id}}">
					<a href="{{AdminOptions::base_url()}}admin/futer/{{$section->futer_sekcija_id}}/1">{{AdminSupport::footer_section_name($section->futer_sekcija_id)}}</a>
					<a class="icon tooltipz JSbtn-delete" aria-label="Obriši" data-link="{{AdminOptions::base_url()}}admin/futer-delete/{{$section->futer_sekcija_id}}"><i class="fa fa-times" aria-hidden="true"></i></a>
				</li>
				@endforeach
			</ul>
		</div> <!-- end of .flat-box -->
	 
	</section> <!-- end of .medium-3 .columns -->
	
	<!-- BANNER EDIT -->
	<section class="page-edit medium-12 large-9 columns">
		<div class="flat-box"> 
		<form action="{{AdminOptions::base_url()}}admin/futer-edit" name="banner_upload" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="logo-right">
			<h3 class="text-center h3-margin">Sekcija</h3>

			@if(count($jezici) > 1)
			<div class="row">
				<div class="languages">
					<ul>	
					@foreach($jezici as $jezik)
						<li><a class="{{ $jezik_id == $jezik->jezik_id ? 'active' : '' }} btn-small btn btn-secondary" href="{{AdminOptions::base_url()}}admin/futer/{{ $id }}/{{ $jezik->jezik_id }}">{{ $jezik->naziv }}</a></li>
					@endforeach
					</ul>
				</div>
			</div>
			@endif 
			<section class="banner-edit-top row">
				<div class="medium-4 columns">
					<label>Naslov</label>
					<input id="naslov" value="{{ Input::old('naslov') ? Input::old('naslov') : trim($lang_data->naslov) }}" type="text" name="naslov" />
					<div class="error">{{ $errors->first('naslov') ? $errors->first('naslov') : '' }}</div>
				</div>

				<div class="medium-4 columns">
					<label>Tip</label>
					<select name="futer_sekcija_tip_id" id="JSFuterSekcijaTip">
						@foreach(DB::table('futer_sekcija_tip')->where('aktivan',1)->get() as $footer_section_type)
						<option value="{{$footer_section_type->futer_sekcija_tip_id}}" 
							{{ ($footer_section_type->futer_sekcija_tip_id == intval(Input::old('futer_sekcija_tip_id')) ? 
								'selected' : 
								((is_null(Input::old('futer_sekcija_tip_id')) AND 
								$item->futer_sekcija_tip_id == $footer_section_type->futer_sekcija_tip_id) ? 'selected' : '')) }}
							>{{ $footer_section_type->naziv }}</option>
						@endforeach
					</select>
				</div>
				<div class="medium-2 columns">
					<label>Aktivna sekcija</label>
					<select name="aktivan">
						<option value="1">DA</option>
						<option value="0" {{ (Input::old('aktivan') == '0') ? 'selected' : (($item->aktivan == 0) ? 'selected' : '') }}>NE</option>
					</select>
				</div>

				<div id="JSFooterSectionContent" class="medium-12 columns" {{ in_array($input_futer_sekcija_tip_id,array(1,2)) ? '' : 'hidden'}}>
					<label>Sadržaj</label>
					<textarea class="special-textareas" name="sadrzaj">{{ Input::old('sadrzaj') ? Input::old('sadrzaj') : trim($lang_data->sadrzaj) }}</textarea>
				</div>
			</section>

			<section id="JSFooterSectionLinks" class="banner-edit-top row" {{ ($input_futer_sekcija_tip_id == 3) ? '' : 'hidden' }}>
				<div class="medium-4 columns">
					<select name="web_b2c_seo_id">
						@foreach($pages as $page)
						<option value="{{ $page->web_b2c_seo_id }}">{{ $page->title }}</option>
						@endforeach
					</select>
				</div>

				<div class="medium-4 columns footer-page">
					<ul>
						@foreach($linked_pages as $linked_page)
						<li data-id="{{ $linked_page->web_b2c_seo_id }}">{{ $linked_page->title }} 
							<a class="right tooltipz" aria-label="Obriši" href="{{AdminOptions::base_url()}}admin/futer-page-delete/{{$item->futer_sekcija_id}}/{{$linked_page->web_b2c_seo_id}}"><i class="fa fa-times" aria-hidden="true"></i>
							</a>
						</li>
						@endforeach
					</ul>
				</div>
			</section>

			<!-- BANNER EDIT TOP -->
			<section id="JSFooterSectionImage" class="banner-edit-top row" {{ ($input_futer_sekcija_tip_id == 1) ? '' : 'hidden' }}>
				<!-- BANNER PREVIEW -->
				<div class="medium-12 columns">
					<img class="banner-preview" src="{{AdminOptions::base_url()}}{{$item->slika}}" alt="{{$lang_data->naslov}}" />
				</div>

				<div class="medium-6 columns">
					<label>&nbsp;</label>
					<div class="banner-upload-area clearfix">  
						<span class="banner-upload has-tooltip"> 
							<input type="file" name="slika"><span>Dodaj sliku</span>
						</span>
					</div>
					<div class="error">{{ $errors->first('slika') ? ($errors->first('slika') == 'Niste popunili polje.' ? 'Niste izabrali sliku.' : $errors->first('slika')) : '' }}</div>
				</div>
				
				<div class="medium-6 columns">
					<label>Link slike</label>
					<input id="link" value="{{ Input::old('link') ? Input::old('link') : trim($item->link) }}" type="text" name="link">
					<div class="error">{{ $errors->first('link') ? $errors->first('link') : '' }}</div>
				</div>
			</section>
			

			<!-- BANNER DISPLAY PAGES -->			
			<div class="banner-display-pages">
				<input type="hidden" name="futer_sekcija_id" value="{{$item->futer_sekcija_id}}" />
				<input type="hidden" name="jezik_id" value="{{$jezik_id}}" />
				<div class="btn-container center">
					<button class="btn btn-primary save-it-btn">Sačuvaj</button>
				</div>
			</div>
		</form>
		</div>
	</section>
</section>

 