<div id="main-content" >
@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
@endif
	@include('admin/partials/tabs')
	<div class="row">
		<div class="large-3 medium-3 small-12 columns ">
			<div class="flat-box">
				<h3 class="title-med">Izaberi status</h3>
				<div class="manufacturer"> 
					<select class="JSeditSupport search_select">
						<option value="{{ AdminOptions::base_url() }}admin/status_narudzbine">Dodaj novi</option>
	   					@foreach(AdminSupport::getFlagStatus(true) as $row)
							<option value="{{ AdminOptions::base_url() }}admin/status_narudzbine/{{ $row->narudzbina_status_id }}" @if($row->narudzbina_status_id == $narudzbina_status_id) {{ 'selected' }} @endif>{{ $row->naziv }} </option>
						@endforeach -->
					</select>
				</div>
			</div>
		</div>

	<section class="small-12 medium-12 large-5 large-centered columns">
		<div class="flat-box">

			<h1 class="title-med">{{ $title }} {{ $narudzbina_status_id == 1 ? '(Podrazumevani)' : '' }}</h1>
			<!-- <h1 id="info"></h1> -->

			<form method="POST" action="{{ AdminOptions::base_url() }}admin/status_narudzbine_edit" enctype="multipart/form-data">
				  <input type="hidden" name="narudzbina_status_id" value="{{ $narudzbina_status_id }}"> 
					<div class="row">
						<div class="columns medium-6 field-group{{ $errors->first('naziv') ? ' error' : '' }}">
							<label for="naziv">Naziv statusa</label>
							<input type="text" name="naziv" value="{{ htmlentities(Input::old('naziv') ? Input::old('naziv') : $naziv) }}" autofocus="autofocus"> 
						</div>
						<div class="columns medium-6 field-group">
							<label>Aktivno</label>
							<select name="selected">
								{{ AdminSupport::selectCheck(Input::old('selected') ? Input::old('selected') : $selected) }}
							</select>
						</div>
				 	</div>
				 	<div class="row">
						<div class="btn-container center">
							<button type="submit" class="btn btn-primary save-it-btn">Sačuvaj</button>
							@if($narudzbina_status_id != 0)	
							<button class="btn btn-danger JSbtn-delete" data-link="{{ AdminOptions::base_url() }}admin/status_narudzbine_delete/{{ $narudzbina_status_id }}">Obriši</button>
							@endif
						</div>
					</div>
				</form>
		 </div> <!-- end of .flat-box -->
			
		</section>
	</div> <!-- end of .row -->
</div>
	