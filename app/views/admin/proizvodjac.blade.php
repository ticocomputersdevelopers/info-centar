<div id="main-content" >
@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
@endif
	@include('admin/partials/tabs')
	<div class="row">
		<div class="large-3 medium-3 small-12 columns ">
			<div class="flat-box">
				<h3 class="title-med">Izaberi proizvođača</h3>
				<div class="manufacturer"> 
					<select class="admin-select JSeditSupport search_select">
						<option value="{{ AdminOptions::base_url() }}admin/proizvodjac/0">Dodaj novi</option>
						@foreach(AdminSupport::getProizvodjaci() as $row)
							<option value="{{ AdminOptions::base_url() }}admin/proizvodjac/{{ $row->proizvodjac_id }}" @if($row->proizvodjac_id == $proizvodjac_id) {{ 'selected' }} @endif >{{ $row->naziv }}</option>
						@endforeach
					</select>
				</div>
			</div>
	 	<!-- ====================== -->
			<div class="text-center">
				<a href="#" class="video-manual" data-reveal-id="manufacturer-manual">Uputstvo <i class="fa fa-film"></i></a>
			   	<div id="manufacturer-manual" class="reveal-modal medium" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
					<div class="video-manual-container"> 
					<p><span class="video-manual-title">Proizvođači</span></p>
					<iframe src="https://player.vimeo.com/video/271252069" width="840" height="425" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
					</div>
					<a class="close-reveal-modal" aria-label="Close">&#215;</a>
				</div>
			</div>
		<!-- ====================== -->
		</div>

		<section class="small-12 medium-12 large-5 large-centered columns">
			<div class="flat-box">

				<h1 class="title-med">{{ $title }}</h1>
				<!-- <h1 id="info"></h1> -->

				<form method="POST" action="{{ AdminOptions::base_url() }}admin/proizvodjac-edit" enctype="multipart/form-data">
					    <input type="hidden" name="proizvodjac_id" value="{{ $proizvodjac_id }}"> 
						<input type="hidden" name="jezik_id" value="{{ $jezik_id }}"> 

						<div class="row">
							<div class=" columns medium-12 field-group{{ $errors->first('naziv') ? ' error' : '' }}">
								<label for="naziv">Naziv proizvođača</label>
								<input type="text" name="naziv" data-id="" value="{{ htmlentities(Input::old('naziv') ? Input::old('naziv') : $naziv) }}" autofocus="autofocus"> 
							</div>
						</div>
						<div class="row">
							<div class="columns medium-6 field-group">
								<label for="brend_prikazi">Brend</label>
								<select name="brend_prikazi">
									@if(Input::old('brend_prikazi') ? Input::old('brend_prikazi') : $brend_prikazi)
									<option value="1" selected>DA</option>
									<option value="0" >NE</option>
									@else
									<option value="1" >DA</option>
									<option value="0" selected>NE</option>
									@endif
								</select>
							</div>
							<div class="columns medium-6 field-group{{ $errors->first('rbr') ? ' error' : '' }}">
								<label for="rbr">Redni broj</label>
								<input type="text" name="rbr" data-id="" value="{{ Input::old('rbr') ? Input::old('rbr') : $rbr }}"> 
							</div>
						</div>

						<div class="row"> 
							<div class="columns medium-12 manufacturers-img"> 
								<img <?php if(isset($slika)){ ?> src="{{ AdminOptions::base_url().$slika }}" <?php } ?>  />
								<div class="field-group">
									<label>Preporučene dimenzije slike: 140x60</label>
									<div class="bg-image-file"> 
										<input type="file" name="slika">
									</div>
									@if(isset($slika))
									<input type="checkbox" name="slika_delete">Obriši sliku
									@endif
								</div>
							</div>
						</div>

						@if(count($jezici) > 1)
						<div class="row"> 
							<div class="languages">
								<ul>	
								@foreach($jezici as $jezik)
									<li><a class="{{ $jezik_id == $jezik->jezik_id ? 'active' : '' }} btn-small btn btn-secondary" href="{{AdminOptions::base_url()}}admin/proizvodjac/{{$proizvodjac_id}}/{{ $jezik->jezik_id }}">{{ $jezik->naziv }}</a></li>
								@endforeach
								</ul>
							</div>
						</div>	
						@endif

						<div class="row"> 
	                        <div class="columns medium-12 field-group{{ $errors->first('seo_title') ? ' error' : '' }}">
	                            <label>Naslov (title) do 60 karaktera</label>
								<input type="text" name="seo_title" data-id="" value="{{ htmlentities(Input::old('seo_title') ? Input::old('seo_title') : $seo_title) }}"> 
	                        </div>
	                    </div>
 
	                    <div class="row"> 
							<div class="columns medium-12 field-group{{ $errors->first('description') ? ' error' : '' }}">
								<label id="seo_opis_label" for="description">Opis (description) do 158 karaktera</label>
								<textarea id="seo_opis" name="description">{{ Input::old('description') ? Input::old('description') : $description }}</textarea>
							</div>
						</div>

						<div class="row"> 
							<div class="columns medium-12 field-group{{ $errors->first('keywords') ? ' error' : '' }}">
								<label for="keywords">Ključne reči (keywords)</label>
								<input type="text" name="keywords" value="{{ Input::old('keywords') ? Input::old('keywords') : $keywords }}">  
							</div>
						</div>
 
						<div class="row"> 
							<div class="btn-container center">
								<button type="submit" class="btn btn-primary save-it-btn">Sačuvaj</button>
								@if($proizvodjac_id != 0)
								<a class="btn btn-danger JSbtn-delete" data-link="{{ AdminOptions::base_url() }}admin/proizvodjac-delete/{{ $proizvodjac_id }}" href="#">Obriši</a>
								@endif
							</div>
						</div>
				 </form>
		 	</div>
 	 </section>
 	</div> <!-- end of .flat-box -->
 </div> <!-- end of .row -->
 