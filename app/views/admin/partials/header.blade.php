<header id="admin-header" class="row">
	<!-- WIDTH TOGGLE BUTTON -->
	<div class="header-width-toggle"><i class="fa fa-chevron-left" aria-hidden="true"></i></div>

	<div class="logo-wrapper">
		<a class="logo" href="{{ AdminOptions::base_url()}}admin" title="Selltico">
			<img src="{{ AdminOptions::base_url()}}/images/admin/logo-selltico-white.png" alt="Selltico">
		</a>
	</div>
	@if(Admin_model::check_admin())
	<div class="">
		<p class="versions logged-user"> 
		{{ AdminOptions::versions() }}
		</p>
	</div>
	@endif	
	<!-- <span class="main-menu-toggler"></span> -->
	<nav class="main-menu">
		<div class="logout-wrapper">
			<div class="menu-item">
				<a href="{{ AdminOptions::base_url()}}" target="_blank" class="menu-item__link">
					<div class="menu-item__icon">
						<img class="shop-logo" src="{{ AdminOptions::base_url()}}images/admin/wbp-logo.png" alt="{{Options::company_name()}}" />
						<span class="menu-tooltip">VIDI {{ AdminOptions::is_shop() ? 'SHOP' : 'SAJT' }}</span>
					</div>
					<div class="menu-item__text">VIDI {{ AdminOptions::is_shop() ? 'SHOP' : 'SAJT' }}</div>
				</a>
			</div>
		</div>
		<ul class="clearfix">
			<li class="menu-item">
				<div class="">
					<div class="logged-user">Ulogovan: {{ Admin_model::get_admin() }}</div>
				</div>
			</li>
			@if(AdminOptions::gnrl_options(3013))
			<li class="menu-item">
				<a href="{{ AdminOptions::base_url()}}admin/podesavanje-naloga" class="menu-item__link">
					<div class="menu-item__icon">
						<i class="fa fa-cogs" aria-hidden="true"></i>
						<span class="menu-tooltip">Podešavanje naloga</span>
					</div>
					<div class="menu-item__text">Podešavanje naloga</div>
				</a>
			</li>
			@endif
			@if(Admin_model::check_admin(array(600,800)) && AdminOptions::is_shop())
			<li class="menu-item">
				<a href="{{ AdminOptions::base_url()}}admin" class="menu-item__link  @if($strana == 'pocetna') active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-ticket" aria-hidden="true"></i>
						@if( AdminAnalitika::getNew() > 0) <span class="notify-num-small">{{ AdminAnalitika::getNew() }}</span> @endif
						<span class="menu-tooltip">Narudžbine</span></span>
					</div>
					<div class="menu-item__text">Narudžbine
						@if( AdminAnalitika::getNew() > 0) <span class="notify-num">{{ AdminAnalitika::getNew() }}</span> @endif
					</div>
				</a>
			</li>
			@endif
			@if(Admin_model::check_admin(array(200)) && AdminOptions::is_shop())
			<li class="menu-item">
				<a href="{{ AdminOptions::base_url()}}admin/artikli/0/0/0/0/0/0/0/nn-nn-nn-nn-nn-nn-nn-nn-1-nn-nn-nn-nn-nn/0/0/nn-nn" class="menu-item__link  @if($strana == 'artikli') active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-cubes" aria-hidden="true"></i>
						<span class="menu-tooltip">Artikli</span>
					</div>
					<div class="menu-item__text">Artikli</div>
				</a>
			</li>
			@endif
			@if(Admin_model::check_admin(array(1600,1800,2000)) && AdminOptions::is_shop())
			<li class="menu-item">
				<a href="{{ AdminOptions::base_url()}}admin/kupci_partneri/kupci" class="menu-item__link @if(in_array($strana,array('kupci','partneri'))) active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-users" aria-hidden="true"></i>
						<span class="menu-tooltip">Kupci i partneri</span>
					</div>
					<div class="menu-item__text">Kupci i partneri</div>
				</a>
			</li>
			@endif
			@if(Admin_model::check_admin(array(9000)) && AdminOptions::is_shop())
			<li class="menu-item">
				<a href="{{ AdminOptions::base_url()}}admin/analitika" class="menu-item__link  @if($strana == 'analitika') active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-line-chart" aria-hidden="true"></i>
						<span class="menu-tooltip">Analitika</span>
					</div>
					<div class="menu-item__text">Analitika</div>
				</a>
			</li>
			@endif
			@if(Admin_model::check_admin(array(7000)))
			<li class="menu-item">
				<a href="{{ AdminOptions::base_url()}}admin/stranice/0" class="menu-item__link  @if($strana == 'stranice') active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-file-text" aria-hidden="true"></i>
						<span class="menu-tooltip">Stranice</span>
					</div>
					<div class="menu-item__text">Stranice</div>
				</a>
			</li>
			@endif
		 	@if(Admin_model::check_admin(array(7000)))
			<li class="menu-item">
				<a href="{{ AdminOptions::base_url()}}admin/futer/0" class="menu-item__link  @if($strana == 'footer') active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-window-maximize" style="transform: rotate(180deg);"></i>
						<span class="menu-tooltip">Futer</span>
					</div>
					<div class="menu-item__text">Futer</div>
				</a>
			</li>
			@endif
			@if(Admin_model::check_admin(array(7100)))
			<li class="menu-item">
				<a href="{{ AdminOptions::base_url()}}admin/baneri-slajdovi/0/1" class="menu-item__link  @if($strana == 'baneri-slajderi') active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-picture-o" aria-hidden="true"></i>
						<span class="menu-tooltip">Baneri i slajder</span>
					</div>
					<div class="menu-item__text">Baneri i slajder</div>
				</a>
			</li>
			@endif
			@if(Admin_model::check_admin(array(6600)))
			<li class="menu-item">
				<a href="{{ AdminOptions::base_url()}}admin/vesti" class="menu-item__link  @if($strana == 'vesti') active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-newspaper-o" aria-hidden="true"></i>
						<span class="menu-tooltip">Vesti</span>
					</div>
					<div class="menu-item__text">Vesti</div>
				</a>
			</li>
			@endif
			@if(Admin_model::check_admin(array(600,800)) && AdminOptions::is_shop())
			<li class="menu-item">
				<a href="{{ AdminOptions::base_url()}}admin/komentari" class="menu-item__link  @if($strana == 'komentari') active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-comment" aria-hidden="true"></i>
						@if( AdminSupport::countNewComments() > 0) <span class="notify-num-small">{{ AdminSupport::countNewComments() }}</span> @endif
						<span class="menu-tooltip">Komentari</span></span>
					</div>
					<div class="menu-item__text">Komentari
						@if( AdminSupport::countNewComments() > 0) <span class="notify-num">{{ AdminSupport::countNewComments() }}</span> @endif
					</div>
				</a>
			</li>
			@endif

			@if(Admin_model::check_admin(array(1000)))
			<li class="menu-item">
				<a href="{{ AdminOptions::base_url()}}admin/kontakt-podaci" class="menu-item__link  @if($strana == 'kontakt') active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-envelope" aria-hidden="true"></i>
						<span class="menu-tooltip">Kontakt podaci</span>
					</div>
					<div class="menu-item__text">Kontakt podaci</div>
				</a>
			</li>
			@endif
			@if(Admin_model::check_admin(array(400)) && AdminOptions::is_shop())
			<li class="menu-item">
				<a href="{{ AdminOptions::base_url()}}admin/web_import/0/0/0/nn-nn-nn-nn-nn-nn-nn-nn-nn-nn-nn-nn/0/nn-nn" class="menu-item__link  @if($strana == 'web_import') active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-cloud-upload" aria-hidden="true"></i>
						<span class="menu-tooltip">Web import</span>
					</div>
					<div class="menu-item__text">Web import</div>
				</a>
			</li>
			@endif
			@if(Admin_model::check_admin(array(5600,5800,6000)))
			<div class="menu-item">
				<a href="{{ AdminOptions::base_url()}}admin/administratori" class="menu-item__link @if($strana == 'administratori' || $strana == 'grupa_modula') active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-user-plus" aria-hidden="true"></i>
						<span class="menu-tooltip">Korisnici</span>
					</div>
					<div class="menu-item__text">Korisnici</div>
				</a>
			</div>
			@endif
			@if(Admin_model::check_admin(array(233,234,235,239,244,245,246,248,3200,3400,1800,2400,5200,9500)) && AdminOptions::is_shop())
			<li class="menu-item">
				<a href="{{ AdminOptions::base_url()}}admin/grupe/0" class="menu-item__link  @if($strana == 'grupe') active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-industry" aria-hidden="true"></i>
						<span class="menu-tooltip">Šifarnici</span>
					</div>
					<div class="menu-item__text">Šifarnici</div>
				</a>
			</li>
			@endif
			@if(Admin_model::check_admin(array(10000)) AND AdminOptions::checkB2B() && AdminOptions::is_shop())
			<li class="menu-item">
				<a href="{{ AdminOptions::base_url()}}admin/b2b" class="menu-item__link">
					<div class="menu-item__icon">
						B2B
						<span class="menu-tooltip">B2B Admin</span>
					</div>
					<div class="menu-item__text">B2B Admin</div>
				</a>
			</li>
			@endif			
			<li class="menu-item">
				<a href="{{ AdminOptions::base_url()}}admin/podesavanja" class="menu-item__link  @if($strana == 'podesavanja') active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-cogs" aria-hidden="true"></i>
						<span class="menu-tooltip">Podešavanja</span>
					</div>
					<div class="menu-item__text">Podešavanja</div>
				</a>
			</li>
			<!-- <div class="menu-item">
				<a href="{{ AdminOptions::base_url()}}admin/crm_zadaci" class="menu-item__link @if($strana == 'crm') active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-user-plus" aria-hidden="true"></i>
						<span class="menu-tooltip">CRM</span>
					</div>
					<div class="menu-item__text">CRM</div>
				</a>
			</div> -->
		</ul>
	</nav>

	<div class="logout-wrapper">
		<div class="menu-item">
			<a href="{{ AdminOptions::base_url()}}admin-logout" class="menu-item__link">
				<div class="menu-item__icon">
					<i class="fa fa-sign-out" aria-hidden="true"></i>
					<span class="menu-tooltip">Odjavi se</span>
				</div>
				<div class="menu-item__text">Odjavi se</div>
			</a>
		</div>
		<p class="footnote">TiCo &copy; {{ date('Y') }} - All rights reserved</p>
	</div>
</header>

