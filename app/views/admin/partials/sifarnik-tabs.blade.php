	<div class="m-tabs clearfix">
		@if(Admin_model::check_admin(array(1800)))
		<div class="m-tabs__tab{{ $strana=='kupci' ? ' m-tabs__tab--active' : '' }}"><a href="{{AdminOptions::base_url()}}admin/kupci_partneri/kupci">Kupci</a></div>
		@endif
		@if(Admin_model::check_admin(array(1600,2000)))
		<div class="m-tabs__tab{{ $strana=='partneri' ? ' m-tabs__tab--active' : '' }}"><a href="{{AdminOptions::base_url()}}admin/kupci_partneri/partneri">Partneri</a></div>
		@endif
	</div>