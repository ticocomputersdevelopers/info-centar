<ul class="custom-menu articles-mn">
	<li>
		<a class="custom-menu-item" id="new-art" href="{{AdminOptions::base_url()}}admin/product/0" target="_blank">Dodaj novi artikal (F2)</a>

	</li>
	<li>
		<button class="custom-menu-item" id="JSIzmeni">Izmeni artikal (F4)</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSUrediOpis">Uredi opis (F7)</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSKlonirajArtikal">Kloniraj artikal</button>
	</li>
	
	<!-- <li>
		<button class="custom-menu-item custom-menu-sub" id="">Postavi karakteristike <i class="fa fa-caret-right" aria-hidden="true"></i></button>
		<ul class="custom-menu-sub-item">
			<li>
				<button class="custom-menu-item JSexecute" data-execute='[{"table":"roba", "column":"web_flag_karakteristike", "val":"0"}]'>HTML кarakteristike</button>	
			</li>
			<li>
				<button class="custom-menu-item JSexecute" data-execute='[{"table":"roba", "column":"web_flag_karakteristike", "val":"1"}]'>Generisane</button>
			</li>
			<li>
				<button class="custom-menu-item JSexecute" data-execute='[{"table":"roba", "column":"web_flag_karakteristike", "val":"2"}]'>Od dobavljača</button>
			</li>	
		</ul>
	</li>	 -->
	<li>
		<button class="custom-menu-item" id="JSobradaKarakteristika">Obrada karakteristika</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSObrisiArtikal">Obriši artikal/le (F8)</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSRefresh">Osveži (F5)</button>
	</li>
<!-- 	<li>
		<button class="custom-menu-item" id="JSEditTeksta" value="">Edit teksta</button>
	</li> -->
	<li>
		<button class="custom-menu-item" id="JSchooseGroup">Promeni grupu</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSchooseBrand">Promeni proizvođača</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSchooseTip">Promeni tip</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSchoosePrice">Promeni vrstu cene</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSchooseQuantity">Promeni količinu</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSchooseTax">Promeni poresku stopu</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSTezina">Promeni težinu</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSTag">Dodeli tagove</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSchoosePictures">Dodavanje slike</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSchooseExport">Export artikala</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSmassEdit">Edit cena</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JScharacteristics">Dodela karakteristika</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JStextEdit">Edit teksta</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSakcijaEdit">Masovna dodela akcije</button>
	</li>


	
</ul>