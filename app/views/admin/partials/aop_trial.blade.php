@if(AdminOptions::checkAOPTrial())
	<div class="row trial-period">
		<span class="time-left">Ostalo je još <span>{{ AdminSupport::trial_date()->days+1 }}</span> dana probnog perioda</span>
		<a href="{{ AdminOptions::base_url()}}admin/podesavanje-naloga/izaberi-paket">Izaberi plan</a>
	</div>
@endif