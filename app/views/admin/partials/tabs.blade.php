	
<div class="m-tabs clearfix">
	@if(Admin_model::check_admin(array(233,234,235,239,244,245,246,248)))
	<div class="m-tabs__tab{{ $strana == 'grupe' ? ' m-tabs__tab--active' : '' }}"><a href="{{ AdminOptions::base_url() }}admin/grupe/0">GRUPE</a></div>
	@endif
	@if(Admin_model::check_admin(array(3200,3400)))
	<div class="m-tabs__tab{{ $strana == 'proizvodjac' ? ' m-tabs__tab--active' : '' }}"><a href="{{ AdminOptions::base_url() }}admin/proizvodjac/0">PROIZVOĐAČI </a></div>
	@endif
<!-- 	@if(Admin_model::check_admin(array(1800)))
	<div class="m-tabs__tab{{ $strana == 'mesto' ? ' m-tabs__tab--active' : '' }}"><a href="{{ AdminOptions::base_url() }}admin/mesto">MESTA </a></div>
	@endif -->
	@if(Admin_model::check_admin(array(2400)))
	<div class="m-tabs__tab{{ $strana == 'tip_artikla' ? ' m-tabs__tab--active' : '' }}"><a href="{{ AdminOptions::base_url() }}admin/tip/0">TIPOVI </a></div>
	@endif
	@if(Admin_model::check_admin(array(5200)))
	<div class="m-tabs__tab{{ $strana == 'jedinica_mere' ? ' m-tabs__tab--active' : '' }}"><a href="{{ AdminOptions::base_url() }}admin/jedinica-mere/0">JEDINICE MERE </a></div>
	@endif
	@if(Admin_model::check_admin(array(5200)))
	<div class="m-tabs__tab{{ $strana == 'poreske_stope' ? ' m-tabs__tab--active' : '' }}"><a href="{{ AdminOptions::base_url() }}admin/poreske-stope">PORESKE STOPE </a></div>
	@endif
	@if(Admin_model::check_admin(array(5200)))
	<div class="m-tabs__tab{{ $strana == 'stanje_artikla' ? ' m-tabs__tab--active' : '' }}"><a href="{{ AdminOptions::base_url() }}admin/stanje-artikla">STANJA ARTIKLA </a></div>
	@endif
	@if(Admin_model::check_admin(array(5200)))
	<div class="m-tabs__tab{{ $strana == 'status_narudzbine' ? ' m-tabs__tab--active' : '' }}"><a href="{{ AdminOptions::base_url() }}admin/status_narudzbine">STATUS NARUDŽBINE </a></div>
	@endif
	@if(Admin_model::check_admin(array(9500)))
	<div class="m-tabs__tab{{ $strana == 'konfigurator' ? ' m-tabs__tab--active' : '' }}"><a href="{{ AdminOptions::base_url() }}admin/konfigurator">KONFIGURATORI </a></div>
	@endif
	@if(Admin_model::check_admin(array(5200)))
		@if(in_array(AdminB2BOptions::web_options(130),array(0,1)) && Admin_model::check_admin(array(5200)))
		<div class="m-tabs__tab{{ $strana == 'komentari' || $strana == 'komentar' ? ' m-tabs__tab--active' : '' }}"><a href="{{ AdminOptions::base_url() }}admin/komentari">KOMENTARI </a></div>
		@endif
	@endif
	@if(Admin_model::check_admin(array(5200)))
	<div class="m-tabs__tab{{ $strana == 'osobine' ? ' m-tabs__tab--active' : '' }}"><a href="{{ AdminOptions::base_url() }}admin/osobine/0">OSOBINE</a></div>
	@endif
	@if(Admin_model::check_admin(array(5200)))
	<div class="m-tabs__tab{{ $strana == 'nacin_placanja' ? ' m-tabs__tab--active' : '' }}"><a href="{{ AdminOptions::base_url() }}admin/nacin_placanja/0">NAČIN PLAĆANJA</a></div>
	@endif
	@if(Admin_model::check_admin(array(5200)))
	<div class="m-tabs__tab{{ $strana == 'nacin_isporuke' ? ' m-tabs__tab--active' : '' }}"><a href="{{ AdminOptions::base_url() }}admin/nacin_isporuke/0">NAČIN ISPORUKE</a></div>
	@endif
	@if(Admin_model::check_admin(array(5200)))
	<div class="m-tabs__tab{{ $strana == 'troskovi_isporuke' ? ' m-tabs__tab--active' : '' }}"><a href="{{ AdminOptions::base_url() }}admin/troskovi-isporuke">TROŠKOVI ISPORUKE</a></div>
	@endif
	@if(Admin_model::check_admin(array(5200)))
	<div class="m-tabs__tab{{ $strana == 'kuriska_sluzba' ? ' m-tabs__tab--active' : '' }}"><a href="{{ AdminOptions::base_url() }}admin/kurirska_sluzba">KURIRSKA SLUŽBA</a></div>
	@endif
	@if(Admin_model::check_admin(array(5200)))
	<div class="m-tabs__tab{{ $strana == 'kurs' ? ' m-tabs__tab--active' : '' }}"><a href="{{ AdminOptions::base_url() }}admin/kurs/2">UNOS KURSA</a></div>
	@endif

</div>