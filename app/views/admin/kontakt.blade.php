<section id="main-content" class="contakt-page">
	<!-- SOCIAL ICONS -->
@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
@endif	
	<section class="small-12 medium-12 large-3 columns">
		<div class="flat-box">
			<h3 class="title-med">Logo</h3>
			
			<div class="logo-upload-area">
				
				<form method="post" name="logo_upload" enctype="multipart/form-data" action="upload_logo" class="text-right JSLogoForm">
					 
					<div class="row"> 
						<div class="column medium-12 clearfix"> 
							<label>Dodajte logo</label>
							<a href="#" class="file-upload btn btn-secondary btn-small JSLogoUploadButton">   
								<input type="file" name="logo" class="file-input JSLogoUpload">
								 Dodaj sliku
							</a>
						</div>
					</div>
				</form>
				<!-- mladja dodao -->
				<section class="logo-preview-area">
					<img class="logo-preview" src="{{Admin_model::logo()}}" alt="" />
				</section>				
			</div>
			
		</div> <!-- end of .flat-box -->
		
	</section>

	<!-- CONTACT DETAILS EDIT -->

	<section class="contact-details-edit small-12 medium-12 large-9 columns">
		<div class="flat-box">
			<h3 class="title-med">Kontakt podaci</h3>
			<form method="POST" action="{{ AdminOptions::base_url() }}admin/kontakt_update">
				<div class="row collapse contact-row">
					<input name="preduzece_id" value="{{ $preduzece->preduzece_id }}" type="hidden">

					<div class="medium-6 columns field-group {{ $errors->first('naziv') ? 'error' : '' }}">
						<label>Naziv firme</label>
						<input class="company-name"  name="naziv" value="{{ htmlentities(!is_null(Input::old('naziv')) ? Input::old('naziv') : $preduzece->naziv) }}" type="text">
					</div>

					<div class="medium-6 columns field-group {{ $errors->first('email') ? 'error' : '' }}">
						<label>E-mail</label>
						<input class="e-mail"  name="email" value="{{ !is_null(Input::old('email')) ? Input::old('email') : $preduzece->email }}" type="text">
					</div>

					@if(AdminNarudzbine::api_posta(3))
					<div class="medium-6 columns field-group"> 
						<label>Opština</label>				 
						<select class="form-control" name="opstina" id="JSOpstina" tabindex="6">
						@foreach(AdminNarudzbine::opstine() as $key => $opstina)
							@if((Input::old('opstina') ? Input::old('opstina') : AdminNarudzbine::opstina_id($preduzece->ulica_id)) == $opstina->narudzbina_opstina_id)
							<option value="{{ $opstina->narudzbina_opstina_id }}" selected>{{ $opstina->naziv }}</option>
							@else
							<option value="{{ $opstina->narudzbina_opstina_id }}">{{ $opstina->naziv }}</option>
							@endif
						@endforeach
						</select>
					</div>
					<div class="medium-6 columns field-group"> 
						<label>Naselje</label>
						<select class="form-control" name="mesto" id="JSMesto">
						@foreach(AdminNarudzbine::mesta(Input::old('opstina') ? Input::old('opstina') : AdminNarudzbine::opstina_id($preduzece->ulica_id)) as $key => $mesto)
							@if((Input::old('mesto') ? Input::old('mesto') : AdminNarudzbine::mesto_id($preduzece->ulica_id)) == $mesto->narudzbina_mesto_id)
							<option value="{{ $mesto->narudzbina_mesto_id }}" selected>{{ $mesto->naziv }}</option>
							@else
							<option value="{{ $mesto->narudzbina_mesto_id }}">{{ $mesto->naziv }}</option>
							@endif
						@endforeach
						</select>
					</div>
					<div class="medium-6 columns field-group"> 
						<label>Ulica</label>				 
						<select class="form-control" name="ulica_id" id="JSUlica">
						@foreach(AdminNarudzbine::ulice(Input::old('mesto') ? Input::old('mesto') : AdminNarudzbine::mesto_id($preduzece->ulica_id)) as $key => $ulica)
							@if((Input::old('ulica_id') ? Input::old('ulica_id') : $preduzece->ulica_id) == $ulica->narudzbina_ulica_id)
							<option value="{{ $ulica->narudzbina_ulica_id }}" selected>{{ $ulica->naziv }}</option>
							@else
							<option value="{{ $ulica->narudzbina_ulica_id }}">{{ $ulica->naziv }}</option>
							@endif
						@endforeach
						</select>
					</div>
					<div class="medium-6 columns field-group {{ $errors->first('broj') ? 'error' : '' }}"> 
						<label>Broj</label>
						<input id="without-reg-number" class="form-control" name="broj" type="text" value="{{ htmlentities(Input::old('broj') ? Input::old('broj') : $preduzece->broj) }}">
					</div>
					@else
					<div class="medium-6 columns field-group {{ $errors->first('mesto') ? 'error' : '' }}">				
						<label>Grad</label>
						<input class="phone-number"  name="mesto" value="{{ !is_null(Input::old('mesto')) ? Input::old('mesto') : $preduzece->mesto }}" type="text">			
					</div>
					
					<div class="medium-6 columns field-group {{ $errors->first('adresa') ? 'error' : '' }}">
						<label>Adresa</label>
						<input class="address"  name="adresa" value="{{ htmlentities(!is_null(Input::old('adresa')) ? Input::old('adresa') : $preduzece->adresa) }}" type="text">
					</div>
					@endif

					<div class="medium-6 columns field-group {{ $errors->first('kontakt_osoba') ? 'error' : '' }}">
						<label>Kontakt Osoba</label>
						<input class="phone-number"  name="kontakt_osoba" value="{{ !is_null(Input::old('kontakt_osoba')) ? Input::old('kontakt_osoba') : $preduzece->kontakt_osoba }}" type="text">
					</div>

					<div class="medium-6 columns field-group {{ $errors->first('telefon') ? 'error' : '' }}">
						<label>Broj telefona</label>
						<input class="phone-number"  name="telefon" value="{{ !is_null(Input::old('telefon')) ? Input::old('telefon') : $preduzece->telefon }}" type="text">
					</div>
					
					<div class="medium-6 columns field-group">		
						<label>Fax</label>
						<input class="fax" name="fax" value="{{ !is_null(Input::old('fax')) ? Input::old('fax') : $preduzece->fax }}"  type="text">
					</div>
					
					<div class="medium-6 columns field-group {{ $errors->first('matbr_registra') ? 'error' : '' }}">					
						<label>Matični broj</label>
						<input class="matbr_registra" name="matbr_registra" value="{{ !is_null(Input::old('matbr_registra')) ? Input::old('matbr_registra') : $preduzece->matbr_registra }}"  type="text">				
					</div>

					<div class="medium-6 columns field-group {{ $errors->first('pib') ? 'error' : '' }}">		
						<label>Pib</label>
						<input class="pib" name="pib" value="{{ !is_null(Input::old('pib')) ? Input::old('pib') : $preduzece->pib }}"  type="text">
					</div>
					
					<div class="medium-6 columns field-group {{ $errors->first('ziro') ? 'error' : '' }}">		
						<label>Žiro račun</label>
						<input class="bank-account" name="ziro" value="{{ !is_null(Input::old('ziro')) ? Input::old('ziro') : $preduzece->ziro }}"  type="text">	
					</div>

					<div class="medium-6 columns field-group {{ $errors->first('delatnost_sifra') ? 'error' : '' }}">	
						<label>Šifra delatnosti</label>
						<input class="del-number" name="delatnost_sifra" value="{{ !is_null(Input::old('delatnost_sifra')) ? Input::old('delatnost_sifra') : $preduzece->delatnost_sifra }}"  type="text">
					</div>

					<div class="medium-6 columns field-group {{ $errors->first('latitude') ? 'error' : '' }}">		
						<label>Geografska širina (latitude)</label>
						<input class="google-map-latitude" name="latitude" value="{{ !is_null(Input::old('latitude')) ? Input::old('latitude') : $latitude }}"  type="text">					
					</div>

					<div class="medium-6 columns field-group {{ $errors->first('longitude') ? 'error' : '' }}">
						<label>Geografska dužina (longitude)</label>
						<input class="google-map-latitude" name="longitude" value="{{ !is_null(Input::old('longitude')) ? Input::old('longitude') : $longitude }}"  type="text">				
					</div>
                   
				</div> <!-- end of .row -->
				<div class="row"> 
					<div class="column medium-12"> 
						<p class="" style="font-size: 11px;">Da bi ste pravilno popunili polja za google mapu mozete posetiti ovaj sajt <a href="http://www.latlong.net/" target="_blank">www.latlong.net</a>, gde upisujete svoju željenu adresu npr: <span style="color: green">Dušanova 65 Niš</span>
                    	 i uzimate potrebne koordinate.</p>
                     </div>
               </div>
				<h3 class="text-center h3-margin">Društvene mreže:</h3>
			
				<ul class="social-edit row">
					
					<li class="column medium-6 social-edit__group has-tooltip" title="Dodajte Facebook link">
						<i class="social-edit__group__icon fa fa-facebook-official" aria-hidden="true"></i>
						<input name="facebook" class="social-edit__group__input {{ $errors->first('facebook') ? 'error' : '' }}" placeholder="Dodajte Facebook link" type="text" value="{{ !is_null(Input::old('facebook')) ? Input::old('facebook') : $preduzece->facebook }}">
					</li>

					<li class="column medium-6 social-edit__group has-tooltip" title="Dodajte Twitter link">
						<i class="social-edit__group__icon fa fa-twitter" aria-hidden="true"></i>
						<input name="twitter" class="social-edit__group__input {{ $errors->first('facebook') ? 'twitter' : '' }}" placeholder="Dodajte Twitter link" type="text" value="{{ !is_null(Input::old('twitter')) ? Input::old('twitter') : $preduzece->twitter }}">
					</li>
					
					<li class="column medium-6 social-edit__group has-tooltip" title="Dodajte Google+ link">
						<i class="social-edit__group__icon fa fa-google-plus" aria-hidden="true"></i>
						<input name="google_p" class="social-edit__group__input {{ $errors->first('facebook') ? 'google_p' : '' }}" placeholder="Dodajte Google Plus link" type="text" value="{{ !is_null(Input::old('google_p')) ? Input::old('google_p') : $preduzece->google_p }}">
					</li>
					
					<li class="column medium-6 social-edit__group has-tooltip" title="Dodajte Skype link">
						<i class="social-edit__group__icon fa fa-skype" aria-hidden="true"></i>
						<input name="skype" class="social-edit__group__input {{ $errors->first('facebook') ? 'skype' : '' }}" placeholder="Dodajte Skype link" type="text" value="{{ !is_null(Input::old('skype')) ? Input::old('skype') : $preduzece->skype }}">
					</li>

					<li class="column medium-6 social-edit__group has-tooltip" title="Dodajte Instagram link">
						<i class="social-edit__group__icon fa fa-instagram" aria-hidden="true"></i>
						<input name="instagram" class="social-edit__group__input {{ $errors->first('facebook') ? 'instagram' : '' }}" placeholder="Dodajte instagram link" type="text" value="{{ !is_null(Input::old('instagram')) ? Input::old('instagram') : $preduzece->instagram }}">
					</li>
					
					<li class="column medium-6 social-edit__group has-tooltip" title="Dodajte linkedIn link">
						<i class="social-edit__group__icon fa fa-linkedin-square" aria-hidden="true"></i>
						<input name="linkedin" class="social-edit__group__input {{ $errors->first('facebook') ? 'linkedin' : '' }}" placeholder="Dodajte linkedIn link" type="text" value="{{ !is_null(Input::old('linkedin')) ? Input::old('linkedin') : $preduzece->linkedin }}">
					</li>

					<li class="column medium-6 social-edit__group has-tooltip" title="Dodajte youtube link">
						<i class="social-edit__group__icon 	fa fa-youtube-square" aria-hidden="true"></i>
						<input name="youtube" class="social-edit__group__input {{ $errors->first('facebook') ? 'youtube' : '' }}" placeholder="Dodajte youtube link" type="text" value="{{ !is_null(Input::old('youtube')) ? Input::old('youtube') : $preduzece->youtube }}">
					</li>
					
				</ul>	
				<div class="row"> 		
					<div class="btn-container center">
						<button type="submit" class="contact-edit-save btn btn-primary save-it-btn">Sačuvaj</button>
					</div>
				</div>
			</form>
		</div> <!-- end of .flat-box -->
	</section>
</section> <!-- end of #main-content -->
