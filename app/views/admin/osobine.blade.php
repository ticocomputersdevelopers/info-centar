<section class="osobine-page" id="main-content">
@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
@endif
	@include('admin/partials/tabs')
	
	<div class="row">
	<section class="small-12 medium-12 large-3 columns">
		<div class="flat-box">	
			<h3 class="title-med">Osobine</h3>
			<div class="row"> 
				<div class="columns medium-12 after-select-margin"> 
					<select name="" id="JSselectProperty">
						<option data-id="0">Nova osobina</option>
						@foreach($osobine as $row)
							<option data-id="{{ $row->osobina_naziv_id }}" @if($row->osobina_naziv_id == $osobina_naziv_id) selected @endif>{{ $row->naziv }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
	</section>

	<section class="small-12 medium-12 large-5 columns">
		<div class="flat-box">		
			@if($osobina_naziv_id != 0)
				
				<form action="{{ AdminOptions::base_url() }}admin/osobine/{{$row->osobina_naziv_id}}" method="POST">
				@foreach($osobina as $row)
					<h3 class="title-med">{{ $row->naziv }}</h3>
					<?php if($row->color_picker == 1){ $color_picker_active = 1;}else{$color_picker_active = 0;}; ?>
					<div class="row"> 
						<input type="hidden" name="osobina_naziv_id" value="{{ $row->osobina_naziv_id }}">
						<div class="columns medium-6 field-group {{ $errors->first('naziv') ? ' error' : '' }}">
							<label for="naziv">Naziv</label>
							<input type="text" name="naziv" value="{{ $row->naziv }}" />
						</div>
						<div class="columns medium-6 field-group {{ $errors->first('rbr') ? ' error' : '' }}">
							<label for="rbr">Redni broj</label>
							<input type="text" name="rbr" value="{{ $row->rbr }}"/>
						</div>
						<div class="columns medium-12 field-group">
							<input name="aktivna" type="checkbox" @if($row->aktivna == 1) checked @endif /> Aktivna
						</div>
						<div class="columns medium-12 field-group">
							<input name="prikazi_vrednost" type="checkbox" @if($row->prikazi_vrednost == 1) checked @endif> Prikaži nazive vrednosti u prodavnici
						</div>
						<div class="columns medium-12 field-group">
							<input name="color_picker" type="checkbox" @if($row->color_picker == 1) checked @endif> Odabir boje
						</div>
						<div class="columns medium-12 field-group btn-container center">
							<button type="submit" class="btn-edit-simple save-it-btn">Sačuvaj</button>
							<button class="btn btn-danger JSbtn-delete" data-link="{{ AdminOptions::base_url() }}admin/osobine/{{ $row->osobina_naziv_id }}/delete">Obriši</button>
						</div>
					</div>
				@endforeach
				</form>

			@else
				<h3 class="title-med">Nova osobina</a></h3>
				<form action="{{ AdminOptions::base_url() }}admin/osobine/0" method="POST">
					<div class="row"> 
						<input type="hidden" name="osobina_naziv_id" value="0">
						<div class="columns medium-6 field-group {{ $errors->first('naziv') ? ' error' : '' }}">
							<label for="naziv">Naziv</label>
							<input type="text" name="naziv" value=""/>
						</div>
						<div class="columns medium-6 field-group {{ $errors->first('rbr') ? ' error' : '' }}">
							<label for="rbr">Redni broj</label>
							<input type="text" name="rbr" value=""/>
						</div>
						<div class="columns medium-12 field-group">
							<input name="aktivna" type="checkbox"> Aktivna
						</div>
						<div class="columns medium-12 field-group">
							<input name="prikazi_vrednost" type="checkbox"> Prikaži nazive vrednosti u prodavnici
						</div>
						<div class="columns medium-12 field-group">
							<input name="color_picker" type="checkbox"> Odabir boje
						</div>
						<div class="columns medium-12 field-group btn-container center">
							<button type="submit" class="btn-edit-simple save-it-btn" value="">Sačuvaj</button>
						</div>
					</div>
				</form>
			@endif
		</div>
	</section>
	
	@if($osobina_naziv_id != 0)
	<section class="small-12 medium-12 large-4 columns">
		<div class="flat-box">
			<h3 class="title-med">Nova vrednost</h3>
			<form action="{{ AdminOptions::base_url() }}admin/osobine/{{$osobina_naziv_id}}/0" method="POST">
				<div class="row">
				
					<div class="columns medium-12 field-group {{ $errors->first('vrednost') ? ' error' : '' }}">
						<label for="vrednost">Naziv</label>	
						<input type="text" name="vrednost" value="" placeholder="Naziv">
					</div>
					<div class="columns medium-12 field-group flex-me">

						<span> 
							<input name="aktivna" type="checkbox" /> Aktivno
						</span>
						  @if($color_picker_active == 1)
							<span class="flex-me"> 
								<label for="boja_css">Izaberite boju</label>
								<input type="color" class="inp-color" name="boja_css" value="#888888">
							</span>
						  @endif
					 </div>
					 
					<div class="field-group center btn-container columns medium-12">
						<button type="submit" class="btn-edit-simple save-it-btn" value=""> Sačuvaj</button>
					</div>	
				
				</div>
			</form>
		</div>
		<ul class="banner-list name-ul" id="JSListOsobine">
		@foreach($vrednosti as $row)
		<div class="flat-box">
			<form action="{{ AdminOptions::base_url() }}admin/osobine/{{$row->osobina_naziv_id}}/{{$row->osobina_vrednost_id}}" method="POST">
			<div class="attributes">
				<li id='{{$row->osobina_vrednost_id}}' class="row">
				<div class="columns medium-4 field-group">
					<span class="sortableKar-span"> 
					<i class="fa fa-arrows-v tooltipz" aria-label="Prevucite da bi sortirali"></i>
					</span>
					<label for="vrednost">Naziv</label>	 
					<input type="text" name="vrednost" value="{{$row->vrednost}}">
				</div>

				<div class="columns medium-4 no-padd">
					<span> 
 						<input name="aktivna" type="checkbox" @if($row->aktivna == 1) {{ 'checked' }} @endif /> Aktivno
 				 	</span>
 				 	@if($color_picker_active == 1)
 				 	<span class="flex-me"> 
					  	<label for="boja_css">Izaberite boju</label>
						<input type="color" class="inp-color" name="boja_css" value="{{$row->boja_css}}">
			        </span>
					 @endif
				</div>

			   <div class="center btn-container columns medium-4 no-padd">
					<button type="submit" class="btn-edit-simple save-it-btn" value="">Sačuvaj</button>
					<button class="btn btn-danger JSbtn-delete" data-link="{{ AdminOptions::base_url() }}admin/osobine/{{$row->osobina_naziv_id}}/{{$row->osobina_vrednost_id}}/delete">Obriši</button>
				</div>	
			</li>
			</div>
			</form>
		</div>
		@endforeach
		</ul>
	</section>
	@endif
</div>
</section>

 


