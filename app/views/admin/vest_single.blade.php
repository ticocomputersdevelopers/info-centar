<section id="main-content">
	@if(Session::has('message'))
		<script>
			alertify.success('{{ Session::get('message') }}');
		</script>
	@endif
	<div class="row m-subnav">
		<div class="large-2 medium-3 small-12 columns ">
			 <a class="m-subnav__link" href="{{AdminOptions::base_url()}}admin/vesti" >
				<div class="m-subnav__link__icon">
					<i class="fa fa-arrow-left" aria-hidden="true"></i>
				</div>
				<div class="m-subnav__link__text">
					Nazad
				</div>
			</a>
		</div>
	</div>
	 <div class="row"> 
	 	<div class="column medium-8 large-8 large-centered"> 
		<form action="{{AdminOptions::base_url()}}admin/vesti/{{ $web_vest_b2c_id }}/save" class="flat-box" method="post" enctype="multipart/form-data">
			<input type="hidden" name="id" value="{{ $web_vest_b2c_id }}">
			<input type="hidden" name="jezik_id" value="{{ $jezik_id }}">

			<div class="row"> 
				<div class="columns medium-12 field-group{{ $errors->first('naslov') ? ' error' : '' }}">
					<label for="">Naslov</label>
					<input class="news-single-title" type="text" name="naslov" value="{{ htmlentities(!is_null(Input::old('naslov')) ? Input::old('naslov') : $naslov) }}">
					<label>{{$errors->first('naslov')}}</label>
					<a class="btn btn-primary btn-secondary" href="{{ AdminVesti::news_link($web_vest_b2c_id) }}" target="_blank">Vidi vest</a>
				</div>
			</div>

			<div class="row"> 
				<div class="columns medium-2 field-group{{ $errors->first('rbr') ? ' error' : '' }}">
					<label for="">Redni broj</label>
					<input class="ordered-number" type="text" name="rbr" value="{{ !is_null(Input::old('rbr')) ? Input::old('rbr') : $rbr }}">
				</div>

				<div class="columns medium-12 field-group">
					<img class="news-pic" src="{{ AdminOptions::base_url() }}{{ $web_vest_b2c_id != 0 ? $slika : 'images/no-image.jpg' }}">

					<div class="input-group">
						<div class="bg-image-file">
							<label>Dodaj sliku</label>
							<input type="file" name="news_img"> 
						</div>
					</div>
				</div>
			</div>

			@if(count($jezici) > 1)
			<div class="row"> 
				<div class="languages">
					<ul>	
					@foreach($jezici as $jezik)
						<li><a class="{{ $jezik_id == $jezik->jezik_id ? 'active' : '' }} btn-small btn btn-secondary" href="{{AdminOptions::base_url()}}admin/vest/{{$web_vest_b2c_id}}/{{ $jezik->jezik_id }}">{{ $jezik->naziv }}</a></li>
					@endforeach
					</ul>
				</div>
			</div>
			@endif

			<div class="row"> 
				<div class="columns medium-12 field-group{{ $errors->first('tekst') ? ' error' : '' }}">
					<textarea class="special-textareas" name="tekst" id="content">{{ !is_null(Input::old('tekst')) ? Input::old('tekst') : $tekst }}</textarea>
				</div>
			</div>
			<div class="row"> 
	            <div class="columns medium-12 field-group{{ $errors->first('seo_title') ? ' error' : '' }}">
	                <label>Naslov (title) do 60 karaktera</label>
					<input type="text" name="seo_title" value="{{ htmlentities(Input::old('seo_title') ? Input::old('seo_title') : $seo_title) }}"> 
	            </div>
	        </div>

	        <div class="row"> 
				<div class="columns medium-12 field-group{{ $errors->first('description') ? ' error' : '' }}">
					<label for="description">Opis (description) do 158 karaktera </label>
					<input type="text" name="description"
					value="{{ Input::old('description') ? Input::old('description') : $description }}" />
				</div>
			</div>	

			<div class="row"> 
				<div class="columns medium-12 field-group{{ $errors->first('keywords') ? ' error' : '' }}">
					<label for="keywords">Ključne reči (keywords)</label>
					<input type="text" name="keywords"
					value="{{ Input::old('keywords') ? Input::old('keywords') : $keywords }}" />
				</div>
			 </div>

			<div class="row">
				<div class="columns medium-12">
				  	<input type="checkbox" name="aktuelno" {{ $aktuelno == 1 ? 'checked' : '' }} >Aktivna vest
			   		<!-- <input type="checkbox" name="b2b_aktuelno" {{ $b2b_aktuelno == 1 ? 'checked' : '' }} >B2b aktivna vest -->
				 </div>
			</div>
			 <div class="row">
				 <div class="columns medium-12 large-12">
				 	<div class="btn-container text-center"> 
						<button class="btn btn-large news-save save-it-btn">Sačuvaj</button>
					  	@if($web_vest_b2c_id != 0)
					  		<button class="btn btn-danger JSbtn-delete" data-link="{{ AdminOptions::base_url() }}admin/vesti/{{ $web_vest_b2c_id }}/delete">Obriši</button>
						@endif
					</div>
				 </div>  
			 </div>			
		</form>
	</div>	
 </div>
	<!-- 	<div class="row">
			<div class="image-upload-area single-news-upload clearfix medium-12 large-10 large-centered columns">
				
				<form method="post" enctype="multipart/form-data" action="{{AdminOptions::base_url()}}admin/image_upload" >
					<label>Dodajte sliku na strani:</label>

					<input type="file" id="img" name="img" class="file-input">
					<button class="file-upload btn btn-secondary" type="submit">Upload</button>
				</form>
				<div class="images_upload has-tooltip" title='Prevucite sliku na željenu poziciju'>
					{{Admin_model::upload_directory()}}
				</div>
			</div>
		</div> -->
 
</section>