<section class="comments-page" id="main-content">

		@include('admin/partials/tabs')

	<div class="row">
		
		<section class="small-12 medium-5 large-5 columns large-centered medium-centered">
			<div class="flat-box">
				<form action="{{AdminOptions::base_url()}}admin/komentari/{{$comment->web_b2c_komentar_id}}" method="POST">
					<div class="row"> 
					<input name="web_b2c_komentar_id" type="hidden" value="{{ $comment->web_b2c_komentar_id }}" />
					<div class="columns medium-12 field-group">
						<label for="ime_osobe">Ime</label>
						<input name="ime_osobe" type="text" value="{{ $comment->ime_osobe }}" />
					</div>
					<div class="columns medium-12 field-group">
						<label for="pitanje">Komentar</label>
						<textarea name="pitanje">{{ $comment->pitanje }}</textarea>
					</div>
					<div class="columns medium-12 field-group">
						<label for="odgovor">Odgovor</label>
						<textarea name="odgovor">{{ $comment->odgovor }}</textarea>
					</div>
					<div class="columns medium-12 field-group">
						<label for="">Datum (G-M-D)</label>
						<input style="width: 80px;" type="text" name="datum" value="{{ $comment->datum }}" disabled/>
					</div>
					<div class="columns medium-12 field-group">
						<label for="">
							Artikal
						</label>
						<a href="{{AdminArticles::article_link($comment->roba_id)}}" target="_blank">
							{{AdminArticles::find($comment->roba_id, 'naziv')}}
						</a>
					</div>
					<div class="columns medium-12 field-group">
						<label for="">Ocena</label>
						<input class="ordered-number" type="text" name="ocena" value="{{ $comment->ocena }}" />
					</div>
					<div class="columns medium-12 field-group">
						@if($comment->komentar_odobren == 1)
						<input name="komentar_odobren" type="checkbox" checked />Dozvoli komentar
						@else
						<input name="komentar_odobren" type="checkbox"/>Dozvoli komentar
						@endif
					</div>
					<div class="columns medium-12 field-group">
						@if($comment->odgovoreno == 1)
						<input name="odgovoreno" type="checkbox" checked />Odgovoreno
						@else
						<input name="odgovoreno" type="checkbox"/>Odgovoreno
						@endif
					</div>
					<div class="columns medium-12 field-group center btn-container">
						<button type="submit" class="btn btn-primary save-it-btn">Sačuvaj</button>

						<button class="btn btn-danger JSbtn-delete" data-link="{{AdminOptions::base_url()}}admin/komentari/{{$comment->web_b2c_komentar_id}}/delete">Obriši</button> 
					</div>
				</div>
				</form>
			</div>
			
		</section>
		 
	</div>
</section>