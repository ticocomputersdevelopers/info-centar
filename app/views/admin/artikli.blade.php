<section class="collapse articles-page" id="main-content">
	
    <div class="row collapse">
		<section class="large-3 medium-3 columns cat-list">
			@include('admin.partials.kat')
		</section>
		
		<?php
			if(Session::get('adminCategories')) {
				echo "<section class='large-9 medium-9 columns articles-content " . Session::get('adminCategories') ."'>";
				
			} else {
				echo "<section class='large-9 medium-9 columns articles-content wide-articles'>";
			}
		?>
			@include('admin/product_list')
		</section>
	</div> <!-- end of .row -->
</section>