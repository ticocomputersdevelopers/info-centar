<div id="main-content" class="">
@include('admin/partials/product-tabs')	
	@include('admin/partials/karak-tabs')

	<input id="roba_id_obj" type="hidden" value="{{ $roba_id }}">

	<div class="row">
		<div class="columns medium-6 large-centered medium-centered">
			<div class="flat-box">
				<div class="row">
					<h3 class="title-med">Naziv karakteristike</h3>
					<div class="columns medium-12 field-group">
						 
						<div class="m-input-and-button">
							 <select id="grupa_pr_naziv_id" class="m-input-and-button__input">
								@foreach($nazivi as $row)
								<option value="{{ $row->grupa_pr_naziv_id }}">{{ $row->naziv }}</option>
								@endforeach
							</select>
								<button id="add_karak" class="btn btn-primary btn-small">DODAJ</button>
								<button id="add_all_karak" class="btn btn-secondary btn-small">DODAJ SVE</button>
								<a class="btn btn-primary btn-small" href="{{AdminOptions::base_url()}}admin/grupe/{{ AdminArticles::find($roba_id, 'grupa_pr_id') }}" target="_blank">DODAJ NOVE NAZIVE</a>
						</div>
					 </div>		
				</div>
			</div> <!-- end of .flat-box -->


			<div class="flat-box">
				<h2 class="title-med">Karakteristike</h2>
				<div class="row"> 
				    <div class="column medium-12"> 
						<table data-id="{{ $roba_id }}" role="grid" class="karakteristike-tabela">
							@foreach(AdminSupport::getGenerisane($roba_id) as $row)
								<tr class="inline-list">
									<td><input type="text" style="width:60px" class="rbr_gener" value="{{ $row->rbr }}"></td>
									<td colspan="3">{{ $row->naziv }}</td>
									<td>			
										<select class="save-gener" data-id="{{ $row->grupa_pr_naziv_id }}">
											@foreach(AdminSupport::getVrednostKarak($row->grupa_pr_naziv_id) as $row1)
											<option value="{{ $row1->grupa_pr_vrednost_id }}" <?php echo $row->grupa_pr_vrednost_id == $row1->grupa_pr_vrednost_id ? 'selected' : ''  ?>>{{ $row1->naziv }}</option>
											@endforeach
										</select>
									</td>
									<td column="1">
										<button class="delete-gener name-ul__li__save button-option" data-id="{{ $row->grupa_pr_naziv_id }}"><i class="fa fa-close" aria-hidden="true"></i></button>
									</td>
								</tr>
							@endforeach
						</table>
					</div>
				</div>
			</div> <!-- end of .flat-box -->
		</div>
	</div> <!-- end of .row -->
</div>