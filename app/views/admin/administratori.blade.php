<section class="nivoi-sec" id="main-content">
@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
@endif
	@include('admin/partials/permissions-tabs')

	<div class="row">
		<section class="small-4 medium-4 large-4 columns">
			<div class="flat-box">
				<h1 class="title-med">Administratori</h1>
				<ul class="name-ul">
					<a href="{{ AdminOptions::base_url() }}admin/administratori" class="admin-users center">
						<li class="name-ul__li"{{$imenik_id == null ? ' style="background-color:#ddd"' : ''}}>
	 						<div class="name-ul__li__name">Dodaj novi</div>
	 					</li>
				 	</a>
					@foreach($administratori as $row)
					<a href="{{ AdminOptions::base_url() }}admin/administratori/{{ $row->imenik_id }}" class="admin-users">
						<li class="name-ul__li"{{$row->imenik_id == $imenik_id && $imenik_id != null ? ' style="background-color:#ddd"' : ''}}>
							<div class="name-ul__li__name">{{ $row->ime }} {{ $row->prezime }} ({{ $row->imenik_id == 0 ? 'Super Administrator' : AdminSupport::nivoPristupa($row->kvota) }}) {{ $row->imenik_id == Session::get('b2c_admin'.AdminOptions::server()) ? 'ULOGOVAN' : '' }}</div>
					 	</li>
				    </a>
					@endforeach
				</ul>				
			</div>
		</section>
		<section class="small-8 medium-8 large-8 columns">
			<div class="flat-box">
				<form method="POST" action="{{AdminOptions::base_url()}}admin/administrator-edit"  autocomplete="off" autocomplete="false">
					<input type="hidden" class="" name="imenik_id" value="{{ $imenik_id }}">
					<div class="row">
						<div class="column medium-12 large-6{{ $errors->first('ime') ? ' error' : '' }}">
							<label for="">Ime</label>
							<input type="text" class="" name="ime" value="{{ htmlentities(Input::old('ime') ? Input::old('ime') : $ime) }}">
						</div>
						<div class="column medium-12 large-6{{ $errors->first('prezime') ? ' error' : '' }}">
							<label for="">Prezime</label>
							<input type="text" class="" name="prezime" value="{{ htmlentities(Input::old('prezime') ? Input::old('prezime') : $prezime) }}">
						</div>
					</div>
					<div class="row">
						<div class="column medium-12 large-6{{ $errors->first('login') ? ' error' : '' }}">
							<label for="">E-mail</label>
							<input type="text" class="" name="login" value="{{ htmlentities(Input::old('login') ? Input::old('login') : $login) }}">
						</div>
						<div class="column medium-12 large-6{{ $errors->first('password') ? ' error' : '' }}">
							<label for="">Lozinka</label>
							<input type="password" class="" id="login-pass1" name="password" value="{{ htmlentities(Input::old('password') ? Input::old('password') : $password) }}">
							<input id="login-pass2" type="text" value="{{ htmlentities(Input::old('password') ? Input::old('password') : $password) }}">
							<label><a href="#" id="show-pass">Prikaži lozinku</a></label>
						</div>
					</div>
					@if($imenik_id != 0 OR is_null($imenik_id))
					<div class="row">
						<div class="column medium-12 large-6">
							<label for="">Nivo pristupa</label>
							<select name="kvota" class="">
							<?php $kvota = Input::old('kvota') ? Input::old('kvota') : $kvota; ?>
							@foreach(DB::table('ac_group')->where('ac_group_id','!=',-1)->get() as $row)
								<option value="{{$row->ac_group_id}}" {{ $row->ac_group_id == $kvota ? 'selected' : '' }}>{{$row->naziv}}</option>
							@endforeach
							</select>
						</div>
					</div>
					@endif
					<div class="btn-container center column medium-12">
						<button type="submit" class="btn btn-primary save-it-btn">Sačuvaj</button>
						@if(isset($imenik_id) AND $imenik_id != Session::get('b2c_admin'.AdminOptions::server()))
						<a class="btn btn-danger JSbtn-delete" data-link="{{ AdminOptions::base_url() }}admin/administrator-delete/{{ $imenik_id }}">Obriši</a>
						@endif
					</div>
				 
				</form>	
			</div>
		</section>	
	</div>
  <!-- </form> -->
</section>