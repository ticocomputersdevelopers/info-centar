<section class="osobine-page" id="main-content">

	@include('admin/partials/tabs')

	@if(Session::has('message'))
		<script>
			alertify.success('{{ Session::get('message') }}');
		</script>
	@endif
	
	<div class="row">
		<section class="small-12 medium-12 large-3 columns">
			<div class="flat-box">	
				<h3 class="title-med">Način isporuke <i class="fa fa-truck"></i></h3>
				<div class="row"> 
					<div class="columns medium-12 after-select-margin"> 
						<select name="" id="JSnacinIsporuke">
							<option data-id="0">Dodaj novi</option>
							@foreach($nacini_isporuke as $row)
								<option data-id="{{ $row->web_nacin_isporuke_id }}" @if($row->web_nacin_isporuke_id == $web_nacin_isporuke_id) selected @endif>{{ $row->naziv }}</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
		</section>
		<section class="small-12 medium-12 large-5 columns">
			<div class="flat-box">
			@if($web_nacin_isporuke_id > 0)		
				@foreach($nacin_isporuke as $row)
					<form action="{{ AdminOptions::base_url() }}admin/nacin_isporuke/{{$web_nacin_isporuke_id}}" method="POST">
						<h3 class="title-med">Izmeni</h3>
						<div class="row"> 
							<div class="columns medium-12 field-group {{ $errors->first('naziv') ? ' error' : '' }}">
								<input type="text" name="naziv" value="{{ $row->naziv }}">
							</div>
							<div class="columns medium-12"> 
							<input type="checkbox" name="aktivno" @if($row->selected == 1) : checked @endif> Aktivno
							</div>
						</div>
						<div class="row"> 
							<div class="btn-container text-center"> 
								<button type="submit" class="btn btn-primary save-it-btn">Sačuvaj</button>
							</div>
						</div>
					</form>
					<form action="{{ AdminOptions::base_url() }}admin/nacin_isporuke/{{$web_nacin_isporuke_id}}/delete" method="POST">
						<div class="row"> 
							<div class="columns medium-12 text-center"> 
								<input type="submit" value="Obriši" class="btn btn-danger">
							</div>
					   </div>
					</form>
				@endforeach
			@elseif($web_nacin_isporuke_id == 0)
				<form action="{{ AdminOptions::base_url() }}admin/nacin_isporuke/{{$web_nacin_isporuke_id}}" method="POST">
					<h3 class="title-med">Novo</h3>
					<div class="row"> 
						<div class="columns medium-12 field-group {{ $errors->first('naziv') ? ' error' : '' }}">
							<input type="text" name="naziv" value="" autofocus="autofocus">
						</div>
						<div class="medium-12 columns"> 
							<input type="checkbox" name="aktivno"> Aktivno
						</div>
					</div>
					<div class="row">
						<div class="btn-container text-center"> 
							<button type="submit" class="btn btn-primary save-it-btn">Sačuvaj</button>
						</div>
					</div>
				</form>
			@endif
			</div>
		</section>
	</div>

</section>