<div id="main-content" >
@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
@endif
	@include('admin/partials/tabs')
	<div class="row">
		<div class="large-3 medium-3 small-12 columns ">
			<div class="flat-box">
				<h3 class="title-med">Izaberi kurirsku službu  <i class="fa fa-truck"></i></h3>
				<select class="JSeditSupport" >
					<option value=""></option>
					<option value="{{ AdminOptions::base_url() }}admin/kurirska_sluzba">Dodaj novi</option>
   					@foreach(AdminSupport::getKurirskaSluzba(true) as $row)
						<option value="{{ AdminOptions::base_url() }}admin/kurirska_sluzba/{{ $row->posta_slanje_id }}" @if($row->posta_slanje_id == $posta_slanje_id) {{ 'aktivna' }} @endif>{{ $row->naziv }} </option>
					@endforeach
				</select>
			</div>
		</div>

	<section class="small-12 medium-12 large-5 large-centered columns">
		<div class="flat-box">

			<h1 class="title-med">{{ $title }} {{ $difolt == 1 ? '(Podrazumevani)' : '' }}</h1>
			<!-- <h1 id="info"></h1> -->

			<form method="POST" action="{{ AdminOptions::base_url() }}admin/kurirska_sluzba_edit" enctype="multipart/form-data">
				  <input type="hidden" name="posta_slanje_id" value="{{ $posta_slanje_id }}"> 
					<div class="row">
						<div class="columns medium-6 field-group{{ $errors->first('naziv') ? ' error' : '' }}">
							<label for="naziv">Kurirska služba</label>
							<input type="text" name="naziv" value="{{ htmlentities(Input::old('naziv') ? Input::old('naziv') : $naziv) }}" autofocus="autofocus"> 
						</div>
						<div class="columns medium-6 field-group">
							<label>Aktivna</label>
							<select name="aktivna">
								{{ AdminSupport::aktivnaCheck(Input::old('aktivna') ? Input::old('aktivna') : $aktivna) }}
							</select>
						</div>
				 	</div>

					<div class="row">
						<div class="columns medium-6 field-group">
							<label for="nasa_sifra">Naša šifra</label>
							<input type="text" name="nasa_sifra" value="{{ htmlentities(Input::old('nasa_sifra') ? Input::old('nasa_sifra') : $nasa_sifra) }}"> 
						</div>
						<div class="columns medium-6 field-group"><br>
							<input name="difolt" type="checkbox"  @if($difolt == 1) checked @endif> Podrazumevan
							<input name="api_aktivna" type="checkbox"  @if($api_aktivna == 1) checked @endif> Api podrška
						</div>
				 	</div>

				 	<div class="row">
						<div class="btn-container center">
							<button type="submit" class="btn btn-primary save-it-btn">Sačuvaj</button>
							@if($posta_slanje_id != 0)	
							<!-- <button class="btn btn-danger JSbtn-delete" data-link="{{ AdminOptions::base_url() }}admin/kurirska_sluzba_delete/{{ $posta_slanje_id }}">Obriši</button> -->
							@endif
						</div>
					</div>
				</form>
		 </div> <!-- end of .flat-box -->
			
		</section>
	</div> <!-- end of .row -->
</div>
	