<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// use IsLogik\DBData;

// Route::get('/test', function(){
// 	$articles = DBData::articles();

// 	All::dd($articles);

    
// });
// use Import\Roaming;
// Route::get('/test',function(){
//     Roaming::executeShort(25);
// });

Route::get('/export-xls/{kind}/{partner_slug?}', 'ExternController@export_xls');
/////////////////////////        ADMIN          ////////////////////////////////////
Route::get('/admin-login', 'Admin@admin_login');
Route::post('/admin-login-store', 'Admin@admin_login_store');
Route::get('/sso-login/{hash}/{shop?}', 'Admin@sso_login');
Route::get('/admin-logout/{confirm?}', 'Admin@logout');


//administracija za artikle (b2c admin na web-u)
Route::group(array('prefix'=>'admin','before'=>'b2c_admin'),function (){

Route::get('/podesavanje-naloga/{select_plan?}', 'Admin@account_settings');

// permisije
Route::get('/administratori/{id?}', 'AdminPermissionController@administratori');
Route::post('/administrator-edit', 'AdminPermissionController@administrator_edit');
Route::get('/administrator-delete/{id}', 'AdminPermissionController@administrator_delete');
Route::get('/grupa-modula/{id?}/{glavni_modul_id?}', 'AdminPermissionController@grupa_modula');
Route::post('/modul-grupa-edit', 'AdminPermissionController@modul_grupa_edit');
Route::get('/modul-grupa-delete/{id}', 'AdminPermissionController@modul_grupa_delete');

//stranice
Route::get('/stranice/{stranica_id}/{jezik_id?}', 'Admin@stranice');
Route::post('/pages', 'Admin@pages');
Route::post('/position', 'Admin@position');
Route::post('/position-baner', 'Admin@position_baner');
//footer
Route::get('/footer', 'Admin@footer');
Route::post('/footer_edit', 'Admin@footer_edit');
//preduzece
Route::get('/kontakt-podaci', 'AdminKontaktController@kontakt_podaci');
Route::post('/kontakt_update', 'AdminKontaktController@kontakt_update');
Route::post('/upload_logo', 'AdminKontaktController@upload_logo');
//baneri i slajderi
Route::post('/banner_uload', 'Admin@banner_uload');
Route::post('/baneri_delete', 'Admin@baneri_delete');
Route::post('/css_edit', 'Admin@css_edit');
Route::post('/image_upload', 'Admin@image_upload');
Route::post('/image_upload_opis', 'Admin@image_upload_opis');
Route::post('/delete_image', 'Admin@delete_image');
Route::post('/delete_page', 'Admin@delete_page');
// Pozadinska slika
Route::post('/bg-img-edit', 'AdminBannersController@bg_uload');
Route::get('/bg-img-delete', 'AdminBannersController@bgImgDelete');

//podesavanja
Route::post('/setings_update', 'Admin@setings_update');
Route::post('/options_setings_update', 'Admin@options_setings_update');
//email opcije
Route::post('/save_email_options', 'Admin@save_email_options');
Route::post('/save-google-analytics', 'Admin@save_google_analytics');
Route::post('/save-chat', 'Admin@save_chat');
Route::post('/save-color', 'Admin@save_color');
Route::post('/save-intesa-keys', 'Admin@save_intesa_keys');

Route::get('/', 'Admin@index');
Route::get('/baneri-slajdovi/{id}/{type}/{jezik_id?}', 'AdminBannersController@banners_sliders');
Route::post('/banner-edit', 'AdminBannersController@banner_edit');
Route::get('/baneri-delete/{id}', 'AdminBannersController@baneri_delete');
Route::get('/futer/{id}/{jezik_id?}', 'AdminFooterController@footer');
Route::post('/futer-edit', 'AdminFooterController@footer_edit');
Route::get('/futer-delete/{id}', 'AdminFooterController@footer_delete');
Route::get('/futer-page-delete/{id}/{web_b2c_seo_id}', 'AdminFooterController@footer_page_delete');
Route::post('/position-footer-section', 'AdminFooterController@position_footer_section');

Route::get('/podesavanja', 'Admin@podesavanja');
Route::get('/css_reset', 'Admin@css_reset');
Route::get('/css_submit', 'Admin@css_submit');
Route::get('/upload_images_pages', 'Admin@upload_images_pages');
Route::post('/aktivan-modul', 'Admin@aktivan_modul');


// artikli
Route::get('/artikli/{grupa_pr_id}/{proizvodjac?}/{dobavljac?}/{tip?}/{karakteristika?}/{magacin?}/{exporti?}/{filteri?}/{flag?}/{search?}/{nabavna?}/{order?}', 'AdminArticlesController@artikli');
Route::get('/product/{id}/{clone_id?}', 'AdminArticlesController@product');
Route::post('/product-edit', 'AdminArticlesController@product_edit');
Route::get('/product-short/{id}/{clone_id?}', 'AdminArticlesController@product_short');
Route::post('/product-edit-short', 'AdminArticlesController@product_edit_short');
Route::get('/product-delete/{roba_id}/{check?}', 'AdminArticlesController@product_delete');
Route::get('/products-delete/{roba_id}/{check?}', 'AdminArticlesController@products_delete');
Route::post('/products-import', 'AdminArticlesController@products_import');

//slike
Route::get('/product_slike/{id}', 'AdminArticlesController@product_slike');
Route::post('/slika-edit', 'AdminArticlesController@slika_edit');
Route::post('/slika-delete', 'AdminArticlesController@slika_delete');
Route::post('/slike-more-delete', 'AdminArticlesController@slike_more_delete');
Route::get('/slika-delete/{id}/{web_slika_id}/{short?}', 'AdminArticlesController@slika_delete_get');

//opis
Route::get('/product_opis/{id}', 'AdminArticlesController@product_opis');
Route::post('/opis_edit', 'AdminArticlesController@opis_edit');

//seo
Route::get('/product_seo/{id}/{jezik_id?}', 'AdminArticlesController@product_seo');
Route::post('/seo_edit', 'AdminArticlesController@seo_edit');

// karakteristike
Route::get('/product_karakteristike/{id}', 'AdminArticlesController@product_karakteristike');
Route::post('/karakteristike_edit', 'AdminArticlesController@karakteristike_edit');
Route::get('/product_generisane/{id}', 'AdminArticlesController@product_generisane');
Route::get('/dobavljac_karakteristike/{id}', 'AdminArticlesController@product_od_dobavljaca');
Route::get('/vezani_artikli/{id}/{vezani?}', 'AdminArticlesController@vezani_artikli');
Route::post('/grupa-karakteristike', 'AdminArticlesController@grupa_karakteristike');

// Product Osobine
Route::get('/product_osobine/{id}/{osobina_naziv_id}', 'AdminOsobineController@product_osobine_index');
Route::post('/product_osobine/{id}', 'AdminOsobineController@product_osobine_add');
Route::get('/product_osobine/{id}/{osobina_naziv_id}/all', 'AdminOsobineController@product_osobine_add_all');
Route::post('/product_osobine/{id}/{osobina_naziv_id}', 'AdminOsobineController@product_osobine_edit');
Route::get('/product_osobine/{roba_id}/{osobina_naziv_id}/{osobina_vrednost_id}/deletegroup', 'AdminOsobineController@product_osobine_grupa_delete');
Route::get('/product_osobine/{roba_id}/{osobina_naziv_id}/{osobina_vrednost_id}/delete', 'AdminOsobineController@product_osobine_delete');
Route::get('/product_osobine/{roba_id}/{osobina_naziv_id}/{osobina_vrednost_id}/deleteall', 'AdminOsobineController@product_osobine_delete_all');
Route::post('/position-osobine', 'AdminOsobineController@position_osobine');

// akcija
Route::get('/product_akcija/{id}', 'AdminArticlesController@product_akcija');
Route::post('/akcija_edit', 'AdminArticlesController@akcija_edit');

// dodatni fajlovi
Route::get('/dodatni_fajlovi/{id}/{extension_id?}', 'AdminArticlesController@dodatni_fajlovi');
Route::post('/dodatni_fajlovi_upload', 'AdminArticlesController@dodatni_fajlovi_upload');
Route::get('/dodatni_fajlovi_delete/{roba_id}/{web_file_id}', 'AdminArticlesController@dodatni_fajlovi_delete');

// ajax upload slika
Route::get('/upload-image-article', 'AdminArticlesController@uploadImageArticle');
Route::post('/upload-image-add', 'AdminArticlesController@uploadImageAdd');
Route::get('/upload-image-delete/{image}', 'AdminArticlesController@uploadImageDelete');

// AJAX
Route::post('/ajax/articles', 'AdminArticlesController@ajaxArticles');



// EXPORT XLS
Route::get('/export_xls', 'AdminArticlesController@export_xls');

// Vesti
Route::get('/vesti/{active?}', 'AdminVestiController@vesti');
Route::get('/vest/{id}/{jezik_id?}', 'AdminVestiController@vest');
Route::post('/vesti/{id}/save', 'AdminVestiController@saveNews');
Route::get('/vesti/{id}/delete', 'AdminVestiController@deleteNews');
Route::get('/vesti/f/{id}', 'AdminVestiController@filter');


// grupe
Route::get('/grupe/{id}/{grupa_pr_naziv_id?}/{jezik_id?}', 'AdminGroupsController@grupe');
Route::post('/grupa-edit', 'AdminGroupsController@grupa_edit');
Route::get('/grupa-delete/{grupa_pr_id}', 'AdminGroupsController@grupa_delete');
Route::post('/position-value', 'AdminGroupsController@position_value');
Route::post('/position-char', 'AdminGroupsController@position_char');

// proizvodjac
Route::get('/proizvodjac/{proizvodjac_id}/{jezik_id?}', 'AdminSupportController@proizvodjac');
Route::post('/proizvodjac-edit', 'AdminSupportController@proizvodjac_edit');
Route::get('/proizvodjac-delete/{proizvodjac_id}', 'AdminSupportController@proizvodjac_delete');

// mesta
Route::get('/mesto/{mesto_id?}', 'AdminSupportController@mesto');
Route::post('/mesto-edit', 'AdminSupportController@mesto_edit');
Route::get('/mesto-delete/{mesto_id}', 'AdminSupportController@mesto_delete');

// tipovi
Route::get('/tip/{tip_artikla_id}', 'AdminSupportController@tip_artikla');
Route::post('/tip-edit', 'AdminSupportController@tip_artikla_edit');
Route::get('/tip-delete/{tip_artikla_id}', 'AdminSupportController@tip_artikla_delete');

// jedinice mere
Route::get('/jedinica-mere/{jedinica_mere_id}', 'AdminSupportController@jedinica_mere');
Route::post('/jedinica-mere-edit', 'AdminSupportController@jedinica_mere_edit');
Route::get('/jedinica-mere-delete/{jedinica_mere_id}', 'AdminSupportController@jedinica_mere_delete');

// poreske stope
Route::get('/poreske-stope/{tarifna_grupa_id?}', 'AdminSupportController@poreske_stope');
Route::post('/poreske-stope-edit', 'AdminSupportController@poreske_stope_edit');
Route::get('/poreske-stope-delete/{tarifna_grupa_id}', 'AdminSupportController@poreske_stope_delete');


// stanje-artikla
Route::get('/stanje-artikla/{roba_flag_cene_id?}', 'AdminSupportController@stanje_artikla');
Route::post('/stanje-artikla-edit', 'AdminSupportController@stanje_artikla_edit');
Route::get('/stanje-artikla-delete/{roba_flag_cene_id}', 'AdminSupportController@stanje_artikla_delete');

// status-narudzbine
Route::get('/status_narudzbine/{narudzbina_status_id?}', 'AdminSupportController@status_narudzbine');
Route::post('/status_narudzbine_edit', 'AdminSupportController@status_narudzbine_edit');
Route::get('/status_narudzbine_delete/{narudzbina_status_id}', 'AdminSupportController@status_narudzbine_delete');
// kurirske-sluzbe
Route::get('/kurirska_sluzba/{narudzbina_status_id?}', 'AdminSupportController@kurirska_sluzba');
Route::post('/kurirska_sluzba_edit', 'AdminSupportController@kurirska_sluzba_edit');
Route::get('/kurirska_sluzba_delete/{narudzbina_status_id}', 'AdminSupportController@kurirska_sluzba_delete');
// konfigurator
Route::get('/konfigurator/{konfigurator_id?}', 'AdminSupportController@konfigurator');
Route::post('/konfigurator-edit', 'AdminSupportController@konfigurator_edit');
Route::get('/konfigurator-delete/{konfigurator_id}', 'AdminSupportController@konfigurator_delete');

// Komentari
Route::get('/komentari', 'AdminSupportController@komentari');
Route::get('/komentari/{id}', 'AdminSupportController@komentar');
Route::post('/komentari/{id}', 'AdminSupportController@updateComment');
Route::get('/komentari/{id}/delete', 'AdminSupportController@deleteComment');

// Osobine
Route::get('/osobine/{id?}', 'AdminOsobineController@index');
Route::post('/osobine/{id?}', 'AdminOsobineController@editProperty');
Route::post('/osobine/{id}/{osobina_vrednost_id}', 'AdminOsobineController@editPropertyValue');
Route::get('/osobine/{id}/delete', 'AdminOsobineController@deleteProperty');
Route::get('/osobine/{id}/{vrednost_id}/delete', 'AdminOsobineController@deletePropertyValue');

// Način plaćanja
Route::get('/nacin_placanja/{id}', 'AdminNacinPlacanjaController@index');
Route::post('/nacin_placanja/{id}', 'AdminNacinPlacanjaController@edit');
Route::post('/nacin_placanja/{id}/delete', 'AdminNacinPlacanjaController@delete');

// Način isporuke
Route::get('/nacin_isporuke/{id}', 'AdminNacinIsporukeController@index');
Route::post('/nacin_isporuke/{id}', 'AdminNacinIsporukeController@edit');
Route::post('/nacin_isporuke/{id}/delete', 'AdminNacinIsporukeController@delete');

// Troskovi isporuke
Route::get('/troskovi-isporuke', 'AdminTroskoviIsporukeController@index');
Route::post('/troskovi-isporuke-save', 'AdminTroskoviIsporukeController@save');
Route::get('/troskovi-isporuke-delete/{id}', 'AdminTroskoviIsporukeController@delete');

Route::get('/cena-isporuke', 'AdminTroskoviIsporukeController@cena');
Route::post('/cena-isporuke-save', 'AdminTroskoviIsporukeController@cena_save');
Route::get('/cena-isporuke-delete/{id}', 'AdminTroskoviIsporukeController@cena_delete');

// Unos kursa
Route::get('/kurs/{id}', 'AdminKursController@index');
Route::post('/kurs/edit', 'AdminKursController@edit');
Route::get('/kurs/{id}/nbs', 'AdminKursController@kurs_nbs');

// Aploadovanje fajlova
Route::get('/file-upload', 'AdminFilesController@index');
Route::post('/file-upload-save', 'AdminFilesController@save');

// Podaci za import
Route::get('/import_podaci_grupa/{partner_id}/{partner_grupa_id}','AdminImportController@import_podaci_grupa');
Route::post('/import_podaci_save','AdminImportController@import_podaci_save');
Route::get('import_podaci_delete/{partner_id}/{partner_grupa_id}','AdminImportController@import_podaci_delete');
Route::get('/import-podaci-proizvodjac/{partner_id}/{partner_proizvodjac_id}', 'AdminImportController@import_podaci_proizvodjac');
Route::post('/import-podaci-proizvodjac-save', 'AdminImportController@import_podaci_proizvodjac_save');
Route::get('import-podaci-proizvodjac-delete/{partner_id}/{partner_proizvodjac_id}', 'AdminImportController@import_podaci_proizvodjac_delete');

//narudzbine
Route::get('/porudzbine/{status}/{narudzbina_status_id}/{datum_od}/{datum_do}/{search}', 'AdminNarudzbineController@porudzbine');
Route::get('/porudzbina/delete/{web_b2c_narudzbina_id}', 'AdminNarudzbineController@porudzbinaDelete');
Route::get('/narudzbina/{web_b2c_narudzbina_id}/{roba_id?}', 'AdminNarudzbineController@narudzbina');
Route::post('/narudzbina', 'AdminNarudzbineController@updateNarudzbinu');
Route::get('/narudzbina-stavka/{narudzbina_stavka_id}', 'AdminNarudzbineController@narudzbina_stavka');
Route::post('/narudzbina-stavka', 'AdminNarudzbineController@narudzbina_stavka_update');
Route::get('/posta-posalji/{web_b2c_narudzbina_id}/{posta_slanje_id}', 'AdminNarudzbineController@posta_posalji');
Route::get('/posta-resetuj/{web_b2c_narudzbina_id}/{posta_slanje_id}', 'AdminNarudzbineController@posta_resetuj');
Route::get('/pdf/{web_b2c_narudzbina_id}', 'AdminNarudzbineController@pdf');
Route::get('/pdf_racun/{web_b2c_narudzbina_id}', 'AdminNarudzbineController@pdf_racun');
Route::get('/pdf_predracun/{web_b2c_narudzbina_id}', 'AdminNarudzbineController@pdf_predracun');
Route::get('/pdf_ponuda/{web_b2c_narudzbina_id}', 'AdminNarudzbineController@pdf_ponuda');


// analitika
Route::get('/analitika/{od?}/{do?}','AdminAnalysisController@analitikaGrupa');
// Route::get('/analitika', 'AdminAnalysisController@analitika');
// Route::get('/analitika/{datum_od?}/{datum_do?}/{id?}', 'AdminAnalysisController@analitika');

// Kupci i partneri
Route::get('/kupci_partneri', 'AdminKupciPartneriController@getIndex');

Route::get('/kupci_partneri/kupci/{id}', 'AdminKupciPartneriController@getKupac');
Route::get('/kupci_partneri/kupci/{id}/{confirm}/delete', 'AdminKupciPartneriController@deleteKupac');
Route::get('/kupci_partneri/kupci/{id}/snimi_kao_partner', 'AdminKupciPartneriController@snimiKaoPartner');
Route::post('/kupci_partneri/kupci/{id}/save', 'AdminKupciPartneriController@saveKupac');
Route::get('/kupci_partneri/kupci/{vrsta_kupca?}/{status_registracija?}/{search?}', 'AdminKupciPartneriController@getKupci');
Route::post('/kupci_partneri/kupci/{vrsta_kupca?}/{status_registracija?}', 'AdminKupciPartneriController@getKupciSearch');

Route::get('/kupci_partneri/partneri/{search?}', 'AdminKupciPartneriController@getPartneri');
Route::post('/kupci_partneri/partneri/', 'AdminKupciPartneriController@partneriSearch');
Route::get('/kupci_partneri/partner/{id?}', 'AdminKupciPartneriController@getPartner');
Route::get('/kupci_partneri/partneri/{id}/{confirm}/delete', 'AdminKupciPartneriController@deletePartner');
Route::post('/kupci_partneri/partneri/{id}/save', 'AdminKupciPartneriController@savePartner');
Route::get('/kupci_partneri/export_kupac/{vrsta_fajla}/{vrsta_kupca}/{status_registracije}/{imena_kolona?}/{search?}', 'AdminKupciPartneriController@exportKupac');
Route::get('/kupci_partneri/export_partner/{vrsta_fajla}/{imena_kolona?}/{search?}', 'AdminKupciPartneriController@exportMailsPartner');

Route::get('/kupci_partneri/export_newslette', 'AdminKupciPartneriController@export_newSletter');

Route::get('/exportpartner', 'AdminKupciPartneriController@exportPartner');

Route::post('/partner/card-refresh/{partner_id}', 'AdminKupciPartneriController@card_refresh');


Route::get('/update_db/{old?}', 'AdminDBUpdateVersion@update');
Route::get('/update_old_db', 'AdminDBUpdateVersion@update_old');
Route::get('/resolve', 'AdminDBUpdateVersion@resolve_encoding');

// admin import
Route::get('/web_import/{grupa_pr_id?}/{proizvodjac?}/{dobavljac?}/{filteri?}/{search?}/{order?}/{nabavna?}', 'AdminImportController@web_import');
Route::post('/ajax/dc_articles', 'AdminAjaxController@ajaxDcArticles');
Route::post('/upload_web_import', 'AdminImportController@upload_web_import');
Route::post('/web_import_ajax', 'AdminImportFileController@web_import_ajax');

Route::get('/crm_zadaci', 'AdminCRMController@index');

Route::get('/export-external/{export_sifra}/{kind}','AdminExportController@export_external');


//ajax
Route::post('/ajax/groups', 'AdminGroupsController@ajaxGroups');
Route::post('/ajax/generisane', 'AdminAjaxController@ajaxGenerisane');
Route::post('/ajax/od_dobavljaca', 'AdminAjaxController@ajaxOdDobavljaca');
Route::post('/ajax/povezani', 'AdminAjaxController@ajaxPovezani');
Route::post('/ajax/articles_search', 'AdminAjaxController@articles_search');
Route::post('/ajax/narudzbine_search', 'AdminNarudzbineController@narudzbineSearch');
Route::post('/ajax/delete_stavka', 'AdminNarudzbineController@deleteStavka');
Route::post('/ajax/update_kolicina', 'AdminNarudzbineController@updateKolicina');
Route::post('/ajax/editCena', 'AdminNarudzbineController@editCena');

Route::post('/ajax/promena_statusa_narudzbine', 'AdminNarudzbineController@promena_statusa_narudzbine');
Route::post('/ajax/porudzbina_vise', 'AdminNarudzbineController@porudzbina_vise');
Route::post('/ajax/prihvati', 'AdminNarudzbineController@prihvati');
Route::post('/ajax/realizuj', 'AdminNarudzbineController@realizuj');
Route::post('/ajax/storniraj', 'AdminNarudzbineController@storniraj');
Route::post('/ajax/nestorniraj', 'AdminNarudzbineController@nestorniraj');
Route::post('/ajax/obrisi', 'AdminNarudzbineController@obrisi');
Route::post('/ajax/narudzbina-kupac', 'AdminNarudzbineController@narudzbina_kupac');

Route::post('/ajax/vrsta-partnera', 'AdminAjaxController@vrstaPartnera');

Route::post('/ajax/permissions', 'AdminAjaxController@permissions');

Route::post('/ajax/support', 'AdminAjaxController@support');

Route::get('/ajax/import/groups', 'AdminAjaxController@selectGroups');
Route::get('/ajax/import/brands', 'AdminAjaxController@renderBrands');
Route::get('/ajax/import/brands_mul', 'AdminAjaxController@renderBrandsMultiple');
Route::get('/ajax/import/tarifna', 'AdminAjaxController@renderTarifna');


//global ajax
Route::post('/ajax', 'AdminAjaxController@ajax');

Route::get('/sitemap-generate', 'AdminSeoController@sitemap_generate');

//ADMIN FRONT
Route::post('/front-admin-save', 'AdminFrontController@front_admin_save');
Route::post('/front-admin-product-list', 'AdminFrontProductsController@product_list');
Route::post('/front-admin-product-list-execute', 'AdminFrontProductsController@product_list_execute');
Route::post('/front-admin-product', 'AdminFrontProductsController@product');

});
Route::any('/posta-odgovor', 'AdminNarudzbineController@posta_odgovor');




/////////////////////////////          ADMIN B2B          ///////////////////////////////////////////////

Route::group(array('prefix'=>'admin/b2b','before'=>'b2b_admin'),function (){
    Route::get('/', 'AdminB2BController@index');
    Route::get('/login-to-portal', 'AdminB2BController@login_to_portal');
    
    Route::get('/narudzbine/{status}/{narudzbina_status_id}/{datum_od}/{datum_do}/{search}/', 'AdminB2BNarudzbinaController@narudzbine');
 	Route::get('/narudzbina/{web_b2b_narudzbina_id}/{roba_id?}', 'AdminB2BNarudzbinaController@narudzbina');
 	Route::post('/narudzbina', 'AdminB2BNarudzbinaController@updateNarudzbinu');

    Route::post('/ajax/b2b_porudzbina_vise', 'AdminB2BNarudzbinaController@porudzbina_vise');
    Route::post('/ajax/b2b_promena_statusa_narudzbine', 'AdminB2BNarudzbinaController@b2b_promena_statusa_narudzbine');

    Route::get('/analitika/{id}','AdminB2BAnalitikaController@analitikaGrupa');
    Route::get('/analitika', 'AdminB2BAnalitikaController@analitika');
    Route::get('/analitika/prihod/{odd?}/{dod?}', 'AdminB2BAnalitikaController@analitika');
	Route::get('/analitika/{datum_od?}/{datum_do?}/{id?}', 'AdminB2BAnalitikaController@analitika');

    Route::get('/article-list/{grupa_pr_id}/{proizvodjac?}/{dobavljac?}/{magacin?}/{filteri?}/{search?}/{nabavna?}/{order?}', 'AdminB2BArticlesController@article_list');

    Route::get('/proizvodjaci', 'AdminB2BPartnerController@proizvodjaci');
    Route::get('/partneri', 'AdminB2BPartnerController@partneri');
    Route::post('/partneri_search', 'AdminB2BPartnerController@partneri_search');
    Route::get('/podesavanja', 'AdminB2BSettingsController@index');
    Route::post('/settings', 'AdminB2BSettingsController@settings');

	Route::post('/ajax/prihvati', 'AdminB2BNarudzbinaController@prihvati');
	Route::post('/ajax/realizuj', 'AdminB2BNarudzbinaController@realizuj');
	Route::post('/ajax/storniraj', 'AdminB2BNarudzbinaController@storniraj');
	Route::post('/ajax/nestorniraj', 'AdminB2BNarudzbinaController@nestorniraj');
	Route::post('/ajax/obrisi', 'AdminB2BNarudzbinaController@obrisi');
	Route::post('/ajax/editCena', 'AdminB2BNarudzbinaController@editCena');
	Route::post('/ajax/delete_stavka', 'AdminB2BNarudzbinaController@deleteStavka');
	Route::post('/ajax/narudzbine_search', 'AdminB2BNarudzbinaController@narudzbineSearch');
	Route::post('/ajax/update_kolicina', 'AdminB2BNarudzbinaController@updateKolicina');

	

	Route::get('/pdf/{web_b2b_narudzbina_id}', 'AdminB2BNarudzbinaController@pdf');
	Route::get('/pdf_racun/{web_b2b_narudzbina_id}', 'AdminB2BNarudzbinaController@pdf_racun');
	Route::get('/pdf_predracun/{web_b2b_narudzbina_id}', 'AdminB2BNarudzbinaController@pdf_predracun');
	Route::get('/pdf_ponuda/{web_b2b_narudzbina_id}', 'AdminB2BNarudzbinaController@pdf_ponuda');
    //IS
    Route::get('/is', 'AdminB2BISController@is');
    Route::post('/is/load', 'AdminB2BISController@custom_load');
    Route::post('is/file-upload', 'AdminB2BISController@file_upload');

    //WINGS
    Route::group(array('prefix'=>'wings','before'=>'wings'),function (){
        Route::post('/post', 'AdminB2BISController@wings_post');
    });
    Route::group(array('prefix'=>'calculus','before'=>'calculus'),function (){
        Route::post('/post', 'AdminB2BISController@calculus_post');
    });
    Route::group(array('prefix'=>'infograf','before'=>'infograf'),function (){
        Route::post('/post', 'AdminB2BISController@infograf_post');
    });
    Route::group(array('prefix'=>'logik','before'=>'logik'),function (){
        Route::post('/post', 'AdminB2BISController@logik_post');
    });
    Route::group(array('prefix'=>'xml','before'=>'xml_is'),function (){
        Route::post('/post', 'AdminB2BISController@xml_post');
    });

    // Rabat i kategorija
    Route::post('ajax/rabat_edit', 'AdminB2BArticlesController@ajax');
    Route::post('ajax/rabat_partner_edit', 'AdminB2BPartnerController@rabat_partner_edit');
    
    Route::post('ajax/kategorija_partner_edit', 'AdminB2BPartnerController@kategorija_partner_edit');
    Route::post('ajax/rabat_group_edit', 'AdminB2BArticlesController@ajax');
    
    Route::post('ajax/akc_rabat_group_edit', 'AdminB2BArticlesController@ajax');
    Route::post('ajax/rabat_proizvodjac_edit', 'AdminB2BPartnerController@ajax');
    Route::get('rabat_kombinacije', 'AdminB2BPartnerController@rabat_kombinacije');
    Route::post('rabat_kombinacije', 'AdminB2BPartnerController@rabat_kombinacije_edit');
    Route::post('rabat_kombinacije/{id?}/delete', 'AdminB2BPartnerController@rabat_kombinacije_delete');

    
    // Stranice
    Route::get('/b2b_stranice/{stranica_id}/{jezik_id?}', 'AdminB2BPagesController@stranice');
    Route::post('/pages', 'AdminB2BPagesController@store');
    Route::post('/delete_page', 'AdminB2BPagesController@delete_page');
    Route::post('/image_upload', 'AdminB2BPagesController@image_upload');
    Route::post('/delete_image', 'AdminB2BPagesController@delete_image');
   
    Route::get('/baneri-slajdovi/{id}/{type}', 'AdminB2BBaneriController@banners_sliders');
	Route::post('/banner-edit', 'AdminB2BBaneriController@banner_edit');
	Route::get('/baneri-delete/{id}', 'AdminB2BBaneriController@baneri_delete');
	Route::post('/bg-img-edit', 'AdminB2BBaneriController@bg_uload');
	Route::get('/bg-img-delete', 'AdminB2BBaneriController@bgImgDelete');
	Route::post('/position', 'AdminB2BController@position');
	Route::post('/position-baner', 'AdminB2BBaneriController@position_baner');
	
	Route::get('/vesti/{active?}', 'AdminB2BVestiController@vesti');
	Route::get('/vest/{id}/{jezik_id?}', 'AdminB2BVestiController@vest');
	Route::post('/vesti/{id}/save', 'AdminB2BVestiController@saveNews');
	Route::get('/vesti/{id}/delete', 'AdminB2BVestiController@deleteNews');
	Route::get('/vesti/f/{id}', 'AdminB2BVestiController@filter');

    // partner kategorije
	Route::get('/partner_kategorija/{id_kategorije?}', 'AdminB2BPartnerController@partner_kategorija');
	Route::post('/partner_kategorija-edit', 'AdminB2BPartnerController@kategorije_edit');
	Route::get('/partner_kategorija-delete/{id_kategorije}', 'AdminB2BPartnerController@kategorije_delete');

});


//automatika za import i is
Route::group(array('before'=>'hash_protect'),function (){
    Route::get('/auto-full-import/{key}','AdminImportFileController@auto_full_import');
    Route::get('/auto-short-import/{key}/{type?}','AdminImportFileController@auto_short_import');
    Route::get('/auto-import-3/{key}','AdminImportFileController@auto_import_3');

    //is
    Route::get('/auto-import-is/{key}','AdminB2BISController@auto_import_is');
});

//export
Route::group(array('before'=>'hash_protect_export'),function (){
    
    Route::get('/direct-export/{export_sifra}/{kind}/{key}','AdminExportController@direct_export');
    Route::get('/export/{export_sifra}/{kind}/{key}','AdminExportController@export');
    Route::get('/export/export-grupe/{export_id}/{kind}/{key}','AdminExportController@export_grupe');
    Route::post('/export/grupe-povezi/{key}','AdminExportController@grupe_povezi');
    Route::post('/export/grupe-razvezi/{key}','AdminExportController@grupe_razvezi');
    Route::post('/export/grupa-file/{key}','AdminExportController@grupa_file');

    Route::get('/export/auto/{key}', 'AdminExportController@shopmania_auto_export');
    Route::post('/export/shopmania-config/{key}','AdminExportController@shopmania_config');
    Route::post('/export/magento-config/{key}','AdminExportController@magento_config');
    Route::post('/export/woocommerce-config/{key}','AdminExportController@woocommerce_config');
    Route::post('/export/shopify-config/{key}','AdminExportController@shopify_config');
    Route::post('/export/kupindo-config/{key}','AdminExportController@kupindo_config');
    Route::post('/export/ceners-config/{key}','AdminExportController@ceners_config');

});
Route::get('/backup/{backup_sifra}/{kind}/{key}','AdminBackupController@backup');





/////////////////////////////          B2B          ///////////////////////////////////////////////

Route::get('/b2b','B2bController@index');
//B2B Login
Route::get('/b2b/registration',array('as'=>'b2b.registration','uses'=>'B2bController@registration'));
Route::post('/b2b/registration',array('as'=>'b2b.registration.store','uses'=>'B2bController@registrationStore'));
Route::get('/b2b/zaboravljena-lozinka',array('as'=>'b2b.forgot_password','uses'=>'B2bController@forgotPassword'));
Route::post('/b2b/zaboravljena-lozinka',array('as'=>'b2b.forgot_password_post','uses'=>'B2bController@forgotPasswordPost'));
Route::get('/b2b/login','B2bController@login');
Route::post('/b2b/login/store',array('as'=>'b2b.login.store', 'uses'=>'B2bController@loginStore'));
//B2B
Route::group(array('prefix'=>'b2b','before'=>'b2b'),function (){
    Route::get('/logout','B2bController@logout');
    Route::get('/ordering/{type}',array('as'=>'b2b.ordering','uses'=>'B2bController@ordering'));
    Route::get('/pretraga',array('as'=>'b2b.search','uses'=>'B2bController@search'));
    Route::get('/user_edit',array('as'=>'b2b.user_edit','uses'=>'B2bController@userEdit'));
    Route::post('/user_edit',array('as'=>'b2b.user_update','uses'=>'B2bController@userUpdate'));
    Route::get('/user/card/{date_start?}/{date_end?}','B2bController@userCard');
    Route::post('/user/card-refresh','B2bController@cardRefresh');
    Route::get('/artikli/{grupa1}', array('as'=>'products_first','uses'=>'B2bController@products_first'));
    Route::get('/artikli/{grupa1}/{grupa2}', array('as'=>'products_second','uses'=>'B2bController@products_second'));
    Route::get('/artikli/{grupa1}/{grupa2}/{grupa3}', array('as'=>'products_third','uses'=>'B2bController@products_third'));
    Route::get('/artikli/{grupa1}/{grupa2}/{grupa3}/{grupa4}', array('as'=>'products_fourth','uses'=>'B2bController@products_fourth'));
    Route::get('/prikaz/{prikaz}', 'B2bController@prikaz');
    Route::get('/valuta/{valuta}', 'B2bController@b2b_valuta');
    Route::get('/artikal/{artikal}', array('as'=>'b2b.product','uses'=>'B2bController@product'));
    Route::post('/cart_add', array('as'=>'b2b.cart_add','uses'=>'B2bController@cartAdd'));
    Route::post('/cart_delete', array('as'=>'b2b.cart_delete','uses'=>'B2bController@cartDelete'));
    Route::post('/cart_delete_all', array('as'=>'b2b.cart_delete_all','uses'=>'B2bController@cartDeleteAll'));
    Route::get('/cart_content', array('as'=>'b2b.cart_content','uses'=>'B2bController@cartContent'));
    Route::post('/check_out', array('as'=>'b2b.check_out','uses'=>'B2bController@checkOut'));
    Route::get('/porudzbina/{bill}', array('as'=>'b2b.order','uses'=>'B2bController@order'));
    Route::get('/artikli-tip/{type}/{page}', array('as'=>'b2b.products.type','uses'=>'B2bController@productsType'));
    Route::get('/blog', 'VestiB2bController@getNews');
    Route::get('/blog/{id?}', 'VestiB2bController@oneNew');
    Route::get('{page}',array('as'=>'b2b.pages','uses'=>'B2bController@page'));
    Route::get('/brendovi', 'B2bController@brendovi');
    Route::get('/servis','B2bController@servis');
    Route::get('/rma','B2bController@servis');
    Route::get('/kontakt','B2bController@kontakt');
    Route::post('/servis_send','B2bController@opisKvaraMail');
    Route::get('/proizvodjac/{naziv}/{grupa?}', 'B2bBrendoviController@proizvodjac');


});


/////////////////////////////       SHOP         ///////////////////////////////////////////////

$shop_filter = Language::multi() ? array('prefix'=>'{lang}','before'=>'lang') : array('before'=>'lang');
Route::group($shop_filter,function (){
    // Vesti
    Route::get('/'.Url_mod::convert_url('blog'), 'VestiController@getNews');
    Route::get('/'.Url_mod::convert_url('blog').'/{blog_slug?}', 'VestiController@oneNew');

    Route::post('/login-post', 'LoginRegistration@user_login_post');
    Route::post('/login', 'LoginRegistration@user_login');
    Route::get('/logout', 'LoginRegistration@logout');
    Route::post('/zaboravljena-lozinka', 'LoginRegistration@forget_password');
    Route::post('/zaboravljena-lozinka-ajax', 'LoginRegistration@forget_password_ajax');
    Route::post('/registracija-post', 'LoginRegistration@user_registracija');
    Route::get('/'.Url_mod::convert_url('potvrda-registracije').'/{kod}/{web_kupac_id}', 'LoginRegistration@confirm_user');
    Route::get('/'.Url_mod::convert_url('korisnik').'/{username}', array('before'=>'user_filter', 'uses'=>'LoginRegistration@korisnik'));
    Route::get('/'.Url_mod::convert_url('lista-zelja').'/{username}', array('before'=>'user_filter', 'uses'=>'LoginRegistration@lista_zelja'));
    Route::post('/korisnik-edit', 'LoginRegistration@korisnik_edit');

    Route::post('/newsletter', 'MainController@newsletter_add');
    Route::get('/'.Url_mod::convert_url('newsletter-potvrda').'/{kod}', 'MainController@newsletter_confirm');
    Route::post('/meil_send', 'MailController@meil_send');
    Route::post('/coment_add','MainController@coment_add');

	Route::post('/quick-view-cart-add', 'CartController@quick_view_cart_add');
    Route::post('/list-cart-add', 'CartController@list_cart_add');
    Route::post('/wish-list-add', 'CartController@wish_list_add');
    Route::post('/wish-list-delete', 'CartController@wish_list_delete');
    Route::post('/product-cart-add', 'CartController@product_cart_add');
    Route::post('/konfigurator-cart-add', 'CartController@konfigurator_cart_add');
    Route::post('/vezani-cart-add', 'CartController@vezani_cart_add');
    Route::post('/cart-add-sub', 'CartController@cart_add_sub');
    Route::post('/cart-stavka-delete', 'CartController@cart_stavka_delete');
    Route::post('/cart-delete', 'CartController@cart_delete');

    Route::post('/select-narudzbina-mesto', 'OrderController@select_narudzbina_mesto');
    Route::post('/select-narudzbina-ulica', 'OrderController@select_narudzbina_ulica');
    Route::post('/order-create', 'OrderController@order_create');
    Route::get('/'.Url_mod::convert_url('narudzbina').'/{web_b2c_narudzbina_id}', 'OrderController@narudzbina');
    Route::get('/intesa/{web_b2c_narudzbina_id}', 'IntesaBankingController@intesa');
    Route::post('/intesa-response', 'IntesaBankingController@intesa_response');
    Route::get('/'.Url_mod::convert_url('intesa-odgovor').'/{web_b2c_narudzbina_id}','IntesaBankingController@intesa_failure');

    Route::post('/livesearch', 'SearchController@livesearch');

    Route::get('/'.Url_mod::convert_url('prikaz').'/{prikaz}', 'ArticlesController@prikaz');
    Route::get('/'.Url_mod::convert_url('limit').'/{limit}', 'ArticlesController@limit');
    Route::get('/'.Url_mod::convert_url('valuta').'/{valuta}', 'ArticlesController@valuta');
    Route::get('/'.Url_mod::convert_url('sortiranje').'/{sortiranje}', 'ArticlesController@sortiranje');
    Route::get('/search/{search}/{grupa_pr_id?}', 'SearchController@search');

    Route::post('/compare', 'CompareController@compare');
    Route::post('/compareajax', 'CompareController@compareajax');
    Route::post('/clearCompare', 'CompareController@clear_compare');
    Route::post('/tip-ajax', 'TipController@tip_ajax');

	Route::post('/quick-view', 'ArticleController@quick_view');

    Route::get('/'.Url_mod::convert_url('konfigurator').'/{id}', 'ConfiguratorController@konfigurator');
    Route::get('/'.Url_mod::convert_url('brendovi'), 'BrendoviController@brend_list');
    Route::get('/'.Url_mod::convert_url('proizvodjac').'/{naziv}/{grupa?}', 'BrendoviController@proizvodjac');
    Route::get('/'.Url_mod::convert_url('tip').'/{naziv}/{grupa?}', 'TipController@tip_products');
    Route::get('/'.Url_mod::convert_url('akcija').'/{grupa?}', 'AkcijaController@akcija_products');

    // Tagovi
    Route::get('/'.Url_mod::convert_url('tagovi').'/{tag}', 'TagsController@index');
	Route::get('/'.Url_mod::convert_url('svi-artikli'), 'ArticlesController@artikli_all');
	Route::post('/link-artikla', 'ArticleController@article_link');
    Route::get('/'.Url_mod::convert_url('artikal').'/{article}', 'ArticleController@article');
    Route::get('/{strana}', 'MainController@page');
    Route::get('/{grupa1}/{proizvodjac?}/{karakteristike?}/{cene?}', 'ArticlesController@artikli_first');
    Route::get('/{grupa1}/{grupa2}/{proizvodjac?}/{karakteristike?}/{cene?}', 'ArticlesController@artikli_second');
    Route::get('/{grupa1}/{grupa2}/{grupa3}/{proizvodjac?}/{karakteristike?}/{cene?}', 'ArticlesController@artikli_third');
    Route::get('/{grupa1}/{grupa2}/{grupa3}/{grupa4}/{proizvodjac?}/{karakteristike?}/{cene?}', 'ArticlesController@artikli_forth');
    Route::get('/', 'MainController@lang');
});

Route::get('/', 'MainController@index');


Route::get('{slug}',function(){
    if(($new_link = Url_mod::checkOldLink(str_replace(Options::base_url(),'',Request::url()))) != null){
        return Redirect::to($new_link);
    }
    $data = array(
        "strana"=>'Not found',
        "title"=>'Not found',
        "description"=>'Not found',
        "keywords"=>'Not found'
        );
	$content = View::make('shop/themes/'.Support::theme_path().'pages/not_found', $data)->render();
	return Response::make($content, 404);
})->where('slug', '^.*');