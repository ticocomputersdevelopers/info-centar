<?php 

class B2bBasket {
    public static function addCartB2b($roba_id,$quantity, $status, $partner_id){
        if(Session::has('b2b_cart')){
            $product = DB::table('web_b2b_korpa_stavka')->where('web_b2b_korpa_id',Session::get('b2b_cart'))->where('roba_id',$roba_id)->first();
            //update
            if($product){
                $data = array();
                if($status == 1){
                    $data = ['kolicina'=>$quantity];
                }
                else if($status==2){
                    $data = ['kolicina'=>$product->kolicina+$quantity];
                }
                else if($status==3) {
                    $data = ['kolicina'=>$product->kolicina-$quantity];
                }
                DB::table('web_b2b_korpa_stavka')->where('web_b2b_korpa_id',Session::get('b2b_cart'))->where('roba_id',$roba_id)->update($data);
            }
            //insert
            else{
                $roba = DB::table('roba')->where('roba_id',$roba_id)->first();
                $rabatCene = B2bArticle::b2bRabatCene($roba_id);
                $data = [
                  'web_b2b_korpa_id'=>Session::get('b2b_cart'),
                  'broj_stavke'=>self::getNextB2bItem(Session::get('b2b_cart')),
                  'roba_id'=>$roba_id,
                  'kolicina'=>$quantity,
                  'jm_cena'=>$rabatCene->ukupna_cena,
                  'tarifna_grupa_id'=>$roba->tarifna_grupa_id,
                  'racunska_cena_nc'=>$roba->racunska_cena_end
                ];
                DB::table('web_b2b_korpa_stavka')->insert($data);
            }
        }
        else{

            $data = DB::table('web_b2b_korpa')->insert(['partner_id'=>$partner_id,'datum'=>date('Y-m-d')]);
            $cart = DB::table('web_b2b_korpa')->where('partner_id',$partner_id)->where('datum',date('Y-m-d'))->orderBy('web_b2b_korpa_id','desc')->first();
            Session::put('b2b_cart',$cart->web_b2b_korpa_id);

            $roba = DB::table('roba')->where('roba_id',$roba_id)->first();
            $rabatCene = B2bArticle::b2bRabatCene($roba_id);

            $data = [
                'web_b2b_korpa_id'=>Session::get('b2b_cart'),
                'broj_stavke'=>self::getNextB2bItem(Session::get('b2b_cart')),
                'roba_id'=>$roba_id,
                'kolicina'=>$quantity,
                'jm_cena'=>$rabatCene->ukupna_cena,
                'tarifna_grupa_id'=>$roba->tarifna_grupa_id,
                'racunska_cena_nc'=>$roba->racunska_cena_end
            ];
            DB::table('web_b2b_korpa_stavka')->insert($data);
        }
    }
    public static function getB2bQuantityItem($roba_id){
        if(Session::has('b2b_cart')){
            $cart = DB::table('web_b2b_korpa_stavka')->where('web_b2b_korpa_id',Session::get('b2b_cart'))->where('roba_id',$roba_id)->first();
            if($cart){
                return $cart->kolicina;
            }
            else {
                return 0;
            }
        }
        else{
            return 0;
        }
    }
    public static function b2bCountItems(){
        if(Session::has('b2b_cart')){
            $cart = DB::table('web_b2b_korpa_stavka')->where('web_b2b_korpa_id',Session::get('b2b_cart'));
            return $cart->count();
        }
        else {
            return 0;
        }
    }
	public static function cena($cena,$valuta_show = true){
        //Dinar
        
        if(Session::get('b2b_valuta')!="2"){ 
            $valuta = ' <span>din.</span>';          
            if($cena!="0"){            
                $iznos = number_format($cena, 2, ',', '.');
            }else{
                $iznos = '0.00';
            }
        //Euro
        }else{
        	$kurs=B2bOptions::kurs();
            $valuta = ' <span>eur.</span>'; 
            if($cena!="0"){            
                $iznos = number_format($cena/$kurs, 2, ',', '.');
            }else{
                $iznos = '0.00';
            }
        }
        if($valuta_show == true){
            return $iznos.$valuta;
        }else{
            return $iznos;
        }
    }
    public static function  getNextB2bItem($web_b2b_korpa_id){
        $b2bCart = DB::table('web_b2b_korpa_stavka')->where('web_b2b_korpa_id',$web_b2b_korpa_id)->count();

        return $b2bCart+1;

    }
   public static function total(){
       $sumOsnovnaCena = 0;
       $sumPdvCena = 0;
       $sumUkupnaCena = 0;
       $cart = DB::table('web_b2b_korpa_stavka')->where('web_b2b_korpa_id', Session::get('b2b_cart'))->get();
       foreach($cart as $row){
            $stavka = B2bArticle::b2bRabatCene($row->roba_id);
            $sumOsnovnaCena += $stavka->cena_sa_rabatom*$row->kolicina;
            $sumPdvCena += $stavka->cena_sa_rabatom * ($stavka->porez/100)*$row->kolicina;
            $sumUkupnaCena += $stavka->ukupna_cena*$row->kolicina;
       }
        return (object) array(
            'porez_cena' => $sumPdvCena,
            'cena_sa_rabatom' => $sumOsnovnaCena,
            'ukupna_cena' => $sumUkupnaCena
            );
   }

   public static function orderTotal($web_b2b_narudzbina_id){
       $sumOsnovnaCena = 0;
       $sumPdvCena = 0;
       $sumUkupnaCena = 0;
       $cart = DB::table('web_b2b_narudzbina_stavka')->where('web_b2b_narudzbina_id', $web_b2b_narudzbina_id)->get();
       foreach($cart as $row){
            $stavka = B2bArticle::b2bRabatCene($row->roba_id);
            $sumOsnovnaCena += $stavka->cena_sa_rabatom*$row->kolicina;
            $sumPdvCena += $stavka->cena_sa_rabatom * ($stavka->porez/100)*$row->kolicina;
            $sumUkupnaCena += $stavka->ukupna_cena*$row->kolicina;
       }
        return (object) array(
            'porez_cena' => $sumPdvCena,
            'cena_sa_rabatom' => $sumOsnovnaCena,
            'ukupna_cena' => $sumUkupnaCena
            );
   }

    public static function nacin_isporuke(){
        
        foreach(DB::table('web_nacin_isporuke')->where('selected',1)->get() as $row){
            
            echo '<option id="'.$row->web_nacin_isporuke_id.'" value="'.$row->web_nacin_isporuke_id.'">'.$row->naziv.'</option> ';
        }
        
    }
   public static function b2bTaxPrice(){
       $sum = 0;
       $cart = DB::table('web_b2b_korpa_stavka')->where('web_b2b_korpa_id', Session::get('b2b_cart'))->get();

       foreach($cart as $row){
           $roba = DB::table('roba')->where('roba_id',$row->roba_id)->first();
           $rab_p = B2bArticle::b2bPartnerGroupRabat($roba->grupa_pr_id, Session::get('b2b_user_'.B2bOptions::server()));
           $priceDiscount = B2bArticle::procenat_m($row->jm_cena,$rab_p);
           $tax = B2bArticle::b2bTax($roba->tarifna_grupa_id);
           $sum += B2bArticle::procenat($priceDiscount, $tax)*$row->kolicina;
       }
       return $sum;

   }
    public static function nacin_placanja(){
        
         foreach(DB::table('web_nacin_placanja')->where('selected',1)->get() as $row){
            
            echo '<option id="'.$row->web_nacin_placanja_id.'" value="'.$row->web_nacin_placanja_id.'">'.$row->naziv.'</option> ';
        }
    }
    public static function b2bItemPrice($roba_id, $basic_price){

        $roba = DB::table('roba')->where('roba_id',$roba_id)->first();
        $cartItem = DB::table('web_b2b_korpa_stavka')->where('roba_id',$roba_id)->where('web_b2b_korpa_id',Session::get('b2b_cart'))->first();
        $rab_p = B2bArticle::b2bPartnerGroupRabat($roba->grupa_pr_id, Session::get('b2b_user_'.B2bOptions::server()));
        $priceDiscount = B2bArticle::procenat_m($basic_price,$rab_p);
        $tax = B2bArticle::b2bTax($roba->tarifna_grupa_id);
        return B2bArticle::procenat_p($priceDiscount, $tax)*$cartItem->kolicina;


    }
    public static function getNameNacinIsporuke($web_nacin_isporuke_id){
        $ni = DB::table('web_nacin_isporuke')->where('web_nacin_isporuke_id',$web_nacin_isporuke_id)->first();
        return $ni->naziv;
    }
    public static function getNameNacinPlacanja($web_nacin_placanja_id){
        $np = DB::table('web_nacin_placanja')->where('web_nacin_placanja_id',$web_nacin_placanja_id)->first();
        return $np->naziv;
    }
    public static function mesto($mesto_id){
        return DB::table('mesto')->where('mesto_id',$mesto_id)->pluck('mesto');
    }
    public static function b2bOrderBasicPrice($order_id){
        $sum = 0;
        $cart = DB::table('web_b2b_narudzbina_stavka')->where('web_b2b_narudzbina_id', $order_id)->get();
        foreach($cart as $row){
            $product = DB::table('roba')->where('roba_id',$row->roba_id)->first();
            $rab_p = B2bArticle::b2bPartnerGroupRabat($product->grupa_pr_id, Session::get('b2b_user_'.B2bOptions::server()));

            $sum += B2bArticle::procenat_m($row->jm_cena,$rab_p)*$row->kolicina;
        }
        return $sum;
    }
    public static function b2bOrderTaxPrice($order_id){
        $sum = 0;
        $cart = DB::table('web_b2b_narudzbina_stavka')->where('web_b2b_narudzbina_id', $order_id)->get();

        foreach($cart as $row){
            $roba = DB::table('roba')->where('roba_id',$row->roba_id)->first();
            $rab_p = B2bArticle::b2bPartnerGroupRabat($roba->grupa_pr_id, Session::get('b2b_user_'.B2bOptions::server()));
            $priceDiscount = B2bArticle::procenat_m($row->jm_cena,$rab_p);
            $tax = B2bArticle::b2bTax($roba->tarifna_grupa_id);
            $sum += B2bArticle::procenat($priceDiscount, $tax)*$row->kolicina;
        }
        return $sum;

    }
}