<?php
namespace IsInfograf;

use DB;

class DBData {

	public static function groups(){
		return DB::connection('infograf')->select("SELECT * FROM grupa_real");
	}

	public static function manufacturers(){
		return DB::connection('infograf')->select("SELECT * FROM proizvodjaci");
	}

	// public static function articles(){
	// 	return DB::connection('infograf')->select("SELECT * FROM artikal WHERE cena > 0 AND IDPorez = 4");
	// }

	public static function articles($stockroom_id){
		return DB::connection('infograf')->select("SELECT * FROM artikal LEFT JOIN lager ON artikal.IDArtikal = lager.idartikal WHERE cena > 0 AND IDPorez = 4 AND (lager.mag = ".strval($stockroom_id)." OR lager.mag IS NULL OR lager.idartikal IS NULL)");
	}

	public static function stock($stockroom_id){
		return DB::connection('infograf')->select("SELECT * FROM lager WHERE mag = ".strval($stockroom_id)."");
	}

	public static function partners(){
		return DB::connection('infograf')->select("SELECT * FROM partneri WHERE neaktivan = 0");
	}

	public static function images(){
		return DB::connection('infograf')->select("SELECT * FROM slike WHERE tip = 0");
	}

	public static function courses(){
		return DB::connection('infograf')->select("SELECT * FROM kurs");
	}
}