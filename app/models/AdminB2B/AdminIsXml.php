<?php
ini_set('memory_limit', '-1');
ini_set('max_execution_time', 3600);

use IsXml\FileData;
use IsXml\Article;
use IsXml\ArticlePartnerGroup;
use IsXml\Stock;
use IsXml\Partner;



class AdminIsXml {

    public static function execute(){
        try {
            //partner
            $partners = FileData::partners();
            $resultPartner = Partner::table_body($partners);
            Partner::query_insert_update($resultPartner->body);

            //articles
            $articleDetails = FileData::articles();
            $resultArticle = Article::table_body($articleDetails->articles);
            Article::query_insert_update($resultArticle->body,array('flag_aktivan','grupa_pr_id','web_opis','jedinica_mere_id','tarifna_grupa_id','barkod','racunska_cena_nc','web_cena','mpcena','akcijska_cena','akcija_flag_primeni'));
            // Article::query_update_unexists($resultArticle->body);

            $articlePartnerGroup = ArticlePartnerGroup::table_body($articleDetails->partner_categories);
            ArticlePartnerGroup::query_insert_update($articlePartnerGroup->body);
            ArticlePartnerGroup::query_delete_unexists($articlePartnerGroup->body);

            //b2b stock
            if(AdminB2BOptions::info_sys('xml')->b2b_magacin){
                $resultStock = Stock::table_body($articleDetails->articles);
                Stock::query_insert_update($resultStock->body);
            }
            //b2c stock
            if(AdminB2BOptions::info_sys('xml')->b2c_magacin){
                $resultStock = Stock::table_body($articleDetails->articles,1);
                Stock::query_insert_update($resultStock->body);
            }

            return (object) array('success'=>true);
        }catch (Exception $e){
            return (object) array('success'=>false,'message'=>$e->getMessage());
        }
    }



}