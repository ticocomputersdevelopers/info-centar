<?php
use ISWings\WingsAPI;
use ISWings\Article;
use ISWings\Lager;
use ISWings\Partner;


class AdminISWings {

    public static function execute(){
        try {
            $wings_data = AdminB2BOptions::info_sys('wings');
            $username = $wings_data->username;
            $password = $wings_data->password;
            $token = WingsAPI::autorize($username,$password);

            if($token){
                $magacini = WingsAPI::magacini($token);
                $articles = array();
                foreach(range(1,100) as $page){
                    $articles = array_merge($articles,WingsAPI::lager($token,$magacini[0]->id,$page));
                }
                
                //articles
                $resultArticle = Article::table_body($articles);
                Article::query_insert_update($resultArticle->body,array('id_is','naziv','jedinica_mere_id','proizvodjac_id','naziv_displej','racunska_cena_nc','racunska_cena_end','naziv_web','web_opis','akcija_flag_primeni','barkod'));
                Article::query_update_unexists($resultArticle->body);
                
                //lager
                $resultLager = Lager::table_body($articles);
                Lager::query_insert_update($resultLager->body);

                //partner
                $partners = WingsAPI::parneri_svi($token);
                $resultPartner = Partner::table_body($partners);
                Partner::query_insert_update($resultPartner->body,array('id_kategorije'));

                return (object) array('success'=>true);
            }
            return (object) array('success'=>false,'message'=>'Podaci za pristup su neispravni.');
        }catch (Exception $e){
            return (object) array('success'=>false,'message'=>$e->getMessage());
        }
    }



}