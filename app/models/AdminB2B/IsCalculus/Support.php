<?php
namespace ISCalculus;

use DB;


class Support {

	public static function updateGroupsParent($groups){
		foreach($groups as $group){
			$group_id = $group->ID;
			$sifra = $group->sifra;
			$level = $group->nivo;
			$parent_id = isset($group->IDnadredjene) && $group->IDnadredjene != 0 ? intval($group->IDnadredjene) : 0;
			if($parent_id != 0 && $level < 4){
				$parrent_grupa_pr = DB::table('grupa_pr')->where('id_is',$parent_id)->first();
				if(!is_null($parrent_grupa_pr)){
					$parrent_grupa_pr_id = $parrent_grupa_pr->grupa_pr_id;
					DB::table('grupa_pr')->where(array('sifra_is'=>$sifra,'id_is'=>$group_id))->update(array('parrent_grupa_pr_id'=>$parrent_grupa_pr_id));
				}
			}
		}

	}

	public static function getJedinicaMereId($jedinica_mere){
		$jm = DB::table('jedinica_mere')->where('naziv',$jedinica_mere)->first();

		if(is_null($jm)){
			DB::table('jedinica_mere')->insert(array(
				'jedinica_mere_id' => DB::table('jedinica_mere')->max('jedinica_mere_id')+1,
				'naziv' => $jedinica_mere
				));
			$jm = DB::table('jedinica_mere')->where('naziv',$jedinica_mere)->first();
		}
		return $jm->jedinica_mere_id;
	}

	public static function getProizvodjacId($proizvodjac){
		$pro = DB::table('proizvodjac')->where('naziv',$proizvodjac)->first();

		if(is_null($pro)){
			DB::table('proizvodjac')->insert(array(
				'naziv' => $proizvodjac,
				'sifra_connect' => 0
				));
			$pro = DB::table('proizvodjac')->where('naziv',$proizvodjac)->first();
		}
		return $pro->proizvodjac_id;
	}

	public static function getGrupaId($is_grupa_id){
		$grupa_pr = DB::table('grupa_pr')->where('id_is',$is_grupa_id)->first();
		if(!is_null($grupa_pr)){
			return $grupa_pr->grupa_pr_id;
		}
		return -1;
	}

}