<?php
namespace IsLogik;

use mysqli;
use DB;

class DBData {

	public static function groups(){
		return DB::connection('logik')->select("SELECT * FROM kategorije WHERE semaphore = 2");
	}
	public static function article_groups($article_id){
		return DB::connection('logik')->select("SELECT * FROM artikli_kategorije WHERE sifra_artikla_logik = ".strval($article_id)." AND semaphore = 2");
	}
	public static function articles_groups(){
		return DB::connection('logik')->select("SELECT sifra_artikla_logik, sifra_kategorije_logik FROM artikli_kategorije WHERE (sifra_artikla_logik, sifra_kategorije_logik) NOT IN (SELECT sifra_artikla_logik, sifra_kategorije FROM artikli) AND semaphore = 2");
	}

	public static function articles(){
		return DB::connection('logik')->select("SELECT DISTINCT *, (SELECT sifra_kategorije_logik FROM artikli_kategorije ak WHERE sifra_artikla_logik = a.sifra_artikla_logik AND sifra_kategorije_logik <> a.sifra_kategorije AND sifra_kategorije_logik IN (SELECT sifra_kategorije_logik FROM kupci_kategorije) LIMIT 1) AS sporedna_grupa_id FROM artikli a WHERE semaphore = 2");
	}
	public static function partners(){
		return DB::connection('logik')->select("SELECT * FROM kupci WHERE vrsta_kupca = 2 AND naziv_firme IS NOT NULL AND naziv_firme <> '' AND semaphore = 2");
	}
	public static function partners_groups_rabat(){
		return DB::connection('logik')->select("SELECT DISTINCT * FROM kupci_kategorije WHERE semaphore = 2");
	}
	public static function partners_cards(){
		return DB::connection('logik')->select("SELECT * FROM kartica_kupca WHERE semaphore = 2");
	}




	public static function update_groups(){
		DB::connection('logik')->statement("UPDATE kategorije SET semaphore = 0 WHERE semaphore = 2");
	}
	public static function update_articles(array $ids_is){
		if(count($ids_is) > 0){
			$articles = DB::table('roba')->select('id_is','roba_id')->whereNotNull('id_is')->whereIn('id_is',$ids_is)->get();

			foreach($articles as $article){
				DB::connection('logik')->statement("UPDATE artikli SET sifra_artikla_tico = '".strval($article->roba_id)."', semaphore = 0 WHERE sifra_artikla_logik = ".$article->id_is."");
			}
		}
	}
	public static function update_articles_groups(array $mapped_articles){
		if(count($mapped_articles) > 0){

			$articles_groups = DB::connection('logik')->select("SELECT sifra_artikla_logik FROM artikli_kategorije WHERE semaphore = 2");
			foreach($articles_groups as $article_group) {
				if(isset($mapped_articles[$article_group->sifra_artikla_logik])){
					DB::connection('logik')->statement("UPDATE artikli_kategorije SET sifra_artikla_tico = '".strval($mapped_articles[$article_group->sifra_artikla_logik])."', semaphore = 0 WHERE sifra_artikla_logik = ".$article_group->sifra_artikla_logik." AND semaphore = 2");
				}
			}
		}

	}
	public static function update_partners(array $ids_is){
		if(count($ids_is) > 0){
			$partners = DB::table('partner')->select('id_is','partner_id')->whereNotNull('id_is')->whereIn('id_is',$ids_is)->get();
			foreach($partners as $partner){
				DB::connection('logik')->statement("UPDATE kupci SET sifra_kupca_tico = '".strval($partner->partner_id)."', semaphore = 0 WHERE sifra_kupca_logik = ".$partner->id_is." AND vrsta_kupca = 2 AND naziv_firme IS NOT NULL AND naziv_firme <> '' AND semaphore = 2");
			}
		}
	}
	public static function update_partners_groups(array $mapped_result){

		if(count($mapped_result) > 0){
			foreach($mapped_result as $row){
				DB::connection('logik')->statement("UPDATE kupci_kategorije SET sifra_kupca_tico = '".$row->partner_id."', sifra_kategorije_tico = '".$row->grupa_pr_id."', semaphore = 0 WHERE sifra_kupca_logik = ".strval($row->sifra_kupca_logik)." AND sifra_kategorije_logik = ".strval($row->sifra_kategorije_logik)." AND semaphore = 2");
			}
		}
	}

	public static function update_partners_cards(array $mapped_result){
		if(count($mapped_result) > 0){
			foreach($mapped_result as $sifra_kupca_logik => $partner_id){
				DB::connection('logik')->statement("UPDATE kartica_kupca SET sifra_kupca_tico = '".$partner_id."', semaphore = 0 WHERE sifra_kupca_logik = ".strval($sifra_kupca_logik)." AND semaphore = 2");
			}
		}
	}

}