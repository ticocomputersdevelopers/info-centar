<?php
namespace IsLogik;

use DB;


class Support {

	public static function updateGroupsParent($groups){
		foreach($groups as $group){
			$group_id = $group->sifra_kategorije_logik;
			$parent_id = isset($group->sifra_nadkategorije_logik) && $group->sifra_nadkategorije_logik != 0 ? intval($group->sifra_nadkategorije_logik) : '0';
			if($parent_id != 0){
				$parrent_grupa_pr = DB::table('grupa_pr')->where('id_is',$parent_id)->first();
				if(!is_null($parrent_grupa_pr)){
					$parrent_grupa_pr_id = $parrent_grupa_pr->grupa_pr_id;
					DB::table('grupa_pr')->where(array('id_is'=>$group_id))->update(array('parrent_grupa_pr_id'=>$parrent_grupa_pr_id));
				}else{
					DB::table('grupa_pr')->where('parrent_grupa_pr_id',$parent_id)->delete();
				}
			}
		}

	}

	public static function getPartnerId($sifra_kupca_logik){
		$partner = DB::table('partner')->where('id_is',$sifra_kupca_logik)->first();
		if(!is_null($partner)){
			return $partner->partner_id;
		}
		return null;
	}

	public static function getTarifnaGrupaId($naziv_tarifne_grupe,$vrednost_tarifne_grupe){
		$tg = DB::table('tarifna_grupa')->where(array('porez'=>$vrednost_tarifne_grupe))->first();

		if(is_null($tg)){
			$next_id = DB::table('tarifna_grupa')->max('tarifna_grupa_id')+1;
			DB::table('tarifna_grupa')->insert(array(
				'tarifna_grupa_id' => $next_id,
				'sifra' => $next_id,
				'naziv' => substr($naziv_tarifne_grupe0,0,99),
				'porez' => $vrednost_tarifne_grupe,
				'active' => 1,
				'default_tarifna_grupa' => 0,
				'tip' => substr($vrednost_tarifne_grupe,0,19)
				));
			$tg = DB::table('tarifna_grupa')->where('porez',$vrednost_tarifne_grupe)->first();
		}
		return $tg->tarifna_grupa_id;
	}

	public static function getJedinicaMereId($jedinica_mere){
		$jm = DB::table('jedinica_mere')->where('naziv',$jedinica_mere)->first();

		if(is_null($jm)){
			DB::table('jedinica_mere')->insert(array(
				'jedinica_mere_id' => DB::table('jedinica_mere')->max('jedinica_mere_id')+1,
				'naziv' => $jedinica_mere
				));
			$jm = DB::table('jedinica_mere')->where('naziv',$jedinica_mere)->first();
		}
		return $jm->jedinica_mere_id;
	}

	public static function getProizvodjacId($proizvodjac){
		$pro = DB::table('proizvodjac')->where('naziv',$proizvodjac)->first();

		if(is_null($pro)){
			DB::table('proizvodjac')->insert(array(
				'naziv' => $proizvodjac,
				'sifra_connect' => 0
				));
			$pro = DB::table('proizvodjac')->where('naziv',$proizvodjac)->first();
		}
		return $pro->proizvodjac_id;
	}

	public static function getMappedArticles(){
		$mapped = array();
		$articles = DB::table('roba')->whereNotNull('id_is')->where('id_is','!=','')->get();
		foreach($articles as $article){
			$mapped[$article->id_is] = $article->roba_id;
		}
		return $mapped;
	}
	public static function getMappedGroups(){
		$mapped = array();
		$groups = DB::table('grupa_pr')->whereNotNull('id_is')->where('id_is','!=','')->get();
		foreach($groups as $group){
			$mapped[$group->id_is] = $group->grupa_pr_id;
		}
		return $mapped;
	}
	public static function getMappedPartners(array $id_is){
		$mapped = array();
		if(count($id_is) > 0){
			$partners = DB::table('partner')->select('id_is','partner_id')->whereNotNull('id_is')->whereIn('id_is',$id_is)->get();
			foreach($partners as $partner){
				$mapped[$partner->id_is] = $partner->partner_id;
			}
		}
		return $mapped;
	}

	public static function getGrupaId($is_grupa_id){
		if(!is_null($is_grupa_id) && $is_grupa_id != ''){
			$grupa_pr = DB::table('grupa_pr')->where('id_is',intval($is_grupa_id))->first();
			if(!is_null($grupa_pr)){
				return $grupa_pr->grupa_pr_id;
			}
		}
		return -1;
	}

	public static function getManufacturerId($manufacturer_id){
		$proizvodjac = DB::table('proizvodjac')->where('id_is',$manufacturer_id)->first();
		if(!is_null($proizvodjac)){
			return $proizvodjac->proizvodjac_id;
		}
		return -1;
	}

	public static function convert($text){
	    $text = preg_replace("/[áàâãªä]/u","a",$text);
	    $text = preg_replace("/[ÁÀÂÃÄ]/u","A",$text);
	    $text = preg_replace("/[ÍÌÎÏ]/u","I",$text);
	    $text = preg_replace("/[íìîï]/u","i",$text);
	    $text = preg_replace("/[éèêë]/u","e",$text);
	    $text = preg_replace("/[ÉÈÊË]/u","E",$text);
	    $text = preg_replace("/[óòôõºö]/u","o",$text);
	    $text = preg_replace("/[ÓÒÔÕÖ]/u","O",$text);
	    $text = preg_replace("/[úùûü]/u","u",$text);
	    $text = preg_replace("/[ÚÙÛÜ]/u","U",$text);
	    $text = preg_replace("/[’‘‹›‚]/u","'",$text);
	    $text = preg_replace("/[“”«»„]/u",'"',$text);
	    $text = str_replace("–","-",$text);
	    $text = str_replace(" "," ",$text);
	    $text = str_replace("ç","c",$text);
	    $text = str_replace("Ç","C",$text);
	    $text = str_replace("ñ","n",$text);
	    $text = str_replace("Ñ","N",$text);
	 
	    //2) Translation CP1252. &ndash; => -
	    $trans = get_html_translation_table(HTML_ENTITIES); 
	    $trans[chr(130)] = '&sbquo;';    // Single Low-9 Quotation Mark 
	    $trans[chr(131)] = '&fnof;';    // Latin Small Letter F With Hook 
	    $trans[chr(132)] = '&bdquo;';    // Double Low-9 Quotation Mark 
	    $trans[chr(133)] = '&hellip;';    // Horizontal Ellipsis 
	    $trans[chr(134)] = '&dagger;';    // Dagger 
	    $trans[chr(135)] = '&Dagger;';    // Double Dagger 
	    $trans[chr(136)] = '&circ;';    // Modifier Letter Circumflex Accent 
	    $trans[chr(137)] = '&permil;';    // Per Mille Sign 
	    $trans[chr(138)] = '&Scaron;';    // Latin Capital Letter S With Caron 
	    $trans[chr(139)] = '&lsaquo;';    // Single Left-Pointing Angle Quotation Mark 
	    $trans[chr(140)] = '&OElig;';    // Latin Capital Ligature OE 
	    $trans[chr(145)] = '&lsquo;';    // Left Single Quotation Mark 
	    $trans[chr(146)] = '&rsquo;';    // Right Single Quotation Mark 
	    $trans[chr(147)] = '&ldquo;';    // Left Double Quotation Mark 
	    $trans[chr(148)] = '&rdquo;';    // Right Double Quotation Mark 
	    $trans[chr(149)] = '&bull;';    // Bullet 
	    $trans[chr(150)] = '&ndash;';    // En Dash 
	    $trans[chr(151)] = '&mdash;';    // Em Dash 
	    $trans[chr(152)] = '&tilde;';    // Small Tilde 
	    $trans[chr(153)] = '&trade;';    // Trade Mark Sign 
	    $trans[chr(154)] = '&scaron;';    // Latin Small Letter S With Caron 
	    $trans[chr(155)] = '&rsaquo;';    // Single Right-Pointing Angle Quotation Mark 
	    $trans[chr(156)] = '&oelig;';    // Latin Small Ligature OE 
	    $trans[chr(159)] = '&Yuml;';    // Latin Capital Letter Y With Diaeresis 
	    $trans['euro'] = '&euro;';    // euro currency symbol 
	    ksort($trans); 
	     
	    foreach ($trans as $k => $v) {
	        $text = str_replace($v, $k, $text);
	    }
	 
	    // 3) remove <p>, <br/> ...
	    $text = strip_tags($text); 
	     
	    // 4) &amp; => & &quot; => '
	    $text = html_entity_decode($text);
	     
	    // 5) remove Windows-1252 symbols like "TradeMark", "Euro"...
	    $text = preg_replace('/[^(\x20-\x7F)]*/','', $text); 
	     
	    $targets=array('\r\n','\n','\r','\t');
	    $results=array(" "," "," ","");
	    $text = str_replace($targets,$results,$text);
	     
		return ($text);
	}

}