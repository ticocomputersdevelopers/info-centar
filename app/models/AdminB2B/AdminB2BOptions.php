<?php

class AdminB2BOptions {

	public static function base_url(){
		
		return DB::table('options')->where('options_id',1316)->pluck('str_data');
		
	}

    public static function server(){
        return DB::table('options')->where('options_id',1326)->pluck('str_data');
    }

    public static function options($option_id,$str_data=true){
        if($str_data){
            return DB::table('options')->where('options_id',$option_id)->pluck('str_data');
        }else{
            return DB::table('options')->where('options_id',$option_id)->pluck('int_data');
        }
    }

	public static function checkB2B(){
		if(in_array(DB::table('web_options')->where('web_options_id',130)->pluck('int_data'),array(1,2))){
			return true;
		}else{
			return false;
		}
	}

    public static function check_admin($ac_module_ids=null){
        $imenik_id = Session::get('b2c_admin'.self::server());
        if($imenik_id == 0){
            return true;
        }
        if($ac_module_ids == 0){
            return false;
        }       
        $ac_group_id = intval(DB::table('imenik')->where('imenik_id',$imenik_id)->pluck('kvota'));
        $count = DB::table('ac_group_module')->where(array('ac_group_id'=>intval($ac_group_id),'alow'=>1))->whereIn('ac_module_id',$ac_module_ids)->count();

        if($count > 0){
            return true;
        }else{
            return false;
        }
    }
    public static function get_admin(){
        $user = DB::table('imenik')->where('imenik_id',Session::get('b2c_admin'.self::server()))->first();
        return $user->ime.' '.$user->prezime;
    }
    public static function checkB2BNarudzbine(){
		if(DB::table('web_options')->where('web_options_id',135)->pluck('int_data')==1){
			return true;
		}else{
			return false;
		}
	}
	public static function search_conver($str){
		 $string_new=strtolower($str); 
		$urlKey = strtr($string_new, array("&frasl;"=>"/"," "=>"%"));
	
		return $urlKey;
	}
	public static function text_convert($string){
		$urlKey = strtr($string, array("?"=>"&scaron;","?"=>"&#269;","?"=>"&#263;","?"=>"&#273;","?"=>"&#382;","?"=>"&Scaron;","?"=>"&#268;","?"=>"&#262;","?"=>"&#272;","?"=>"&#381;",'"'=>""));
    	return $urlKey;
	}
    public static function limit_liste_robe(){
        $limit =  DB::table('options')->where('options_id',1329)->pluck('int_data');
        if($limit > 400){
            $limit = 400;
        }
        return $limit;
    }
    public static function info_sys($sifra){
        return DB::table('informacioni_sistem')->where(array('sifra'=>$sifra,'aktivan'=>1,'izabran'=>1))->first();
    }

    public static function convert_url($str,$lang=null){
        return strtolower(AdminLanguage::slug_trans($str,$lang));
    }

    public static function kurs(){
        $obj = DB::table('kursna_lista')->orderBy('kursna_lista_id','desc')->first();
        if(isset($obj)){
            return $obj->prodajni;
        }else{
            return 0;
        }
    }
    public static function gnrl_options($options_id,$kind='int_data'){
        return DB::table('options')->where('options_id',$options_id)->pluck($kind);
    }

    public static function web_options($options_id){
        return DB::table('web_options')->where('web_options_id',$options_id)->pluck('int_data');
    }    


}
