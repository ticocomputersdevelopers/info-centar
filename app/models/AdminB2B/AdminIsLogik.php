<?php
ini_set('memory_limit', '-1');
ini_set('max_execution_time', 3600);

use IsLogik\DBData;

use IsLogik\Support;
use IsLogik\Group;
use IsLogik\ArticleGroup;
use IsLogik\Article;
use IsLogik\Stock;
use IsLogik\Partner;
use IsLogik\PartnerGroup;
use IsLogik\PartnerCard;


class AdminIsLogik {

    public static function execute(){
        try {
            //groups
            $groups = DBData::groups();
            $resultGroup = Group::table_body($groups);
            Group::query_insert_update($resultGroup->body,array('parrent_grupa_pr_id'));
            // Group::query_update_unexists($resultGroup->body);
            Support::updateGroupsParent($groups);
            DBData::update_groups();
            //articles
            $articles = DBData::articles();
            $resultArticle = Article::table_body($articles);
            Article::query_insert_update($resultArticle->body,array('flag_aktivan','grupa_pr_id','stara_grupa_id','jedinica_mere_id','tarifna_grupa_id','barkod','racunska_cena_nc','web_cena','mpcena','akcijska_cena','akcija_flag_primeni'));
            // Article::query_update_unexists($resultArticle->body);
            DBData::update_articles($resultArticle->ids_is);
            //articles_groups
            $articles_groups = DBData::articles_groups();
            $resultArticleGroup = ArticleGroup::table_body($articles_groups);
            ArticleGroup::query_insert_delete($resultArticleGroup->body);
            DBData::update_articles_groups($resultArticleGroup->mapped_articles);
            //b2b stock
            if(AdminB2BOptions::info_sys('logik')->b2b_magacin){
                $resultStock = Stock::table_body($articles);
                Stock::query_insert_update($resultStock->body);
            }
            //b2c stock
            if(AdminB2BOptions::info_sys('logik')->b2c_magacin){
                $resultStock = Stock::table_body($articles,1);
                Stock::query_insert_update($resultStock->body);
            }
            //partner
            $partners = DBData::partners();
            $resultPartner = Partner::table_body($partners);
            Partner::query_insert_update($resultPartner->body);
            DBData::update_partners($resultPartner->ids_is);

            //partner group
            $partnersGroups = DBData::partners_groups_rabat();
            $resultPartnersGroups = PartnerGroup::table_body($partnersGroups);
            PartnerGroup::query_insert_update($resultPartnersGroups->body);
            PartnerGroup::query_delete_unexists($resultPartnersGroups->body);
            DBData::update_partners_groups($resultPartnersGroups->mapped_result);

            //partner card
            $partnersCards = DBData::partners_cards();
            $resultPartnersCards = PartnerCard::table_body($partnersCards);
            PartnerCard::query_insert_update($resultPartnersCards->body);
            DBData::update_partners_cards($resultPartnersCards->mapped_result);


            return (object) array('success'=>true);
        }catch (Exception $e){
            return (object) array('success'=>false,'message'=>$e->getMessage());
        }
    }



}