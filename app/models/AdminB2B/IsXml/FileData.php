<?php
namespace IsXml;

use DB;
use File;

use All;


class FileData {

	public static function articles(){
		$file_path = "files/IS/xml/roba.xml";
		$products = array();
		$partner_categories = array();

		if(!File::exists($file_path)){
			return $products;
		}

		$xml_products = simplexml_load_file($file_path);
		foreach($xml_products as $xml_product){
			$xml_product_arr = (array) $xml_product;
			$xml_product_arr['cena_nc'] = strval($xml_product->cena);
			$products[] = (object) $xml_product_arr;


			$partner_categories[] = (object) array('sifra_artikla' => $xml_product->sifra_artikla, 'kategorija_partnera' => 
				array(
					(object) array('naziv' => 'KA', 'cena' => $xml_product->KA),
					(object) array('naziv' => 'LKA', 'cena' => $xml_product->LKA),
					(object) array('naziv' => 'TT', 'cena' => $xml_product->TT),
					(object) array('naziv' => 'RT', 'cena' => $xml_product->RT)
				)
			);
			
		}



		return (object) array('articles' => $products, 'partner_categories' => $partner_categories);
	}

	public static function partners(){
		$file_path = "files/IS/xml/partner.xml";
		$partner_categories = array('KA','LKA','TT','RT');

		$partners = array();
		if(!File::exists($file_path)){
			return $partners;
		}

		$xml_partners = simplexml_load_file($file_path);
		foreach($xml_partners as $xml_partner){
			$xml_partner_arr = (array) $xml_partner;
			$xml_partner_arr['id_is'] = strval($xml_partner->id_s);
			$xml_partner_arr['kategorija_partnera'] = $partner_categories[array_rand($partner_categories)];
			$partners[] = (object) $xml_partner_arr;
		}

		return $partners;
	}


}