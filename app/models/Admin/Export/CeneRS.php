<?php
namespace Export;

use Export\Support;
use DB;
use AdminOptions;
use AdminCommon;
use DOMDocument;
use Response;
use View;

class CeneRS {

	public static function execute($export_id,$kind,$short=false){

		$export_products = DB::select("SELECT roba_id, web_cena, naziv_web, web_flag_karakteristike, web_karakteristike, web_opis, (SELECT naziv FROM proizvodjac WHERE proizvodjac_id = roba.proizvodjac_id) AS proizvodjac, (SELECT grupa FROM grupa_pr WHERE grupa_pr_id = roba.grupa_pr_id) AS grupa, model, (SELECT ROUND(kolicina) FROM lager WHERE roba_id = roba.roba_id AND poslovna_godina_id = (SELECT poslovna_godina_id FROM poslovna_godina WHERE status=0) AND orgj_id = (SELECT orgj_id FROM imenik_magacin WHERE izabrani=1)) AS kolicina, akcija_flag_primeni, akcijska_cena, datum_akcije_od, datum_akcije_do, barkod, napomena, garancija, tezinski_faktor FROM roba WHERE roba_id IN (SELECT roba_id FROM roba_export WHERE export_id=".$export_id.") AND proizvodjac_id <> -1 AND flag_aktivan = 1 AND web_cena > 0");

		if($kind=='xml'){
			return self::xml_exe($export_products);
		}else{
			echo '<h2>Dati format nije podržan!</h2>';
		}

	}

	public static function xml_exe($products){
		$xml = new DOMDocument("1.0","UTF-8");
		$root = $xml->createElement("proizvodi");
		$xml->appendChild($root);

		foreach($products as $article){

			$product = $xml->createElement("proizvod");

		    Support::xml_node($xml,"sku",$article->roba_id,$product);
		    Support::xml_node_cdata($xml,"proizvodjac",$article->proizvodjac,$product);
		    Support::xml_node($xml,"kategorija_proizvoda",$article->grupa,$product);
		    Support::xml_node_cdata($xml,"naziv_proizvoda",$article->naziv_web,$product);
		    Support::xml_node_cdata($xml,"proizvod_url",Support::product_link_ceners($article->roba_id),$product);
		    Support::xml_node($xml,"cena",($article->akcija_flag_primeni==1?$article->akcijska_cena:$article->web_cena),$product);
		    Support::xml_node($xml,"ean_upc",$article->barkod,$product);
		    Support::xml_node_cdata($xml,"slika_url",Support::major_image($article->roba_id),$product);
			Support::xml_node_cdata($xml,"specifikacija",$article->web_opis."\n".Support::characteristics($article->roba_id,$article->web_flag_karakteristike,$article->web_karakteristike),$product);
		    Support::xml_node_cdata($xml,"cena_klika",'',$product);
		    Support::xml_node($xml,"datum_pocetka",$article->datum_akcije_od,$product);
		    Support::xml_node($xml,"datum_kraja",$article->datum_akcije_do,$product);
			Support::xml_node_cdata($xml,"max_cena_klika",'',$product);

			$root->appendChild($product);
		}

		$xml->formatOutput = true;
		$store_path = 'files/exporti/ceners/ceners.xml';
		$xml->save($store_path) or die("Error");

		// header('Content-type: text/xml');
		// //header('Content-Disposition: attachment; filename="CeneRS.xml"');
		// echo file_get_contents($store_path); die;	
		return $store_path;
	}

}