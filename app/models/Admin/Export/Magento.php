<?php
namespace Export;

use Export\Support;
use DB;
use AdminOptions;
use AdminCommon;
use AdminSupport;

class Magento {

	public static function execute($export_id,$kind,$config){

		$export_products = DB::select("SELECT roba_id, sku, web_cena, REPLACE(REPLACE(naziv_web,';',' '),',',' ') AS naziv_web, web_flag_karakteristike, web_karakteristike, REPLACE(REPLACE(web_opis,';',' '),',',' ') AS web_opis, grupa_pr_id, flag_prikazi_u_cenovniku, tarifna_grupa_id, akcija_flag_primeni, akcijska_cena, datum_akcije_od, datum_akcije_do, REPLACE(REPLACE((SELECT grupa FROM grupa_pr WHERE grupa_pr_id = roba.grupa_pr_id),';',' '),',',' ') AS grupa, (SELECT ROUND(kolicina) FROM lager WHERE roba_id = roba.roba_id AND poslovna_godina_id = (SELECT poslovna_godina_id FROM poslovna_godina WHERE status=0) AND orgj_id = (SELECT orgj_id FROM imenik_magacin WHERE izabrani=1)) AS kolicina FROM roba WHERE roba_id IN (SELECT roba_id FROM roba_export WHERE export_id=".$export_id.") AND flag_aktivan = 1 AND web_cena > 0");

		if($kind=='csv'){
			self::csv_exe($export_id,$export_products,$config);
		}else{
			echo '<h2>Dati format nije podržan!</h2>';
		}

	}

	public static function csv_exe($export_id,$products,$config){
		header('Content-type: text/csv; charset=UTF-8');
		// header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="magento-export.csv"');

		$header = array(
			'sku',
			isset($config['is_full'])?'store_view_code':'null',
			isset($config['is_full'])?'attribute_set_code':'null',
			isset($config['is_full'])?'product_type':'null',
			isset($config['category'])?'categories':'null',
			isset($config['is_full'])?'product_websites':'null',
			isset($config['name'])?'name':'null',
			isset($config['description'])?'description':'null',
			isset($config['short_description'])?'short_description':'null',
			isset($config['weight'])?'weight':'null',
			isset($config['visibility'])?'product_online':'null',
			isset($config['visibility'])?'visibility':'null',
			isset($config['tax'])?'tax_class_name':'null',
			isset($config['price'])?'price':'null',
			isset($config['action'])?'special_price':'null',
			isset($config['action'])?'special_price_from_date':'null',
			isset($config['action'])?'special_price_to_date':'null',
			isset($config['url'])?'url_key':'null',
			isset($config['title_meta'])?'meta_title':'null',
			isset($config['keywords'])?'meta_keywords':'null',
			isset($config['description_meta'])?'meta_description':'null',
			isset($config['image'])?'base_image':'null',
			isset($config['image'])?'base_image_label':'null',
			isset($config['image'])?'small_image':'null',
			isset($config['image'])?'small_image_label':'null',
			isset($config['is_full'])?'thumbnail_image':'null',
			isset($config['is_full'])?'thumbnail_image_label':'null',
			isset($config['is_full'])?'created_at':'null',
			isset($config['is_full'])?'updated_at':'null',
			isset($config['is_full'])?'new_from_date':'null',
			isset($config['is_full'])?'new_to_date':'null',
			isset($config['is_full'])?'display_product_options_in':'null',
			isset($config['is_full'])?'map_price':'null',
			isset($config['is_full'])?'msrp_price':'null',
			isset($config['is_full'])?'map_enabled':'null',
			isset($config['is_full'])?'gift_message_available':'null',
			isset($config['is_full'])?'custom_design':'null',
			isset($config['is_full'])?'custom_design_from':'null',
			isset($config['is_full'])?'custom_design_to':'null',
			isset($config['is_full'])?'custom_layout_update':'null',
			isset($config['is_full'])?'page_layout':'null',
			isset($config['is_full'])?'product_options_container':'null',
			isset($config['is_full'])?'msrp_display_actual_price_type':'null',
			isset($config['is_full'])?'country_of_manufacture':'null',
			isset($config['is_full'])?'additional_attributes':'null',
			isset($config['quantity'])?'qty':'null',
			isset($config['quantity'])?'out_of_stock_qty':'null',
			isset($config['quantity'])?'use_config_min_qty':'null',
			isset($config['quantity'])?'is_qty_decimal':'null',
			isset($config['quantity'])?'allow_backorders':'null',
			isset($config['quantity'])?'use_config_backorders':'null',
			isset($config['quantity'])?'min_cart_qty':'null',
			isset($config['quantity'])?'use_config_min_sale_qty':'null',
			isset($config['quantity'])?'max_cart_qty':'null',
			isset($config['quantity'])?'use_config_max_sale_qty':'null',
			isset($config['stock'])?'is_in_stock':'null',
			isset($config['stock'])?'notify_on_stock_below':'null',
			isset($config['stock'])?'use_config_notify_stock_qty':'null',
			isset($config['stock'])?'manage_stock':'null',
			isset($config['stock'])?'use_config_manage_stock':'null',
			isset($config['quantity'])?'use_config_qty_increments':'null',
			isset($config['quantity'])?'qty_increments':'null',
			isset($config['quantity'])?'use_config_enable_qty_inc':'null',
			isset($config['quantity'])?'enable_qty_increments':'null',
			isset($config['is_full'])?'is_decimal_divided':'null',
			isset($config['is_full'])?'website_id':'null',
			isset($config['related'])?'related_skus':'null',
			isset($config['related'])?'crosssell_skus':'null',
			isset($config['related'])?'upsell_skus':'null',
			isset($config['is_full'])?'additional_images':'null',
			isset($config['is_full'])?'additional_image_labels':'null',
			isset($config['is_full'])?'custom_options':'null'
			);

		$products_CSV[0] = array_diff($header,array('null'));

		foreach($products as $article){
			$meta_title = AdminCommon::seo_title($article->roba_id);
			$base_image = self::base_image($article->roba_id);
			$add_images = self::add_images($article->roba_id);

			$row = array(
			$article->roba_id,
			isset($config['is_full'])?'':'null',
			isset($config['is_full'])?'Default':'null',
			isset($config['is_full'])?'simple':'null',
			isset($config['category'])?self::puna_putanja_grupe($article->grupa_pr_id):'null',
			substr(AdminOptions::base_url(),0,-1),
			isset($config['name'])?$article->naziv_web:'null',
			isset($config['description'])?$article->web_opis." ".Support::characteristics($article->roba_id,$article->web_flag_karakteristike,$article->web_karakteristike):'null',
			isset($config['short_description'])?(isset($web_opis)?$article->web_opis:$article->naziv_web):'null',
			isset($config['weight'])?'5 kg':'null',
			isset($config['visibility'])?($article->flag_prikazi_u_cenovniku==1?'1':'2'):'null',
			isset($config['visibility'])?'Catalog, Search':'null',
			isset($config['tax'])?AdminSupport::find_tarifna_grupa($article->tarifna_grupa_id,'porez'):'null',
			isset($config['price'])?floatval($article->web_cena):'null',
			isset($config['action'])?($article->akcijska_cena>0 && $article->akcija_flag_primeni==1?$article->akcijska_cena:$article->web_cena):'null',
			isset($config['action'])?$article->datum_akcije_od:'null',
			isset($config['action'])?$article->datum_akcije_do:'null',
			isset($config['url'])?AdminOptions::url_convert($meta_title):'null',
			isset($config['title_meta'])?$meta_title:'null',
			isset($config['keywords'])?(isset($article->keywords) ? $article->keywords : str_replace(' ',', ',strtolower ($article->naziv_web))):'null',
			isset($config['description_meta'])?strtolower(AdminSupport::seo_description($article->roba_id)):'null',
			isset($config['image'])?$base_image:'null',
			isset($config['image'])?($base_image!=''?$meta_title:''):'null',
			isset($config['image'])?$base_image:'null',
			isset($config['image'])?($base_image!=''?$meta_title:''):'null',
			isset($config['is_full'])?'':'null',
			isset($config['is_full'])?'':'null',
			isset($config['is_full'])?'':'null',
			isset($config['is_full'])?'':'null',
			isset($config['is_full'])?'':'null',
			isset($config['is_full'])?'':'null',
			isset($config['is_full'])?'Product Info Column':'null',
			isset($config['is_full'])?'':'null',
			isset($config['is_full'])?'':'null',
			isset($config['is_full'])?'0':'null',
			isset($config['is_full'])?'0':'null',
			isset($config['is_full'])?'':'null',
			isset($config['is_full'])?'':'null',
			isset($config['is_full'])?'':'null',
			isset($config['is_full'])?'':'null',
			isset($config['is_full'])?'No layout updates':'null',
			isset($config['is_full'])?'Product Info Column':'null',
			isset($config['is_full'])?'In Cart':'null',
			isset($config['is_full'])?'':'null',
			isset($config['is_full'])?'':'null',
			isset($config['quantity'])?(is_null($article->kolicina)?'0':$article->kolicina):'null',
			isset($config['quantity'])?'0':'null',
			isset($config['quantity'])?'0':'null',
			isset($config['quantity'])?'0':'null',
			isset($config['quantity'])?'0':'null',
			isset($config['quantity'])?'0':'null',
			isset($config['quantity'])?'1':'null',
			isset($config['quantity'])?'0':'null',
			isset($config['quantity'])?'999999':'null',
			isset($config['quantity'])?'0':'null',
			isset($config['stock'])?($article->kolicina>0?'1':'0'):'null',
			isset($config['stock'])?'':'null',
			isset($config['stock'])?'0':'null',
			isset($config['stock'])?'0':'null',
			isset($config['stock'])?'0':'null',
			isset($config['quantity'])?'0':'null',
			isset($config['quantity'])?'0':'null',
			isset($config['quantity'])?'0':'null',
			isset($config['quantity'])?'0':'null',
			isset($config['is_full'])?'0':'null',
			isset($config['is_full'])?'':'null',
			isset($config['related'])?self::related_ids($article->roba_id,$export_id):'null',
			isset($config['related'])?'':'null',
			isset($config['related'])?'':'null',
			isset($config['is_full'])?$add_images:'null',
			isset($config['is_full'])?($add_images!=''?$meta_title:''):'null',
			isset($config['is_full'])?'':'null'
			);

			$products_CSV[] = array_diff($row,array('null'));
		}
		$fp = fopen('php://output', 'w');
		foreach ($products_CSV as $line) {
		    fputcsv($fp, $line, ';');
		}
		fclose($fp);
	}

	public static function characteristics($roba_id,$web_flag_karakteristike,$karakteristike){
		if($web_flag_karakteristike == 0){
			return $karakteristike;
		}
		elseif($web_flag_karakteristike == 1){
			$generisane = DB::select("SELECT REPLACE(REPLACE((SELECT naziv FROM grupa_pr_naziv WHERE grupa_pr_naziv_id = wrk.grupa_pr_naziv_id),';',' '),',',' ') as naziv, REPLACE(REPLACE(vrednost,';',' '),',',' ') as vrednost FROM web_roba_karakteristike wrk WHERE roba_id = ".$roba_id." ORDER BY rbr ASC");
			$generisane_karakteristike = '';
			foreach($generisane as $row){
				$generisane_karakteristike .= $row->naziv.': '.$row->vrednost.', ';
			}
			return substr($generisane_karakteristike,0,-2);
		}
		elseif($web_flag_karakteristike == 2){
			$dobavljac = DB::select("SELECT REPLACE(REPLACE(karakteristika_naziv,';',' '),',',' ') as karakteristika_naziv, REPLACE(REPLACE(karakteristika_vrednost,';',' '),',',' ') as karakteristika_vrednost FROM dobavljac_cenovnik_karakteristike WHERE roba_id = ".$roba_id." ORDER BY dobavljac_cenovnik_karakteristike_id ASC");
			$dobavljac_karakteristike = '';
			foreach($dobavljac as $row){
				$dobavljac_karakteristike .= $row->karakteristika_naziv.': '.$row->karakteristika_vrednost.', ';
			}
			return substr($dobavljac_karakteristike,0,-2);
		}		
	}

	public static function base_image($roba_id){
        $obj_slika = DB::table('web_slika')->select('putanja')->where('roba_id',$roba_id)->orderBy('akcija','desc')->first();
	    if(isset($obj_slika)){		    	
		    return '/'.$obj_slika->putanja;
	    }else{
	    	return '';
	    }
	}
	// public static function add_image($roba_id){
 //    	$image_addon = DB::table('web_files')->where(array('roba_id'=>$roba_id,'vrsta_fajla_id'=>6))->orderBy('web_file_id','asc')->first();
 //    	if(isset($image_addon)){
 //    		return '/'.$image_addon->putanja;
 //    	}else{
 //    		return '';
 //    	}
 //    }
	public static function add_images($roba_id){
        $slike = DB::table('web_slika')->select('putanja')->where(array('roba_id'=>$roba_id,'akcija'=>0))->get();
	    $add_imgs = '';
	    foreach($slike as $sl) {
	    	$add_imgs .= '/'.$sl->putanja.', ';
	    }
	    return substr($add_imgs,0,-2);
	}

	public static function puna_putanja_grupe($grupa_pr_id,$link=''){
        $first = DB::select("SELECT REPLACE(REPLACE(grupa,';',' '),',',' ') AS grupa, parrent_grupa_pr_id FROM grupa_pr WHERE grupa_pr_id = ".$grupa_pr_id."")[0];
        if($link!=''){
        	$link = $first->grupa.'/'.$link;
        }else{
        	$link = $first->grupa;
        }
        if($first->parrent_grupa_pr_id != 0){
        	$link = self::puna_putanja_grupe($first->parrent_grupa_pr_id,$link);
        }
        return $link;
	}

    public static function related_ids($roba_id,$export_id){

        $grupa_pr_id=DB::table('roba')->where('roba_id',$roba_id)->pluck('grupa_pr_id');
        $web_cena=DB::table('roba')->where('roba_id',$roba_id)->pluck('web_cena');
        $cena_od = $web_cena * 9/10;
        $cena_do = $web_cena * 6/5;
        
        $related = DB::select("SELECT roba_id FROM roba r WHERE roba_id IN (SELECT roba_id FROM roba_export WHERE export_id=".$export_id.") AND grupa_pr_id = ".$grupa_pr_id." AND flag_aktivan = 1 AND flag_prikazi_u_cenovniku = 1 AND roba_id <> -1 AND roba_id <> ".$roba_id." AND web_cena > ".$cena_od." AND web_cena < ".$cena_do." LIMIT 4");
        $result = '';
        foreach($related as $rel){
        	$result .= strval($rel->roba_id).', ';
        }
        if($result!=''){
        	$result = substr($result,0,-2);
        }
        return $result;
    }
}