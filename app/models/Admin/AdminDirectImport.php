<?php
use DirectImport\ReadFile;
use DirectImport\Article;
use DirectImport\Lager;
use DirectImport\Images;

class AdminDirectImport {

    public static function xml_execute(){
        $result = ReadFile::xml();
        if($result->success){        
            self::execute($result->articles);
        }
        return $result->success;
    }

    public static function xls_execute(){
        $result = ReadFile::xls();
        if($result->success){        
            self::execute($result->articles);
        }
        return $result->success;
    }

    public static function xlsx_execute(){
        $result = ReadFile::xls(true);
        if($result->success){        
            self::execute($result->articles);
        }
        return $result->success;
    }

    public static function csv_execute(){
        $result = ReadFile::csv();
        if($result->success){        
            self::execute($result->articles);
        }
        return $result->success;
    }

    public static function execute($articles){
        //articles
        $resultArticle = Article::table_body($articles);
        if($resultArticle->body!=''){
            Article::query_insert_update($resultArticle->body,array('id_is','naziv','jedinica_mere_id','proizvodjac_id','naziv_displej','racunska_cena_nc','racunska_cena_end','web_cena','mpcena','naziv_web','web_opis','web_karakteristike','akcija_flag_primeni','barkod','model'));
            // Article::query_update_unexists($resultArticle->body);
        }

        //lager
        $resultLager = Lager::table_body($articles);
        if($resultLager->body!=''){
            Lager::query_insert_update($resultLager->body);
        }
        //images
        $resultImage = Images::table_body($articles);
        if($resultImage->body!=''){
            Images::query_insert_update($resultImage->body);
        }
    }


}