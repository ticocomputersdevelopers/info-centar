<?php
namespace Import;
use Import\Support;
use DB;
use File;
use PHPExcel; 
use PHPExcel_IOFactory;

class Gembird {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){
		if($extension==null){
			$products_file = "files/gembird/gembird_xml/gembird.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}
		
		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

			$products = simplexml_load_file($products_file);	        
			foreach ($products as $product):
				if(!empty($product->id)){
					$opis = $product->xpath('specifications/attribute_group/attribute/value')[0];
					$cena = floatval(str_replace(',','',$product->price))/1.2;
					$opis = pg_escape_string($opis);

					$images = $product->xpath('images/image');
					for($i=0;$i<count($images);$i++){
						DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",".Support::quotedStr($product->id).",'".$images[$i]."',".($i==0?"1":"0").")");
					}



					$sPolja = '';
					$sVrednosti = '';
					$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
					$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "'" . addslashes(Support::encodeTo1250($product->id)) . "',";
					$sPolja .= "naziv,";					$sVrednosti .= "'" . addslashes(Support::encodeTo1250($product->name)) . "',";
					$sPolja .= "grupa,";					$sVrednosti .= "'" . addslashes(Support::encodeTo1250($product->category)) . "',";
					$sPolja .= "proizvodjac,";				$sVrednosti .= "'" . addslashes(Support::encodeTo1250($product->manufacturer)) . "',";
					$sPolja .= "opis,";						$sVrednosti .= "'" . Support::encodeTo1250($opis) . "',";
					if(!empty($opis)) {
					$sPolja .= "flag_opis_postoji,";		$sVrednosti .= "1,";
					}
					if(!empty($imges)) {
					$sPolja .= "flag_slika_postoji,";		$sVrednosti .= "1,";
					}
					$sPolja .= "pdv,";						$sVrednosti .= "" . $product->vat . ",";
					$sPolja .= "kolicina,";					$sVrednosti .= "" . ($product->stock=='Ima'?"1":"0") . ",";
					$sPolja .= "cena_nc";					$sVrednosti .= "" . number_format(floatval(Support::replace_empty_numeric($cena,1,$kurs,$valuta_id_nc)), 2, '.', '') . "";
						
					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");



				}

			endforeach;

			Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){

		if($extension==null){
			$products_file = "files/gembird/gembird_xml/gembird.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

			$products = simplexml_load_file($products_file);	        
			foreach ($products as $product):
				if(!empty($product->id)){
					$cena = floatval(str_replace(',','',$product->price))/1.2;

					$sPolja = '';
					$sVrednosti = '';
					$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
					$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "'" . addslashes(Support::encodeTo1250($product->id)) . "',";
					$sPolja .= "kolicina,";					$sVrednosti .= "" . ($product->stock=='Ima'?"1":"0") . ",";
					$sPolja .= "cena_nc";					$sVrednosti .= "" . number_format(floatval(Support::replace_empty_numeric($cena,1,$kurs,$valuta_id_nc)), 2, '.', '') . "";
						
					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
				}

			endforeach;

			//Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}

}