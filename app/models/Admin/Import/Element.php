<?php
namespace Import;
use Import\Support;
use DB;
use File; 

class Element {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){
		
	if($extension==null){
			Support::autoDownload(Support::autoLink($dobavljac_id),'files/element/element_xml/element.xml');
			$file_name = Support::file_name("files/element/element_xml/");
			if($file_name!==false){
				$products = simplexml_load_file("files/element/element_xml/".$file_name);
			}else{
				$products = array();
			}
		}else{
			$file = AdminOptions::base_url().'files/import.'.$extension;
			$products = simplexml_load_file($file);	
			
		}
	
		//Ubacujem artikle u dobavljac_cenovnik_temp
		$products_2 = $products->xpath('//product');

		foreach($products_2 as $product):

			$sifra= $product->textId;
			$naziv= $product->naziv;			
			$cena = $product->cena;
			$kolicina= $product->lagerVp;
			$slika= $product->slika;

			//var_dump($naziv);die;
			
			$sPolja = '';
			$sVrednosti = '';

			$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
			$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "" . Support::quotedStr($sifra) . ",";
			$sPolja .= "kolicina,";					$sVrednosti .= "" . Support::quotedStr($kolicina) . ",";
			$sPolja .= "naziv,";						$sVrednosti .= "'" . $naziv . "   ( " . $sifra . " )',";
			if(isset($slika)){
			$sPolja .= "flag_slika_postoji,";		$sVrednosti .= " 1,";	
			}else{
			$sPolja .= "flag_slika_postoji,";		$sVrednosti .= " 0,";	
			}
			$sPolja .= "cena_nc";					$sVrednosti .= "" . $cena . "";

			DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");			
			DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp (partner_id,sifra_kod_dobavljaca,putanja,akcija) VALUES(".$dobavljac_id.",'".$sifra."','".$slika."',1 )");	
			endforeach;
			
			Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array('i','u'));
	
				$file_name = Support::file_name("files/element/element_xml/");
		if($extension!=null){
			File::delete('files/import.'.$extension);
		}
		elseif($file_name!==false){
			File::delete("files/element/element_xml/".$file_name);
		}
		}



	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){
				
			if($extension==null){
			Support::autoDownload(Support::autoLink($dobavljac_id),'files/element/element_xml/element.xml');
			$file_name = Support::file_name("files/element/element_xml/");
			if($file_name!==false){
				$products = simplexml_load_file("files/element/element_xml/".$file_name);
			}else{
				$products = array();
			}
		}else{
			$file = AdminOptions::base_url().'files/import.'.$extension;
			$products = simplexml_load_file($file);	
			
		}
	
		//Ubacujem artikle u dobavljac_cenovnik_temp
		$products_2 = $products->xpath('//product');

		foreach($products_2 as $product):

			$sifra= $product->textId;
			$naziv= $product->naziv;			
			$cena = $product->cena;
			$kolicina= $product->lagerVp;
			$slika= $product->slika;

			$sPolja = '';
			$sVrednosti = '';

			$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
			$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "" . Support::quotedStr($sifra) . ",";
			$sPolja .= "kolicina,";					$sVrednosti .= "" . Support::quotedStr($kolicina) . ",";
			if(isset($slika)){
			$sPolja .= "flag_slika_postoji,";		$sVrednosti .= " 1,";	
			}else{
			$sPolja .= "flag_slika_postoji,";		$sVrednosti .= " 0,";	
			}
			$sPolja .= "cena_nc";					$sVrednosti .= "" . $cena . "";

			DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");	
		
			endforeach;
			
			Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array('i','u'));
	
				$file_name = Support::file_name("files/element/element_xml/");
		if($extension!=null){
			File::delete('files/import.'.$extension);
		}
		elseif($file_name!==false){
			File::delete("files/element/element_xml/".$file_name);
		}
	}
}