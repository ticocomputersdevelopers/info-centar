<?php
namespace Import;
use Import\Support;
use DB;
use File;
use PHPExcel; 
use PHPExcel_IOFactory;

class EuromDenis {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){
		if($extension==null){
			Support::autoDownload(Support::autoLink($dobavljac_id),'files/euromdenis/euromdenis_xml/euromdenis.xml');
			$products_file = "files/euromdenis/euromdenis_xml/euromdenis.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}
		
		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

			self::improve_file($products_file);
			$products = simplexml_load_file($products_file);

			foreach ($products as $product):
				if(isset($product->EAN)){

					$opis = '';
					if(isset($product->Opis1) && $product->Opis1!='<![CDATA[]]>'){
						$opis.=Support::encodeTo1250(self::convertToUTF8($product->Opis1));
					}
					if($opis=='<br>'){
						$opis = '';
					}
					$opis = preg_replace('/<br>$/', '', $opis);
					if(isset($product->Opis2) && $product->Opis2!='<![CDATA[]]>'){
						$opis.='<br>'.Support::encodeTo1250(self::convertToUTF8($product->Opis2));
					}
					if($opis=='<br>'){
						$opis = '';
					}
					$opis = preg_replace('/<br>$/', '', $opis);
					if(isset($product->Opis3) && $product->Opis3!='<![CDATA[]]>'){
						$opis.='<br>'.Support::encodeTo1250(self::convertToUTF8($product->Opis3));
					}
					if($opis=='<br>'){
						$opis = '';
					}
					$opis = preg_replace('/<br>$/', '', $opis);
					if(isset($product->Opis4) && $product->Opis4!='<![CDATA[]]>'){
						$opis.='<br>'.Support::encodeTo1250(self::convertToUTF8($product->Opis4));
					}
					if($opis=='<br>'){
						$opis = '';
					}
					$opis = preg_replace('/<br>$/', '', $opis);
					if(isset($product->Opis5) && $product->Opis5!='<![CDATA[]]>'){
						$opis.='<br>'.Support::encodeTo1250(self::convertToUTF8($product->Opis5));
					}
					if($opis=='<br>'){
						$opis = '';
					}
					$opis = preg_replace('/<br>$/', '', $opis);
					if(isset($product->Opis6) && $product->Opis6!='<![CDATA[]]>'){
						$opis.='<br>'.Support::encodeTo1250(self::convertToUTF8($product->Opis6));
					}
					if($opis=='<br>'){
						$opis = '';
					}
					$opis = preg_replace('/<br>$/', '', $opis);
					// $opis = addslashes($opis);
					$opis = addslashes(str_replace("'", "\"", $opis));

					$naziv = Support::encodeTo1250(self::convertToUTF8($product->Naziv));
					$naziv = str_replace("'", "\"", $naziv);

					$flag_slika_postoji = "0";
					
					if(isset($product->Slika1)){
						
						DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",'".addslashes($product->EAN)."','".$product->Slika1."',1 )");
					}	
					

					$sPolja = '';
					$sVrednosti = '';
					
				
					$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
					$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "'" . addslashes($product->EAN) . "',";
					$sPolja .= "naziv,";					$sVrednosti .= "'" . $naziv ." ( ".$product->Sifra." )" . "',";
					$sPolja .= "pdv,";						$sVrednosti .= "" . floatval($product->Porez) . ",";
					$sPolja .= "kolicina,";					$sVrednosti .= "" . number_format(intval($product->Kolicina), 2, '.', '') . ",";
					$sPolja .= "cena_nc,";					$sVrednosti .= "" . number_format(floatval(Support::replace_empty_numeric(str_replace(',', '.', $product->Ugovorena_cena),1,$kurs,$valuta_id_nc)), 2, '.', '') . ",";
					$sPolja .= "pmp_cena,";					$sVrednosti .= "" . number_format(floatval(Support::replace_empty_numeric(str_replace(',', '.', $product->Preporucena_cena),1,$kurs,$valuta_id_nc)), 2, '.', '') . ",";					
					$sPolja .= "web_flag_karakteristike,";	$sVrednosti .= " 0,";
					$sPolja .= "opis,";						$sVrednosti .= "'" . $opis . "',";					
					if(isset($opis)){
					$sPolja .= "flag_opis_postoji,";		$sVrednosti .= " 1,";	
					}else{
					$sPolja .= "flag_opis_postoji,";		$sVrednosti .= " 0,";	
					}
					$sPolja .= "flag_slika_postoji";		$sVrednosti .= "" . $flag_slika_postoji . "";
							
					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

					
				}

			endforeach;

			Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){

		if($extension==null){
			$products_file = "files/euromdenis/euromdenis_xml/euromdenis.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

			self::improve_file($products_file);
			$products = simplexml_load_file($products_file);

			foreach ($products as $product):
				if(isset($product->EAN)){

					$sPolja = '';
					$sVrednosti = '';
					
				
					$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
					$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "'" . addslashes($product->EAN) . "',";
					$sPolja .= "pdv,";						$sVrednosti .= "" . floatval($product->Porez) . ",";
					$sPolja .= "kolicina,";					$sVrednosti .= "" . number_format(intval($product->Kolicina), 2, '.', '') . ",";
					$sPolja .= "cena_nc,";					$sVrednosti .= "" . number_format(floatval(Support::replace_empty_numeric(str_replace(',', '.', $product->Ugovorena_cena),1,$kurs,$valuta_id_nc)), 2, '.', '') . ",";
					$sPolja .= "pmp_cena";					$sVrednosti .= "" . number_format(floatval(Support::replace_empty_numeric(str_replace(',', '.', $product->Preporucena_cena),1,$kurs,$valuta_id_nc)), 2, '.', '') . "";
							
					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

					
				}

			endforeach;

			//Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}

	public static function improve_file($putanja){
		$str=file_get_contents($putanja);
		$str=str_replace(array("Ugovorena-cena","Preporucena-cena"), array("Ugovorena_cena","Preporucena_cena"), $str);
		file_put_contents($putanja, $str);
	}

	public static function convertToUTF8($text){

	    $encoding = mb_detect_encoding($text, mb_detect_order(), false);
	    if($encoding == "UTF-8")
	    {
	        $text = mb_convert_encoding($text, 'UTF-8', 'UTF-8');    
	    }
	    $out = iconv(mb_detect_encoding($text, mb_detect_order(), false), "UTF-8//IGNORE", $text);
	    return $out;
	}

}