<?php
namespace Import;
use Import\Support;
use DB;
use File;
use PHPExcel; 
use PHPExcel_IOFactory;

class Keprom {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){
		if($extension==null){
			$products_file = "files/keprom/keprom_excel/keprom.xlsx";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();
	        
	        $niz_sifara=array();
	        for ($row = 1; $row <= $lastRow; $row++) {
	            $sifra = $worksheet->getCell('A'.$row)->getValue();
	            $naziv = $worksheet->getCell('B'.$row)->getValue();
	            $kolicina = $worksheet->getCell('D'.$row)->getValue();
	            $pdv = $worksheet->getCell('G'.$row)->getValue();
				$ncena = $worksheet->getCell('E'.$row)->getValue();
				$pmpcena = $worksheet->getCell('F'.$row)->getValue();

				if(isset($sifra) && isset($naziv) && isset($ncena) && is_numeric($ncena) && !in_array($sifra, $niz_sifara)){
					$niz_sifara[]=$sifra;

					$sPolja = '';
					$sVrednosti = '';
					$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
					$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . addslashes($sifra) . "',";
					$sPolja .= " naziv,";					$sVrednosti .= " '" . addslashes(Support::encodeTo1250($naziv . " ( " . $sifra)) . " )" . "',";
					if(isset($pdv) && is_numeric($pdv)){	
					$sPolja .= " pdv,";						$sVrednosti .= " " . number_format(floatval($pdv),2,'.','') . ",";
					}
					if(isset($kolicina) && is_numeric($kolicina)){
					$sPolja .= " kolicina,";				$sVrednosti .= " " . number_format(intval($kolicina),2,'.','') . ",";
					}else{
					$sPolja .= " kolicina,";				$sVrednosti .= " 0,";	
					}
					
					if(isset($pmpcena) && is_numeric($pmpcena)){
					$sPolja .= "pmp_cena,";					$sVrednosti .= "" . number_format(Support::replace_empty_numeric(floatval($pmpcena),1,$kurs,$valuta_id_nc),2, '.', '') . ",";
					$sPolja .= "web_cena,";					$sVrednosti .= "" . number_format(Support::replace_empty_numeric(floatval($pmpcena),1,$kurs,$valuta_id_nc),2, '.', '') . ",";
					$sPolja .= "mpcena,";					$sVrednosti .= "" . number_format(Support::replace_empty_numeric(floatval($pmpcena),1,$kurs,$valuta_id_nc),2, '.', '') . ",";
					}
					
					$sPolja .= " cena_nc";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric((floatval($ncena)*0.9),1,$kurs,$valuta_id_nc),2, '.', '') . "";

					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
			
				}
		
			}

			Support::queryExecute($dobavljac_id,array('i','u'),array(),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){

		if($extension==null){
			$products_file = "files/keprom/keprom_excel/keprom.xlsx";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();

	        $niz_sifara=array();
	        for ($row = 1; $row <= $lastRow; $row++) {
	            $sifra = $worksheet->getCell('A'.$row)->getValue();
	            $kolicina = $worksheet->getCell('D'.$row)->getValue();
	            $pdv = $worksheet->getCell('G'.$row)->getValue();
				$ncena = $worksheet->getCell('E'.$row)->getValue();
				$pmpcena = $worksheet->getCell('F'.$row)->getValue();

				if(isset($sifra) && isset($ncena) && is_numeric($ncena) && !in_array($sifra, $niz_sifara)){
					$niz_sifara[]=$sifra;

					$sPolja = '';
					$sVrednosti = '';
					$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
					$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . addslashes($sifra) . "',";
					if(isset($pdv) && is_numeric($pdv)){	
					$sPolja .= " pdv,";						$sVrednosti .= " " . number_format(floatval($pdv),2,'.','') . ",";
					}
					if(isset($kolicina) && is_numeric($kolicina)){
					$sPolja .= " kolicina,";				$sVrednosti .= " " . number_format(intval($kolicina),2,'.','') . ",";
					}else{
					$sPolja .= " kolicina,";				$sVrednosti .= " 0,";	
					}
					
					if(isset($pmpcena) && is_numeric($pmpcena)){
					$sPolja .= "pmp_cena,";					$sVrednosti .= "" . number_format(Support::replace_empty_numeric(floatval($pmpcena),1,$kurs,$valuta_id_nc),2, '.', '') . ",";
					$sPolja .= "mpcena,";					$sVrednosti .= "" . number_format(Support::replace_empty_numeric(floatval($pmpcena),1,$kurs,$valuta_id_nc),2, '.', '') . ",";
					$sPolja .= "web_cena,";					$sVrednosti .= "" . number_format(Support::replace_empty_numeric(floatval($pmpcena),1,$kurs,$valuta_id_nc),2, '.', '') . ",";
					}
					$sPolja .= " cena_nc";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric((floatval($ncena)*0.9),1,$kurs,$valuta_id_nc),2, '.', '') . "";

					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
			
				}
	
			}

			//Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}


}