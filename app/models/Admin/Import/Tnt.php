<?php
namespace Import;
use Import\Support;
use DB;
use AdminOptions;
use File;

class Tnt {

	public static function execute($dobavljac_id,$extension=null){
		Support::initQueryExecute();

		if($extension==null){
			if(Support::autoLink($dobavljac_id)!=null){
				Support::autoDownload(Support::autoLink($dobavljac_id),'files/tnt/tnt_xml/tnt.xml');
			}
			$file_name = Support::file_name("files/tnt/tnt_xml/");
			if($file_name!==false){
				$products = simplexml_load_file("files/tnt/tnt_xml/".$file_name);
			}else{
				$products = array();
			}
		}else{
			$file = AdminOptions::base_url().'files/import.'.$extension;
			$products = simplexml_load_file($file);			
		}

		$prvi = 0;
		foreach ($products as $product) {

			$kolicina = $product->stanje;
			$nc = $product->veleprodajna_cena_din;
			$pc = $product->maloprodajna_cena_din;

			if($nc != '' && $pc != ''){

			$sPolja = '';
			$sVrednosti = '';
			$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
			$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250($product->id)) . ",";
			if($product->proizvodjac != 'No name'){
			$sPolja .= "proizvodjac,";				$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250($product->proizvodjac)) . ",";
			}
			$sPolja .= "naziv,";					$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250($product->naziv)) . ",";
			$sPolja .= "grupa,";					$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250($product->vrsta_naziv)) . ",";
			$sPolja .= "model,";					$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250($product->model)) . ",";
			if($kolicina != ''){
			$sPolja .= "kolicina,";					$sVrednosti .= "" . $product->stanje . ",";
			}else{
			$sPolja .= "kolicina,";					$sVrednosti .= " 0,";	
			}
			$sPolja .= "cena_nc,";					$sVrednosti .= "" . $product->veleprodajna_cena_din . ",";
			$sPolja .= "mpcena,";					$sVrednosti .= "" . $product->maloprodajna_cena_din . ",";
			$sPolja .= "pmp_cena,";					$sVrednosti .= "" . $product->maloprodajna_cena_din . ",";
			$sPolja .= "pdv,";						$sVrednosti .= "" . 20 . ",";
			$sPolja .= "flag_opis_postoji,";		$sVrednosti .= "" . $product->opis == "" ? "0," : "1,";
			$sPolja .= "opis,";						$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250($product->opis)) . ",";
			$sPolja .= "web_flag_karakteristike,";	$sVrednosti .= " 0,";
			$sPolja .= "flag_slika_postoji";		$sVrednosti .= "" . $product->slika == "" ? "0" : "1";
			
			if ($prvi>1) {

				DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
			}
			}
			if($product->slika != ""){

				$sPolja = '';
				$sVrednosti = '';

				$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
				$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250($product->id)) . ",";
				$sPolja .= "akcija,";					$sVrednosti .= "" . 1 . ",";
				$sPolja .= "putanja";				    $sVrednosti .= "'" .str_replace(array("![CDATA[","]]"),array("","") ,  Support::encodeTo1250($product->slika)). "'";


					
				DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
				//var_dump($sVrednosti);die;
			}

		    $prvi++;
		}
		
		Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array('i','u'));
		
		//Brisemo fajl
		$file_name = Support::file_name("files/tnt/tnt_xml/");
		if($extension!=null){
			File::delete('files/import.'.$extension);
		}
		elseif($file_name!==false){
			File::delete("files/tnt/tnt_xml/".$file_name);
		}
	
	}

	public static function executeShort($dobavljac_id,$extension=null){

		Support::initQueryExecute();

		if($extension==null){
			if(Support::autoLink($dobavljac_id)!=null){
				Support::autoDownload(Support::autoLink($dobavljac_id),'files/tnt/tnt_xml/tnt.xml');
			}
			$file_name = Support::file_name("files/tnt/tnt_xml/");
			if($file_name!==false){
				$products = simplexml_load_file("files/tnt/tnt_xml/".$file_name);
			}else{
				$products = array();
			}
		}else{
			$file = AdminOptions::base_url().'files/import.'.$extension;
			$products = simplexml_load_file($file);			
		}

		$prvi = 0;
		foreach ($products as $product) {

			$kolicina = $product->stanje;
			$nc = $product->veleprodajna_cena_din;
			$pc = $product->maloprodajna_cena_din;

			if($nc != '' && $pc != ''){

			$sPolja = '';
			$sVrednosti = '';
			$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
			$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250($product->id)) . ",";
			if($kolicina != ''){
			$sPolja .= "kolicina,";					$sVrednosti .= "" . $product->stanje . ",";
			}else{
			$sPolja .= "kolicina,";					$sVrednosti .= " 0,";	
			}
			$sPolja .= "cena_nc,";					$sVrednosti .= "" . $product->veleprodajna_cena_din . ",";
			$sPolja .= "mpcena,";					$sVrednosti .= "" . $product->maloprodajna_cena_din . ",";
			$sPolja .= "pmp_cena";					$sVrednosti .= "" . $product->maloprodajna_cena_din . "";
			
			if ($prvi>1) {
				DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
			}

		    $prvi++;
		}
		}
		//Support::queryShortExecute($dobavljac_id);
		
		//Brisemo fajl
		$file_name = Support::file_name("files/tnt/tnt_xml/");
		if($extension!=null){
			File::delete('files/import.'.$extension);
		}
		elseif($file_name!==false){
			File::delete("files/tnt/tnt_xml/".$file_name);
		}
	
	}

}