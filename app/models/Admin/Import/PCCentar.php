<?php
namespace Import;
use Import\Support;
use DB;
use AdminOptions;
use File;

class PCCentar {

	public static function execute($dobavljac_id,$extension=null){
		Support::initQueryExecute();

		if($extension==null){
			if(Support::autoLink($dobavljac_id) != null){
				Support::autoDownload(Support::autoLink($dobavljac_id),'files/pccentar/pccentar_xml/pccentar.xml');
			}
			$file_name = Support::file_name("files/pccentar/pccentar_xml/");
			if($file_name!==false){
				$products = simplexml_load_file("files/pccentar/pccentar_xml/".$file_name);
			}else{
				$products = array();
			}
		}else{
			$file = AdminOptions::base_url().'files/import.'.$extension;
			$products = simplexml_load_file($file);			
		}



		foreach ($products as $product) {
			
			//karakteristike
			$specifikacija=$product->xpath('Specifikacija/Spec');			
			
			foreach ($specifikacija as $el) {
				
				$parent_group=$el->attributes()->title;
				
					$sPolja = '';
					$sVrednosti = '';
					$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
					$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250( $product->Sifra)) . ",";	
					$sPolja .= "karakteristika_naziv,";		$sVrednosti .= "" . Support::quotedStr(pg_escape_string(trim(Support::encodeTo1250(($parent_group))))) . ",";
					$sPolja .= "karakteristika_vrednost";	$sVrednosti .= "'" . trim(Support::encodeTo1250(pg_escape_string($el))) . "'";
						
					DB::statement("INSERT INTO dobavljac_cenovnik_karakteristike_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
				
			}
			
			//slike
			$images = $product->xpath('Slike/Slika');
			$flag_slika_postoji = "0";
			$i=0;
			foreach ($images as $slika){
				if($i==0){
					DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",".Support::quotedStr($product->Sifra).",'".Support::encodeTo1250($slika)."',1 )");
				}else{
					DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",".Support::quotedStr($product->Sifra).",'".Support::encodeTo1250($slika)."',0 )");
				}
				$flag_slika_postoji = "1";
				$i++;
			}	

	
			$pmp_cena= $product->{'PreporucenaCena'};
			$ncena = $product->{'NabavnaCena'};
			
			//proizvodi
			$sPolja = '';
			$sVrednosti = '';
			$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
			$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250( $product->Sifra)) . ",";
			$sPolja .= "naziv,";					$sVrednosti .= "" . Support::quotedStrPC(Support::encodeTo1250($product->Naziv)) . ",";
			$sPolja .= "grupa,";					$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250($product->Kategorija1)) . ",";			
		
			$sPolja .= "web_flag_karakteristike,";	$sVrednosti .= " 2,";

			$sPolja .= "kolicina,";					$sVrednosti .= "" . $product->Kolicina . ",";
			
			$sPolja .= "cena_nc,";					$sVrednosti .= "" . str_replace(',', '.', $ncena) . ",";
			
			$sPolja .= "pmp_cena,";					$sVrednosti .= "" . number_format(Support::replace_empty_numeric($pmp_cena,1,1,2),2, '.', '') . ",";	

			$sPolja .= "pdv,";						$sVrednosti .= "" . str_replace(',', '.', $product->Porez) . ",";

			$sPolja .= "flag_slika_postoji";		$sVrednosti .= "" . $flag_slika_postoji."";
							
			DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

		}


		Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array('i','u'));
		
		//Brisemo fajl
		$file_name = Support::file_name("files/pccentar/pccentar_xml/");
		if($extension!=null){
			File::delete('files/import.'.$extension);
		}
		elseif($file_name!==false){
			File::delete("files/pccentar/pccentar_xml/".$file_name);
		}
	
	}

	public static function executeShort($dobavljac_id,$extension=null){

		Support::initQueryExecute();

		if($extension==null){
			if(Support::autoLink($dobavljac_id) != null){
				Support::autoDownload(Support::autoLink($dobavljac_id),'files/pccentar/pccentar_xml/ewe.xml');
			}
			$file_name = Support::file_name("files/pccentar/pccentar_xml/");
			if($file_name!==false){
				$products = simplexml_load_file("files/pccentar/pccentar_xml/".$file_name);
			}else{
				$products = array();
			}
		}else{
			$file = AdminOptions::base_url().'files/import.'.$extension;
			$products = simplexml_load_file($file);			
		}

		foreach ($products as $product) {
			$pmp_cena= $product->{'PreporucenaCena'};
			$ncena = $product->{'NabavnaCena'};

			$sPolja = '';
			$sVrednosti = '';
			$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
			$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250( $product->Sifra)) . ",";
			$sPolja .= "kolicina,";					$sVrednosti .= "" . $product->Kolicina . ",";	
			$sPolja .= "cena_nc,";					$sVrednosti .= "" . str_replace(',', '.', $ncena) . ",";
			$sPolja .= "pmp_cena";					$sVrednosti .= "" . number_format(Support::replace_empty_numeric($pmp_cena,1,1,2),2, '.', '') . "";
							
			DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

		}
		
		//Support::queryShortExecute($dobavljac_id);
		
		//Brisemo fajl
		$file_name = Support::file_name("files/pccentar/pccentar_xml/");
		if($extension!=null){
			File::delete('files/import.'.$extension);
		}
		elseif($file_name!==false){
			File::delete("files/pccentar/pccentar_xml/".$file_name);
		}
	
	}

}