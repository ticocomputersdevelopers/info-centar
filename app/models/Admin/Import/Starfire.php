<?php
namespace Import;
use Import\Support;
use DB;
use File;
use PHPExcel; 
use PHPExcel_IOFactory;

class Starfire {

public static function execute($dobavljac_id,$kurs=null,$extension=null){

		if($extension==null){
			$products_file = "files/Starfire/Starfire_excel/Starfire.xlsx";
						$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}
		
		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}


       		$excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();
	        //$loadedSheetName = $excelObj->getSheetNames();
		
	        for ($row = 1; $row <= $lastRow; $row++) {
	        	
  				$grupa	  = "Starfire Putnički letnji";
	            $sifra    = $worksheet->getCell('B'.$row)->getValue();	            
				$cena_nc  = $worksheet->getCell('J'.$row)->getValue();
				$pmp_cena = $worksheet->getCell('K'.$row)->getFormattedValue();
				$dezen    = $worksheet->getCell('D'.$row)->getValue();
				$dimenzi  = $worksheet->getCell('C'.$row)->getValue();
				$nosivos  = $worksheet->getCell('E'.$row)->getValue();				
				$gorivo   = $worksheet->getCell('G'.$row)->getValue();
				$prijanj  = $worksheet->getCell('H'.$row)->getValue();
				$buka     = $worksheet->getCell('I'.$row)->getValue();
				$oznaka   = $worksheet->getCell('F'.$row)->getValue();
				
				$buka = str_replace(")", "", $buka);
				
				if(isset($sifra) && isset($cena_nc) && is_numeric($cena_nc)){
				
				$cena_nc=$cena_nc/1.25;	

				$proizvodjac = "Starfire";				
				$d = "Dimenzije: ". $dimenzi ." ";
				$dz= "Dezen: ". $dezen ." ";
				$n = "Index Brzine i Nosivosti: ". $nosivos ." ";				
				$g = "Stepen iskorišćenja goriva: ". $gorivo ." ";
				$pr = "Prianjanje na mokroj podlozi: ". $prijanj ." ";
				if(isset($oznaka)){
				$oz = "Posebna oznaka: ". $oznaka ." ";
				}else{
				$oz = "Posebna oznaka: N/A ";	
				}
				$b = "Spoljašnja buka pri kotrljanju: ". $buka ." db";
				
				$opis = "<ul> 						
						<li> $d </li> 
						<li> $dz </li>
						<li> $oz </li>
						<li> $g </li>  
						<li> $pr </li>
						<li> $n </li>
						<li> $b </li> 
						</ul>";

				$sPolja = '';
				$sVrednosti = '';
				$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
				$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= "'" . addslashes($sifra) . "',";
				$sPolja .= " naziv,";					$sVrednosti .= "'" . addslashes(Support::encodeTo1250($proizvodjac)) . " " . addslashes(Support::encodeTo1250($dimenzi)) . "',";
				$sPolja .= " grupa,";					$sVrednosti .= "'" . addslashes(Support::encodeTo1250($grupa)) . "',";
				$sPolja .= " opis,";					$sVrednosti .= "'" . Support::encodeTo1250($opis) . "',";
				$sPolja .= " proizvodjac,";				$sVrednosti .= "'" . $proizvodjac . "',";
				$sPolja .= " kolicina,";				$sVrednosti .= " " . number_format(1.00,2,'.','') . ",";
				$sPolja .= " cena_nc,";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric(floatval($cena_nc),1,$kurs,$valuta_id_nc),2, '.', '') . ",";
				$sPolja .= " pmp_cena";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric(floatval($pmp_cena),1,$kurs,$valuta_id_nc),2, '.', '') . "";		

				
				DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
			}		
		
}	
       		$excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(1);
	        $lastRow = $worksheet->getHighestRow();
	        //$loadedSheetName = $excelObj->getSheetNames();
		
	        for ($row = 1; $row <= $lastRow; $row++) {
	        	
  				$grupa	  = "Starfire sva godišnja doba";
	            $sifra    = $worksheet->getCell('B'.$row)->getValue();	            
				$cena_nc  = $worksheet->getCell('G'.$row)->getValue();
				$pmp_cena = $worksheet->getCell('H'.$row)->getFormattedValue();
				$dezen    = $worksheet->getCell('D'.$row)->getValue();
				$dimenzi  = $worksheet->getCell('C'.$row)->getValue();
				$nosivos  = $worksheet->getCell('E'.$row)->getValue();				
				$gorivo   = $worksheet->getCell('I'.$row)->getValue();
				$prijanj  = $worksheet->getCell('J'.$row)->getValue();
				$buka     = $worksheet->getCell('K'.$row)->getValue();
				
				$buka = str_replace(")", "", $buka);
			
				
				if(isset($sifra) && isset($cena_nc) && is_numeric($cena_nc)){
				
				$cena_nc=$cena_nc/1.25;	
				$proizvodjac = "Starfire";				
				$d = "Dimenzije: ". $dimenzi ." ";
				$dz= "Dezen: ". $dezen ." ";
				$n = "Index Brzine i Nosivosti: ". $nosivos ." ";				
				$g = "Stepen iskorišćenja goriva: ". $gorivo ." ";
				$pr = "Prianjanje na mokroj podlozi: ". $prijanj ." ";
				$b = "Spoljašnja buka pri kotrljanju: ". $buka ." db";
				
				$opis = "<ul>
						<li> $d </li> 
						<li> $dz </li>						
						<li> $g </li>  
						<li> $pr </li>
						<li> $n </li>
						<li> $b </li> 
						</ul>";

				$sPolja = '';
				$sVrednosti = '';
				$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
				$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= "'" . addslashes($sifra) . "',";
				$sPolja .= " naziv,";					$sVrednosti .= "'" . addslashes(Support::encodeTo1250($proizvodjac)) . " " . addslashes(Support::encodeTo1250($dimenzi)) . "',";
				$sPolja .= " grupa,";					$sVrednosti .= "'" . addslashes(Support::encodeTo1250($grupa)) . "',";
				$sPolja .= " opis,";					$sVrednosti .= "'" . Support::encodeTo1250($opis) . "',";
				$sPolja .= " proizvodjac,";				$sVrednosti .= "'" . $proizvodjac . "',";
				$sPolja .= " kolicina,";				$sVrednosti .= " " . number_format(1.00,2,'.','') . ",";
				$sPolja .= " cena_nc,";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric(floatval($cena_nc),1,$kurs,$valuta_id_nc),2, '.', '') . ",";
				$sPolja .= " pmp_cena";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric(floatval($pmp_cena),1,$kurs,$valuta_id_nc),2, '.', '') . "";		

				
				DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
			}		
		
}			
			
			Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}

		}

	}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){

		if($extension==null){
			Support::autoDownload(Support::autoLink($dobavljac_id),'files/pirot/pirot_excel/pirot.xlsx');
			$products_file = "files/pirot/pirot_excel/pirot.xlsx";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}
		
		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	       	
	       	$i = 0;
   			while ($excelObj->setActiveSheetIndex($i)){

        	$worksheet = $excelObj->getActiveSheet();
			$lastRow = $worksheet->getHighestRow();
			
       		$i++;
       		if($i == 22){
       			break;
       		}


	        for ($row = 1; $row <= $lastRow; $row++) {
	            $sifra   = $worksheet->getCell('A'.$row)->getValue();
	            $naziv   = $worksheet->getCell('B'.$row)->getValue();
				$cena_nc = $worksheet->getCell('C'.$row)->getValue();
				$slika   = $worksheet->getCell('E'.$row)->getValue();
				$opis    = $worksheet->getCell('D'.$row)->getValue();
				$proizvodjac = 'pirot Toys';
					
					$naziv=strtolower($naziv);
					$cena_nc=$cena_nc*1.2;
																
					$sPolja = '';
					$sVrednosti = '';
					$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
					$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= "'" . addslashes($sifra) . "',";
					$sPolja .= " naziv,";					$sVrednosti .= "'" . addslashes(Support::encodeTo1250($naziv)) . "  " .  addslashes(Support::encodeTo1250($opis)) . "  ( " .  addslashes(Support::encodeTo1250($sifra)) . " )',";
					$sPolja .= " kolicina,";				$sVrednosti .= " " . number_format(1.00,2,'.','') . ",";
					$sPolja .= " proizvodjac,";				$sVrednosti .= " '" . $proizvodjac . "',";
					$sPolja .= " flag_slika_postoji,";		$sVrednosti .= " 1,";
					$sPolja .= " mpcena,";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric(floatval($cena_nc),1,$kurs,$valuta_id_nc),2, '.', '') . ",";
					$sPolja .= " pmp_cena,";				$sVrednosti .= " " . number_format(Support::replace_empty_numeric(floatval($cena_nc),1,$kurs,$valuta_id_nc),2, '.', '') . ",";
					$sPolja .= " web_cena";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric(floatval($cena_nc),1,$kurs,$valuta_id_nc),2, '.', '') . "";


					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");	
					if(!empty($slika)){
					DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp (partner_id,sifra_kod_dobavljaca,putanja,akcija) VALUES(".$dobavljac_id.",'".$sifra."','".$slika."',1 )");	
				}
			}
		}

			//Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}


}