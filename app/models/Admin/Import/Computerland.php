<?php
namespace Import;
use Import\Support;
use DB;
use AdminOptions;
use File;
use ZipArchive;

class Computerland {

	public static function execute($dobavljac_id,$extension=null){
		
		Support::initQueryExecute();

		if($extension==null){
			if(Support::autoLink($dobavljac_id) != null && Support::serviceUsername($dobavljac_id) != null && Support::servicePassword($dobavljac_id) != null){
				self::autoDownload(Support::autoLink($dobavljac_id),Support::serviceUsername($dobavljac_id),Support::servicePassword($dobavljac_id));
			}
			$file_name = Support::file_name("files/computerland/computerland_xml/");
			if($file_name!==false){
				$products = simplexml_load_file("files/computerland/computerland_xml/".$file_name);
			}else{
				$products = array();
			}
		}else{
			$file = AdminOptions::base_url().'files/import.'.$extension;
			$products = simplexml_load_file($file);	
		}

		//podaci o proizvodima
		foreach ($products as $product) {
			$insert_arr = array();
			$insert_arr['partner_id'] = $dobavljac_id;
			$insert_arr['sifra_kod_dobavljaca'] = $product->attributes()->code;
			$insert_arr['proizvodjac'] = Support::encodeTo1250($product->producer);
			$insert_arr['naziv'] = Support::encodeTo1250($product->name." ( ".$product->model." ) ");
			$insert_arr['grupa'] = Support::encodeTo1250($product->group);
			$insert_arr['model'] = $product->model;
			$insert_arr['kolicina'] = $product->onStock == "true" ? "1" : "0";
			$insert_arr['barkod'] = $product->barcode;
			$insert_arr['cena_nc'] = $product->priceA;
			$insert_arr['mpcena'] = $product->priceC;
			$insert_arr['pmp_cena'] = $product->priceC;
			$insert_arr['pdv'] = $product->tax;
			$insert_arr['flag_opis_postoji'] = $product->description == "" ? "0" : "1";
			$insert_arr['opis'] = Support::encodeTo1250($product->description);
			$insert_arr['web_flag_karakteristike'] = "0";
			$insert_arr['flag_slika_postoji'] = $product->image == "" || $product->image == "https://b2b.computerland.rs/b2b/res/basic/images/no_image.png" ? "0" : "1";

			DB::table('dobavljac_cenovnik_temp')->insert($insert_arr);

			if($insert_arr['flag_slika_postoji'] == '1'){
				$sPolja = '';
				$sVrednosti = '';
				$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
				$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "" . Support::quotedStr($product->attributes()->code) . ",";
				$sPolja .= "akcija,";		            $sVrednosti .= "" . 1 . ",";
				$sPolja .= "putanja";				    $sVrednosti .= "'" . $product->image. "'";
				
					
				DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
			}
		}

		Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array('i','u'));

		//Brisemo fajl
		$file_name = Support::file_name("files/computerland/computerland_xml/");
		if($extension!=null){
			File::delete('files/import.'.$extension);
		}
		elseif($file_name!==false){
			File::delete("files/computerland/computerland_xml/".$file_name);
		}

	}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){
		Support::initQueryExecute();

		if($extension==null){
			if(Support::autoLink($dobavljac_id) != null && Support::serviceUsername($dobavljac_id) != null && Support::servicePassword($dobavljac_id) != null){
				self::autoDownload(Support::autoLink($dobavljac_id),Support::serviceUsername($dobavljac_id),Support::servicePassword($dobavljac_id));
			}
			$file_name = Support::file_name("files/computerland/computerland_xml/");
			if($file_name!==false){
				$products = simplexml_load_file("files/computerland/computerland_xml/".$file_name);
			}else{
				$products = array();
			}
		}else{
			$file = AdminOptions::base_url().'files/import.'.$extension;
			$products = simplexml_load_file($file);			
		}

		//podaci o proizvodima
		foreach ($products as $product) {
			$insert_arr = array();
			$insert_arr['partner_id'] = $dobavljac_id;
			$insert_arr['sifra_kod_dobavljaca'] = $product->attributes()->code;
			$insert_arr['kolicina'] = $product->onStock == "true" ? "1" : "0";
			$insert_arr['cena_nc'] = $product->priceA;
			$insert_arr['mpcena'] = $product->priceC;
			$insert_arr['pmp_cena'] = $product->priceC;

			DB::table('dobavljac_cenovnik_temp')->insert($insert_arr);
		}

		//Support::queryShortExecute($dobavljac_id);

		//Brisemo fajl
		$file_name = Support::file_name("files/computerland/computerland_xml/");
		if($extension!=null){
			File::delete('files/import.'.$extension);
		}
		elseif($file_name!==false){
			File::delete("files/computerland/computerland_xml/".$file_name);
		}

	}

	public static function autoDownload($links,$username,$password){

		$links = explode('---',$links);
		$postinfo = "j_username=".$username."&j_password=".$password."";
		$cookie_file_path = 'cookie.txt';

		$myfile = fopen("files/computerland/computerland_xml/computerland.zip", "w");

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_NOBODY, false);
		curl_setopt($ch, CURLOPT_URL, $links[0]);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

		curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file_path);
		curl_setopt($ch, CURLOPT_COOKIE, "cookiename=0");
		curl_setopt($ch, CURLOPT_USERAGENT,
		    "Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.7.12) Gecko/20050915 Firefox/1.0.7");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_REFERER, $_SERVER['REQUEST_URI']);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);

		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postinfo);
		curl_exec($ch);

		//dovlacenje xml-a
		curl_setopt($ch, CURLOPT_URL, $links[1]);
		curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($ch, CURLOPT_FILE, $myfile);
		curl_exec ($ch);
		curl_close($ch);

		$zip = new ZipArchive;
		if ($zip->open('files/computerland/computerland_xml/computerland.zip') === TRUE) {
		    $zip->extractTo('files/computerland/computerland_xml');
		    $zip->close();
		    File::delete('files/computerland/computerland_xml/computerland.zip');
		}
	}

	public static function getComputerlandImageContent($image,$destination){
		$arrContextOptions=array(
		    "ssl"=>array(
		        "verify_peer"=>false,
		        "verify_peer_name"=>false,
		    ),
		); 
		$content = file_get_contents($image,false,stream_context_create($arrContextOptions));
		// $fp = fopen(str_replace('.jpg','.png',$destination), "w");
		$fp = fopen($destination, "w");
		fwrite($fp, $content); 
		fclose($fp);
	}

}

?>