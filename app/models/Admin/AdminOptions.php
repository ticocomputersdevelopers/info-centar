<?php 

class AdminOptions {
	
	public static function base_url(){
		
		return DB::table('options')->where('options_id',1316)->pluck('str_data');
		
	}
	public static function company_name(){
			$cn_query=DB::table('preduzece')->get();
			foreach($cn_query as $row){
				return $row->naziv;
			}
			
		}

	public static function company_adress(){
		$cn_query=DB::table('preduzece')->get();
			foreach($cn_query as $row){
				return $row->adresa;
			}
	}
	public static function company_phone(){
		$cn_query=DB::table('preduzece')->get();
			foreach($cn_query as $row){
				return $row->telefon;
			}
	}
	public static function company_fax(){
		$cn_query=DB::table('preduzece')->get();
			foreach($cn_query as $row){
				return $row->fax;
			}
			
	}

	public static function company_ziro(){
		$cn_query=DB::table('preduzece')->get();
			foreach($cn_query as $row){
				return $row->ziro;
			}
	}
	public static function company_pib(){
		$cn_query=DB::table('preduzece')->get();
			foreach($cn_query as $row){
				return $row->pib;
			}
	}
	public static function company_email(){
		$cn_query=DB::table('preduzece')->get();
			foreach($cn_query as $row){
				return $row->email;
			}
	}
	public static function company_delatnost_sifra(){
		$cn_query=DB::table('preduzece')->get();
			foreach($cn_query as $row){
				return $row->delatnost_sifra;
			}
	}
    
	public static function user_cnfirm_registration(){
			$cn_query=DB::table('web_options')->where('web_options_id',100)->get();
			foreach($cn_query as $row){
				return $row->int_data;
			}
	}
	public static function user_registration(){
				$cn_query=DB::table('web_options')->where('web_options_id',0)->get();
			foreach($cn_query as $row){
				return $row->int_data;
			}
	}
    public static function neregistrovani_korisnici(){
				$cn_query=DB::table('web_options')->where('web_options_id',1)->get();
			foreach($cn_query as $row){
				return $row->int_data;
			}
	}
	public static function vodjenje_lagera(){
				$cn_query=DB::table('web_options')->where('web_options_id',103)->get();
			foreach($cn_query as $row){
				return $row->int_data;
			}
	}
    public static function newsletter(){
				$cn_query=DB::table('web_options')->where('web_options_id',104)->get();
			foreach($cn_query as $row){
				return $row->int_data;
			}
	}
    
    public static function product_view(){
				$cn_query=DB::table('web_options')->where('web_options_id',106)->get();
			foreach($cn_query as $row){
				return $row->int_data;
			}
	}
	public static function sifra_view(){
				$cn_query=DB::table('web_options')->where('web_options_id',200)->get();
			foreach($cn_query as $row){
				return $row->int_data;
			}
	}
	public static function sifra_view_web(){
				$cn_query=DB::table('web_options')->where('web_options_id',201)->get();
			foreach($cn_query as $row){
				return $row->int_data;
			}
	}
	public static function prikaz_sifre(){
				$cn_query=DB::table('web_options')->where('web_options_id',203)->get();
			foreach($cn_query as $row){
				return $row->int_data;
			}
	}
    public static function product_number(){
				$cn_query=DB::table('web_options')->where('web_options_id',107)->get();
			foreach($cn_query as $row){
				return $row->int_data;
			}
	}
    
    public static function product_sort(){
				$cn_query=DB::table('web_options')->where('web_options_id',108)->get();
			foreach($cn_query as $row){
				return $row->int_data;
			}
	}
    
    public static function product_currency(){
				$cn_query=DB::table('web_options')->where('web_options_id',109)->get();
			foreach($cn_query as $row){
				return $row->int_data;
			}
	}
    
    public static function category_type(){
				$cn_query=DB::table('web_options')->where('web_options_id',112)->get();
			foreach($cn_query as $row){
				return $row->int_data;
			}
	}
    	public static function category_view(){
				$cn_query=DB::table('web_options')->where('web_options_id',115)->get();
			foreach($cn_query as $row){
				return $row->int_data;
			}
	}
    	public static function all_category(){
				$cn_query=DB::table('web_options')->where('web_options_id',116)->get();
			foreach($cn_query as $row){
				return $row->int_data;
			}
	}
    	public static function compare(){
				$cn_query=DB::table('web_options')->where('web_options_id',117)->get();
			foreach($cn_query as $row){
				return $row->int_data;
			}
	}

	public static function header_type(){
		$cn_query=DB::table('web_options')->where('web_options_id',114)->get();
		foreach($cn_query as $row){
		return $row->int_data;
		}
	}

	public static function gnrl_options($options_id,$kind='int_data'){
		return DB::table('options')->where('options_id',$options_id)->pluck($kind);
	}

	public static function web_options($options_id){
		return DB::table('web_options')->where('web_options_id',$options_id)->pluck('int_data');
	}

	public static function kurs(){
		$obj = DB::table('kursna_lista')->orderBy('kursna_lista_id','desc')->first();
		if(isset($obj)){
			return $obj->prodajni;
		}else{
			return 0;
		}
	} 

    public static function server(){
        return DB::table('options')->where('options_id',1326)->pluck('str_data');
    }
	
	public static function enable_filters(){
			return DB::table('web_options')->where('web_options_id',110)->pluck('int_data');
	}

	public static function filters_type(){
			return DB::table('web_options')->where('web_options_id',111)->pluck('int_data');
	}

	public static function limit_liste_robe(){
		// $limit =  DB::table('options')->where('options_id',1329)->pluck('int_data');
		// if($limit > 400){
		// 	$limit = 400;
		// }
		return 100;
	}
	public static function versions(){
		$first =  DB::table('verzija_prodavnice')->orderBy('verzija_id','desc')->first();
		$string = '';
		if($first->prodavnica != null){
			$string .= 'Selltico: '.$first->prodavnica.', ';
		}
		// if($first->web_admin != null){
		// 	$string .= 'Web admin: '.$first->web_admin.', ';
		// }
		// if($first->b2b_prodavnica != null){
		// 	$string .= 'B2b shop: '.$first->b2b_prodavnica.', ';
		// }
		// if($first->b2b_admin != null){
		// 	$string .= 'B2b admin: '.$first->b2b_admin.', ';
		// }
		return substr($string,0,-2);
	}
	public static function checkB2B(){
		if(in_array(DB::table('web_options')->where('web_options_id',130)->pluck('int_data'),array(1,2))){
			return true;
		}else{
			return false;
		}
	}

    public static function updateSelected($data){
        DB::table('web_b2c_newsletter_options')
            ->where('selected', 1)
            ->update(array(
                    'server' => $data['email_host'],
                    'port' => $data['email_port'],
                    'username' => $data['email_username'],
                    'pass' => $data['email_password'],
                    'subject' => $data['email_subject'],
                    'name_from' => $data['email_name']
                ));
    }

    //EMAIL
    // vraca username
    public static function username(){
        return DB::table('web_b2c_newsletter_options')->where('selected', 1)->pluck('username');
    }
    // vraca password
    public static function password(){
        return DB::table('web_b2c_newsletter_options')->where('selected', 1)->pluck('pass');
    }
    // vraca server
    public static function email_server(){
        return DB::table('web_b2c_newsletter_options')->where('selected', 1)->pluck('server');
    }
    // vraca port
    public static function port(){
        return DB::table('web_b2c_newsletter_options')->where('selected', 1)->pluck('port');
    }
    // vraca name
    public static function name(){
        return DB::table('web_b2c_newsletter_options')->where('selected', 1)->pluck('name_from');
    }
    // vraca subject
    public static function subject(){
        return DB::table('web_b2c_newsletter_options')->where('selected', 1)->pluck('subject');
    }
    public static function mail_from(){
        return DB::table('web_b2c_newsletter_options')->where('selected', 1)->pluck('mail_from');
    }

	 public static function url_convert($str,$lang=null){
	    $table = array(
	            'Š'=>'S', 'š'=>'s', 'Đ'=>'Dj', 'đ'=>'dj', 'Ž'=>'Z', 'ž'=>'z', 'Č'=>'C', 'č'=>'c', 'Ć'=>'C', 'ć'=>'c',
	            'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
	            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O',
	            'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss',
	            'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e',
	            'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o',
	            'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b',
	            'ÿ'=>'y', 'Ŕ'=>'R', 'ŕ'=>'r', '/' => '-', ' ' => '-'," )"=> ""
	    );
	    $string = strtolower(strtr($str, $table));
	    $string = str_replace(array('А','Б','В','Г','Д','Ђ','Е','Ж','З','И','Ј','К','Л','Љ','М','Н','Њ','О','П','Р','С','Т','Ћ','У','Ф','Х','Ц','Ч','Џ','Ш','а','б','в','г','д','ђ','е','ж','з','и','ј','к','л','љ','м','н','њ','о','п','р','с','т','ћ','у','ф','х','ц','ч','џ','ш'),array('a','b','v','g','d','dj','e','z','z','i','j','k','l','lj','m','n','nj','o','p','r','s','t','c','u','f','h','c','c','dz','s','a','b','v','g','d','dj','e','z','z','i','j','k','l','lj','m','n','nj','o','p','r','s','t','c','u','f','h','c','c','dz','s'), $string);
	    $urlKey = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $string)));
		// return Language::slug_trans($urlKey,$lang);
		return $urlKey;
	}
	 public static function convert_url($str,$lang=null){
		return strtolower(AdminLanguage::slug_trans($str,$lang));
	}
		
	public static function search_conver($str){
		 $string_new=strtolower($str); 
		$urlKey = strtr($string_new, array("&frasl;"=>"/"," "=>"%"));
	
		return $urlKey;
	}
	public static function text_convert($string){
		$urlKey = strtr($string, array("?"=>"&scaron;","?"=>"&#269;","?"=>"&#263;","?"=>"&#273;","?"=>"&#382;","?"=>"&Scaron;","?"=>"&#268;","?"=>"&#262;","?"=>"&#272;","?"=>"&#381;",'"'=>"","±"=>" "));
    	return $urlKey;
	}

	public static function checkImage($join=null){
		if(AdminOptions::gnrl_options(1308)){
			return '';
		}else{
			$result = 'AND ws.roba_id IS NOT NULL ';
			if($join == 'join'){
				$result = ' LEFT JOIN web_slika ws ON r.roba_id = ws.roba_id';
			}
			return $result;
		}
	}

	public static function checkCharacteristics($join=null){
		if(AdminOptions::gnrl_options(1306)){
			return '';
		}else{
			$result = 'AND (r.web_karakteristike IS NOT NULL OR wrk.roba_id IS NOT NULL OR dck.roba_id IS NOT NULL) ';
			if($join == 'join'){
				$result = ' LEFT JOIN web_roba_karakteristike wrk ON r.roba_id = wrk.roba_id LEFT JOIN dobavljac_cenovnik_karakteristike dck ON r.roba_id = dck.roba_id';
			}
			return $result;
		}
	}
	public static function checkPrice(){
		if(AdminOptions::gnrl_options(1307)){
			return '';
		}else{
			return 'AND r.web_cena > 0 ';
		}
	}

	public static function checkDescription(){
		if(AdminOptions::gnrl_options(1305)){
			return '';
		}else{
			return 'AND r.web_opis IS NOT NULL ';
		}
	}

		public static function sifra(){
		return DB::table('options')->where('options_id', 3002)->pluck('str_data');
	}

	public static function prodavnica_boje($prodavnica_boje,$default=false){
		$prodavnica_boje = DB::table('prodavnica_boje')->where('prodavnica_boje_id',$prodavnica_boje)->first();
		if(!$default && $prodavnica_boje->kod && $prodavnica_boje->kod != ''){
			return $prodavnica_boje->kod;
		}
		return $prodavnica_boje->default_kod;
	}
	public static function live_base_url($lang=null){
		$link = AdminOptions::base_url();
		if(AdminLanguage::multi()){
			if(is_null($lang)){
				$link .= AdminLanguage::lang().'/';
			}else{
				$link .= $lang.'/';
			}
		}
		return $link;
	}

	public static function checkNarudzbine(){
		if(DB::table('web_options')->where('web_options_id',135)->pluck('int_data')==1){
			return true;
		}else{
			return false;
		}
	}

	public static function checkAOPTrial(){
		$user = DB::table('imenik')->where('imenik_id',1)->first();
		return AdminOptions::gnrl_options(3013) == 1 && !is_null($user) and $user->trial == 1 && AdminSupport::trial_date() != false;
	}

    public static function is_shop(){
    	$stil = DB::table('prodavnica_stil')->where(array('aktivna'=>1,'izabrana'=>1))->first();
    	return DB::table('prodavnica_tema')->where('prodavnica_tema_id',$stil->prodavnica_tema_id)->pluck('shop') == 1;
    }
    public static function info_sys($sifra){
        return DB::table('informacioni_sistem')->where(array('sifra'=>$sifra,'aktivan'=>1,'izabran'=>1))->first();
    }
    public static function web_import_kolone($kolona_sifra,$col){
    	if(!in_array($col,array('kolona_sifra','naziv','width','css_class'))){
    		return null;
    	}
        return DB::table('web_import_kolone')->where(array('kolona_sifra'=>$kolona_sifra))->pluck($col);
    }
    public static function admin_artikli_kolone($kolona_sifra,$col){
    	if(!in_array($col,array('kolona_sifra','naziv','width','css_class'))){
    		return null;
    	}
        return DB::table('admin_artikli_kolone')->where(array('kolona_sifra'=>$kolona_sifra))->pluck($col);
    }
    
}