<?php
namespace DirectImport;

use DirectImport\Support;
use DB;

class Article {

	public static function table_body($articles){
		$result_arr = array();
		$codes = array();

		$roba_id = DB::select("SELECT nextval('roba_roba_id_seq')")[0]->nextval;
		$sifra_k = DB::table('roba')->max('sifra_k')+1;
		$grupa_pr_id = -1;

		foreach($articles as $article) {

			$roba_id++;
			$sifra_k++;
			$sifra_is = $article->code;
			$naziv = pg_escape_string(substr($article->name,0,300));
			$grupa_pr_id = Support::getGrupaId($article->group);
			$tarifna_grupa_id = Support::getTarifnaGrupaId($article->tax);
			// $jedinica_mere_id = Support::getJedinicaMereId($article->jm);
			$jedinica_mere_id = "1";
			$proizvodjac_id = Support::getProizvodjacId($article->manufacturer);
			$racunska_cena_nc = intval($article->purchase_price);
			$mpcena = intval($article->retail_price); //(1+intval($article->porez)/100)
			$web_cena = intval($article->web_price);
			$web_opis = pg_escape_string($article->description);
			$web_karakteristike = pg_escape_string(Support::htmlCharacteristics($article->characteristics_groups));
			$barkod = $article->barcode;
			$model = pg_escape_string($article->model);
			// $akcija = strpos($article->atributi, 'A') !== false ? 1 : 0;
			$akcija = 0;

			$result_arr[] = "(".strval($roba_id).",NULL,'".addslashes($naziv)."',NULL,NULL,NULL,".$grupa_pr_id.",".$tarifna_grupa_id.",".strval($jedinica_mere_id).",".strval($proizvodjac_id).",-1,".strval($sifra_k).",NULL,NULL,'".addslashes(substr($naziv,0,20))."',0,-1,0,0,0,0,9,0,0,0,0,1,1,0,NULL,1,".strval($racunska_cena_nc).",0,".strval($racunska_cena_nc).",0,NULL,".strval($mpcena).",false,0,(NULL)::integer,'".addslashes($naziv)."',1,NULL,NULL,(NULL)::integer,(NULL)::integer,0,0,0,-1,-1,".strval($web_cena).",1,0,'".strval($web_opis)."','".strval($web_karakteristike)."',NULL,0,".strval($akcija).",0,NULL,NULL,NULL,NULL,1,0,'".addslashes($barkod)."',0,0,1,1,-1,'".strval($model)."',NULL,NULL,NULL,0,0.00,0.00,0.00,0,NULL,(NULL)::date,(NULL)::date,(NULL)::integer,NULL,'".strval($sifra_is)."',NULL)";

		}

		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {


		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='roba'"));
		$table_temp = "(VALUES ".$table_temp_body.") roba_temp(".implode(',',$columns).")";
		// DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");

		// update
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		$updated_columns_without_prices=array();
		foreach($columns as $col){
			if($col!="roba_id" && $col!="sifra_is" && $col!="sifra_d"){
		    	$updated_columns[] = "".$col." = roba_temp.".$col."";
		    	if($col!="racunska_cena_nc" && $col!="mpcena"){
		    		$updated_columns_without_prices[] = "".$col." = roba_temp.".$col."";
		    	}
			}
		}

		DB::statement("UPDATE roba t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE roba_temp.racunska_cena_nc > 0 AND roba_temp.mpcena > 0 AND t.sifra_is=roba_temp.sifra_is::varchar");
		if(count($updated_columns_without_prices) > 0){
			DB::statement("UPDATE roba t SET ".implode(',',$updated_columns_without_prices)." FROM ".$table_temp." WHERE (roba_temp.racunska_cena_nc <= 0 OR roba_temp.mpcena <= 0) AND t.sifra_is=roba_temp.sifra_is::varchar");
		}
		//insert
		DB::statement("INSERT INTO roba (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM roba t WHERE t.sifra_is=roba_temp.sifra_is::varchar))");



		DB::statement("SELECT setval('roba_roba_id_seq', (SELECT MAX(roba_id) FROM roba), FALSE)");
	}

	public static function query_update_unexists($table_temp_body) {

		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='roba'"));
		$table_temp = "(VALUES ".$table_temp_body.") roba_temp(".implode(',',$columns).")";

		// DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		DB::statement("UPDATE roba t SET flag_aktivan = 0 WHERE t.sifra_is IS NOT NULL AND NOT EXISTS(SELECT * FROM ".$table_temp." WHERE t.sifra_is=roba_temp.sifra_is)");
		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
	}

}