<?php

class OrderIS {

    public static function orderLogik($nacin_isporuke,$nacin_placanja,$napomena=''){
        $mp_kupac_id_is = !is_null(Options::info_sys('logik')->mp_kupac_id_is) ? Options::info_sys('logik')->mp_kupac_id_is : '100';
        $partner_id = DB::table('partner')->where('id_is',$mp_kupac_id_is)->pluck('partner_id');
        $web_b2c_narudzbina_id = DB::select("SELECT currval('web_b2c_narudzbina_web_b2c_narudzbina_id_seq')")[0]->currval;
        $current_order_id = DB::connection('logik')->select("SELECT MAX(sifra_narudzbine_logik) as current_order_id FROM narudzbine")[0]->current_order_id + 1;
        $sql = "INSERT INTO narudzbine (sifra_narudzbine_logik,sifra_narudzbine_tico,sifra_kupca_logik,sifra_kupca_tico,vrsta_narudzbina,datum_narudzbine,sifra_nacin_isporuke,naziv_nacin_isporuke,sifra_nacin_placanja,naziv_nacin_placanja,narudzbina_prihvacena,narudzbina_stornirana,narudzbina_realizovana,roba_poslata,sluzba_slanje,broj_posiljke,napomena,semaphore) VALUES (".$current_order_id.",'".strval($web_b2c_narudzbina_id)."',".$mp_kupac_id_is.",".($partner_id ? $partner_id : 'NULL').",0,'".date('Y-m-d')."',1,'".$nacin_isporuke."',1,'".$nacin_placanja."',0,0,0,0,'Nedefinisana',NULL,'".$napomena."',1)";
        DB::connection('logik')->statement($sql);
        return (object) array('id_is'=>$current_order_id,'web_b2c_narudzbina_id'=>$web_b2c_narudzbina_id);
    }
    public static function addOrderStavkaLogik($order_ids,$article,$item_number,$quantity){
        $sql = "INSERT INTO narudzbine_stavke (sifra_narudzbine_logik,sifra_narudzbine_tico,sifra_artikla_logik,sifra_artikla_tico,redni_broj_stavke,kolicina,sifra_tarifne_grupe,nabavna_cena,dilerska_cena,end_cena,maloprodajna_cena,web_cena,prodajna_cena,semaphore) VALUES (".strval($order_ids->id_is).",'".strval($order_ids->web_b2c_narudzbina_id)."',".strval($article->id_is).",'".strval($article->roba_id)."',".strval($item_number).",".strval($quantity).",".strval($article->tarifna_grupa_id).",".$article->racunska_cena_nc.",".$article->racunska_cena_nc.",".$article->web_cena.",".$article->mpcena.",".$article->web_cena.",".($article->web_cena*(1+(1 / DB::table('tarifna_grupa')->where('tarifna_grupa_id',$article->tarifna_grupa_id)->pluck('porez')))).",1)";
        DB::connection('logik')->statement($sql);
    }

    public static function createOrderLogik($orderDetails){
        $success = false;
        $order_id = 0;

        $customer = DB::table('web_kupac')->where('web_kupac_id',$orderDetails['web_kupac_id'])->first();
        if(!is_null($customer)){
            $delivery = DB::table('web_nacin_isporuke')->where('web_nacin_isporuke_id',$orderDetails['web_nacin_isporuke_id'])->pluck('naziv');
            $payment = DB::table('web_nacin_placanja')->where('web_nacin_placanja_id',$orderDetails['web_nacin_placanja_id'])->pluck('naziv');

            $note = ($customer->flag_vrsta_kupca == 0 ? ($customer->ime.' '.$customer->prezime) : ($customer->naziv.' '.$customer->pib)) .';'. ($customer->adresa ? $customer->adresa : '') .';'. ($customer->mesto ? $customer->mesto : '') .';'. ($customer->telefon ? $customer->telefon : '') .';'. ($customer->email ? $customer->email : '') .';'.$orderDetails['napomena'];

            $order_ids = self::orderLogik($delivery,$payment,$note);
            $cartItems = DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$order_ids->web_b2c_narudzbina_id)->get();
            foreach($cartItems as $key => $stavka){
                $article = DB::table('roba')->where('roba_id',$stavka->roba_id)->first();
                self::addOrderStavkaLogik($order_ids,$article,($key+1),$stavka->kolicina);
            }

            $success = true;
        }
        return (object) array('success'=>$success, 'order_id'=>$order_id);
    }



    public static function orderInfograf($partner_id,$napomena=''){
        $sql = "INSERT INTO narudzbina (partner_id,napomena) VALUES (".strval($partner_id).",'".strval($napomena)."')";
        DB::connection('infograf')->statement($sql);
        $last_order_id = DB::connection('infograf')->select("SELECT MAX(id) as last_order_id FROM narudzbina")[0]->last_order_id;
        return $last_order_id;
    }
    public static function addOrderStavkaInfograf($web_order_id,$article_id,$quantity,$amount,$discount){
        $sql = "INSERT INTO narudzbina_stavka (narudzbina_id,artikal_id,kolicina,cena,rabat) VALUES (".strval($web_order_id).",".strval($article_id).",".strval($quantity).",".strval($amount).",".strval($discount).")";
        DB::connection('infograf')->statement($sql);
    }
    public static function orderConfirmInfograf($order_id){
        $sql = "CALL web_order(".strval($order_id).")";
        $result = DB::connection('infograf')->select($sql);
        if(isset($result[0])){
            return $result[0]->dok;
        }
        return 0;
    }
    public static function createOrderInfograf($orderDetails){
        $partner_id_is = !is_null(Options::info_sys('infograf')->mp_kupac_id_is) ? Options::info_sys('logik')->mp_kupac_id_is : '100';
        $success = false;
        $order_id = 0;

        $kurs = DB::table('kursna_lista')->where('kursna_lista_id',DB::table('kursna_lista')->max('kursna_lista_id'))->pluck('ziralni');

        $customer = DB::table('web_kupac')->where('web_kupac_id',$orderDetails['web_kupac_id'])->first();

        if(!is_null($customer)){
            $web_b2c_narudzbina_id = DB::select("SELECT currval('web_b2c_narudzbina_web_b2c_narudzbina_id_seq')")[0]->currval;
            $delivery = DB::table('web_nacin_isporuke')->where('web_nacin_isporuke_id',$orderDetails['web_nacin_isporuke_id'])->pluck('naziv');
            $payment = DB::table('web_nacin_placanja')->where('web_nacin_placanja_id',$orderDetails['web_nacin_placanja_id'])->pluck('naziv');

            $note = strval($web_b2c_narudzbina_id) .';'. ($customer->flag_vrsta_kupca == 0 ? ($customer->ime.' '.$customer->prezime) : ($customer->naziv.' '.$customer->pib)) .';'. ($customer->adresa ? $customer->adresa : '') .';'. ($customer->mesto ? $customer->mesto : '') .';'. ($customer->telefon ? $customer->telefon : '') .';'. ($customer->email ? $customer->email : '') .';'. $delivery .';'. $payment .';'. $orderDetails['napomena'];

            $cartItems = DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->get();

            $order_id = self::orderInfograf($partner_id_is,$note);

            foreach($cartItems as $stavka){
                $stavka_id = DB::table('roba')->where('roba_id',$stavka->roba_id)->pluck('id_is');
                $b2bRabatCene = B2bArticle::b2bRabatCene($stavka->roba_id);
                self::addOrderStavkaInfograf($order_id,$stavka_id,$stavka->kolicina,($b2bRabatCene->ukupna_cena / $kurs),$b2bRabatCene->ukupan_rabat);
            }
            
            $order_id = self::orderConfirmInfograf($order_id);
            $success = true;
        }

        return (object) array('success'=>$success, 'order_id'=>$order_id);
    }
}
