<?php
use Service\Mailer;

class WebKupac {

    public static function send_user_request($web_kupac_id){
        
       $user = DB::table('web_kupac')->where('web_kupac_id',$web_kupac_id)->first();

        $body='Da biste zavrsili Vašu registraciju, potrebno je da kliknete na link <a href="'.Options::base_url().'potvrda-registracije/'.$user->kod.'/'.$web_kupac_id.'" target="_blank">'.Options::base_url().'potvrda-registracije/'.$user->kod.'/'.$web_kupac_id.'</a>';
        $subject="Potvrda regisistracije za ".Options::server();
        WebKupac::send_email_to_client($body,$user->email,$subject);
    }

    public static function send_email_to_client($body,$email,$subject){
        
        if(Options::gnrl_options(3003)){        

             Mail::send('shop.email.order', array('body'=>$body), function($message) use ($email, $subject)
            {
                $message->from(EmailOption::mail_from(), EmailOption::name());

                $message->to($email)->subject($subject);
            });
        }else{
            Mailer::send(Options::company_email(),$email, $subject, $body);    
        }

    }

    public static function get_user_name(){
        $query=DB::table('web_kupac')->where('web_kupac_id',Session::get('b2c_kupac'));
        foreach($query->get() as $row){
            return $row->ime." ".$row->prezime;
        }
    }

    public static function get_company_name(){
        $query=DB::table('web_kupac')->where('web_kupac_id',Session::get('b2c_kupac'));
        foreach($query->get() as $row){
            return $row->naziv;
        }
    }

    // public static function user_name($web_kupac_id){
    //     $name=DB::table('web_kupac')->where('web_kupac_id',$web_kupac_id)->pluck('ime');
    //     return $name;
    // }

    // public static function user_surname($web_kupac_id){
        
    //     $prezime=DB::table('web_kupac')->where('web_kupac_id',$web_kupac_id)->pluck('prezime');
        
    //     return $prezime;
    // }
    
    // public static function user_company($web_kupac_id){
        
    //     $naziv=DB::table('web_kupac')->where('web_kupac_id',$web_kupac_id)->pluck('naziv');
        
    //     return $naziv;
    // }

    // public static function user_pib($web_kupac_id){
        
    //     $pib=DB::table('web_kupac')->where('web_kupac_id',$web_kupac_id)->pluck('pib');
        
    //     return $pib;
    // }

    // public static function user_email($web_kupac_id){
        
    //     $email=DB::table('web_kupac')->where('web_kupac_id',$web_kupac_id)->pluck('email');
        
    //     return $email;
    // }


    // public static function user_lozinka($web_kupac_id){
        
    //     $lozinka=DB::table('web_kupac')->where('web_kupac_id',$web_kupac_id)->pluck('lozinka');
        
    //     return base64_decode($lozinka);
    // }

    // public static function user_address($web_kupac_id){
    //     $adresa=DB::table('web_kupac')->where('web_kupac_id',$web_kupac_id)->pluck('adresa');
    //     return $adresa;
    // }

    // public static function user_mesto($web_kupac_id){
        
    //     $mesto_id=DB::table('web_kupac')->where('web_kupac_id',$web_kupac_id)->pluck('mesto_id');
        
    //     return $mesto_id;
    // }
    // 
    
    // public static function user_phone($web_kupac_id){
        
    //     $telefon=DB::table('web_kupac')->where('web_kupac_id',$web_kupac_id)->pluck('telefon');
        
    //     return $telefon;
    // }

    // public static function tip_usera($web_kupac_id){
    //    $tip_kupca=DB::table('web_kupac')->where('web_kupac_id',$web_kupac_id)->pluck('flag_vrsta_kupca');
        
    //     return $tip_kupca; 
    // }

    
}