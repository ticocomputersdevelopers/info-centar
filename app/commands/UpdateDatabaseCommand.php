<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class UpdateDatabaseCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'update:db';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		DB::statement("create or replace function addcol(schemaname varchar, tablename varchar, colname varchar, coltype varchar)
returns varchar 
language 'plpgsql'
as 
$$
declare 
    col_name varchar ;
begin 
      execute 'select column_name from information_schema.columns  where  table_schema = ' ||
      quote_literal(schemaname)||' and table_name='|| quote_literal(tablename) || '   and    column_name= '|| quote_literal(colname)    
      into   col_name ;   

      raise info  ' the val : % ', col_name;
      if(col_name is null ) then 
          col_name := colname;
          execute 'alter table ' ||schemaname|| '.'|| tablename || ' add column '|| colname || '  ' || coltype; 
      else
           col_name := colname ||' Already exist';
      end if;
return col_name;
end;
$$");
		DB::statement("create or replace function dropcol(schemaname varchar, tablename varchar, colname varchar)
returns varchar 
language 'plpgsql'
as 
$$
declare 
    col_name varchar ;
begin 
      execute 'select column_name from information_schema.columns  where  table_schema = ' ||
      quote_literal(schemaname)||' and table_name='|| quote_literal(tablename) || '   and    column_name= '|| quote_literal(colname)    
      into   col_name ;   

      raise info  ' the val : % ', col_name;
      if(col_name is null ) then 
           col_name := colname ||' Not exist';
      else
          col_name := colname;
          execute 'alter table ' ||schemaname|| '.'|| tablename || ' drop column '|| colname; 
      end if;
return col_name;
end;
$$");
		DB::statement("create or replace function addtable(schemaname varchar, tablename varchar, body varchar)
returns varchar 
language 'plpgsql'
as 
$$
declare 
    tab_name varchar;
begin 
      execute 'select table_name from information_schema.tables  where  table_schema = ' ||
      quote_literal(schemaname)||' and table_name='|| quote_literal(tablename)    
      into   tab_name;   

      raise info  ' the val : % ', tab_name;
      if(tab_name is null ) then 
          tab_name := tablename;
          execute 'create table ' ||schemaname|| '.'|| tablename || ' ('|| body ||') with (OIDS=TRUE)';
      else
           tab_name := tablename ||' Already exist';
      end if;
return tab_name;
end;
$$");

DB::statement("CREATE OR REPLACE FUNCTION addconstraint (t_name text, c_name text, constraint_sql text)
  RETURNS void
AS
$$
  begin
    -- Look for our constraint
    if not exists (SELECT * FROM information_schema.constraint_column_usage where constraint_name = c_name) 
    	then
        execute 'ALTER TABLE ' || t_name || ' ADD CONSTRAINT ' || c_name || ' ' || constraint_sql;
    end if;
end;
$$
LANGUAGE plpgsql VOLATILE;");

	}

	/**
	 * Execute the console command. 
	 *
	 * @return mixed
	 */
	public function fire()
	{
		DB::statement("SELECT addcol('public','prodavnica_tema','shop','integer DEFAULT 1')");
		//teme
		DB::statement("INSERT INTO prodavnica_tema (prodavnica_tema_id,naziv,putanja,aktivna,shop) SELECT * FROM (VALUES 
	    	(6,'Avatar','avatar/',1,1),
	    	(7,'Clothes','clothes/',1,1),
	    	(8,'Shoes','shoes/',1,1),
	    	(9,'Prezentacija Opšta','landingPage/',1,0),
	    	(10,'Furniture','furniture/',1,1)) 
	    	redovi(prodavnica_tema_id,naziv,putanja,aktivna,shop) 
	        WHERE prodavnica_tema_id NOT IN (SELECT prodavnica_tema_id FROM prodavnica_tema)");

	    DB::statement("INSERT INTO prodavnica_stil (prodavnica_stil_id,prodavnica_tema_id,naziv,putanja,aktivna,izabrana,zakljucana) SELECT * FROM (VALUES 
	    	(13,6,'Light','light/',1,0,0), (14,6,'Light Green','light-green/',1,0,0), 
	    	(15,6,'Light Orange','light-orange/',1,0,0), (16,7,'Light','light/',1,0,0), 
	    	(17,8,'Light','light/',1,0,0), (18,8,'Sportska oprema','sportska_oprema/',1,0,0), 
	    	(19,9,'Light','light/',1,0,0), (20,10,'Light','light/',1,0,0))
	    	redovi(prodavnica_stil_id,prodavnica_tema_id,naziv,putanja,aktivna,izabrana,zakljucana) 
	        WHERE prodavnica_stil_id NOT IN (SELECT prodavnica_stil_id FROM prodavnica_stil)");

	    //web_b2c_seo
	    DB::statement("SELECT addcol('public','web_b2c_seo','grupa_pr_id','integer DEFAULT -1')");
 		DB::statement("SELECT addcol('public','web_b2c_seo','tekst','character varying (300)')");
 		DB::statement("SELECT addcol('public','web_b2c_seo','parrent_id','integer DEFAULT 0')");
	    // DB::statement("INSERT INTO web_b2c_seo (naziv_stranice,title,menu_top,header_menu,footer,b2b_header,b2b_footer,disable,tip_artikla_id) SELECT * FROM (VALUES 
	    // 	('o-nama','O nama',0,0,0,0,0,0,-1)
	    // 	) redovi(naziv_stranice,title,menu_top,header_menu,footer,b2b_header,b2b_footer,disable,tip_artikla_id) 
	    //     WHERE naziv_stranice NOT IN (SELECT naziv_stranice FROM web_b2c_seo)");

		// add table baneri jezik
	    DB::statement("SELECT addtable('public','baneri_jezik','baneri_id integer NOT NULL,jezik_id integer NOT NULL,naslov character varying(100),nadnaslov character varying(100),sadrzaj text,naslov_dugme character varying(100), CONSTRAINT baneri_jezik_pkey PRIMARY KEY (baneri_id, jezik_id), CONSTRAINT jezik_id_fkey FOREIGN KEY (jezik_id) REFERENCES public.jezik (jezik_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");

		//preduzece
		DB::statement("SELECT addcol('public','preduzece','instagram','character varying(255)')");
		DB::statement("SELECT addcol('public','preduzece','linkedin','character varying(255)')");
		DB::statement("SELECT addcol('public','preduzece','youtube','character varying(255)')");
		DB::statement("ALTER TABLE preduzece ALTER COLUMN delatnost_sifra TYPE character varying(255)");

		//posta_slanje
		DB::statement("SELECT addcol('public','posta_slanje','difolt','smallint DEFAULT 0')");
		DB::statement("SELECT addcol('public','posta_slanje','api_aktivna','smallint DEFAULT 0')");
		DB::statement("SELECT addcol('public','posta_slanje','nasa_sifra','character varying(255)')");
		
	    //options
	    DB::statement("INSERT INTO options (options_id,preduzece_id,naziv,str_data,int_data) SELECT * FROM (VALUES (3023,1,'Intesa banking Client ID',null,0),
	        (3024,1,'Intesa banking Store Key',null,0),
	        (3025,1,'Uključen limit za artikle',null,1),
	        (3026,1,'AOP limit za artikle',null,100),
	        (3028,1,'Web cena kao osnovna B2B cena',null,1),
	        (3029,1,'Skraćeni admin',null,1),
	        (3030,1,'Brzo umatičenje artikala',null,0),
	        (3031,1,'Pakovanje artikla',null,0),
	        (3032,1,'Kombinacija rabata - rabat IS kategoriaja',null,0),
	        (3033,1,'Web Import - update naziva',null,0),
	        (3034,1,'Korišćenje kategorije kupca za formiranje cena na B2B-u',null,0),
	        (3035,1,'Slanje mail-a kupcu posle obrade narudžbine',null,0)
	        ) redovi(options_id,preduzece_id,naziv,str_data,int_data) 
	        WHERE options_id NOT IN (SELECT options_id FROM options)");
	    //web options
		DB::statement("INSERT INTO web_options (web_options_id,sekcija_id,naziv,str_data,int_data,bool_data,aplikacija_id) SELECT * FROM (VALUES 
			(98,1,'Prikaz akcije na početnoj strani',null,1,TRUE,100),
			(99,1,'Prikaz blogova na početnoj',null,1,TRUE,100),
			(150,1,'Troskovi isporuke',null,0,TRUE,100),
			(151,1,'Prikaz najpopularnijih proizvoda na B2B-u',null,1,TRUE,100),
			(152,1,'Zaokruživanje cena',null,0,TRUE,100),
			(100,1,'Korisnik mora da uradi potvrdu registracije',null,0,TRUE,100),
			(101,1,'Broj proizvoda na akciji',null,1,TRUE,100),
			(200,1,'Prikaz sifre,id,sifre_d ili seo',null,1,TRUE,100),
			(201,1,'Vrsta sifre webu',null,1,TRUE,100),
			(203,1,'Prikaz sifre',null,0,TRUE,100),
			(204,1,'Kurs na B2C-u',null,0,TRUE,100),
			(205,1,'Kurs na B2B-u',null,0,TRUE,100),
			(206,1,'Artikli akcije i tipova na početnoj, bez ajax-a u slick-u',null,0,TRUE,100),
			(207,1,'Cene opadajući redosled',null,0,TRUE,100),
			(208,1,'Najnoviji, najpopularniji, najprodavaniji na početnoj',null,1,TRUE,100),
			(209,1,'Zaokruživanje cena, broj decimala',null,0,TRUE,100)
			) redovi (web_options_id,sekcija_id,naziv,str_data,int_data,bool_data,aplikacija_id) WHERE web_options_id NOT IN (SELECT web_options_id FROM web_options)");
	    
	    //update export
	    DB::statement("INSERT INTO export (export_id,naziv,vrednost_kolone,dozvoljen) SELECT * FROM (VALUES (1,'IT Svet','it-svet',0),(2,'Pametno.rs','pametno-rs',0),(3,'Shopmania','shopmania',0),(4,'Magento','magento',0),(5,'Woocommerce','woocommerce',0),(6,'Shopify','shopify',0),(7,'Eponuda','eponuda',0),(8,'Kupindo','kupindo',0),(9,'CeneRS','ceners',1)) redovi(export_id,naziv,vrednost_kolone,dozvoljen) 
	        WHERE export_id NOT IN (SELECT export_id FROM export)");

	    DB::statement("SELECT addtable('public','export_grupe','export_grupe_id bigserial NOT NULL, grupa_id bigint NOT NULL, naziv character varying(255), parent_id bigint, grupa_pr_ids character varying(255), export_id integer NOT NULL, CONSTRAINT export_grupe_pkey PRIMARY KEY (export_grupe_id)')");
	    DB::statement("DROP TABLE IF EXISTS shopmania_grupe");

	    DB::statement("SELECT dropcol('public','roba','narudzbina_status_id')");

	    DB::statement("INSERT INTO narudzbina_status (narudzbina_status_id,naziv,selected) SELECT * FROM (VALUES 
	    	(1,'Bez statusa',1)) 
	    	redovi(narudzbina_status_id,naziv,selected) 
	        WHERE narudzbina_status_id NOT IN (SELECT narudzbina_status_id FROM narudzbina_status)");

		DB::statement("SELECT addcol('public','grupa_pr','sifra_is','character varying(255)')");
		DB::statement("SELECT addcol('public','grupa_pr','id_is','character varying(255)')");
		DB::statement("SELECT addcol('public','grupa_pr','pozadinska_slika','character varying(255)')");

		DB::statement("SELECT addcol('public','proizvodjac','id_is','character varying(255)')");
		DB::statement("SELECT addcol('public','web_slika','id_is','character varying(255)')");



		//dodaj id_kategorije u partner tabelu
		DB::statement("SELECT addcol('public','partner','id_kategorije','integer DEFAULT NULL')");

/*****************************************************************************************************************/
		// B2C baneri
		DB::statement("SELECT addcol('public','baneri','aktivan','integer DEFAULT 1')");
		DB::statement("SELECT addcol('public','baneri','datum_od','date')");
		DB::statement("SELECT addcol('public','baneri','datum_do','date')");
		DB::statement("SELECT addcol('public','baneri','link2','character varying(255)')");

		//B2B baneri
		DB::statement("SELECT addtable('public','baneri_b2b','baneri_id bigserial, img text, link text, redni_broj integer,naziv character (255), tip_prikaza integer NOT NULL, web_b2b_seo_id integer')");
		// B2B baneri
		DB::statement("SELECT addcol('public','baneri_b2b','aktivan','integer DEFAULT 1')");
		DB::statement("SELECT addcol('public','baneri_b2b','datum_od','date')");
		DB::statement("SELECT addcol('public','baneri_b2b','datum_do','date')");
		DB::statement("SELECT addcol('public','baneri_b2b','link2','character varying(255)')");
/*****************************************************************************************************************/
		// B2B Vesti
		DB::statement("SELECT addtable('public','web_vest_b2b','web_vest_b2b_id bigserial, datum date, naslov character varying(200), aktuelno smallint DEFAULT 1,slika character varying (300), rbr integer')");
		DB::statement("SELECT addtable('public','web_vest_b2b_jezik','web_vest_b2b_id bigint NOT NULL, jezik_id integer, naslov character varying(200), sadrzaj text, title character varying (100), description character varying(255), keywords character varying(255)')");
		// b2b seo
		DB::statement("SELECT addtable('public','web_b2b_seo','web_b2b_seo_id bigserial, naziv_stranice character varying(300), title character varying(300), description text, keywords text, og_title character varying(300), og_description character varying(300), og_image character varying(300), og_type character varying(300), og_url character varying(300), og_site_name character varying(300), twitter_card character varying(300), twitter_title character varying(300), twitter_description character varying(300), twitter_url character varying(300), twitter_image character varying(300), twitter_creator character varying(300), flag_page integer, rb_strane integer, web_content text, seo_title character varying(255), flag_b2b_show integer, tip_artikla_id integer DEFAULT -1, disable integer DEFAULT 0, menu_top integer DEFAULT 1, header_menu integer DEFAULT 1, footer integer DEFAULT 1, b2b_header integer DEFAULT 1, b2b_footer integer DEFAULT 1, grupa_pr_id integer DEFAULT -1')"); 
		DB::statement("SELECT addtable('public','web_b2b_seo_jezik','web_b2b_seo_id integer NOT NULL, jezik_id integer NOT NULL, sadrzaj text, title character varying(100), description character varying(255), keywords character varying(255)')");	

/*****************************************************************************************************************/	    
		//informacioni sistem
	    DB::statement("SELECT addtable('public','informacioni_sistem','informacioni_sistem_id bigserial NOT NULL, naziv character varying(255), sifra character varying(255), aktivan smallint DEFAULT 0, izabran smallint DEFAULT 0, CONSTRAINT informacioni_sistem_pkey PRIMARY KEY (informacioni_sistem_id)')");
	    DB::statement("INSERT INTO informacioni_sistem (informacioni_sistem_id, naziv, sifra, aktivan, izabran) SELECT * FROM (VALUES (1, 'Excel fajl', 'excel', 1, 0),(2, 'Wings', 'wings', 1, 0),(3, 'Calculus', 'calculus', 1, 0),(4, 'Infograf', 'infograf', 1, 0),(5, 'Logik', 'logik', 1, 0),(6, 'Xml', 'xml', 1, 0)) redovi(informacioni_sistem_id, naziv, sifra, aktivan, izabran) WHERE informacioni_sistem_id NOT IN (SELECT informacioni_sistem_id FROM informacioni_sistem)");
	    DB::statement("SELECT addcol('public','informacioni_sistem','api_url','character varying(255)')");
	    DB::statement("SELECT addcol('public','informacioni_sistem','portal','character varying(255)')");
	    DB::statement("SELECT addcol('public','informacioni_sistem','username','character varying(255)')");
	    DB::statement("SELECT addcol('public','informacioni_sistem','password','character varying(255)')");
	    DB::statement("SELECT addcol('public','informacioni_sistem','b2b_magacin','character varying(255)')");
	    DB::statement("SELECT addcol('public','informacioni_sistem','b2c_magacin','character varying(255)')");
	    DB::statement("SELECT addcol('public','informacioni_sistem','mp_kupac_id_is','character varying(255)')");


	    DB::statement("SELECT addtable('public','narudzbina_opstina','narudzbina_opstina_id bigserial NOT NULL,code integer NOT NULL,naziv character varying(255),ptt character varying(255), CONSTRAINT narudzbina_opstina_unique UNIQUE (code), CONSTRAINT narudzbina_opstina_id_pkey PRIMARY KEY (narudzbina_opstina_id, code)')");

	    DB::statement("SELECT addtable('public','narudzbina_mesto','narudzbina_mesto_id bigserial NOT NULL,code integer NOT NULL,narudzbina_opstina_code integer NOT NULL,naziv character varying(100),ptt character varying(255), CONSTRAINT narudzbina_mesto_unique UNIQUE (code), CONSTRAINT narudzbina_mesto_pkey PRIMARY KEY (narudzbina_mesto_id, code), CONSTRAINT narudzbina_opstina_code_fkey FOREIGN KEY (narudzbina_opstina_code) REFERENCES public.narudzbina_opstina (code) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");

	    DB::statement("SELECT addtable('public','narudzbina_ulica','narudzbina_ulica_id bigserial NOT NULL,code integer NOT NULL,narudzbina_mesto_code integer NOT NULL,naziv character varying(100),ptt character varying(255), CONSTRAINT narudzbina_ulica_unique UNIQUE (code,narudzbina_ulica_id), CONSTRAINT narudzbina_ulica_pkey PRIMARY KEY (narudzbina_ulica_id, code), CONSTRAINT narudzbina_mesto_code_fkey FOREIGN KEY (narudzbina_mesto_code) REFERENCES public.narudzbina_mesto (code) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");

		//web_kupac
		DB::statement("SELECT addcol('public','web_kupac','ulica_id','integer')");
		DB::statement("SELECT addcol('public','web_kupac','broj','character varying(255)')");
		// DB::statement("ALTER TABLE web_kupac ADD CONSTRAINT web_kupac_ulica_id_fkey FOREIGN KEY (ulica_id) REFERENCES public.narudzbina_ulica (narudzbina_ulica_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE");

	    DB::statement("SELECT addtable('public','posta_status','posta_status_id bigserial NOT NULL,code character varying(100),name character varying(100),description character varying(255), posta_slanje_id integer NOT NULL, CONSTRAINT posta_status_pkey PRIMARY KEY (posta_status_id), CONSTRAINT posta_slanje_id_fkey FOREIGN KEY (posta_slanje_id) REFERENCES public.posta_slanje (posta_slanje_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");

/*******************************************************************************/
	    DB::statement("SELECT addtable('public','partner_grupa','partner_grupa_id bigserial NOT NULL, partner_id integer NOT NULL,grupa_pr_id integer NOT NULL,grupa character varying(255),web_marza numeric(20,4) DEFAULT 0,mp_marza numeric(20,4) DEFAULT 0, CONSTRAINT partner_grupa_pkey PRIMARY KEY (partner_grupa_id), CONSTRAINT partner_grupa_partner_id_fkey FOREIGN KEY (partner_id) REFERENCES public.partner (partner_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE, CONSTRAINT partner_grupa_grupa_pr_id_fkey FOREIGN KEY (grupa_pr_id) REFERENCES public.grupa_pr (grupa_pr_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");
	    DB::statement("SELECT addtable('public','partner_proizvodjac','partner_proizvodjac_id bigserial NOT NULL, partner_id integer NOT NULL,proizvodjac_id integer NOT NULL,proizvodjac character varying(255), CONSTRAINT partner_proizvodjac_pkey PRIMARY KEY (partner_proizvodjac_id), CONSTRAINT partner_proizvodjac_partner_id_fkey FOREIGN KEY (partner_id) REFERENCES public.partner (partner_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE, CONSTRAINT partner_proizvodjac_proizvodjac_id_fkey FOREIGN KEY (proizvodjac_id) REFERENCES public.proizvodjac (proizvodjac_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");

	    //tabela za kategorije
	    DB::statement("SELECT addtable('public','partner_kategorija','id_kategorije bigserial NOT NULL, naziv character varying(255), rabat integer, active integer DEFAULT 0, CONSTRAINT partner_kategorija_pkey PRIMARY KEY (id_kategorije)')");

/*******************************************************************************/

	    DB::statement("INSERT INTO posta_status (code, name, description, posta_slanje_id) SELECT * FROM (VALUES 
	    	('-2', 'Obrisana pošiljka', 'Obrisana pošiljka', 3),
	    	('-1', 'Storno isporuke', 'Storno isporuke', 3),
	    	('0', 'Kreirana pošiljka', 'Priprema za dostavu', 3),
	    	('1', 'Pošiljka isporučena', 'Pošiljka je isporučena', 3),
	    	('3', 'Pošiljka preuzeta', 'Pošiljka preuzeta', 3),
	    	('5', 'Odbijena pošiljka', 'Pošiljka je odbijena od strane primaoca', 3),
	    	('6', 'Nema nikog', 'Pokušana isporuka, nema nikoga na adresi', 3),
	    	('7', 'Na odmoru', 'Pokušana isporuka, primalac je na godišnjem odmoru', 3),
	    	('8', 'Netačna adresa', 'Pokušana isporuka, netačna je adresa primaoca', 3),
	    	('9', 'Nema novac', 'Pokušana isporuka, primalac nema novac', 3),
	    	('10', 'Neodgovarajuća sadržina', 'Sadržaj pošiljke nije odgovarajući', 3),
	    	('11', 'Oštećena pošiljka', 'Pošiljka je oštećena-reklamacioni postupak', 3),
	    	('12', 'Primalac odložio', 'Isporuka odložena u dogovoru sa primaocem', 3),
	    	('20', 'Pošiljka vraćena pošiljaocu', 'Pošiljka vraćena pošiljaocu', 3),
	    	('21', 'Kreiran povrat', 'Pošiljka se vraća pošiljaocu', 3),
	    	('22', 'Ukinut povrat', 'Ukinut povrat pošiljke', 3),
	    	('105', 'Odbijena pošiljka', 'Pošiljka je odbijena od strane primaoca', 3),
	    	('106', 'Nema nikog', 'Ponovni pokušaj isporuke, nema nikoga na adresi', 3),
	    	('107', 'Na odmoru', 'Ponovni pokušaj isporuke, primalac je na odmoru', 3),
	    	('108', 'Netačna adresa', 'Ponovni pokušaj isporuke, netačna adresa primaoca', 3),
	    	('109', 'Nema novac', 'Ponovni pokušaj isporuke, primalac nema novac', 3),
	    	('110', 'Neodgovarajuća sadržina', 'Sadržaj pošiljke nije odgovarajući', 3),
	    	('111', 'Oštećena pošiljka', 'Pošiljka je oštećena-reklamacioni postupak', 3),
	    	('112', 'Primalac odložio', 'Isporuka odložena u dogovoru sa primaocem', 3)
	    	) redovi(code, name, description, posta_slanje_id) 
	        WHERE NOT EXISTS (SELECT * FROM posta_status)");

	    DB::statement("SELECT addcol('public','web_b2c_narudzbina_stavka','posta_zahtev','integer NOT NULL DEFAULT 0')");
	    DB::statement("SELECT addcol('public','web_b2c_narudzbina_stavka','posta_reference_id','character varying(255)')");
	    DB::statement("SELECT addcol('public','web_b2c_narudzbina_stavka','posta_response_data','character varying(500)')");

		DB::statement("SELECT addcol('public','web_b2c_narudzbina_stavka','posiljalac_id','integer DEFAULT 1')");
		DB::statement("SELECT addconstraint('web_b2c_narudzbina_stavka','web_b2c_narudzbina_stavka_posiljalac_id_fkey','FOREIGN KEY (posiljalac_id) REFERENCES public.partner (partner_id) MATCH SIMPLE ON UPDATE SET NULL ON DELETE SET NULL')");

		DB::statement("SELECT addcol('public','web_b2c_narudzbina_stavka','posta_slanje_id','integer')");
		DB::statement("SELECT addconstraint('web_b2c_narudzbina_stavka','web_b2c_narudzbina_stavka_posta_slanje_id_fkey','FOREIGN KEY (posta_slanje_id) REFERENCES public.posta_slanje (posta_slanje_id) MATCH SIMPLE ON UPDATE SET NULL ON DELETE SET NULL')");
		DB::statement("SELECT addcol('public','web_b2c_narudzbina_stavka','posta_status_id','integer')");
		DB::statement("SELECT addconstraint('web_b2c_narudzbina_stavka','web_b2c_narudzbina_stavka_posta_status_id_fkey','FOREIGN KEY (posta_status_id) REFERENCES public.posta_status (posta_status_id) MATCH SIMPLE ON UPDATE SET NULL ON DELETE SET NULL')");
		DB::statement("SELECT addcol('public','web_b2c_narudzbina_stavka','ulica_id','integer')");
		DB::statement("SELECT addcol('public','web_b2c_narudzbina_stavka','broj','character varying(255)')");
		DB::statement("SELECT addcol('public','web_b2c_narudzbina_stavka','broj_paketa','numeric(20,4) DEFAULT 0')");
		DB::statement("SELECT addcol('public','web_b2c_narudzbina_stavka','web_nacin_placanja_id','integer')");
		DB::statement("SELECT addcol('public','web_b2c_narudzbina_stavka','transport_placa','integer DEFAULT 0')");
		DB::statement("SELECT addcol('public','web_b2c_narudzbina_stavka','cena_otkupa','numeric(20,4) DEFAULT 0')");
		DB::statement("SELECT addcol('public','web_b2c_narudzbina_stavka','racun_otkupa','character varying(255)')");
		DB::statement("SELECT addcol('public','web_b2c_narudzbina_stavka','otkup_prima','integer DEFAULT -1')");
		DB::statement("SELECT addcol('public','web_b2c_narudzbina_stavka','vrednost','numeric(20,4) DEFAULT 0')");
		DB::statement("SELECT addcol('public','web_b2c_narudzbina_stavka','masa','numeric(20,4) DEFAULT 0')");
		DB::statement("SELECT addcol('public','web_b2c_narudzbina_stavka','odgovor','integer DEFAULT 0')");
		DB::statement("SELECT addcol('public','web_b2c_narudzbina_stavka','napomena','character varying(255)')");
		DB::statement("SELECT addcol('public','web_b2c_narudzbina_stavka','opis','character varying(255)')");

		DB::statement("SELECT addcol('public','preduzece','ulica_id','integer')");
		DB::statement("SELECT addcol('public','preduzece','broj','character varying(255)')");
		DB::statement("SELECT addcol('public','preduzece','kontakt_osoba','character varying(255)')");
		
		DB::statement("SELECT addcol('public','partner','ulica_id','integer')");
		DB::statement("SELECT addcol('public','partner','broj','character varying(255)')");
		DB::statement("SELECT addcol('public','partner','kontakt_osoba','character varying(255)')");
		DB::statement("ALTER TABLE partner ALTER COLUMN naziv TYPE character varying(255)");
		DB::statement("ALTER TABLE partner ALTER COLUMN login TYPE character varying(255)");
		DB::statement("ALTER TABLE partner ALTER COLUMN password TYPE character varying(255)");
		DB::statement("ALTER TABLE partner ALTER COLUMN racun TYPE character varying(255)");
		DB::statement("ALTER TABLE partner ALTER COLUMN adresa TYPE character varying(255)");
		DB::statement("ALTER TABLE partner ALTER COLUMN telefon TYPE character varying(255)");

	    DB::statement("INSERT INTO export_grupe (export_grupe_id, grupa_id, naziv, export_id) SELECT * FROM (VALUES 
	    	(1,18,'Desktop računari',8), (2,2725,'Bicikli',8), (3,1981,'Prirodne nauke',8), (4,2729,'Dodatna oprema',8), (5,2733,'Rezervni delovi',8), (6,2013,'Hrišćanstvo',8), (7,4353,'LiceUlice',8), (8,2053,'Majice i košulje',8), (9,518,'Programi',8), (10,1031,'Srbija i ex-YU',8), (11,524,'Antropologija',8), (12,1036,'Automobili',8), (13,528,'Bušilice',8), (14,1809,'Kućišta, maske i fioke za laptop',8), (15,1044,'Ručni satovi',8), (16,534,'Sklopke',8), (17,1050,'Etno predmeti',8), (18,1056,'Jugoslavija 1918-41',8), (19,545,'Setovi',8), (20,1313,'Pribor za zavarivanje i lemljenje',8), (21,291,'Ploče',8), (22,297,'Nakit',8), (23,301,'Šeširi i kačketi',8), (24,1070,'CEPT i Evropa',8), (25,1077,'Azija',8), (26,1349,'Pneumatski pištolji i creva',8), (27,1097,'Srbija i YU 1876-1960',8), (28,2889,'Sport',8), (29,1102,'Srbija i YU 1868-1945',8), (30,1118,'Ulje',8), (31,1127,'Srbija i ex-YU do 1945',8), (32,1143,'Delovi',8), (33,1153,'Punjači za laptop',8), (34,901,'Elektronika',8), (35,136,'Mobilni telefoni',8), (36,4237,'Bade mantil',8), (37,1685,'Futrole',8), (38,675,'Romani',8), (39,973,'Gelovi',8), (40,976,'Ukrasi',8), (41,2001,'Katalozi',8), (42,1493,'Politikin Zabavnik',8), (43,3029,'Akcija / Avantura',8), (44,1245,'Burgije',8), (45,232,'Automehanika',8), (46,1012,'Figure',8), (47,1017,'Srbija i ex-YU',8), (48,2041,'Decoupage predmeti',8), (49,2553,'Sadnice',8), (50,506,'Router i switch',8), (51,1277,'Voltmetri, ampermetri i termometri',8), (52,766,'Senke i olovke za oči',8), (53,2541,'Seme',8), (54,3593,'Stari nakit',8), (55,1024,'Gradovi',8), (56,2305,'Sanitarije',8), (57,517,'Operativni sistemi i drajveri',8), (58,1032,'Inostranstvo',8), (59,1289,'Ključevi',8), (60,1545,'Bonelli stripovi',8), (61,269,'Maske',8), (62,1039,'Lutke',8), (63,1043,'Džepni satovi',8), (64,533,'Aku alati',8), (65,1813,'Šarke za laptop',8), (66,2325,'Oprema za pse',8), (67,535,'Prekidači i utičnice',8), (68,1305,'Aparati za zavarivanje',8), (69,28,'Optički uređaji',8), (70,2589,'Za pripremu hrane',8), (71,288,'Audio diskovi',8), (72,1057,'Jugoslavija 1944-60',8), (73,1063,'Rusija',8), (74,1833,'Konzole',8), (75,2349,'Vrata',8), (76,2613,'Kamere i kompleti',8), (77,1078,'Afrika',8), (78,1597,'Felne i ratkapne',8), (79,2365,'Lampe',8), (80,2621,'Fiskalne kase',8), (81,72,'Klima uređaji i ventilatori',8), (82,1353,'Pumpe, dizalice i prese',8), (83,1098,'Srbija i YU 1961-danas',8), (84,1103,'Srbija i YU 1946-danas',8), (85,1111,'LCD',8), (86,1113,'Fotografije, grafike i crteži',8), (87,2905,'Životinje',8), (88,1117,'Kombinovane tehnike',8), (89,1128,'Srbija i ex-YU 1945-60',8), (90,108,'Patike',8), (91,2925,'Domaća zimnica',8), (92,110,'Cipele',8), (93,370,'Torbe, torbice i novčanici',8), (94,117,'Mirisi',8), (95,2421,'Šerpe, lonci, tiganji i džezve',8), (96,1142,'Oprema',8), (97,1917,'Kardio sprave',8), (98,132,'Fotoaparati i kamere',8), (99,1925,'Pidžame i bade mantili',8), (100,2449,'Ukrasi',8), (101,4241,'Benkica',8), (102,153,'Audio komponente',8), (103,2713,'Nakit i ukrasi za kosu',8), (104,419,'Arheologija',8), (105,2477,'Posteljine',8), (106,431,'Žičani instrumenti',8), (107,2737,'Sportska obuća',8), (108,441,'Televizori',8), (109,1721,'Brushalteri i setovi',8), (110,1213,'Stolarske mašine',8), (111,2749,'Štapovi za pecanje',8), (112,1473,'Laptopovi',8), (113,1989,'Osnovna škola',8), (114,1481,'Baterije za laptop',8), (115,971,'Četkice',8), (116,975,'Nalepnice',8), (117,2005,'Brošure',8), (118,2521,'Zavesa',8), (119,987,'Košulje',8), (120,1501,'Sport i hobi',8), (121,990,'Haljine',8), (122,1249,'Glodala i noževi',8), (123,2017,'Priče',8), (124,2273,'Garniture, ležajevi i fotelje',8), (125,482,'Majice',8), (126,233,'Autoelektrika',8), (127,2793,'Roleri i rolšue',8), (128,491,'Haljine',8), (129,1011,'Posuđe',8), (130,2037,'Decoupage oprema',8), (131,1273,'Mikrometri i pomična merila',8), (132,1018,'Inostranstvo',8), (133,507,'Mrežni kablovi',8), (134,763,'Sjajevi, olovke i karmini',8), (135,3069,'Biografija',8), (136,1281,'Vage',8), (137,771,'Lakovi',8), (138,267,'SIM kartice (brojevi)',8), (139,1805,'Konektori i kablovi',8), (140,2573,'Lukovice i rizomi',8), (141,1038,'Makete',8), (142,529,'Brusilice',8), (143,1045,'Zidni satovi',8), (144,1048,'Narodne nošnje',8), (145,1051,'Gobleni',8), (146,1309,'Lemilice',8), (147,543,'Klešta',8), (148,289,'Audio kasete',8), (149,1058,'Jugoslavija 1961-80',8), (150,298,'Satovi i oprema',8), (151,1068,'Bugarska',8), (152,1073,'Australija',8), (153,1329,'Spojnice',8), (154,1090,'Bonovi, obveznice i menice',8), (155,1096,'Evropa',8), (156,1099,'Antika i Srednji vek',8), (157,1116,'Akvarel, akril i tempera',8), (158,2909,'Crtani, film i junaci',8), (159,1120,'Ikone',8), (160,1129,'Srbija i ex-YU 1961-80',8), (161,1393,'Poljoprivredne mašine',8), (162,3193,'Etnografija',8), (163,1149,'Torbe, rančevi i futrole',8), (164,897,'Građevinski materijal',8), (165,904,'Ostalo',8), (166,140,'Baterije',8), (167,2959,'Touch screen (bez ekrana)',8), (168,4245,'Bermude',8), (169,2967,'Digitroni',8), (170,674,'Bajke',8), (171,970,'Tipse',8), (172,977,'Šabloni',8), (173,1241,'Brusni materijali',8), (174,2009,'Uputstva',8), (175,1505,'Muzika i film',8), (176,227,'Do 50 ccm',8), (177,238,'Enterijer',8), (178,1010,'Lampe',8), (179,499,'Suknje',8), (180,505,'Wireless kartice',8), (181,3065,'Drama',8), (182,1019,'Ostalo',8), (183,764,'Puderi i rumenila',8), (184,2199,'Miljei',8), (185,2533,'Ukrasno cveće',8), (186,2877,'Sport',8), (187,2185,'Ukrasi',8), (188,2545,'Saksije i žardinjere',8), (189,1025,'Kompanije',8), (190,2561,'Preparati',8), (191,1541,'Alan Ford',8), (192,1801,'Inverteri za laptop',8), (193,2313,'Slavine i tuševi',8), (194,1035,'Disney',8), (195,525,'Etnologija',8), (196,1297,'Noževi i sečiva',8), (197,787,'Nalepnice i folije',8), (198,532,'Testere',8), (199,789,'Budizam',8), (200,25,'Tastature',8), (201,793,'Mitologija',8), (202,1049,'Ćilimi i tkanja',8), (203,2329,'Oprema za mačke',8), (204,284,'Filmovi',8), (205,29,'Grafičke kartice',8), (206,2593,'Za termičku obradu hrane',8), (207,1059,'Jugoslavija 1981-91',8), (208,1065,'Mađarska',8), (209,299,'Naočare i oprema',8), (210,1325,'Ventili i manometri',8), (211,1837,'Kontroleri',8), (212,2353,'Prozori',8), (213,1076,'Severna Amerika',8), (214,1589,'Stoni satovi',8), (215,2617,'Ostala sigurnosna oprema',8), (216,1089,'Pribor i literatura',8), (217,1601,'Xenon kompleti i sijalice',8), (218,2625,'Ostala oprema',8), (219,1093,'Azija',8), (220,74,'Frižideri i zamrzivači',8), (221,2637,'Optički instrumenti',8), (222,2893,'Životinje',8), (223,2385,'Sijalice',8), (224,1106,'Nemačka',8), (225,1115,'Pastel, kolaž i vitraž',8), (226,97,'Obuća',8), (227,1121,'Skulpture i maske',8), (228,2913,'Ostalo',8), (229,105,'Cipele',8), (230,1130,'Srbija i ex-YU od 1981',8), (231,111,'Čizme',8), (232,2929,'Domaći džem, pekmez i marmelada',8), (233,2429,'Posude za čuvanje hrane i pića',8), (234,1921,'Sprave za vežbe snage',8), (235,1929,'Gaće',8), (236,2453,'Satovi',8), (237,4249,'Bluza',8), (238,154,'Slušalice i mikrofoni',8), (239,157,'Video i DVD plejeri',8), (240,2717,'Kape, rukavice i šalovi',8), (241,426,'Duvački instrumenti',8), (242,2481,'Prekrivači, jorgani i ćebad',8), (243,2741,'Dresovi i sportska odeća',8), (244,1725,'Spavaćice',8), (245,2753,'Mašinice',8), (246,1993,'Srednja škola',8), (247,972,'Lampe',8), (248,978,'Ostalo',8), (249,979,'Ostalo',8), (250,1753,'Flat kablovi',8), (251,988,'Mantili',8), (252,2269,'Stolovi, stolice i klupe',8), (253,2525,'Oprema',8), (254,3037,'Epska fantastika',8), (255,223,'Car Hifi',8), (256,991,'Mantili',8), (257,480,'Košulje',8), (258,228,'Do 250 ccm',8), (259,1253,'Ležajevi',8), (260,1509,'Nauka i kultura',8), (261,2025,'Basne',8), (262,234,'Autolimarija',8), (263,2797,'Skejt',8), (264,1013,'Ostalo',8), (265,1269,'Multimetri',8), (266,508,'Modemi',8), (267,765,'Maskare i eyelineri',8), (268,2897,'Crtani, filmovi i junaci',8), (269,1037,'Avioni',8), (270,1293,'Ureznice i nareznice',8), (271,271,'Kablovi',8), (272,1041,'Kinder i PEZ figurice',8), (273,531,'Šlajferice',8), (274,790,'Hinduizam',8), (275,1046,'Ostalo',8), (276,1052,'Ostalo',8), (277,1066,'Poljska',8), (278,300,'Ukrasi za kosu',8), (279,1074,'Južna Amerika',8), (280,66,'Medicinske nauke',8), (281,1091,'Žetoni i čekovi',8), (282,1094,'Amerika i Australija',8), (283,1357,'Klipovi, cilindri i razvodnici',8), (284,1105,'Mađarska',8), (285,1361,'Libele i metri',8), (286,1119,'Ostalo',8), (287,1123,'Reprodukcije',8), (288,1131,'Evropa',8), (289,119,'Za kosu',8), (290,3197,'Filologija',8), (291,129,'Monitori',8), (292,898,'Za termotehniku',8), (293,2193,'Nameštaj',8), (294,4253,'Bodi',8), (295,1973,'Pidžame',8), (296,967,'Ostalo',8), (297,974,'Ostalo',8), (298,1757,'Zvučnici i mikrofoni',8), (299,229,'Do 750 ccm',8), (300,2021,'Pesme',8), (301,1261,'Pribor za stolarske mašine',8), (302,239,'Prateća oprema',8), (303,1777,'Ekrani za laptop',8), (304,498,'Pantalone',8), (305,3061,'Epska poezija',8), (306,504,'Mrežne karte i adapteri',8), (307,1533,'Časopisi za žene',8), (308,1829,'Motorna ulja',8), (309,2901,'Ostalo',8), (310,1026,'Politika',8), (311,1285,'Ostalo',8), (312,2309,'Za čišćenje i higijenu',8), (313,519,'Igre',8), (314,265,'Zvučnici',8), (315,272,'Tastature',8), (316,1040,'Ostalo',8), (317,2833,'Američki stripovi',8), (318,1821,'Kamere, zvučnici i mikrofoni',8), (319,30,'Hard diskovi i SSD',8), (320,544,'Šrafcigeri',8), (321,2341,'Oprema za akvaristiku',8), (322,2597,'Za pripremu kafe i čaja',8), (323,1064,'Čehoslovačka',8), (324,1321,'Kompresori',8), (325,1075,'Srednja Amerika',8), (326,2357,'Oprema',8), (327,2873,'Naočare za decu i oprema',8), (328,62,'Filozofija',8), (329,1092,'Ostalo',8), (330,1605,'LED trake i sijalice',8), (331,2373,'Lusteri',8), (332,2629,'Risiveri i tjuneri',8), (333,1095,'Afrika',8), (334,2641,'Oprema',8), (335,1107,'Italija',8), (336,2645,'Igre',8), (337,1377,'Za baštu',8), (338,1122,'Ramovi i oprema',8), (339,106,'Čizme',8), (340,1132,'Amerika i Australija',8), (341,116,'Lična higijena',8), (342,1653,'Lektire',8), (343,2933,'Domaće slatko',8), (344,128,'Sandale',8), (345,1417,'Kaiševi',8), (346,2441,'Servisi i tanjiri',8), (347,2701,'Nega i pribor',8), (348,142,'Zaštitne folije',8), (349,403,'Tehničke nauke',8), (350,151,'Šporeti, ploče i rerne',8), (351,2457,'Ogledala',8), (352,4257,'Čarape',8), (353,2211,'Ostalo',8), (354,676,'Slikovnice',8), (355,1189,'Udarne bušilice i čekići',8), (356,2215,'Miševi',8), (357,937,'Rezervni delovi i materijal',8), (358,1449,'Farmerke',8), (359,428,'Instrumenti sa dirkama',8), (360,2485,'Jastuci',8), (361,1729,'Korseti',8), (362,3017,'Erotski roman',8), (363,2261,'Ormari, vitrine, komode',8), (364,989,'Pantalone',8), (365,2781,'Najloni (strune)',8), (366,992,'Košulje',8), (367,481,'Jakne',8), (368,230,'Preko 750 ccm',8), (369,236,'Signalizacija',8), (370,241,'Čamci i brodovi',8), (371,1265,'Ostalo',8), (372,2801,'Delovi za rolere i skejt',8), (373,1529,'Časopisi za muškarce',8), (374,509,'Antene',8), (375,1861,'Kape, šalovi i rukavice',8), (376,2917,'Grafičke kartice',8), (377,934,'Rezervni delovi i materijal',8), (378,530,'Glodalice',8), (379,1301,'Čekići i stege',8), (380,791,'Islam',8), (381,1825,'Mrežne kartice i Bluetooth',8), (382,1060,'Nemačka',8), (383,1067,'Rumunija',8), (384,1333,'Ostalo',8), (385,1079,'Ostalo',8), (386,1857,'Kaputi i mantili',8), (387,1104,'Bugarska',8), (388,1365,'Za električne mašine',8), (389,1124,'Ostalo',8), (390,1381,'Agregati',8), (391,365,'Istorija i teorija književnosti i jezika',8), (392,1133,'Azija i Afrika',8), (393,1389,'Za građevinarstvo',8), (394,2161,'Gume',8), (395,1401,'Setovi za negu tela',8), (396,2169,'Predškolski uzrast',8), (397,1413,'Marame i ešarpe',8), (398,906,'Ostalo',8), (399,3213,'Geografija',8), (400,1761,'Kamere',8), (401,3045,'Horor',8), (402,486,'Trenerke',8), (403,490,'Džemperi',8), (404,2029,'Bojanke',8), (405,1521,'Časopisi za decu',8), (406,1385,'Mašine za sečenje',8), (407,2993,'Zastave',8), (408,2549,'Ostalo',8), (409,1027,'Sport',8), (410,1033,'Militarija',8), (411,2317,'Galanterija',8), (412,1553,'Disney stripovi',8), (413,2577,'Usisivači i pegle',8), (414,792,'Judaizam',8), (415,2333,'Oprema za ptice',8), (416,31,'Kablovi',8), (417,546,'Ručne testere',8), (418,1061,'Ostale bivše republike',8), (419,1069,'Skandinavija',8), (420,1849,'Oprema i delovi',8), (421,61,'Biznis i organizacija',8), (422,65,'Istorija',8), (423,2369,'Plafonjere',8), (424,1609,'Folije i nalepnice',8), (425,78,'Mikrotalasne rerne',8), (426,1108,'Turska',8), (427,107,'Papuče',8), (428,1134,'Praznične',8), (429,112,'Papuče',8), (430,114,'Kreme i mleka za telo',8), (431,1397,'Za mašine i alate',8), (432,2165,'Kursevi',8), (433,2937,'Med i proizvodi od meda',8), (434,2437,'Escajg i pribor',8), (435,1421,'Kravate i manžetne',8), (436,1677,'Portabl uređaji',8), (437,146,'Bluetooth, slušalice i handsfree',8), (438,914,'Ostalo',8), (439,147,'Kertridži i toneri',8), (440,148,'Projektori',8), (441,2461,'Kartolerija',8), (442,2979,'Edukativne knjige',8), (443,430,'Udaraljke',8), (444,1477,'Tableti',8), (445,1733,'Gaćice',8), (446,3013,'Istorijski roman',8), (447,1225,'Mašine za pranje pod pritiskom',8), (448,2761,'Udice',8), (449,1497,'Politika i svet',8), (450,993,'Pantalone',8), (451,994,'Suknje',8), (452,483,'Odela i sakoi',8), (453,1765,'Ostalo',8), (454,235,'Vetrobrani i prozori',8), (455,492,'Jakne',8), (456,1773,'Tastature za laptop',8), (457,2285,'Kreveti i dušeci',8), (458,502,'Bebi oprema',8), (459,2861,'Kišobrani',8), (460,2517,'Peškiri',8), (461,2857,'Pozamanterija i izrada nakita',8), (462,3077,'Klasici',8), (463,8,'Promo materijali',8), (464,526,'Kriminalistika',8), (465,24,'Štampači',8), (466,1817,'Kuleri i hladnjaci za laptop',8), (467,1071,'Ostalo',8), (468,1585,'Za teretna i kombi vozila',8), (469,2865,'Plakati i posteri',8), (470,1082,'Srbija',8), (471,1109,'Švedska',8), (472,1373,'Ašovi i lopate',8), (473,1136,'Flora i fauna',8), (474,121,'Za mršavljenje',8), (475,899,'Stolarski materijal',8), (476,141,'Punjači',8), (477,2975,'Enciklopedije za decu',8), (478,936,'HTZ oprema',8), (479,1965,'Veš majice',8), (480,1513,'Kompjuterski časopisi',8), (481,493,'Kaputi i mantili',8), (482,1977,'Bade mantili',8), (483,3001,'Bodi',8), (484,1028,'Auto',8), (485,1797,'Procesori',8), (486,268,'Držači',8), (488,2581,'Za negu tela i zdravlje',8), (489,2837,'Evropski i ostali stripovi',8), (490,23,'Skeneri',8), (491,32,'Kuleri',8), (492,2337,'Ostala oprema',8), (493,2849,'Stari uređaji',8), (494,1317,'Ostalo',8), (495,1080,'SCG 1992-2006',8), (496,2633,'Oprema i delovi',8), (497,75,'Mašine za veš i sudove',8), (498,1100,'Ostatak Evrope',8), (499,2381,'LED trake',8), (500,2405,'Police',8), (501,616,'Ostalo',8), (502,109,'Sandale',8), (503,2413,'Čaše',8), (504,1135,'Komične',8), (505,113,'Patike',8), (506,115,'Kozmetika za brijanje',8), (507,2941,'Sokovi',8), (508,2689,'Nameštaj',8), (509,3201,'Lingvistika',8), (510,907,'Ostalo',8), (511,916,'Ostalo',8), (512,664,'Ostalo',8), (513,155,'Mini linije',8), (514,2971,'Ostalo',8), (515,2465,'Baštenski dekor',8), (516,4261,'Farmerke / Pantalone',8), (517,2219,'Rezervni delovi i oprema za tablet',8), (518,429,'Oprema i delovi',8), (519,2489,'Stolnjaci',8), (520,1229,'Konfekcijske i šivaće mašine',8), (521,2765,'Varalice',8), (522,478,'Duksevi',8), (523,2529,'Delovi i oprema',8), (524,487,'Bunde',8), (525,1000,'Salvete',8), (526,3057,'Lirska poezija',8), (527,1525,'Automobilizam',8), (528,1517,'National Geographic',8), (529,1537,'Ostalo',8), (530,270,'Memorijske kartice',8), (531,527,'Pedagogija',8), (532,27,'Zvučnici',8), (533,1054,'Austrija',8), (534,59,'Rečnici i leksikoni',8), (535,1101,'Ostatak Sveta',8), (536,80,'Kućišta',8), (537,1369,'Kosačice, trimeri i lančane testere',8), (538,865,'Prirodni preparati',8), (539,1137,'Ostalo',8), (540,2957,'Za venčanje',8), (541,2963,'Lazy bag',8), (542,3005,'Ljubavni roman',8), (543,497,'Odela i sakoi',8), (544,242,'Društvene igre',8), (545,1781,'Memorija za laptop',8), (546,1020,'Ulaznice i pozivnice',8), (547,905,'Kontroleri',8), (548,3085,'Memoari',8), (549,2585,'Fiksni telefoni i fax',8), (550,2841,'Manga stripovi',8), (551,26,'Web kamere',8), (552,33,'Matične ploče',8), (553,2609,'Oprema i delovi',8), (554,1084,'Dopisnice i koverte',8), (555,64,'Informacione tehnologije',8), (556,67,'Političke nauke',8), (557,73,'Bojleri',8), (558,2377,'Spotovi',8), (559,1110,'Ostalo',8), (560,360,'Enciklopedije i atlasi',8), (561,617,'Ostalo',8), (562,2153,'Militarija',8), (563,876,'Baletanke',8), (564,2417,'Šolje',8), (565,2945,'Vino',8), (566,2697,'Kolica',8), (567,144,'Ukrasi i olovke',8), (568,2469,'Ostalo',8), (569,4269,'Helanke',8), (570,2991,'Ortopedska pomagala',8), (571,2493,'Krpe i kecelje',8), (572,2769,'Primame i mamci',8), (573,479,'Džemperi',8), (574,2277,'Čiviluci, cipelarnici i vešalice',8), (575,1015,'Duvanijada',8), (576,1785,'Optički uređaji',8), (577,2297,'Grejna tela',8), (578,2809,'Zaštitna oprema i steznici',8), (579,1681,'PC gaming oprema',8), (580,1717,'USB flash memorije',8), (581,3589,'Popularna psihologija',8), (582,1055,'Flora i fauna',8), (583,34,'Memorije',8), (584,68,'Poljoprivreda',8), (585,357,'Antikvarne knjige',8), (586,398,'Pravo',8), (587,405,'Naučna fantastika',8), (588,489,'Duksevi',8), (589,1021,'Istorijski dokumenti',8), (590,1789,'Matične ploče',8), (591,69,'Psihologija',8), (592,1793,'Hard diskovi',8), (593,1557,'Revije',8), (594,35,'Napajanja',8), (595,2601,'Oprema i delovi',8), (596,811,'Rezervni delovi',8), (597,1081,'Sport',8), (598,326,'Domeni i hosting',8), (599,2389,'Spoljna rasveta',8), (600,3189,'Sociologija',8), (601,2425,'Modle i kalupi',8), (602,2693,'Auto sedišta',8), (603,2949,'Rakija i žestoka pića',8), (604,2497,'Otirači i prostirke',8), (605,4293,'Kaput',8), (606,3021,'Politički roman',8), (607,1237,'Ostalo',8), (608,2773,'Plovci i olova',8), (609,2265,'Kuhinjski elementi',8), (610,484,'Pantalone',8), (611,494,'Kupaći',8), (612,2813,'Sportski rekviziti',8), (613,36,'Procesori',8), (614,1083,'Umetnost',8), (615,60,'Alternativna učenja',8), (616,3205,'Teologija',8), (617,1437,'Bermude',8), (618,3049,'Pripovetke',8), (619,1957,'Šortsevi',8), (620,37,'TV, FM  i SAT Kartice',8), (621,1085,'UPU, UIT, ptt i komunikacije',8), (622,2393,'Profesionalna rasveta',8), (623,2433,'Sudopere',8), (624,2705,'Rekreacija',8), (625,1441,'Prsluci',8), (626,1445,'Farmerke',8), (627,425,'Žurnalistika i novinarstvo',8), (628,2501,'Tepisi i staze',8), (629,4301,'Kupaći',8), (630,3025,'Psihološki roman',8), (631,2777,'Prateći pribor i oprema',8), (632,2289,'Sezonski predmeti',8), (633,1022,'Tito',8), (634,3073,'Putopisi',8), (635,38,'Zvučne kartice',8), (636,1086,'Ostalo',8), (637,361,'Umetnost',8), (638,3209,'Ostalo',8), (639,1433,'Čarape',8), (640,1961,'Kardigani',8), (641,2565,'Roštilji',8), (642,2821,'Zimski sportovi',8), (643,2845,'Rezervni delovi',8), (644,2397,'Reflektori',8), (645,872,'Igračke',8), (646,1139,'Baterije i punjači',8), (647,1409,'Prsluci',8), (648,400,'Sport',8), (649,1181,'Helanke',8), (650,3009,'Romantična komedija (Čik lit)',8), (651,4309,'Pidžama',8), (652,2281,'Oprema',8), (653,1014,'Breweriana',8), (654,2869,'Pribor za kancelariju',8), (655,1969,'Kombinezoni',8), (656,3081,'Satirični roman',8), (657,1053,'Ostalo',8), (658,367,'Kuvari i ishrana',8), (659,4313,'Prsluk',8), (660,500,'Trenerke',8), (661,257,'Planinarenje i kampovanje',8), (662,1029,'Ostalo',8), (663,1573,'Ostalo',8), (664,2401,'Dodatna oprema',8), (665,875,'Ostalo',8), (666,624,'Ostalo',8), (667,625,'Ostalo',8), (668,629,'Ostalo',8), (669,630,'Ostalo',8), (670,894,'Ostalo',8), (671,896,'Ostalo',8), (672,2953,'Ostalo',8), (673,404,'Vojna literatura',8), (674,1949,'Bermude',8), (675,2509,'Ostalo',8), (676,982,'Ostalo',8), (677,986,'Ostalo',8), (678,4317,'Rolka',8), (679,999,'Ostalo',8), (680,3053,'Tinejdž roman',8), (681,1945,'Šortsevi',8), (682,4321,'Sako',8), (683,501,'Venčanice',8), (684,375,'Navigacija',8), (685,1425,'Odeća za trudnice',8), (686,414,'Školska oprema',8), (687,4325,'Skafander',8), (688,2805,'Tenis, stoni tenis i badminton',8), (689,3041,'Triler / Misterija',8), (690,3089,'Ostalo',8), (691,622,'Ostalo',8), (692,663,'Ostalo',8), (693,668,'Ostalo',8), (694,2785,'Lopte',8), (695,2829,'Šah',8), (696,2195,'Psi i mačke',8), (697,1429,'Čarape',8), (698,4341,'Tunika',8), (699,4345,'Zeke',8), (700,2825,'Ostali sportovi',8), (701,4349,'Ostala odeća',8), (702,476,'Šorts',8), (703,1144,'Košulja',8), (704,475,'Haljina',8), (705,2557,'Ostalo',8), (706,470,'Džemper',8), (707,468,'Donji veš',8), (708,469,'Duks',8), (709,474,'Majica',8), (710,473,'Trenerka',8), (711,471,'Jakna',8), (712,503,'Suknja',8), (713,2537,'Ostalo',8), (714,2293,'Ostalo',8), (487,14,'Pokloni',28)) 
	    	redovi(export_grupe_id, grupa_id, naziv, export_id) 
	        WHERE export_grupe_id NOT IN (SELECT export_grupe_id FROM export_grupe)");
		DB::statement("SELECT setval('export_grupe_export_grupe_id_seq', (SELECT MAX(export_grupe_id) + 1 FROM export_grupe), FALSE)");

	    DB::statement("INSERT INTO validator (validator_id,kod,aktivan) SELECT * FROM (VALUES 
	    	(8,'captcha',1) ) redovi(validator_id,kod,aktivan) WHERE validator_id NOT IN (SELECT validator_id FROM validator)");
	    DB::statement("INSERT INTO validator_jezik (validator_id,jezik_id,poruka) SELECT * FROM (VALUES 
	    	(8,1,'Dokažite da niste robot!')) redovi(validator_id,jezik_id,poruka) WHERE poruka NOT IN (SELECT poruka FROM validator_jezik)");

		DB::statement("DROP TABLE IF EXISTS footer_data");
		DB::statement("DROP TABLE IF EXISTS footer_data_type");
		DB::statement("DROP TABLE IF EXISTS footer_data_stranice");

	    // footer section type
		DB::statement("SELECT addtable('public','futer_sekcija_tip','futer_sekcija_tip_id bigserial NOT NULL,naziv character varying(255),aktivan smallint DEFAULT 1, CONSTRAINT futer_sekcija_tip_pkey PRIMARY KEY (futer_sekcija_tip_id),CONSTRAINT futer_sekcija_tip_unique UNIQUE (naziv)')");
		// footer section
	    DB::statement("SELECT addtable('public','futer_sekcija','futer_sekcija_id bigserial NOT NULL,slika character varying(255),link character varying(255),rbr smallint DEFAULT 0,aktivan smallint DEFAULT 1,futer_sekcija_tip_id integer NOT NULL, CONSTRAINT futer_sekcija_pkey PRIMARY KEY (futer_sekcija_id), CONSTRAINT futer_sekcija_tip_id_fkey FOREIGN KEY (futer_sekcija_tip_id) REFERENCES public.futer_sekcija_tip (futer_sekcija_tip_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");
	    DB::statement("SELECT addtable('public','futer_sekcija_jezik','futer_sekcija_id integer NOT NULL,jezik_id integer NOT NULL,naslov character varying(255),sadrzaj text, CONSTRAINT futer_sekcija_jezik_pkey PRIMARY KEY (futer_sekcija_id, jezik_id), CONSTRAINT futer_sekcija_id_fkey FOREIGN KEY (futer_sekcija_id) REFERENCES public.futer_sekcija (futer_sekcija_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE, CONSTRAINT jezik_id_fkey FOREIGN KEY (jezik_id) REFERENCES public.jezik (jezik_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");
		DB::statement("SELECT addtable('public','futer_sekcija_strana','futer_sekcija_id integer DEFAULT NULL,web_b2c_seo_id integer DEFAULT NULL, CONSTRAINT futer_sekcija_id_fkey FOREIGN KEY (futer_sekcija_id) REFERENCES public.futer_sekcija (futer_sekcija_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE, CONSTRAINT web_b2c_seo_id_fkey FOREIGN KEY (web_b2c_seo_id) REFERENCES public.web_b2c_seo (web_b2c_seo_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");

		DB::statement("DELETE FROM futer_sekcija_tip WHERE naziv = 'drustvene_mreze'");
		DB::statement("INSERT INTO futer_sekcija_tip (naziv) SELECT * FROM (VALUES 
	    	('slika'),('text'),('linkovi'),('kontakt'),('društvene mreže') ) redovi(naziv) WHERE naziv NOT IN (SELECT naziv FROM futer_sekcija_tip)");
	    DB::statement("INSERT INTO futer_sekcija (futer_sekcija_id,futer_sekcija_tip_id,rbr) SELECT * FROM (VALUES 
	    	(1,1,1),
	    	(2,3,2),
	    	(3,3,3),
	    	(4,4,4)
	    ) redovi(futer_sekcija_id,futer_sekcija_tip_id,rbr) WHERE futer_sekcija_id NOT IN (SELECT futer_sekcija_id FROM futer_sekcija)");
	    DB::statement("INSERT INTO futer_sekcija_jezik (futer_sekcija_id,jezik_id,naslov,sadrzaj) SELECT * FROM (VALUES 
	    	(1,1,'Logo','U našoj prodavnici imamo široku ponudu artikala.'),
	    	(2,1,'Brzi linkovi',NULL),
	    	(3,1,'Informacije',NULL),
	    	(4,1,'Kontaktirajte nas',NULL)
	    ) redovi(futer_sekcija_id,jezik_id,naslov,sadrzaj) WHERE NOT EXISTS (SELECT * FROM futer_sekcija_jezik)");
	    DB::statement("SELECT setval('futer_sekcija_futer_sekcija_id_seq', (SELECT MAX(futer_sekcija_id) + 1 FROM futer_sekcija), FALSE)");
	    // DB::statement("INSERT INTO futer_sekcija_strana (futer_sekcija_id,web_b2c_seo_id) SELECT * FROM (VALUES 
	    // 	(2,25),(2,27),(3,36) ) redovi(futer_sekcija_id,web_b2c_seo_id) WHERE NOT EXISTS (SELECT * FROM futer_sekcija_strana)");

		DB::statement("SELECT addtable('public','web_b2c_newslatter_description','web_b2c_newslatter_description_id integer NOT NULL, jezik_id integer, naslov character varying(255), sadrzaj text, CONSTRAINT jezik_id_fkey FOREIGN KEY (jezik_id) REFERENCES public.jezik (jezik_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");
		DB::statement("INSERT INTO web_b2c_newslatter_description (web_b2c_newslatter_description_id,jezik_id,naslov,sadrzaj) SELECT * FROM (VALUES 
	    	(1,1,'Želite li da Vas obavestimo o novim kolekcijama?','Za najnovije informacije o našim proizvodima i kolekcijama prijavite se na našu e-mail listu.')
	    ) redovi(web_b2c_newslatter_description_id,jezik_id,naslov,sadrzaj) WHERE NOT EXISTS (SELECT * FROM web_b2c_newslatter_description)");

		//stari linkovi
		DB::statement("SELECT addtable('public','stari_linkovi','sifra character varying(255), link character varying(500)')");
		DB::statement("SELECT addcol('public','stari_linkovi','id','integer DEFAULT NULL')");
		DB::statement("SELECT addcol('public','stari_linkovi','kategorija','smallint DEFAULT 0')");
		//front admin labels
		DB::statement("SELECT addtable('public','front_admin_labele','front_admin_labele_id bigserial NOT NULL, labela character varying(500), CONSTRAINT front_admin_labele_pkey PRIMARY KEY (front_admin_labele_id)')");
	    DB::statement("SELECT addtable('public','front_admin_labele_jezik','front_admin_labele_id integer NOT NULL,jezik_id integer NOT NULL,sadrzaj text, CONSTRAINT front_admin_labele_jezik_pkey PRIMARY KEY (front_admin_labele_id, jezik_id), CONSTRAINT front_admin_labele_id_fkey FOREIGN KEY (front_admin_labele_id) REFERENCES public.front_admin_labele (front_admin_labele_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE, CONSTRAINT jezik_id_fkey FOREIGN KEY (jezik_id) REFERENCES public.jezik (jezik_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");

		DB::statement("INSERT INTO front_admin_labele (front_admin_labele_id,labela) SELECT * FROM (VALUES 
	    	(1,'Najpopularniji proizvodi'),
	    	(2,'Najprodavaniji proizvodi'),
	    	(3,'Najnoviji proizvodi')
	    ) redovi(front_admin_labele_id,labela) WHERE NOT EXISTS (SELECT * FROM front_admin_labele)");
		DB::statement("SELECT setval('front_admin_labele_front_admin_labele_id_seq', (SELECT MAX(front_admin_labele_id) + 1 FROM front_admin_labele), FALSE)");

		//kurs
		DB::statement("SELECT addcol('public','kursna_lista','web','numeric(20,2) DEFAULT 0')");

		//roba_partner_kategorija
		DB::statement("SELECT addtable('public','roba_partner_kategorija','roba_id integer, id_kategorije integer, cena numeric(20,2) DEFAULT 0')");

		// web import kolone
		DB::statement("SELECT addtable('public','web_import_kolone','kolona_sifra character varying(255),naziv character varying(255),width integer DEFAULT 35,css_class text, CONSTRAINT web_import_kolone_pkey PRIMARY KEY (kolona_sifra)')");
		DB::statement("INSERT INTO web_import_kolone (kolona_sifra,naziv,width,css_class) SELECT * FROM (VALUES 
	    	('naziv','naziv',330,NULL)
	    ) redovi(kolona_sifra,naziv,width,css_class) WHERE kolona_sifra NOT IN (SELECT kolona_sifra FROM web_import_kolone)");

		// admin artikli kolone
		DB::statement("SELECT addtable('public','admin_artikli_kolone','kolona_sifra character varying(255),naziv character varying(255),width integer DEFAULT 35,css_class text, CONSTRAINT admin_artikli_kolone_pkey PRIMARY KEY (kolona_sifra)')");
		DB::statement("INSERT INTO admin_artikli_kolone (kolona_sifra,naziv,width,css_class) SELECT * FROM (VALUES 
	    	('naziv','naziv',330,NULL)
	    ) redovi(kolona_sifra,naziv,width,css_class) WHERE kolona_sifra NOT IN (SELECT kolona_sifra FROM admin_artikli_kolone)");

/****************************************************************************************************************************/
		// // DB::statement("UPDATE dobavljac_cenovnik SET flag_opis_postoji = 0 WHERE opis = ''");
		// // DB::statement("UPDATE dobavljac_cenovnik SET flag_opis_postoji = 1 WHERE opis != ''");

		// // DB::statement("ALTER TABLE baneri ALTER COLUMN web_b2c_seo_id TYPE integer USING CAST(web_b2c_seo_id AS integer)");
		// // DB::statement("ALTER TABLE baneri_b2b ALTER COLUMN web_b2b_seo_id TYPE integer USING CAST(web_b2b_seo_id AS integer)");

		DB::statement("DELETE FROM options WHERE options_id IN (1340,3014,3015,3016,3017,3027,3031)");

		// DB::statement("UPDATE web_b2c_seo SET naziv_stranice = 'blog', title = 'Blog', seo_title = 'Blog' WHERE naziv_stranice = 'vesti'");
		// DB::statement("UPDATE web_b2c_seo SET naziv_stranice = 'prijava', title = 'Prijava' WHERE naziv_stranice = 'login'");
		// DB::statement("UPDATE web_b2c_seo SET seo_title = 'Akcija' WHERE naziv_stranice = 'akcija'");
		// DB::statement("UPDATE web_b2c_seo SET seo_title = 'Lista svih artikala' WHERE naziv_stranice = 'pocetna'");
		// DB::statement("UPDATE web_b2c_seo SET disable = 0 WHERE naziv_stranice = 'sve-o-kupovini'");

		// DB::statement("UPDATE web_nacin_isporuke SET naziv = 'Preuzimam lično' WHERE naziv = 'Preuzimam licno'");
		// DB::statement("UPDATE web_nacin_placanja SET naziv = 'Uplatnicom preko žiro računa' WHERE naziv = 'Uplatnicom preko ziro računa'");
		// DB::statement("UPDATE web_nacin_placanja SET naziv = 'Pouzećem u gotovini pri prijemu robe' WHERE naziv = 'Pouzecem u gotovini pri prijemu robe'");

		// DB::statement("SELECT addtable('public','cena_isporuke','cena_id integer, cena_do numeric(20,2) DEFAULT 0, cena numeric(20,2) DEFAULT 0')");
		// DB::statement("INSERT INTO cena_isporuke (cena_id,cena_do,cena) SELECT * FROM (VALUES 
	 //    	(1,0,0)
	 //    ) redovi(cena_id,cena_do,cena) WHERE cena_id NOT IN (SELECT cena_id FROM cena_isporuke)");

		// $prodavnica_stil = DB::select("SELECT prodavnica_stil_id FROM prodavnica_stil WHERE izabrana = 1");
		// if(isset($prodavnica_stil[0])){
		// 	if(in_array($prodavnica_stil[0]->prodavnica_stil_id, array(1,2,4,5,9))){
		// 		DB::statement("UPDATE prodavnica_stil SET izabrana = 0");
		// 		DB::statement("UPDATE prodavnica_stil SET izabrana = 1 WHERE prodavnica_stil_id = 11");
		// 	}else if(in_array($prodavnica_stil[0]->prodavnica_stil_id, array(3,6))){
		// 		DB::statement("UPDATE prodavnica_stil SET izabrana = 0");
		// 		DB::statement("UPDATE prodavnica_stil SET izabrana = 1 WHERE prodavnica_stil_id = 12");
		// 	}else if(in_array($prodavnica_stil[0]->prodavnica_stil_id, array(7,8))){
		// 		DB::statement("UPDATE prodavnica_stil SET izabrana = 0");
		// 		DB::statement("UPDATE prodavnica_stil SET izabrana = 1 WHERE prodavnica_stil_id = 10");
		// 	}
		// }

		// DB::statement("SELECT dropcol('public','partner','id_kategorije')");
		// DB::statement("SELECT addcol('public','partner','id_kategorije','integer DEFAULT NULL')");

		// DB::statement("UPDATE web_options SET naziv = 'Korisnik mora da uradi potvrdu registracije' WHERE web_options_id = 100");
		// DB::statement("UPDATE web_options SET naziv = 'Broj proizvoda na akciji' WHERE web_options_id = 101");
		
		// DB::statement("UPDATE options SET naziv = 'Chat' WHERE options_id = 3020");

		// DB::statement("UPDATE posta_slanje SET difolt = 1 WHERE posta_slanje_id = 3");


/*****************************************************************************************************************************/


	    $this->info('Database has been updated successfully.');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			// array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			// array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
