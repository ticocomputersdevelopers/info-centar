<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ClearInitAopDataCommand extends Command {
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'clear:init-data';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Clear init data.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		DB::statement("TRUNCATE baneri");
		DB::table('web_slika')->where('web_slika_id','<',1444)->delete();

		DB::table('osobina_roba')->where('roba_id','<',725)->delete();
		DB::table('osobina_vrednost')->where('osobina_vrednost_id','<',15)->delete();
		DB::table('osobina_naziv')->where('osobina_naziv_id','<',4)->delete();

		DB::table('web_roba_karakteristike')->where('roba_id','<',725)->delete();
		DB::table('grupa_pr_vrednost')->where('grupa_pr_vrednost_id','<',60)->delete();
		DB::table('grupa_pr_naziv')->where('grupa_pr_naziv_id','<',15)->delete();

		DB::table('lager')->where('roba_id','<',725)->delete();
		DB::table('roba')->where('roba_id','<',725)->delete();
		DB::table('grupa_pr')->where('grupa_pr_id','>',0)->where('grupa_pr_id','<',5)->delete();
		DB::table('proizvodjac')->where('proizvodjac_id','>',0)->where('proizvodjac_id','<',9)->delete();
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
		// 	array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
		// 	array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}