<?php
require __DIR__.'/../../vendor/autoload.php';

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

// use PHPExcel; 
// use PHPExcel_IOFactory;

class InsertOldLinksCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'import:old-links';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$xml = simplexml_load_file(__DIR__.'/../../old_links.xml');

		$data_body = "";
		foreach($xml->xpath('database/table') as $item) {
			$full_link = trim(strval($item->column[6]));
			if(isset($full_link) && $full_link != ''){	
				$id = trim(strval($item->column[2]));
				$full_link_arr = explode('/', $full_link);
				unset($full_link_arr[0]);
				unset($full_link_arr[1]);
				unset($full_link_arr[2]);
				$link = trim(implode('/',$full_link_arr));

				$data_body .= "('".$link."','".$id."'),";
			}	
		}

		if($data_body != ''){
		    DB::statement("INSERT INTO stari_linkovi (link,id) VALUES ".substr($data_body,0,-1));
		}

		$this->info('Links has been inserted successfully.');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
		// 	array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
		// 	array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
