<?php
ini_set('memory_limit', '-1');

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;



class TransferArticlesDetailsCommand extends Command {
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'transfer:articles:details';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Resize images.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$transfer_pgsql = DB::connection('transfer_pgsql');
        $sirina_slike = DB::table('options')->where('options_id',1331)->pluck('int_data');

		$products_file = 'linked_articles.xlsx';
		PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
        $excelObj = $excelReader->load($products_file);
        $worksheet = $excelObj->getSheet(0);
        $lastRow = $worksheet->getHighestRow();

       
        for ($row = 1; $row <= $lastRow; $row++) {

            $sifra_logik = trim($worksheet->getCell('A'.$row)->getValue());
            $sifra_old = trim($worksheet->getCell('B'.$row)->getValue());

            $article_old = $transfer_pgsql->select("SELECT roba_id, naziv_web, web_opis, web_karakteristike FROM roba WHERE sifra_d = '1310".trim($sifra_old)."'");
            $article = DB::select("SELECT roba_id FROM roba WHERE id_is = '".trim($sifra_logik)."'");
            if(isset($article_old[0]) && isset($article[0])){
            	      	
				DB::table('roba')->where('roba_id',$article[0]->roba_id)->update(array('naziv_web' => $article_old[0]->naziv_web,'web_opis'=>$article_old[0]->web_opis, 'web_karakteristike'=>$article_old[0]->web_karakteristike,'sifra_is'=>trim($sifra_old)));

				if(count(DB::table('web_slika')->where('roba_id',$article[0]->roba_id)->get()) == 0){
					$slike = $transfer_pgsql->select("SELECT akcija, putanja FROM web_slika WHERE roba_id = ".$article[0]->roba_id."");		
		            foreach($slike as $slika){
		                try { 
		                    $slika_id = DB::select("SELECT nextval('web_slika_web_slika_id_seq')")[0]->nextval;
		                    $name = $slika_id.'.jpg';
		                    $destination = 'images/products/big/'.$name;
		                    $url = 'http://nijansa.rs/'.$slika->putanja;
		                    $akcija = $slika->akcija;
                    	
	                        $content = @file_get_contents($url);
	                        file_put_contents($destination,$content);

		                    $insert_data = array(
		                        'web_slika_id'=>intval($slika_id),
		                        'roba_id'=> $article[0]->roba_id,
		                        'akcija'=>$akcija, 
		                        'flag_prikazi'=>1,
		                        'putanja'=>'images/products/big/'.$name
		                        );

		                    DB::table('web_slika')->insert($insert_data);
		 
		                }
		                catch (Exception $e) {
		                }
		            }

				}

            }

        }

		$this->info('Finish.');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			// array('transfer_domain', InputArgument::OPTIONAL, 'Image path on root.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
		// 	array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}