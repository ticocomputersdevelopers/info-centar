<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use IsLogik\Support as ISLogikSupport;

class TransferDBDataCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'transfer:db';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	public static function decrypt($string){
		$key = 'opIaod3392iiu';
	    $sizes = array(16,24,32);
	    foreach($sizes as $s){
	        while(strlen($key) < $s) $key = $key."\0";
	        if(strlen($key) == $s) break;
	    }
		$string = base64_decode($string);
		return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_128,$key,$string,MCRYPT_MODE_CBC));
	}

	public static function getMappedArticles(){
		$mapped = array();
		$articles = DB::table('roba')->whereNotNull('id_is')->where('id_is','!=','')->get();
		foreach($articles as $article){
			$mapped[$article->id_is] = $article->roba_id;
		}
		return $mapped;
	}
	public static function getMappedGroups(){
		$mapped = array();
		$groups = DB::table('grupa_pr')->whereNotNull('id_is')->where('id_is','!=','')->get();
		foreach($groups as $group){
			$mapped[$group->id_is] = $group->grupa_pr_id;
		}
		return $mapped;
	}
	public static function getMappedManufacturers(){
		$mapped = array();
		$manufacturers = DB::table('proizvodjac')->whereNotNull('id_is')->where('id_is','!=','')->get();
		foreach($manufacturers as $group){
			$mapped[$group->id_is] = $group->proizvodjac_id;
		}
		return $mapped;
	}

	/**
	 * Execute the console command. 
	 *
	 * @return mixed
	 */
	public function fire()
		{
		
		$transfer_pgsql = DB::connection('transfer_pgsql');

		// grupe
		$groups = $transfer_pgsql->select("SELECT * FROM grupa_pr WHERE grupa_pr_id >0 ");

		$group_rows ="";
		foreach ($groups as $group) {

		$group_rows .= "(nextval('grupa_pr_grupa_pr_id_seq'),'".$group->grupa."','".$group->opis."',(".(is_null($group->parrent_grupa_pr_id) ? 'NULL' : $group->parrent_grupa_pr_id ).")::integer,nextval('grupa_pr_grupa_pr_id_seq'),".$group->web_b2b_prikazi.",".$group->web_b2c_prikazi.",".$group->prikaz.",0,NULL,NULL,NULL,NULL,0,0,'".$group->putanja_slika."',NULL,(NULL)::integer,NULL,'".$group->grupa_pr_id."',NULL),";
		}

		$columns = array_map('current',$transfer_pgsql->select("SELECT column_name FROM information_schema.columns where table_name='grupa_pr'"));
		$table_temp = "(VALUES ".substr($group_rows,0,-1).") grupa_pr_temp(".implode(',',$columns).")";		

		DB::statement("INSERT INTO grupa_pr (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM grupa_pr t WHERE t.grupa=grupa_pr_temp.grupa))");
		DB::statement("update grupa_pr gp set parrent_grupa_pr_id = (select grupa_pr_id from grupa_pr where (id_is)::integer = gp.parrent_grupa_pr_id limit 1) where parrent_grupa_pr_id > 0");
		

		// proizvodjaci
		$proizvodjaci = $transfer_pgsql->select("SELECT * FROM proizvodjac WHERE proizvodjac_id > -1");
		$proizvodjac_rows="";
		foreach ($proizvodjaci as $proizvodjac) {
		$proizvodjac_rows .= "(nextval('proizvodjac_proizvodjac_id_seq'),'".$proizvodjac->naziv."','".trim($proizvodjac->sifra)."',0,0,0,NULL,NULL,NULL,1,(NULL)::integer,(NULL)::integer,'".$proizvodjac->proizvodjac_id."'),";
		}
		$columns = array_map('current',$transfer_pgsql->select("SELECT column_name FROM information_schema.columns where table_name='proizvodjac'"));
		$table_temp = "(VALUES ".substr($proizvodjac_rows,0,-1).") proizvodjac_temp(".implode(',',$columns).")";		

		DB::statement("INSERT INTO proizvodjac (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM proizvodjac t WHERE t.naziv=proizvodjac_temp.naziv))");

	
		// partneri
		$partneri = $transfer_pgsql->select("SELECT *, (select mesto from mesto where mesto_id = p.mesto_id) AS mesto FROM partner p WHERE partner_id >0 ");
		$partner_rows="";
		foreach ($partneri as $partner) {
		$partner_rows .= "(nextval('partner_partner_id_seq'),'".$partner->sifra."','".trim($partner->naziv)."','".trim($partner->adresa)."','".trim($partner->mesto)."',0,'".trim($partner->telefon)."',NULL,'".strval($partner->pib)."','".strval($partner->broj_maticni)."',NULL,NULL,NULL,NULL,0,0,0,'".trim($partner->naziv_puni)."',0,0,1,1,0,'".trim($partner->login)."','".trim($partner->password)."','".strval($partner->racun)."',NULL,0,NULL,NULL,NULL,'".trim($partner->mail)."',0,1,0,0,NULL,1,NULL,(NULL)::integer,NULL,NULL,(NULL)::integer),";
		}
		$columns = array_map('current',$transfer_pgsql->select("SELECT column_name FROM information_schema.columns where table_name='partner'"));
		$table_temp = "(VALUES ".substr($partner_rows,0,-1).") partner_temp(".implode(',',$columns).")";		

		DB::statement("INSERT INTO partner (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM partner t WHERE t.naziv=partner_temp.naziv))");

		// web_kupac
		$kupci = $transfer_pgsql->select("SELECT *, (select mesto from mesto where mesto_id = wk.mesto_id) AS mesto FROM web_kupac wk WHERE web_kupac_id >0 ");
		$kupac_rows="";
		foreach ($kupci as $kupac) {
		$kupac_rows .= "(nextval('web_kupac_web_kupac_id_seq'),NULL,'".$kupac->lozinka."','".$kupac->ime."','".$kupac->adresa."','".$kupac->telefon."','".$kupac->email."',(".($kupac->flag_potvrda == '' ? '0' : $kupac->flag_potvrda ).")::integer,(".($kupac->partner_id == '' ? 'NULL' : $kupac->partner_id ).")::integer,'".$kupac->prezime."',0,(".($kupac->flag_vrsta_kupca == '' ? 'NULL' : $kupac->flag_vrsta_kupca ).")::integer,'".$kupac->telefon_mobilni."','".$kupac->naziv."',NULL,'".$kupac->mesto."','".$kupac->pib."',(".($kupac->status_registracije == '' ? 'NULL' : $kupac->status_registracije ).")::integer,NULL,0,NULL,0,NULL),";
		}
		$columns = array_map('current',$transfer_pgsql->select("SELECT column_name FROM information_schema.columns where table_name='web_kupac'"));
		$table_temp = "(VALUES ".substr($kupac_rows,0,-1).") web_kupac_temp(".implode(',',$columns).")";		

		DB::statement("INSERT INTO web_kupac (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM web_kupac t WHERE t.email=web_kupac_temp.email))");


		// roba
		$articles = $transfer_pgsql->select("SELECT * FROM roba where roba_id > -1");
		$getMappedGroups = self::getMappedGroups();
		$getMappedManufacturers = self::getMappedManufacturers();
		
		$article_rows="";
		foreach ($articles as $article) {
			$flag_prikazi_u_cenovniku = $article->flag_aktivan == 1 ? ($article->web_cena > 0 ? $article->flag_prikazi_u_cenovniku : 0) : 0;
			$article_rows .= "(nextval('roba_roba_id_seq'),NULL,'".pg_escape_string(ISLogikSupport::convert($article->naziv))."',NULL,NULL,NULL,".($article->grupa_pr_id > 0 ? $getMappedGroups[$article->grupa_pr_id] : '-1').",0,1,".($article->proizvodjac_id > -1 ? strval($getMappedManufacturers[$article->proizvodjac_id]) : -1).",-1,nextval('roba_roba_id_seq'),NULL,NULL,'".substr(pg_escape_string(ISLogikSupport::convert($article->naziv)),0,20)."',0,-1,0,0,0,0,9,0,0,0,0,1,".strval($flag_prikazi_u_cenovniku).",0,NULL,".$article->flag_aktivan.",".strval($article->racunska_cena_nc).",0,".strval($article->racunska_cena_nc).",0,NULL,".strval($article->mpcena).",false,0,(NULL)::integer,'".pg_escape_string(ISLogikSupport::convert($article->naziv))."',1,NULL,NULL,(NULL)::integer,(NULL)::integer,0,0,0,-1,-1,".strval($article->web_cena).",1,0,'".pg_escape_string(ISLogikSupport::convert($article->web_opis))."',NULL,NULL,0,0,0,NULL,NULL,NULL,NULL,1,0,NULL,0,0,1,1,-1,NULL,NULL,NULL,NULL,0,0.00,0.00,0.00,0,NULL,(NULL)::date,(NULL)::date,(NULL)::integer,NULL,NULL,'".strval($article->roba_id)."'),";
		}

		$columns = array_map('current',$transfer_pgsql->select("SELECT column_name FROM information_schema.columns where table_name='roba'"));
		$table_temp = "(VALUES ".substr($article_rows,0,-1).") roba_temp(".implode(',',$columns).")";		

		DB::statement("INSERT INTO roba (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM roba t WHERE t.naziv=roba_temp.naziv))");


		//slike
	    $getMappedArticles = self::getMappedArticles();
		$images = $transfer_pgsql->select("SELECT * FROM web_slika");
		$web_slika_id= DB::select("SELECT MAX(web_slika_id) + 1 AS next_id FROM web_slika")[0]->next_id;
		if(is_null($web_slika_id)){
			$web_slika_id = 1;
		}
		if($images){
			foreach($images as $image){

			    if(isset($getMappedArticles[$image->roba_id])){
			    	$extension = 'jpg';
			    	$slash_parts = explode('/',$image->putanja);
			    	$old_name = $slash_parts[count($slash_parts)-1];
			    	$old_name_arr = explode('.',$old_name);
			    	$extension = $old_name_arr[count($old_name_arr)-1];

			    	if(in_array($extension,array('jpg','png','jpeg'))){
						try { 
						    $name = $web_slika_id.'.'.$extension;

						    $source = '/var/www/html/redruster/'.$image->putanja;
						    $destination = '/var/www/html/wbp/images/products/big/'.$name;

						    File::copy($source,$destination);

							DB::statement("INSERT INTO web_slika (web_slika_id,roba_id,akcija,putanja) VALUES (".$web_slika_id.",".$getMappedArticles[$image->roba_id].",".$image->akcija.",'images/products/big/".$name."')");
							$web_slika_id++;
						}
						catch (Exception $e) {
						}
					}
				}
			}

			// DB::statement("SELECT setval('web_slika_web_slika_id_seq', (SELECT MAX(web_slika_id)+1 FROM web_slika), FALSE)");
		}

die('updated');
		// podrzan import
		$importi = $transfer_pgsql->select("SELECT * FROM podrzan_import WHERE podrzan_import_id >0 ");
		$import_rows="";
		foreach ($importi as $import) {
		$import_rows .= "(('".$import->podrzan_import_id."')::integer,'".$import->naziv."','".$import->vrednost_kolone."',(".($import->dozvoljen == '' ? 'NULL' : $import->dozvoljen ).")::integer,(".($import->file_upload == '' ? 'NULL' : $import->file_upload ).")::integer,'".$import->file_type."'),";
		}
		$columns = array_map('current',$transfer_pgsql->select("SELECT column_name FROM information_schema.columns where table_name='podrzan_import'"));
		$table_temp = "(VALUES ".substr($import_rows,0,-1).") podrzan_import_temp(".implode(',',$columns).")";		

		DB::statement("INSERT INTO podrzan_import (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM podrzan_import t WHERE t.naziv=podrzan_import_temp.naziv))");

		// dob_cen_kolone
		$kolone = $transfer_pgsql->select("SELECT * FROM dobavljac_cenovnik_kolone WHERE dobavljac_cenovnik_kolone_id >0 ");
		$kolone_rows="";
		foreach ($kolone as $kolona) {
		$kolone_rows .= "(nextval('dobavljac_cenovnik_kolone_dobavljac_cenovnik_kolone_id_seq'),(".($kolona->partner_id == '' ? 'NULL' : $kolona->partner_id ).")::integer,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,(".($kolona->sifra_attribute == '' ? 'NULL' : $kolona->sifra_attribute ).")::integer,NULL,(".($kolona->valuta_id == '' ? 'NULL' : $kolona->valuta_id ).")::integer,(".($kolona->naziv_proizvodjac == '' ? 'NULL' : $kolona->naziv_proizvodjac ).")::integer,'".$kolona->naziv_skripte."','".$kolona->naziv_skripte_slika."','".$kolona->import_naziv_web."','".$kolona->service_username."','".$kolona->service_password."','".$kolona->auto_link."',(".($kolona->auto_import == '' ? 'NULL' : $kolona->auto_import ).")::integer,(".($kolona->auto_import_short == '' ? 'NULL' : $kolona->auto_import_short ).")::integer),";
		}
		$columns = array_map('current',$transfer_pgsql->select("SELECT column_name FROM information_schema.columns where table_name='dobavljac_cenovnik_kolone'"));
		$table_temp = "(VALUES ".substr($kolone_rows,0,-1).") dobavljac_cenovnik_kolone_temp(".implode(',',$columns).")";		

		DB::statement("INSERT INTO dobavljac_cenovnik_kolone (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM dobavljac_cenovnik_kolone t WHERE t.import_naziv_web=dobavljac_cenovnik_kolone_temp.import_naziv_web))");

		
		// web b2c seo
		$seos = $transfer_pgsql->select("SELECT * FROM web_b2c_seo WHERE web_b2c_seo_id >0 ");
		$seo_rows="";
		foreach ($seos as $seo) {
		$seo_rows .= "(nextval('web_b2c_seo_web_b2c_seo_id_seq'),'".$seo->naziv_stranice."','".$seo->title."','".$seo->description."',
		'".$seo->keywords."',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,(".($seo->flag_page == '' ? 'NULL' : $seo->flag_page ).")::integer,(".($seo->rb_strane == '' ? '0' : $seo->rb_strane ).")::integer,(".($seo->menu_top == '' ? 'NULL' : $seo->menu_top ).")::integer,(".($seo->header_menu == '' ? 'NULL' : $seo->header_menu ).")::integer,(".($seo->footer == '' ? 'NULL' : $seo->footer ).")::integer,(".($seo->disable == '' ? 'NULL' : $seo->disable ).")::integer,-1,(".($seo->header_menu == '' ? 'NULL' : $seo->header_menu ).")::integer,(".($seo->footer == '' ? 'NULL' : $seo->footer ).")::integer,(".($seo->b2b_header == '' ? 'NULL' : $seo->b2b_header ).")::integer,(".($seo->b2b_footer == '' ? 'NULL' : $seo->b2b_footer ).")::integer,(NULL)::integer,(NULL)::integer),";


		}
		$columns = array_map('current',$transfer_pgsql->select("SELECT column_name FROM information_schema.columns where table_name='web_b2c_seo'"));
		$table_temp = "(VALUES ".substr($seo_rows,0,-1).") web_b2c_seo_temp(".implode(',',$columns).")";		

		DB::statement("INSERT INTO web_b2c_seo (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM web_b2c_seo t WHERE t.naziv_stranice=web_b2c_seo_temp.naziv_stranice))");

		

	   // $this->info('Database has been updated successfully.');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			// array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			// array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
