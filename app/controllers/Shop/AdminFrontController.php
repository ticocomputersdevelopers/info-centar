<?php


class AdminFrontController extends Controller {

    function front_admin_save()
    {
        $change_data = Input::get('change_data');
        $lang_id = Language::lang_id();
        
        foreach($change_data as $data){
            if($data['action'] == 'sale'){
                AdminFront::saveSale($data['content']);
            }elseif($data['action'] == 'blogs'){
                AdminFront::saveBlogs($data['content']);
            }elseif($data['action'] == 'home_all_articles'){
                AdminFront::saveAllArticles($data['content']);
            }elseif($data['action'] == 'type'){
                AdminFront::saveType($data['content'],$data['id']);
            }elseif($data['action'] == 'slide_title'){
                AdminFront::saveSlideTitle($data['content'],$data['id'],$lang_id);
            }elseif($data['action'] == 'slide_pretitle'){
                AdminFront::saveSlidePreTitle($data['content'],$data['id'],$lang_id);
            }elseif($data['action'] == 'slide_content'){
                AdminFront::saveSlideContent($data['content'],$data['id'],$lang_id);
            }elseif($data['action'] == 'slide_button'){
                AdminFront::saveSlideButton($data['content'],$data['id'],$lang_id);
            }elseif($data['action'] == 'footer_section_label'){
                AdminFront::saveFooterSectionLabel($data['content'],$data['id'],$lang_id);
            }elseif($data['action'] == 'footer_section_content'){
                AdminFront::saveFooterSectionContent($data['content'],$data['id'],$lang_id);
            }elseif($data['action'] == 'newslatter_label'){
                AdminFront::saveNewslatterLabel($data['content'],$lang_id);
            }elseif($data['action'] == 'newslatter_content'){
                AdminFront::saveNewslatterContent($data['content'],$lang_id);
            }elseif($data['action'] == 'front_admin_label'){
                AdminFront::saveFrontAdminLabel($data['content'],$data['id']);
            }elseif($data['action'] == 'front_admin_content'){
                AdminFront::saveFrontAdminContent($data['content'],$data['id'],$lang_id);
            }

        }
    }

}