﻿﻿<?php
class OrderController extends BaseController{
    public function order_create(){
        $lang = Language::multi() ? Request::segment(1) : null;

        $data=Input::get();

        $stavka=DB::select("SELECT COUNT(*) FROM web_b2c_korpa_stavka WHERE web_b2c_korpa_id = ".Cart::korpa_id()."")[0]->count;
        if(intval($stavka)==0){
        return Redirect::to(Options::base_url().'korpa');
        }
     
        if(!Session::has('b2c_kupac')){
            $validator_arr = array(
                'flag_vrsta_kupca' => 'required|in:0,1',
                'email' => 'required|email|unique:web_kupac,email,NULL,id,status_registracije,1',
                'telefon' => 'required|regex:'.Support::regex().'|between:3,15',
                'mesto' => 'required|regex:'.Support::regex().'|between:2,50',
                'adresa' => 'required|regex:'.Support::regex().'|between:3,50'
                );

            $translate_mess =  Language::validator_messages();
            
            if($data['flag_vrsta_kupca'] == 0){
                unset($data['naziv']);
                unset($data['pib']);
                $validator_arr['ime'] = 'required|regex:'.Support::regex().'|between:3,20';
                $validator_arr['prezime'] = 'required|regex:'.Support::regex().'|between:3,20';
            }
            if($data['flag_vrsta_kupca'] == 1){
                unset($data['ime']);
                unset($data['prezime']);
                $validator_arr['naziv'] = 'required|regex:'.Support::regex().'|between:3,20';
                $validator_arr['pib'] = 'required|digits_between:9,9|numeric';
                $translate_mess['digits_between'] = 'Broj cifara mora biti 9!';
                $translate_mess['numeric'] = 'Polje sme da sadrzi samo brojeve!';
            }


            $validator = Validator::make($data,$validator_arr,$translate_mess);
            if($validator->fails() || ($data['web_nacin_placanja_id'] == 3 && !Captcha::check())){
                if($data['web_nacin_placanja_id'] == 3 && !Captcha::check()){
                    $validator->getMessageBag()->add('captcha', $translate_mess['captcha']);
                }
                return Redirect::to(Options::base_url().'korpa#cart_form_scroll')->withInput()->withErrors($validator->messages());
            }
            else {
                $order_data = array(
                    'web_nacin_isporuke_id' => $data['web_nacin_isporuke_id'],
                    'web_nacin_placanja_id' => $data['web_nacin_placanja_id'],
                    'napomena' => $data['napomena'],
                    'stornirano' => $data['web_nacin_placanja_id'] == 3 ? 1 : 0
                );

                $data['kod'] = All::userCodeGenerate();
                $data['lozinka'] = '';
                $data['status_registracije'] = 0;
                $data['flag_potvrda'] = 0;

                unset($data['web_nacin_isporuke_id']);
                unset($data['web_nacin_placanja_id']);
                unset($data['napomena']);
                unset($data['captcha-string']);


                DB::table('web_kupac')->insert($data);
                $kupac_id = DB::select("SELECT currval('web_kupac_web_kupac_id_seq')")[0]->currval;
                $order_data['web_kupac_id'] = $kupac_id;
                Session::put('b2c_kupac_temp',$kupac_id);
                
                Order::cart_to_order($order_data);

                if(Options::info_sys('logik')){
                    OrderIS::createOrderLogik($order_data);
                }elseif(Options::info_sys('infograf')){
                    OrderIS::createOrderInfograf($order_data);
                }
            
                $web_b2c_narudzbina_id = DB::select("SELECT currval('web_b2c_narudzbina_web_b2c_narudzbina_id_seq')")[0]->currval;
                if($order_data['web_nacin_placanja_id'] == 3){
                    return Redirect::to(Options::base_url().'intesa/'.$web_b2c_narudzbina_id);
                }else{
                    return Redirect::to(Options::base_url().'narudzbina/'.$web_b2c_narudzbina_id);
                }
            }
        }else{
            if($data['web_nacin_placanja_id'] == 3 && !Captcha::check()){
                $translate_mess =  Language::validator_messages();
                $validator = Validator::make($data,array());
                $validator->getMessageBag()->add('captcha', $translate_mess['captcha']);
                return Redirect::to(Options::base_url().'korpa')->withInput()->withErrors($validator->messages());
            }

            $data['web_kupac_id'] = Session::get('b2c_kupac');
            $data['stornirano'] = $data['web_nacin_placanja_id'] == 3 ? 1 : 0;
            Order::cart_to_order($data);

            if(Options::info_sys('logik')){
                OrderIS::createOrderLogik($data);
            }elseif(Options::info_sys('infograf')){
                OrderIS::createOrderInfograf($data);
            }

            $web_b2c_narudzbina_id = DB::select("SELECT currval('web_b2c_narudzbina_web_b2c_narudzbina_id_seq')")[0]->currval;
            if($data['web_nacin_placanja_id'] == 3){
                return Redirect::to(Options::base_url().'intesa/'.$web_b2c_narudzbina_id);
            }else{
                return Redirect::to(Options::base_url().Url_mod::convert_url('narudzbina').'/'.$web_b2c_narudzbina_id);
            }          
        }    
    }
    public function narudzbina(){
        $offset = Language::segment_offset();
        $lang = Language::multi() ? Request::segment(1) : null;
        $web_b2c_narudzbina_id = Request::segment(2+$offset);
        $web_b2c_narudzbina = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->first();

        if(!Session::has('b2c_korpa') || is_null($web_b2c_narudzbina) || (!is_null($web_b2c_narudzbina) && $web_b2c_narudzbina->stornirano==1)){
            return Redirect::to(Options::base_url());
        }
        Session::forget('b2c_korpa');
        $subject=Language::trans('Porudžbina')." ".Order::broj_dokumenta($web_b2c_narudzbina_id);
        $kupac = DB::table('web_kupac')->where('web_kupac_id',DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->pluck('web_kupac_id'))->first();
        
        if($kupac->flag_vrsta_kupca == 0){
            $name = $kupac->ime.' '.$kupac->prezime;
        }else{
            $name = $kupac->naziv.' '.$kupac->pib;
        }
        $data=array(
            "strana"=>'order',
            "title"=> $subject,
            "description"=>"",
            "keywords"=>"",
            "web_b2c_narudzbina_id"=>$web_b2c_narudzbina_id,
            "bank_result"=> $web_b2c_narudzbina->web_nacin_placanja_id==3 ? json_decode($web_b2c_narudzbina->result_code) : null,
            "kupac"=>$kupac,
            "message" => !Session::has('b2c_kupac') ? true : false
            );
        $body = View::make('shop/themes/'.Support::theme_path().'partials/order_details',$data)->render();
        WebKupac::send_email_to_client($body,$kupac->email,$subject);
        All::send_email_to_admin($body,$kupac->email,$name,$subject);
        if(!Session::has('b2c_kupac')){
            $kupac_id = Session::get('b2c_kupac_temp');
            Session::put('b2c_kupac',$kupac_id);
            Session::forget('b2c_kupac_temp');
            Session::forget('b2c_kupac');
        }
        return View::make('shop/themes/'.Support::theme_path().'pages/order',$data);  
    }

    public function select_narudzbina_mesto(){
        $lang = Language::multi() ? Request::segment(1) : null;
        $narudzbina_opstina_id = Input::get('narudzbina_opstina_id');

        $narudzbina_opstina = DB::table('narudzbina_opstina')->where('narudzbina_opstina_id',$narudzbina_opstina_id)->first();
        if(!is_null($narudzbina_opstina)){
            $narudzbina_mesta = DB::table('narudzbina_mesto')->where('narudzbina_opstina_code',$narudzbina_opstina->code)->orderBy('naziv','asc')->get();
            if(count($narudzbina_mesta) > 0){
                $narudzbina_mesto = $narudzbina_mesta[0];
                $narudzbina_ulice = DB::table('narudzbina_ulica')->where('narudzbina_mesto_code',$narudzbina_mesto->code)->orderBy('naziv','asc')->get();
                $select_mesta = View::make('shop/ajax/select-narudzbina-mesto',array('narudzbina_mesta'=>$narudzbina_mesta))->render();
                $select_ulice = View::make('shop/ajax/select-narudzbina-ulica',array('narudzbina_ulice'=>$narudzbina_ulice))->render();

                echo $select_mesta."===DEVIDER===".$select_ulice;      
            }
        }
        echo '===DEVIDER===';
    }

    public function select_narudzbina_ulica(){
        $lang = Language::multi() ? Request::segment(1) : null;
        $narudzbina_mesto_id = Input::get('narudzbina_mesto_id');

        $narudzbina_mesto = DB::table('narudzbina_mesto')->where('narudzbina_mesto_id',$narudzbina_mesto_id)->first();

            if(!is_null($narudzbina_mesto)){
                $narudzbina_ulice = DB::table('narudzbina_ulica')->where('narudzbina_mesto_code',$narudzbina_mesto->code)->orderBy('naziv','asc')->get();
                $select_ulice = View::make('shop/ajax/select-narudzbina-ulica',array('narudzbina_ulice'=>$narudzbina_ulice))->render();

                
                    echo $select_ulice;                
            }
        echo '';
    }

}
