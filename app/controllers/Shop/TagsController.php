<?php 

class TagsController extends Controller {

	public function index() {
        $offset = Language::segment_offset();
        $lang = Language::multi() ? Request::segment(1) : null;
        $tag = Request::segment(2+$offset);

		$tag_convert = Language::slug_convert($tag);
		
		if($tag_convert == '' || $tag_convert == null) {
			return Redirect::to(Options::base_url());
		}

		if(Session::has('limit')) {
			$limit = Session::get('limit');
		} else {
			$limit = 20;
		}

	    if(Input::get('page')) {
	    	$pageNo = Input::get('page');
	    } else {
	    	$pageNo = 1;
	    }
		$offset = ($pageNo-1) * $limit;

		$query = "SELECT roba_id FROM roba WHERE flag_aktivan=1 AND flag_prikazi_u_cenovniku=1 AND tags ILIKE '%" . $tag_convert . "%' ";
		$query_products = DB::select($query." ORDER BY ".Options::checkCena()." ".(Options::web_options(207) == 0 ? "ASC" : "DESC")." LIMIT ".$limit." OFFSET ".$offset."");


		$data=array( 
			"strana"=>'tagovi',
			"title"=>$tag,
			"description"=>$tag.' | '.Options::company_name(),
			"keywords"=>$tag.', '.Seo::company_tag(Options::company_name()),
			"articles"=>$query_products,
			"count_products"=>count(DB::select($query)),
			"filter_prikazi"=>0,
			"limit"=>$limit
		);

		return View::make('shop/themes/'.Support::theme_path().'pages/products_list',$data);
	}


} 