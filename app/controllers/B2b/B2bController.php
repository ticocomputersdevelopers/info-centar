<?php

use Illuminate\Support\Facades\Response;
use Service\Mailer;

class B2bController extends Controller {

    function index(){
        if(!B2bOptions::checkB2B()){
            return Redirect::to(B2bOptions::base_url());
        }
        if(Session::has('b2b_user_'.Options::server())){
            return Redirect::to(B2bOptions::base_url().'b2b/pocetna');
        }

        return Redirect::to(B2bOptions::base_url().'b2b/login');
    }

    function login(){
        if(Session::has('b2b_user_'.Options::server())){
            return Redirect::to(B2bOptions::base_url().'b2b/pocetna');
        }
        $strana='login';
        $seo=array(
            "title"=>'Prijava',
            "description"=>'Prijava',
            "keywords"=>'prijava',

        );
        return View::make('b2b.pages.login',compact('seo'));
    }

    function loginStore(){
        $input = Input::all();

        //WINGS
        if(B2bOptions::info_sys('wings')){
            $validator = Validator::make($input,array('username' => 'required','password' => 'required'),array('required' => 'Niste popunili polje!'));
            if ($validator->fails()) {
                return Redirect::to('b2b/login')->withErrors($validator);
            }else{
                $result = Wings::autorize($input['username'],$input['password']);
         
                if($result == false || isset($result->errors)){
                    $validator->getMessageBag()->add('password', 'Uneli ste pogrešno korisničko ime ili lozinku!');
                    return Redirect::to('b2b/login')->withErrors($validator);  
                }else{
                    $token = $result->data[0]->attributes->token;
                    $result = Wings::kupac_info($token);

                    $id_is=$result->data[0]->id;
                    
                    $partner = DB::table('partner')->where('login',$input['username'])->where('password',$input['password'])->first();
                    if(is_null($partner)){
                        $data['login']=$input['username'];
                        $data['password']=$input['password'];
                        if(!is_null(DB::table('partner')->where('id_is',$id_is)->first())){
                            DB::table('partner')->where('id_is',$id_is)->update($data);
                        }else{
                            $wingsPartner = $result->data[0];
                            $sifra=isset($wingsPartner->attributes->sifra) ? $wingsPartner->attributes->sifra : null;
                            $naziv=isset($wingsPartner->attributes->naziv) ? $wingsPartner->attributes->naziv : null;
                            $mesto=isset($wingsPartner->attributes->adresa2) ? $wingsPartner->attributes->adresa2 : null;
                            $adresa=isset($wingsPartner->attributes->adresa1) ? $wingsPartner->attributes->adresa1 : null;
                            $telefon=isset($wingsPartner->attributes->telefon) ? $wingsPartner->attributes->telefon : null;
                            $pib=isset($wingsPartner->attributes->pib) ? $wingsPartner->attributes->pib : null;
                            $mail=isset($wingsPartner->attributes->mail) ? $wingsPartner->attributes->mail : null;
                            $rabat=isset($wingsPartner->attributes->rabat) ? $wingsPartner->attributes->rabat : 0;
                            $status=isset($wingsPartner->attributes->status) ? $wingsPartner->attributes->status : "K";
                            $data = array(
                                'id_is' => $id_is,
                                'drzava_id' => 0,
                                'sifra' => $sifra,
                                'naziv' => $naziv,
                                'mesto' => $mesto,
                                'adresa' => $adresa,
                                'telefon' => $telefon,
                                'pib' => $pib,
                                'rabat' => $rabat,
                                'stara_sifra' => $status,
                                'login' => $input['username'],
                                'password' => $input['password']
                                );
                            DB::table('partner')->insert($data);
                        }
                        
                        $partner = DB::table('partner')->where('login',$input['username'])->where('password',$input['password'])->first();
                    }

                    if(is_null($partner)){
                        $validator->getMessageBag()->add('password', 'Uneli ste pogrešno korisničko ime ili lozinku!');
                        return Redirect::to('b2b/login')->withErrors($validator); 
                    }else{
                        Session::put('wings_token_'.B2bOptions::server(),$token);
                        Session::put('b2b_user_'.B2bOptions::server(),$partner->partner_id);
                        Wings::updateKartica();
                        return Redirect::to('b2b'); 
                    }
                }               
            }
        }

        $validator = Validator::make($input,array(
                'username' => 'required|between:3,50',
                'password' => 'required|between:3,50|exists:partner,password,login,'.$input['username'].''
            ),
            array(
                'required' => 'Niste popunili polje!',
                'regex' => 'Uneli ste ne dozvoljene karaktere!',
                'between' => 'Vaš sadzaj polja je prekratak ili predugačak!',
                'exists' => 'Uneli ste pogrešno korisničko ime ili lozinku!',
            )
        );
        if ($validator->fails()) {
            return Redirect::to('b2b/login')->withInput()->withErrors($validator);

        }else{

            $partner = B2bPartner::where('login',$input['username'])->where('password',$input['password'])->first();
            Session::put('b2b_user_'.B2bOptions::server(), $partner->partner_id);

            if(B2bOptions::info_sys('infograf')){
                if(!is_null($partner->id_is) && $partner->id_is != ''){
                    Session::put('b2b_user_'.B2bOptions::server().'_discounts', Infograf::discounts($partner->id_is));
                }else{
                    Session::put('b2b_user_'.B2bOptions::server().'_discounts', array());
                }

            }
            return Redirect::to('b2b/pocetna')->with('login_message',true);
        }

    }

    public function logout(){
        if(B2bOptions::info_sys('wings')){
            Wings::logout(Session::get('wings_token_'.B2bOptions::server()));
            Session::forget('wings_token_'.B2bOptions::server());

        }elseif(B2bOptions::info_sys('infograf')){
            Session::forget('b2b_user_'.B2bOptions::server().'_discounts');
        }

        Session::forget('b2b_user_'.B2bOptions::server());
        return Redirect::to(B2bOptions::base_url());
    }

    public function registration(){
        if(Session::has('b2b_user_'.Options::server())){
            return Redirect::to(B2bOptions::base_url().'b2b/pocetna');
        }

        if(B2bOptions::info_sys('calculus')){
            return Redirect::to('b2b'); 
        }
        $strana='login';
        $seo=array(
            "title"=>'Registracija',
            "description"=>'Registracija',
            "keywords"=>'registracija',

        );

        return View::make('b2b.pages.registration',compact('seo'));

    }

    public function registrationStore(){
        $inputs = Input::all();

        $rules = array(
                'naziv' => 'required|regex:'.Support::regex().'|between:2,50',
                'adresa' => 'required|regex:'.Support::regex().'|between:3,100',
                'mesto' => 'required|regex:'.Support::regex().'|between:2,100',
                'kontakt_osoba' => 'required|regex:'.AdminSupport::regex().'|max:255',
                'telefon' => 'required|regex:'.Support::regex().'|between:0,30',
                'mail' => 'required|email|max:50|unique:partner,mail',
                'pib' => 'required|numeric|digits_between:9,9',
                'broj_maticni' => 'numeric|digits_between:8,8'
            );
        $messages = array(
                'required' => 'Niste popunili polje!',
                'regex' => 'Polje sadrži neodgovarajuće karaktere!',
                'email' => 'E-mail adresa je neodgovarajuća!',
                'between' => 'Dužina sadžaja nije odgovarajuća!',
                'unique' => 'E-mail adresa je zauzeta!',
                'max'=>'Dužina sadžaja je neodgovarajuća!',
                'numeric'=>'Polje može sadržati samo brojeve',
                'digits_between' => 'Dužina sadžaja nije odgovarajuća!'
            );

        $validator = Validator::make($inputs, $rules, $messages);
        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator);
        } else {
            if(!B2bOptions::info_sys('wings')){
                $inputs['drzava_id']=0;
                DB::table('partner')->insert($inputs);
            }

            $body = '<h2>Zahtev za registraciju novog partnera na B2B portalu.</h2><br>';
            foreach($inputs as $key => $field){
                if($key != 'drzava_id'){
                    $body .= ($key == 'komercijalista' ? 'Kontakt Osoba' : ($key == 'broj_maticni' ? 'Matični Broj' : $key)).' : '.$field.'<br>';
                }
            }
            Mailer::send(B2bOptions::company_email(),B2bOptions::company_email(),'Registracija novog korisnika', $body);
            
            return Redirect::to('b2b/registration')->with('registration_message',true);
        }

    }

    public function userEdit(){

        $mesto = DB::table('mesto')->where('mesto_id','>','0')->orderBy('mesto','asc')->get();
        $seo=array(
            "title"=>"Informacije o korisniku",
            "description"=>"",
            "keywords"=>"",
        );
        $is_is = B2bOptions::info_sys('wings') || B2bOptions::info_sys('calculus') || B2bOptions::info_sys('infograf') || B2bOptions::info_sys('logik');
        $partner = DB::table('partner')->where('partner_id',Session::get('b2b_user_'.B2bOptions::server()))->first();
        trim($partner->pib);
        $pib = explode(" ", $partner->pib);
        for ($i=0;$i<count($pib);$i++) {
            if(!is_numeric($pib[$i]))
            {
                array_splice($pib, $i, 1);
                $i--;
            }
        }
        $partner->pib = implode(" ", $pib);

        return View::make('b2b.pages.user_edit',compact('mesto','seo','partner','is_is'));
    }

    public function userUpdate(){
        $partner_id = Session::get('b2b_user_'.B2bOptions::server());
        $inputs = Input::all();

        $rules = array(
                'naziv' => 'required|regex:'.Support::regex().'|between:2,50',
                'adresa' => 'required|regex:'.Support::regex().'|between:3,100',
                'mesto' => 'required|regex:'.Support::regex().'|between:2,100',
                'kontakt_osoba' => 'required|regex:'.AdminSupport::regex().'|max:255',
                'telefon' => 'required|regex:'.Support::regex().'|between:0,30',
                'mail' => 'required|email|max:50|unique:partner,mail,'.$partner_id.',partner_id',
                'pib' => 'required|numeric|digits_between:9,9',
                'broj_maticni' => 'numeric|digits_between:8,8',
                'telefon' => 'required|numeric',
                'login' => 'required',
                'password' => 'required'
               
            );
        $messages = array(
                'required' => 'Niste popunili polje!',
                'regex' => 'Polje sadrži neodgovarajuće karaktere!',
                'email' => 'E-mail adresa je neodgovarajuća!',
                'between' => 'Dužina sadžaja nije odgovarajuća!',
                'max' => 'Dužina sadžaja je neodgovarajuća!',
                'numeric'=>'Polje može sadržati samo brojeve',
                'digits_between' => 'Dužina sadžaja nije odgovarajuća!'
            );

         $validator = Validator::make($inputs, $rules, $messages);
        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator);
        } else{

        DB::table('partner')->where('partner_id',$partner_id)->update($inputs);

        return Redirect::route('b2b.user_edit');
        }
    }

    public function forgotPassword(){
        $seo=array(
            "title"=>"Zaboravljena lozinka",
            "description"=>"Zaboravljena lozinka",
            "keywords"=>"zaboravljena, lozinka",
        );
        return View::make('b2b.pages.forgot_password',compact('seo'));
    }

    public function forgotPasswordPost(){
        $input = Input::get();

        $validator = Validator::make($input,array(
                'mail' => 'required|exists:partner,mail'
            ),
            array(
                'required' => 'Niste popunili polje!',
                'exists' => 'E-mail ne postoji u bazi!',
            )
        );

        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator);
        } else {
            B2bPartner::forgotPassword($input['mail']);
            return Redirect::back()->with('forgot_message',"Poruka sa novom lozinkom je poslata na ".$input['mail']);
        }

    }

    function page($page){      
        if($page==B2bCommon::get_page_start()){
            $strana=B2bCommon::get_page_start();
            $seo=array(
                "title"=>B2bCommon::seo_title($strana),
                "description"=>B2bCommon::seo_description($strana),
                "keywords"=>B2bCommon::seo_keywords($strana),

            );
            return View::make('b2b.pages.pocetna',compact('seo'));
        }
        else if($page == B2bCommon::get_korpa()){
            $strana=B2bCommon::get_korpa();
            $items = DB::table('web_b2b_korpa_stavka')->where('web_b2b_korpa_id', Session::get('b2b_cart'))->orderBy('broj_stavke','asc')->get();
            $seo=array(
                "title"=>B2bCommon::seo_title($strana),
                "description"=>B2bCommon::seo_description($strana),
                "keywords"=>B2bCommon::seo_keywords($strana),

            );
            return View::make('b2b.pages.cart',compact('seo','items'));
        }
        else if($page == 'servis'){
            $strana= 'servis';
           
            $seo=array(
                "title"=>'Servis',
                "description"=>'Servis',
                "keywords"=>'servis',

            );
            return View::make('b2b.pages.servis',compact('seo','items'));
        }
        else if($page == 'rma'){
            $strana= 'rma';
           
            $seo=array(
                "title"=>'Rma',
                "description"=>'Rma',
                "keywords"=>'rma',

            );
            return View::make('b2b.pages.servis',compact('seo','items'));
        }
        else if($page == 'brendovi'){
            
            $strana= 'brendovi';
            $brendovi = DB::table('proizvodjac')->where('proizvodjac_id','!=',-1)->where(array('brend_prikazi'=>1))->orderBy('rbr','asc')->orderBy('naziv','asc')->get();

            $seo=array(
                "title"=>'brendovi',
                "description"=>'brendovi',
                "keywords"=>'brendovi',
                "brendovi"=>$brendovi

            );
            return View::make('b2b.pages.brendovi',compact('seo','brendovi'));
        }
          else if($page == 'kontakt'){
            $strana= 'kontakt';
           
            $seo=array(
                "title"=>'Kontakt',
                "description"=>'Kontakt',
                "keywords"=>'kontakt',

            );
            return View::make('b2b.pages.contact',compact('seo','items'));
        }
        else {
            $content = DB::table('web_b2b_seo')->where('naziv_stranice',$page)->first();
            if(is_null($content)){
                return Redirect::to(B2bOptions::base_url().'b2b');
            }

            if($content->grupa_pr_id > 0){
                $redirect = B2bOptions::base_url() . 'b2b/artikli/' . B2bUrl::url_convert(B2bCommon::getGrupa($content->grupa_pr_id));
                return Redirect::to($redirect);
            }

            $stranica_jezik=DB::table('web_b2b_seo_jezik')->where(array('web_b2b_seo_id'=>$content->web_b2b_seo_id, 'jezik_id'=>1))->first();
            $web_content = "";
            if(!is_null($stranica_jezik)){
                $web_content = $stranica_jezik->sadrzaj;
            }

            $seo=array(
                "title"=> !is_null($stranica_jezik) && $stranica_jezik->title ? $stranica_jezik->title : $page,
                "description"=> !is_null($stranica_jezik) && $stranica_jezik->description ? $stranica_jezik->description : $page,
                "keywords"=> !is_null($stranica_jezik) && $stranica_jezik->keywords ? $stranica_jezik->keywords : $page,
                "web_content"=>$web_content

            );
            return View::make('b2b.pages.page',compact('seo','page','content','web_content'));
        }

    }

    public function opisKvaraMail() {
       
       $partner = DB::table('partner')->where('partner_id',Session::get('b2b_user_'.B2bOptions::server()))->pluck('naziv');

        $input = Input::get();
        $naziv = Input::get('naziv');
        $datum = Input::get('datum');
        $brfakture = Input::get('brfakture');
        $serial = Input::get('serial');        
        $opis = Input::get('opis');
  

        $validator = Validator::make($input,array(
                'naziv' => 'required|between:3,50',
                'datum' => 'required',  
                'brfakture' => 'required',
                'serial' => 'required',                
                'opis' => 'required|between:3,50'
            )
        );
        if ($validator->fails()) {
            $messages = $validator->messages();

            return Redirect::to('b2b/servis')->withErrors($validator);

        } else {

            $body=" <p>Poštovani,<br /> Imate prijavu za servis<br />
            <b>Komitent:</b> ".$partner." <br />
            <b>Uređaj:</b> ".Input::get('naziv')." <br />
            <b>Datum kupovine:</b> ".Input::get('datum')." <br />
            <b>Broj fakture:</b> ".Input::get('brfakture')."<br />
            <b>Serijski broj:</b> ".Input::get('serial')."<br />
            <b>Opis kvara:</b> ".Input::get('opis')."<br />
            </p> <p>".Input::get('message')."</p>";

            

            Mailer::send(B2bOptions::company_email(),B2bOptions::company_email(),'Servis prijava', $body);

            return Redirect::to('b2b/servis')->withMessage('Uspešno ste poslali email.');
        }
 
    }

    public function userCard($date_start=null,$date_end=null){
        $cart_items = DB::table('web_b2b_kartica')->where('partner_id',Session::get('b2b_user_'.B2bOptions::server()));
        if(!is_null($date_start) && $date_start != 'null'){
            $cart_items = $cart_items->where('datum_dokumenta','>=',$date_start);
        }else{
            $date_start = null;
        }
        if(!is_null($date_end) && $date_end != 'null'){
            $cart_items = $cart_items->where('datum_dokumenta','<=',$date_end);
        }else{
            $date_end = null;
        }

        $sumDuguje = B2b::sumDuguje($cart_items);
        $sumPotrazuje = B2b::sumPotrazuje($cart_items);
        $cart_items = $cart_items->orderBy('datum_dokumenta','desc')->paginate(30);
        $seo =array(
            'title'=>"Kartica kupca",
            'description'=>"Kartica kupca",
            'keywords'=>"Kartica kupca"
        );
        $saldo = $sumPotrazuje  - $sumDuguje;
        $items = DB::table('web_b2b_korpa_stavka')->where('web_b2b_korpa_id', Session::get('b2b_cart'))->orderBy('broj_stavke','asc')->get();
        
        return View::make('b2b.pages.user_card',compact('items','cart_items','sumDuguje','sumPotrazuje','seo','saldo','date_start','date_end'));

    }
    public function servis(){       

        $data =array(
            "strana"=>'servis',
            'title'=>"Servis"
        );
        
        return View::make('b2b.pages.servis',$data);

    }
   
    public function brendovi(){       

        $brendovi = DB::table('proizvodjac')->where('proizvodjac_id','!=',-1)->where(array('brend_prikazi'=>1))->orderBy('rbr','asc')->orderBy('naziv','asc')->get();

        $data =array(
            "strana"=>'brendovi',
            'title'=>"brendovi",
            'brendovi'=>$brendovi
        );
        
        return View::make('b2b.pages.brendovi',$data);

    }

        public function kontakt(){       

        $data =array(
            "strana"=>'kontakt',
            'title'=>"kontakt"
        );
        
        return View::make('b2b.pages.contact',$data);

    }

    public function products_first($grupa1){
        $grupa_pr_id=B2bUrl::get_grupa_pr_id(array($grupa1));
        $filters=[];
        if($grupa_pr_id>0){
            if(B2bCommon::broj_cerki($grupa_pr_id)>0)
            {	
                $grupa_pr_ids = array();
                B2bCommon::allGroups($grupa_pr_ids,$grupa_pr_id);
                $queryOsnovni=DB::table('roba')->where('flag_aktivan',1)->where('flag_cenovnik',1)->whereIn('grupa_pr_id',$grupa_pr_ids);

                if(Session::has('limit')){
                    $limit=Session::get('limit');
                }
                else {
                    $limit=20;
                }
                $query_products2=$queryOsnovni->remember(5)->count();

                if(Session::has('order')){
                    if(Session::get('order')=='price_asc')
                    {

                        $query_products=$queryOsnovni->orderBy('web_cena','asc')->paginate($limit);
                    }
                    else if(Session::get('order')=='price_desc'){
                        $query_products=$queryOsnovni->orderBy('web_cena','desc')->paginate($limit);
                    }
                    else if(Session::get('order')=='news'){
                        $query_products=$queryOsnovni->orderBy('roba_id','desc')->paginate($limit);
                    }
                    else if(Session::get('order')=='name'){
                        $query_products=$queryOsnovni->orderBy('naziv_web','asc')->paginate($limit);
                    }
                                     }
                else {

                    $query_products=$queryOsnovni->orderBy('web_cena',(B2bOptions::web_options(207) == 0 ? 'asc' : 'desc'))->paginate($limit);
                }

                $filter_flag=0;

            }
            else {

                    foreach(Input::all() as $key => $row){
                        if($key != 'page'){
                            $filters[$key] = B2bCommon::get_filter_array(Input::get($key));
                        }
                    }
					
					
				$query_products=DB::table('roba')->where('flag_aktivan',1)->where('flag_cenovnik',1)->where('grupa_pr_id',$grupa_pr_id);
				
				if(isset($filters['proizvodjac'])){

					$query_products=$query_products->whereIn('proizvodjac_id',$filters['proizvodjac']);
				}
				
				if((!isset($filters['proizvodjac']) and count($filters)>0) or (isset($filters['proizvodjac']) and count($filters)>1)) {
                   $br=0;
                    $filtersStr2 ="0";
                    $filtersStr ="";
                        foreach($filters as $key => $row){
                            if($key != 'proizvodjac'){
                            $filtersStr2 .=",".implode(',',$row);
                            $br++;
                        }
                        }

                    if($filtersStr2!=''){
                    $filtersStr = "grupa_pr_vrednost_id IN ( ".$filtersStr2.")";
                            $products = [0];
            $webRobaKarakteristike = DB::select("SELECT wrk1.roba_id,  (SELECT count(roba_id) FROM web_roba_karakteristike WHERE ".$filtersStr." AND roba_id=wrk1.roba_id)  as products  from web_roba_karakteristike wrk1
                                                WHERE wrk1.roba_id > 0 AND wrk1.".$filtersStr." GROUP BY wrk1.roba_id ",array());
            foreach($webRobaKarakteristike as $row){
                if($row->products == $br){
                    $products[]=$row->roba_id;
                }

            }


            $query_products=$query_products->whereIn('roba_id',$products);

                        }
                    }


                    $query_products2=$query_products->count();
                    if(Session::has('limit')){
                        $limit=Session::get('limit');
                    }
                    else {
                        $limit=20;
                    }
                    if(Session::has('order')){
                        if(Session::get('order')=='price_asc')
                        {
                          $query_products=  $query_products->orderBy('web_cena','asc')->paginate($limit);
                        }
                        else if(Session::get('order')=='price_desc'){
                          $query_products=  $query_products->orderBy('web_cena','desc')->paginate($limit);
                        }
                        else if(Session::get('order')=='news'){
                          $query_products=  $query_products->orderBy('roba_id','desc')->paginate($limit);
                        }
                        else if(Session::get('order')=='name'){
                          $query_products=  $query_products->orderBy('naziv_web','asc')->paginate($limit);
                        }
                    }
                    else {

                       $query_products= $query_products->orderBy('web_cena',(B2bOptions::web_options(207) == 0 ? 'asc' : 'desc'))->paginate($limit);
                    }



                }
                $seo= array(
                    "title"=>B2bCommon::grupa_title($grupa_pr_id),
                    "description"=>B2bCommon::grupa_description($grupa_pr_id),
                    "keywords"=>B2bCommon::grupa_keywods($grupa_pr_id)
                );

                $data=array(
                    "strana"=>'artikli',
                    "grupa_pr_id"=>$grupa_pr_id,
                    "sub_cats" => B2bArticle::subGroups($grupa_pr_id),
                    "url"=>$grupa1,
                    "seo"=>$seo,
                    "query_products"=>$query_products,
                    "count_products"=>$query_products2,
                    "filters"=>$filters,
                    "filtersItems"=>B2b::getCategoryFilters($grupa_pr_id, $filters)
                );

                return View::make('b2b.pages.products',$data);


        }
        else {
            $strana=B2bCommon::get_page_start();
            $seo=array(
                "title"=>B2bCommon::seo_title($strana),
                "description"=>B2bCommon::seo_description($strana),
                "keywords"=>B2bCommon::seo_keywords($strana),

            );
            return View::make('b2b.pages.pocetna',compact('seo'));
        }
    }

    public function products_second($grupa1,$grupa2){

        $grupa_pr_id=B2bUrl::get_grupa_pr_id(array($grupa1,$grupa2));
        $filters=[];


        if($grupa_pr_id>0){
            if(B2bCommon::broj_cerki($grupa_pr_id)>0)
            {	
                $grupa_pr_ids = array();
                B2bCommon::allGroups($grupa_pr_ids,$grupa_pr_id);
                $queryOsnovni=DB::table('roba')->where('flag_aktivan',1)->where('flag_cenovnik',1)->whereIn('grupa_pr_id',$grupa_pr_ids);

                if(Session::has('limit')){
                    $limit=Session::get('limit');
                }
                else {
                    $limit=20;
                }

                $query_products2=$queryOsnovni->count();


                if(Session::has('order')){
                    if(Session::get('order')=='price_asc')
                    {
                        $query_products=$queryOsnovni->orderBy('web_cena','asc')->paginate($limit);
                    }
                    else if(Session::get('order')=='price_desc'){
                        $query_products=$queryOsnovni->orderBy('web_cena','desc')->paginate($limit);
                    }
                    else if(Session::get('order')=='news'){
                        $query_products=$queryOsnovni->orderBy('roba_id','desc')->paginate($limit);
                    }
                    else if(Session::get('order')=='name'){
                        $query_products=$queryOsnovni->orderBy('naziv_web','asc')->paginate($limit);
                    }
                }
                else {

                    $query_products=$queryOsnovni->orderBy('web_cena',(B2bOptions::web_options(207) == 0 ? 'asc' : 'desc'))->paginate($limit);
                }

                $filter_flag=0;

            }

            else {

                foreach(Input::all() as $key => $row){
                    if($key != 'page'){
                        $filters[$key] = B2bCommon::get_filter_array(Input::get($key));
                    }
                }


                $query_products=DB::table('roba')->where('flag_aktivan',1)->where('flag_cenovnik',1)->where('grupa_pr_id',$grupa_pr_id);

                if(isset($filters['proizvodjac'])){

                    $query_products=$query_products->whereIn('proizvodjac_id',$filters['proizvodjac']);
                }

                if((!isset($filters['proizvodjac']) and count($filters)>0) or (isset($filters['proizvodjac']) and count($filters)>1)) {
                    $br=0;
                    $filtersStr2 ="0";
                    $filtersStr ="";
                    foreach($filters as $key => $row){
                        if($key != 'proizvodjac'){
                            $filtersStr2 .=",".implode(',',$row);
                            $br++;
                        }
                    }

                    if($filtersStr2!=''){
                        $filtersStr = "grupa_pr_vrednost_id IN ( ".$filtersStr2.")";
                        $products = [0];
                        $webRobaKarakteristike = DB::select("SELECT wrk1.roba_id,  (SELECT count(roba_id) FROM web_roba_karakteristike WHERE ".$filtersStr." AND roba_id=wrk1.roba_id)  as products  from web_roba_karakteristike wrk1
                                                WHERE wrk1.roba_id > 0 AND wrk1.".$filtersStr." GROUP BY wrk1.roba_id ",array());
                        foreach($webRobaKarakteristike as $row){
                            if($row->products == $br){
                                $products[]=$row->roba_id;
                            }

                        }


                        $query_products=$query_products->whereIn('roba_id',$products);

                    }
                }


                $query_products2=$query_products->count();
                if(Session::has('limit')){
                    $limit=Session::get('limit');
                }
                else {
                    $limit=20;
                }
                if(Session::has('order')){
                    if(Session::get('order')=='price_asc')
                    {
                        $query_products=  $query_products->orderBy('web_cena','asc')->paginate($limit);
                    }
                    else if(Session::get('order')=='price_desc'){
                        $query_products=  $query_products->orderBy('web_cena','desc')->paginate($limit);
                    }
                    else if(Session::get('order')=='news'){
                        $query_products=  $query_products->orderBy('roba_id','desc')->paginate($limit);
                    }
                    else if(Session::get('order')=='name'){
                        $query_products=  $query_products->orderBy('naziv_web','asc')->paginate($limit);
                    }
                }
                else {

                    $query_products= $query_products->orderBy('web_cena',(B2bOptions::web_options(207) == 0 ? 'asc' : 'desc'))->paginate($limit);
                }



            }
            $seo= array(
                "title"=>B2bCommon::grupa_title($grupa_pr_id),
                "description"=>B2bCommon::grupa_description($grupa_pr_id),
                "keywords"=>B2bCommon::grupa_keywods($grupa_pr_id)
            );

            $data=array(
                "strana"=>'artikli',
                "grupa_pr_id"=>$grupa_pr_id,
                "sub_cats" => B2bArticle::subGroups($grupa_pr_id),
                "url"=>$grupa1."/".$grupa2,
                "seo"=>$seo,
                "query_products"=>$query_products,
                "count_products"=>$query_products2,
                "filters"=>$filters,
                "filtersItems"=>B2b::getCategoryFilters($grupa_pr_id, $filters)
            );


            return View::make('b2b.pages.products',$data);
        }
        else {
            $strana=B2bCommon::get_page_start();
            $seo=array(
                "title"=>B2bCommon::seo_title($strana),
                "description"=>B2bCommon::seo_description($strana),
                "keywords"=>B2bCommon::seo_keywords($strana),

            );
            return View::make('b2b.pages.pocetna',compact('seo'));
        }


    }

    public function products_third($grupa1,$grupa2,$grupa3){

        $grupa_pr_id=B2bUrl::get_grupa_pr_id(array($grupa1,$grupa2,$grupa3));
        $filters = [];
        if($grupa_pr_id>0){


                foreach(Input::all() as $key => $row){
                    if($key != 'page'){
                        $filters[$key] = B2bCommon::get_filter_array(Input::get($key));
                    }
                }

                $grupa_pr_ids = array();
                B2bCommon::allGroups($grupa_pr_ids,$grupa_pr_id);
                $query_products=DB::table('roba')->where('flag_aktivan',1)->where('flag_cenovnik',1)->whereIn('grupa_pr_id',$grupa_pr_ids);


                if(isset($filters['proizvodjac'])){

                    $query_products=$query_products->whereIn('proizvodjac_id',$filters['proizvodjac']);
                }

                if((!isset($filters['proizvodjac']) and count($filters)>0) or (isset($filters['proizvodjac']) and count($filters)>1)) {
                    $br=0;
                    $filtersStr2 ="0";
                    $filtersStr ="";
                    foreach($filters as $key => $row){
                        if($key != 'proizvodjac'){
                            $filtersStr2 .=",".implode(',',$row);
                            $br++;
                        }
                    }

                    if($filtersStr2!=''){
                        $filtersStr = "grupa_pr_vrednost_id IN ( ".$filtersStr2.")";
                        $products = [0];
                        $webRobaKarakteristike = DB::select("SELECT wrk1.roba_id,  (SELECT count(roba_id) FROM web_roba_karakteristike WHERE ".$filtersStr." AND roba_id=wrk1.roba_id)  as products  from web_roba_karakteristike wrk1
                                                WHERE wrk1.roba_id > 0 AND wrk1.".$filtersStr." GROUP BY wrk1.roba_id ",array());
                        foreach($webRobaKarakteristike as $row){
                            if($row->products == $br){
                                $products[]=$row->roba_id;
                            }

                        }


                        $query_products=$query_products->whereIn('roba_id',$products);

                    }
                }


                $query_products2=$query_products->count();
                if(Session::has('limit')){
                    $limit=Session::get('limit');
                }
                else {
                    $limit=20;
                }
                if(Session::has('order')){
                    if(Session::get('order')=='price_asc')
                    {
                        $query_products=  $query_products->orderBy('web_cena','asc')->paginate($limit);
                    }
                    else if(Session::get('order')=='price_desc'){
                        $query_products=  $query_products->orderBy('web_cena','desc')->paginate($limit);
                    }
                    else if(Session::get('order')=='news'){
                        $query_products=  $query_products->orderBy('roba_id','desc')->paginate($limit);
                    }
                    else if(Session::get('order')=='name'){
                        $query_products=  $query_products->orderBy('naziv_web','asc')->paginate($limit);
                    }
                }
                else {

                    $query_products= $query_products->orderBy('web_cena',(B2bOptions::web_options(207) == 0 ? 'asc' : 'desc'))->paginate($limit);
                }


            $seo= array(
                "title"=>B2bCommon::grupa_title($grupa_pr_id),
                "description"=>B2bCommon::grupa_description($grupa_pr_id),
                "keywords"=>B2bCommon::grupa_keywods($grupa_pr_id)
            );

            $data=array(
                "strana"=>'artikli',
                "grupa_pr_id"=>$grupa_pr_id,
                "sub_cats" => B2bArticle::subGroups($grupa_pr_id),
                "url"=>$grupa1."/".$grupa2."/".$grupa3,
                "seo"=>$seo,
                "query_products"=>$query_products,
                "count_products"=>$query_products2,
                "filters"=>$filters,
                "filtersItems"=>B2b::getCategoryFilters($grupa_pr_id, $filters)
            );


            return View::make('b2b.pages.products',$data);
            }


        else {
            $strana=B2bCommon::get_page_start();
            $seo=array(
                "title"=>B2bCommon::seo_title($strana),
                "description"=>B2bCommon::seo_description($strana),
                "keywords"=>B2bCommon::seo_keywords($strana),

            );
            return View::make('b2b.pages.pocetna',compact('seo'));
        }
    }

    public function products_fourth($grupa1,$grupa2,$grupa3,$grupa4){

        $grupa_pr_id=B2bUrl::get_grupa_pr_id(array($grupa1,$grupa2,$grupa3,$grupa4));
        $filters = [];
        if($grupa_pr_id>0){


                foreach(Input::all() as $key => $row){
                    if($key != 'page'){
                        $filters[$key] = B2bCommon::get_filter_array(Input::get($key));
                    }
                }

                $grupa_pr_ids = array();
                B2bCommon::allGroups($grupa_pr_ids,$grupa_pr_id);
                $query_products=DB::table('roba')->where('flag_aktivan',1)->where('flag_cenovnik',1)->whereIn('grupa_pr_id',$grupa_pr_ids);


                if(isset($filters['proizvodjac'])){

                    $query_products=$query_products->whereIn('proizvodjac_id',$filters['proizvodjac']);
                }

                if((!isset($filters['proizvodjac']) and count($filters)>0) or (isset($filters['proizvodjac']) and count($filters)>1)) {
                    $br=0;
                    $filtersStr2 ="0";
                    $filtersStr ="";
                    foreach($filters as $key => $row){
                        if($key != 'proizvodjac'){
                            $filtersStr2 .=",".implode(',',$row);
                            $br++;
                        }
                    }

                    if($filtersStr2!=''){
                        $filtersStr = "grupa_pr_vrednost_id IN ( ".$filtersStr2.")";
                        $products = [0];
                        $webRobaKarakteristike = DB::select("SELECT wrk1.roba_id,  (SELECT count(roba_id) FROM web_roba_karakteristike WHERE ".$filtersStr." AND roba_id=wrk1.roba_id)  as products  from web_roba_karakteristike wrk1
                                                WHERE wrk1.roba_id > 0 AND wrk1.".$filtersStr." GROUP BY wrk1.roba_id ",array());
                        foreach($webRobaKarakteristike as $row){
                            if($row->products == $br){
                                $products[]=$row->roba_id;
                            }

                        }


                        $query_products=$query_products->whereIn('roba_id',$products);

                    }
                }


                $query_products2=$query_products->count();
                if(Session::has('limit')){
                    $limit=Session::get('limit');
                }
                else {
                    $limit=20;
                }
                if(Session::has('order')){
                    if(Session::get('order')=='price_asc')
                    {
                        $query_products=  $query_products->orderBy('web_cena','asc')->paginate($limit);
                    }
                    else if(Session::get('order')=='price_desc'){
                        $query_products=  $query_products->orderBy('web_cena','desc')->paginate($limit);
                    }
                    else if(Session::get('order')=='news'){
                        $query_products=  $query_products->orderBy('roba_id','desc')->paginate($limit);
                    }
                    else if(Session::get('order')=='name'){
                        $query_products=  $query_products->orderBy('naziv_web','asc')->paginate($limit);
                    }
                }
                else {

                    $query_products= $query_products->orderBy('web_cena',(B2bOptions::web_options(207) == 0 ? 'asc' : 'desc'))->paginate($limit);
                }


            $seo= array(
                "title"=>B2bCommon::grupa_title($grupa_pr_id),
                "description"=>B2bCommon::grupa_description($grupa_pr_id),
                "keywords"=>B2bCommon::grupa_keywods($grupa_pr_id)
            );

            $data=array(
                "strana"=>'artikli',
                "grupa_pr_id"=>$grupa_pr_id,
                "seo"=>$seo,
                "sub_cats" => B2bArticle::subGroups($grupa_pr_id),
                "url"=>$grupa1."/".$grupa2."/".$grupa3."/".$grupa4,
                "query_products"=>$query_products,
                "count_products"=>$query_products2,
                "filters"=>$filters,
                "filtersItems"=>B2b::getCategoryFilters($grupa_pr_id, $filters)
            );


            return View::make('b2b.pages.products',$data);
            }


        else {
            $strana=B2bCommon::get_page_start();
            $seo=array(
                "title"=>B2bCommon::seo_title($strana),
                "description"=>B2bCommon::seo_description($strana),
                "keywords"=>B2bCommon::seo_keywords($strana),

            );
            return View::make('b2b.pages.pocetna',compact('seo'));
        }
    }
    public function search(){
        $search = Input::get('q');

        $search = strtolower($search);

        $a = array('љ','њ','е','р','т','з','у','и','о','п','ш','ђ','а','с','д','ф','г','х','ј','к','л','ч','ћ','ж','ѕ','џ','ц','в','б','н','м', 'Љ','Њ','Е','Р','Т','З','У','И','О','П','Ш','Ђ','А','С','Д','Ф','Г','Х','Ј','К','Л','Ч','Ћ','Ж','Ѕ','Џ','Ц','В','Б','Н','М'); 
        $b = array('lj', 'nj', 'e', 'r', 't', 'z', 'u', 'i', 'o', 'p', 'š', 'đ', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'č', 'ć', 'ž', 'y', 'dž', 'c', 'v', 'b', 'n', 'm', 'lj', 'nj', 'e', 'r', 't', 'z', 'u', 'i', 'o', 'p', 'š', 'đ', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'č', 'ć', 'ž', 'y', 'dž', 'c', 'v', 'b', 'n', 'm'); 

        $search = str_replace($a, $b, $search); 


        if(Session::has('limit')){
            $limit=Session::get('limit');
        }
        else {
            $limit=20;
        }

         if(Session::has('order')){
            if(Session::get('order')=='price_asc')
            {
                $query_products=DB::table('roba')->where('flag_aktivan',1)->where('flag_cenovnik',1)->where('naziv_web', 'ILIKE', "%".B2bUrl::search_conver($search)."%")->orWhere('tags', 'ILIKE', "%".B2bUrl::search_conver($search)."%")->orWhere('sku', 'ILIKE', "%".B2bUrl::search_conver($search)."%")->orWhere('sifra_d', 'ILIKE', "%".B2bUrl::search_conver($search)."%")->orWhere('sifra_is', 'ILIKE', "%".B2bUrl::search_conver($search)."%")->orWhere('web_opis', 'ILIKE', "%".B2bUrl::search_conver($search)."%")->orWhere('naziv_dopunski', 'ILIKE', "%".B2bUrl::search_conver($search)."%")->orderBy('web_cena','asc')->paginate($limit);
            }
            else if(Session::get('order')=='price_desc'){
                $query_products=DB::table('roba')->where('flag_aktivan',1)->where('flag_cenovnik',1)->where('naziv_web', 'ILIKE', "%".B2bUrl::search_conver($search)."%")->orWhere('tags', 'ILIKE', "%".B2bUrl::search_conver($search)."%")->orWhere('sku', 'ILIKE', "%".B2bUrl::search_conver($search)."%")->orWhere('sifra_d', 'ILIKE', "%".B2bUrl::search_conver($search)."%")->orWhere('sifra_is', 'ILIKE', "%".B2bUrl::search_conver($search)."%")->orWhere('web_opis', 'ILIKE', "%".B2bUrl::search_conver($search)."%")->orWhere('naziv_dopunski', 'ILIKE', "%".B2bUrl::search_conver($search)."%")->orderBy('web_cena','desc')->paginate($limit);
            }
            else if(Session::get('order')=='news'){
                $query_products=DB::table('roba')->where('flag_aktivan',1)->where('flag_cenovnik',1)->where('naziv_web', 'ILIKE', "%".B2bUrl::search_conver($search)."%")->orWhere('tags', 'ILIKE', "%".B2bUrl::search_conver($search)."%")->orWhere('sku', 'ILIKE', "%".B2bUrl::search_conver($search)."%")->orWhere('sifra_d', 'ILIKE', "%".B2bUrl::search_conver($search)."%")->orWhere('sifra_is', 'ILIKE', "%".B2bUrl::search_conver($search)."%")->orWhere('web_opis', 'ILIKE', "%".B2bUrl::search_conver($search)."%")->orWhere('naziv_dopunski', 'ILIKE', "%".B2bUrl::search_conver($search)."%")->orderBy('roba_id','desc')->paginate($limit);
            }
            else if(Session::get('order')=='name'){
                $query_products=DB::table('roba')->where('flag_aktivan',1)->where('flag_cenovnik',1)->where('naziv_web', 'ILIKE', "%".B2bUrl::search_conver($search)."%")->orWhere('tags', 'ILIKE', "%".B2bUrl::search_conver($search)."%")->orWhere('sku', 'ILIKE', "%".B2bUrl::search_conver($search)."%")->orWhere('sifra_d', 'ILIKE', "%".B2bUrl::search_conver($search)."%")->orWhere('sifra_is', 'ILIKE', "%".B2bUrl::search_conver($search)."%")->orWhere('web_opis', 'ILIKE', "%".B2bUrl::search_conver($search)."%")->orWhere('naziv_dopunski', 'ILIKE', "%".B2bUrl::search_conver($search)."%")->orderBy('naziv_web','asc')->paginate($limit);
            }
        }
        else {

            $query_products=DB::table('roba')->where('flag_aktivan',1)->where('flag_cenovnik',1)->where('naziv_web', 'ILIKE', "%".B2bUrl::search_conver($search)."%")->orWhere('tags', 'ILIKE', "%".B2bUrl::search_conver($search)."%")->orWhere('sku', 'ILIKE', "%".B2bUrl::search_conver($search)."%")->orWhere('sifra_d', 'ILIKE', "%".B2bUrl::search_conver($search)."%")->orWhere('sifra_is', 'ILIKE', "%".B2bUrl::search_conver($search)."%")->orWhere('web_opis', 'ILIKE', "%".B2bUrl::search_conver($search)."%")->orWhere('naziv_dopunski', 'ILIKE', "%".B2bUrl::search_conver($search)."%")->orderBy('web_cena','asc')->paginate($limit);
        }

        $query_products2=DB::table('roba')->where('flag_aktivan',1)->where('flag_cenovnik',1)->where('naziv_web', 'ILIKE', "%".B2bUrl::search_conver($search)."%")->orWhere('tags', 'ILIKE', "%".B2bUrl::search_conver($search)."%")->orWhere('sku', 'ILIKE', "%".B2bUrl::search_conver($search)."%")->orWhere('sifra_d', 'ILIKE', "%".B2bUrl::search_conver($search)."%")->orWhere('sifra_is', 'ILIKE', "%".B2bUrl::search_conver($search)."%")->orWhere('web_opis', 'ILIKE', "%".B2bUrl::search_conver($search)."%")->orWhere('naziv_dopunski', 'ILIKE', "%".B2bUrl::search_conver($search)."%")->count();
        
        $seo = array(
            "title"=>"Pretraga",
            "description"=>"",
            "keywords"=>"");
        $data=array(
            "title"=>"Pretraga",
            "description"=>"",
            "keywords"=>"",
            "strana"=>'pretraga',
            "seo"=>$seo,
            "query_products"=>$query_products,
            "count_products"=>$query_products2,
            "filter_flag"=>0,
        );
        return View::make('b2b.pages.products',$data);
    }
    

    public function productsType($type,$page){
        $queryOsnovni=DB::table('roba')->where('flag_aktivan',1)->where('flag_cenovnik',1)->where('tip_cene',$type);
        $query_products2=DB::table('roba')->where('flag_aktivan',1)->where('flag_cenovnik',1)->where('tip_cene',$type)->count();
        $query_products=$queryOsnovni->get();

        if(Session::has('limit')){
            $limit=Session::get('limit');
        }
        else {
            $limit=20;
        }
        $query_products2=$queryOsnovni->remember(5)->count();

        if(Session::has('order')){
            if(Session::get('order')=='price_asc')
            {
                $query_products=$queryOsnovni->orderBy('web_cena','asc')->paginate($limit);
            }
            else if(Session::get('order')=='price_desc'){
                $query_products=$queryOsnovni->orderBy('web_cena','desc')->paginate($limit);
            }
            else if(Session::get('order')=='news'){
                $query_products=$queryOsnovni->orderBy('roba_id','desc')->paginate($limit);
            }
            else if(Session::get('order')=='name'){
                $query_products=$queryOsnovni->orderBy('naziv_web','asc')->paginate($limit);
            }
        }
        else {

            $query_products=$queryOsnovni->orderBy('web_cena',(B2bOptions::web_options(207) == 0 ? 'asc' : 'desc'))->paginate($limit);
        }
        $seo = [
            "title"=>B2bCommon::seo_title($page),
            "description"=>B2bCommon::seo_description($page),
            "keywords"=>B2bCommon::seo_keywords($page),
        ];
        $data=array(
            "strana"=>$page,
            "seo"=>$seo,
            "query_products"=>$query_products,
            "count_products"=>$query_products2,
        );
        return View::make('b2b.pages.products',$data);
    }

    public function product($artikal){

        $roba_id=B2bArticle::get_product_id($artikal);
        
        if($roba_id>0){
            $seo = array(
                "title"=>B2bArticle::seo_title($roba_id),
                "description"=>B2bArticle::seo_description($roba_id),
                "keywords"=>B2bArticle::seo_keywords($roba_id)
            );
            $data=array(
                "strana"=>'artikal',
                "seo"=>$seo,
                "roba_id"=>$roba_id,
                "fajlovi"=>DB::select("SELECT * FROM web_files WHERE roba_id = ".$roba_id." ORDER BY vrsta_fajla_id ASC")

            );
            return View::make('b2b.pages.product',$data);
        }
        else {
            $strana=B2bCommon::get_page_start();
            $seo=array(
                "title"=>B2bCommon::seo_title($strana),
                "description"=>B2bCommon::seo_description($strana),
                "keywords"=>B2bCommon::seo_keywords($strana),

            );
            return View::make('b2b.pages.pocetna',compact('seo'));
        }
    }



    public function ordering($type){
        Session::put('order',$type);
        return Redirect::back();
    }


    /* =============== CART ==================== */

    public function cartAdd(){
        B2bBasket::addCartB2b(Input::get('roba_id'),Input::get('quantity'), Input::get('status'), Session::get('b2b_user_'.B2bOptions::server()));
        $cartAvailable = B2bArticle::quantityB2b(Input::get('roba_id')) - B2bBasket::getB2bQuantityItem(Input::get('roba_id'));
        $items = DB::table('web_b2b_korpa_stavka')->where('web_b2b_korpa_id', Session::get('b2b_cart'))->orderBy('broj_stavke','asc')->get();
        return Response::json([
            'countItems'      => B2bBasket::b2bCountItems(),
            'cartContent'     => View::make('b2b.partials.ajax.header_cart', compact('items'))->render(),
            'cartAvailable'    => $cartAvailable
        ]);
    }

    public function cartContent(){
        $items = DB::table('web_b2b_korpa_stavka')->where('web_b2b_korpa_id', Session::get('b2b_cart'))->orderBy('broj_stavke','asc')->get();
        return Response::json(['cartContent' => View::make('b2b.partials.ajax.header_cart', compact('items'))->render()]);
    }

    public function cartDelete(){

        DB::table('web_b2b_korpa_stavka')->where('web_b2b_korpa_stavka_id',Input::get('cart_id'))->delete();


    }
    public function cartDeleteAll(){
        DB::table('web_b2b_korpa_stavka')->where('web_b2b_korpa_id', Session::get('b2b_cart'))->delete();
    }

    public function cardRefresh(){
        if(B2bOptions::info_sys('calculus')){
            Calculus::updateCart();
        }elseif(B2bOptions::info_sys('infograf')){
            Infograf::updateCart();
        }elseif(B2bOptions::info_sys('logik')){
            Logik::updateCart();
        }
        
        return Redirect::back();    
    }

    /* =============== END CART ==================== */


    /* =============== CHECK OUT =================== */

    public function checkOut(){
            $input = Input::all();
        $narudzbine=DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id','!=','-1');
        date_default_timezone_set( 'Europe/Belgrade');

        $num = array(1);
        foreach($narudzbine->get() as $row){
            $num[] = intval(str_replace( 'WNB', '', $row->broj_dokumenta));
        }
        $br_nar = max($num) + 1;
        $num_nar = str_pad($br_nar, 4, '0', STR_PAD_LEFT);

        $data=array(
            'partner_id'=>Session::get('b2b_user_'.B2bOptions::server()),
            'orgj_id'=>-1,
            'poslovna_godina_id'=>B2bOptions::poslovna_godina(),
            'vrsta_dokumenta_id'=>500,
            'broj_dokumenta'=>"WNB".$num_nar,
            'datum_dokumenta'=>date("Y-m-d"),
            'valuta_id'=>1,
            'kurs'=>B2bOptions::kurs(),
            'web_nacin_placanja_id'=>$input['nacin_placanja'],
            'web_nacin_isporuke_id'=>$input['nacin_isporuke'],
            'iznos'=>0,
            'prihvaceno'=>0,
            'stornirano'=>0,
            'realizovano'=>0,
            'napomena'=>$input['napomena'],
            'ip_adresa'=>B2bCommon::ip_adress(),
            'posta_slanje_id'=>-1,
            'posta_slanje_poslato'=>0,
            'promena'=>1,
            'flag_promena_connect'=>0,
            'sifra_connect'=>0,
        );

        $cart = DB::table('web_b2b_korpa_stavka')->where('web_b2b_korpa_id',Session::get('b2b_cart'))->get();
        //WINGS
        $success = true;
        if(B2bOptions::info_sys('wings')){
            $response = Wings::wingsOrder($cart);
            if($response->success){
                $data['sifra_is'] = $response->order_id;
            }else{
                $success = false;
            }
        }elseif(B2bOptions::info_sys('calculus')){
            $nacin_isporuke = DB::table('web_nacin_isporuke')->where('web_nacin_isporuke_id',$input['nacin_isporuke'])->pluck('naziv');
            $response = Calculus::createOrder($cart,$nacin_isporuke,substr($input['napomena'],0,250));
            if($response->success){
                $data['sifra_is'] = $response->order_id;
            }else{
                $success = false;
            }            
        }elseif(B2bOptions::info_sys('infograf')){
            $nacin_isporuke = DB::table('web_nacin_isporuke')->where('web_nacin_isporuke_id',$input['nacin_isporuke'])->pluck('naziv');
            $nacin_placanja = DB::table('web_nacin_placanja')->where('web_nacin_placanja_id',$input['nacin_placanja'])->pluck('naziv');
            $napomena = 'Način isporuke: '.$nacin_isporuke.'. Način plaćanja: '.$nacin_placanja.'. Napomena: '.$input['napomena'];
            $response = Infograf::createOrder($cart,$napomena);
            if($response->success){
                $data['sifra_is'] = $response->order_id;
            }else{
                $success = false;
            }            
        }elseif(B2bOptions::info_sys('logik')){
            $nacin_isporuke = DB::table('web_nacin_isporuke')->where('web_nacin_isporuke_id',$input['nacin_isporuke'])->pluck('naziv');
            $nacin_placanja = DB::table('web_nacin_placanja')->where('web_nacin_placanja_id',$input['nacin_placanja'])->pluck('naziv');
            
            $response = Logik::createOrder($cart,$nacin_isporuke,$nacin_placanja,$input['napomena']);
            if($response->success){
                $data['sifra_is'] = $response->order_id;
            }else{
                $success = false;
            }            
        }
        
        if($success){
            DB::table('web_b2b_narudzbina')->insert($data);

            $order_id = DB::table('web_b2b_narudzbina')->where('partner_id',Session::get('b2b_user_'.B2bOptions::server()))->where('broj_dokumenta',"WNB".$num_nar)->pluck('web_b2b_narudzbina_id');

            $cartItems = array();
            foreach($cart as $row){
                $roba = DB::table('roba')->where('roba_id',$row->roba_id)->first();
                if(isset($roba->roba_id) && $roba->roba_id != null){
                    $cartItems[]=[
                      'web_b2b_narudzbina_id'=>$order_id,
                      'broj_stavke'=>$row->broj_stavke,
                      'roba_id'=>$row->roba_id,
                      'kolicina'=>$row->kolicina,
                      'jm_cena'=>$row->jm_cena,
                      'tarifna_grupa_id'=>$row->tarifna_grupa_id,
                      'racunska_cena_nc'=>$row->racunska_cena_nc,
                    ];
                }
            }
            if(count($cartItems) > 0){
                DB::table('web_b2b_narudzbina_stavka')->insert($cartItems);
            }
            Session::forget('b2b_cart');

            $order = DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id',$order_id)->first();
            $partner = DB::table('partner')->where('partner_id',Session::get('b2b_user_'.B2bOptions::server()))->first();
            $orderItems = DB::table('web_b2b_narudzbina_stavka')->where('web_b2b_narudzbina_id',$order->web_b2b_narudzbina_id)->orderBy('broj_stavke','asc')->get();

            $body = View::make('b2b.emails.order',compact('order','partner','orderItems'))->render();

            B2bCommon::send_email_to_client($body,$partner->mail,"Porudžbina WNB".$num_nar);
            B2bCommon::send_email_to_admin($body, $partner->mail, $partner->naziv, "Porudžbina WNB".$num_nar);

           return  Redirect::route('b2b.order',["WNB".$num_nar]);
       }
       return  Redirect::to('/b2b/korpa');
    }

    public function order($bill){
        $order = DB::table('web_b2b_narudzbina')->where('broj_dokumenta',$bill)->where('partner_id',Session::get('b2b_user_'.B2bOptions::server()))->first();
        if(! $order){
           return Redirect::to('b2b');
        }
        else {
            $partner = DB::table('partner')->where('partner_id',Session::get('b2b_user_'.B2bOptions::server()))->first();
            $orderItems = DB::table('web_b2b_narudzbina_stavka')->where('web_b2b_narudzbina_id',$order->web_b2b_narudzbina_id)->orderBy('broj_stavke','asc')->get();
            $seo=array(
                "title"=>"Porudžbina ".$order->broj_dokumenta,
                "description"=>"",
                "keywords"=>"",

            );
            return View::make('b2b.pages.order',compact('seo','order','partner','orderItems'));
        }
    }

    /* ============ END CHECK OUT ================== */
    public function prikaz($prikaz){
       Session::put('b2b_prikaz',$prikaz);
    
        return Redirect::back();
    
    }
    public function b2b_valuta($valuta){
       Session::put('b2b_valuta',$valuta);
    
       return Redirect::back();
    
    }
    

}