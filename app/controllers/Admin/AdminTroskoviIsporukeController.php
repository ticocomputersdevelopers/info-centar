<?php
 
class AdminTroskoviIsporukeController extends Controller {

	public function index() {
		$tr_isp = DB::table('web_troskovi_isporuke')->orderBy('web_troskovi_isporuke','asc')->get();
		$tr_count = count($tr_isp);

		$troskovi_isporuke = array();
		if($tr_count>0){		
			for($i=0;$i<$tr_count;$i++){
				if($i==0){
					$troskovi_isporuke[$i] = (object) array('web_troskovi_isporuke_id'=>$tr_isp[$i]->web_troskovi_isporuke_id,'tezina_od'=>0,'tezina_do'=>$tr_isp[$i]->tezina_gr,'cena'=>$tr_isp[$i]->cena);
				}else{
					$troskovi_isporuke[$i] = (object) array('web_troskovi_isporuke_id'=>$tr_isp[$i]->web_troskovi_isporuke_id,'tezina_od'=>$tr_isp[$i-1]->tezina_gr,'tezina_do'=>$tr_isp[$i]->tezina_gr,'cena'=>$tr_isp[$i]->cena);
				}
			}
			$troskovi_isporuke[$tr_count] = (object) array('web_troskovi_isporuke_id'=>0,'tezina_od'=>$tr_isp[$tr_count-1]->tezina_gr,'tezina_do'=>'','cena'=>'');
		}else{
			$troskovi_isporuke[] = (object) array('web_troskovi_isporuke_id'=>0,'tezina_od'=>0,'tezina_do'=>'','cena'=>'');
		}

		$data = array(
	                'strana'=>'troskovi_isporuke',
	                'title'=> 'Troškovi isporuke',
	                'cena_isporuke'=>DB::table('cena_isporuke')->get(),
	                'troskovi_isporuke'=>$troskovi_isporuke
	            );
		return View::make('admin/page', $data);
	}

	public function save() {
		$data = Input::get();
		$max_id = DB::table('web_troskovi_isporuke')->max('web_troskovi_isporuke_id');
		$validator_messages = array('required'=>'Polje nesme biti prazno!','numeric'=>'Polje sme da sadrzi samo brojeve!','digits_between'=>'Duzina unetih cifara je predugačka!','min'=>'Minimalna vrednost polja je neodgovarajuća!','max'=>'Prekoračili ste maksimalnu vrednost polja!');
		if($data['web_troskovi_isporuke_id']!=0 && $data['web_troskovi_isporuke_id'] < $max_id){
			$max_tezina = DB::table('web_troskovi_isporuke')->where('web_troskovi_isporuke_id','>',$data['web_troskovi_isporuke_id'])->orderBy('web_troskovi_isporuke_id','asc')->first()->tezina_gr-0.0001;
        	$validator = Validator::make(array('tezina_gr'=>$data['tezina_gr'],'cena'=>$data['cena']), array('tezina_gr' => 'required|numeric|digits_between:0,20|min:'.($data['tezina_od']+0.0001).'|max:'.$max_tezina,'cena' => 'required|numeric|digits_between:0,10'),$validator_messages);
		}else{
        	$validator = Validator::make(array('tezina_gr'=>$data['tezina_gr'],'cena'=>$data['cena']), array('tezina_gr' => 'required|numeric|digits_between:0,20|min:'.($data['tezina_od']+0.0001),'cena' => 'required|numeric|digits_between:0,10'),$validator_messages);
		}
        if($validator->fails()){
        	return Redirect::to(AdminOptions::base_url().'admin/troskovi-isporuke')->withInput()->withErrors($validator->messages());
        }else{
        	if($data['web_troskovi_isporuke_id']==0){
        		$data['web_troskovi_isporuke_id'] = $max_id+1;
        		DB::table('web_troskovi_isporuke')->insert(array('web_troskovi_isporuke_id'=>$data['web_troskovi_isporuke_id'],'tezina_gr'=>$data['tezina_gr'],'cena'=>$data['cena']));
        	}else{
        		DB::table('web_troskovi_isporuke')->where('web_troskovi_isporuke_id',$data['web_troskovi_isporuke_id'])->update(array('tezina_gr'=>$data['tezina_gr'],'cena'=>$data['cena']));
        	}
        	return Redirect::to(AdminOptions::base_url().'admin/troskovi-isporuke')->with('success',true);
        }
	}

	public function delete($id) {
		DB::table('web_troskovi_isporuke')->where('web_troskovi_isporuke_id',$id)->delete();
		return Redirect::to(AdminOptions::base_url().'admin/troskovi-isporuke')->with('success-delete',true);
	}

	
	public function cena() {

		$cena_isporuke =DB::table('cena_isporuke')->pluck('cena');
		
		$data = array(
	                'strana'=>'troskovi_isporuke',
	                'title'=> 'Troškovi isporuke',
	                'cena_isporuke'=>$cena_isporuke
	            );

		return View::make('admin/troskovi_isporuke', $data);
	}

	public function cena_save() {
		
		$inputs = Input::get();
		$validator = Validator::make($inputs, array('cena' => 'numeric' ));
		if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }else{
		 $general_data = $inputs;

            if($inputs['cena_id'] != 0){
                DB::table('cena_isporuke')->where('cena_id',$inputs['cena_id'])->update($general_data);
            }else{
                DB::table('cena_isporuke')->insert($general_data);
            }
		return Redirect::to(AdminOptions::base_url().'admin/troskovi-isporuke')->with('success',true);
        }
	}

	public function cena_delete($id) {
		DB::table('cena_isporuke')->where('cena_isporuke_id',$id)->delete();
		return Redirect::to(AdminOptions::base_url().'admin/troskovi-isporuke')->with('success-delete',true);
	}




}