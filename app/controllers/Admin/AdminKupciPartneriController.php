<?php

class AdminKupciPartneriController extends Controller {



	public function getKupci($vrsta_kupca='n',$status_registracije='n',$search=null){

		$kupci = AdminKupci::getKupci($vrsta_kupca,$status_registracije,$search);

		$data=array(
	                "strana" =>'kupci',
	                "title" => 'Kupci',
	                "vrsta_kupca" => $vrsta_kupca,
	                "status_registracije" => $status_registracije,
	                "search" => !is_null($search) ? $search : '',
	                "web_kupac" => $kupci
	            );
		return View::make('admin/page', $data);
		}
	public function getKupciSearch($vrsta_kupca='n',$status_registracije='n'){
		return Redirect::to(AdminOptions::base_url().'admin/kupci_partneri/kupci/'.$vrsta_kupca.'/'.$status_registracije.'/'.urlencode(Input::get('search')));
	}
	public function getPartneri($search=null){
		
		$partneri=AdminKupci::getPartneriSearch($search);
		$data=array(
	        "strana"=>'partneri',
	        "title"=> 'Partneri',
	        "search" => !is_null($search) ? $search : '',
	        "partneri" => $partneri
	    );
		return View::make('admin/page', $data);
	}

	public function partneriSearch(){
		return Redirect::to(AdminOptions::base_url().'admin/kupci_partneri/partneri/'.urlencode(Input::get('search')));
	}


	public function getKupac($id) {
		$data=array(
			"strana"=>'kupac',
			"title"=> 'Kupac ',
			"web_kupac_id" => AdminKupci::getKupac($id, 'web_kupac_id') == null ? 0 : AdminKupci::getKupac($id, 'web_kupac_id'),
			"ime" => AdminKupci::getKupac($id, 'ime'),
			"prezime" => AdminKupci::getKupac($id, 'prezime'),
			"flag_vrsta_kupca" => AdminKupci::getKupac($id, 'flag_vrsta_kupca'),
			"naziv" => AdminKupci::getKupac($id, 'naziv'),
			"pib" => AdminKupci::getKupac($id, 'pib'),
			"adresa" => AdminKupci::getKupac($id, 'adresa'),
			"mesto" => AdminKupci::getKupac($id, 'mesto'), 
			"telefon" => AdminKupci::getKupac($id, 'telefon'),
			"fax" => AdminKupci::getKupac($id, 'fax'),
			"email" => AdminKupci::getKupac($id, 'email'),
			"telefon_mobilni" => AdminKupci::getKupac($id, 'telefon_mobilni'),
			"lozinka" => base64_decode(AdminKupci::getKupac($id, 'lozinka')),
			"flag_prima_poruke" => AdminKupci::getKupac($id, 'flag_prima_poruke'),
			"status_registracije" => AdminKupci::getKupac($id, 'status_registracije'),
			"ulica_id" => AdminKupci::getKupac($id, 'ulica_id'),
			"broj" => AdminKupci::getKupac($id, 'broj'),

		);
		return View::make('admin/page', $data);
		}	
		
	public function deleteKupac($id,$confirm){
		$check_confirm = $confirm=='1'?true:false;
		$obj_korpa = DB::table('web_b2c_korpa')->select('web_b2c_korpa_id')->where('web_kupac_id', $id);
		$obj_narudzbina = DB::table('web_b2c_narudzbina')->select('web_b2c_narudzbina_id')->where('web_kupac_id', $id);
		$check_korpa = $obj_korpa->count()>0?false:true;
		$check_narudzbina = $obj_narudzbina->count()>0?false:true;

		if($check_korpa && $check_narudzbina){
			DB::table('web_kupac')->where('web_kupac_id', $id)->delete();
			AdminSupport::saveLog('Kupci, DELETE web_kupac_id -> '.$id);
		}else{
			if($check_confirm){
				DB::table('web_b2c_narudzbina_stavka')->whereIn('web_b2c_narudzbina_id',array_map('current',$obj_narudzbina->get()))->delete();
				$obj_narudzbina->delete();
				DB::table('web_b2c_korpa_stavka')->whereIn('web_b2c_korpa_id',array_map('current',$obj_korpa->get()))->delete();
				$obj_korpa->delete();

				DB::table('web_kupac')->where('web_kupac_id', $id)->delete();
				AdminSupport::saveLog('Kupci, DELETE web_kupac_id -> '.$id);
			}else{
				return Redirect::to('admin/kupci_partneri/kupci')->with('confirm_kupac_id',$id);
			}
		}
		return Redirect::to('admin/kupci_partneri/kupci')->with('message','Uspešno ste obrisali sadržaj.'); 
	}

	public function snimiKaoPartner($id){
		$kupac = DB::table('web_kupac')->where('web_kupac_id', $id)->get();

		foreach ($kupac as $row) {
			$naziv = $row->naziv;
			$adresa = $row->adresa;	
			$telefon = $row->telefon;
			$email = $row->email;
			$mesto = $row->mesto;
			$fax = $row->fax;
			$pib = $row->pib;
		}
		DB::table('partner')->insert([
			'naziv' => $naziv, 
			'adresa' => $adresa,
			'telefon' => $telefon,
			'mail' => $email,
			'mesto' => $mesto,
			'drzava_id' => 0,
			'fax' => $fax,
			'pib' => $pib
			]);
		

		AdminSupport::saveLog('Kupac u partnera, INSERT web_kupac_id -> '.$id);
		return Redirect::to('admin/kupci_partneri/kupci'); 
	}

	public function saveKupac($id){
		$data = Input::get();
		
		if(isset($data['flag_vrsta_kupca'])){
			$data['flag_vrsta_kupca'] = 1;
		} else {
			$data['flag_vrsta_kupca'] = 0;
		}

		if(isset($data['status_registracije'])){
			$data['status_registracije'] = 1;
		} else {
			$data['status_registracije'] = 0;
		}

		if(isset($data['flag_prima_poruke'])){
			$data['flag_prima_poruke'] = 1;
		} else {
			$data['flag_prima_poruke'] = 0;
		}
		
		$api_posta = false;
		if(AdminNarudzbine::api_posta(3)){
			$api_posta = true;
		}

		$messages = array(
    		'required' => 'Niste popunili polje!',
    		'regex' => 'Unesite odgovarajuči karakter!',
    		'email' => 'Unesite odgovarajuči mail!',
    		'between' => 'Unesite više od tri karakera ili brojeva!',
    		'unique' => 'Mail već postoji u bazi!'
		);

		$validator_arr = array(
 			'telefon' => 'regex:'.Support::regex().'|between:3,15',
            'email' => 'required|email|unique:web_kupac,email,'.$data['web_kupac_id'].',web_kupac_id,status_registracije,1|between:5,50',
            'fax' => 'between: 3, 20|regex:'.Support::regex().'',
            'telefon_mobilni' => 'between: 3, 20|regex:'.Support::regex().''
		);

		if ($data['flag_vrsta_kupca']==1) {
			$validator_arr['naziv'] = 'required|regex:'.Support::regex().'|between:3,20';
			$validator_arr['pib'] = 'required|regex:'.Support::regex().'|between:3,20';
		}else{
			$validator_arr['ime'] = 'required|regex:'.Support::regex().'|between:3,20';
			$validator_arr['prezime'] = 'required|regex:'.Support::regex().'|between:3,20';
		}

		if ($data['status_registracije']==1) {
			$validator_arr['lozinka'] = 'required|regex:'.Support::regex().'|between:3,20';
		}else{
			$validator_arr['lozinka'] = 'regex:'.Support::regex().'|between:3,20';
		}

		if(AdminNarudzbine::api_posta(3)){
			$validator_arr['broj'] = 'required|regex:'.Support::regex().'|max:20';
		}else{
			$validator_arr['adresa'] = 'required|regex:'.Support::regex().'|between:3,50';
 			$validator_arr['mesto'] = 'required|regex:'.Support::regex().'|between:3,50';
		}

		$validator = Validator::make($data, $validator_arr, $messages);
		if ($validator->passes()) {
			unset($data['opstina']);
			unset($data['mesto_id']);
			$data['lozinka'] = base64_encode($data['lozinka']);

			if($data['web_kupac_id']==0){
				unset($data['web_kupac_id']);
				DB::table('web_kupac')->insert($data);
				$data['web_kupac_id'] = DB::select("SELECT currval('web_kupac_web_kupac_id_seq') as new_id")[0]->new_id;
			}else{
				DB::table('web_kupac')->where('web_kupac_id',$data['web_kupac_id'])->update($data);
			}
			$message='Uspešno ste sačuvali podatke.';
			return Redirect::to(AdminOptions::base_url().'admin/kupci_partneri/kupci/'.$data['web_kupac_id'])->with('message',$message);
		}else{
			return Redirect::back()->withInput()->withErrors($validator);
		}
	}

	public function getPartner($id) {
		$import_check = in_array(117, AdminKupci::checkVrstaPartner($id));
		$data=array(
			"strana"=>'partner',
			"title"=> 'Partner',
			"partner_id" => AdminKupci::getPartner($id, 'partner_id') == null ? 0 : AdminKupci::getPartner($id, 'partner_id'),
			"naziv" => AdminKupci::getPartner($id, 'naziv'),
			"pib" => trim(AdminKupci::getPartner($id, 'pib')),
			"adresa" => AdminKupci::getPartner($id, 'adresa'),
			"mesto" => AdminKupci::getPartner($id, 'mesto'),
			"telefon" => AdminKupci::getPartner($id, 'telefon'),
			"fax" => AdminKupci::getPartner($id, 'fax'),
			"mail" => AdminKupci::getPartner($id, 'mail'),
			"sifra" => AdminKupci::getPartner($id, 'sifra'),
			"naziv_puni" => AdminKupci::getPartner($id, 'naziv_puni'),
			"drzava_id" => AdminKupci::getPartner($id, 'drzava_id'),
			"racun" => AdminKupci::getPartner($id, 'racun'),
			"broj_maticni" => AdminKupci::getPartner($id, 'broj_maticni'),
			"broj_registra" => AdminKupci::getPartner($id, 'broj_registra'),
			"broj_resenja" => AdminKupci::getPartner($id, 'broj_resenja'),
			"delatnost_sifra" => AdminKupci::getPartner($id, 'delatnost_sifra'),
			"delatnost_naziv" => AdminKupci::getPartner($id, 'delatnost_naziv'),
			"rabat" => AdminKupci::getPartner($id, 'rabat'),
			"limit_p" => AdminKupci::getPartner($id, 'limit_p'),
			"valuta_id" => AdminKupci::getPartner($id, 'valuta_id'),
			"tip_cene_id" => AdminKupci::getPartner($id, 'tip_cene_id'),
			"login" => AdminKupci::getPartner($id, 'login'),
			"password" => AdminKupci::getPartner($id, 'password'),
			"napomena" => AdminKupci::getPartner($id, 'napomena'),
			"preracunavanje_cena" => AdminKupci::getPartner($id, 'preracunavanje_cena'),
			"kontakt_osoba" => AdminKupci::getPartner($id,'kontakt_osoba'),
			"ulica_id" => AdminKupci::getPartner($id,'ulica_id') ? AdminKupci::getPartner($id,'ulica_id') : 0,
			"broj" => AdminKupci::getPartner($id,'broj'),
			"service_username" => $import_check ? AdminKupci::findPartnerImport($id,'service_username') : '',
			"service_password" => $import_check ? AdminKupci::findPartnerImport($id,'service_password') : '',
			"auto_link" => $import_check ? AdminKupci::findPartnerImport($id,'auto_link') : '',
			"import_naziv_web" => $import_check ? AdminKupci::findPartnerImport($id,'import_naziv_web') : '',
			"auto_import" => $import_check ? AdminKupci::findPartnerImport($id,'auto_import') : '',
			"auto_import_short" => $import_check ? AdminKupci::findPartnerImport($id,'auto_import_short') : '',
			"zadnji_kurs" => $import_check ? AdminKupci::findPartnerImport($id,'zadnji_kurs') : '',
			"kartica" => AdminPartneri::getB2Bkartica($id),
			"sumPotrazuje" => AdminPartneri::getPotrazuje($id),
			"saldo" => AdminPartneri::getSum($id),
			"sumDuguje" => AdminPartneri::getDuguje($id)
		);

		return View::make('admin/page', $data);
	}

	public function savePartner($id){
		$inputs = Input::get();
		$data = $inputs;

	if(Admin_model::check_admin()){
		if(isset($data['preracunavanje_cena']) ) {
			$data['preracunavanje_cena'] = 1;
		} else {
			$data['preracunavanje_cena'] = 0;
		}
	}

		unset($data['service_username']);
		unset($data['service_password']);
		unset($data['auto_link']);
		unset($data['zadnji_kurs']);
		unset($data['import_naziv_web']);
		unset($data['auto_import']);
		unset($data['auto_import_short']);
		$messages = array(
    		'required' => 'Niste popunili polje!',
    		'regex' => 'Unesite odgovarajuči karakter!',
    		'email' => 'Unesite odgovarajuči mail!',
    		'between' => 'Unesite više od tri karakera ili brojeva!',
    		'unique' => 'Mail već postoji u bazi!',
    		'max'=>'Prekoračili ste limit',
    		'numeric'=>'Unesite samo brojeve',
    		'digits_between' => 'Dužina sadržaja nije odgovarajuća'
		);
		if($id == 0 || $id == null){
			$mail_validation = 'email|max:50|unique:partner,mail';
		}else{
			$mail_validation = 'email|max:50|unique:partner,mail,'.$data['partner_id'].',partner_id';
		}
		$rules = array(
                'naziv' => 'required|regex:'.Support::regex().'|between:2,50',
                'sifra' => 'regex:'.Support::regex().'|max:15',
                'adresa' => 'required|regex:'.Support::regex().'|between:3,100',
                'mesto' => 'regex:'.Support::regex().'|between:2,100',
                'kontakt_osoba' => 'required|regex:'.AdminSupport::regex().'|max:255',
                'telefon' => 'required|regex:'.Support::regex().'|between:0,30',
                'fax' => 'regex:'.Support::regex().'|numeric',
                'mail' => $mail_validation,
                'limit_p' => 'digits_between:0,9|numeric',
                'pib' => 'required|numeric|digits_between:9,9',
                'broj_maticni' => 'numeric|digits_between:8,8',
                'napomena' => 'regex:'.Support::regex().'|max:500',
                'login' => 'regex:'.Support::regex().'|max:20',
                'naziv_puni' => 'regex:'.Support::regex().'|max:200',
                'racun' => 'regex:'.Support::regex().'|between:0,35',
                'broj_registra' => 'regex:'.Support::regex().'|max:30',
                'broj_resenja' => 'regex:'.Support::regex().'|max:15',
                'delatnost_sifra' => 'digits_between:4,4|numeric',
                'delatnost_naziv' => 'regex:'.Support::regex().'|max:50',
                'rabat' => 'digits_between:0,9|numeric',
                'password' => 'regex:'.Support::regex().'|between:3,20',
                'service_username' => 'max:255',
                'service_password' => 'max:255',
                'auto_link' => 'max:500'
            );
		if(AdminNarudzbine::api_posta(3)){
			$rules['broj'] = 'required|regex:'.Support::regex().'|max:25';
		}else{
			$rules['adresa'] = 'required|regex:'.Support::regex().'|between:3,100';
            $rules['mesto'] = 'required|regex:'.Support::regex().'|between:2,100';
		}
		$validator = Validator::make($inputs, $rules, $messages);
        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator);
        } else {

            if(AdminNarudzbine::api_posta(3)){
        		unset($data['opstina']);
                unset($data['mesto_id']);
                $data['drzava_id'] = 1;
            }        	
			if($id == 0 || $id == null){
				$data['partner_id'] = DB::select("SELECT nextval('partner_partner_id_seq')")[0]->nextval;
				$data['rabat'] = 0.00;
				$data['limit_p'] = 0.00;
				
				// unset($data['partner_id']);
				DB::table('partner')->insert($data);
				AdminSupport::saveLog('Partneri, INSERT partner_id -> '. DB::select("SELECT MAX(partner_id) FROM partner")[0]->max);
				return Redirect::to(AdminOptions::base_url().'admin/kupci_partneri/partner/'.$data['partner_id'])->with('message','Uspešno ste sačuvali podatke');
			} else {

				DB::table('partner')->where('partner_id', $data['partner_id'])->update($data);

				if(in_array(117, AdminKupci::checkVrstaPartner($data['partner_id'])) && Admin_model::check_admin()){
					$data_kolone = array();
					$data_kolone['service_username'] = $inputs['service_username'];
					$data_kolone['service_password'] = $inputs['service_password'];
					$data_kolone['auto_link'] = $inputs['auto_link'];
					$data_kolone['import_naziv_web'] = $inputs['import_naziv_web'];
					$data_kolone['auto_import'] = $inputs['auto_import'];
					$data_kolone['auto_import_short'] = $inputs['auto_import_short'];
					$data_kolone['zadnji_kurs'] = $inputs['zadnji_kurs'];

					DB::table('dobavljac_cenovnik_kolone')->where('partner_id', $data['partner_id'])->update($data_kolone);
				}

				return Redirect::to(AdminOptions::base_url().'admin/kupci_partneri/partner/'.$data['partner_id'])->with('message','Uspešno ste sačuvali podatke');
				AdminSupport::saveLog('Partneri, EDIT partner_id -> '.$data['partner_id']);
			}
	}	
}

	public function deletePartner($id,$confirm){

		if($id == 1){
			return Redirect::to(AdminOptions::base_url().'admin/kupci_partneri/partneri');
		}

		$check_confirm = $confirm=='1'?true:false;
		$obj_narudzbina = DB::table('web_b2b_narudzbina')->select('web_b2b_narudzbina_id')->where('partner_id', $id);
		$check_narudzbina = $obj_narudzbina->count()>0?false:true;
		$obj_korpa = DB::table('web_b2b_korpa')->select('web_b2b_korpa_id')->where('partner_id', $id);
		$check_korpa = $obj_korpa->count()>0?false:true;
		$obj_partner_je = DB::table('partner_je')->where('partner_id', $id);
		$check_partner_je = $obj_partner_je->count()>0?false:true;
		$obj_dobavljac_cenovnik_kolone = DB::table('dobavljac_cenovnik_kolone')->where('partner_id', $id);
		$check_dobavljac_cenovnik_kolone = $obj_dobavljac_cenovnik_kolone->count()>0?false:true;
		$obj_dobavljac = DB::table('dobavljac_cenovnik')->where('partner_id', $id);
		$check_dobavljac = $obj_dobavljac->count()>0?false:true;
		$obj_kartica = DB::table('partner_kartica')->where('partner_id', $id);
		$check_kartica = $obj_kartica->count()>0?false:true;
		$obj_komercijalista = DB::table('partner_komercijalista')->where('partner_id', $id);
		$check_komercijalista = $obj_komercijalista->count()>0?false:true;
		$obj_kontakt = DB::table('partner_kontakt')->where('partner_id', $id);
		$check_kontakt = $obj_kontakt->count()>0?false:true;
		$obj_lokacija = DB::table('partner_lokacija')->where('partner_id', $id);
		$check_lokacija = $obj_lokacija->count()>0?false:true;
		$obj_rabat_grupa = DB::table('partner_rabat_grupa')->where('partner_id', $id);
		$check_rabat_grupa = $obj_rabat_grupa->count()>0?false:true;
		$obj_racun = DB::table('partner_racun')->where('partner_id', $id);
		$check_racun = $obj_racun->count()>0?false:true;
		$obj_vrsta_dokumenta = DB::table('partner_vrsta_dokumenta')->where('partner_id', $id);
		$check_vrsta_dokumenta = $obj_vrsta_dokumenta->count()>0?false:true;

		if($check_korpa && $check_narudzbina && $check_partner_je && $check_dobavljac_cenovnik_kolone && $check_dobavljac && $check_kartica && $check_komercijalista && $check_kontakt && $check_lokacija && $check_rabat_grupa && $check_racun && $check_vrsta_dokumenta){
			DB::table('partner')->where('partner_id', $id)->delete();
			AdminSupport::saveLog('Partneri, DELETE partner_id -> '.$id);
		}else{
			if($check_confirm){
				DB::table('web_b2b_narudzbina_stavka')->whereIn('web_b2b_narudzbina_id',array_map('current',$obj_narudzbina->get()))->delete();
				$obj_narudzbina->delete();
				DB::table('web_b2b_korpa_stavka')->whereIn('web_b2b_korpa_id',array_map('current',$obj_korpa->get()))->delete();
				$obj_korpa->delete();
				$obj_partner_je->delete();
				$obj_dobavljac_cenovnik_kolone->delete();
				DB::table('dobavljac_cenovnik_karakteristike')->where('partner_id', $id)->delete();
				DB::table('dobavljac_cenovnik_slike')->where('partner_id', $id)->delete();

				// $roba_ids = array_map('current',DB::table('roba')->select('roba_id')->where('dobavljac_id', $id)->get());
		        // DB::table('vezani_artikli')->whereIn('vezani_roba_id',$roba_ids)->delete();
		        // DB::table('roba_export')->whereIn('roba_id',$roba_ids)->delete();
		        // DB::table('web_roba_karakteristike')->whereIn('roba_id',$roba_ids)->delete();
		        // DB::table('osobina_roba')->whereIn('roba_id',$roba_ids)->delete();
		        // DB::table('roba')->where('dobavljac_id', $id)->delete();
		        DB::table('roba')->where('dobavljac_id', $id)->update(array('dobavljac_id'=>null,'sifra_d'=>null));

				$obj_dobavljac->delete();
				$obj_kartica->delete();
				$obj_komercijalista->delete();
				$obj_kontakt->delete();
				$obj_lokacija->delete();
				$obj_rabat_grupa->delete();
				$obj_racun->delete();
				$obj_vrsta_dokumenta->delete();

				DB::table('partner')->where('partner_id', $id)->delete();
				AdminSupport::saveLog('Partneri, DELETE partner_id -> '.$id);
			}else{
				return Redirect::to('admin/kupci_partneri/partneri')->with('confirm_partner_id',$id);
			}
		}
		return Redirect::to('admin/kupci_partneri/partneri')->with('message','Uspešno ste obrisali sadržaj.'); 

	}

	public function exportKupac($vrsta_fajla,$vrsta_kupca,$status_registracije,$imena_kolona='null',$search=null){

		if($imena_kolona != 'null'){
			$kolone = explode('-',$imena_kolona);
		}else{
			$kolone = array('ime','prezime','naziv','pib','email');
		}

		$db_data = AdminKupci::getKupci($vrsta_kupca,$status_registracije,$search,false);

		if($vrsta_fajla == 'xml'){
			$xml = new DOMDocument("1.0","UTF-8");
			$root = $xml->createElement("kupci");
			$xml->appendChild($root);

			foreach($db_data as $row){
				$kupac   = $xml->createElement("kupac");

				if($row->flag_vrsta_kupca == 0){
					if(in_array('ime',$kolone)){
						AdminSupport::xml_node($xml,'ime',$row->ime,$kupac);
					}
					if(in_array('prezime',$kolone)){
						AdminSupport::xml_node($xml,'prezime',$row->prezime,$kupac);
					}				
				}elseif($row->flag_vrsta_kupca == 1){
					if(in_array('naziv',$kolone)){
						AdminSupport::xml_node($xml,'naziv',$row->naziv,$kupac);
					}
					if(in_array('pib',$kolone)){
						AdminSupport::xml_node($xml,'pib',$row->pib,$kupac);
					}
				}

				if(in_array('email',$kolone)){
					AdminSupport::xml_node($xml,'email',$row->email,$kupac);
				}

				if(in_array('adresa',$kolone)){
					AdminSupport::xml_node($xml,'adresa',$row->adresa,$kupac);
				}

				if(in_array('mesto',$kolone)){
					AdminSupport::xml_node($xml,'mesto',$row->mesto,$kupac);
				}

				if(in_array('telefon',$kolone)){
					AdminSupport::xml_node($xml,'telefon',$row->telefon,$kupac);
				}
				
				$root->appendChild($kupac);
			}
			$xml->formatOutput = true;
			if(File::exists("files/kupci.xml")){
				File::delete("files/kupci.xml");
			}
			$xml->save("files/kupci.xml") or die("Error");
			$file = "files/kupci.xml";  

			AdminSupport::saveLog('Export meilova kupca');
	    	return Response::download($file);
	    }elseif($vrsta_fajla == 'xls'){
	    	header("Content-Type: application/vnd.ms-excel");
			header("Content-disposition: attachment; filename=export_kupci.xls");
	    	$printIme="";
	    	$printPrezime="";
	    	$printNaziv="";
	    	$printPib="";
	    	$printEmail="";
	    	$printAdresa="";
	    	$printMesto="";
	    	$printTelefon="";
				if(in_array('ime',$kolone)){
						$printIme="Ime";
					}
					if(in_array('prezime',$kolone)){
						$printPrezime="Prezime";
					}				
					if(in_array('naziv',$kolone)){						
						 $printNaziv="Naziv";
					}
					if(in_array('pib',$kolone)){
						 $printPib="Pib";
					}
					if(in_array('email',$kolone)){
						 $printEmail="Email";
					}
					if(in_array('adresa',$kolone)){
						 $printAdresa="Adresa";
					}
					if(in_array('mesto',$kolone)){
						 $printMesto="Mesto";
					}
					if(in_array('telefon',$kolone)){
						 $printTelefon="Telefon";
					}
			echo $printIme."\t".$printPrezime."\t".$printNaziv."\t".$printPib."\t".$printEmail."\t".$printAdresa."\t".$printMesto."\t".$printTelefon."\t\n";
	    	foreach($db_data as $row){
	    		$ime="";
	    		$prezime="";
	    		$naziv="";
	    		$pib="";
	    		$email="";
	    		$adresa="";
	    		$mesto="";
	    		$telefon="";
	    		if($row->flag_vrsta_kupca == 0){
					if(in_array('ime',$kolone)){
						$ime = $row->ime;
					}
					if(in_array('prezime',$kolone)){
						$prezime = $row->prezime;
					}				
				}elseif($row->flag_vrsta_kupca == 1){
					if(in_array('naziv',$kolone)){						
						 $naziv=$row->naziv;
					}
					if(in_array('pib',$kolone)){
						 $pib=$row->pib;
					}
				}
				if (in_array('email',$kolone)) {
					$email=$row->email;
				}
				if (in_array('adresa',$kolone)) {
					$adresa=$row->adresa;
				}
				if (in_array('mesto',$kolone)) {
					$mesto=$row->mesto;
				}
				if (in_array('telefon',$kolone)) {
					$telefon=$row->telefon;
				}
				echo $ime."\t".$prezime."\t".$naziv."\t".$pib."\t".$email."\t".$adresa."\t".$mesto."\t".$telefon."\t\n";
	    	}
	    }else{
		    return Redirect::to(AdminOptions::base_url().'/admin/kupci_partneri/kupci');
		}
	}

	public function exportMailsPartner($vrsta_fajla, $imena_kolona='null',$search=null){

		if($imena_kolona != 'null'){
			$kolone = explode('-',$imena_kolona);
		}else{
			$kolone = array('naziv','telefon','email');
		}

		$db_data = AdminKupci::getPartneriSearch($search,false);
		if($vrsta_fajla == 'xml'){

			$xml = new DOMDocument("1.0","UTF-8");
			$root = $xml->createElement("partneri");
			$xml->appendChild($root);

			foreach($db_data as $row){
				
				$partner   = $xml->createElement("partner");
				
				if(in_array('naziv',$kolone)){
					AdminSupport::xml_node($xml,'naziv',$row->naziv,$partner);
				}
				if(in_array('telefon',$kolone)){
					AdminSupport::xml_node($xml,'telefon',$row->telefon,$partner);
				}
				if(in_array('email',$kolone)){
					AdminSupport::xml_node($xml,'email',$row->mail,$partner);
				}
				if(in_array('adresa',$kolone)){
					AdminSupport::xml_node($xml,'adresa',$row->adresa,$partner);
				}
				if(in_array('mesto',$kolone)){
					AdminSupport::xml_node($xml,'mesto',$row->mesto,$partner);
				}
				
				$root->appendChild($partner);
			}
			$xml->formatOutput = true;
			if(File::exists("files/partneri.xml")){
				File::delete("files/partneri.xml");
			}
			$xml->save("files/partneri.xml") or die("Error");
			$file = "files/partneri.xml";  

			AdminSupport::saveLog('Export meilova kupca');
			return Response::download($file);
		}elseif($vrsta_fajla == 'xls'){
			header("Content-Type: application/vnd.ms-excel");
			header("Content-disposition: attachment; filename=export_kupci.xls");
	    	$printNaziv="";
	    	$printAdresa="";
	    	$printMesto="";
	    	$printEmail="";
	    	$printTelefon="";				
				if(in_array('naziv',$kolone)){						
					$printNaziv="Naziv";
				}
				if(in_array('adresa',$kolone)){
					$printAdresa="Adresa";
				}
				if(in_array('mesto',$kolone)){
					$printMesto="Mesto";
				}
				if(in_array('email',$kolone)){
					$printEmail="Email";
				}
				if(in_array('telefon',$kolone)){
					$printTelefon="Telefon";
				}
			echo $printNaziv."\t".$printAdresa."\t".$printMesto."\t".$printEmail."\t".$printTelefon."\t\n";
	    	foreach($db_data as $row){				
	    		$naziv="";
	    		$adresa="";
	    		$mesto="";
	    		$email="";
	    		$telefon="";
				if(in_array('naziv',$kolone)){						
					$naziv=$row->naziv;
				}
				if (in_array('adresa',$kolone)) {
					$adresa=$row->adresa;
				}
				if (in_array('mesto',$kolone)) {
					$mesto=$row->mesto;
				}
				if (in_array('email',$kolone)) {
					$email=$row->mail;
				}
				if (in_array('telefon',$kolone)) {
					$telefon=$row->telefon;
				}
				echo $naziv."\t".$adresa."\t".$mesto."\t".$email."\t".$telefon."\t\n";
	    	}
	    }else{
		    return Redirect::to(AdminOptions::base_url().'/admin/kupci_partneri/partneri');
		}
	}

	public function partneri_search(){
		$word = trim(Input::get('search'));

		$data=array(
	                "strana"=>'partneri',
	                "title"=> 'Partneri',
	                "partner" => AdminKupci::searchPartneri($word),
	                "word" => $word
	            );
			return View::make('admin/page', $data);
	}
	public function export_newSletter(){
		header('Content-type: text/plain; charset=utf-8');

		$query=DB::table('web_b2c_newsletter')->get();
		foreach ($query as $email) {
			echo $email->email.", ";		
		}
		            
       header('Content-Type: application/csv');
	   header('Content-Disposition: attachment; filename="newsletter.csv"'); 
       exit();
            
	}

	public function card_refresh($partner_id){
        if(AdminOptions::info_sys('calculus')){
            Calculus::updateCart($partner_id);
        }elseif(AdminOptions::info_sys('infograf')){
            Infograf::updateCart($partner_id);
        }elseif(AdminOptions::info_sys('logik')){
            Logik::updateCart($partner_id);
        }
        
        return Redirect::back();  
	}

}