<?php

class AdminPermissionController extends Controller {

    function administratori($id=null)
    {
        if(!Admin_model::check_admin()){
            if($id == 0){
                $id = null;
            }
            $admin = "AND imenik_id <> 0";
        }else{
            $admin = "";
        }

        if($id != null){
            $adninistrator = DB::table('imenik')->where('imenik_id',$id)->first();
        }
        $data=array(
            "strana"=>'administratori',
            "title"=> 'Administratori',
            "administratori" => DB::select("SELECT * FROM imenik WHERE imenik_id <> -1".$admin." ORDER BY imenik_id ASC"),
            "imenik_id"=> $id==null ? null : $id,
            "ime"=> $id!=null ? $adninistrator->ime : null,
            "prezime"=> $id!=null ? $adninistrator->prezime : null,
            "login"=> $id!=null ? $adninistrator->login : null,
            "password"=> $id!=null ? $adninistrator->password : null,
            "kvota"=> $id!=null ? intval($adninistrator->kvota) : null
        );

        return View::make('admin/page', $data);
    }

    function administrator_edit()
    {
        $inputs = Input::get();
        $login_unique = '';
        if($inputs['imenik_id'] != ''){
            $login_unique = ','.$inputs['imenik_id'].',imenik_id';
        }

        Validator::extend('one_digit', 'AdminSupport@one_digit');
        
        $validator_array = array(
            'ime' => 'required|between:1,20|regex:'.AdminSupport::regex().'',
            'prezime' => 'required|between:1,20|regex:'.AdminSupport::regex().'',
            'login' => 'required|email|unique:imenik,login'.$login_unique,
            'password' => 'required|between:8,20|one_digit'
            );

        $validator = Validator::make($inputs, $validator_array);
        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }else{
            if($inputs['imenik_id'] == ''){
                $imenik_id = DB::select("SELECT MAX(imenik_id) FROM imenik")[0]->max + 1;
                $inputs['imenik_id'] = $imenik_id;
                $inputs['jmbg'] = '';
                $inputs['pol'] = 'm';
                $inputs['password'] = $inputs['password']; //md5
                DB::table('imenik')->insert($inputs);
            }else{
                $imenik_id = $inputs['imenik_id'];
                $user = DB::table('imenik')->where('imenik_id',$imenik_id)->first();
                if(!is_null($user->aop_user_id)){
                    $inputs['aop_user_id'] = $user->aop_user_id;
                    $updated = AdminSupport::updateAOPUser((object) $inputs);
                    if($updated){
                        $inputs['password'] = $inputs['password']; //md5
                        unset($inputs['imenik_id']);
                        DB::table('imenik')->where('imenik_id',$imenik_id)->update($inputs);
                    }else{
                        $validator->getMessageBag()->add('login', 'Email already used.');
                        return Redirect::back()->withInput()->withErrors($validator->messages());
                    }
                }else{
                    $inputs['password'] = $inputs['password']; //md5
                    unset($inputs['imenik_id']);
                    DB::table('imenik')->where('imenik_id',$imenik_id)->update($inputs);                    
                }

            }

            AdminSupport::saveLog('Korisnici administracije, INSERT/EDIT imenik_id -> '.$imenik_id);
            return Redirect::to(AdminOptions::base_url().'admin/administratori/'.$imenik_id)->with('message','Uspešno ste sačuvali podatke.');            
        }        
    }

    function administrator_delete($id)
    {
        if($id != 0){            
            DB::table('imenik')->where('imenik_id',$id)->delete();
            AdminSupport::saveLog('Korisnici administracije, DELETE imenik_id -> '.$id);
        }
        return Redirect::to(AdminOptions::base_url().'admin/administratori');
    }

    function grupa_modula($id=null,$glavni_modul_id = null)
    {
        if(!Admin_model::check_admin()){
            if($id == 0){
                $id = null;
            }
            $administrator = "AND ac_group_id <> 0";
        }else{
            $administrator = "";
        }

        if($id!=null){    
            $grupa = DB::table('ac_group')->where('ac_group_id','!=',-1)->where('ac_group_id',$id)->first();
        }
        $data=array(
            "strana"=>'grupa_modula',
            "title"=>$id != null ? $grupa->naziv : 'Novi nivo',
            "grupe_modula" => DB::select("SELECT * FROM ac_group WHERE ac_group_id <> -1".$administrator." ORDER BY ac_group_id ASC"),
            "ac_group_id"=> $id != null ? $id : null,
            "naziv"=> $id != null ? $grupa->naziv : null,
            "opis"=> $id != null ? $grupa->opis : null,
            "moduli"=> $id != null ? array_map('current',DB::select("SELECT ac_module_id FROM ac_group_module WHERE ac_group_id=".$id." AND alow=1")) : array(),
            "glavni_modul_id"=> $glavni_modul_id != null ? $glavni_modul_id : null
        );

        return View::make('admin/page', $data);
    }

    function modul_grupa_edit()
    {
        $inputs = Input::get();

        $validator = Validator::make($inputs, array('naziv' => 'required|alpha_num'));
        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }else{
            if($inputs['ac_group_id'] == ''){
                $ac_group_id = DB::select("SELECT nextval('ac_group_ac_group_id_seq')")[0]->nextval;
                $inputs['ac_group_id'] = $ac_group_id;
                DB::table('ac_group')->insert($inputs);
                DB::statement("INSERT INTO ac_group_module (ac_group_id, ac_module_id, alow, alow_tico) SELECT ".intval($inputs['ac_group_id']).", ac_module_id, 0, 1 FROM ac_module WHERE aktivan = 1");
            }else{
                $ac_group_id = $inputs['ac_group_id'];
                unset($inputs['ac_group_id']);
                DB::table('ac_group')->where('ac_group_id',$ac_group_id)->update($inputs);
            }

            AdminSupport::saveLog('Nivoi pristupa, INSERT/EDIT ac_group_id -> '.$ac_group_id);
            return Redirect::to(AdminOptions::base_url().'admin/grupa-modula/'.$ac_group_id)->with('message','Uspešno ste sačuvali podatke.');
        }
    }

    function modul_grupa_delete($id)
    {
        if($id != 0){
            DB::table('ac_group_module')->where('ac_group_id',$id)->delete();
            DB::table('ac_group')->where('ac_group_id',$id)->delete();
        }

        AdminSupport::saveLog('Nivoi pristupa, DELETE ac_group_id -> '.$id);
        return Redirect::to(AdminOptions::base_url().'admin/grupa-modula');
    }
}

