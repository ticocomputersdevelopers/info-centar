<?php
use Export\BackupArticles;

class AdminBackupController extends Controller {

	function backup($backup_sifra,$kind,$key){

		if($backup_sifra == 'articles'){
			BackupArticles::execute($kind);
		}elseif($backup_sifra == 'images'){
			if($kind=='zip'){
				AdminSupport::zip_images();
			}else{
				return Redirect::to(AdminOptions::base_url());
			}
		}else{
			return Redirect::to(AdminOptions::base_url());
		}
	}


}