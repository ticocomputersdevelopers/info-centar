<?php
use Import\Support as ImportSupport;


class AdminImportController extends Controller {

    function web_import($grupa_pr_id=null, $proizvodjac = null, $dobavljac = null, $filteri = null, $search = null, $nabavna = null, $order = null)
    {
        $criteriaImport = array("grupa_pr_id" => $grupa_pr_id, "proizvodjac"=>$proizvodjac, "dobavljac"=>$dobavljac, "filteri"=>$filteri, "search"=>$search, "nabavna"=>$nabavna);    
        $limit = AdminOptions::limit_liste_robe();
        $pagination = array(
            'limit' => $limit,
            'offset' => Input::get('page') ? (Input::get('page')-1)*$limit : 0
            );

        $dobavljaci = AdminImport::dobavljaci_web_import();
        $tabela = AdminImport::fetchAll_Import($criteriaImport, $pagination, $order);

        $all_articles = array_map('current',AdminImport::fetchAll_Import($criteriaImport));
        $count = count($all_articles);
        
        $criteriaRoba = $criteriaImport;
        unset($criteriaRoba['filteri']);
        $roba = array(); 
        if($criteriaRoba['grupa_pr_id'] || $criteriaRoba['proizvodjac']  || $criteriaRoba['search']){
            unset($criteriaRoba['dobavljac']);
            $roba = AdminArticles::fetchAll($criteriaRoba,array('limit' => $limit,'offset' => 0),'r.roba_id-ASC');
        }


        if($dobavljac != 0) {
            $kurs = DB::table('dobavljac_cenovnik_kolone')->where('partner_id', $dobavljac)->pluck('zadnji_kurs');
            if($kurs == 0 || $kurs == '') {
                $kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
            }
        } else {
            $kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
        }
        

        $data=array(
            "strana"=>'web_import',
            "title"=>'Web import',
            "dobavljaci"=>$dobavljaci,
            "partner_id"=>$dobavljac != null && $dobavljac != 0 ? $dobavljac : '',
            "kurs"=>$kurs,
            "tabela"=>$tabela,
            "count"=>$count,
            "criteriaImport"=>$criteriaImport,
            "limit"=>$limit,
            "all_ids" => json_encode($all_articles),
            "roba"=>$roba,
            "naziv_width" => AdminOptions::web_import_kolone('naziv','width')
        );
        return View::make('admin/page', $data);
    }

    function upload_web_import()
    {
        $file = Input::file('upload_file');
        $inputs = Input::all();
          
        $upload_file = 0;
        $extension = '';
        if(Input::hasFile('upload_file') && $file->isValid() && ($file->getClientOriginalExtension() == 'xml' || $file->getClientOriginalExtension() == 'xls' || $file->getClientOriginalExtension() == 'xlsx' || $file->getClientOriginalExtension() == 'csv' || $file->getClientOriginalExtension() == 'json')){
            $upload_file = 1;
            // if($file->getClientOriginalExtension() == 'xls'){
            //     if(ImportSupport::xls_to_xlsx($file->getPathName(),"files/import.xlsx")){
            //         File::delete("files/import.xls");
            //         $extension = 'xlsx';
            //     }else{
            //         $upload_file = 0;
            //     }
            // }else{
                $extension = $file->getClientOriginalExtension();
                $file->move("./files/", 'import.'.$extension);
            // }

        }
        if(!isset($inputs['partner_id']) || (isset($inputs['partner_id']) && $inputs['partner_id']=='')){
            $inputs['partner_id'] = '0';
        }
        return Redirect::to(AdminOptions::base_url().'admin/web_import/0/0/'.$inputs['partner_id'].'/nn-nn-nn-nn-nn-nn-nn-nn-nn-nn-nn-nn/0/nn-nn')->withInput()->with('upload_file_js',array('import',$upload_file,$extension,$inputs['partner_id'],(isset($inputs['roba_insert']) ? '1' : '0')));
    }
    function import_podaci_grupa($partner_id,$partner_grupa_id){
        if($partner_grupa_id == 0){
            $grupe_dobavljaca = DB::table('dobavljac_cenovnik')->select('grupa','podgrupa')->distinct()->where('partner_id',$partner_id)->whereNotIn('grupa',array_map('current',DB::table('partner_grupa')->select('grupa')->where('partner_id',$partner_id)->get()))->orderBy('grupa','ASC')->get();
        }else{
            $grupe_dobavljaca = DB::table('dobavljac_cenovnik')->select('grupa','podgrupa')->distinct()->where('partner_id',$partner_id)->whereNotIn('grupa',array_map('current',DB::table('partner_grupa')->select('grupa')->where('partner_id',$partner_id)->where('partner_grupa_id','<>',$partner_grupa_id)->get()))->orderBy('grupa','ASC')->get();
        }

        $data=array(
            "strana"=>'import_podaci_grupa',
            "title"=>'Import podaci grupa',
            "partner_id"=>$partner_id,
            "partner_grupa_id"=>$partner_grupa_id,
            'partner_grupa' => $partner_id != 0 ? DB::table('partner_grupa')->where('partner_id',$partner_id)->get() : array(),
            'partner_grupa_item' => $partner_grupa_id != 0 ? DB::table('partner_grupa')->where('partner_grupa_id',$partner_grupa_id)->first() : (object) array('partner_grupa_id'=>0,'partner_id'=>$partner_id,'partner_id'=>null,'grupa'=>null,'grupa_pr_id'=>null,'web_marza'=>0, 'mp_marza'=>0),
            'grupe_dobavljaca' => $grupe_dobavljaca
            );
        return View::make('admin/page', $data);
    }
    function import_podaci_save(){
       $data=Input::all();
       if($data['web_marza']==''){
            unset($data['web_marza']);
       }
       if($data['mp_marza']==''){
            unset($data['mp_marza']);
       }
       $validate_arr = array(
        'partner_id' => 'not_in:0',
        'grupa_pr_id' => 'not_in:0,-1',
        'grupa' => 'required|regex:'.AdminSupport::regex().'|max:255|unique:partner_grupa,grupa,'.$data['partner_grupa_id'].',partner_grupa_id,partner_id,'.$data['partner_id'].'',
        'web_marza'=>'numeric|digits_between:0,15|regex:'.AdminSupport::regex(),
        'mp_marza'=>'numeric|digits_between:0,15|regex:'.AdminSupport::regex()
        );
       $messages = array(
            'required' => 'Niste popunili polje.',
            'regex' => 'Polje sadrži ne dozvoljene karaktere',
            'digits_between' => 'Dužina sadržaja je neodgovarajuća.',
            'unique' => 'Vrednost polja već postoji.',
            'numeric'=>'Polje može da sadrži samo brojeve.',
            'max'=>'Sadržaj polja je predugačak.',
            'not_in'=>'Niste izabrali stavku.'
        );
        $validator = Validator::make($data, $validate_arr, $messages);

        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator);
        }else{
            $mapped_all = isset($data['mapped_all']) ? true : false;
            $mapped_news = isset($data['mapped_news']) ? true : false;
            unset($data['mapped_all']);
            unset($data['mapped_news']);

            if($data['partner_grupa_id'] == 0){
                unset($data['partner_grupa_id']);
                DB::table('partner_grupa')->insert($data);
                $message = 'Uspešno ste uneli stavku.';
            }else{
                DB::table('partner_grupa')->where('partner_grupa_id',$data['partner_grupa_id'])->update($data);
                $message = 'Uspešno ste sačuvali izmene.';
            }

            $query = "SELECT dobavljac_cenovnik_id FROM dobavljac_cenovnik WHERE partner_id = ".$data['partner_id']." AND roba_id = -1 AND povezan = 0 AND flag_zakljucan = 0";
            $grupa_arr = explode(' -> ',$data['grupa']);
            if(isset($grupa_arr[1])){
                $query .= " AND grupa = '".$grupa_arr[0]."' AND podgrupa = '".$grupa_arr[1]."'";
            }else{
                $query .= " AND grupa = '".$grupa_arr[0]."'";
            }
            $dc_ids = array_map('current',DB::select($query));
            if($mapped_all){
                AdminImport::dc_mapped($dc_ids,true,true,false,false);
            }elseif($mapped_news){
                AdminImport::dc_mapped($dc_ids,false,true,false,false);
            }
        }
        return Redirect::to(AdminOptions::base_url().'admin/import_podaci_grupa/'.$data['partner_id'].'/0')->with('message',$message);  
}
    function import_podaci_delete($partner_id, $partner_grupa_id){
        $grupa = DB::table('partner_grupa')->where('partner_grupa_id',$partner_grupa_id)->pluck('grupa');

        $query = "UPDATE dobavljac_cenovnik SET grupa_pr_id = -1 WHERE partner_id = ".$partner_id." AND roba_id = -1 AND povezan = 0 AND flag_zakljucan = 0";
        $grupa_arr = explode(' -> ',$grupa);
        if(isset($grupa_arr[1])){
            $query .= " AND grupa = '".$grupa_arr[0]."' AND podgrupa = '".$grupa_arr[1]."'";
        }else{
            $query .= " AND grupa = '".$grupa_arr[0]."'";
        }
        DB::statement($query);

        DB::table('partner_grupa')->where('partner_grupa_id',$partner_grupa_id)->delete();
        
        return Redirect::to(AdminOptions::base_url().'admin/import_podaci_grupa/'.$partner_id.'/0')->with('message','Uspešno ste obrisali stavku'); 
    }

    function import_podaci_proizvodjac($partner_id,$partner_proizvodjac_id){
        if($partner_proizvodjac_id == 0){
            $proizvodjaci_dobavljaca = DB::table('dobavljac_cenovnik')->select('proizvodjac')->distinct()->where('partner_id',$partner_id)->whereNotIn('proizvodjac',array_map('current',DB::table('partner_proizvodjac')->select('proizvodjac')->where('partner_id',$partner_id)->get()))->orderBy('proizvodjac','ASC')->get();
        }else{
            $proizvodjaci_dobavljaca = DB::table('dobavljac_cenovnik')->select('proizvodjac')->distinct()->where('partner_id',$partner_id)->whereNotIn('proizvodjac',array_map('current',DB::table('partner_proizvodjac')->select('proizvodjac')->where('partner_id',$partner_id)->where('partner_proizvodjac_id','<>',$partner_proizvodjac_id)->get()))->orderBy('proizvodjac','ASC')->get();
        }


        $data=array(
            "strana"=>'import_partner_proizvodjac',
            "title"=>'Povezivanje proizvodjača',
            "partner_id"=>$partner_id,
            "partner_proizvodjac_id"=>$partner_proizvodjac_id,
            'parner_proizvodjaci' => $partner_id != 0 ? DB::table('partner_proizvodjac')->where('partner_id',$partner_id)->orderBy('partner_proizvodjac_id','DESC')->get() : array(),
            'parner_proizvodjac_item' => $partner_proizvodjac_id != 0 ? DB::table('partner_proizvodjac')->where('partner_proizvodjac_id',$partner_proizvodjac_id)->first() : (object) array('partner_proizvodjac_id'=>0,'partner_id'=>$partner_id,'proizvodjac_id'=>-1,'partner_id'=>null,'proizvodjac'=>null),
            'proizvodjaci_dobavljaca'=> $proizvodjaci_dobavljaca
        );        
        return View::make('admin/page', $data);     
    }

    function import_podaci_proizvodjac_save(){
        $data = Input::all();

        $rules = array(
            'partner_id' => 'not_in:0',
            'proizvodjac_id' => 'not_in:0,-1',
            'proizvodjac' => 'required|regex:'.AdminSupport::regex().'|max:255|unique:partner_proizvodjac,proizvodjac,'.$data['partner_proizvodjac_id'].',partner_proizvodjac_id,partner_id,'.$data['partner_id'].''
        );
        $messages = array(
            'not_in' => 'Niste izabrali stavku.',
            'required' => 'Niste popunili polje.',
            'regex' => 'Polje sadrži ne dozvoljene karaktere.',
            'max' => 'Sadržaj polja je predugačak.',
            'unique' => 'Vrednost polja već postoji.'
            );

        $validator = Validator::make($data, $rules, $messages);
        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'admin/import-podaci-proizvodjac/'.$data['partner_id'].'/'.$data['partner_proizvodjac_id'])->withInput()->withErrors($validator->messages());
        }else{
            $mapped_all = isset($data['mapped_all']) ? true : false;
            $mapped_news = isset($data['mapped_news']) ? true : false;
            unset($data['mapped_all']);
            unset($data['mapped_news']);

            if($data['partner_proizvodjac_id'] == 0){
                unset($data['partner_proizvodjac_id']);
                DB::table('partner_proizvodjac')->insert($data);
                $mesage = 'Uspešno ste uneli stavku.';
            }else{
                DB::table('partner_proizvodjac')->where('partner_proizvodjac_id',$data['partner_proizvodjac_id'])->update($data);
                $mesage = 'Uspešno ste sačuvali izmene.';
            }

            $dc_ids = array_map('current',DB::select("SELECT dobavljac_cenovnik_id FROM dobavljac_cenovnik WHERE partner_id = ".$data['partner_id']." AND roba_id = -1 AND povezan = 0 AND flag_zakljucan = 0 AND proizvodjac = '".$data['proizvodjac']."'"));
            if($mapped_all){
                AdminImport::dc_mapped($dc_ids,true,false,true,false);
            }elseif($mapped_news){
                AdminImport::dc_mapped($dc_ids,false,false,true,false);
            }

            return Redirect::to(AdminOptions::base_url().'admin/import-podaci-proizvodjac/'.$data['partner_id'].'/0')->with('message',$mesage);

        }
    }

    function import_podaci_proizvodjac_delete($partner_id,$partner_proizvodjac_id){
        $proizvodjac = DB::table('partner_proizvodjac')->where('partner_proizvodjac_id',$partner_proizvodjac_id)->pluck('proizvodjac');

        DB::statement("UPDATE dobavljac_cenovnik SET proizvodjac_id = -1 WHERE partner_id = ".$partner_id." AND roba_id = -1 AND povezan = 0 AND flag_zakljucan = 0 AND proizvodjac = '".$proizvodjac."'");

        DB::table('partner_proizvodjac')->where('partner_proizvodjac_id',$partner_proizvodjac_id)->delete();
        
        return Redirect::to(AdminOptions::base_url().'admin/import-podaci-proizvodjac/'.$partner_id.'/0')->with('message','Uspešno ste obrisali stavku');    
    }
    
}
