<?php

class AdminB2BController extends Controller {
    function index(){

        if(AdminB2BOptions::check_admin(array(9000))){
        	return Redirect::to('admin/b2b/narudzbine/0/0/0/0/0');
        }
    	return View::make('adminb2b/pocetna',array('strana'=>'b2b_pocetna','title'=>'B2B Pocetna'));
    }
    function position(){
        $order_arr = Input::get('order');

        foreach($order_arr as $key => $val){
            DB::table('web_b2b_seo')->where('web_b2b_seo_id',$val)->update(array('rb_strane'=>$key));
        }
        
    }

    function login_to_portal(){
        Session::put('b2b_user_'.Options::server(),1);
        if(B2bOptions::info_sys('infograf')){
            Session::put('b2b_user_'.B2bOptions::server().'_discounts',array());
        }
        return Redirect::to(AdminB2BOptions::base_url().'b2b/pocetna');
    }
}