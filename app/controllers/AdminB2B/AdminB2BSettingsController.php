<?php

class AdminB2BSettingsController extends Controller {

	function index(){
        if(!Admin_model::check_admin()){
            return Redirect::to(AdminB2BOptions::base_url().'admin/b2b');
        }
		$data=array(
			"strana" => 'b2b_podesavanja',
			"title" => 'Podešavanja'
		);
		return View::make('adminb2b/pages/podesavanja', $data);
	}

    function settings(){
        $inputs = Input::all();
        if($inputs['action'] == 'magacin_enable'){
            DB::table('orgj')->where('orgj_id',$inputs['id'])->update(array('b2b'=>$inputs['aktivan']));
            AdminSupport::saveLog('Podesavanja B2B, magacin id -> '.$inputs['id'].', aktivan -> '.$inputs['aktivan']);
        }
        elseif($inputs['action'] == 'magacin_primary'){
            DB::table('imenik_magacin')->where('imenik_magacin_id',20)->update(array('orgj_id'=>$inputs['id']));
            AdminSupport::saveLog('Podesavanja B2B, magacin id -> '.$inputs['id'].', PRIMARNI');

        }elseif($inputs['action'] == 'magacin_add'){
            $orgj_id = DB::table('orgj')->max('orgj_id')+1;
            $preduzece_id = 1;
            $sifra = '00'.$orgj_id;
            $level = 2;
            $tip_orgj_id = -1;
            $parent_orgj_id = 1;
            $b2b = 1;
            $naziv = $inputs['value'];

            DB::table('orgj')->insert(array(
                'orgj_id'=>$orgj_id,
                'preduzece_id'=> $preduzece_id,
                'sifra'=> $sifra,
                'level'=> $level,
                'tip_orgj_id'=> $tip_orgj_id,
                'parent_orgj_id'=> $parent_orgj_id,
                'naziv'=> $naziv,
                'b2b'=> $b2b
            ));
            AdminSupport::saveLog('Podesavanja B2B, magacin id -> '.$orgj_id.', '.$naziv);

        }elseif($inputs['action'] == 'magacin_delete'){
            $orgj_id = $inputs['orgj_id'];
            DB::table('orgj')->where('orgj_id',$orgj_id)->delete();
            AdminSupport::saveLog('Podesavanja B2B, magacin id -> '.$orgj_id.', DELETE');

        }elseif($inputs['action'] == 'switch_option'){
            DB::table('options')->where('options_id',$inputs['option_id'])->update(array('int_data'=>$inputs['value']));
            AdminSupport::saveLog('Podesavanja B2B MAGACIN, '.$inputs['value'].' ADD');

        }elseif($inputs['action'] == 'save_str_option'){
            DB::table('options')->where('options_id',$inputs['option_id'])->update(array('str_data'=>$inputs['value']));
            AdminSupport::saveLog('Podesavanja B2B OPTION, options id -> '.$inputs['option_id'].', str_data');
        }

    }
}