<?php

class AdminB2BAnalitikaController extends Controller {

    function analitika($datum_od_din=null, $datum_do_din=null)
    {      
        if(!AdminB2BOptions::check_admin(array(9000))){
            return Redirect::to(Option::base_url().'admin/b2b');
        }  

        $postojeNarudzbine = DB::table('web_b2b_narudzbina')->where(array('realizovano'=>1,'stornirano'=>0))->count() > 0;

        $between = "";
        if(isset($datum_od_din) && isset($datum_do_din)) {
            $between .= " AND datum_dokumenta BETWEEN '".$datum_od_din."' AND '".$datum_do_din."'";
        }

        $grupa_pr_ids1=array();
        $analitikaSkraceno=array();

        if($postojeNarudzbine){
        $analitikaSkraceno=DB::select("SELECT (SELECT grupa_pr_id FROM grupa_pr WHERE grupa_pr_id = (SELECT grupa_pr_id FROM roba WHERE roba_id = wbns.roba_id LIMIT 1)),
            (SELECT grupa FROM grupa_pr WHERE grupa_pr_id = (SELECT grupa_pr_id FROM roba WHERE roba_id = wbns.roba_id LIMIT 1) ),
            sum(kolicina*jm_cena) AS ukupno
            FROM web_b2b_narudzbina_stavka wbns join web_b2b_narudzbina n
             on wbns.web_b2b_narudzbina_id = n.web_b2b_narudzbina_id 
             WHERE wbns.web_b2b_narudzbina_id IN 
            (SELECT web_b2b_narudzbina_id FROM web_b2b_narudzbina WHERE realizovano = 1  and stornirano=0)
            ".$between."
            GROUP BY grupa_pr_id, grupa
            order by ukupno desc
            limit 5
            ");

        $grupa_pr_ids1=array_map('current', $analitikaSkraceno);
            }

            $ukupno1 = DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id', '!=', -1)->where('realizovano', 0)->where('prihvaceno', 0)->where('stornirano', '!=', 1);
        if(isset($datum_od_din) && isset($datum_do_din)) {
            $ukupno1 = $ukupno1->whereBetween('datum_dokumenta', array($datum_od_din, $datum_do_din));
        }
        $ukupno1 = $ukupno1->count();
    
        $ukupno2 = DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id', '!=', -1)->where('stornirano', '=', 1);
        if(isset($datum_od_din) && isset($datum_do_din)) {
            $ukupno2 = $ukupno2->whereBetween('datum_dokumenta', array($datum_od_din, $datum_do_din));
        }
        $ukupno2 = $ukupno2->count();
   
        $ukupno3 = DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id', '!=', -1)->where('realizovano', 1)->where('stornirano', '!=', 1);
        if(isset($datum_od_din) && isset($datum_do_din)) {
            $ukupno3 = $ukupno3->whereBetween('datum_dokumenta', array($datum_od_din, $datum_do_din));
        }
        $ukupno3 = $ukupno3->count();


        $ukupno4 = DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id', '!=', -1)->where('prihvaceno', 1)->where('realizovano','!=', 1)->where('stornirano', '!=', 1);
        if(isset($datum_od_din) && isset($datum_do_din)) {
            $ukupno4 = $ukupno4->whereBetween('datum_dokumenta', array($datum_od_din, $datum_do_din));
        }
        $ukupno4 = $ukupno4->count();

        $analitikaOstalo=array();
        if($postojeNarudzbine && count($grupa_pr_ids1) > 0){
            $analitikaOstalo=DB::select(
                "SELECT sum(kolicina*jm_cena) AS ostalo FROM web_b2b_narudzbina_stavka wbns WHERE wbns.web_b2b_narudzbina_id IN (SELECT web_b2b_narudzbina_id FROM web_b2b_narudzbina WHERE realizovano = 1 and stornirano=0) AND roba_id IN (SELECT roba_id FROM roba WHERE grupa_pr_id NOT IN (".implode(",",$grupa_pr_ids1)."))");
        }
        $ostalo = 0;
        if(isset($analitikaOstalo[0])){
            $ostalo = $analitikaOstalo[0]->ostalo;
        }

        $analitikaRuc=array();
        if($postojeNarudzbine){
            $analitikaRuc=DB::select("SELECT (SELECT grupa_pr_id FROM grupa_pr WHERE grupa_pr_id = (SELECT grupa_pr_id FROM roba WHERE roba_id = wbns.roba_id LIMIT 1)),
                (SELECT grupa FROM grupa_pr WHERE grupa_pr_id = (SELECT grupa_pr_id FROM roba WHERE roba_id = wbns.roba_id LIMIT 1) ),
                sum(CASE WHEN racunska_cena_nc > 0 THEN (kolicina*jm_cena/1.2) - racunska_cena_nc*kolicina ELSE 0 END)  AS razlika
                FROM web_b2b_narudzbina_stavka wbns WHERE web_b2b_narudzbina_id IN 
                (SELECT web_b2b_narudzbina_id FROM web_b2b_narudzbina WHERE realizovano = 1 and stornirano=0)
                GROUP BY grupa_pr_id, grupa
                order by razlika desc
                limit 5");
        }
        $analitikaGrupaPregled=DB::select("SELECT grupa_pr_id, (select(select grupa from grupa_pr  where grupa_pr.grupa_pr_id=roba.grupa_pr_id)) as grupa1, sum(pregledan_puta) from roba
            group by grupa_pr_id
            order by sum desc
            limit 5
            ");

        $grupa_pr_ids2=array_map('current', $analitikaRuc);
        $rucOstalo=array();
        if($postojeNarudzbine && count($grupa_pr_ids2) > 0){
            $rucOstalo=DB::select("SELECT sum(CASE WHEN racunska_cena_nc > 0 THEN (kolicina*jm_cena/1.2) - racunska_cena_nc*kolicina ELSE 0 END)  AS razlika FROM web_b2b_narudzbina_stavka wbns WHERE wbns.web_b2b_narudzbina_id IN (SELECT web_b2b_narudzbina_id FROM web_b2b_narudzbina WHERE realizovano = 1 and stornirano=0) AND roba_id IN (SELECT roba_id FROM roba WHERE grupa_pr_id NOT IN (".implode(",",$grupa_pr_ids2)."))");
        }

        $ostalo1 = 0;
        if(isset($rucOstalo[0])){
            $ostalo1 = $rucOstalo[0]->razlika;

        }

        $artikli = DB::select("
            SELECT roba_id, SUM(kolicina) as count 
            FROM web_b2b_narudzbina_stavka ns LEFT JOIN web_b2b_narudzbina n 
            ON n.web_b2b_narudzbina_id = ns.web_b2b_narudzbina_id 
            WHERE realizovano = 1 AND stornirano != 1
            GROUP BY roba_id 
            ORDER BY count DESC LIMIT 10");

        $analitikaGrupa=array();
        if($postojeNarudzbine){
            $analitikaGrupa=DB::select("SELECT (SELECT grupa_pr_id FROM grupa_pr WHERE grupa_pr_id = (SELECT grupa_pr_id FROM roba WHERE roba_id = wbns.roba_id LIMIT 1)),
                (SELECT grupa FROM grupa_pr WHERE grupa_pr_id = (SELECT grupa_pr_id FROM roba WHERE roba_id = wbns.roba_id LIMIT 1)),
                sum(kolicina), sum(kolicina*jm_cena) AS ukupno, sum(kolicina), sum(kolicina*jm_cena) AS ukupno,
                sum(CASE WHEN racunska_cena_nc > 0 THEN (kolicina*jm_cena/1.2) - racunska_cena_nc*kolicina ELSE 0 END)  AS razlika,
                sum(CASE WHEN racunska_cena_nc > 0 THEN kolicina ELSE 0 END) as nc_cena
                FROM web_b2b_narudzbina_stavka wbns WHERE web_b2b_narudzbina_id IN 
                (SELECT web_b2b_narudzbina_id FROM web_b2b_narudzbina WHERE realizovano = 1 and stornirano=0)
                GROUP BY grupa_pr_id, grupa
                ORDER BY ukupno DESC");
        }

        $prihod = DB::select("
            SELECT SUM(kolicina * jm_cena) as count 
            FROM web_b2b_narudzbina_stavka ns LEFT JOIN web_b2b_narudzbina n 
            ON n.web_b2b_narudzbina_id = ns.web_b2b_narudzbina_id 
            WHERE realizovano = 1 AND stornirano != 1 
            ".$between."
            ");

        $razlika = DB::select("
            SELECT SUM((kolicina*jm_cena/1.2) - racunska_cena_nc*kolicina) as count 
            FROM web_b2b_narudzbina_stavka ns LEFT JOIN web_b2b_narudzbina n 
            ON n.web_b2b_narudzbina_id = ns.web_b2b_narudzbina_id
            WHERE realizovano = 1 and racunska_cena_nc>0
            ".$between."
            ");

        $realizovano = DB::select("
            SELECT COUNT(realizovano) as count 
            FROM web_b2b_narudzbina           
            WHERE realizovano = 1 and stornirano !=1
            ".$between."
            ");

        $stornirano = DB::select("
            SELECT COUNT(web_b2b_narudzbina_id) as count 
            FROM web_b2b_narudzbina            
            WHERE stornirano =1
            ".$between."
            ");

        $prihvaceno = DB::select("
            SELECT COUNT(web_b2b_narudzbina_id) as count 
            FROM web_b2b_narudzbina            
            WHERE prihvaceno =1 AND stornirano != 1 AND realizovano != 1
            ".$between."
            ");

        $nove = DB::select("
            SELECT COUNT(web_b2b_narudzbina_id) as count 
            FROM web_b2b_narudzbina            
            WHERE prihvaceno =0 AND stornirano = 0 AND realizovano = 0
            ".$between."
            ");

        $ukupno = DB::select("
            SELECT COUNT(web_b2b_narudzbina_id) as count 
            FROM web_b2b_narudzbina            
            WHERE web_b2b_narudzbina_id != -1
            ".$between."
            ");

        $partner = array();
        if($postojeNarudzbine){
        $partner=DB::select("select par.naziv, sum(wbns.jm_cena*wbns.kolicina) as promet
            from web_b2b_narudzbina wbn
            join partner par on wbn.partner_id = par.partner_id
            join web_b2b_narudzbina_stavka wbns on wbns.web_b2b_narudzbina_id = wbn.web_b2b_narudzbina_id
            ".$between."
            group by par.naziv
            limit 5");
        }

        $prikazGrupa='';

 $data=array(
            'strana'=>'b2b_analitika',
            'title'=>'B2BAnalitika',
            'datum_od'=>isset($datum_od) ? $datum_od : '',
            'datum_do'=>isset($datum_do) ? $datum_do : '',
            'artikli'=>$artikli,
            'analitikaGrupa'=>$analitikaGrupa,
            'analitikaGrupaPregled'=>$analitikaGrupaPregled,
            'prikazGrupa'=>$prikazGrupa,
            'analitikaSkraceno'=>$analitikaSkraceno,
            'analitikaRuc'=>$analitikaRuc,
            'ostalo'=>$ostalo,
            'ostalo1'=>$ostalo1,
            'partner'=>$partner,
            'ukupno1'=>$ukupno1,
            'ukupno2'=>$ukupno2,
            'ukupno3'=>$ukupno3,
            'ukupno4'=>$ukupno4,
            'od' => isset($datum_od_din) ? $datum_od_din : '',
            'do' => isset($datum_do_din) ? $datum_do_din : ''
            );


            
        return View::make('adminb2b/pages/analitika', $data);
    }
}

    