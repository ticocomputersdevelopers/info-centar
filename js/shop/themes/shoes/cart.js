$(document).ready(function(){
	$('#JSRegToggle').click(function(){
		if($('#JSRegToggleSec').attr('hidden') == 'hidden'){
			$('#JSRegToggleSec').removeAttr('hidden');
		}else{
			$('#JSRegToggleSec').attr('hidden','true');
		}
	});

	$('#JSAddCartSubmit').click(function(){
		$(this).attr('disabled',true);
		$('#JSAddCartForm').submit();
	});
	
	$(document).on('click','.JSadd-to-cart',function(){
		var obj = $(this);
		var roba_id = obj.data('roba_id');
		$.post(base_url+'list-cart-add',{roba_id: roba_id},function(response){

			
			alertSuccess("Artikal je dodat u korpu.");

			var results = $.parseJSON(response);
			if(parseInt(results.check_available) == 0){
				obj.after('<button class="dodavnje not-available">Nije dostupno</button>');
				obj.remove();
			}
			$('.JSheader-cart-content').html(results.mini_cart_list);
			$('.JSbroj_cart').text(results.broj_cart);
			$('#JScart_ukupno').html(results.cart_ukupno);

		});
	});


	$(document).on('click','.JSadd-to-wish',function(){
		var obj = $(this);
		var roba_id = obj.data('roba_id');
		$.post(base_url+'wish-list-add',{roba_id: roba_id},function(response){
			var results = $.parseJSON(response);

			alertSuccess(results.message);

			$('.JSbroj_wish').text(results.broj_wish);
		});
	});

	$(document).on('click','.JSukloni',function(){
		var obj = $(this);
		var roba_id = obj.data('roba_id');
		
		swal("Arkal će biti uklonjen sa liste želja. Da li ste sigurni?", {
			  buttons: {
			    no: {
			      text: "Ne",
			      value: false,
			    },
			    yes: {
			      text: "Da",
			      value: true,
			    }
			  },
			}).then(function(value){
				if(value){
				  	$.post(base_url+'wish-list-delete',{roba_id: roba_id},function(response){
					setTimeout(function(){ location.reload(true); }, 800);
					alertSuccess("Artikal je uklonjen sa liste želja.");
				});

				} else{
					return 0;
				}
			});
	});


	$('.JSadd-to-cart-vezani').click(function(){
		var obj = $(this);
		var roba_id = obj.data('roba_id');
		var kolicina = obj.closest('div').find('.JSkolicina').val();
		$.post(base_url+'vezani-cart-add',{roba_id: roba_id, kolicina: kolicina},function(response){
			var results = $.parseJSON(response);

			alertSuccess("Artikal je dodat u korpu.");

			if(results.check_available <= 0){
				obj.closest('div').after('<button class="dodavnje not-available">Nije dostupno</button>');
				obj.closest('div').remove();			
			}

			$('.JSheader-cart-content').html(results.mini_cart_list);
			$('.JSbroj_cart').text(results.broj_cart);
		});
	});

	$('.JScart-less, .JScart-more').click(function(){
		var obj = $(this);
		var stavka_id = obj.data('stavka_id');
		var kolicina_temp = obj.closest('div').find('.JScart-amount').val();
		
		var kolicina;
		if(obj.attr('class') == 'JScart-less'){
			kolicina = parseInt(kolicina_temp) - 1;
		}
		else if(obj.attr('class') == 'JScart-more'){
			kolicina = parseInt(kolicina_temp) + 1;
		}

		if(kolicina > 0){		
			$.post(base_url+'cart-add-sub',{stavka_id: stavka_id, kolicina: kolicina},function(response){
				if(response=='kolicina-changed'){
					obj.closest('li').find('.JScart-amount').val(kolicina);
					setTimeout(function(){ location.reload(true); }, 800);
					alertAmount("Količina je promenjena.");
				}else{
					alertAmount("Količina je promenjena.");
				}
			});
		}else{
			alertError("Količina ne sme biti manja od 1")
		}
	});
	$('.JScart-amount').keyup(function(){
		var obj = $(this);
		var stavka_id = obj.data('stavka_id');
		var kolicina = $(this).val();

		if(kolicina > 0){		
			$.post(base_url+'cart-add-sub',{stavka_id: stavka_id, kolicina: kolicina},function(response){
				setTimeout(function(){ location.reload(true); }, 800);
				if(response=='kolicina-changed'){
					alertAmount("Količina je promenjena.");
				}else{
					alertAmount("Tražena količina nije dostupna.");
				}
			});
		}else{
			setTimeout(function(){ location.reload(true); }, 800);
			alertError("Količina ne sme biti manja od 1")
		}
	});

	$(document).on('click','.JSDeleteStavka',function(){
		var stavka_id = $(this).data('stavka_id');

		swal("Artikal će biti uklonjen iz korpe. Da li ste sigurni?", {
		  buttons: {
		    no: {
		      text: "Ne",
		      value: false,
		    },
		    yes: {
		      text: "Da",
		      value: true,
		    }
		  },
		}).then(function(value){
			if(value){
			    $.post(base_url+'cart-stavka-delete',{stavka_id: stavka_id},function(response){
				setTimeout(function(){ location.reload(true); }, 800);
				alertSuccess("Artikal je uklonjen iz korpe.");
			});

			} else{
				return 0;
			}
		});

	});
	
	if($('select[name="web_nacin_placanja_id"]').val() == 3){
		$('#JSCaptcha').removeAttr('hidden');
	}
	$(document).on('change','select[name="web_nacin_placanja_id"]',function(){
		if($(this).val() == 3){
			$('#JSCaptcha').removeAttr('hidden');
		}else{
			$('#JSCaptcha').attr('hidden','hidden');
		}
	});
	
    if (window.location.hash) {
        window.location.hash;
    }
});

function validate(evt) {
  var theEvent = evt || window.event;
  var key = theEvent.keyCode || theEvent.which;
  key = String.fromCharCode( key );
  var regex = /[0-9]|\./;
  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    if(theEvent.preventDefault) theEvent.preventDefault();
  }
} 