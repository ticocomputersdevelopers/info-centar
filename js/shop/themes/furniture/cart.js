$(document).ready(function(){
	$('#JSRegToggle').click(function(){
		if($('#JSRegToggleSec').attr('hidden') == 'hidden'){
			$('#JSRegToggleSec').removeAttr('hidden');
		}else{
			$('#JSRegToggleSec').attr('hidden','true');
		}
	});

	$('#JSAddCartSubmit').click(function(){
		$(this).attr('disabled',true);
		$('#JSAddCartForm').submit();
	});
	
	$(document).on('click','.JSadd-to-cart',function(){
		var obj = $(this);
		var roba_id = obj.data('roba_id');
		$.post(base_url+'list-cart-add',{roba_id: roba_id},function(response){
			$('.JSinfo-popup').fadeIn().delay(1000).fadeOut();
			$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>Artikal je dodat u korpu.</p>");

			var results = $.parseJSON(response);
			if(parseInt(results.check_available) == 0){
				obj.after('<button class="dodavnje not-available">Nije dostupno</button>');
				obj.remove();
			}
			$('.JSheader-cart-content').html(results.mini_cart_list);
			$('.JSbroj_cart').text(results.broj_cart);			

		});
	});

	 $(document).on('click','.JSAddToCartQuickView',function(){
		var obj = $(this);
		var roba_id = obj.data('roba_id');
		var kolicina = $('#JSQuickViewAmount').val();
		if(!(!isNaN(kolicina) && kolicina > 0)){
			$('.JSinfo-popup').fadeIn("fast").delay(1000).fadeOut("fast");
			$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>Niste pravilno uneli količinu.</p>");
		}else{
			$.post(base_url+'quick-view-cart-add',{roba_id: roba_id, kolicina: kolicina},function(response){
				var results = $.parseJSON(response);
				$('.JSinfo-popup').fadeIn("fast").delay(1000).fadeOut("fast");
				if(results.added){
					$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>Artikal je dodat u korpu.</p>");
					if(parseInt(results.check_available) == 0){
						$('#JSQuickViewAmount').remove();
						obj.after('<button class="not-available comment-btn" title="Nije dostupno"> Nije dostupno</button>');
						obj.remove();

						var obj_list = $('.JSadd-to-cart[data-roba_id="'+roba_id+'"]');
						obj_list.after('<button class="dodavnje not-available">Nije dostupno</button>');
						obj_list.remove();
					}
					$('.JSheader-cart-content').html(results.mini_cart_list);
					$('.JSbroj_cart').text(results.broj_cart);
					$('#JScart_ukupno').html(results.cart_ukupno);
				}else{
					$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>Tražena količina nije dostupna.</p>");
				}

			});
		}
	});

	$('.JSadd-to-cart-vezani').click(function(){
		var obj = $(this);
		var roba_id = obj.data('roba_id');
		var kolicina = obj.closest('div').find('.JSkolicina').val();
		$.post(base_url+'vezani-cart-add',{roba_id: roba_id, kolicina: kolicina},function(response){
			var results = $.parseJSON(response);
			$('.JSinfo-popup').fadeIn().delay(1000).fadeOut('fast');
			$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>Artikal je dodat u korpu.</p>");					
			if(results.check_available <= 0){
		        obj.after('<button class="dodavnje not-available">Nije dostupno</button>');
				obj.remove();			
			}
			$('.JSheader-cart-content').html(results.mini_cart_list);
			$('.JSbroj_cart').text(results.broj_cart);
		});
	});

	$('.JScart-less, .JScart-more').click(function(){
		var obj = $(this);
		var stavka_id = obj.data('stavka_id');
		var kolicina_temp = obj.closest('li').find('.JScart-amount').val();
		var kolicina;
		if(obj.attr('class') == 'JScart-less'){
			kolicina = parseInt(kolicina_temp) - 1;
		}
		else if(obj.attr('class') == 'JScart-more'){
			kolicina = parseInt(kolicina_temp) + 1;
		}
		if(kolicina > 0){		
			$.post(base_url+'cart-add-sub',{stavka_id: stavka_id, kolicina: kolicina},function(response){
				if(response=='kolicina-changed'){
					obj.closest('li').find('.JScart-amount').val(kolicina);
					location.reload(true);
					$('.JSinfo-popup').fadeIn().delay(1000).fadeOut('fast');
					$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>Količina je promenjena.</p>");
				}else{
					$('.JSinfo-popup').fadeIn().delay(1000).fadeOut('fast');
					$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>Tražena količina nije dostupna.</p>");
				}
			});
		}else{
			$('.JSinfo-popup').fadeIn().delay(1000).fadeOut('fast');
			$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>Količina ne sme biti manja od 1!</p>");
		}
	});

	$('.JScart-amount').keyup(function(){
		var obj = $(this);
		var stavka_id = obj.data('stavka_id');
		var kolicina = $(this).val();

		if(kolicina > 0){		
			$.post(base_url+'cart-add-sub',{stavka_id: stavka_id, kolicina: kolicina},function(response){
				location.reload(true);
				if(response=='kolicina-changed'){
					$('.JSinfo-popup').fadeIn().delay(1000).fadeOut('fast');
					$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>Količina je promenjena.</p>");
				}else{
					$('.JSinfo-popup').fadeIn().delay(1000).fadeOut('fast');
					$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>Tražena količina nije dostupna.</p>");
				}
			});
		}else{
			location.reload(true);
			$('.JSinfo-popup').fadeIn().delay(1000).fadeOut('fast');
			$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>Količina ne sme biti manja od 1!</p>");
		}
	});

	$(document).on('click','.JSDeleteStavka',function(){
		var stavka_id = $(this).data('stavka_id');
		bootbox.confirm({
            message: "<p>Artikal će biti uklonjen iz korpe. Da li ste sigurni?<p>",
            buttons: {
                cancel: {
                    label: 'Ne'
                },
                confirm: {
                    label: 'Da'
                }
            },
            callback: function (result) {
                if(result){
					$.post(base_url+'cart-stavka-delete',{stavka_id: stavka_id},function(response){
						$('.JSinfo-popup').fadeIn().delay(1000).fadeOut();
						$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>Artikal je uklonjen iz korpe.</p>");

						setTimeout(function(){
							location.reload(true);
						},1000);
					});
                }
            }
        });
	});
	
	$('#JSDeleteCart').click(function(){
		bootbox.confirm({
            message: "<p>Da li ste sigurni da želite da ispraznite korpu?<p>",
            buttons: {
                cancel: {
                    label: 'Ne'
                },
                confirm: {
                    label: 'Da'
                }
            },
            callback: function (result) {
                if(result){
					$.post(base_url+'cart-delete',{},function(response){
						$('.JSinfo-popup').fadeIn().delay(1500).fadeOut();
						$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>Korpa je ispražnjena.</p>");

						setTimeout(function(){
							location.reload(true);
						},1000);
					});
                }
            }
        });
	});

	$(document).on('click','.JSadd-to-wish',function(){
		var obj = $(this);
		var roba_id = obj.data('roba_id');
		$.post(base_url+'wish-list-add',{roba_id: roba_id},function(response){
			var results = $.parseJSON(response);
			$('.JSinfo-popup').fadeIn().delay(1000).fadeOut('fast');
			$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>"+results.message+"</p>");
			$('.JSbroj_wish').text(results.broj_wish);
		});
	});

	$(document).on('click','.JSukloni',function(){
		var roba_id = $(this).data('roba_id');
		
		bootbox.confirm({
            message: "<p>Artikal će biti uklonjen iz liste želja. Da li ste sigurni?<p>",
            buttons: {
                cancel: {
                    label: 'Ne'
                },
                confirm: {
                    label: 'Da'
                }
            },
            callback: function (result) {
                if(result){
					$.post(base_url+'wish-list-delete',{roba_id: roba_id},function(response){
						location.reload(true);
						$('.JSinfo-popup').fadeIn().delay(1500).fadeOut();
						$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>Artikal je uklonjen.</p>");
					});
                }
            }
        });
	});

    $(document).on('click','.JSnot_logged',function(){
		bootbox.alert({ 
			message: "<p>Da biste dodali proizvod na listu želja, morate biti ulogovani.</p>"
		});
	});

	if($('select[name="web_nacin_placanja_id"]').val() == 3){
		$('#JSCaptcha').removeAttr('hidden');
	}
	$(document).on('change','select[name="web_nacin_placanja_id"]',function(){
		if($(this).val() == 3){
			$('#JSCaptcha').removeAttr('hidden');
		}else{
			$('#JSCaptcha').attr('hidden','hidden');
		}
	});

    if (window.location.hash) {
        window.location.hash;
    }
});

function validate(evt) {
  var theEvent = evt || window.event;
  var key = theEvent.keyCode || theEvent.which;
  key = String.fromCharCode( key );
  var regex = /[0-9]|\./;
  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    if(theEvent.preventDefault) theEvent.preventDefault();
  }
} 