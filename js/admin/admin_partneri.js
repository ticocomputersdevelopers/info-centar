$(document).ready(function() {
	var partner_id = $('#JSCheckPartnerConfirm').data("id");
	if(partner_id!=0 && partner_id!=null){
		var check = confirm("Partner je vezan za narudžbine, import ili artikle! Da li želite obrisati sve sto je vezano za ovog partnera?");
		if(check){
			location.href = base_url+'admin/kupci_partneri/partneri/'+partner_id+'/1/delete';
		}
	}

	$('.JSExport').on('click', function(){
	$('#JSchoose').foundation('reveal', 'open');
	});

	$('.JSExportPartneri').click(function(){
		var names = $('.JSKolonaExport:checked').map(function(idx, elem) {
		    return $(elem).data('name');
		  }).get();
		var target_url = $(this).data('link');
		var target_url_arr = target_url.split('/');

		var url_names = 'null';
		if(names.length > 0){
			url_names = names.join('-');
		}
		target_url_arr[7] = url_names;
		
		location.href= target_url_arr.join('/');

	});

});