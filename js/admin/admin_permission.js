$(document).ready(function () {

	$('.JSGlavni, .JSSporedni').click(function(){

		var execute;
		if($(this).attr('checked')){
			$(this).removeAttr('checked');
			execute = 0;
		}else{
			$(this).attr('checked',true);
			execute = 1;
		}

		if($(this).hasClass('JSGlavni')){
			$('.JSSporednaKolona').hide();
		}

		var general_module = $(this).data('general-module');
		var data = {
			action: 'modul',
			execute: execute,
			group_id: parseInt(general_module.group),
			module_id: parseInt(general_module.module_id),
			tip: parseInt(general_module.tip)
		};

		$.post(base_url+'admin/ajax/permissions',data, function (response){});
	});

});
