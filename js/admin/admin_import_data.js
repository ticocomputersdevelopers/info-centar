
$(document).ready(function () {

	$('#JSDobavljacSelect').change(function(){
		var dobavljac_id = $(this).val();
		window.location.href = base_url+"admin/import-podaci-proizvodjac/"+dobavljac_id+"/0";
	});

	$(document).click(function(event){
		if(
			!$(event.target).hasClass('JSListaProizvodjac') 
			&& $(event.target).attr('name') != 'proizvodjac'
		){
			$('#JSListaProizvodjaca').attr('hidden','hidden');	
		}
		if(
			!$(event.target).hasClass('JSListaGrupa') 
			&& $(event.target).attr('name') != 'grupa'
		){
			$('#JSListaGrupe').attr('hidden','hidden');	
		}
	});
	//proizvodjaci
	$('input[name="proizvodjac"]').click(function(){
		if($('#JSListaProizvodjaca').attr('hidden') == 'hidden'){
			$('#JSListaProizvodjaca').removeAttr('hidden');
		}else{
			$('#JSListaProizvodjaca').attr('hidden','hidden');	
		}	
	});
	$('.JSListaProizvodjac').click(function(){
		var proizvodjac = $(this).data('proizvodjac');
		$('input[name="proizvodjac"]').val(proizvodjac);
		$('#JSListaProizvodjaca').attr('hidden','hidden');	
	});



	//Import podaci grupa
	$('#JSPartneriSelect').change(function(){
		var dobavljac_id = $(this).val();
		window.location.href = base_url+"admin/import_podaci_grupa/"+dobavljac_id+"/0";
	});
	//grupe
	$('input[name="grupa"]').click(function(){
		if($('#JSListaGrupe').attr('hidden') == 'hidden'){
			$('#JSListaGrupe').removeAttr('hidden');
		}else{
			$('#JSListaGrupe').attr('hidden','hidden');	
		}	
	});
	$('.JSListaGrupa').click(function(){
		var grupa = $(this).data('grupa');
		$('input[name="grupa"]').val(grupa);
		$('#JSListaGrupe').attr('hidden','hidden');	
	});

});