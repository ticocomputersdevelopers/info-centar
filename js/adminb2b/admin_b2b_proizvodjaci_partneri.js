$(document).ready(function () {


// RABAT PROIZVODJACA
$('.JSrabat').keyup(function(e){

	if(e.keyCode == 13) {

		var proizvodjac_id = $(this).data('id');
		var rabat = $(this).val();

		$(this).blur();

		$.post(
			base_url+'admin/b2b/ajax/rabat_proizvodjac_edit', {
				action: 'rabat_proizvodjac_edit',
				proizvodjac_id: proizvodjac_id,
				rabat: rabat
			}, function (response){
				if(response == 1) {
					alertify.success('Uspešno ste sačuvali rabat.');
				} else {
					alertify.error('Greška prilikom upisivanja u bazu.');
				}
			}
		);
		
	}
});

	
     var partner_ids = new Array();
     var selected_partner_ids;
     
	  $(function() {
	    $( "#selectable" ).selectable({
	      filter: 'tr',
	      stop: function() {
		      	partner_ids = [];
		      	$("td").removeClass("ui-selected");
		      	$("td").find('input').removeClass("ui-selected");
		        
		        $( ".ui-selected", this ).each(function() {
		          var id = $( this ).data("id");
		          partner_ids.push(id);
		        });
		        selected_partner_ids = partner_ids;
	        }
	    });
	  });

//RABAT PARTNERA
$('#rabat_partner_edit-btn').click(function(e){
	var rabat = $('.rabat_partner_edit').val();
	var partner_ids = selected_partner_ids;

	if(partner_ids && rabat != '') {

		$.post(
			base_url+'admin/b2b/ajax/rabat_partner_edit', {
				action: 'rabat_partner_edit',
				partner_ids: partner_ids,
				rabat: rabat
			}, function (response){

				alertify.success('Uspešno ste sačuvali rabat.');
				var data = $.parseJSON(response);

				$.each(data, function(index, value) {
	  				var obj_row = $("#selectable").find('[data-id="'+ index +'"]');
	  				obj_row.find('.rabat').html(value);
				});

			}
		);
	} else {
		alertify.error('Morate izabrati artikal/e i upisati željeni rabat.');
	}

});

//KATEGORIJA PARTNERA
$('#kategorija_partner_edit-btn').click(function(){

	var kategorija = $('.kategorija_partner_edit').val();
	var partner_ids = selected_partner_ids;

	if(partner_ids && kategorija != 'Bez kategorije') {
		
		$.post(				
			base_url+'admin/b2b/ajax/kategorija_partner_edit', {
				action: 'kategorija_partner_edit',
				partner_ids: partner_ids,
				kategorija: kategorija
			}, function (response){
			
				alertify.success('Uspešno ste sačuvali kategoriju.');

				 var data = $.parseJSON(response);

				$.each(data, function(index, value) {
	  				var obj_row = $("#selectable").find('[data-id="'+ index +'"]');
	  				obj_row.find('.kategorija').html(value);
				});

			}
		);
	} else if(partner_ids && kategorija == 'Bez kategorije'){

		$.post(
			base_url+'admin/b2b/ajax/kategorija_partner_edit', {
				action: 'kategorija_partner_edit',
				partner_ids: partner_ids,
				kategorija: kategorija
			}, function (response){
				
				alertify.success('Uspešno ste obrisali kategorije');
				var data = $.parseJSON(response);

				$.each(data, function(index, value) {
	  				var obj_row = $("#selectable").find('[data-id="'+ index +'"]');
	  				obj_row.find('.kategorija').html("Bez kategorije");
				});

			}
		);
	} else{
		alertify.error('Morate izabrati artikal/e.');
	}

});





	$(document).ready(function(){
	  $("#myInput").on("keyup", function() {
	    var value = $(this).val().toLowerCase();
	    $("#myTable tr").filter(function() {
	      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
	    });
	  });
	});
	
});