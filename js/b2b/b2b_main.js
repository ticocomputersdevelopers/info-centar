
$(document).ready(function () {
$('[data-toggle="tooltip"]').tooltip(); 
// SCROLL TO TOP
$(window).scroll(function () {
	if ($(this).scrollTop() > 150) {
		$('.JSscroll-top').css('right', '20px');
	} else {
		$('.JSscroll-top').css('right', '-70px');
	}
});

$('.JSscroll-top').click(function () {
	$('body,html').animate({
		scrollTop: 0
	}, 600);
	return false;
});

//Facebook
window.fbAsyncInit = function() {
	FB.init({
	appId      : 'your-app-id',
	xfbml      : true,
	version    : 'v2.4'
	});
};

(function(d, s, id){
var js, fjs = d.getElementsByTagName(s)[0];
if (d.getElementById(id)) {return;}
js = d.createElement(s); js.id = id;
js.src = "//connect.facebook.net/en_US/sdk.js";
fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

/*====== ACTION ARTICLES ==========*/
var akcija_products = $(".sale-products .product");
if ($(window).width() > 1024 ) {
	for(var i = 0; i < akcija_products.length; i+=4) {
		akcija_products.slice(i, i+4).wrapAll("<div class='single-slide'></div>");
	}
}else if($(window).width() > 768 ){
	for(var i = 0; i < akcija_products.length; i+=3) {
		akcija_products.slice(i, i+3).wrapAll("<div class='single-slide'></div>");
	}
}else{
	for(var i = 0; i < akcija_products.length; i+=1) {
		akcija_products.slice(i, i+1).wrapAll("<div class='single-slide'></div>");
	}
}
 
// RELATED ARTICLES TITLE
if($('body').find('.related-products').text().trim() == ''){
	$('.JSLinked').hide();
}
// IZDVAJAMO

// var izdvajamo_products = $(".fetured-products .product");
// for(var i = 0; i < izdvajamo_products.length; i+=12) {
// 	izdvajamo_products.slice(i, i+12).wrapAll("<div class='single-slide'></div>");
// }

// SLICK SLIDER INCLUDE

if ($("#main-slider")[0]){
	$('#main-slider').slick({
		autoplay: true
	});
}

if ($(".sale-products")[0]){
	$('.product-slider').slick({
		autoplay: true,
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1
	});
}

if ($(".related-products")[0]){
	$('.product-slider').slick({
		infinite: true,
		slidesToShow: 4,
		slidesToScroll: 1,
		arrows: false, 
		responsive: [
		{
			breakpoint: 1160,
			settings: {
			slidesToShow: 3,
			slidesToScroll: 3,
			infinite: true,
			dots: true
			}
		},
		{
			breakpoint: 880,
			settings: {
			slidesToShow: 2,
			slidesToScroll: 2
			}
		},
		{
			breakpoint: 480,
			settings: {
			slidesToShow: 1,
			slidesToScroll: 1
			}
		}
		]
	});
}

if ($(".b2bMostPopular")[0]){
	$('.b2bMostPopular').slick({
		infinite: true,
		slidesToShow: 4,
		slidesToScroll: 1,
		arrows: false,
		responsive: [
		{
			breakpoint: 1160,
			settings: {
			slidesToShow: 3,
			slidesToScroll: 3,
			infinite: true,
			dots: true
			}
		},
		{
			breakpoint: 880,
			settings: {
			slidesToShow: 2,
			slidesToScroll: 2
			}
		},
		{
			breakpoint: 480,
			settings: {
			slidesToShow: 1,
			slidesToScroll: 1
			}
		}
		]
	});
}
 
$('#JScategories .JSlevel-2 li').parent().parent().addClass('parent').append('<span class="JSsubcategory-toggler"><span class="glyphicon glyphicon-menu-down"></span></span>');
$('.JSsubcategory-toggler').click(function () {
    $(this).siblings('ul').slideToggle();
	$(this).parent().siblings().children('ul').slideUp();
});

// SUBCATEGORIES

$('.JSlevel-1 li').each(function () {
	var subitems = $(this).find(".JSlevel-2 > li");
	for(var i = 0; i < subitems.length; i+=2) {
	subitems.slice(i, i+2).wrapAll("<div class='clearfix'></div>");
	}
});

$('.JSCategoryLinkExpend').click(function (e) {
	e.preventDefault();
	$(this).next('ul').children().slideToggle('fast');
});

// SELECT ARROW - FIREFOX FIX
$('select').wrap('<span class="select-wrapper"></span>');
	
// POPUP
// $('.popup-close').click(function () {
// $('.popup').removeClass('popup-opened');
// });
// LOGIN POPUP
// $('#login-icon, .cart-login-button').click(function () {
// $('#b2b-login').removeClass('login-popup popup-opened');
// $('.login-popup').addClass('popup-opened');
// });
// REGISTRATION POPUP & FORM
// $('#registration-icon, .cart-registration-button').click(function () {
// $('#b2b-registration').removeClass('login-popup popup-opened');
// $('.registration-popup').addClass('popup-opened');
// });  
/*
$('#registration-icon, .cart-registration-button').click(function () {
$('.registration-popup').addClass('popup-opened');
$('.registration-form-open-buttons, .company-fields').hide();
$('.person-fields, .registration-popup h3').show();
$('.registration-form').slideDown(440);
$('.registration-popup').addClass('registration-popup-high');
});
$('.registration-cancel').click(function () {
$('.popup').removeClass('popup-opened');
});

*/
// $('.person-registration').click(function () {
// $('.registration-form-open-buttons, .company-fields').hide();
// $('.person-fields, .registration-popup h3').show();
// $('.registration-form').slideDown(440);
// $('.registration-popup').addClass('registration-popup-high');
// });  
// $('.company-registration').click(function () {
// $('.registration-form-open-buttons, .person-fields').hide();
// $('.company-fields, .registration-popup h3').show();
// $('.registration-form').slideDown(440);
// $('.registration-popup').addClass('registration-popup-high');
// }); 
// $('.registration-cancel').click(function () {
// $('.registration-form').slideUp(320);
// $('.registration-popup h3').hide();
// $('.registration-popup').removeClass('registration-popup-high');
// setTimeout(function() {
// $('.registration-form-open-buttons').fadeIn();
// }, 440);
// });

 
// PRODUCT LIST - SELECT FIELD

// $('.select-field').click(function (e) {
// $(this).children('ul').slideToggle('fast');
// $('.select-field ul').not($(this).children('ul')).slideUp('fast');
// e.stopPropagation();
// });
// $('body').click(function () {
// $('.select-field ul').slideUp('fast');
// });
// $(".select-field ul").click(function(e) {
// e.stopPropagation();
// });
// PAGINATION DOWN
// if ($('.pagination').prev('.product-list')) {
// $('.pagination').addClass('down');
// $('.product-list-options .pagination').removeClass('down');
// }
// FILTERS
// $(".select-field").click(function(){
// $(this).children('.multiselect').toggleClass('m-shown');
// $(".select-field").not(this).children('.multiselect').removeClass('m-shown');
// });
// PRODUCT PREVIEW IMAGE

	// if ($(".JSproduct-preview-image")[0]){
	// 	jQuery(".JSzoom_03").elevateZoom({gallery:'gallery_01', cursor: 'pointer', galleryActiveClass: 'active', imageCrossfade: true }); 
	// 	jQuery(".JSzoom_03").bind("click", function(e) { var ez = $('.JSzoom_03').data('elevateZoom');	$.fancybox(ez.getGalleryList()); return false; });
 //    }
	
 
// GENERATED FEATURES

// if ($(window).width() > 640 ) {
// 	$('.features-list-title').each(function () {
// 		var item_height= $(this).siblings('.features-list-items').height();
// 		$(this).height(item_height).css('line-height', item_height + 'px');
// 	});
// }

var searchTime;
$('#search').on("keyup", function() {
	clearTimeout(searchTime);
	searchTime = setTimeout(timer, 500);
});
$('html :not(.JSsearch_list)').on("click", function() {
	$('.JSsearch_list').remove();
});
 
function timer(){  
	$('.search_list').remove();
	var search = $('#search').val();
	if (search != '') {
		$.post(base_url + 'livesearch', {search:search}, function (response){
			//$('#JSlive-search').append(response);
		});
	} 
}

// function categori_dropdown(){
// $('.level-1 .parent').each(function(){
// var leftOffset = $(this).offset().left;
// var windowWidth = $(window).width();
// if (leftOffset > (windowWidth / 2)) {
// $(this).addClass('dropdown-left');
// }
// });
// }


/*****************B2B******************/

// $('#b2b-login-icon').click(function () {
// 	$('#b2b-login').addClass('login-popup popup-opened');
// });

// $('#b2b-registration-icon').click(function () {
// 	$('#b2b-registration').addClass('registration-popup popup-opened');
// });


// (function($){

// // init variables
// var $widoutRegForm,
// 	$btnPesonal,
// 	$btnNonePersonal,
// 	$widoutRegPersonalBox,
// 	$widoutRegNonePersonalBox;
// function onWidoutBtnNonePerosnalClick() {
// 	$this = $(this);
// 	$this.addClass("active");
// 	$btnPesonal.removeClass('active');
// 	$widoutRegNonePersonalBox.addClass('active');
// 	$widoutRegPersonalBox.removeClass('active');
// }
// function onWidoutBtnPerosnalClick() {
// 	$this = $(this);
// 	$this.addClass("active");
// 	$btnNonePersonal.removeClass('active');
// 	$widoutRegNonePersonalBox.removeClass('active');
// 	$widoutRegPersonalBox.addClass('active');
// }
// // event bind
// function eventsBind(){
// 	$btnPesonal.on('click', onWidoutBtnPerosnalClick);
// 	$btnNonePersonal.on('click', onWidoutBtnNonePerosnalClick);
// }
// $(document).ready(function(){
// 	$widoutRegForm = $('.without-reg-form');
// 	$btnPesonal = $widoutRegForm.find('.without-btn.personal');
// 	$btnNonePersonal = $widoutRegForm.find('.without-btn.none-personal');
// 	$widoutRegPersonalBox = $widoutRegForm.find('.without-reg-personal');
// 	$widoutRegNonePersonalBox = $widoutRegForm.find('.without-reg-none-personal');
// 	eventsBind();
// });
// })(jQuery);

// filters JAVASCIRP
// (function($){
// "use strict";
// // var init
// // f => filters
// var $fTitle,
// $fBox,
// $fMultiselect;
// // events binding
 
// function init (){
// if ($fBox.length) {
// $fBox.accordion_2({
// "transitionSpeed": 400
// });
// }
// }
// // on ready
// $(document).ready(function() {
// $fTitle = $(".filter-box__title");
// $fBox = $fTitle.parent();
// $fMultiselect = $fBox.find(".multiselect");
// init();
// bindings();
// });
// })(jQuery);

(function($){
	$("#valuta_eur").on("click", function(event){
		var base_url= $('#base_url').val();
		event.preventDefault();
		$.get(base_url + 'b2b/valuta/2', function (response){
			location.reload(true);
		});
	});	
	$("#valuta_din").on("click", function(event){
		var base_url= $('#base_url').val();
		event.preventDefault();
		$.get(base_url + 'b2b/valuta/1', function (response){
			location.reload(true);
		});
	});	
})(jQuery); 

(function($){
	var total_amount = $("#header_total_amount").val();
	$("#header_total_amount_show").html(total_amount);
})(jQuery);

// ADD TO CART BUTTON
$(document).ready(function() {
	var base_url= $('#base_url').val();
	$('.addCart').click(function(){
		var roba_id = $(this).data('product-id');
		var max = $(this).data('max-quantity');
		if(max <=0 ){
			$('.info-popup').fadeIn("fast").delay(1000).fadeOut();
			$('.info-popup .popup-inner').html("<p class='p-info'>Za dati arikal dodali ste maksimalnu kolicinu u korpu.</p>");
		}
		else {
			var _this = $(this);
			$.ajax({
				type: "POST",
				url: base_url + '/b2b/cart_add',
				cache: false,
				data:{roba_id:roba_id, status:2, quantity:1},
				success:function(res){
					$('.info-popup').fadeIn("fast").delay(1000).fadeOut();
					$('.info-popup .popup-inner').html("<p class='p-info'>Artikal je dodat u korpu.</p>");
					$('#broj_cart').text(res.countItems);
					$('#header-cart-content').html(res.cartContent);
					_this.data('max-quantity', res.cartAvailable);
					location.reload();
				}
			});
		}
	});
});
 
var base_url= $('#base_url').val();
$.ajax({
	type: "GET",
	url: base_url+'b2b/cart_content',
	success:function(res){
		$('#header-cart-content').html(res.cartContent);
	}
});

$('.add-to-cart-products').click(function(){
	var id = $(this).data('roba-id');
	var quantity =  $('#quantity-'+id).val();
	var max = $(this).data('max-quantity');

	if(isNaN(quantity)){
		$('.info-popup').fadeIn("fast").delay(1000).fadeOut("fast");
		$('.info-popup .popup-inner').html("<p class='p-info'>Tražena količina nije dostupna.</p>");	
	}else{
		if(quantity<1){
			$('.info-popup').fadeIn("fast").delay(1000).fadeOut("fast");
			$('.info-popup .popup-inner').html("<p class='p-info'>Minimalna količina je 1 kom.</p>");
			$('#quantity-'+id).val(1);
		}
		if(quantity > max){
			$('.info-popup').fadeIn("fast").delay(1000).fadeOut("fast");
			$('.info-popup .popup-inner').html("<p class='p-info'>Maksimalna količina je "+ max +" kom.</p>");
			$('#quantity-'+id).val(max);
		}
		else{
			var _this = $(this);
			$.ajax({
				type: "POST",
				url: base_url + '/b2b/cart_add',
				cache: false,
				data:{roba_id:id, status:2, quantity:quantity},
				success:function(res){
					$('.info-popup').fadeIn("fast").delay(1000).fadeOut();
					$('.info-popup .popup-inner').html("<p class='p-info'>Artikal je dodat u korpu.</p>");
					$('#broj_cart').text(res.countItems);
					$('#header-cart-content').html(res.cartContent);
					_this.data('max-quantity', res.cartAvailable);
					location.reload();
				}
			});
		}
	}
});

$('.filter-item').change(function (){
	var url = window.location.href;
	var name = $(this).data('name');
	var value = $(this).val();
	var old_value = $(this).data('old-value');
	var type = $(this).data('type');
	if(type=='add'){
		if(url.indexOf(name)>0){
			window.location.href = url.replace(name+"="+old_value , name+"="+value);
		}
		else {
			if(url.indexOf('?')>0){
				window.location.href = url+"&"+ name+"="+value;
			}
			else {
				window.location.href = url+"?"+ name+"="+value;
			}
		}
	}
	else if(type=='remove'){
		if(old_value == value) {
			window.location.href = url.replace(name+"="+old_value , "");
		}
		else {
			if(old_value.indexOf("-"+value)>0){
				var new_value = old_value.replace("-"+value,"");
				window.location.href = url.replace(name+"="+old_value , name+"="+new_value);
			}
			else if(old_value.indexOf(value+"-")>=0){
				var new_value = old_value.replace(value+"-","");
				window.location.href = url.replace(name+"="+old_value, name+"="+new_value);
			}
		}
	}
});
 
/* ======== Togle filter =========== */
$('.filter-links').click(function() {
	$(this).next('.select-fields-content').slideToggle('fast');
});

// FOOTER SOCIAL ICONS
$('footer .social-icons .facebook').append('<i class="fa fa-facebook-official"></i>');
$('footer .social-icons .twitter').append('<i class="fa fa-twitter-square"></i>');
$('footer .social-icons .google-plus').append('<i class="fa fa-google-plus-square"></i>');
$('footer .social-icons .instagram').append('<i class="fa fa-instagram"></i>');
$('footer .social-icons .skype').append('<i class="fa fa-skype"></i>');
$('footer .social-icons .linkedin').append('<i class="fa fa-linkedin-square"></i>');
$('footer .social-icons .youtube').append('<i class="fa fa-youtube-square"></i>');

 // RESPONSIVE NAVIGATON
$("header .resp-nav-btn").on("click", function(){  
	$("#responsive-nav").toggleClass("openMe");		 
});
$(".JSclose-nav").on("click", function(){  
	$("#responsive-nav").removeClass("openMe");		 
});

 // ARTICLE MODAL
(function($){
	var modal = $('#JSarticle-modal-container'),
		articleImg = $('#JSarticle-img'),
		modalImg = $("#JSarticle-modal-img"),
		trigger = $('.product-view');
		galeryImg = $('.img_01');

		trigger.click(function(){
			modal.show();
			modalImg.src = this.src;
		}); 
 
	 	galeryImg.click(function(e){
	 		e.preventDefault();
	 		var miniImg = $(this).attr('src');
	 		articleImg.attr('src', miniImg);
	 		modalImg.attr('src', miniImg);
	 	});

	 	$(".JSarticle-modal-close").click(function(){
	 		modal.fadeOut('fast');
	 	});  
 
})(jQuery);
 
 // Main Content Height
	if ($(window).width() > 1024 && $('.main-content').height() < $('.JSlevel-1').height()) {
		$('#JScategories').on('mouseover' ,function(){
			$('.main-content').height($('.JSlevel-1').height());
		});
		$('#JScategories').on('mouseleave' ,function(){
			$('.main-content').height('');
		});
	 }
	 

// POPUP BANER
	if ($(window).width() > 991 ) {
		var first_banner = $('.JSfirst-popup'),
		popup_img = $('.popup-img'),
		close_banner = $('.JSclose-me-please');
		if($('#main-slider').length){ //|| $('body').is('#product-page')
			
		 	setTimeout(function(){ 
			 	first_banner.animate({ top: '50%' }, 700);
			}, 1000);   

			close_banner.click(function(){ $('.JSfirst-popup').hide(); });  
		} 
	}

	 // LEFT & RIGHT BODY LINK
	if ($(window).width() > 1024 ) {  
		setTimeout(function(){ 
			$('body').before($('.JSleft-body-link').css('top' ,$('header').position().top + $('header').outerHeight() ));
			$('body').after($('.JSright-body-link').css('top' ,$('header').position().top + $('header').outerHeight() ));
			$('.JSleft-body-link, .JSright-body-link').width(($(window).outerWidth() - $('.container').outerWidth())/2)
			.height($('body').height()-$('footer').outerHeight()-$('header').outerHeight());   
		}, 1000);
  	} 

}); // $(document).ready(function () END
 

 